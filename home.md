<!-- TITLE: Protection des données personnelles -->
<!-- SUBTITLE: Dossier législatif du projet de loi sur la protection des données personnelles -->

# Projet de loi relatif la protection des données personnelles : Première lecture en Hémicycle
* **NOUVEAU (Mardi 6 février 2018) :** Suivez les discussions et l'adoption des amendements déposés lors de cette première lecture en hémicycle. <mark style="background-color: #F8EDA8;">Visitez [PARLEMENT LIVE](http://live.parlement-ouvert.fr/) !</mark>

* [Projet de loi](/pjl-592), version issue de la 1ère lecture en Commission à l'Assemblée
  (Projet de loi n° 592 relatif à la protection des données personnelles, après passage en première lecture à la Commission des lois de l'Assemblée nationale)
* [Loi informatique et liberté , intégrant les modifications de l'actuel projet de loi n°592](/loi-78-17-pjl-592), présenté actuellement à l'hémicycle.
* [Amendements en 1ère lecture en hémicycle à l'Assemblée](/amendements-1-assemblee-hemicycle)
# Projet de loi relatif la protection des données personnelles : Première lecture en Commission

* [Projet de loi](/pjl-490), version du gouvernement
  Projet de loi n° 490 relatif à la protection des données personnelles
* <mark style="background-color: #F8EDA8;">[Loi Informatique et libertés](/loi-78-17-pjl-490) version modifiée par le projet de loi initial</mark>
  Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, intégrant les modifications du projet de loi initial
* [Amendements en 1ère lecture à la Commission de lois de l'Assemblée](/amendements-1-assemblee-commission)

# Textes européens

* [Règlement européen](/reglement-2016-679)
  Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données

* [Directive européenne](/directive-2016-680)
  Directive (UE) 2016/680 du Parlement européen et du Conseil du 27 avril 2016 relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales, et à la libre circulation de ces données

# Lois françaises existantes

* [Loi Informatique et libertés](/loi-78-17) version originale
  Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
* [Loi République numérique](/loi-2016-1321)
  Loi n° 2016-1321 du 7 octobre 2016 pour une République numérique

# Avis, études et rapports

* [Étude d'impact](/etude-impact-490)
  Étude d'impact du projet de loi n° 490 relatif à la protection des données personnelles
* [Avis du Conseil d'État](/avis-conseil-etat-393836)
  Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
* [Délibération de la Commission nationale de l'informatique et des libertés (CNIL)](/deliberation-cnil-2017-299)
  Délibération n° 2017-299 du 30 novembre 2017 portant avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n°78-17 du janvier 1978
* [Rapport d'information sur les incidences des nouvelles normes européennes en matière de protection des données personnelles sur la législation française](/rapport-information-4544)
  Rapport n° 4544 du 22 février 2017 présenté par Anne-Yvonne Le Dain et Philippe Gosselin
* [Rapport d'information de la Commission des affaires européennes](/rapport-information-577)
  Rapport n° 577 du 18 janvier 2018 présenté par Christine Hennion

# Divers

* [Tous les liens externes](/liens-externes)

----

# À propos de ce site

Ce dossier législatif contributif vous est proposé par la [députée Paula Forteza](https://forteza.fr/), rapporteure du projet de loi relatif à la protection des données personnelles pour la Commission des lois de l'Assemblée nationale, et l'équipe du [bureau ouvert](https://forteza.fr/index.php/2017/10/26/bureau-ouvert/).

Ce site est un prototype, monté en quelques jours, servant à démontrer qu'avec quelques outils simples, il est possible de proposer :
* une navigation aisée dans un dossier législatif complexe
* une amélioration progressive de cette navigation grace à des contributions des visiteurs
* une interaction ouverte entre citoyens, entreprises, experts et responsables politiques lors de l'examen d'un texte législatif

Il reprend entre autres des éléments du [dossier législatif de l'Assemblée nationale](http://www.assemblee-nationale.fr/15/dossiers/donnees_personnelles_protection.asp).

[Comment contribuer](/documentation-technique/contribution) -- [Présentation technique](/documentation-technique/fonctionnement) -- [Mentions légales](/mentions-legales)

----

<div id='discourse-comments'></div>
<script type="text/javascript">
  DiscourseEmbed = {
		discourseUrl: 'https://forum.parlement-ouvert.fr/',
    discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/home'
	};
  (function() {
    var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
    d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
  })();
</script>

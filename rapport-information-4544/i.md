<!-- TITLE: I -->
<!-- SUBTITLE: LE RÉGIME EN VIGUEUR DE LA PROTECTION DES DONNÉES PERSONNELLES DANS L’UNION EUROPÉENNE : UN CADRE ANCIEN LAISSANT D’IMPORTANTES MARGES D’INTERPRÉTATION AUX ÉTATS MEMBRES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/introduction" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/i/a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

La directive 95/46/CE du 24 octobre 1995, applicable jusqu’au 25 mai 2018, constitue le cadre de référence de la protection des données dans l’Union européenne.

Cette directive n’est pas le premier instrument juridique de protection des données personnelles en Europe. Dès 1981, le Conseil de l’Europe a en effet adopté une convention pour la protection des personnes à l’égard des traitements automatisés des données à caractère personnel, **la convention 108** , aujourd’hui ratifiée par l’ensemble de ses États membres. Cette convention pose les principes de la collecte licite et loyale des données, de leur traitement à des fins légitimes définies, de leur caractère exact et non excessif par rapport aux finalités du traitement, ainsi que de leur conservation pendant une durée n’excédant pas celle nécessaire à ces finalités. Elle reconnaît le droit pour toute personne physique d’être informée de l’existence du traitement et de rectifier ses données. Elle autorise également les États parties à limiter les transferts internationaux de certaines catégories de données personnelles vers des États tiers lorsqu’ils ne garantissent pas une protection équivalente.

Par ailleurs, le droit à la protection des données personnelles fait partie des droits protégés par **l’article 8 de la Convention européenne des droits de l’homme** , dans le cadre du droit au respect de la vie privée et familiale, du domicile et de la correspondance.

* [A — LE CADRE DE RÉFÉRENCE DÉFINI PAR LA DIRECTIVE DE 1995, COMPLÉTÉ PAR LA DIRECTIVE DE 2002 « VIE PRIVÉE ET COMMUNICATIONS ÉLECTRONIQUES »](/rapport-information-4544/i/a)

  * [1 — La directive 95/46/CE du 24 octobre 1995 relative à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données](/rapport-information-4544/i/a/1)

    * [a — L’affirmation des grands principes de la protection des données personnelles](/rapport-information-4544/i/a/1/a)

    * [b — Des limites liées à l’évolution de l’environnement numérique et aux divergences d’application dans les États membres](/rapport-information-4544/i/a/1/b)

  * [2 — La directive 2002/58/CE du 12 juillet 2002 « vie privée et communications électroniques »](/rapport-information-4544/i/a/2)

* [B — L’APPORT DE LA COUR DE JUSTICE DE L’UNION EUROPÉENNE AU RÉGIME EUROPÉEN DE PROTECTION DES DONNÉES PERSONNELLES](/rapport-information-4544/i/b)

* [C — LES MESURES NATIONALES DE PROTECTION DES DONNÉES PERSONNELLES DES PERSONNES PHYSIQUES](/rapport-information-4544/i/c)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/introduction" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/i/a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

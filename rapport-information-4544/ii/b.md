<!-- TITLE: B -->
<!-- SUBTITLE: LE RENFORCEMENT DES DROITS DES PERSONNES PHYSIQUES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/ii/a/3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/ii/b/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [1 — L’évolution de la définition des données protégées](/rapport-information-4544/ii/b/1)

* [2 — Le renforcement du consentement](/rapport-information-4544/ii/b/2)

* [3 — L’extension du droit à l’information](/rapport-information-4544/ii/b/3)

* [4 — L’affirmation de nouveaux droits](/rapport-information-4544/ii/b/4)

  * [a — Le droit à l’effacement (« droit à l’oubli »)](/rapport-information-4544/ii/b/4/a)

  * [b — Le droit à la portabilité des données](/rapport-information-4544/ii/b/4/b)

  * [c — Le recours aux actions collectives](/rapport-information-4544/ii/b/4/c)

* [5 — La question du profilage](/rapport-information-4544/ii/b/5)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/ii/a/3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/ii/b/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: a -->
<!-- SUBTITLE: Le droit à l’effacement (« droit à l’oubli ») -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/ii/b/4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/ii/b/4/b" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Dans ses réponses au questionnaire écrit des rapporteurs, la direction des affaires civiles et du Sceau (DACS) du ministère de la justice indique que « _le « droit à l’oubli » recoupe différents droits que sont, d’une part, le droit au déréférencement (effacement d’un lien) consacré par l’arrêt de la CJUE du 13 mai 2014 Google Spain et, d’autre part, le droit à l’effacement de la donnée à caractère personnel que l’article 17 du règlement (UE) 2016/679 instaure_. »

Cet article définit le droit à l’effacement comme « _le droit_ [pour une personne] _d’obtenir du responsable du traitement l’effacement, dans les meilleurs délais, de données à caractères personnel la concernant_ ».

Les responsables de traitement auront l’obligation d’effacer les données personnelles dans les cas suivants :

– elles ne sont plus nécessaires au regard des finalités pour lesquelles elles ont été collectées ou traitées d’une autre manière ;

– la personne concernée retire le consentement sur lequel est fondé le traitement et il n’existe pas d’autre fondement juridique au traitement ;

– la personne concernée s’oppose au traitement et il n’existe pas de motif légitime impérieux pour le traitement, ou la personne concernée s’oppose au traitement à des fins de prospection ;

– les données à caractère personnel ont fait l’objet d’un traitement illicite ;

– les données à caractère personnel doivent être effacées pour respecter une obligation légale qui est prévue par le droit de l’Union ou par le droit de l’État membre auquel le responsable du traitement est soumis ;

– les données à caractère personnel ont été collectées auprès d’enfants.

Sans être reconnu comme tel, le droit à l’effacement avait déjà une existence par le biais des dispositions de la directive 95/46 et de la loi du 6 janvier 1978 imposant que la durée de conservation des données n’excède pas celle nécessaire aux finalités de la collecte et du traitement (43).

Outre l’élargissement des motifs ouvrant droit à l’effacement des données, en particulier lorsque les données ont été collectées auprès d’enfants, le règlement innove en prévoyant que les responsables de traitement qui avaient rendu les données publiques et qui auront été tenus de les effacer devront prendre « _des mesures raisonnables, y compris d’ordre technique, pour informer les responsables du traitement qui traitent ces données à caractère personnel que la personne concernée a demandé l’effacement par ces responsables du traitement de tout lien vers ces données_ [...] _, ou de toute copie ou reproduction de celles-ci_ ». Il s’agit cependant **uniquement d’une obligation de moyens** qui ne garantit pas l’effacement général des données et n’empêche pas leur éventuelle réutilisation publique ou privée.

Le paragraphe 3 de l’article 17 prévoit différentes limites : le droit à l’effacement ne s’applique pas lorsque le traitement est nécessaire à l’exercice du droit à la liberté d’expression et d’information ; pour respecter une obligation légale ou exécuter une mission d’intérêt public ; pour des motifs d’intérêt public dans le domaine de la santé publique ; à des fins archivistiques, de recherche scientifique ou historique ou à des fins statistiques ; à la constatation, à l’exercice ou à la défense des droits en justice.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/ii/b/4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/ii/b/4/b" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

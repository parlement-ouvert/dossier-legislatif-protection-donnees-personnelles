<!-- TITLE: b -->
<!-- SUBTITLE: Les nouvelles obligations pesant sur les entreprises -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/ii/c/2/a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/ii/c/3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Les nouveaux outils de mise en conformité des responsables de traitement et des sous-traitants sont la mise en place de mesures de protection des données appropriées, la tenue d’un registre des activités de traitement de données, la mise en place d’études de l’impact des traitements sur la vie privée et d’un délégué à la protection des données (DPO), la notification de failles de sécurité aux autorités et aux personnes concernées et des démarches de certification ou de code de bonne conduite.

 **•** _La protection des données dès la conception et la protection des données par défaut_

La responsabilisation des entreprises s’incarne par les **principes de la « protection des données dès la conception »** ( _privacy by design_ ) et de **« protection des données par défaut »** ( _privacy by default_ ). Ces principes imposent aux responsables du traitement des données de mettre en œuvre toutes les techniques nécessaires au respect de la protection des données personnelles, à la fois dès la conception du produit ou du service et par défaut.

**La définition du « _privacy by design_ » et du « _privacy by default_ » dans le règlement**

L’article 25 du règlement affirme ainsi que le responsable de traitement « _met en_ _œ uvre, _tant au moment de la détermination des moyens du traitement qu’au moment du traitement lui-même_ , des mesures techniques et organisationnelles appropriées, telles que la pseudonymisation, qui sont destinées à mettre en œuvre les principes relatifs à la protection des données, par exemple la minimisation des données, de façon effective et à assortir le traitement des garanties nécessaires afin de répondre aux exigences du présent règlement et de protéger les droits de la personne concernée._ »

Celui-ci met en œuvre les mesures techniques et organisationnelles appropriées « _pour garantir que, par défaut, seules les données à caractère personnel qui sont nécessaires au regard de chaque finalité spécifique du traitement sont traitées. Cela s’applique à la quantité de données à caractère personnel collectées, à l’étendue de leur traitement, à leur durée de conservation et à leur accessibilité. En particulier, ces mesures garantissent que, par défaut, les données à caractère personnel ne sont pas rendues accessibles à un nombre indéterminé de personnes physiques sans l’intervention de la personne physique concernée_. »

Concrètement, ces deux principes impliquent que les données traitées soient limitées à ce qui est strictement nécessaire à la finalité du traitement, que des durées de conservation aient été fixées, qu’il existe des règles de suppression automatique des données et qu’un processus d’habilitation soit mis en place pour l’accès aux données personnelles au bénéfice des seules personnes ayant à en connaître.

Parmi les techniques de sécurisation, le règlement évoque le chiffrement (59) et la pseudonymisation des données (60).

 **•** _L’obligation de tenir un registre des activités de traitement de données_

Afin de démontrer qu’ils ont mis en place des mesures de protection des données appropriées, l’article 30 du règlement prévoit que les responsables de traitement et les sous-traitants ont l’obligation de tenir un **«** _registre des activités de traitement effectuées sous leur responsabilité_ », ce registre étant mis à la disposition de l’autorité de contrôle à sa demande.

Le paragraphe 5 de l’article 30 précise cependant que cette obligation ne s’applique pas à une entreprise ou à une organisation **comptant moins de 250 employés** , sauf si le traitement qu’elles effectuent est susceptible de comporter un risque pour les droits et des libertés des personnes concernées, s’il n’est pas occasionnel ou s’il porte notamment sur les catégories particulières de données visées au paragraphe 1 de l’article 9 ( _61_ ), ou sur des données à caractère personnel relatives à des condamnations pénales et à des infractions visées à l’article 10 ( _62_ ) .

Ce registre, qui se présente sous forme écrite, devra être tenu à la disposition de l’autorité de contrôle et permettra à cette dernière de vérifier que l’entreprise a bien pris en compte la protection des données et que celle-ci est respectueuse de la réglementation en la matière.

**Le registre des activités de traitement**

En application du paragraphe 1 de l’article 30 du règlement, le responsable de traitements doit tenir un registre comportant les informations suivantes :

– le nom et les coordonnées du responsable du traitement et, le cas échéant, du responsable conjoint du traitement, du représentant du responsable du traitement et du délégué à la protection des données ;

– les finalités du traitement ;

– une description des catégories de personnes concernées et des catégories de données à caractère personnel ;

– les catégories de destinataires auxquels les données à caractère personnel ont été ou seront communiquées, y compris les destinataires dans des pays tiers ou des organisations internationales ;

– les transferts de données à caractère personnel vers un pays tiers ou à une organisation internationale, y compris l’identification de ce pays tiers ou de cette organisation internationale et, dans le cas des transferts visés à l’article 49 du règlement, les documents attestant de l’existence de garanties appropriées ;

Dans la mesure du possible, doivent aussi figurer les délais d’effacement des données et les mesures de sécurité mises en place.

Le paragraphe 2 de l’article 30 précise les informations contenues dans le registre tenu par le sous-traitant.

Concrètement les entreprises qui ont nommé un correspondant « informatique et liberté », comme leur en laissait la possibilité la loi du 6 janvier 1978 ( _63_ ) et le décret du 20 octobre 2005 (64), disposent déjà d’un registre interne que ce correspondant est chargé de tenir. Il devra néanmoins être complété.

 **•** _L’obligation de faire une analyse de l’impact des opérations de traitement sur la protection des données à caractère personnel_

L’article 35 du règlement prévoit que des **analyses de l’impact des opérations de traitement sur la protection des données à caractère personnel** devront être conduites par les responsables de traitement « _lorsqu’un type de traitement, en particulier par le recours à de nouvelles technologies, et compte tenu de la nature, de la portée, du contexte et des finalités du traitement, est susceptible d’engendrer un risque élevé pour les droits et libertés des personnes physiques_ ».

Ainsi, pour tous les traitements à risque et préalablement à ceux-ci, le responsable devra conduire une étude d’impact faisant apparaître les caractéristiques du traitement, les risques et les mesures adoptées.

L’article 35 précise qu’une analyse d’impact est obligatoire :

– lorsque le traitement consiste en une « _évaluation systématique et approfondie d’aspects personnels concernant des personnes physiques, qui est fondée sur un traitement automatisé, y compris le profilage, et sur la base de laquelle sont prises des décisions produisant des effets juridiques à l’égard d’une personne physique ou l’affectant de manière significative de façon similaire_ » ;

– lorsque sont traitées à grande échelle des catégories particulières de données visées au paragraphe 1 de l’article 9 du règlement (65) ou de données à caractère personnel relatives à des condamnations pénales et à des infractions visées à l’article 10 du règlement (1) ;

– lorsque le traitement consiste en une surveillance systématique à grande échelle d’une zone accessible au public.

La CNIL a précisé, quant à elle, qu’étaient concernés les traitements de données sensibles (66), et les traitements reposant sur « _l’évaluation systématique et approfondie d’aspects personnels des personnes physique_ s », c’est-à-dire notamment sur des techniques de profilage (67).

En cas de risque élevé, le responsable de traitements doit consulter l’autorité de protection des données avant de mettre en œuvre ce traitement. Cette dernière pourra s’opposer au traitement à la lumière de ses caractéristiques et conséquences.

Plusieurs personnes entendues par la mission ont considéré que la notion de « risque élevé » restait floue. Afin de la préciser, l’article 35 du règlement prévoit que les autorités de contrôle :

– établiront et publieront une liste des types d’opérations de traitement pour lesquelles une analyse d’impact relative à la protection des données est requise ;

– pourront aussi établir et publier une liste des types d’opérations de traitement pour lesquelles aucune analyse d’impact relative à la protection des données n’est requise.

L’article 35 du règlement précise que ces listes sont transmises au comité européen de protection des données afin de garantir que des critères compatibles sont adoptés au sein de l’Union européenne.

En outre, le G29 mène actuellement des travaux sur cette question et devrait adopter des lignes directrices au premier trimestre 2017. Cet avis devrait clarifier notamment :

– la notion de traitement pouvant engendrer des risques élevés pour lesquels une analyse d’impact relative à la protection des données est obligatoire ;

– les critères permettant de déterminer qu’une méthodologie d’analyse d’impact est compatible avec les exigences du règlement ;

– les conditions de consultation préalable de l’autorité, en application de l’article 36 du règlement.

 **•** _L’obligation de désigner un délégué à la protection des données_

L’article 37 du règlement prévoit la mise en place d’un délégué à la protection des données par certains responsables de traitement et sous-traitants.

La désignation d’un délégué à la protection des données sera obligatoire lorsque le traitement est effectué par une autorité publique ou un organisme public « _à l’exception des juridictions agissant dans l’exercice de leur fonction juridictionnelle_ ».

S’agissant des organismes privés, cette désignation sera obligatoire lorsque :

– les activités de base du responsable du traitement ou du sous-traitant consistent en des opérations de traitement qui « _du fait de leur nature, de leur portée et/ou de leurs finalités, exigent un suivi régulier et systématique à grande échelle des personnes concernées_ » ;

– les activités de base du responsable du traitement ou du sous-traitant consistent en « _un traitement à grande échelle de catégories particulières de données visées à l’article 9_ [du règlement] (68) _et de données à caractère personnel relatives à des condamnations pénales et à des infractions visées à l’article 10_ [du même règlement] (2) ».

**Les organismes publics devant désigner un délégué à la protection des données**

Le G29, dans des lignes directrices relatives au délégué à la protection personnelle (DPO) publiées le 13 décembre 2016 (69), rappelle que le règlement ne définit pas ce qu’est une « autorité ou un organisme public ». Si cette notion doit être déterminée en application du droit national, le G29 précise néanmoins que seront concernés les autorités nationales, régionales et locales, mais aussi les organismes de droit public et des personnes physiques ou morales de droit public ou privé, dans des secteurs tels que les services de transport public, de l’approvisionnement en eau et en énergie, des infrastructures routières, de la radiodiffusion de service public, du logement public ou des instances disciplinaires pour les professions réglementées. Dans ces cas, la désignation d’un DPO est obligatoire.

Bien qu’il n’y ait aucune obligation dans de tels cas, le G29 recommande qu’un délégué à la protection des données soit désigné dans des organismes privés exerçant des fonctions publiques ou exerçant une autorité publique et que l’activité de ce DPO concerne toutes les opérations de traitement effectuées, y compris celles qui ne sont pas liées à l’exécution d’une tâche publique (par exemple, la gestion d’une base de données).

L’article 37 du règlement précise que le DPO est désigné sur la base de ses qualités professionnelles et, en particulier, « _de ses connaissances spécialisées du droit et des pratiques en matière de protection des données, et de sa capacité à accomplir les missions_ ».

Le délégué à la protection des données personnelles peut être un membre du personnel du responsable de traitement ou du sous-traitant, il peut aussi être externe à l’entreprise et accomplir ses missions dans le cadre d’un contrat de service. Il n’est pas nécessairement exclusivement dédié à la protection des données, il peut ainsi avoir d’autres missions, à charge, pour le responsable de traitement, de veiller à ce que ces missions n’entraînent pas de conflits d’intérêts.

Le responsable du traitement et le sous-traitant doivent veiller à ce que le délégué à la protection des données :

– soit associé, d’une manière appropriée et en temps utile, à toutes les questions relatives à la protection des données à caractère personnel ;

– dispose de toutes les ressources nécessaires pour exercer ces missions, ainsi que l’accès aux données à caractère personnel et aux opérations de traitement ;

– ne reçoive aucune instruction en ce qui concerne l’exercice des missions (il ne peut être relevé de ses fonctions ou pénalisé par le responsable du traitement ou le sous-traitant pour l’exercice de ses missions) ;

Ce délégué est chargé :

– d’informer et de conseiller le responsable de traitements ou le sous-traitant, ainsi que ses employés ;

– de contrôler le respect du règlement européen et du droit national en matière de protection des données ;

– de conseiller l’organisme sur la réalisation d’une analyse d’impact et d’en vérifier l’exécution ;

– de coopérer avec l’autorité de contrôle et d’être le point de contact de celle-ci.

Concrètement les entreprises qui ont nommé un correspondant « informatique et liberté » (CIL), comme leur en laissaient la possibilité la loi du 6 janvier 1978 et le décret du 20 octobre 2005, ont déjà une personne en leur sein dont les fonctions sont dédiées la protection des données. 18 000 organismes sont aujourd’hui dotés d’un tel correspondant. En revanche, comme le montre le tableau ci-dessous, ces entreprises devront procéder à certains aménagements, les fonctions du DPO étant plus larges que celles du CIL. Par ailleurs, la liste des traitements actuellement tenus par les CIL ne coïncide pas avec le registre de des activités de traitement.

 **LE STATUT ET LES MISSIONS DU CIL ET DU DPO**

<table>
<tr>
<td>

</td>
<td>

</td>
<td>

**CIL**

</td>
<td>

**DPO**

</td></tr>
<tr>
<td>

**Statut**

</td>
<td>

Caractère obligatoire

</td>
<td>

Non

</td>
<td>

Oui

</td></tr>
<tr>
<td>

Indépendance fonctionnelle

</td>
<td>

Oui

</td>
<td>

Oui

</td></tr>
<tr>
<td>

Rattachement au plus haut niveau de la direction du responsable de traitement

</td>
<td>

Non

</td>
<td>

Oui

</td></tr>
<tr>
<td>

Obligation du responsable de traitements de fournir les moyens

</td>
<td>

Oui

</td>
<td>

Oui

</td></tr>
<tr>
<td>

Expertise attestée

</td>
<td>

Non

</td>
<td>

Oui

</td></tr>
<tr>
<td>

Externalisation

</td>
<td>

Condition de seuil

</td>
<td>

Sans condition

</td></tr>
<tr>
<td>

Secret professionnel

</td>
<td>

Non

</td>
<td>

Oui

</td></tr>
<tr>
<td>

À l’abri des conflits d’intérêts

</td>
<td>

Oui

</td>
<td>

Oui

</td></tr>
<tr>
<td>

**Missions**

</td>
<td>

Assurer le respect de la législation

</td>
<td>

Veille

</td>
<td>

Oui

</td></tr>
<tr>
<td>

Contrôle a priori formalisé pour les traitements à risques

</td>
<td>

Facultatif

</td>
<td>

Oui

</td></tr>
<tr>
<td>

Sensibilisation

</td>
<td>

Oui

</td>
<td>

Oui

</td></tr>
<tr>
<td>

Supervision des audits

</td>
<td>

Non

</td>
<td>

Oui

</td></tr>
<tr>
<td>

Saisine directe par les personnes concernées

</td>
<td>

Non

</td>
<td>

Oui

</td></tr>
<tr>
<td>

Avis sur les études d’impact sur la vie privée

</td>
<td>

Non prévu

</td>
<td>

Oui

</td></tr>
<tr>
<td>

Correspondant de la CNIL

</td>
<td>

Oui

</td>
<td>

Oui

</td></tr>
<tr>
<td>

Droit d’alerte

</td>
<td>

Oui

</td>
<td>

Non prévu

</td></tr>
<tr>
<td>

Coopération avec la CNIL

</td>
<td>

Non

</td>
<td>

Oui

</td></tr></table>

_Source : «_ Le règlement sur la protection des données : les 10 commandements à connaître pour passer de la théorie à la pratique _», par M. Emmanuel Jouffin, responsable juridique de banque, M. Xavier Lemarteleur, responsable juridique Technologies de l’information et Mme Marie-Noëlle Gibon, correspondante informatique et liberté du groupe La Poste et La Banque Postale, Revue de Droit bancaire et financier (n° 4, juillet 2016, étude 18)._

 **•** _L’obligation de notification des violations de données à caractère personnel_

Lorsqu’il constate une violation de données à caractère personnel, le responsable du traitement des données doit la **notifier à l’autorité nationale de protection des données dans un délai de 72 heures** , « _à moins que la violation en question ne soit pas susceptible d’engendrer un risque pour les droits et libertés des personnes physiques_ » (article 33 du règlement)  (70)

Par ailleurs l’article 34 prévoit que l **’information des personnes concernées** est requise si cette violation est susceptible d’engendrer « un risque élevé » pour les droits et libertés de ces personnes.

 **La notification de la violation des données personnelles à la personne concernée**

La communication auprès de la personne concernée doit intervenir dans les meilleurs délais et dans des termes « _clairs et simples_ ». Les informations communiquées visent, la nature de la violation, les coordonnés du délégué à la protection des données, les conséquences probables de la violation, une description des mesures prises ou à prendre pour remédier à la violation. L’autorité de contrôle a la faculté d’exiger du responsable du traitement qu’il procède à cette communication (art. 34 du règlement).

Une telle information individuelle n’est pas nécessaire si l’une des conditions suivantes est remplie :

– les mesures de protection techniques et organisationnelles appropriées notamment le chiffrement des données ont été prises ;

– des mesures ultérieures garantissant que le risque pour les droits et libertés des personnes n’est plus susceptible de se matérialiser ont été prises ;

– si la communication individuelle exige « _des efforts disproportionnés_ », une communication publique ou une autre mesure étant alors possible.

Si le règlement ne définit pas ce qu’est une « _violation de données à caractère personnel_ », l’article 34 bis I de la loi de 1978 la définit comme « _toute violation de la sécurité entraînant accidentellement ou de manière illicite la destruction, la perte, l’altération, la divulgation ou l’accès non autorisé à des données à caractère personnel faisant l’objet d’un traitement dans le cadre de la fourniture au public de services de communications électroniques_ ».

 **•** _Développer les codes de bonne conduite et la certification pour favoriser le respect du règlement_

Le règlement réserve une place importante à des mécanismes tels que les codes de bonne conduite et la certification.

Ainsi la certification d’un responsable de traitement, ou d’un sous-traitant situé à l’étranger, pourra servir de base à un transfert de données vers l’étranger. De même, l’application d’un code de conduite ou l’obtention d’une certification pourra être utilisée pour démontrer le bon respect par le responsable de traitement ou un sous-traitant de ses obligations en matière de protection de données (article 25 du règlement), de garanties apportées par un sous-traitant (article 28 du règlement) ou de l’obligation de sécurités des données (article 32 du règlement). Enfin, lorsque l’autorité de contrôle envisage d’infliger une sanction administrative à une entreprise, le fait que cette dernière ait appliqué des codes de conduite, ou obtenu une certification, constitue un élément pris en considération afin de moduler ladite sanction (article 83 du règlement).

 **Les codes de bonne conduite et la certification dans le règlement**

L’article 40 du règlement indique que les **codes de bonne conduite** , qui peuvent être élaborés par des associations ou des syndicats professionnels, peuvent être soumis et approuvés par l’autorité de contrôle. L’article 41 du règlement précise que le contrôle du respect du code de conduite peut être effectué « _par un organisme qui dispose d’un niveau d’expertise approprié au regard de l’objet du code et qui est agréé à cette fin par l’autorité de contrôle compétente_ ».

Une **certification** est une démarche volontaire par laquelle l’entreprise fait constater la conformité de ses procédures en matière de protection des données à une norme avalisée par l’autorité de contrôle. Contrairement aux codes de conduites, la certification ne peut être délivrée que par un organisme de certification, celui-ci ayant été au préalable agréé par l’autorité de contrôle.

Dans les éléments transmis à la mission, la CNIL rappelle qu’elle dispose par rapport à ses homologues européens d’une expérience acquise en la matière, puisqu’elle délivre des labels depuis 2012. Cette mission est donc confortée et élargie par le règlement européen, puisque la CNIL pourra :

– délivrer des labels nationaux, sur la base de ses propres référentiels (4 à ce jour) et européens, sur la base de référentiels européens, approuvés par le comité européen de protection des données ;

– agréer des tiers certificateurs sur la base de critères approuvés par le comité européen de protection des données et sur la base de critères définis par elle ;

– contrôler la certification et les labels délivrés.

Des lignes directrices du G29 sont en cours d’élaboration sur ce sujet.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/ii/c/2/a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/ii/c/3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

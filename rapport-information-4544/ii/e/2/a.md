<!-- TITLE: a -->
<!-- SUBTITLE: Un interlocuteur unique pour les responsables de traitement -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/ii/e/2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/ii/e/2/b" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Les responsables de traitement ne rendront compte qu’à une seule autorité de contrôle au sein de l’Union européenne. Suivant une logique de guichet unique, ces organismes devront s’adresser à l’autorité de contrôle du pays où ils disposent de leur établissement principal, cette autorité étant alors désignée comme l’autorité « chef de file » (article 56 du règlement). Cet établissement principal étant entendu comme le lieu de leur siège central dans l’Union européenne ou de l’établissement au sein duquel sont prises les décisions relatives aux finalités et aux modalités du traitement de données (16 de l’article 4 du règlement).

Les entreprises bénéficieront ainsi d’un interlocuteur unique pour l’Union européenne en matière de protection des données personnelles lorsqu’elles mettront en œuvre des traitements transnationaux.

Les autorités de protection nationales sont réunies au sein d’un Comité européen de la protection des données (CEPD), créé par l’article 68 du règlement, qui veille à l’application uniforme du droit sur la protection des données. Il a vocation à remplacer l’actuel G 29.

 **Le comité européen de la protection des données (CEPD)**

L’article 68 du règlement prévoit que le CEPD est composé des chefs des autorités de contrôle de chaque État membre et du contrôleur européen de la protection des données, ou de leurs représentants respectifs. Le contrôleur est nommé pour un mandat de cinq ans renouvelable.

En application de l’article 70 du règlement, le CEPD :

– contrôle les activités de traitement de données à caractère personnel par l’administration européenne afin d’en vérifier la conformité avec les règles en matière de protection de la vie privée ;

– conseille les institutions et organes de l’Union européenne en ce qui concerne tous les aspects du traitement de données à caractère personnel, ainsi que les politiques et textes législatifs pertinents ;

– gère les plaintes et mène des enquêtes ;

– collabore avec les autorités nationales des pays de l’Union européenne pour assurer la cohérence dans le domaine de la protection des données ;

– suit l’évolution des technologies susceptibles d’avoir des incidences sur la protection des données.

L’activité courante du CEPD s’appuie sur deux grands services :

– l’entité « contrôle et mise en application », qui vérifie si les institutions et organes de l’Union européenne respectent les règles en matière de protection des données ;

– l’entité « politique et consultations », qui conseille les législateurs européens sur les questions de protection des données dans différents domaines d’action et sur les nouvelles propositions législatives.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/ii/e/2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/ii/e/2/b" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: C -->
<!-- SUBTITLE: LES PRINCIPES S’IMPOSANT AUX OPÉRATEURS TRAITANT DES DONNÉES PERSONNELLES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/ii/b/5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/ii/c/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Le chapitre IV du règlement réunit, au sein des articles 24 à 43, l’ensemble des règles applicables aux responsables de traitement et aux sous-traitants c’est-à-dire les obligations générales qu’ils doivent respecter (section 1), les règles relatives à la sécurité des données à caractère personnel (section 2), celles relatives aux analyses d’impact (section 3) au délégué à la protection des données (section 4) et aux codes de conduite et à la certification (section 5).

L’ensemble de ces règles sont marquées par trois innovations majeures du règlement, saluées par l’ensemble des personnes entendues par la mission : la reconnaissance d’une responsabilité conjointe des responsables de traitement et des sous-traitants, l’extension du champ d’application de la réglementation européenne en matière de protection des données et le passage d’une logique de contrôle préalable des responsables de traitement à une logique de responsabilité.

* [1 — Un périmètre étendu d’application](/rapport-information-4544/ii/c/1)

  * [a — Responsable de traitements et sous-traitant : une responsabilité conjointe](/rapport-information-4544/ii/c/1/a)

  * [b — Une application extra-territoriale du règlement](/rapport-information-4544/ii/c/1/b)

* [2 — D’une logique de contrôle préalable à une logique de responsabilité](/rapport-information-4544/ii/c/2)

  * [a — Une logique de conformité et de responsabilité](/rapport-information-4544/ii/c/2/a)

  * [b — Les nouvelles obligations pesant sur les entreprises](/rapport-information-4544/ii/c/2/b)

* [3 — La sanction du non-respect des obligations](/rapport-information-4544/ii/c/3)

* [4 — Une attention particulière doit être accordée aux TPE et aux PME](/rapport-information-4544/ii/c/4)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/ii/b/5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/ii/c/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

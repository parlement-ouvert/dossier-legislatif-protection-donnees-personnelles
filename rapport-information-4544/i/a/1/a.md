<!-- TITLE: a -->
<!-- SUBTITLE: L’affirmation des grands principes de la protection des données personnelles -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/i/a/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/i/a/1/b" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

L’adoption de la directive 95/46/CE du 24 octobre 1995 a été la première étape de l’élaboration d’un cadre juridique européen relatif à la protection des données personnelles.

La directive ne s’applique pas au traitement des données à caractère personnel transmises ou mises à disposition entre les États membres dans le cadre de la coopération judiciaire en matière pénale et de la coopération policière, qui relève d’une décision-cadre de 2008 (13).

L’article 2 de la directive définit plusieurs concepts centraux pour la protection des données personnelles.

 **Les données à caractère personnel sont définies comme** « _toute information concernant une personne physique identifiée ou identifiable_ […] _; est réputée identifiable une personne qui peut être identifiée, directement ou indirectement, notamment par référence à un numéro d’identification ou à un ou plusieurs éléments spécifiques, propres à son identité physique, physiologique, psychique, économique, culturelle ou sociale_ ».

Ces informations peuvent être nominatives ou non. L’image d’une personne ou d’éléments se rapportant à cette personne est une donnée à caractère personnel si elle permet d’identifier la personne. Les personnes dont l’image apparaît dans des applications telles que _Google Street View_ ont donc le droit d’obtenir le floutage de cette image au titre de la protection de leurs données personnelles.

 **Le traitement de données à caractère personnel** désigne « _toute opération ou ensemble d’opérations effectuées ou non à l’aide de procédés automatisés et appliquées à des données à caractère personnel, telles que la collecte, l’enregistrement, l’organisation, la conservation, l’adaptation ou la modification, l’extraction, la consultation, l’utilisation, la communication par transmission, diffusion ou toute autre forme de mise à disposition, le rapprochement ou l’interconnexion, ainsi que le verrouillage, l’effacement ou la destruction_ » _._

 **Le responsable de traitement** de données personnelles est « _la personne physique ou morale, l’autorité publique, le service ou tout autre organisme qui, seul ou conjointement avec d’autres, détermine les finalités et les moyens du traitement de données à caractère personnel ; lorsque les finalités et les moyens du traitement sont déterminés par des dispositions législatives ou réglementaires nationales ou communautaires, le responsable du traitement ou les critères spécifiques pour le désigner peuvent être fixés par le droit national ou communautaire_ ».

 **Le sous-traitant** est « _la personne physique ou morale, l’autorité publique, le service ou tout autre organisme qui traite des données à caractère personnel pour le compte du responsable du traitement_ ».

Les responsables du traitement de données et leurs sous-traitants doivent respecter différents principes en matière de protection des libertés individuelles et des données à caractère personnel.

En application de l’article 6, les données à caractère personnel doivent être traitées **loyalement et licitement** , collectées pour **des finalités déterminées** , **explicites et légitimes**. Elles doivent être adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont collectées, exactes et, si nécessaire, mises à jour. Elles doivent être conservées sous une forme permettant l’identification des personnes concernées pendant une durée n’excédant pas celle nécessaire à la réalisation des finalités pour lesquelles elles sont collectées.

L’article 7 définit les **conditions de licéité du traitement**. Celui-ci ne peut être effectué que dans les cas suivants :

– la personne concernée a **indubitablement donné son** **consentement** ;

– le traitement est nécessaire à **l’exécution d’un contrat** auquel la personne concernée est partie ;

– le traitement est nécessaire au **respect d’une obligation légale** à laquelle le responsable du traitement est soumis ;

– le traitement est nécessaire à la **sauvegarde de l’intérêt vital** de la personne, ou à **l’exécution des missions d’intérêt public** relevant de l’exercice de l’autorité publique ;

– le traitement est nécessaire à la **réalisation de l’intérêt légitime poursuivi par le responsable du traitement** , à condition que ne prévalent pas l’intérêt ou les droits et libertés fondamentaux de la personne concernée. Le groupe de l’article 29 (G29), qui rassemble les autorités de contrôle des États membres, a adopté en 2014 un avis précisant l’interprétation de cette condition (14).

L’article 8 **interdit en principe le traitement des** **données dites sensibles** , c’est-à-dire celles qui peuvent révéler l’origine raciale ou ethnique, les opinions politiques, les convictions religieuses ou philosophiques, l’appartenance syndicale et les données relatives à la santé et à la vie sexuelle. Il prévoit néanmoins plusieurs exceptions : consentement de la personne, respect des obligations et droits du responsable de traitement en matière de droit du travail, défense des intérêts vitaux de la personne, activités de certains organismes à but non lucratif, données rendues publiques ou nécessaires à l’exercice d’un droit en justice, domaine médical.

En application de l’article 10, le responsable du traitement doit fournir des informations à la personne auprès de laquelle il collecte les données. Ces informations sont relatives à l’identité du responsable du traitement, aux finalités du traitement, aux destinataires des données, au caractère facultatif ou non de la réponse aux questions posées et au droit d’accès aux données.

La personne dont les données sont soumises à un traitement dispose d’un **droit d’accès** à ces données qui doit être possible sans contrainte, à des intervalles raisonnables et sans délais ou frais excessifs ; elle doit pouvoir obtenir leur rectification, leur effacement ou leur verrouillage si leur traitement n’est pas conforme à la directive (article 12). Elle doit également pouvoir s’opposer au traitement, au moins lorsqu’il est fondé sur un intérêt public ou sur l’intérêt légitime du responsable de traitement, pour des raisons prépondérantes et légitimes tenant à sa situation particulière. Elle peut également s’opposer à tout traitement à des fins de prospection (article 14). La directive n’impose pas de forme particulière pour l’exercice de ces droits, ce qui peut poser la question de la facilité avec laquelle la personne peut, ou non, les exercer.

En application de l’article 18, les responsables du traitement de données et leurs sous-traitants doivent respecter une **obligation de notification à l’autorité de contrôle** de la protection des données, la Commission nationale de l’informatique et des libertés (CNIL) en France, préalablement à la mise en  œuvre d’un traitement entièrement ou partiellement automatisé de données à caractère personnel. Ces notifications sont obligatoires et ne peuvent faire l’objet de dérogation que dans des conditions limitativement énumérées. Par ailleurs, ceux des traitements de données susceptibles de présenter des risques particuliers au regard des droits et libertés des personnes physiques sont soumis à un régime plus strict et doivent faire l’objet d’un examen avant leur mise en œuvre.

L’article 28 prévoit que chaque État membre dispose d’une ou plusieurs autorités publiques indépendantes chargées de surveiller le respect, sur son territoire, des dispositions adoptées par les États membres en application de la directive. Ces autorités sont rassemblées au sein d’un groupe de protection des personnes en matière de traitement de données à caractère personnel, en application de l’article 29.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/i/a/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/i/a/1/b" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: SYNTHÈSE DU RAPPORT -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/entete" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/introduction" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**I. LE RÉGIME EN VIGUEUR DE LA PROTECTION DES DONNÉES PERSONNELLES : UN CADRE ANCIEN LAISSANT D’IMPORTANTES MARGES D’INTERPRÉTATION AUX ÉTATS MEMBRES**

La directive 95/46/CE, en vigueur jusqu’au 25 mai 2018, a affirmé **les grands principes de la protection des données personnelles** , notamment en définissant ses concepts centraux et en fixant les conditions de licéité des traitements. Cependant, cette directive avait été élaborée dans le contexte des débuts d’internet, et n’a donc **pas pris en compte les évolutions technologiques majeures** intervenues du fait de son développement.

La Cour de justice de l’Union européenne a significativement contribué à préciser la définition du régime de protection des données personnelles, dans un sens particulièrement favorable aux droits des personnes.

En France, la loi du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés constitue, avant même la réglementation communautaire, un premier régime de protection des données personnelles des personnes physiques.

La marge de manœuvre laissée par la directive 95/46/CE a entraîné, en pratique, des différences dans les législations nationales. Au-delà de la volonté de certains États membres de diminuer les contraintes des responsables de traitement et de limiter la pression pesant sur les autorités nationales de contrôle, c’est également l’imprécision de certaines dispositions de la directive qui a permis de donner lieu à des applications variées.

 **II.– LE RÈGLEMENT DU 27 AVRIL 2016 : DES ÉVOLUTIONS SIGNIFICATIVES ET UN RENFORCEMENT VOLONTARISTE DE L’HARMONISATION DE LA PROTECTION DES DONNÉES PERSONNELLES**

 **A. Le résultat d’une longue négociation**

Le 25 janvier 2012, la Commission européenne a publié une proposition de règlement général sur la protection des données et une proposition de directive sur les données policières et judiciaires, ces deux textes constituant le « paquet données personnelles », qui a fait l’objet de plus de quatre ans de négociations.

De manière générale, la France a approuvé les objectifs d’approfondissement du cadre législatif de la directive 95/46/CE et de renforcement des droits des personnes concernées. Elle s’est opposée à toute disposition du règlement créant un recul par rapport au niveau de protection des droits des personnes assuré par cette directive.

Le texte final est le résultat d’un compromis, mêlant des dispositions harmonisées à de multiples renvois aux droits nationaux (une cinquantaine), ce qui en fait un règlement sui generis, laissant de nombreuses marges de manœuvre aux États membres. Ce résultat fait peser le risque d’une nouvelle fragmentation du régime de la protection des données personnelles dans l’Union européenne.

**B. Le renforcement des droits des personnes physiques**

Le règlement renforce les conditions applicables au **consentement** des personnes au traitement des données les concernant.

De nouveaux droits sont consacrés. Le **droit à l’oubli** recouvre le droit au déréférencement reconnu par la CJUE et un nouveau droit à l’effacement des données à caractère personnel. Le **droit à la portabilité** permet la récupération par les personnes concernées des données personnelles qu’elles ont fournies, dans un format réutilisable, ainsi que leur transmission à un autre responsable de traitement.

Le règlement précise également l’encadrement du **profilage** , c’est-à-dire des traitements de données personnelles visant à évaluer certains aspects personnels.

Les **actions collectives** en matière de protection des données personnelles sont autorisées. Les États membres pourront prévoir dans leur droit national que ces actions peuvent tendre à la réparation du préjudice subi.

 **C. Les principes s’imposant aux opérateurs traitant des données personnelles**

● Le règlement « égalise » les obligations applicables aux sous-traitants et aux responsables de traitements, qui verront leur responsabilité conjointement engagée en cas de manquement à leurs obligations.

Par ailleurs, le champ d’application territorial du règlement est élargi. En pratique, le droit européen s’appliquera chaque fois qu’un résident européen sera directement visé par un traitement de données, y compris par internet.

● Alors que la directive de 1995 reposait en grande partie sur l’existence de formalités préalables (déclaration, autorisations), le règlement européen repose sur une **logique de conformité et de responsabilité** , dite d’« _accountability_ ».

La responsabilisation des entreprises s’incarne par les **principes de la « protection des données dès la conception »** ( _privacy by design_ ) et de **« protection des données par défaut »** ( _privacy by default_ ), qui imposent aux responsables de traitement de mettre en œuvre toutes les techniques nécessaires au respect de la protection des données personnelles, à la fois dès la conception du produit ou du service et par défaut.

Des **analyses de l’impact des traitements sur la protection des données à caractère personnel** devront être conduites par les responsables de traitement lorsque le traitement est susceptible d’engendrer un risque élevé pour les droits et libertés des personnes physiques.

La désignation d’un **délégué à la protection des données** sera obligatoire dans le secteur public et lorsque l’activité principale d’une entreprise concerne le suivi régulier et systématique des personnes à grande échelle ou le traitement à grande échelle de données sensibles ou relatives à des condamnations.

Les responsables de traitement devront notifier les violations de données personnelles à l’autorité de contrôle, ainsi qu’aux personnes concernées en cas de risque élevé pour leurs droits et libertés.

● Le règlement donne aux autorités de contrôle la possibilité de prononcer des amendes administratives qui peuvent atteindre, selon la catégorie de l’infraction, **10 ou 20 millions d’euros, ou, dans le cas d’une entreprise, de 2 % jusqu’à 4 % du chiffre d’affaires annuel mondial** , le montant le plus élevé étant retenu.

● Les rapporteurs estiment qu’une attention particulière devra être accordée aux petites et moyennes entreprises qui pourront rencontrer des difficultés pour respecter les nouvelles obligations posées par le règlement.

**D. L’encadrement des transferts de données à caractère personnel vers des pays tiers**

Le règlement autorise les transferts de données fondés sur une décision de la Commission européenne constatant que le pays tiers assure un niveau de protection adéquat. Ces transferts seront également autorisés lorsque le responsable de traitement aura prévu des garanties telles que des règles d’entreprises contraignantes ou des clauses types de protection, ou dans certains cas limitativement énumérés (menace grave et immédiate pour la sécurité publique d’un État membre ou d’un État tiers, nécessité à des fins de prévention et de détection d’infractions pénales par exemple).

L’entrée en vigueur du règlement pose la question de son articulation avec l’accord « Bouclier vie privée » ( _Privacy Shield_ ) que la Commission a conclu en février 2016 avec les États-Unis. Cet accord pourrait être remis en cause par la CJUE, qui en est saisie, ou par la Commission européenne elle-même, en raison du changement récent de politique des États-Unis en matière de protection des données personnelles.

 **E. Le renforcement des autorités de régulation et la mise en place d’un guichet unique**

Le règlement implique une évolution des missions des autorités nationales de contrôle, qui auront un rôle essentiel d’accompagnement des responsables de traitement. Les amendes administratives qu’elles pourront prononcer seront considérablement renforcées.

Le nouveau mécanisme de décision conjointe de ces autorités, lorsqu’un traitement est transnational, représente également une évolution importante de leur mode de fonctionnement. En cas de désaccord entre ces autorités, un Comité européen de protection des données (CEPD) tranchera.

 **III. L’APPLICATION DU RÈGLEMENT À PARTIR DE MAI 2018 REND NÉCESSAIRE UNE ADAPTATION DU CADRE NATIONAL DE LA PROTECTION DES DONNÉES PERSONNELLES**

L’interruption prochaine des travaux parlementaires imposera d’engager dès le début de la nouvelle législature la révision de la loi « Informatique et libertés » et **il est indispensable qu’un projet de loi puisse être déposé dès juin 2017.**

La **loi pour une République numérique** a pris en compte la problématique de la protection des données personnelles, sans pour autant couvrir l’ensemble du champ du règlement. Certaines de ses dispositions visent à anticiper l’application du règlement (droit à l’oubli numérique des mineurs), tandis que d’autres ont été adoptées à titre transitoire (renforcement des sanctions prononcées par la CNIL) ou traitent de sujets connexes (données des personnes décédées, portabilité des données n’ayant pas un caractère personnel).

**A. De nécessaires adaptations**

La loi devra adapter plusieurs dispositions relatives aux sanctions pouvant être prononcées par la CNIL. Si la loi pour une République numérique a d’ores et déjà prévu qu’à compter du 25 mai 2018, les sanctions entrant dans le champ du règlement seront celles prévues par ledit règlement, d’autres évolutions seront nécessaires concernant les mesures correctives ainsi que les sanctions d’autres manquements.

Par ailleurs, si le règlement prévoit les mécanismes de coopération et de décision des autorités nationales de contrôle, il ne comporte aucune disposition sur les règles procédurales, qui relèvent de la seule compétence des États membres. Les lignes directrices adoptées par le G 29 devraient donner un cadre au législateur.

**B. Des questions restent en suspens**

1\. _L’interprétation de certains concepts_

Plusieurs notions évoquées dans le règlement devront être précisées par le G 29 afin de permettre une application uniforme du règlement parmi les États membres de l’Union européenne. C’est le cas par exemple de la notion de « risque élevé » nécessitant qu’un responsable de traitement consulte l’autorité de contrôle avant de mettre en œuvre un traitement de données.

Sur l’ensemble de ces notions, les rapporteurs considèrent que les avis du G 29 seront essentiels pour éviter toute incertitude juridique potentiellement préjudiciable pour les responsables de traitement et pour les personnes concernées.

2\. _Les règles spécifiques à certains types de traitements_

Plusieurs dispositions du règlement prévoient que les États membres pourront maintenir ou adopter des règles spécifiques pour certains types de traitement.

S’agissant des données de santé, la question de la compatibilité avec le règlement européen du nouveau régime d’accès aux données de santé médico-administratives à caractère personnel défini par la loi de modernisation de notre système de santé du 26 janvier 2016 se posera lors de la discussion du projet de loi adaptant notre législation aux nouvelles normes européennes.

D’autres traitements font l’objet de règles spécifiques définies par la loi du 6 janvier 1978 : données biométriques et génétiques, traitements aux fins d’expression journalistique, artistique, et littéraire, traitements de données relatives aux infractions, aux condamnations et aux mesures de sûreté, traitements portant sur le numéro d’identification national, traitements à des fins archivistiques, de recherche scientifique ou historique ou à des fins statistiques. Ces règles spécifiques devraient pouvoir être maintenues dans le cadre des marges ouvertes par le règlement.

3\. _Les actions de groupe_

L’action de groupe, introduite par la loi du 18 novembre 2016 de modernisation de la justice du XXIe siècle ( ), est ouverte lorsque plusieurs personnes physiques subissent un dommage ayant pour cause commune un manquement aux dispositions de la loi du 6 janvier 1978 et permet d’obtenir la cessation du manquement.

Le règlement prévoit la simple possibilité pour les États membres d’adopter des dispositions nationales autorisant des actions collectives avec mandat tendant à la réparation du préjudice subi. **La question d’un éventuel élargissement du champ de l’action de groupe devra donc être tranchée par le législateur.**

4\. _Le droit à la portabilité_

La loi pour une République numérique prévoit la mise en œuvre, à compter du 25 mai 2018, d’un droit à la portabilité de l’ensemble de ses données pour le consommateur. S’agissant des données personnelles, elle renvoie au régime défini par l’article 20 du règlement. Les autres données relèvent d’un régime différent, ne s’imposant qu’aux opérateurs de communications électroniques.

Les rapporteurs estiment que la **mise en** **œ uvre de ces deux régimes risque de poser des difficultés d’interprétation** et souhaitent que ceux-ci puissent être **clarifiés et mieux articulés dans le cadre de la future loi.**

5\. _Les dispositions spécifiques concernant les enfants_

La question de l’articulation des dispositions nationales et du règlement se pose en raison **des âges différents fixés par la loi pour une République numérique (18 ans) et par le règlement (13 à 16 ans) pour l’exercice du droit à l’effacement** des données personnelles.

Cependant, selon une interprétation étudiée par le ministère de la justice, une disposition de l’article 17 du règlement, rendant obligatoire l’effacement des données pour respecter une obligation légale définie par le droit national, pourrait permettre de fixer une condition supplémentaire par rapport au règlement, telle que le prévoit la loi pour une République numérique s’agissant des mineurs âgés de 16 à 18 ans.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/entete" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/introduction" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

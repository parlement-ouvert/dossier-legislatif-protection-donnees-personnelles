<!-- TITLE: c -->
<!-- SUBTITLE: Les traitements aux fins d’expression journalistique, artistique, universitaire et littéraire -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/iii/b/2/b" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/iii/b/2/d" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

L’article 85 du règlement dispose que « _les États membres concilient, par la loi, le droit à la protection des données à caractère personnel au titre du présent règlement et le droit à la liberté d’expression et d’information, y compris le traitement à des fins journalistiques et à des fins d’expression universitaire, artistique ou littéraire_ » et « _prévoient des exemptions et dérogations_ » aux chapitres II à IX « _si celles-ci sont nécessaires pour concilier le droit à la protection des données à caractère personnel et la liberté d’expression et d’information_ ».

La directive de 1995 prévoyait déjà des dérogations s’agissant de ce type de traitements, à l’exception de la finalité d’expression universitaire, ajoutée par le règlement.

En application de l’article 67 de la loi du 6 janvier 1978, les traitements mis en œuvre aux seules fins « _d’expression littéraire et artistique_ » et « _d’exercice, à titre professionnel, de l’activité de journaliste, dans le respect des règles déontologiques de cette profession_ » sont dispensés du respect des obligations suivantes :

– la limitation de la durée de conservation des données ;

– l’interdiction de traiter des données sensibles ou des données relatives aux infractions, condamnations et mesures de sûreté ;

– les formalités de déclaration, à condition que le responsable du traitement désigne « _un correspondant à la protection des données appartenant à un organisme de la presse écrite ou audiovisuelle, chargé de tenir un registre des traitements mis en_ _œ uvre par ce responsable et d’assurer, d’une manière indépendante, l’application des dispositions de la présente loi_ » ;

– les régimes d’autorisations pour les traitements statistiques, ceux concernant des données sensibles ou relatifs aux infractions, condamnations et mesures de sûreté ;

– l’information préalable des personnes concernées par le traitement ;

– le droit d’accès et le droit de rectification ;

– le transfert de données vers des États tiers.

Le dernier alinéa de l’article 67 précise que ces dérogations ne font pas obstacle à l’application des dispositions législatives relatives au droit de réponse et à la protection de la vie privée et de la réputation des personnes.

Dans leurs réponses écrites au questionnaire des rapporteurs, la CNIL comme la DACS ont estimé que le régime actuel avait permis d’atteindre un équilibre satisfaisant entre liberté d’expression et protection des données personnelles.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/iii/b/2/b" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/iii/b/2/d" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: a -->
<!-- SUBTITLE: Les traitements des données de santé -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/iii/b/2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/iii/b/2/b" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

L’article 4 du règlement définit les données de santé comme « _les données à caractère personnel relatives à la santé physique ou mentale d’une personne physique, y compris la prestation de services de soins de santé, qui révèlent des informations sur l’état de santé de cette personne_ ».

L’article 9, paragraphe 1, dispose que les données de santé sont des données sensibles, dont le traitement est par principe interdit. Cependant, le paragraphe 2 prévoit que le traitement de ces données est autorisé dans différents cas, notamment :

– s’il est « _nécessaire pour des motifs d’intérêt public important, sur la base du droit de l’Union ou du droit d’un État membre qui doit être proportionné à l’objectif poursuivi, respecter l’essence du droit à la protection des données et prévoir des mesures appropriées et spécifiques pour la sauvegarde des droits fondamentaux et des intérêts de la personne concernée_ » (g) ;

– s’il est _« nécessaire aux fins de la médecine préventive ou de la médecine du travail, de l’appréciation de la capacité de travail du travailleur, de diagnostics médicaux, de la prise en charge sanitaire ou sociale, ou de la gestion des systèmes et des services de soins de santé ou de protection sociale sur la base du droit de l’Union, du droit d’un État membre ou en vertu d’un contrat conclu avec un professionnel de la santé_ [...] » (h) ;

– s’il est « _nécessaire pour des motifs d’intérêt public dans le domaine de la santé publique, tels que la protection contre les menaces transfrontalières graves pesant sur la santé, ou aux fins de garantir des normes élevées de qualité et de sécurité des soins de santé et des médicaments ou des dispositifs médicaux, sur la base du droit de l’Union ou du droit de l’État membre qui prévoit des mesures appropriées et spécifiques pour la sauvegarde des droits et libertés de la personne concernée, notamment le secret professionnel_ » (i) ;

– s’il est « _nécessaire à des fins archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique ou à des fins statistiques conformément à l’article 89, paragraphe 1, sur la base du droit de l’Union ou du droit d’un État membre qui doit être proportionné à l’objectif poursuivi, respecter l’essence du droit à la protection des données et prévoir des mesures appropriées et spécifiques pour la sauvegarde des droits fondamentaux et des intérêts de la personne concernée_ » (j).

Le paragraphe 4 dispose que « **_les États membres peuvent maintenir ou introduire des conditions supplémentaires, y compris des limitations, en ce qui concerne le traitement des données génétiques, des données biométriques ou des données concernant la santé_** ».

En droit national, l’article 8 de la loi du 6 janvier 1978 autorise :

– les traitements « _nécessaires aux fins de la médecine préventive, des diagnostics médicaux, de l’administration de soins ou de traitements, ou de la gestion de services de santé et mis en_ _œ uvre par un membre d’une profession de santé, ou par une autre personne à laquelle s’impose en raison de ses fonctions l’obligation de secret professionnel prévue par [l’article 226-13](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417944&dateTexte=&categorieLien=cid) du code pénal_ » ;

– « _les traitements nécessaires à la recherche, aux études et évaluations dans le domaine de la santé_ ».

L’article 193 de la loi n° 2016-41 du 26 janvier 2016 de modernisation de notre système de santé a défini **un nouveau cadre d’accès aux données de santé médico-administratives à caractère personnel** , par l’introduction de dispositions symétriques dans le code de la santé publique et dans la loi du 6 janvier 1978.

L’article L. 1461-1 du code de la santé publique a créé un système national des données de santé (SNDS) rassemblant les différentes bases de données existantes en matière sanitaire et médico-sociale.

L’article L. 1460-2 dispose que les données du SNDS mises à la disposition du public « _sont traitées pour prendre la forme de statistiques agrégées ou de données individuelles constituées de telle sorte que l’identification, directe ou indirecte, des personnes concernées y est impossible_ ».

Le chapitre IX de la loi du 6 janvier 1978 (articles 53 à 61) définit le régime applicable aux traitements de données personnelles à des fins de recherche, d’étude ou d’évaluation dans le domaine de la santé.

L’article 54 prévoit que la CNIL autorise les traitements ayant une finalité d’intérêt public de recherche, d’étude ou d’évaluation dans le domaine de la santé, après avis du comité d’experts compétent (107). Elle peut adopter des méthodologies de référence afin de simplifier la procédure pour les traitements les plus usuels.

La question de la compatibilité du nouveau régime défini par la loi du 26 janvier 2016 avec le règlement européen, dont l’adoption est postérieure, se posera lors de la discussion du projet de loi adaptant notre législation aux nouvelles normes européennes. Vos rapporteurs soulignent à cet égard que cette réflexion pourrait s’appuyer sur les travaux actuellement menés par la mission d’évaluation et de contrôle des lois de financement de la sécurité sociale (MECSS) sur l’accès aux données médicales personnelles détenues par l’assurance maladie.

M. Édouard Geffray, secrétaire général de la CNIL, auditionné par la MECSS, a indiqué que, dans le cadre du futur projet de loi, « _le Parlement devrait [...] se prononcer sur le point de savoir si nous conservons le dispositif issu de la loi de modernisation de notre système de santé ou si nous remettons l’ouvrage sur le métier. Selon notre première analyse, le régime actuel pourrait demeurer, compte tenu du périmètre des exceptions prévues par le règlement européen – cette analyse doit cependant être affinée par la Direction des affaires civiles et du Sceau, qui pilote le projet au niveau interministériel. Je crains donc que le chantier ne soit pas totalement achevé, alors même que les décrets d’application de la loi de modernisation du système de santé ne sont pas encore publiés_. »  (108) .

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/iii/b/2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/iii/b/2/b" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

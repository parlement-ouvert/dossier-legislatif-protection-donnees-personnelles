<!-- TITLE: B -->
<!-- SUBTITLE: DES QUESTIONS RESTENT EN SUSPENS -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/iii/a/2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/iii/b/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [1 — Une nécessaire clarification de certaines notions](/rapport-information-4544/iii/b/1)

* [2 — Les règles spécifiques à certains types de traitements](/rapport-information-4544/iii/b/2)

  * [a — Les traitements des données de santé](/rapport-information-4544/iii/b/2/a)

  * [b — Les traitements des données biométriques et génétiques](/rapport-information-4544/iii/b/2/b)

  * [c — Les traitements aux fins d’expression journalistique, artistique, universitaire et littéraire](/rapport-information-4544/iii/b/2/c)

  * [d — Les traitements de données relatives aux infractions, aux condamnations et aux mesures de sûreté](/rapport-information-4544/iii/b/2/d)

  * [e — Les traitements portant sur le numéro d’identification national](/rapport-information-4544/iii/b/2/e)

  * [f — Les traitements des données personnelles à des fins archivistiques, de recherche scientifique ou historique ou à des fins statistiques](/rapport-information-4544/iii/b/2/f)

* [3 — Les actions de groupe](/rapport-information-4544/iii/b/3)

* [4 — Le droit à la portabilité](/rapport-information-4544/iii/b/4)

* [5 — Les dispositions spécifiques concernant les enfants](/rapport-information-4544/iii/b/5)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/iii/a/2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/iii/b/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: PERSONNES ENTENDUES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/examen-en-commission" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/deplacement-a-bruxelles" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

• Mme Aurélia Schaff, conseillère, cheffe du secteur Espace judiciaire européen au Secrétariat général des Affaires européennes (SGAE), Mme Ève Jullien, adjointe aux conseillères judiciaire et juridique, chargée de la protection des données personnelles, et Mme Lorraine Simonnet, adjointe à la conseillère juridique chargée du suivi de la transposition des directives

• M. Edouard Geffray, secrétaire général de la Commission nationale de l’informatique et des libertés (CNIL) et Mme Tiphaine Inglebert, conseillère pour les questions institutionnelles et parlementaires

• M. Henri Verdier, directeur de la direction interministérielle des systèmes d’information et de communication (DINSIC) et M. Périca Sucevic, conseiller juridique

• M. Daniel le Métayer, directeur de recherche à l’Institut national de recherche en informatique et en automatique (INRIA)

• M. Philippe Aigrain, président et Mme Léa Caillère Falgueyrac, analyste juridique et politique de l’association La Quadrature du Net

• M. Jean-Christophe Gracia, chef de service, adjoint à la directrice des affaires civiles et du Sceau du ministère de la Justice, MM. Romain Felsenheld, chef du bureau du droit constitutionnel et du droit public général, Corentin Hellendorff, rédacteur au bureau du droit constitutionnel et du droit public général et Emmanuel Laforêt, adjoint au chef de bureau

• M. Winston Maxwell, avocat associé au cabinet Hogan Lovells

• Mme Célia Zolynski, professeure de droit à l’université de Versailles-Saint-Quentin-en-Yvelines, et M. Romain Delassus, rapporteur général, du Conseil national du numérique (CNNum)

• MM. Éric Barbry, administrateur et président de la commission juridique, et Nicolas Herbreteau, administrateur de l’Association de l’économie numérique (ACSEL) et Mme Céline Avignon, avocate

• Mme Marie-Blanche Niel-Gilly, directrice des données personnelles et correspondante Informatique et Libertés, M. Maxime Duclaux, responsable des relations institutionnelles et Mme Clara Hanot, chargée des affaires publiques de la société SoLocal Group

• M. Alexandre Tisserant, directeur adjoint du cabinet de Mme Axelle Lemaire, secrétaire d’État auprès du ministre de l’Économie et des Finances, chargée du numérique et de l’innovation, Mme Chantal Rubin, adjointe à la direction générale des entreprises et Mme Julie Wable, membre du cabinet

• M. Massimo Bucalossi, vice-président de la commission « Intranet et nouvelles technologies » du Conseil national des Barreaux, et Mme Laurence Dupont, juriste, référente CNIL

• M. Éric Le Quellenec, avocat, vice-président de l’Union des jeunes avocats de Paris, et Mme Marie-Hélène Fabiani, avocate, responsable de la commission « Nouvelles technologies »

• M. Dominique Guibert, président de l’Association européenne des droits de l’homme (AEDH) et Mme Maryse Artiguelong, membre du bureau exécutif et responsable du groupe de travail protection des données

• M. Marc Mossé, directeur des affaires publiques et juridiques Europe de Microsoft, Mme Marie-Charlotte Roques-Bonnet, directrice de la politique de confidentialité et M. Jean-Renaud Roy, directeur des affaires publiques France

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/examen-en-commission" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/deplacement-a-bruxelles" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

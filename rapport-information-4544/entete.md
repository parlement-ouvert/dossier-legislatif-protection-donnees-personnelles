<!-- TITLE: Entête -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/synthese-du-rapport" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

![](i4544-1.gif)
N ° 4544
______
ASSEMBLÉE NATIONALE
CONSTITUTION DU 4 OCTOBRE 1958
QUATORZIÈME LÉGISLATURE

Enregistré à la Présidence de l’Assemblée nationale le 22 février 2017.

**RAPPORT D’INFORMATION**
déposé
 _en application de l’article 145 du Règlement_
PAR LA COMMISSION DES LOIS CONSTITUTIONNELLES, DE LA LÉGISLATION ET DE L’ADMINISTRATION GÉNÉRALE DE LA RÉPUBLIQUE,
en conclusion des travaux d’une mission d’information (1)
 _sur les incidences des_ **nouvelles normes européennes** _en matière de_ **protection** _des_ **données personnelles** _sur la_ **législation française** ,
ET PRÉSENTÉ
PAR Mme Anne-Yvonne LE DAIN et M. Philippe GOSSELIN
Députés.
——

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/synthese-du-rapport" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Notes -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/annexe-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <span> </span> </div>

1 _() La composition de cette mission figure au verso de la présente page._

 _2 () Directive 95/46/CE du 24 octobre 1995 relative à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation des données._

 _3 () Communication de la Commission européenne, « _Protection de la vie privée dans un monde en réseau _», 25 mai 2012._

 _4 () Règlement n° 2016/679 du 27 avril 2016 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE._

5 _() COM (2012) 10 final du 25 janvier 2012._

 _6 () Audition de Mme Isabelle Falque-Pierrotin, Assemblée Nationale, Commission des lois constitutionnelles, de la législation et de l’administration générale de la République, 12 octobre 2016._

7 _() Loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés._

8 _() Loi ****_ n° 2016-1321 du 7 octobre 2016 pour une République numérique _._

9 _() Compte tenu de la fin prochaine des travaux de l’actuelle législature._

10 _() La principale différence entre les traditions juridiques de «_ common law _» et de droit civil repose sur la source principale du droit. Alors que les systèmes basés sur la «_ common law _» considèrent les décisions judiciaires comme la source la plus importante de la loi, les systèmes basés sur le droit civil mettent particulièrement l’accent sur le droit codifié._

11 _() CJUE, 6 octobre 2015, C-362/14,_ Schrems _._

12 _() Comme en témoigne le décret adopté le 25 janvier 2017_

13 _() Décision-cadre 2008/977/JAI du Conseil du 27 novembre 2008 relative à la protection des données à caractère personnel traitées dans le cadre de la coopération policière et judiciaire en matière pénale. Ce texte relève de l’ancien « troisième pilier » de l’Union européenne relatif à la justice et aux affaires intérieures._

14 _() Groupe de l’article 29, avis 06/2014 sur la notion d’intérêt légitime poursuivi par le responsable du traitement des données au sens de l’article 7 de la directive 95/46/CE, 9 avril 2014, 844/14/FR ,WP 21._

15 _() Directive 2002/58/CE du Parlement européen et du Conseil du 12 juillet 2002 concernant le traitement des données à caractère personnel et la protection de la vie privée dans le secteur des communications électroniques (directive vie privée et communications électroniques) ; cette directive s’est substituée à la directive 97/66/CE du 15 décembre 1997 concernant le traitement des données à caractère personnel et la protection de la vie privée dans le secteur des télécommunications._

16 _() Les « cookies » sont des fichiers déposés dans le terminal d’un utilisateur, par exemple lors de la consultation d’un site internet, afin, notamment, d’enregistrer des informations sur cet utilisateur._

17 _() CJUE, 13 mai 2014, C-131/12,_ Google c/Espagne _._

18 _() Elle a aussi considéré que l’exploitant d’un moteur de recherche sur internet était «_ responsable d’un traitement de données personnelles _» (), au sens de la directive 95/46/CE, lorsqu’il indexe et met à disposition des données personnelles figurant sur des pages web publiées par des tiers._

19 _()_ CJUE, 8 avril 2014, affaires jointes n° C-293/12 et C-594/12 _._

 _20 () <https://www.cnil.fr/fr/la-directive-200624ce-contraire-aux-articles-7-et-8-de-la-charte-des-droits-fondamentaux-de-lunion>._

21 _() CJUE, 6 octobre 2015, C-362/14,_ Schrems _._

22 _() Ces insuffisances ont notamment été mises en lumières par les révélations de M. Edward Snowden sur la surveillance pratiquée par l’Agence nationale de la sécurité (NSA), voire par l’espionnage du téléphone portable de Mme Merkel par cette agence._

23 _() Cf._ infra.

24 _() Loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés._

25 _() Loi n° 2004-801 du 6 août 2004 relative à la protection des personnes physiques à l’égard des traitements de données à caractère personnel._

 _26 () La directive 95/46/CE a été transposée tardivement par la loi du 6 août 2004 relative à la protection des personnes physiques concernant les traitements de données à caractère personnel._

 _27 () Patricia Blanc-Gonnet Jonason, « _Vers une meilleure adaptation du droit de la protection des données personnelles à la réalité informationnelle _», 2008, AJDA._

28 _() Communication de la Commission européenne «_ Une approche globale de la protection des données à caractère personnel dans l’Union européenne _», COM (2010) 609 final du 4 novembre 2010._

29 _() La directive 95/46 se fondait sur l’article 100A du traité instituant la Communauté européenne (TCE), relatif au marché intérieur._

30 _() COM (2012) 11 final du 25 janvier 2012._

31 _() COM (2012) 10 final du 25 janvier 2012._

32 _() Directive (UE) 2016/680 du Parlement européen et du Conseil du 27 avril 2016 relative à la protection des personnes physiques à l’égard du traitement des données à caractère personnel par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d’enquêtes et de poursuites en la matière ou d’exécution de sanctions pénales, et à la libre circulation de ces données, et abrogeant la décision-cadre 2008/977/JAI du Conseil._

33 _() Cf._ infra _._

34 _() G29, avis 05/2014 sur les Techniques d’anonymisation adopté le 10 avril 2014, 0829/14/FR-WP216._

35 _() Cf. annexe 2, résolution européenne sur la proposition de règlement relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données, texte adopté n°888 du 23 mars 2012 ; rapport de la commission des affaires européennes n° 4227 du 7 février 2012 présenté par M. Philippe Gosselin._

36 _() Cf._ infra _._

37 _() Communication de M. Guy Geoffroy et de Mme Marietta Karamanli, commission des Lois, 17 octobre 2012, compte-rendu n° 4 ; communication de M. Guy Geoffroy, commission des Lois, 11 juin 2014, compte-rendu n° 64 ; communication de M. Guy Geoffroy, commission des affaires européennes, 15 février 2012, compte-rendu n° 241 ; communication de Mme Marietta Karamanli, commission des affaires européennes, 14 mai 2014._

38 _() Annexe 1._

39 _() Le_ forum shopping _est un terme informel anglais de[droit international privé](https://fr.wikipedia.org/wiki/Droit_international_priv%C3%A9), qui désigne la possibilité qu’offre à un demandeur la diversité des règles de compétences internationales de saisir les tribunaux des pays appelés à rendre la décision la plus favorable à ses intérêts. _

40 _() Cf._ supra _._

41 _() Rapport d’information déposé par la Commission de réflexion et de propositions sur le droit et les libertés à l’âge du numérique, co-présidée par M. Christian Paul et Mme Christiane Féral-Schuhl, n° 3119, 9 octobre 2015._

42 _() Considérants 42 et 43 ; article 7, paragraphe 4._

43 _() Article 6, paragraphe 1, de la directive 95/46 et article 6 (5°) de la loi du 6 janvier 1978._

44 _() G29, Guidelines on the right to data portability, 13 décembre 2016; 16/EN, WP242._

45 _() Article 15 de la directive 95/46._

46 _() Les responsables de traitement sont définis comme les organismes qui déterminent les finalités et les modalités de traitement de données personnelles._

 _47 () Le paragraphe 8 de l’article 4 du règlement reprend la définition du sous-traitant de la directive 95/46/CE, le sous-traitant étant « _la personne physique ou morale, l’autorité publique, le service ou un autre organisme qui traite des données à caractère personnel pour le compte du responsable de traitements _»._

48 _() Dans le cas d’une responsabilité solidaire, un créancier peut se retourner contre n’importe lequel des associés d’une entreprise pour obtenir le règlement de l’ensemble des dettes, À l’inverse, dans une responsabilité conjointe, le créancier ne peut demander à un associé que le remboursement d’une dette à la hauteur de sa participation dans la société._

49 _()_ Le « _mailing_ », qui est initialement le terme anglais pour désigner un publipostage courrier, est une campagne de marketing direct qui consiste à envoyer une proposition commerciale plus ou moins personnalisée par voie postale à un ensemble d’individus soigneusement ciblés.

50 _() Selon la Cour de justice, la présence d’un représentant, d’une adresse et d’un compte bancaire au sein d’un État membre suffit à caractériser cette activité_ (CJUE, 1er octobre 2015, aff. C-230/14, Weltimmo : JurisData n° 2015-025844) _._

51 _() Cf._ infra. __

52 _() Considérant 23 du règlement._

53 _() En revanche, la simple accessibilité du site internet du responsable du traitement, d’un sous-traitant ou d’un intermédiaire dans l’Union européenne, d’une adresse électronique ou d’autres coordonnées, ou l’utilisation d’une langue généralement utilisée dans le pays tiers où le responsable du traitement ne suffisent pas pour établir l’intention d’offrir des biens ou des services à des personnes qui se trouvent dans l’Union européenne._

54 _() Considérant 24 du règlement._

 _55 () Ainsi que l’a précisé le G29 dans un avis du 16 septembre 2014, la directive 95/46 s’applique déjà à ce type de traitements (avis 8/2014 sur les récentes évolutions relatives à l’internet des objets, 1471/14/FR,WP 223)._

 _56 () A l’exception des autorités publiques de pays tiers, et des responsables de traitement mettant en œuvre des traitements de données occasionnels et qui ne présentent pas de risques. Ils restent toutefois soumis aux autres dispositions du règlement._

 _57 () « _Le règlement sur la protection des données : les 10 commandements à connaître pour passer de la théorie à la pratique _», par M. Emmanuel Jouffin, responsable juridique de banque, M. Xavier Lemarteleur, responsable juridique Technologies de l’information et Mme Marie-Noëlle Gibon, correspondante informatique et liberté du groupe La Poste et La Banque Postale, Revue de Droit bancaire et financier (n° 4, Juillet 2016, étude 18)._

58 _() Quant aux traitements soumis actuellement à autorisation, le régime d’autorisation pourra être maintenu par le droit national (par exemple en matière de santé) ou sera remplacé par une nouvelle procédure centrée sur l’étude d’impact sur la vie privée._

59 _() Selon la CNIL : «_ le chiffrement d’un message permet de garantir que seuls l’émetteur et le(s) destinataire(s) légitime(s) d’un message en connaissent le contenu. (…) Une fois chiffré, faute d’avoir la clé spécifique, un message est inaccessible et illisible, que ce soit par les humains ou les machines _. »_ _<https://www.cnil.fr/fr/comprendre-les-grands-principes-de-la-cryptologie-et-du-chiffrement>_

60 _() Cf._ supra.

 _61 () L’article 9 du règlement dispose que « _Le traitement des données à caractère personnel qui révèle l’origine raciale ou ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l’appartenance syndicale, ainsi que le traitement des données génétiques, des données biométriques aux fins d’identifier une personne physique de manière unique, des données concernant la santé ou des données concernant la vie sexuelle ou l’orientation sexuelle d’une personne physique sont interdits. _»_

 _62 () L’article 10 du règlement prévoit que « _Le traitement des données à caractère personnel relatives aux condamnations pénales et aux infractions ou aux mesures de sûreté connexes fondé sur l’article 6, paragraphe 1, ne peut être effectué que sous le contrôle de l’autorité publique, ou si le traitement est autorisé par le droit de l’Union ou par le droit d’un ‘État membre qui prévoit des garanties appropriées pour les droits et libertés des personnes concernées. Tout registre complet des condamnations pénales ne peut être tenu que sous le contrôle de l’autorité publique _. »_

 _63 () Loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés._

64 _() ****_ Décret n° 2005-1309 du 20 octobre 2005 pris pour l’application de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés. _ ****_

65 _() Cf._ supra _._

66 _() Données qui révèlent l’origine raciale ou ethnique, les opinions politiques, philosophiques ou religieuses, l’appartenance syndicale, les données concernant la santé ou l’orientation sexuelle, mais aussi, fait nouveau, les données génétiques ou biométriques._

67 _()_ _<https://www.cnil.fr/fr/reglement-europeen-sur-la-protection-des-donnees-ce-qui-change-pour-les-professionnels>_

68 _() Cf._ supra _._

69 _()_ _<https://www.cnil.fr/sites/default/files/atoms/files/dpoen.pdf>_

70 _() Lorsque la notification à l’autorité de contrôle n’a pas lieu dans les 72 heures, elle est accompagnée des motifs du retard. _

71 _() Paragraphe 4 de l’article 83 du règlement._

 _72 () Le Royaume-Uni devra appliquer le règlement à partir du 25 mai 2018 mais ne sera plus lié par celui-ci après sa sortie de l’Union européenne. Pour permettre la circulation des données personnelles, il devra solliciter une décision d’adéquation auprès de la Commission._

73 _() Cf._ supra. __

74 _() CJUE, 6 octobre 2015, C-362/14,_ Schrems _._

75 _() Décision d’exécution 2016/1250 (UE)._

 _76 () Executive Order 13768, « _Enhancing Public Safety in the Interior of the United States _», 25 janvier 2017._

 _77 () H.R.1428 - Judicial Redress Act of 2015-11-4th \- Congress (2015-2016)._

78 _() La loi sur la protection de la vie privée encadre l’utilisation des données personnelles par les agences fédérales américaines – y compris les agences de renseignement et services de sécurité comme la NSA et le FBI – en prévoyant notamment pour les citoyens américains et les résidents permanents légaux, des droits d’accès, de rectification et de recours en cas d’utilisation illicite de leurs données. Sont prévues des exceptions en matière de sécurité nationale notamment._

 _79 () Presidential policy directive / PPD-28, 3, 17 janvier 2014. _

_80 () « _Trump’s anti-privacy order stirs EU angst _», 27 janvier 2017,_[ _https://euobserver.com/justice/136699_](https://euobserver.com/justice/136699) __

_81 () Audition de Mme Isabelle Falque-Pierrotin, Assemblée Nationale, Commission des lois constitutionnelles, de la législation et de l’administration générale de la République, 12 octobre 2016._

82 _() 100 000 déclarations par an à la CNIL, mobilisant moins de deux équivalents temps plein travaillés._

83 _() Audition du 12 octobre 2016._

 _84 () En 2014, la CNIL avait pu prononcer une sanction pécuniaire maximale de 150 000 euros à l’encontre de Google cf. <https://www.cnil.fr/fr/la-formation-restreinte-de-la-cnil-prononce-une-sanction-pecuniaire-de-150-000-eu-lencontre-de-la>._

 _85 () Cette architecture a toutefois été fortement contestée par l’Irlande, la Belgique, le Royaume-Uni, la République tchèque, la Finlande et l’Espagne. _

86 _() Les agendas et les minutes de ces réunions sont publiés sur le site internet de la Commission européenne,_ _[http://ec.europa.eu/transparency/re gexpert/index.cfm?do=groupDetail.groupDetailPDF&groupID=3461](http://ec.europa.eu/transparency/regexpert/index.cfm?do=groupDetail.groupDetailPDF&groupID=3461)_ _._

87 _() Proposition de règlement du Parlement européen et du Conseil concernant le respect de la vie privée et la protection des données personnelles dans le secteur des communications électroniques et abrogeant la directive 2002/58/CE (règlement vie privée et communications électroniques), COM (2017) 10 final, 10 janvier 2017._

88 _() Commission européenne, communiqué de presse du 10 janvier 2017, IP-17-16_FR._

89 _() Proposition de règlement du Parlement européen et du Conseil_ _relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel par les institutions, organes bureaux et agences de l’Union européenne et à la libre circulation de ces données et abrogeant le règlement CE 45/2001 et la décision 1247/2002/CE, COM (2017) 8 final, 10 janvier 2017._

90 _() Communication de la Commission européenne au Parlement européen et au Conseil, « Échanger et protéger les données personnelles dans un monde globalisé », COM (2017) 7 final, 10 mai 2017._

91 _() Statement on the 2016 action plan for the implementation of the General Data Protection Regulation (GDPR), 2 février 2016, 442/16/EN-WP 236._

92 _() Cf._ supra _._

93 _() Audition du 12 octobre 2016._

94 _()_ Conseil d’État, Étude annuelle, « _Le numérique et les droits fondamentaux_ », 2014. __

95 _() Rapport d’information déposé par la Commission de réflexion et de propositions sur le droit et les libertés à l’âge du numérique,_ op. cit _._

96 _() Cf._ supra _._

97 _() Cf._ supra _._

98 _() Le considérant 27 du règlement précise que celui-ci ne s’applique pas aux données à caractère personnel des personnes décédées et que les États membres peuvent prévoir les règles relatives à ces données._

99 _() CADA, Conseil n° 20155079 du 19 novembre 2015 sur le projet de loi pour une République numérique._

100 _() Articles 25 et 26 (articles 13 et 15 de la loi du 6 janvier 1978)._

101 _() Article 61 (article L. 135 du code des postes et des communications électroniques)._

102 _() Article 66 (article 49 bis de la loi du 6 janvier 1978)._

103 _() INRIA, communiqué de presse du 1 er décembre 2016, « Création d’une plateforme scientifique pour le développement de la transparence et de la responsabilité des algorithmes et des données « TransAlgo »._

104 _() Conseil général de l’économie, de l’industrie, de l’énergie et des technologies,_ Modalités de régulation des algorithmes de traitement des contenus _, établi par MM. Ilarion Pavel et Jacques Serris, 13 mai 2016._

105 _() Article L 34-5 du code des postes et communications électroniques._

 _106 () Audition de Mme Isabelle Falque-Pierrotin, Assemblée Nationale, Commission des lois constitutionnelles, de la législation et de l’administration générale de la République, 12 octobre 2016._

107 _() Si les recherches portent sur la personne humaine, le comité compétent de protection des personnes prévu à l’article L. 1123-6 du code de la santé publique ; dans les autres cas, le comité d’expertise pour les recherches, les études et les évaluations dans le domaine de la santé (article L. 1451-1 du même code)._

108 _() Audition du 11 janvier 2017, compte-rendu n°8._

109 _() Conseil d’État, 28 juillet 2014,_ Fathy X _, n° 262851._

110 _() Conseil d’État, 11 mai 2015,_ Société Renault Trucks _, n° 375669._

111 _() Conseil constitutionnel, décision n° 2004-499 DC 29 juillet 2004._

112 _() Selon la nature des données et les finalités du traitement, par décret en Conseil d’État, par arrêté ou délibération de l’organe délibérant compétent, après avis motivé et publié de la CNIL._

113 _() L_ oi n° 2016-1547 du 18 novembre 2016 de modernisation de la justice du XXIe siècle _._

114 _() Cf._ supra _._

115 _() Rapport de M. Luc Belot, au nom de la commission des Lois, sur le projet de loi (n° 3318) pour une République numérique, n° 3399, 15 janvier 2016._

116 _() Cf._ supra _._

117 _() Liste présentée par ordre chronologique._

118 _() XIIIe législature. Session 2011-2012. TA n° 888_

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/annexe-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <span> </span> </div>

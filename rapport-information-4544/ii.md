<!-- TITLE: II -->
<!-- SUBTITLE: LE RÈGLEMENT DU 27 AVRIL 2016 : DES ÉVOLUTIONS SIGNIFICATIVES ET UN RENFORCEMENT VOLONTARISTE DE L’HARMONISATION DE LA PROTECTION DES DONNÉES PERSONNELLES DANS L’UNION EUROPÉENNE -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/i/c" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/ii/a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [A — LE RÉSULTAT D’UNE LONGUE NÉGOCIATION](/rapport-information-4544/ii/a)

  * [1 — Le paquet « données personnelles »](/rapport-information-4544/ii/a/1)

  * [2 — La position de la France](/rapport-information-4544/ii/a/2)

  * [3 — Un règlement sui generis, laissant d’importantes marges de manœuvre aux États membres](/rapport-information-4544/ii/a/3)

* [B — LE RENFORCEMENT DES DROITS DES PERSONNES PHYSIQUES](/rapport-information-4544/ii/b)

  * [1 — L’évolution de la définition des données protégées](/rapport-information-4544/ii/b/1)

  * [2 — Le renforcement du consentement](/rapport-information-4544/ii/b/2)

  * [3 — L’extension du droit à l’information](/rapport-information-4544/ii/b/3)

  * [4 — L’affirmation de nouveaux droits](/rapport-information-4544/ii/b/4)

    * [a — Le droit à l’effacement (« droit à l’oubli »)](/rapport-information-4544/ii/b/4/a)

    * [b — Le droit à la portabilité des données](/rapport-information-4544/ii/b/4/b)

    * [c — Le recours aux actions collectives](/rapport-information-4544/ii/b/4/c)

  * [5 — La question du profilage](/rapport-information-4544/ii/b/5)

* [C — LES PRINCIPES S’IMPOSANT AUX OPÉRATEURS TRAITANT DES DONNÉES PERSONNELLES](/rapport-information-4544/ii/c)

  * [1 — Un périmètre étendu d’application](/rapport-information-4544/ii/c/1)

    * [a — Responsable de traitements et sous-traitant : une responsabilité conjointe](/rapport-information-4544/ii/c/1/a)

    * [b — Une application extra-territoriale du règlement](/rapport-information-4544/ii/c/1/b)

  * [2 — D’une logique de contrôle préalable à une logique de responsabilité](/rapport-information-4544/ii/c/2)

    * [a — Une logique de conformité et de responsabilité](/rapport-information-4544/ii/c/2/a)

    * [b — Les nouvelles obligations pesant sur les entreprises](/rapport-information-4544/ii/c/2/b)

  * [3 — La sanction du non-respect des obligations](/rapport-information-4544/ii/c/3)

  * [4 — Une attention particulière doit être accordée aux TPE et aux PME](/rapport-information-4544/ii/c/4)

* [D — L’ENCADREMENT DES TRANSFERTS DE DONNÉES À CARACTÈRE PERSONNEL VERS DES PAYS TIERS](/rapport-information-4544/ii/d)

  * [1 — L’encadrement par le règlement des transferts de données à caractère personnel vers des pays tiers ou des organisations internationales](/rapport-information-4544/ii/d/1)

  * [2 — L’impact du règlement sur le « bouclier vie privée Union européenne-États-Unis »](/rapport-information-4544/ii/d/2)

* [E — LE RENFORCEMENT DES AUTORITÉS DE RÉGULATION ET LA MISE EN PLACE D’UN GUICHET UNIQUE](/rapport-information-4544/ii/e)

  * [1 — L’évolution des missions des autorités de contrôle](/rapport-information-4544/ii/e/1)

  * [2 — La mise en place de décisions conjointes des autorités de contrôle des États membres](/rapport-information-4544/ii/e/2)

    * [a — Un interlocuteur unique pour les responsables de traitement](/rapport-information-4544/ii/e/2/a)

    * [b — Un mécanisme de décision conjointe des autorités de contrôle des États membres](/rapport-information-4544/ii/e/2/b)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/i/c" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/ii/a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

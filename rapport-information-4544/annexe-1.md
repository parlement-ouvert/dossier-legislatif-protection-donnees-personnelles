<!-- TITLE: ANNEXE N° 1 -->
<!-- SUBTITLE: LISTE DES RENVOIS AU DROIT NATIONAL PRÉVUS PAR LE RÈGLEMENT 2016/679 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/deplacement-a-bruxelles" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/annexe-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1) Pour les règles sectorielles spécifiques des Etats membres, notamment pour les données sensibles et les conditions de licéité, et la marge de manœuvre des Etats membres : _considérant 10_

2) Pour la prise en compte des besoins spécifiques des TPE/PME : _considérant 13_

3) Pour adapter le règlement pour le secteur public, y compris pour adopter des conditions spécifiques ou des restrictions : _considérant 19_ ; _article 6, paragraphe 2_

4) Pour désigner certains responsables de traitement : _article 4, point 7_ ;

5) Pour les traitements de données par les juridictions et la supervision de ceux-ci : _considérant 20_

6) Pour les données des personnes décédées : _considérant 27)_

7) Pour les tiers autorisés : _considérant 31_ et _article 4, point 9_ ;

8) Pour la licéité des traitements du secteur public (dans l’intérêt public ou imposant une obligation légale) et la création de tels traitements : _considérants 45, 47_ ; _article 6, paragraphe 3_ ;

9) Pour déterminer la compatibilité, la licéité et la base légale des traitements de données ultérieurs dans l’intérêt public : _considérants 50,_ _51_ ; _article 6, paragraphe 4_ ;

10) Pour les conditions relatives au consentement des enfants de moins de 16 ans et de plus de 13 ans : _article 8, paragraphe 1 er_ ;

11) Pour les traitements de données sensibles, y compris de santé, génétiques ou biométriques : _considérants 51, 52, 53_ , _article 9_ , _article 17_ pour les limitations au droit à l’oubli et 21 _, paragraphe 6_ pour les traitements de données sensibles à des fins scientifiques, statistiques ou historiques dans l’intérêt privé ;

12) Pour le traitement des données relatives aux condamnations pénales : _article 10_ ;

13) Pour déterminer les conséquences de demandes d’exercice de droits excessives ou manifestement infondées : _article 12_ ;

14) Pour l’obtention ou la divulgation d’information par le responsable de traitement : _article 14, paragraphe 5, point c)_ ;

15) Pour la compilation des opinions politiques dans le cadre des activités électorales : _considérant 56,_

16) Pour le droit à l’effacement et le droit à l’oubli : _considérant 65_ et _article 17 ;_

17) Pour la limitation du traitement des données au lieu de l’effacement : _article 18_ ;

18) Pour autoriser le profilage : _considérant 73_ et _article 22 ;_

19) Pour les restrictions aux droits des personnes et obligations des responsables de traitement : _considérant 59_ et _article 23_ ;

20) Pour déterminer les responsabilités respectives des responsables de traitement conjoints : _article 26_

21) Pour déterminer les exigences sur la validité juridique d’un acte liant le responsable de traitement au sous-traitant : _considérant 81_ et _articles 28_ et _29_ ;

22) Pour les exigences relatives aux instructions du responsable de traitement à son sous-traitant, y compris pour obliger le sous-traitant à conserver les données après la fin du contrat avec le responsable de traitement: _considérant 81_ et _article 28_ ;

23) Pour la sécurité des traitements : _article 32_ ;

24) Pour prévoir des analyses d’impact dans le cadre de l’adoption d’une législation nationale : _considérant 93_ et _article 35_ ;

25) Pour la procédure de consultation préalable de l’autorité de contrôle dans le cadre de l’adoption d’une nouvelle législation ou de la mise en place d’un nouveau traitement de données dans l’intérêt public : _article 36_ ;

26) Pour obliger à la désignation d’un délégué à la protection des données : _article 37_ ;

27) Pour l’obligation de secret professionnel du délégué à la protection des données : _article 38_ ;

28) Pour encourager les codes de conduite et la certification : _articles 40_ et _42_ ;

29) Pour l’accréditation des organismes certificateurs : _article 43_ ;

30) Pour conclure des accords internationaux : _considérant 102_ et _article 46_ ;

31) Pour des transferts de données dans l’intérêt public : _considérant 111_ ;

32) Pour certains transferts dérogatoires : _article 49_ ;

33) Pour limiter les transferts de données vers un pays tiers ou une organisation internationale en l’absence de décision d’adéquation à certaines catégories : _considérant 112_ ; _article 49 ;_

34) Pour la création des autorités de contrôle : _considérant 117_ ;

35) Pour prévoir la coopération entre les autorités de protection des données nationales s’il en existe plus d’une : _considérant 119,_ _article 51_ ;

36) Pour les conditions générales de désignation des membres et du personnel des autorités de contrôle : _considérant 121_ , _articles 51, 52, 53, 54,;_

37) Pour les pouvoirs des autorités de contrôle : _considérant 129_ , _article 58 ;_

38) Pour les instances auxquelles les autorités de contrôle font rapport : _article 59_ ;

39) Pour confier des pouvoirs d’enquête aux autorités de contrôle des autres Etats membres effectuant des enquêtes sur son territoire dans le cadre d’opérations conjointes : _article 62_ ;

40) Pour la désignation de l’autorité de protection des données participant au CEPD lorsqu’il y en a plusieurs : _considérant 119_ et _article 68_ ;

41) Pour les actions collectives et pour les exigences concernant les associations pouvant agir en représentation : _considérant 142,_ _article 80 ;_

42) Pour la désignation de la juridiction compétente sur le territoire : _considérant 143_ ; _article 78_ et _82 ;_

43) Pour les régimes de responsabilité : _considérant 146_ , _article 82 ;_

44) Pour les sanctions administratives des responsables de traitement publics : _considérants 150_ et _article 83 paragraphe 7 ;_

45) Pour prévoir des sanctions lorsque le Règlement n’a pas harmonisé les sanctions, y compris pénales : _considérants149 et 151_ et _article 84_ ;

46) Pour l’articulation des dérogations nationales en matière de liberté d’expression et de droit à l’information et les dérogations spécifiques : _considérant 153_ ; _article 85 ;_

47) Pour l’accès aux documents publics et la réutilisation des données du secteur public : _considérant 154_ et _article 86 ;_

48) Pour fixer les conditions spécifiques du traitement d’un numéro national d’identification : _article 87_ ;

49) Pour les traitements de données des salariés : _considérant 155_ ; _article 88 ;_

50) Pour les traitements des données à des fins archivistiques dans l’intérêt public, statistiques, scientifiques, historiques, pour prévoir les garanties appropriées nécessaires et les dérogations : _considérant 156_ , _article 89_ (et _articles 14_ et _17_ ) ;

51) Pour la recherche scientifique : _considérant 157_ ;

52) Pour les traitements de données à des fins archivistiques dans l’intérêt public : _considérant 158_ ;

53) Pour les traitements de données à des fins statistiques : _considérant 162_ ;

54) Pour les statistiques publiques : _considérant 163)_ ;

55) Pour limiter les pouvoirs des autorités de contrôle pour respecter le secret professionnel : _considérant 164_ et _articles 13, paragraphe 5, point d)_ et _90_ ;

56) Pour les traitements de données des églises et associations religieuses : _considérant 165._

_Source : SGAE_

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/deplacement-a-bruxelles" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/annexe-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

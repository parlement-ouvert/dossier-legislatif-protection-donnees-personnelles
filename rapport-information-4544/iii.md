<!-- TITLE: III -->
<!-- SUBTITLE: L’APPLICATION DU RÈGLEMENT À PARTIR DE MAI 2018 REND NÉCESSAIRE UNE ADAPTATION DU CADRE NATIONAL DE LA PROTECTION DES DONNÉES PERSONNELLES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/ii/e/2/b" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/iii/a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Le règlement du 27 avril 2016 sera applicable à partir du 25 mai 2018, ainsi que le prévoit l’article 99. Il est donc nécessaire d’adapter préalablement le cadre législatif de la protection des données à caractère personnel, principalement défini par la loi du 6 janvier 1978. La loi pour une République numérique du 7 octobre 2016 a déjà modifié certaines de ses dispositions mais un travail plus vaste devra être mené lors de la prochaine législature. Parallèlement, la coopération entre les États membres et les travaux du G29 jouent un rôle essentiel pour préparer l’application du règlement.

 **• Les travaux de la Commission européenne et du G29**

La Commission européenne, qui a la responsabilité de veiller à l’application du droit dérivé, en application de l’article 17 du traité sur l’Union européenne, suit déjà les travaux des États membres pour préparer l’application du règlement et la transposition de la directive sur les données policières et judiciaires. À cette fin, elle réunit mensuellement depuis le mois de septembre 2016 les experts nationaux chargés de ces travaux. Les réunions portent alternativement sur le règlement et la directive (86).

Les représentants de la Commission européenne rencontrés à Bruxelles par vos rapporteurs en janvier dernier leur ont indiqué qu’à ce stade, un dizaine d’États membres avaient finalisé un projet de loi.

Les réunions tenues jusqu’à présent sur le règlement ont notamment donné lieu à des discussions sur :

– les paragraphes 2 et 3 de l’article 6 permettant de maintenir ou introduire des dispositions spécifiques relatives aux traitements de données personnelles nécessaires au respect d’une obligation légale et aux traitements nécessaires à l’exécution d’une mission d’intérêt public ou relevant de l’exercice de l’autorité publique dont est investi le responsable du traitement ;

– les conditions applicables au consentement des enfants en ce qui concerne les services de la société de l’information (article 8) ;

– les règles applicables aux traitements portant sur des catégories particulières de données, notamment les données de santé (article 9) ;

– le délégué à la protection des données (article 37) ;

– les normes limitant certains transferts de catégories particulières de données (article 49) et les transferts non autorisés par le droit de l’Union européenne (article 48) ;

– les actions de groupe (article 80) ;

– le chapitre IX du règlement relatif aux situations particulières de traitement.

Par ailleurs, **la Commission européenne a entrepris la révision d’autres textes** afin de les mettre en conformité avec le règlement. Deux propositions législatives ont été publiées le 10 janvier dernier :

– une proposition de règlement devant se substituer à la directive de 2002 « vie privée et communications électroniques » ;

– une proposition de règlement relatif à la protection des données personnelles par les institutions et organes de l’Union européenne.

Elle a également publié une communication relative aux transferts internationaux de données personnelles.

 **Les propositions de la Commission européenne du 10 janvier 2017**

La **proposition de règlement « vie privée et communications électroniques »** (87) vise à harmoniser les règles applicables aux communications électroniques avec les nouvelles normes contenues dans le règlement général sur le protection des données personnelles du 27 avril 2016.

Le champ d’application est élargi par rapport à la directive de 2002. La Commission prévoit que le règlement s’appliquera non seulement aux opérateurs de télécommunications traditionnels mais aussi à de nouveaux acteurs comme _WhatsApp_ , _Facebook Messenger_ , _Skype_ , _Gmail_ , _iMessage_ ou _Viber (_88 _)_.

La protection de la vie privée portera sur le contenu des communications électroniques mais aussi sur les métadonnées (par exemple les données permettant d’identifier la source et la destination d’une communication, la date, l’heure et la durée d’une communication, sa localisation). Ces données devront être anonymisées ou effacées si l’utilisateur ne consent pas à leur traitement, sauf s’il s’agit de données nécessaires à la facturation.

Les utilisateurs devront avoir la possibilité de régler leurs paramètres sur leur navigateur de façon à accepter ou refuser les « cookies ».

Les communications non sollicitées seront interdites en l’absence d’accord de l’utilisateur. Le numéro du démarcheur ou un indicatif spécial indiquant qu’il s’agit d’un appel commercial devront apparaître.

Les autorités nationales de contrôle de la protection des données personnelles seront compétentes pour contrôler le respect du règlement.

La **proposition de règlement relatif à la protection des données personnelles par les institutions et organes de l’Union européenne** (89) **** renforce cette protection, afin de tirer les conséquences du règlement du 27 avril 2016.

Dans sa **communication sur les transferts internationaux de données personnelles** (90) **,** la Commission européenne annonce son intention de mener des discussions pouvant **** déboucher sur des décisions d’adéquation avec le Japon et la Corée en 2017, mais aussi l’Inde, ainsi que des pays d’Amérique latine et du voisinage européen. Lorsqu’il n’aura pas été possible d’adopter une décision d’adéquation, la Commission européenne souhaite utiliser d’autres instruments comme les clauses contractuelles standards ou les règles d’entreprise contraignantes.

Le G29, qui regroupe l’ensemble des autorités de protection des données des États membres et est présidé par Mme Isabelle Falque-Pierrotin, présidente de la CNIL depuis février 2014, est également fortement impliqué dans la mise en œuvre du règlement.

Le groupe a adopté le 2 février 2016 un plan d’action à cette fin (91), puis, en décembre 2016, des lignes directrices portant sur le statut et les missions des délégués à la protection des données, sur l’identification de l’autorité « chef de file » ainsi que sur le droit à la portabilité (92). Il a également adopté des documents relatifs à l’assistance mutuelle, au guichet unique et aux opérations conjointes.

D’autres lignes directrices sont attendues en 2017 sur la certification ainsi que sur l’évaluation des risques et les analyses d’impact.

 **• La préparation de l’application du règlement en France**

L’adaptation de notre législation relative à la protection des données est requise pour la mettre en conformité avec le règlement. Elle doit se traduire par **l’abrogation des dispositions incompatibles ou redondantes** – ce qui est l’effet classique d’un règlement – mais aussi par **l’adoption de dispositions nouvelles pour le compléter lorsqu’il ne peut s’appliquer directement**. Cela sera nécessaire en particulier en ce qui concerne la procédure de décision conjointe des autorités nationales de contrôle, prévue par l’article 60 mais qui appelle des précisions en droit national.

Interrogé par vos rapporteurs sur l’absence d’harmonisation des règles de procédure dans le règlement, le SGAE a indiqué que « _la diversité des législations nationales en matière de protection des données à caractère personnel et de sanction en cas de non-respect des obligations légales est telle qu’il était difficile de définir dans le règlement, de manière précise, les procédures applicables. En outre, l’ampleur et la complexité des négociations du règlement rendaient difficile de prévoir des règles détaillées permettant de procéder à une harmonisation également sur ces aspects_. »

À défaut d’une révision de la loi du 6 janvier 1978 avant le 25 mai 2018 précisant cette procédure, le nouveau régime de sanctions prononcées conjointement par les autorités de contrôle des États membres ne pourrait fonctionner, ainsi que l’a souligné Mme Isabelle Falque-Pierrotin, présidente de la CNIL, devant la commission des Lois (93). L’interruption prochaine des travaux parlementaires jusqu’en juin 2017 imposera donc d’engager très rapidement ensuite la révision de la loi « Informatique et libertés » **afin que les travaux législatifs aboutissent avant la fin de l’année 2017** , compte tenu du temps nécessaire pour les éventuels décrets d’application.

Dans ses réponses écrites au questionnaire des rapporteurs, la direction des affaires civiles et du Sceau (DACS) du ministère de la justice, chargée de la mise en conformité du droit national avec le règlement, indique qu’outre __ la loi du 6 janvier 1978, il sera nécessaire de modifier les différents textes législatifs évoquant des traitements de données à caractère personnel ou renvoyant à la loi de 1978 ainsi que le décret n° 2005-1309 pris pour son application. L’ampleur de ces modifications est en cours d’évaluation en lien avec les ministères concernés.

Dans la perspective du dépôt d’un projet de loi, la DACS a mis en place un groupe de travail associant le commissaire du Gouvernement auprès de la CNIL, des agents de la DACS, des représentants de l’administration de la CNIL, des universitaires, ainsi que des agents de la direction des affaires criminelles et des grâces (DACG), chargée de la transposition de la directive sur les données policières et judiciaires. La DACS sollicite également l’avis d’autres ministères sur des points particuliers les concernant. L’objectif est de parvenir à un projet de loi unique tirant les conséquences du règlement et transposant la directive.

Le III de l’article 65 de la loi pour une République numérique prévoit que le Gouvernement remet au Parlement, **au plus tard le 30 juin 2017** , un rapport sur les modifications de la loi de 1978 rendues nécessaires par l’entrée en vigueur du règlement. Compte tenu du calendrier précédemment évoqué, **vos rapporteurs jugent indispensable que** **la transmission de ce rapport et le dépôt du projet de loi révisant la loi du 6 janvier 1978 soient concomitants**.

La loi n° 2016-1321 du 7 octobre 2016 pour une République numérique a pris en compte la problématique de la protection des données personnelles, sans pour autant couvrir l’ensemble du champ du règlement.

Les dispositions de la loi pour une République numérique relatives aux données personnelles

L’article 54 de la loi pour une République numérique consacre, à l’article 1er de la loi du 6 janvier 1978, un **droit à la libre disposition de ses données personnelles,** selon lequel « _toute personne dispose du droit de décider et de contrôler les usages qui sont faits des données à caractère personnel la concernant, dans les conditions fixées par la présente loi_ ». La reconnaissance de ce droit avait été recommandée par le Conseil d’État, dans son étude annuelle de 2014  (94), ainsi que par la commission de réflexion sur les droits et libertés à l’âge du numérique (95).

L’article 48 crée dans le code de la consommation un nouvel article L.224-42-1 selon lequel _« le consommateur dispose en toutes circonstances d’un droit de récupération de l’ensemble de ses données. »_ Ce nouveau **droit à la portabilité** sera __ applicable à partir du 25 mai 2018. S’agissant des données personnelles, le nouvel article L.224-42-2 renvoie au régime défini par l’article 20 du règlement (96). Pour les données n’ayant pas un caractère personnel, situées hors du champ du règlement, l’article L.224-42-3 définit un régime spécifique, ne s’appliquant qu’aux opérateurs de communication électronique.

L’article 63 introduit un « **droit à l’oubli numérique » pour les mineurs,** dans l’objectif d’anticiper l’application du règlement européen s’agissant des mineurs **** : ****_« sur demande de la personne concernée, le responsable du traitement est tenu d’effacer dans les meilleurs délais les données à caractère personnel qui ont été collectées dans le cadre de l’offre de services de la société de l’information lorsque la personne concernée était mineure au moment de la collecte. Lorsqu’il a transmis les données en cause à un tiers lui-même responsable de traitement, il prend des mesures raisonnables, y compris d’ordre technique, compte tenu des technologies disponibles et des coûts de mise en_ _œ uvre, pour informer le tiers qui traite ces données que la personne concernée a demandé l’effacement de tout lien vers celles-ci, ou de toute copie ou de toute reproduction de celles-ci. »_.

Cette disposition ne concerne ainsi pas uniquement les mineurs mais la période pendant laquelle une personne était mineure, au titre de laquelle celle-ci peut demander l’effacement des données personnelles la concernant.

En cas d’absence de réponse ou de refus du responsable du traitement dans un délai d’un mois après la demande, la personne pourra saisir la CNIL qui devra se prononcer dans un délai restreint (trois semaines) suivant la date de réception de cette réclamation.

Les exceptions prévues sont identiques à celles énoncées à l’article 17 du règlement s’agissant du droit à l’effacement (97).

Le même article définit également **le régime** **des données personnelles après le décès de la personne concernée (« mort numérique** »), sujet qui ne relève pas du règlement européen  (98). Selon le nouvel article 40-1 de la loi du 6 janvier 1978, toute personne peut donner des directives aux responsables de traitement quant à l’utilisation de ses données personnelles, après son décès. Ces directives peuvent concerner la conservation, l’effacement et la communication des données. Elles peuvent être générales ou particulières (propres à certains traitements). En l’absence de directives ou de mentions contraires des directives, les héritiers peuvent obtenir la clôture des comptes utilisateurs des défunts, s’opposer à la poursuite des traitements ou faire procéder à leur mise à jour.

L’article 57 modifie l’article 32 de la loi du 6 janvier 1978 afin de rendre obligatoire l’information par le responsable de traitement des personnes concernées sur la durée de conservation des données, ou en cas d’impossibilité, sur les critères utilisés pour déterminer cette durée.

L’article 58 créé un nouvel article 43 _bis_ dans la loi du 6 janvier 1978 imposant aux responsables de traitement, « _lorsque cela est possible_ », de permettre l’exercice par voie électronique des différents droits des personnes concernées par un traitement (droits d’accès, de rectification, d’opposition, d’information), dès lors que les données ont été recueillies par voie électronique. Cet article sera abrogé à compter du 25 mai 2018, l’exercice des droits étant régi par l’article 12, paragraphe 2, du règlement, d’application directe.

S’agissant enfin du **profilage** , l’article 4 crée un article L. 311-3-1 au sein du code des relations entre le public et l’administration, prévoyant que l’administration doit informer les intéressés lorsqu’une décision individuelle est prise sur le fondement d’un traitement algorithmique et leur communiquer, à leur demande, les règles définissant ce traitement ainsi que les principales caractéristiques de sa mise en œuvre, sous réserve des dispositions du 2° de l’article L. 311-5 relatives aux secrets protégés (par exemple le secret de la défense nationale, le secret relatif à la sûreté de l’État, à la sécurité publique, à la sécurité des personnes ou à la sécurité des systèmes d’information des administrations). La Commission d’accès aux documents administratifs (CADA), qui avait déjà jugé que les codes sources et les logiciels étaient des documents administratifs communicables, a précisé s’agissant de ce nouvel article, que « _pour présenter un effet utile, les dispositions du nouvel_ [article] _doivent être comprises comme ouvrant aux personnes le droit d’obtenir de l’administration, en complément de la communication éventuelle du code source, dont la compréhension nécessite des compétences techniques en code informatique, des explications complémentaires, explicitant les règles de traitement mises en_ _œ uvre et les principales caractéristiques de celle–ci_. _Ces dispositions complètent ainsi, notamment en faveur des personnes morales, le droit que toute personne physique tient de l’article 39 de la loi du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés d’obtenir du responsable d’un traitement de données à caractère personnel « les informations permettant de connaître et de contester la logique qui sous-tend le traitement automatisé en cas de décision prise sur le fondement de celui-ci et produisant des effets juridiques à l’égard de l’intéressé._ » (99)

Les articles 59 et 60 de la loi ont élargi **les missions de la CNIL** :

– alors qu’il lui appartenait déjà de rendre un avis sur les projets de loi ou de décret relatifs à la protection des personnes à l’égard des traitements automatisés, sa consultation devient obligatoire sur les dispositions d’un projet de loi ou de décret relatives à la protection des données à caractère personnel ou à leur traitement ;

– il lui appartient désormais de mener une réflexion sur les questions éthiques et sociétales soulevées par l’évolution des technologies numériques ;

– elle se voit confier une mission de promotion de l’utilisation des technologies protectrices de la vie privée, notamment les technologies de chiffrement des données ;

– elle peut certifier ou homologuer et publier des référentiels ou des méthodologies générales aux fins de certification de processus d’anonymisation des données à caractère personnel, notamment en vue d’une réutilisation d’informations publiques mises en ligne.

Par ailleurs, différentes dispositions de la loi organisent **la coopération de la CNIL avec des organismes extérieurs** :

– le président de la commission d’accès aux documents administratifs (CADA) ou son représentant siège à la CNIL et les deux autorités se réunissent dans un collège unique pour traiter de sujets d’intérêt commun, sur l’initiative conjointe de leurs présidents (100) ;

– la CNIL et l’Autorité de régulation des communications électroniques et des postes (ARCEP) peuvent se saisir mutuellement pour avis sur les questions relevant de leurs compétences respectives (101) ;

– la CNIL peut, à la demande d’une autorité de contrôle d’un État non membre de l’Union européenne, « _dès lors que celui-ci offre un niveau de protection adéquat des données à caractère personnel_ » procéder à des contrôles et lui communiquer des informations, sous réserve d’avoir conclu une convention avec cette autorité  (102) et à l’exception des traitements dits « de souveraineté ».

La loi a également renforcé **l’efficacité des procédures** de la CNIL. L’article 64 a modifié l’article 45 de la loi du 6 janvier 1978 afin de réduire le délai de mise en demeure de faire cesser un manquement, en cas « _d’extrême urgence_ », de cinq jours à vingt-quatre heures. Lorsqu’un manquement ne peut pas faire l’objet d’une mise en conformité dans le cadre d’une mise en demeure, la formation restreinte de la CNIL peut directement prononcer une sanction, après une procédure contradictoire.

Le I de l’article 65 modifie l’article 47 de la loi du 6 janvier 1978 afin de porter le **montant maximal des sanctions** , auparavant fixé à 150 000 euros, ou 300 000 euros cas de manquement réitéré du responsable de traitement à ses obligations, à **3 millions d’euros**. Il précise que la formation restreinte de la CNIL doit notamment prendre en compte « _le caractère intentionnel ou de négligence du manquement, les mesures prises par le responsable du traitement pour atténuer les dommages subis par les personnes concernées, le degré de coopération avec la commission afin de remédier au manquement et d’atténuer ses effets négatifs éventuels, les catégories de données à caractère personnel concernées et la manière dont le manquement a été porté à la connaissance de la commission_. »

Il s’agit d’une **disposition transitoire** , le II de l’article 65 prévoyant qu’à compter du 25 mai 2018, les sanctions prononcées par la CNIL dans le champ du règlement le seront conformément à l’article 83 dudit règlement.

 **Vos rapporteurs tiennent à souligner l’importance de la réflexion sur l’usage des algorithmes** et se félicitent des **avancées intervenues en France** , notamment du lancement par l’Institut national de recherche en informatique et automatique (INRIA) de la plateforme TransAlgo, évoqué lors de son audition par M. Daniel Le Métayer, directeur de recherche dans cet institut. Cette plateforme scientifique aura notamment pour objectif de vérifier et de tester les algorithmes de traitement de données et de favoriser la conception d’algorithmes responsables et transparents (103). Cette initiative fait suite aux recommandations d’un rapport du Conseil général de l’économie sur la régulation des algorithmes (104).

Si certaines mesures d’adaptation requises par le règlement ne suscitent pas de débats, en revanche d’autres questions demeurent en suspens et devront être tranchées par le législateur.

* [A — DE NÉCESSAIRES ADAPTATIONS](/rapport-information-4544/iii/a)

  * [1 — Modifier le montant des amendes que peut prononcer la CNIL](/rapport-information-4544/iii/a/1)

  * [2 — Mettre en place une procédure de coopération en matière de sanction avec les autorités de contrôle des États membres](/rapport-information-4544/iii/a/2)

* [B — DES QUESTIONS RESTENT EN SUSPENS](/rapport-information-4544/iii/b)

  * [1 — Une nécessaire clarification de certaines notions](/rapport-information-4544/iii/b/1)

  * [2 — Les règles spécifiques à certains types de traitements](/rapport-information-4544/iii/b/2)

    * [a — Les traitements des données de santé](/rapport-information-4544/iii/b/2/a)

    * [b — Les traitements des données biométriques et génétiques](/rapport-information-4544/iii/b/2/b)

    * [c — Les traitements aux fins d’expression journalistique, artistique, universitaire et littéraire](/rapport-information-4544/iii/b/2/c)

    * [d — Les traitements de données relatives aux infractions, aux condamnations et aux mesures de sûreté](/rapport-information-4544/iii/b/2/d)

    * [e — Les traitements portant sur le numéro d’identification national](/rapport-information-4544/iii/b/2/e)

    * [f — Les traitements des données personnelles à des fins archivistiques, de recherche scientifique ou historique ou à des fins statistiques](/rapport-information-4544/iii/b/2/f)

  * [3 — Les actions de groupe](/rapport-information-4544/iii/b/3)

  * [4 — Le droit à la portabilité](/rapport-information-4544/iii/b/4)

  * [5 — Les dispositions spécifiques concernant les enfants](/rapport-information-4544/iii/b/5)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-4544/ii/e/2/b" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information</span> <a href="/rapport-information-4544/iii/a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

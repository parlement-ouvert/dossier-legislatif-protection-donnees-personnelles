<!-- TITLE: Mentions légales -->

Conformément aux dispositions des Articles 6-III et 19 de la Loi n°2004-575 du 21 juin 2004 pour la Confiance dans l’économie numérique, dite L.C.E.N., il est porté à la connaissance des utilisateurs et visiteurs du site [donnees-personnelles.parlement-ouvert.fr](/) les présentes mentions légales.

## Éditrice, directrice de la publication et hébergeur

Paula Forteza
101 rue de l’Université
75355 Paris 07 SP

Téléphone : +33 1 40 63 74 20
Courriel : paula.forteza@assemblee-nationale.fr

## Protection des données personnelles

Ce site n’est pas déclaré à la Commission nationale de l'informatique et des libertés (CNIL), car il ne collecte pas de données personnelles.

<!-- TITLE: Amendements en commission des lois de l'Assemblée (première lecture) -->

* [Index des amendements par auteur](/amendements-1-assemblee-hemicycle/index-amendements-par-auteur)

* [Article PREMIER](/amendements-1-assemblee-hemicycle/article-premier)

  * [Amendement n° 37 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-37)
  * [Amendement n° 102 — Adopté](/amendements-1-assemblee-hemicycle/article-premier/amendement-102)
  * [Amendement n° 179 — Tombé](/amendements-1-assemblee-hemicycle/article-premier/amendement-179)
  * [Amendement n° 7 — Retiré](/amendements-1-assemblee-hemicycle/article-premier/amendement-7)
  * [Amendement n° 39 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-39)
  * [Amendement n° 38 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-38)
  * [Amendement n° 111 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-111)
  * [Amendement n° 181 — Retiré](/amendements-1-assemblee-hemicycle/article-premier/amendement-181)
  * [Amendement n° 3 — Non soutenu](/amendements-1-assemblee-hemicycle/article-premier/amendement-3)
  * [Amendement n° 156 — Adopté](/amendements-1-assemblee-hemicycle/article-premier/amendement-156)
  * [Amendement n° 40 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-40)
  * [Amendement n° 104 — Adopté](/amendements-1-assemblee-hemicycle/article-premier/amendement-104)
  * [Amendement n° 105 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-105)
  * [Amendement n° 152 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-152)
  * [Amendement n° 163 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-163)

* [Après l'article PREMIER](/amendements-1-assemblee-hemicycle/apres-l-article-premier)

  * [Amendement n° 41 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-premier/amendement-41)
  * [Amendement n° 2 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-premier/amendement-2)
  * [Amendement n° 142 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-premier/amendement-142)

* [Article 1er bis](/amendements-1-assemblee-hemicycle/article-1-bis)

  * [Amendement n° 4 — Non soutenu](/amendements-1-assemblee-hemicycle/article-1-bis/amendement-4)
  * [Amendement n° 5 — Non soutenu](/amendements-1-assemblee-hemicycle/article-1-bis/amendement-5)
  * [Amendement n° 6 — Non soutenu](/amendements-1-assemblee-hemicycle/article-1-bis/amendement-6)
  * [Amendement n° 99 — Adopté](/amendements-1-assemblee-hemicycle/article-1-bis/amendement-99)

* [Article 2](/amendements-1-assemblee-hemicycle/article-2)

  * [Amendement n° 158 — Adopté](/amendements-1-assemblee-hemicycle/article-2/amendement-158)
  * [Amendement n° 42 — Rejeté](/amendements-1-assemblee-hemicycle/article-2/amendement-42)
  * [Amendement n° 43 — Rejeté](/amendements-1-assemblee-hemicycle/article-2/amendement-43)

* [Après l'article 2](/amendements-1-assemblee-hemicycle/apres-l-article-2)

  * [Amendement n° 54 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-2/amendement-54)

* [Article 2 bis](/amendements-1-assemblee-hemicycle/article-2-bis)

  * [Amendement n° 123 — Adopté](/amendements-1-assemblee-hemicycle/article-2-bis/amendement-123)
  * [Amendement n° 98 — Adopté](/amendements-1-assemblee-hemicycle/article-2-bis/amendement-98)
  * [Amendement n° 100 — Adopté](/amendements-1-assemblee-hemicycle/article-2-bis/amendement-100)
  * [Amendement n° 72 — Non soutenu](/amendements-1-assemblee-hemicycle/article-2-bis/amendement-72)

* [Article 3](/amendements-1-assemblee-hemicycle/article-3)

  * [Amendement n° 157 — Adopté](/amendements-1-assemblee-hemicycle/article-3/amendement-157)

* [Article 4](/amendements-1-assemblee-hemicycle/article-4)

  * [Amendement n° 153 — Retiré](/amendements-1-assemblee-hemicycle/article-4/amendement-153)
  * [Amendement n° 76 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-76)
  * [Amendement n° 23 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-23)
  * [Amendement n° 24 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-24)
  * [Amendement n° 162 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-162)
  * [Amendement n° 122 — Non soutenu](/amendements-1-assemblee-hemicycle/article-4/amendement-122)
  * [Amendement n° 77 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-77)
  * [Amendement n° 78 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-78)
  * [Amendement n° 161 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-161)
  * [Amendement n° 124 — Retiré](/amendements-1-assemblee-hemicycle/article-4/amendement-124)

* [Après l'article 4](/amendements-1-assemblee-hemicycle/apres-l-article-4)

  * [Amendement n° 49 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-4/amendement-49)

* [Article 5](/amendements-1-assemblee-hemicycle/article-5)

  * [Amendement n° 26 — Rejeté](/amendements-1-assemblee-hemicycle/article-5/amendement-26)
  * [Amendement n° 136 — Non soutenu](/amendements-1-assemblee-hemicycle/article-5/amendement-136)
  * [Amendement n° 45 — Rejeté](/amendements-1-assemblee-hemicycle/article-5/amendement-45)
  * [Amendement n° 137 — Non soutenu](/amendements-1-assemblee-hemicycle/article-5/amendement-137)

* [Article 6](/amendements-1-assemblee-hemicycle/article-6)

  * [Amendement n° 131 — Non soutenu](/amendements-1-assemblee-hemicycle/article-6/amendement-131)
  * [Amendement n° 48 — Rejeté](/amendements-1-assemblee-hemicycle/article-6/amendement-48)
  * [Amendement n° 11 — Rejeté](/amendements-1-assemblee-hemicycle/article-6/amendement-11)
  * [Amendement n° 79 — Rejeté](/amendements-1-assemblee-hemicycle/article-6/amendement-79)
  * [Amendement n° 47 — Rejeté](/amendements-1-assemblee-hemicycle/article-6/amendement-47)
  * [Amendement n° 59 — Rejeté](/amendements-1-assemblee-hemicycle/article-6/amendement-59)
  * [Amendement n° 80 — Rejeté](/amendements-1-assemblee-hemicycle/article-6/amendement-80)

* [Après l'article 6](/amendements-1-assemblee-hemicycle/apres-l-article-6)

  * [Amendement n° 85 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-6/amendement-85)
  * [Amendement n° 46 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-6/amendement-46)

* [Article 7](/amendements-1-assemblee-hemicycle/article-7)

  * [Amendement n° 8 — Non soutenu](/amendements-1-assemblee-hemicycle/article-7/amendement-8)
  * [Amendement n° 139 — Non soutenu](/amendements-1-assemblee-hemicycle/article-7/amendement-139)
  * [Amendement n° 106 — Rejeté](/amendements-1-assemblee-hemicycle/article-7/amendement-106)
  * [Amendement n° 64 — Adopté](/amendements-1-assemblee-hemicycle/article-7/amendement-64)
  * [Amendement n° 112 — Adopté](/amendements-1-assemblee-hemicycle/article-7/amendement-112)
  * [Amendement n° 101 — Adopté](/amendements-1-assemblee-hemicycle/article-7/amendement-101)
  * [Amendement n° 50 — Rejeté](/amendements-1-assemblee-hemicycle/article-7/amendement-50)
  * [Amendement n° 65 — Rejeté](/amendements-1-assemblee-hemicycle/article-7/amendement-65)

* [Après l'article 7](/amendements-1-assemblee-hemicycle/apres-l-article-7)

  * [Amendement n° 135 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-7/amendement-135)
  * [Amendement n° 63 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-7/amendement-63)

* [Article 9](/amendements-1-assemblee-hemicycle/article-9)

  * [Amendement n° 66 — Rejeté](/amendements-1-assemblee-hemicycle/article-9/amendement-66)
  * [Amendement n° 81 — Rejeté](/amendements-1-assemblee-hemicycle/article-9/amendement-81)
  * [Amendement n° 147 — Rejeté](/amendements-1-assemblee-hemicycle/article-9/amendement-147)

* [Avant l'article 10](/amendements-1-assemblee-hemicycle/avant-l-article-10)

  * [Amendement n° 107 — Rejeté](/amendements-1-assemblee-hemicycle/avant-l-article-10/amendement-107)

* [Article 10](/amendements-1-assemblee-hemicycle/article-10)

  * [Amendement n° 177 — Retiré](/amendements-1-assemblee-hemicycle/article-10/amendement-177)
  * [Amendement n° 159 — Rejeté](/amendements-1-assemblee-hemicycle/article-10/amendement-159)

* [Après l'article 10](/amendements-1-assemblee-hemicycle/apres-l-article-10)

  * [Amendement n° 114 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-10/amendement-114)
  * [Amendement n° 115 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-10/amendement-115)
  * [Amendement n° 116 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-10/amendement-116)

* [Article 11](/amendements-1-assemblee-hemicycle/article-11)

  * [Amendement n° 67 — Rejeté](/amendements-1-assemblee-hemicycle/article-11/amendement-67)
  * [Amendement n° 126 — Non soutenu](/amendements-1-assemblee-hemicycle/article-11/amendement-126)
  * [Amendement n° 51 — Rejeté](/amendements-1-assemblee-hemicycle/article-11/amendement-51)

* [Article 12](/amendements-1-assemblee-hemicycle/article-12)

  * [Amendement n° 52 — Rejeté](/amendements-1-assemblee-hemicycle/article-12/amendement-52)

* [Article 13](/amendements-1-assemblee-hemicycle/article-13)

  * [Amendement n° 68 — Rejeté](/amendements-1-assemblee-hemicycle/article-13/amendement-68)
  * [Amendement n° 128 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-128)
  * [Amendement n° 148 — Rejeté](/amendements-1-assemblee-hemicycle/article-13/amendement-148)
  * [Amendement n° 125 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-125)
  * [Amendement n° 176 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-176)
  * [Amendement n° 94 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-94)
  * [Amendement n° 175 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-175)
  * [Amendement n° 29 — Non soutenu](/amendements-1-assemblee-hemicycle/article-13/amendement-29)
  * [Amendement n° 36 — Non soutenu](/amendements-1-assemblee-hemicycle/article-13/amendement-36)
  * [Amendement n° 30 — Non soutenu](/amendements-1-assemblee-hemicycle/article-13/amendement-30)
  * [Amendement n° 141 — Rejeté](/amendements-1-assemblee-hemicycle/article-13/amendement-141)
  * [Amendement n° 95 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-95)
  * [Amendement n° 174 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-174)

* [Après l'article 13](/amendements-1-assemblee-hemicycle/apres-l-article-13)

  * [Amendement n° 134 — Retiré](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-134)
  * [Amendement n° 90 — Adopté](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-90)
  * [Amendement n° 144 — Adopté](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-144)
  * [Amendement n° 167 — Adopté](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-167)
  * [Amendement n° 62 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-62)
  * [Amendement n° 118 — Non soutenu](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-118)
  * [Amendement n° 75 — Retiré](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-75)

* [Avant l'article 14 A](/amendements-1-assemblee-hemicycle/avant-l-article-14-a)

  * [Amendement n° 86 — Rejeté](/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-86)
  * [Amendement n° 91 — Rejeté](/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-91)
  * [Amendement n° 92 — Rejeté](/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-92)
  * [Amendement n° 183 — Non soutenu](/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-183)
  * [Amendement n° 108 — Non soutenu](/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-108)
  * [Amendement n° 184 — Adopté](/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-184)

* [Article 14 A](/amendements-1-assemblee-hemicycle/article-14-a)

  * [Amendement n° 35 — Rejeté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-35)
  * [Amendement n° 69 — Rejeté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-69)
  * [Amendement n° 21 — Rejeté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-21)
  * [Amendement n° 10 — Rejeté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-10)
  * [Amendement n° 103 — Adopté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-103)
  * [Amendement n° 182 — Adopté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-182)
  * [Amendement n° 9 — Tombé](/amendements-1-assemblee-hemicycle/article-14-a/amendement-9)
  * [Amendement n° 12 — Adopté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-12)

* [Après l'article 14 A](/amendements-1-assemblee-hemicycle/apres-l-article-14-a)

  * [Amendement n° 19 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-14-a/amendement-19)

* [Article 14](/amendements-1-assemblee-hemicycle/article-14)

  * [Amendement n° 53 — Rejeté](/amendements-1-assemblee-hemicycle/article-14/amendement-53)
  * [Amendement n° 119 — Rejeté](/amendements-1-assemblee-hemicycle/article-14/amendement-119)
  * [Amendement n° 127 — Adopté](/amendements-1-assemblee-hemicycle/article-14/amendement-127)

* [Après l'article 14](/amendements-1-assemblee-hemicycle/apres-l-article-14)

  * [Amendement n° 61 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-14/amendement-61)
  * [Amendement n° 154 — Adopté](/amendements-1-assemblee-hemicycle/apres-l-article-14/amendement-154)

* [Article 15](/amendements-1-assemblee-hemicycle/article-15)

  * [Amendement n° 74 — Rejeté](/amendements-1-assemblee-hemicycle/article-15/amendement-74)
  * [Amendement n° 149 — Rejeté](/amendements-1-assemblee-hemicycle/article-15/amendement-149)
  * [Amendement n° 160 — Rejeté](/amendements-1-assemblee-hemicycle/article-15/amendement-160)
  * [Amendement n° 55 — Rejeté](/amendements-1-assemblee-hemicycle/article-15/amendement-55)
  * [Amendement n° 132 — Non soutenu](/amendements-1-assemblee-hemicycle/article-15/amendement-132)
  * [Amendement n° 56 — Rejeté](/amendements-1-assemblee-hemicycle/article-15/amendement-56)

* [Après l'article 15](/amendements-1-assemblee-hemicycle/apres-l-article-15)

  * [Amendement n° 71 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-15/amendement-71)

* [Article 16 A](/amendements-1-assemblee-hemicycle/article-16-a)

  * [Amendement n° 82 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-82)
  * [Amendement n° 164 — Adopté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-164)
  * [Amendement n° 87 — Adopté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-87)
  * [Amendement n° 165 — Adopté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-165)
  * [Amendement n° 70 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-70)
  * [Amendement n° 73 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-73)
  * [Amendement n° 109 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-109)
  * [Amendement n° 150 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-150)

* [Article 16](/amendements-1-assemblee-hemicycle/article-16)

  * [Amendement n° 133 — Non soutenu](/amendements-1-assemblee-hemicycle/article-16/amendement-133)
  * [Amendement n° 88 — Adopté](/amendements-1-assemblee-hemicycle/article-16/amendement-88)
  * [Amendement n° 110 — Rejeté](/amendements-1-assemblee-hemicycle/article-16/amendement-110)
  * [Amendement n° 151 — Rejeté](/amendements-1-assemblee-hemicycle/article-16/amendement-151)

* [Article 17](/amendements-1-assemblee-hemicycle/article-17)

  * [Amendement n° 83 — Rejeté](/amendements-1-assemblee-hemicycle/article-17/amendement-83)

* [Après l'article 17](/amendements-1-assemblee-hemicycle/apres-l-article-17)

  * [Amendement n° 169 — Retiré](/amendements-1-assemblee-hemicycle/apres-l-article-17/amendement-169)
  * [Amendement n° 173 — Retiré](/amendements-1-assemblee-hemicycle/apres-l-article-17/amendement-173)

* [Article 19](/amendements-1-assemblee-hemicycle/article-19)

  * [Amendement n° 57 — Rejeté](/amendements-1-assemblee-hemicycle/article-19/amendement-57)
  * [Amendement n° 13 — Adopté](/amendements-1-assemblee-hemicycle/article-19/amendement-13)
  * [Amendement n° 14 — Adopté](/amendements-1-assemblee-hemicycle/article-19/amendement-14)
  * [Amendement n° 138 — Non soutenu](/amendements-1-assemblee-hemicycle/article-19/amendement-138)
  * [Amendement n° 15 — Adopté](/amendements-1-assemblee-hemicycle/article-19/amendement-15)
  * [Amendement n° 140 — Non soutenu](/amendements-1-assemblee-hemicycle/article-19/amendement-140)
  * [Amendement n° 143 — Non soutenu](/amendements-1-assemblee-hemicycle/article-19/amendement-143)
  * [Amendement n° 145 — Non soutenu](/amendements-1-assemblee-hemicycle/article-19/amendement-145)
  * [Amendement n° 16 — Adopté](/amendements-1-assemblee-hemicycle/article-19/amendement-16)
  * [Amendement n° 17 — Adopté](/amendements-1-assemblee-hemicycle/article-19/amendement-17)
  * [Amendement n° 18 — Adopté](/amendements-1-assemblee-hemicycle/article-19/amendement-18)

* [Article 20](/amendements-1-assemblee-hemicycle/article-20)

  * [Amendement n° 31 — Rejeté](/amendements-1-assemblee-hemicycle/article-20/amendement-31)
  * [Amendement n° 58 — Rejeté](/amendements-1-assemblee-hemicycle/article-20/amendement-58)
  * [Amendement n° 120 — Rejeté](/amendements-1-assemblee-hemicycle/article-20/amendement-120)
  * [Amendement n° 129 — Rejeté](/amendements-1-assemblee-hemicycle/article-20/amendement-129)
  * [Amendement n° 146 — Non soutenu](/amendements-1-assemblee-hemicycle/article-20/amendement-146)

* [Après l'article 20](/amendements-1-assemblee-hemicycle/apres-l-article-20)

  * [Amendement n° 155 — Adopté](/amendements-1-assemblee-hemicycle/apres-l-article-20/amendement-155)

* [Article 21](/amendements-1-assemblee-hemicycle/article-21)

  * [Amendement n° 32 — Adopté](/amendements-1-assemblee-hemicycle/article-21/amendement-32)
  * [Amendement n° 89 — Adopté](/amendements-1-assemblee-hemicycle/article-21/amendement-89)

* [Après l'article 22](/amendements-1-assemblee-hemicycle/apres-l-article-22)

  * [Amendement n° 84 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-22/amendement-84)

* [Après l'article 23](/amendements-1-assemblee-hemicycle/apres-l-article-23)

  * [Amendement n° 60 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-23/amendement-60)

* [Après l'article 24](/amendements-1-assemblee-hemicycle/apres-l-article-24)

  * [Amendement n° 20 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-24/amendement-20)

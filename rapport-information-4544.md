<!-- TITLE: N° 4544 - Rapport d'information de Mme Anne-Yvonne Le Dain et M. Philippe Gosselin déposé en application de l'article 145 du règlement, par la commission des lois constitutionnelles, de la législation et de l'administration générale de la République, en conclusion des travaux d'une mission d'information sur les incidences des nouvelles normes européennes en matière de protection des données personnelles sur la législation française. -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">N° 4544 - Rapport d'information de Mme Anne-Yvonne Le Dain et M. Philippe Gosselin déposé en application de l'article 145 du règlement, par la commission des lois constitutionnelles, de la législation et de l'administration générale de la République, en conclusion des travaux d'une mission d'information sur les incidences des nouvelles normes européennes en matière de protection des données personnelles sur la législation française.</span> <a href="/rapport-information-4544/synthese-du-rapport" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Entête](/rapport-information-4544/entete)

* [SYNTHÈSE DU RAPPORT](/rapport-information-4544/synthese-du-rapport)

* [INTRODUCTION](/rapport-information-4544/introduction)

* [I — LE RÉGIME EN VIGUEUR DE LA PROTECTION DES DONNÉES PERSONNELLES DANS L’UNION EUROPÉENNE : UN CADRE ANCIEN LAISSANT D’IMPORTANTES MARGES D’INTERPRÉTATION AUX ÉTATS MEMBRES](/rapport-information-4544/i)

  * [A — LE CADRE DE RÉFÉRENCE DÉFINI PAR LA DIRECTIVE DE 1995, COMPLÉTÉ PAR LA DIRECTIVE DE 2002 « VIE PRIVÉE ET COMMUNICATIONS ÉLECTRONIQUES »](/rapport-information-4544/i/a)

    * [1 — La directive 95/46/CE du 24 octobre 1995 relative à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données](/rapport-information-4544/i/a/1)

      * [a — L’affirmation des grands principes de la protection des données personnelles](/rapport-information-4544/i/a/1/a)

      * [b — Des limites liées à l’évolution de l’environnement numérique et aux divergences d’application dans les États membres](/rapport-information-4544/i/a/1/b)

    * [2 — La directive 2002/58/CE du 12 juillet 2002 « vie privée et communications électroniques »](/rapport-information-4544/i/a/2)

  * [B — L’APPORT DE LA COUR DE JUSTICE DE L’UNION EUROPÉENNE AU RÉGIME EUROPÉEN DE PROTECTION DES DONNÉES PERSONNELLES](/rapport-information-4544/i/b)

  * [C — LES MESURES NATIONALES DE PROTECTION DES DONNÉES PERSONNELLES DES PERSONNES PHYSIQUES](/rapport-information-4544/i/c)

* [II — LE RÈGLEMENT DU 27 AVRIL 2016 : DES ÉVOLUTIONS SIGNIFICATIVES ET UN RENFORCEMENT VOLONTARISTE DE L’HARMONISATION DE LA PROTECTION DES DONNÉES PERSONNELLES DANS L’UNION EUROPÉENNE](/rapport-information-4544/ii)

  * [A — LE RÉSULTAT D’UNE LONGUE NÉGOCIATION](/rapport-information-4544/ii/a)

    * [1 — Le paquet « données personnelles »](/rapport-information-4544/ii/a/1)

    * [2 — La position de la France](/rapport-information-4544/ii/a/2)

    * [3 — Un règlement sui generis, laissant d’importantes marges de manœuvre aux États membres](/rapport-information-4544/ii/a/3)

  * [B — LE RENFORCEMENT DES DROITS DES PERSONNES PHYSIQUES](/rapport-information-4544/ii/b)

    * [1 — L’évolution de la définition des données protégées](/rapport-information-4544/ii/b/1)

    * [2 — Le renforcement du consentement](/rapport-information-4544/ii/b/2)

    * [3 — L’extension du droit à l’information](/rapport-information-4544/ii/b/3)

    * [4 — L’affirmation de nouveaux droits](/rapport-information-4544/ii/b/4)

      * [a — Le droit à l’effacement (« droit à l’oubli »)](/rapport-information-4544/ii/b/4/a)

      * [b — Le droit à la portabilité des données](/rapport-information-4544/ii/b/4/b)

      * [c — Le recours aux actions collectives](/rapport-information-4544/ii/b/4/c)

    * [5 — La question du profilage](/rapport-information-4544/ii/b/5)

  * [C — LES PRINCIPES S’IMPOSANT AUX OPÉRATEURS TRAITANT DES DONNÉES PERSONNELLES](/rapport-information-4544/ii/c)

    * [1 — Un périmètre étendu d’application](/rapport-information-4544/ii/c/1)

      * [a — Responsable de traitements et sous-traitant : une responsabilité conjointe](/rapport-information-4544/ii/c/1/a)

      * [b — Une application extra-territoriale du règlement](/rapport-information-4544/ii/c/1/b)

    * [2 — D’une logique de contrôle préalable à une logique de responsabilité](/rapport-information-4544/ii/c/2)

      * [a — Une logique de conformité et de responsabilité](/rapport-information-4544/ii/c/2/a)

      * [b — Les nouvelles obligations pesant sur les entreprises](/rapport-information-4544/ii/c/2/b)

    * [3 — La sanction du non-respect des obligations](/rapport-information-4544/ii/c/3)

    * [4 — Une attention particulière doit être accordée aux TPE et aux PME](/rapport-information-4544/ii/c/4)

  * [D — L’ENCADREMENT DES TRANSFERTS DE DONNÉES À CARACTÈRE PERSONNEL VERS DES PAYS TIERS](/rapport-information-4544/ii/d)

    * [1 — L’encadrement par le règlement des transferts de données à caractère personnel vers des pays tiers ou des organisations internationales](/rapport-information-4544/ii/d/1)

    * [2 — L’impact du règlement sur le « bouclier vie privée Union européenne-États-Unis »](/rapport-information-4544/ii/d/2)

  * [E — LE RENFORCEMENT DES AUTORITÉS DE RÉGULATION ET LA MISE EN PLACE D’UN GUICHET UNIQUE](/rapport-information-4544/ii/e)

    * [1 — L’évolution des missions des autorités de contrôle](/rapport-information-4544/ii/e/1)

    * [2 — La mise en place de décisions conjointes des autorités de contrôle des États membres](/rapport-information-4544/ii/e/2)

      * [a — Un interlocuteur unique pour les responsables de traitement](/rapport-information-4544/ii/e/2/a)

      * [b — Un mécanisme de décision conjointe des autorités de contrôle des États membres](/rapport-information-4544/ii/e/2/b)

* [III — L’APPLICATION DU RÈGLEMENT À PARTIR DE MAI 2018 REND NÉCESSAIRE UNE ADAPTATION DU CADRE NATIONAL DE LA PROTECTION DES DONNÉES PERSONNELLES](/rapport-information-4544/iii)

  * [A — DE NÉCESSAIRES ADAPTATIONS](/rapport-information-4544/iii/a)

    * [1 — Modifier le montant des amendes que peut prononcer la CNIL](/rapport-information-4544/iii/a/1)

    * [2 — Mettre en place une procédure de coopération en matière de sanction avec les autorités de contrôle des États membres](/rapport-information-4544/iii/a/2)

  * [B — DES QUESTIONS RESTENT EN SUSPENS](/rapport-information-4544/iii/b)

    * [1 — Une nécessaire clarification de certaines notions](/rapport-information-4544/iii/b/1)

    * [2 — Les règles spécifiques à certains types de traitements](/rapport-information-4544/iii/b/2)

      * [a — Les traitements des données de santé](/rapport-information-4544/iii/b/2/a)

      * [b — Les traitements des données biométriques et génétiques](/rapport-information-4544/iii/b/2/b)

      * [c — Les traitements aux fins d’expression journalistique, artistique, universitaire et littéraire](/rapport-information-4544/iii/b/2/c)

      * [d — Les traitements de données relatives aux infractions, aux condamnations et aux mesures de sûreté](/rapport-information-4544/iii/b/2/d)

      * [e — Les traitements portant sur le numéro d’identification national](/rapport-information-4544/iii/b/2/e)

      * [f — Les traitements des données personnelles à des fins archivistiques, de recherche scientifique ou historique ou à des fins statistiques](/rapport-information-4544/iii/b/2/f)

    * [3 — Les actions de groupe](/rapport-information-4544/iii/b/3)

    * [4 — Le droit à la portabilité](/rapport-information-4544/iii/b/4)

    * [5 — Les dispositions spécifiques concernant les enfants](/rapport-information-4544/iii/b/5)

* [EXAMEN EN COMMISSION](/rapport-information-4544/examen-en-commission)

* [PERSONNES ENTENDUES](/rapport-information-4544/personnes-entendues)

* [DÉPLACEMENT À BRUXELLES](/rapport-information-4544/deplacement-a-bruxelles)

* [ANNEXE N° 1 — LISTE DES RENVOIS AU DROIT NATIONAL PRÉVUS PAR LE RÈGLEMENT 2016/679](/rapport-information-4544/annexe-1)

* [ANNEXE N° 2 — RÉSOLUTION EUROPÉENNE ADOPTÉE PAR L’ASSEMBLÉE NATIONALE LE 23 MARS 2012 SUR LA PROPOSITION DE RÈGLEMENT RELATIF À LA PROTECTION DES PERSONNES PHYSIQUES À L’ÉGARD DU TRAITEMENT DES DONNÉES À CARACTÈRE PERSONNEL ET À LA LIBRE CIRCULATION DE CES DONNÉES](/rapport-information-4544/annexe-2)

* [Notes](/rapport-information-4544/notes)

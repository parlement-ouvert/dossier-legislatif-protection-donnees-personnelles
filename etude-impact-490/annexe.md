<!-- TITLE: ANNEXE -->
<!-- SUBTITLE: TABLEAU DE CONCORDANCE ENTRE LES DISPOSITIONS DE LA DIRECTIVE ET CELLES DE LA LOI DE 1978 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-v/articles-24" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/annexe-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**(actuelles ou résultant du présent projet de loi)**

<table>
<tr>
<td>

**DIRECTIVE (UE) 2016/680
DU PARLEMENT EUROPÉEN ET DU CONSEIL**

**Du 27 avril 2016**

**relative à la protection des personnes physiques
à l'égard du traitement des données à caractère personnel par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites
en la matière ou d'exécution de sanctions pénales, et à la libre circulation de ces données,
et abrogeant la décision-cadre 2008/977/JAI du Conseil**

**_(les dispositions mentionnées en italique constituent soit des marges de manœuvre pour la transposition, soit des dispositions qui seront transposées par décret)_**

</td>
<td>

**Droit interne en vigueur susceptible d’être modifié ou complété**

(citation de la disposition concernée)

Loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, ci-après dénommée « LIL »

Décret n°2005-1309 du 20 octobre 2005 pris pour application de la loi 78-17 relative à l’informatique, aux fichiers et aux libertés

</td>
<td>

**Dispositions nouvelles résultant des articles 20 à 22 du projet de loi**

</td>
<td>

**Observations**

</td></tr>
<tr>
<td>

**CHAPITRE I
DISPOSITIONS GÉNÉRALES**

**_Article premier
Objet et objectifs_**

1\. La présente directive établit des règles relatives à la protection des personnes physiques à l'égard du traitement des données à caractère personnel par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces.

</td>
<td>

</td>
<td>

Le chapitre XIII de la même loi devient le chapitre XIV, et après l’article 70, il est inséré les dispositions suivantes :

_Chapitre XIII._

_**Dispositions applicables aux traitements relevant de la directive (UE) 2016/680 du 27 avril 2016**_

_Section 1._

_**Dispositions générales**_

_Art. 70-1._ Les dispositions du présent chapitre s’appliquent, le cas échéant par dérogation aux autres dispositions de la présente loi, aux traitements des données à caractère personnel mis en œuvre :

1° A des fins de prévention et de détection des infractions pénales, d’enquêtes et de poursuites en la matière ou d’exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces.

2° Par toute autorité publique compétente pour l’une des finalités énoncées au 1°, ou tout autre organisme ou entité à qui a été confié, à ces mêmes fins, l'exercice de l'autorité publique et des prérogatives de puissance publique, ci-après dénommée autorité compétente.

Ces traitements ne sont licites que si et dans la mesure où ils sont nécessaires à l'exécution d'une mission effectuée, pour les finalités énoncées au 1°, par une autorité compétente au sens du 2°, et où sont respectées les dispositions des articles 70-3 et 70-4.

Pour l’application du présent chapitre, lorsque les notions utilisées ne sont pas définies au chapitre premier de la présente loi, les définitions de l’article 4 du règlement (UE) 2016-679 sont applicables.

</td>
<td>

</td></tr>
<tr>
<td>

2\. Conformément à la présente directive, les États membres:

a) protègent les libertés et droits fondamentaux des personnes physiques, et en particulier leur droit à la protection des données à caractère personnel; et

b) veillent à ce que l'échange de données à caractère personnel par les autorités compétentes au sein de l'Union, lorsque cet échange est requis par le droit de l'Union ou le droit d'un État membre, ne soit ni limité ni interdit pour des motifs liés à la protection des personnes physiques à l'égard du traitement des données à caractère personnel.

</td>
<td>

**Titre Ier : Dispositions communes**

**Chapitre Ier : Principes et définitions**

**Art 1 er**. - L'informatique doit être au service de chaque citoyen. Son développement doit s'opérer dans le cadre de la coopération internationale. Elle ne doit porter atteinte ni à l'identité humaine, ni aux droits de l'homme, ni à la vie privée, ni aux libertés individuelles ou publiques.

Toute personne dispose du droit de décider et de contrôler les usages qui sont faits des données à caractère personnel la concernant, dans les conditions fixées par la présente loi.

</td>
<td>

</td>
<td>

Conforme

</td></tr>
<tr>
<td>

3. _La présente directive n'empêche pas les États membres de prévoir des garanties plus étendues que celles établies dans la présente directive pour la protection des droits et des libertés des personnes concernées à l'égard du traitement des données à caractère personnel par les autorités compétentes._

</td>
<td>

**Maintien des Formalités préalables à la mise en œuvre des traitements concernés par la directive**

**Article 26**

I. - Sont autorisés _par arrêté du ou des ministres compétents, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat et_ :

1° Qui intéressent la sûreté de l'Etat, la défense ou la sécurité publique ;

2 _° Ou qui ont pour objet la prévention, la recherche, la constatation ou la poursuite des infractions pénales ou l'exécution des condamnations pénales ou des mesures de sûreté_.

L'avis de la commission est publié avec l'arrêté autorisant le traitement.

II. - _Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 8 sont autorisés par décret en Conseil d'Etat pris après avis motivé et publié de la commission_ ; cet avis est publié avec le décret autorisant le traitement.

III. - Certains traitements mentionnés au I et au II peuvent être dispensés, par décret en Conseil d'Etat, de la publication de l'acte réglementaire qui les autorise ; pour ces traitements, est publié, en même temps que le décret autorisant la dispense de publication de l'acte, le sens de l'avis émis par la commission.

IV. - Pour l'application du présent article, les traitements qui répondent à une même finalité, portent sur des catégories de données identiques et ont les mêmes destinataires ou catégories de destinataires peuvent être autorisés par un acte réglementaire unique. Dans ce cas, le responsable de chaque traitement adresse à la commission un engagement de conformité de celui-ci à la description figurant dans l'autorisation.

**Article 30**

I. - Les déclarations, demandes d'autorisation et demandes d'avis adressées à la Commission nationale de l'informatique et des libertés en vertu des dispositions des sections 1 et 2 précisent :

1° L'identité et l'adresse du responsable du traitement ou, si celui-ci n'est établi ni sur le territoire national ni sur celui d'un autre Etat membre de la Communauté européenne, celle de son représentant et, le cas échéant, celle de la personne qui présente la demande ;

2° La ou les finalités du traitement, ainsi que, pour les traitements relevant des articles 25, 26 et 27, la description générale de ses fonctions ;

3° Le cas échéant, les interconnexions, les rapprochements ou toutes autres formes de mise en relation avec d'autres traitements ;

4° Les données à caractère personnel traitées, leur origine et les catégories de personnes concernées par le traitement ;

_5° La durée de conservation des informations traitées_ ;

6° Le ou les services chargés de mettre en oeuvre le traitement ainsi que, pour les traitements relevant des articles 25, 26 et 27, les catégories de personnes qui, en raison de leurs fonctions ou pour les besoins du service, ont directement accès aux données enregistrées ;

7° Les destinataires ou catégories de destinataires habilités à recevoir communication des données ;

8° La fonction de la personne ou le service auprès duquel s'exerce le droit d'accès prévu à l'article 39, ainsi que les mesures relatives à l'exercice de ce droit ;

9° Les dispositions prises pour assurer la sécurité des traitements et des données et la garantie des secrets protégés par la loi et, le cas échéant, l'indication du recours à un sous-traitant ;

10° Le cas échéant, les transferts de données à caractère personnel envisagés à destination d'un Etat non membre de la Communauté européenne, sous quelque forme que ce soit, à l'exclusion des traitements qui ne sont utilisés qu'à des fins de transit sur le territoire français ou sur celui d'un autre Etat membre de la Communauté européenne au sens des dispositions du 2° du I de l'article 5.

Les demandes d'avis portant sur les traitements intéressant la sûreté de l'Etat, la défense ou la sécurité publique peuvent ne pas comporter tous les éléments d'information énumérés ci-dessus. Un décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, fixe la liste de ces traitements et des informations que les demandes d'avis portant sur ces traitements doivent comporter au minimum.

II. - Le responsable d'un traitement déjà déclaré ou autorisé informe sans délai la commission :

\- de tout changement affectant les informations mentionnées au I ;

\- de toute suppression du traitement.

**Article 31**

I. - La commission met à la disposition du public, dans un format ouvert et aisément réutilisable, la liste des traitements automatisés ayant fait l'objet d'une des formalités prévues par les articles 23 à 27, à l'exception de ceux mentionnés au III de l'article 26.

Cette liste précise pour chacun de ces traitements :

1° L'acte décidant la création du traitement ou la date de la déclaration de ce traitement ;

2° La dénomination et la finalité du traitement ;

3° L'identité et l'adresse du responsable du traitement ou, si celui-ci n'est établi ni sur le territoire national ni sur celui d'un autre Etat membre de la Communauté européenne, celles de son représentant ;

4° La fonction de la personne ou le service auprès duquel s'exerce le droit d'accès prévu à l'article 39 ;

5° Les catégories de données à caractère personnel faisant l'objet du traitement, ainsi que les destinataires et catégories de destinataires habilités à en recevoir communication ;

6° Le cas échéant, les transferts de données à caractère personnel envisagés à destination d'un Etat non membre de la Communauté européenne.

II. - La commission tient à la disposition du public ses avis, décisions ou recommandations.

III. - La Commission nationale de l'informatique et des libertés publie la liste des Etats dont la Commission des Communautés européennes a établi qu'ils assurent un niveau de protection suffisant à l'égard d'un transfert ou d'une catégorie de transferts de données à caractère personnel.

</td>
<td>

**Art. 70-3** _._ \- Si le traitement est mis en œuvre pour le compte de l’Etat pour au moins l’une des finalités prévues au 1° de l’article 70-1, il doit être prévu par un acte règlementaire pris conformément au I de l’article 26 et aux articles 28 à 31.

Si le traitement porte sur des données mentionnées au I de l’article 8, il est prévu par un acte règlementaire pris conformément au II de l’article 26.

</td>
<td>

Cette disposition, combinée avec le considérant 15 de la directive, sert de fondement :

Au maintien des formalités préalables à la mise en place d’un traitement en matière pénale, bien que non exigée par la directive, ces formalités représentant des garanties supplémentaires pour les droits des personnes concernées.

Au maintien des dispositions impliquant que soit précisément déterminée la durée maximale de conservation des données dans un traitement, alors que la directive ne l’exige pas dès lors que la durée de conservation n’excède pas celle nécessaire aux finalités du traitement

</td></tr>
<tr>
<td>

**_Article 2
Champ d'application_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. La présente directive s'applique au traitement de données à caractère personnel effectué par les autorités compétentes aux fins énoncées à l'article 1 _er_ , paragraphe 1.

</td>
<td>

</td>
<td>

Cf nouvel **article 70-1 susvisé**

</td>
<td>

</td></tr>
<tr>
<td>

2\. La présente directive s'applique au traitement de données à caractère personnel, automatisé en tout ou en partie, ainsi qu'au traitement non automatisé de données à caractère personnel contenues ou appelées à figurer dans un fichier.

</td>
<td>

**Art. 2**. La présente loi s'applique aux traitements automatisés de données à caractère personnel, ainsi qu'aux traitements non automatisés de données à caractère personnel contenues ou appelées à figurer dans des fichiers, à l'exception des traitements mis en œuvre pour l'exercice d'activités exclusivement personnelles, lorsque leur responsable remplit les conditions prévues à l'article 5.

</td>
<td>

</td>
<td>

Conforme

</td></tr>
<tr>
<td>

3\. La présente directive ne s'applique pas au traitement de données à caractère personnel effectué:

a) dans le cadre d'une activité qui ne relève pas du champ d'application du droit de l'Union;

b) par les institutions, organes, et organismes de l'Union.

</td>
<td>

**Article 41 LIL**

Par dérogation aux articles 39 et 40, lorsqu'un traitement intéresse la sûreté de l'Etat, la défense ou la sécurité publique, le droit d'accès s'exerce dans les conditions prévues par le présent article pour l'ensemble des informations qu'il contient.

La demande est adressée à la commission qui désigne l'un de ses membres appartenant ou ayant appartenu au Conseil d'Etat, à la Cour de cassation ou à la Cour des comptes pour mener les investigations utiles et faire procéder aux modifications nécessaires. Celui-ci peut se faire assister d'un agent de la commission. Il est notifié au requérant qu'il a été procédé aux vérifications.

Lorsque la commission constate, en accord avec le responsable du traitement, que la communication des données qui y sont contenues ne met pas en cause ses finalités, la sûreté de l'Etat, la défense ou la sécurité publique, ces données peuvent être communiquées au requérant.

Lorsque le traitement est susceptible de comprendre des informations dont la communication ne mettrait pas en cause les fins qui lui sont assignées, l'acte réglementaire portant création du fichier peut prévoir que ces informations peuvent être communiquées au requérant par le gestionnaire du fichier directement saisi.

</td>
<td>

</td>
<td>

Maintien des dispositions actuelles de la LIL pour les traitements intéressant la sûreté de l’Etat et la défense qui ne relèvent pas du droit de l’Union.

La disposition b) ne nécessite pas de transposition.

</td></tr>
<tr>
<td>

**_Article 3
Définitions_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

Aux fins de la présente directive, on entend par:

1."données à caractère personnel", toute information se rapportant à une personne physique identifiée ou identifiable (ci-après dénommée "personne concernée"); est réputée être une "personne physique identifiable" une personne physique qui peut être identifiée, directement ou indirectement, notamment par référence à un identifiant, tel qu'un nom, un numéro d'identification, des données de localisation, un identifiant en ligne, ou à un ou plusieurs éléments spécifiques propres à son identité physique, physiologique, génétique, psychique, économique, culturelle ou sociale;

2\. "traitement", toute opération ou tout ensemble d'opérations effectuées ou non à l'aide de procédés automatisés et appliquées à des données à caractère personnel ou des ensembles de données à caractère personnel, telles que la collecte, l'enregistrement, l'organisation, la structuration, la conservation, l'adaptation ou la modification, l'extraction, la consultation, l'utilisation, la communication par transmission, la diffusion ou toute autre forme de mise à disposition, le rapprochement ou l'interconnexion, la limitation, l'effacement ou la destruction;

3."limitation du traitement", le marquage de données à caractère personnel conservées en vue de limiter leur traitement futur;

4\. "profilage", toute forme de traitement automatisé de données à caractère personnel consistant à utiliser ces données à caractère personnel pour évaluer certains aspects personnels relatifs à une personne physique, notamment pour analyser ou prédire des éléments concernant le rendement au travail, la situation économique, la santé, les préférences personnelles, les intérêts, la fiabilité, le comportement, la localisation ou les déplacements de cette personne;

5\. "pseudonymisation", le traitement de données à caractère personnel de telle façon que celles-ci ne puissent plus être attribuées à une personne concernée précise sans avoir recours à des informations supplémentaires, pour autant que ces informations supplémentaires soient conservées séparément et soumises à des mesures techniques et organisationnelles afin de garantir que les données à caractère personnel ne sont pas attribuées à une personne physique identifiée ou identifiable;

6."fichier", tout ensemble structuré de données à caractère personnel accessibles selon des critères déterminés, que cet ensemble soit centralisé, décentralisé ou réparti de manière fonctionnelle ou géographique;

7\. "autorité compétente":

a) toute autorité publique compétente pour la prévention et la détection des infractions pénales, les enquêtes et les poursuites en la matière ou l'exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces; ou

b) tout autre organisme ou entité à qui le droit d'un État membre confie l'exercice de l'autorité publique et des prérogatives de puissance publique à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces;

8."responsable du traitement", l'autorité compétente qui, seule ou conjointement avec d'autres, détermine les finalités et les moyens du traitement de données à caractère personnel; lorsque les finalités et les moyens de ce traitement sont déterminés par le droit de l'Union ou le droit d'un État membre, le responsable du traitement ou les critères spécifiques applicables à sa désignation peuvent être prévus par le droit de l'Union ou le droit d'un État membre;

9\. "sous-traitant", la personne physique ou morale, l'autorité publique, le service ou un autre organisme qui traite des données à caractère personnel pour le compte du responsable du traitement ;

10\. "destinataire", la personne physique ou morale, l'autorité publique, le service ou tout autre organisme qui reçoit communication des données à caractère personnel, qu'il s'agisse ou non d'un tiers. Toutefois, les autorités publiques qui sont susceptibles de recevoir communication de données à caractère personnel dans le cadre d'une mission d'enquête particulière conformément au droit d'un État membre ne sont pas considérées comme des destinataires; le traitement de ces données par les autorités publiques en question est conforme aux règles applicables en matière de protection des données en fonction des finalités du traitement;

11\. "violation de données à caractère personnel", une violation de la sécurité entraînant, de manière accidentelle ou illicite, la destruction, la perte, l'altération, la divulgation non autorisée de données à caractère personnel transmises, conservées ou traitées d'une autre manière, ou l'accès non autorisé à de telles données;

12."données génétiques", les données à caractère personnel relatives aux caractéristiques génétiques héréditaires ou acquises d'une personne physique qui donnent des informations uniques sur la physiologie ou l'état de santé de cette personne physique et qui résultent, notamment, d'une analyse d'un échantillon biologique de la personne physique en question;

13\. "données biométriques", les données à caractère personnel résultant d'un traitement technique spécifique, relatives aux caractéristiques physiques, physiologiques ou comportementales d'une personne physique, qui permettent ou confirment son identification unique, telles que des images faciales ou des données dactyloscopiques;

14\. "données concernant la santé", les données à caractère personnel relatives à la santé physique ou mentale d'une personne physique, y compris la fourniture de soins de santé, qui révèlent des informations sur l'état de santé de cette personne;

15."autorité de contrôle", une autorité publique indépendante qui est instituée par un État membre en vertu de l'article 41;

16\. "organisation internationale", une organisation internationale et les organismes de droit public international qui en relèvent, ou tout autre organisme qui est créé par un accord entre deux pays ou plus, ou en vertu d'un tel accord.

</td>
<td>

**Art. 2.** Constitue une donnée à caractère personnel toute information relative à une personne physique identifiée ou qui peut être identifiée, directement ou indirectement, par référence à un numéro d'identification ou à un ou plusieurs éléments qui lui sont propres.

Pour déterminer si une personne est identifiable, il convient de considérer l'ensemble des moyens en vue de permettre son identification dont dispose ou auxquels peut avoir accès le responsable du traitement ou toute autre personne.

Constitue un traitement de données à caractère personnel toute opération ou tout ensemble d'opérations portant sur de telles données, quel que soit le procédé utilisé, et notamment la collecte, l'enregistrement, l'organisation, la conservation, l'adaptation ou la modification, l'extraction, la consultation, l'utilisation, la communication par transmission, diffusion ou toute autre forme de mise à disposition, le rapprochement ou l'interconnexion, ainsi que le verrouillage, l'effacement ou la destruction.

Constitue un fichier de données à caractère personnel tout ensemble structuré et stable de données à caractère personnel accessibles selon des critères déterminés.

**Art. 3. I-** Le responsable d'un traitement de données à caractère personnel est, sauf désignation expresse par les dispositions législatives ou réglementaires relatives à ce traitement, la personne, l'autorité publique, le service ou l'organisme qui détermine ses finalités et ses moyens.

**Art. 3. II** \- Le destinataire d'un traitement de données à caractère personnel est toute personne habilitée à recevoir communication de ces données autre que la personne concernée, le responsable du traitement, le sous-traitant et les personnes qui, en raison de leurs fonctions, sont chargées de traiter les données. Toutefois, les autorités légalement habilitées, dans le cadre d'une mission particulière ou de l'exercice d'un droit de communication, à demander au responsable du traitement de leur communiquer des données à caractère personnel ne constituent pas des destinataires.

**Article 35 alinéa 2** \- Toute personne traitant des données à caractère personnel pour le compte du responsable du traitement est considérée comme un sous-traitant au sens de la présente loi.

**Article 34 bis** \- I. - Le présent article s'applique au traitement des données à caractère personnel mis en œuvre dans le cadre de la fourniture au public de services de communications électroniques sur les réseaux de communications électroniques ouverts au public, y compris ceux prenant en charge les dispositifs de collecte de données et d'identification.

Pour l'application du présent article, on entend par violation de données à caractère personnel toute violation de la sécurité entraînant accidentellement ou de manière illicite la destruction, la perte, l'altération, la divulgation ou l'accès non autorisé à des données à caractère personnel faisant l'objet d'un traitement dans le cadre de la fourniture au public de services de communications électroniques. (…)

**Art. 11.** La Commission nationale de l'informatique et des libertés est une autorité administrative indépendante. Elle exerce les missions suivantes :

1° Elle informe toutes les personnes concernées et tous les responsables de traitements de leurs droits et obligations;

2° Elle veille à ce que les traitements de données à caractère personnel soient mis en œuvre conformément aux dispositions de la présente loi (…).

</td>
<td>

Cf nouvel **article 70-1 susvisé**

**Article 70-1 2° susvisé -** Par toute autorité publique compétente pour l’une des finalités énoncées au 1°, ou tout autre organisme ou entité à qui a été confié, à ces mêmes fins, l'exercice de l'autorité publique et des prérogatives de puissance publique, ci-après dénommée autorité compétente.

**Dernier alinéa de l’article 70-1 -** Pour l’application du présent chapitre, lorsque les notions utilisées ne sont pas définies au chapitre premier de la présente loi, les définitions de l’article 4 du règlement (UE) 2016-679 sont applicables.

</td>
<td>

Le droit français est conforme sur certaines définitions, telles que celles des données à caractère personnel, du traitement et du fichier. Les quelques variantes (comme les termes de structuration ou de limitation du traitement) restent de pure forme et sans conséquence sur le fond.

Une transposition, dans le nouvelarticle et 70-1, est en revanche indispensable pour les définitions nouvelles qui ne figurent pas dans la loi informatique et libertés. Cet article opère un renvoi aux définitions identiques prévues par l’article 4 du règlement UE, et reproduit la définition propre à la directive de l’autorité compétente.

</td></tr></table>

<table>
<tr>
<td>

**CHAPITRE II
PRINCIPES**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

**_Article 4
Principes relatifs au traitement des données à caractère personnel_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1Les États membres prévoient que les données à caractère personnel sont:

a)traitées de manière licite et loyale;

b) collectées pour des finalités déterminées, explicites et légitimes et ne sont pas traitées d'une manière incompatible avec ces finalités ;

c)adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont traitées;

d)exactes et, si nécessaire, tenues à jour; toutes les mesures raisonnables doivent être prises pour que les données à caractère personnel qui sont inexactes, eu égard aux finalités pour lesquelles elles sont traitées, soient effacées ou rectifiées sans tarder;

e)conservées sous une forme permettant l'identification des personnes concernées pendant une durée n'excédant pas celle nécessaire au regard des finalités pour lesquelles elles sont traitées;

f)traitées de façon à garantir une sécurité appropriée des données à caractère personnel, y compris la protection contre le traitement non autorisé ou illicite et contre la perte, la destruction ou les dégâts d'origine accidentelle, à l'aide de mesures techniques ou organisationnelles appropriées.

</td>
<td>

**Art. 6**. Un traitement ne peut porter que sur des données à caractère personnel qui satisfont aux conditions suivantes :

1° Les données sont collectées et traitées de manière loyale et licite ;

2° Elles sont collectées pour des finalités déterminées, explicites et légitimes et ne sont pas traitées ultérieurement de manière incompatible avec ces finalités. Toutefois, un traitement ultérieur de données à des fins statistiques ou à des fins de recherche scientifique ou historique est considéré comme compatible avec les finalités initiales de la collecte des données, s'il est réalisé dans le respect des principes et des procédures prévus au présent chapitre, au chapitre IV et à la section 1 du chapitre V ainsi qu'au chapitre IX et s'il n'est pas utilisé pour prendre des décisions à l'égard des personnes concernées ;

3° Elles sont adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont collectées et de leurs traitements ultérieurs ;

4° Elles sont exactes, complètes et, si nécessaire, mises à jour ; les mesures appropriées doivent être prises pour que les données inexactes ou incomplètes au regard des finalités pour lesquelles elles sont collectées ou traitées soient effacées ou rectifiées ;

5° Elles sont conservées sous une forme permettant l'identification des personnes concernées pendant une durée qui n'excède pas la durée nécessaire aux finalités pour lesquelles elles sont collectées et traitées.

**Maintien des formalités préalables (cf art premier 3° directive), notamment l’article 30 LIL susvisé -** I. - Les déclarations, demandes d'autorisation et demandes d'avis adressées à la Commission nationale de l'informatique et des libertés en vertu des dispositions des sections 1 et 2 précisent : (…)

9° Les dispositions prises pour assurer la sécurité des traitements et des données et la garantie des secrets protégés par la loi et, le cas échéant, l'indication du recours à un sous-traitant ;

**Art. 34 premier alinéa** \- Le responsable du traitement est tenu de prendre toutes précautions utiles, au regard de la nature des données et des risques présentés par le traitement, pour préserver la sécurité des données et, notamment, empêcher qu'elles soient déformées, endommagées, ou que des tiers non autorisés y aient accès. (…)

</td>
<td>

**_Section 2. - Obligations incombant aux autorités compétentes et aux responsables de traitements_**

**Art. 70-11**. _-_ Les autorités compétentes prennent toutes les mesures raisonnables pour garantir que les données à caractère personnel qui sont inexactes, incomplètes ou ne sont plus à jour soient effacées ou rectifiées sans tarder ou ne soient pas transmises ou mises à disposition. A cette fin, chaque autorité compétente vérifie, dans la mesure du possible, la qualité des données à caractère personnel avant leur transmission ou mise à disposition.

Dans la mesure du possible, lors de toute transmission de données à caractère personnel, sont ajoutées des informations nécessaires permettant à l'autorité compétente destinataire de juger de l'exactitude, de l'exhaustivité, et de la fiabilité des données à caractère personnel, et de leur niveau de mise à jour.

S'il s'avère que des données à caractère personnel inexactes ont été transmises ou que des données à caractère personnel ont été transmises de manière illicite, le destinataire en est informé sans retard. Dans ce cas, les données à caractère personnel sont rectifiées ou effacées ou leur traitement est limité conformément à l'article 70-20.

**Art. 70-20** _. -_ I. - La personne concernée a le droit d'obtenir du responsable du traitement :

1° Que soit rectifiées dans les meilleurs délais des données à caractère personnel la concernant qui sont inexactes ;

2° Que soient complétées des données à caractère personnel la concernant incomplètes, y compris en fournissant à cet effet une déclaration complémentaire ;

3° Que soit effacées dans les meilleurs délais des données à caractère personnel la concernant lorsque le traitement est réalisé en violation des dispositions de la présente loi ou lorsque ces données doivent être effacées pour respecter une obligation légale à laquelle est soumis le responsable du traitement.

II. - Lorsque l'intéressé en fait la demande, le responsable du traitement doit justifier qu'il a procédé aux opérations exigées en vertu du I. (…)

</td>
<td>

Le droit français est conforme pour la plupart des dispositions.

Le traitement sécurisé des données est assuré par le maintien des formalités préalables (cf art premier 3° de la directive), ainsi que par l’article 34 de la LIL. Des dispositions particulières sur la protection des données font par ailleurs l’objet d’une transposition (cf infra art. 19, 20 et 29 de la directive).

La nécessité de rectification d’informations erronées « sans tarder » mentionnée au d) est transposée dans le nouvel article 70-11 pour les autorités compétentes, et à travers la notion de « meilleurs délais » reprise de l’article 16 de la directive dans l’article 70-20 s’agissant du responsable du traitement.

</td></tr>
<tr>
<td>

2\. Le traitement, par le même ou par un autre responsable du traitement, pour l'une des finalités énoncées à l'article 1 _er_ , paragraphe 1, autre que celles pour lesquelles les données ont été collectées, est autorisé à condition que:

a) le responsable du traitement soit autorisé à traiter ces données à caractère personnel pour une telle finalité conformément au droit de l'Union ou au droit d'un État membre; et

b) le traitement soit nécessaire et proportionné à cette autre finalité conformément au droit de l'Union ou au droit d'un État membre.

3\. Le traitement des données par le même ou par un autre responsable du traitement peut comprendre l'archivage dans l'intérêt public, à des fins scientifiques, statistiques ou historiques, aux fins énoncées à l'article 1 _er_ , paragraphe 1, sous réserve de garanties appropriées pour les droits et libertés de la personne concernée.

</td>
<td>

**Art. 6 susvisé** \- **2°** [Les données à caractère personnel] sont collectées pour des finalités déterminées, explicites et légitimes et ne sont pas traitées ultérieurement de manière incompatible avec ces finalités.

Toutefois, un traitement ultérieur de données à des fins statistiques ou à des fins de recherche scientifique ou historique est considéré comme compatible avec les finalités initiales de la collecte des données, s'il est réalisé dans le respect des principes et des procédures prévus au présent chapitre, au chapitre IV et à la section 1 du chapitre V ainsi qu'au chapitre IX et s'il n'est pas utilisé pour prendre des décisions à l'égard des personnes concernées ;

**Art. 36** actuel - Les données à caractère personnel ne peuvent être conservées au-delà de la durée prévue au 5° de l'article 6 qu'en vue d'être traitées à des fins historiques, statistiques ou scientifiques; le choix des données ainsi conservées est opéré dans les conditions prévues à l'article L. 212-3 du code du patrimoine.

Les traitements dont la finalité se limite à assurer la conservation à long terme de documents d'archives dans le cadre du livre II du même code sont dispensés des formalités préalables à la mise en œuvre des traitements prévues au chapitre IV de la présente loi.

Il peut être procédé à un traitement ayant des finalités autres que celles mentionnées au premier alinéa :

-soit avec l'accord exprès de la personne concernée ou en vertu de ses directives, formulées dans les conditions définies à l'article 40-1 ;

-soit avec l'autorisation de la Commission nationale de l'informatique et des libertés ;

-soit dans les conditions prévues au 8° du II et au IV de l'article 8 s'agissant de données mentionnées au I de ce même article.

</td>
<td>

**Art. 70-6** _. -_ Les traitements effectués pour l’une des finalités énoncées au 1° de l’article 70-1 autre que celles pour lesquelles les données ont été collectées sont autorisés sous réserve du respect des principes prévus au chapitre I er de la présente loi et au présent chapitre.

Ces traitements peuvent comprendre l’archivage dans l’intérêt public, à des fins scientifiques, statistiques ou historiques, aux fins énoncées à l’article 70-1.

**Art. 70-7.** \- Les traitements à des fins archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques sont mis en œuvre dans les conditions de l’article 36 de la présente loi.

**Article 36 LIL modifié par l’article 12 du PJL** \- Les données à caractère personnel ne peuvent être conservées au-delà de la durée prévue au 5° de l'article 6 qu'en vue d'être traitées à des fins archivistiques dans l'intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques; le choix des données ainsi conservées est opéré dans les conditions prévues à l'article L. 212-3 du code du patrimoine.

Il peut être procédé à un traitement ayant des finalités autres que celles mentionnées au premier alinéa :

-soit avec l'accord exprès de la personne concernée ou en vertu de ses directives, formulées dans les conditions définies à l'article 40-1 ;

-soit dans les conditions prévues au 8° du II et au IV de l'article 8 s'agissant de données mentionnées au I de ce même article.

Lorsque les traitements de données à caractère personnel sont mis en œuvre par les services publics d'archives à des fins archivistiques dans l'intérêt public conformément à l'article L. 211-2 du code du patrimoine, les droits visés aux articles 15, 16, 18, 19, 20 et 21 du règlement (UE) 2016/679 ne s'appliquent pas dans la mesure où ces droits rendent impossible ou entravent sérieusement la réalisation des finalités spécifiques et où de telles dérogations sont nécessaires pour atteindre ces finalités. Les conditions et garanties appropriées prévues à l'article 89 du règlement (UE) 2016/679 sont déterminées par le code du patrimoine et les autres dispositions législatives et réglementaires applicables aux archives publiques. Elles sont également assurées par le respect des normes conformes à l'état de l'art en matière d'archivage électronique.

</td>
<td>

</td></tr>
<tr>
<td>

4\. Le responsable du traitement est responsable du respect des paragraphes 1, 2 et 3 et est en mesure de démontrer que ces dispositions sont respectées.

</td>
<td>

**Art. 30 et 34 susvisés**

</td>
<td>

**Art 70-20 susvisé**

**Art. 70-13** _. -_ I. - Afin de démontrer que le traitement est effectué conformément au présent chapitre, le responsable du traitement et le sous-traitant mettent en œuvre les mesures prévues aux paragraphes 1 et 2 de l’article 24 et aux paragraphes 1 et 2 de l’article 25 du règlement (UE) 2016/679 et celles appropriées afin de garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l'article 8.

</td>
<td>

Cf également infra articles 19, 20, 29 de la directive.

</td></tr>
<tr>
<td>

**_Article 5
Délais de conservation et d'examen_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

Les États membres prévoient que des délais appropriés sont fixés pour l'effacement des données à caractère personnel ou pour la vérification régulière de la nécessité de conserver les données à caractère personnel. Des règles procédurales garantissent le respect de ces délais.

</td>
<td>

**Art. 6 susvisé** – 5° Les données à caractère personnel] sont conservées sous une forme permettant l'identification des personnes concernées pendant une durée qui n'excède pas la durée nécessaire aux finalités pour lesquelles elles sont collectées et traitées.

**Article 30**

I. - Les déclarations, demandes d'autorisation et demandes d'avis adressées à la Commission nationale de l'informatique et des libertés en vertu des dispositions des sections 1 et 2 précisent :

(…);

5° La durée de conservation des informations traitées ;

**Art. 36 susvisé -** Les données à caractère personnel ne peuvent être conservées au-delà de la durée prévue au 5° de l'article 6 qu'en vue d'être traitées à des fins historiques, statistiques ou scientifiques; le choix des données ainsi conservées est opéré dans les conditions prévues à l'article L. 212-3 du code du patrimoine.

(…)

**Art. L. 212-3 du code du patrimoine** \- Lorsque les archives publiques comportent des données à caractère personnel collectées dans le cadre de traitements régis par la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, ces données font l'objet, à l'expiration de la durée prévue au 5° de l'article 6 de ladite loi, d'une sélection pour déterminer les données destinées à être conservées et celles, dépourvues d'utilité administrative ou d'intérêt scientifique, statistique ou historique, destinées à être éliminées.

Les catégories de données destinées à l'élimination ainsi que les conditions de cette élimination sont fixées par accord entre l'autorité qui a produit ou reçu ces données et l'administration des archives.

Lors des formalités préalables, il est par ailleurs prévu :

**Art. 30 - I**. - Les déclarations, demandes d'autorisation et demandes d'avis adressées à la Commission nationale de l'informatique et des libertés en vertu des dispositions des sections 1 et 2 précisent (…)

5° La durée de conservation des informations traitées ;

</td>
<td>

**Art. 36 premier alinéa modifié par l’article 12 du PJL -** Les données à caractère personnel ne peuvent être conservées au-delà de la durée prévue au 5° de l'article 6 qu'en vue d'être traitées à des fins archivistiques dans l'intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques ; le choix des données ainsi conservées est opéré dans les conditions prévues à l'article L. 212-3 du code du patrimoine.

</td>
<td>

Conforme

L’article 30 de la LIL exige même que des durées maximales de conservation soient toujours déterminées.

</td></tr>
<tr>
<td>

**_Article 6
Distinction entre différentes catégories de personnes concernées_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

Les États membres prévoient que le responsable du traitement établit, le cas échéant et dans la mesure du possible, une distinction claire entre les données à caractère personnel de différentes catégories de personnes concernées, telles que:

a) les personnes à l'égard desquelles il existe des motifs sérieux de croire qu'elles ont commis ou sont sur le point de commettre une infraction pénale;

b) les personnes reconnues coupables d'une infraction pénale;

c) les victimes d'une infraction pénale ou les personnes à l'égard desquelles certains faits portent à croire qu'elles pourraient être victimes d'une infraction pénale; et

d) les tiers à une infraction pénale, tels que les personnes pouvant être appelées à témoigner lors d'enquêtes en rapport avec des infractions pénales ou des procédures pénales ultérieures, des personnes pouvant fournir des informations sur des infractions pénales, ou des contacts ou des associés de l'une des personnes visées aux points a) et b).

</td>
<td>

</td>
<td>

**Art. 70-12**. - Le responsable du traitement établit dans la mesure du possible et le cas échéant une distinction claire entre les données à caractère personnel de différentes catégories de personnes concernées, telles que:

1° Les personnes à l'égard desquelles il existe des motifs sérieux de croire qu'elles ont commis ou sont sur le point de commettre une infraction pénale;

2° Les personnes reconnues coupables d'une infraction pénale;

3° Les victimes d'une infraction pénale ou les personnes à l'égard desquelles certains faits portent à croire qu'elles pourraient être victimes d'une infraction pénale;

4° Les tiers à une infraction pénale, tels que les personnes pouvant être appelées à témoigner lors d'enquêtes en rapport avec des infractions pénales ou des procédures pénales ultérieures, des personnes pouvant fournir des informations sur des infractions pénales, ou des contacts ou des associés de l'une des personnes visées aux 1° et 2°.

</td>
<td>

L’article 70-12 reprend intégralement les dispositions nouvelles de la directive.

</td></tr>
<tr>
<td>

**_Article 7
Distinction entre les données à caractère personnel et vérification de la qualité des données à caractère personnel_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les États membres prévoient que les données à caractère personnel fondées sur des faits sont, dans la mesure du possible, distinguées de celles fondées sur des appréciations personnelles.

</td>
<td>

</td>
<td>

**Art. 70-8** - **** Les données à caractère personnel fondées sur des faits sont dans la mesure du possible distinguées de celles fondées sur des appréciations personnelles.

</td>
<td>

</td></tr>
<tr>
<td>

2\. Les États membres prévoient que les autorités compétentes prennent toutes les mesures raisonnables pour garantir que les données à caractère personnel qui sont inexactes, incomplètes ou ne sont plus à jour ne soient pas transmises ou mises à disposition. À cette fin, chaque autorité compétente vérifie, dans la mesure du possible, la qualité des données à caractère personnel avant leur transmission ou mise à disposition. Dans la mesure du possible, lors de toute transmission de données à caractère personnel, sont ajoutées des informations nécessaires permettant à l'autorité compétente destinataire de juger de l'exactitude, de l'exhaustivité, et de la fiabilité des données à caractère personnel, et de leur niveau de mise à jour.

</td>
<td>

</td>
<td>

**_Section 2. - Obligations incombant aux autorités compétentes et aux responsables de traitements_**

**Art. 70-11 susvisé**. _-_ Les autorités compétentes prennent toutes les mesures raisonnables pour garantir que les données à caractère personnel qui sont inexactes, incomplètes ou ne sont plus à jour soient effacées ou rectifiées sans tarder ou ne soient pas transmises ou mises à disposition. A cette fin, chaque autorité compétente vérifie, dans la mesure du possible, la qualité des données à caractère personnel avant leur transmission ou mise à disposition.

Dans la mesure du possible, lors de toute transmission de données à caractère personnel, sont ajoutées des informations nécessaires permettant à l'autorité compétente destinataire de juger de l'exactitude, de l'exhaustivité, et de la fiabilité des données à caractère personnel, et de leur niveau de mise à jour.

S'il s'avère que des données à caractère personnel inexactes ont été transmises ou que des données à caractère personnel ont été transmises de manière illicite, le destinataire en est informé sans retard. Dans ce cas, les données à caractère personnel sont rectifiées ou effacées ou leur traitement est limité conformément à l'article 70-20.

</td>
<td>

La LIL prévoit le droit pour la personne concernée de solliciter la mise à jour de ses données personnelles auprès du responsable du traitement, suivi le cas échéant d’une obligation pour ce dernier d’informer le tiers, à qui les données initiales avaient été transmises, de ces modifications.

L’article 70-11 transpose donc l’obligation nouvelle expressément mise à la charge des autorités compétentes de veiller également à la qualité des données transmises, qui est prévue par la directive.

</td></tr>
<tr>
<td>

3\. S'il s'avère que des données à caractère personnel inexactes ont été transmises ou que des données à caractère personnel ont été transmises de manière illicite, le destinataire en est informé sans retard. Dans ce cas, les données à caractère personnel sont rectifiées ou effacées ou leur traitement est limité conformément à l'article 16.

</td>
<td>

**Art. 40 alinéa 5 LIL susvisé** \- Si une donnée a été transmise à un tiers, le responsable du traitement doit accomplir les diligences utiles afin de lui notifier les opérations qu'il a effectuées conformément au premier alinéa.

_**Art. 99 décret** \- Lorsque des données à caractère personnel ont été transmises à un tiers, le responsable du traitement qui a procédé à leur rectification en informe sans délai ce tiers. Celui-ci procède également sans délai à la rectification._

</td>
<td>

**Dernier alinéa de l’article 70-11 susvisé**

**Art. 70-20**

V. Le responsable du traitement communique la rectification des données à caractère personnel inexactes à l'autorité compétente dont elles proviennent.

VI. Lorsque des données à caractère personnel ont été rectifiées ou effacées ou que le traitement a été limité au titre des I, II et III, le responsable du traitement le notifie aux destinataires afin que ceux-ci rectifient ou effacent les données ou limitent le traitement des données sous leur responsabilité.

</td>
<td>

</td></tr>
<tr>
<td>

**_Article 8
Licéité du traitement_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les États membres prévoient que le traitement n'est licite que si et dans la mesure où il est nécessaire à l'exécution d'une mission effectuée par une autorité compétente, pour les finalités énoncées à l'article 1 _er_ , paragraphe 1, et où il est fondé sur le droit de l'Union ou le droit d'un État membre.

</td>
<td>

</td>
<td>

**Art. 70-1 susvisé** (avant-dernier alinéa) - Ces traitements ne sont licites que si et dans la mesure où ils sont nécessaires à l'exécution d'une mission effectuée, pour les finalités énoncées au 1°, par une autorité compétente au sens du 2°, et où sont respectées les dispositions des articles 70-3 et 70-4.

</td>
<td>

</td></tr>
<tr>
<td>

2\. Une disposition du droit d'un État membre qui réglemente le traitement relevant du champ d'application de la présente directive précise au moins les objectifs du traitement, les données à caractère personnel devant faire l'objet d'un traitement et les finalités du traitement.

</td>
<td>

Maintien des formalités préalables : **art 26, 28 à 31 LIL** (cf supra art premier 3° directive)

</td>
<td>

Cf art. 70-3 susvisé

</td>
<td>

</td></tr>
<tr>
<td>

**_Article 9
Conditions spécifiques applicables au traitement_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les données à caractère personnel collectées par les autorités compétentes pour les finalités énoncées à l'article 1 er, paragraphe 1, ne peuvent être traitées à des fins autres que celles énoncées à l'article 1 _er_ , paragraphe 1, à moins qu'un tel traitement ne soit autorisé par le droit de l'Union ou le droit d'un État membre. Lorsque des données à caractère personnel sont traitées à de telles autres fins, le règlement (UE) 2016/679 s'applique, à moins que le traitement ne soit effectué dans le cadre d'une activité ne relevant pas du champ d'application du droit de l'Union.

2\. Lorsque les autorités compétentes sont chargées par le droit d'un État membre d'exécuter des missions autres que celles exécutées pour les finalités énoncées à l'article 1 _er_ , paragraphe 1, le règlement (UE) 2016/679 s'applique au traitement effectué à de telles fins, y compris à des fins archivistiques dans l'intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques, à moins que le traitement ne soit effectué dans le cadre d'une activité ne relevant pas du champ d'application du droit de l'Union.

3\. Les États membres prévoient que, lorsque le droit de l'Union ou le droit d'un État membre applicable à l'autorité compétente qui transmet les données soumet le traitement à des conditions spécifiques, l'autorité compétente qui transmet les données informe le destinataire de ces données à caractère personnel de ces conditions et de l'obligation de les respecter.

4\. Les États membres prévoient que l'autorité compétente qui transmet les données n'applique pas aux destinataires dans les autres États membres ou aux services, organes et organismes établis en vertu des chapitres 4 et 5 du titre V du traité sur le fonctionnement de l'Union européenne des conditions en vertu du paragraphe 3 différentes de celles applicables aux transferts de données similaires à l'intérieur de l'État membre dans lequel ladite autorité est établie.

</td>
<td>

</td>
<td>

**Art. 70-5**. - Les données à caractère personnel collectées par les autorités compétentes pour les finalités énoncées au 1° de l’article 70-1, ne peuvent être traitées pour d’autres finalités, à moins qu'un tel traitement ne soit autorisé par des dispositions législatives ou réglementaires, ou par le droit de l'Union européenne. Lorsque des données à caractère personnel sont traitées à de telles autres fins, le règlement (UE) 2016/679 s'applique, à moins que le traitement ne soit effectué dans le cadre d'une activité ne relevant pas du champ d'application du droit de l'Union européenne.

Lorsque les autorités compétentes sont chargées d'exécuter des missions autres que celles exécutées pour les finalités énoncées au 1° de l’article 70-1, le règlement (UE) 2016/679 s'applique au traitement effectué à de telles fins, y compris à des fins archivistiques dans l'intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques, à moins que le traitement ne soit effectué dans le cadre d'une activité ne relevant pas du champ d'application du droit de l'Union européenne.

Si le traitement est soumis à des conditions spécifiques, l'autorité compétente qui transmet les données informe le destinataire de ces données à caractère personnel de ces conditions et de l'obligation de les respecter.

L'autorité compétente qui transmet les données n'applique pas aux destinataires dans les autres États membres ou aux services, organes et organismes établis en vertu des chapitres 4 et 5 du titre V du traité sur le fonctionnement de l'Union européenne des conditions en vertu du paragraphe 3 différentes de celles applicables aux transferts de données similaires à l'intérieur de l'État membre dont relève l'autorité compétente qui transmet les données.

</td>
<td>

Les « conditions spécifiques » sont explicitées dans le considérant 36 de la directive.

</td></tr>
<tr>
<td>

**_Article 10_**

**_
Traitement portant sur des catégories particulières de données à caractère personnel_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

Le traitement des données à caractère personnel qui révèlent l'origine raciale ou ethnique, les opinions politiques, les convictions religieuses ou philosophiques, ou l'appartenance syndicale, et le traitement des données génétiques, des données biométriques aux fins d'identifier une personne physique de manière unique, des données concernant la santé ou des données concernant la vie sexuelle ou l'orientation sexuelle d'une personne physique est autorisé uniquement en cas de nécessité absolue, sous réserve de garanties appropriées pour les droits et libertés de la personne concernée, et uniquement:

a) lorsqu'ils sont autorisés par le droit de l'Union **** ou le droit d'un État membre;

b) pour protéger les intérêts vitaux de la personne concernée ou d'une autre personne physique; ou

c) lorsque le traitement porte sur des données manifestement rendues publiques par la personne concernée.

</td>
<td>

**Article 8 actuel**

I. - Il est interdit de collecter ou de traiter des données à caractère personnel qui font apparaître, directement ou indirectement, les origines raciales ou ethniques, les opinions politiques, philosophiques ou religieuses ou l'appartenance syndicale des personnes, ou qui sont relatives à la santé ou à la vie sexuelle de celles-ci.

II. - Dans la mesure où la finalité du traitement l'exige pour certaines catégories de données, ne sont pas soumis à l'interdiction prévue au I :

1° Les traitements pour lesquels la personne concernée a donné son consentement exprès, sauf dans le cas où la loi prévoit que l'interdiction visée au I ne peut être levée par le consentement de la personne concernée ;

2° Les traitements nécessaires à la sauvegarde de la vie humaine, mais auxquels la personne concernée ne peut donner son consentement par suite d'une incapacité juridique ou d'une impossibilité matérielle ;

3° Les traitements mis en œuvre par une association ou tout autre organisme à but non lucratif et à caractère religieux, philosophique, politique ou syndical :

\- pour les seules données mentionnées au I correspondant à l'objet de ladite association ou dudit organisme ;

\- sous réserve qu'ils ne concernent que les membres de cette association ou de cet organisme et, le cas échéant, les personnes qui entretiennent avec celui-ci des contacts réguliers dans le cadre de son activité ;

\- et qu'ils ne portent que sur des données non communiquées à des tiers, à moins que les personnes concernées n'y consentent expressément ;

4° Les traitements portant sur des données à caractère personnel rendues publiques par la personne concernée ;

5° Les traitements nécessaires à la constatation, à l'exercice ou à la défense d'un droit en justice ;

6° Les traitements nécessaires aux fins de la médecine préventive, des diagnostics médicaux, de l'administration de soins ou de traitements, ou de la gestion de services de santé et mis en œuvre par un membre d'une profession de santé, ou par une autre personne à laquelle s'impose en raison de ses fonctions l'obligation de secret professionnel prévue par l'article 226-13 du code pénal ;

7° Les traitements statistiques réalisés par l'Institut national de la statistique et des études économiques ou l'un des services statistiques ministériels dans le respect de la loi n° 51-711 du 7 juin 1951 sur l'obligation, la coordination et le secret en matière de statistiques, après avis du Conseil national de l'information statistique et dans les conditions prévues à l'article 25 de la présente loi ;

8° Les traitements nécessaires à la recherche, aux études et évaluations dans le domaine de la santé selon les modalités prévues au chapitre IX.

III. Si les données à caractère personnel visées au I sont appelées à faire l'objet à bref délai d'un procédé d'anonymisation préalablement reconnu conforme aux dispositions de la présente loi par la Commission nationale de l'informatique et des libertés, celle-ci peut autoriser, compte tenu de leur finalité, certaines catégories de traitements selon les modalités prévues à l'article 25. Les dispositions du chapitre IX ne sont pas applicables.

IV. - De même, ne sont pas soumis à l'interdiction prévue au I les traitements, automatisés ou non, justifiés par l'intérêt public et autorisés dans les conditions prévues au I de l'article 25 ou au II de l'article 26, soit déclarés dans les conditions prévues au V de l’article 22.

**L’article 26 prévoit par ailleurs des formalités préalables particulières :**

I. - Sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés, les traitements de données à caractère personnel mis en œuvre pour le compte de l'Etat et :

1° Qui intéressent la sûreté de l'Etat, la défense ou la sécurité publique ;

2° Ou qui ont pour objet la prévention, la recherche, la constatation ou la poursuite des infractions pénales ou l'exécution des condamnations pénales ou des mesures de sûreté.

L'avis de la commission est publié avec l'arrêté autorisant le traitement.

II. - Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 8 sont autorisés par décret en Conseil d'Etat pris après avis motivé et publié de la commission ; cet avis est publié avec le décret autorisant le traitement.(…)

</td>
<td>

**Art. 70-2** _._ \- Le traitement de données mentionnées au I de l’article 8 est possible uniquement en cas de nécessité absolue, sous réserve de garanties appropriées pour les droits et libertés de la personne concernée, et, soit s’il est prévu par un acte législatif ou règlementaire, soit s’il vise à protéger les intérêts vitaux d’une personne physique, soit s’il porte sur des données manifestement rendues publiques par la personne concernée.

**Article 8 modifié par l’article 7 du PJL, notamment en ses I et IV** –

I. Il est interdit de traiter des données à caractère personnel, qui révèlent la prétendue origine raciale ou l’origine ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l'appartenance syndicale ou de traiter des données génétiques, des données biométriques aux fins d’identifier une personne physique de manière unique, des données concernant la santé ou des données concernant vie sexuelle ou l’orientation sexuelle d’une personne physique. (…)

_IV._ \- De même, ne sont pas soumis à l’interdiction prévue au I les traitements, automatisés ou non, justifiés par l’intérêt public et autorisés dans les conditions prévues au II de l’article 26.

</td>
<td>

Par ailleurs, l’exigence d’un décret en Conseil d’Etat pour la création d’un traitement portant sur de telles données garantit la protection de ces données sensibles et la régularité du traitement (cf art. 70-3 susvisé).

</td></tr>
<tr>
<td>

**_Article 11
Décision individuelle automatisée_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les États membres prévoient que toute décision fondée exclusivement sur un traitement automatisé, y compris le profilage, qui produit des effets juridiques défavorables pour la personne concernée ou l'affecte de manière significative, est interdite, à moins qu'elle ne soit autorisée par le droit de l'Union ou le droit d'un État membre auquel le responsable du traitement est soumis et qui fournit des garanties appropriées pour les droits et libertés de la personne concernée, et au minimum le droit d'obtenir une intervention humaine de la part du responsable du traitement.

2\. Les décisions visées au paragraphe 1 du présent article ne sont pas fondées sur les catégories particulières de données à caractère personnel visées à l'article 10, à moins que des mesures appropriées pour la sauvegarde des droits et des libertés et des intérêts légitimes de la personne concernée ne soient en place.

3\. Tout profilage qui entraîne une discrimination à l'égard des personnes physiques sur la base des catégories particulières de données à caractère personnel visées à l'article 10 est interdit, conformément au droit de l'Union.

</td>
<td>

**Art. 10. actuel**

Aucune décision de justice impliquant une appréciation sur le comportement d'une personne ne peut avoir pour fondement un traitement automatisé de données à caractère personnel destiné à évaluer certains aspects de sa personnalité.

Aucune autre décision produisant des effets juridiques à l'égard d'une personne ne peut être prise sur le seul fondement d'un traitement automatisé de données destiné à définir le profil de l'intéressé ou à évaluer certains aspects de sa personnalité.

Ne sont pas regardées comme prises sur le seul fondement d'un traitement automatisé les décisions prises dans le cadre de la conclusion ou de l'exécution d'un contrat et pour lesquelles la personne concernée a été mise à même de présenter ses observations, ni celles satisfaisant les demandes de la personne concernée.

</td>
<td>

**Art. 70-9** \- Aucune décision de justice impliquant une appréciation sur le comportement d'une personne ne peut avoir pour fondement un traitement automatisé de données à caractère personnel destiné à évaluer certains aspects de sa personnalité.

Aucune autre décision produisant des effets juridiques à l'égard d'une personne ne peut être prise sur le seul fondement d'un traitement automatisé de données destiné à prévoir ou à évaluer certains aspects personnels relatifs à la personne concernée.

Tout profilage qui entraîne une discrimination à l'égard des personnes physiques sur la base des catégories particulières de données à caractère personnel visées à l'article 8 est interdit.

**Art. 10 modifié par l’article 14 du PJL** :

Aucune décision de justice impliquant une appréciation sur le comportement d'une personne ne peut avoir pour fondement un traitement automatisé de données à caractère personnel destiné à évaluer certains aspects de sa personnalité.

Outre les cas mentionnés aux _a_ et _c_ sous le 2 de l’article 22 du règlement 2016/679, aucune autre décision produisant des effets juridiques à l'égard d'une personne ne peut être prise sur le seul fondement d'un traitement automatisé de données destiné à prévoir ou à évaluer certains aspects personnels relatifs à la personne concernée, à l’exception des décisions administratives individuelles prises dans le respect de l’article L. 311-3-1 et du chapitre I er du titre Ier du livre IV du code des relations du public et de l’administration, à condition que le traitement ne porte pas sur des données mentionnées au I de l’article 8.

Pour les décisions administratives mentionnées à l’alinéa précédent, le responsable du traitement s’assure de la maîtrise du traitement algorithmique et de ses évolutions.

</td>
<td>

Le dernier alinéa de l’article 70-9 transpose le §3 de l’article 11 de la directive qui n’a pas d’équivalent dans la LIL.

Les premier et deuxième alinéas de l’article 10 de la LIL ont été repris afin de ne pas réduire les droits existants et, dans un souci de lisibilité, afin de regrouper l’ensemble des dispositions relatives aux décisions individuelles prises sur le fondement d’un traitement automatisé de données personnelles.

</td></tr></table>

<table>
<tr>
<td>

**CHAPITRE III
DROITS DE LA PERSONNE CONCERNÉE**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

**_Article 12
Communication et modalités de l'exercice des droits de la personne concernée_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les États membres prévoient que le responsable du traitement prend des mesures raisonnables pour fournir toute information visée à l'article 13 et procède à toute communication relative au traitement ayant trait à l'article 11, aux articles 14 à 18 et à l'article 31 à la personne concernée d'une façon concise, compréhensible et aisément accessible, en des termes clairs et simples. Les informations sont fournies par tout moyen approprié, y compris par voie électronique. De manière générale, le responsable du traitement fournit les informations sous la même forme que la demande.

2\. Les États membres prévoient que le responsable du traitement facilite l'exercice des droits conférés à la personne concernée par l'article 11 et les articles 14 à 18.

3\. Les États membres prévoient que le responsable du traitement informe par écrit, dans les meilleurs délais, la personne concernée des suites données à sa demande.

4\. Les États membres prévoient qu'aucun paiement n'est exigé pour fournir les informations visées à l'article 13 et pour procéder à toute communication et prendre toute mesure au titre de l'article 11, des articles 14 à 18 et de l'article 31.

Lorsque les demandes d'une personne concernée sont manifestement infondées ou excessives, notamment en raison de leur caractère répétitif, le responsable du traitement peut:

a) soit exiger le paiement de frais raisonnables qui tiennent compte des coûts administratifs supportés pour fournir les informations, procéder à la communication ou prendre les mesures demandées;

b) soit refuser de donner suite à la demande.

Il incombe au responsable du traitement de démontrer le caractère manifestement infondé ou excessif de la demande.

5\. Lorsque le responsable du traitement a des doutes raisonnables quant à l'identité de la personne physique présentant la demande visée à l'article 14 ou 16, il peut demander que lui soient fournies des informations supplémentaires nécessaires pour confirmer l'identité de la personne concernée.

</td>
<td>

**_LIL_**

**Chapitre V : Obligations incombant aux responsables de traitements et droits des personnes**

**Section 2 : Droits des personnes à l'égard des traitements de données à caractère personnel.**

**Article 38**

Toute personne physique a le droit de s'opposer, pour des motifs légitimes, à ce que des données à caractère personnel la concernant fassent l'objet d'un traitement.

Elle a le droit de s'opposer, sans frais, à ce que les données la concernant soient utilisées à des fins de prospection, notamment commerciale, par le responsable actuel du traitement ou celui d'un traitement ultérieur.

Les dispositions du premier alinéa ne s'appliquent pas lorsque le traitement répond à une obligation légale ou lorsque l'application de ces dispositions a été écartée par une disposition expresse de l'acte autorisant le traitement.

**Article 39**

I.-Toute personne physique justifiant de son identité a le droit d'interroger le responsable d'un traitement de données à caractère personnel en vue d'obtenir :

1° La confirmation que des données à caractère personnel la concernant font ou ne font pas l'objet de ce traitement ;

2° Des informations relatives aux finalités du traitement, aux catégories de données à caractère personnel traitées et aux destinataires ou aux catégories de destinataires auxquels les données sont communiquées ;

3° Le cas échéant, des informations relatives aux transferts de données à caractère personnel envisagés à destination d'un Etat non membre de la Communauté européenne ;

4° La communication, sous une forme accessible, des données à caractère personnel qui la concernent ainsi que de toute information disponible quant à l'origine de celles-ci ;

5° Les informations permettant de connaître et de contester la logique qui sous-tend le traitement automatisé en cas de décision prise sur le fondement de celui-ci et produisant des effets juridiques à l'égard de l'intéressé. Toutefois, les informations communiquées à la personne concernée ne doivent pas porter atteinte au droit d'auteur au sens des dispositions du livre Ier et du titre IV du livre III du _code de la propriété intellectuelle._ pour tirer les conséquences de ce nouveau droit à l’information.

</td></tr>
<tr>
<td>

3. _Les États membres peuvent adopter des mesures législatives visant à retarder ou limiter la fourniture des informations à la personne concernée en application du paragraphe 2, ou à ne pas fournir ces informations, dès lors et aussi longtemps qu'une mesure de cette nature constitue une mesure nécessaire et proportionnée dans une société démocratique, en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne physique concernée pour:_

_a) éviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires;_

_b) éviter de nuire à la prévention ou à la détection d'infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l'exécution de sanctions pénales;_

_c) protéger la sécurité publique;_

_d) protéger la sécurité nationale;_

_e) protéger les droits et libertés d'autrui._

</td>
<td>

</td>
<td>

**Art. 70-21**

I. Les droits de la personne physique concernée peuvent faire l’objet de restrictions selon les modalités prévues au II du présent article, dès lors et aussi longtemps qu'une telle restriction constitue une mesure nécessaire et proportionnée dans une société démocratique en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne pour :

1° éviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires;

2° éviter de nuire à la prévention ou à la détection d'infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l'exécution de sanctions pénales ;

3° protéger la sécurité publique ;

4° protéger la sécurité nationale ;

5° protéger les droits et libertés d'autrui.

Ces restrictions sont prévues par l’acte instaurant le traitement.

II. Lorsque les conditions prévues au I sont remplies, le responsable du traitement peut :

1° Retarder ou limiter la fourniture à la personne concernée des informations mentionnées au II de l’article 70-18, ou ne pas fournir ces informations ;

2° Limiter, entièrement ou partiellement, le droit d'accès de la personne concernée prévu par l’article 70-19 ;

3° Ne pas informer la personne de son refus de rectifier ou d'effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs de cette décision conformément au IV de l’article 70-20.

III. Dans les cas visés au 2° du II, le responsable du traitement informe la personne concernée, dans les meilleurs délais, de tout refus ou de toute limitation d'accès, ainsi que des motifs du refus ou de la limitation. Ces informations peuvent ne pas être fournies lorsque leur communication risque de compromettre l'un des objectifs énoncés au I. Le responsable du traitement consigne les motifs de fait ou de droit sur lesquels se fonde la décision, et met ces informations à la disposition de la Commission nationale de l’informatique et des libertés.

IV. En cas de restriction des droits de la personne concernée intervenue en application du II ou du III, le responsable du traitement informe la personne concernée de la possibilité d’exercer ses droits par l’intermédiaire de la Commission nationale de l’informatique et des libertés ou de former un recours juridictionnel.

</td>
<td>

La marge de manœuvre laissée aux Etats membres par la directive est transposée de manière générale pour tous les droits dans l’article 70-21 (cf infra).

Celle prévue pour le droit d’information est ainsi transposée dans l’article 70-19 I et II 1°.

</td></tr>
<tr>
<td>

4. _Les États membres peuvent adopter des mesures législatives afin de déterminer des catégories de traitements susceptibles de relever, dans leur intégralité ou en partie, des points a) à e) du paragraphe 3._

</td>
<td>

</td>
<td>

</td>
<td>

Pour les raisons précisées dans l’étude d’impact, le choix a été fait de ne pas faire usage de cette dérogation possible qui n’existe dans la directive que pour les droits d’information et d’accès.

</td></tr>
<tr>
<td>

**_Article 14
Droit d'accès par la personne concernée_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

Sous réserve de l'article 15, les États membres prévoient que la personne concernée a le droit d'obtenir du responsable du traitement la confirmation que des données à caractère personnel la concernant sont ou ne sont pas traitées et, lorsqu'elles le sont, l'accès auxdites données ainsi que les informations suivantes:

a) les finalités du traitement ainsi que sa base juridique;

b) les catégories de données à caractère personnel concernées;

c) les destinataires ou catégories de destinataires auxquels les données à caractère personnel ont été communiquées, en particulier les destinataires qui sont établis dans des pays tiers ou les organisations internationales;

d) lorsque cela est possible, la durée de conservation des données à caractère personnel envisagée ou, lorsque ce n'est pas possible, les critères utilisés pour déterminer cette durée;

e) l'existence du droit de demander au responsable du traitement la rectification ou l'effacement des données à caractère personnel, ou la limitation du traitement des données à caractère personnel relatives à la personne concernée;

f) le droit d'introduire une réclamation auprès de l'autorité de contrôle et les coordonnées de ladite autorité;

g) la communication des données à caractère personnel en cours de traitement, ainsi que toute information disponible quant à leur source.

</td>
<td>

**Art. 39 susvisé**

I.- Toute personne physique justifiant de son identité a le droit d'interroger le responsable d'un traitement de données à caractère personnel en vue d'obtenir :

1° La confirmation que des données à caractère personnel la concernant font ou ne font pas l'objet de ce traitement ;

2° Des informations relatives aux finalités du traitement, aux catégories de données à caractère personnel traitées et aux destinataires ou aux catégories de destinataires auxquels les données sont communiquées ;

3° Le cas échéant, des informations relatives aux transferts de données à caractère personnel envisagés à destination d'un Etat non membre de la Communauté européenne ;

4° La communication, sous une forme accessible, des données à caractère personnel qui la concernent ainsi que de toute information disponible quant à l'origine de celles-ci ;

5° Les informations permettant de connaître et de contester la logique qui sous-tend le traitement automatisé en cas de décision prise sur le fondement de celui-ci et produisant des effets juridiques à l'égard de l'intéressé. Toutefois, les informations communiquées à la personne concernée ne doivent pas porter atteinte au droit d'auteur au sens des dispositions du livre Ier et du titre IV du livre III du _code de la propriété intellectuelle._ et celles appropriées afin de garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l'article 8.

II- En ce qui concerne le traitement automatisé, le responsable du traitement ou le sous-traitant met en œuvre, à la suite d'une évaluation des risques, des mesures destinées à :

1° Empêcher toute personne non autorisée d'accéder aux installations utilisées pour le traitement (contrôle de l'accès aux installations) ;

2° Empêcher que des supports de données puissent être lus, copiés, modifiés ou supprimés de façon non autorisée (contrôle des supports de données) ;

3° Empêcher l'introduction non autorisée de données à caractère personnel dans le fichier, ainsi que l'inspection, la modification ou l'effacement non autorisé de données à caractère personnel enregistrées (contrôle de la conservation) ;

4° Empêcher que les systèmes de traitement automatisé puissent être utilisés par des personnes non autorisées à l'aide d'installations de transmission de données (contrôle des utilisateurs) ;

5° Garantir que les personnes autorisées à utiliser un système de traitement automatisé ne puissent accéder qu'aux données à caractère personnel sur lesquelles porte leur autorisation (contrôle de l'accès aux données) ;

6° Garantir qu'il puisse être vérifié et constaté à quelles instances des données à caractère personnel ont été ou peuvent être transmises ou mises à disposition par des installations de transmission de données (contrôle de la transmission) ;

7° Garantir qu'il puisse être vérifié et constaté a posteriori quelles données à caractère personnel ont été introduites dans les systèmes de traitement automatisé, et à quel moment et par quelle personne elles y ont été introduites (contrôle de l'introduction);

8° Empêcher que, lors de la transmission de données à caractère personnel ainsi que lors du transport de supports de données, les données puissent être lues, copiées, modifiées ou supprimées de façon non autorisée (contrôle du transport) ;

9° Garantir que les systèmes installés puissent être rétablis en cas d'interruption (restauration) ;

10° Garantir que les fonctions du système opèrent, que les erreurs de fonctionnement soient signalées (fiabilité) et que les données à caractère personnel conservées ne puissent pas être corrompues par un dysfonctionnement du système (intégrité).

</td>
<td>

L’article 70-13, qui reprend des dispositions issues des articles 19, 20, 22§1 et 29 de la directive (cf infra), précisent et complètent les dispositions de l’article 34 de la LIL relatif aux mesures à mettre en œuvre pour assurer la sécurité des données.

</td></tr>
<tr>
<td>

**_Article 20
Protection des données dès la conception et protection des données par défaut_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les États membres prévoient que, compte tenu de l'état des connaissances, des coûts de la mise en œuvre et de la nature, de la portée, du contexte et des finalités du traitement, ainsi que des risques, dont le degré de probabilité et de gravité varie, que présente le traitement pour les droits et libertés des personnes physiques, le responsable du traitement met en œuvre, tant lors de la détermination des moyens du traitement que lors du traitement proprement dit, des mesures techniques et organisationnelles appropriées, telles que la pseudonymisation, qui sont destinées à mettre en œuvre les principes relatifs à la protection des données, par exemple la minimisation des données, de façon effective et à assortir le traitement des garanties nécessaires, afin de répondre aux exigences de la présente directive et de protéger les droits des personnes concernées.

2\. Les États membres prévoient que le responsable du traitement met en œuvre les mesures techniques et organisationnelles appropriées pour garantir que, par défaut, seules les données à caractère personnel qui sont nécessaires au regard de chaque finalité spécifique du traitement sont traitées. Cette obligation s'applique à la quantité de données à caractère personnel collectées, à l'étendue de leur traitement, à leur durée de conservation et à leur accessibilité. En particulier, ces mesures garantissent que, par défaut, les données à caractère personnel ne sont pas rendues accessibles à un nombre indéterminé de personnes physiques sans l'intervention de la personne concernée.

</td>
<td>

**Art. 34 susvisé**

**_Art. 91-3 du décret_**

_Constitue une mesure de protection appropriée, au sens de l'article 34 bis de la loi du 6 janvier 1978, toute mesure technique efficace destinée à rendre les données incompréhensibles à toute personne qui n'est pas autorisée à y avoir accès._

</td>
<td>

**Art. 70-13 I susvisé**

</td>
<td>

</td></tr>
<tr>
<td>

**_Article 21
Responsables conjoints du traitement_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

_1\. Les États membres prévoient que, lorsque deux responsables du traitement ou plus déterminent conjointement les finalités et les moyens du traitement, ils sont les responsables conjoints du traitement. Les responsables conjoints du traitement définissent de manière transparente leurs obligations respectives aux fins d'assurer le respect de la présente directive, notamment en ce qui concerne l'exercice des droits de la personne concernée, et leurs obligations respectives quant à la communication des informations visées à l'article 13, par voie d'accord entre eux, sauf si et dans la mesure où leurs obligations respectives sont définies par le droit de l'Union ou le droit d'un État membre auquel les responsables du traitement sont soumis. Le point de contact pour les personnes concernées est désigné dans l'accord. Les États membres peuvent préciser lequel des responsables conjoints peut servir de point de contact unique pour que les personnes concernées puissent exercer leurs droits._

_2\. Indépendamment des termes de l'accord visé au paragraphe 1, les États membres peuvent prévoir que la personne concernée peut exercer les droits que lui confère les dispositions adoptées en vertu de la présente directive à l'égard de et contre chacun des responsables du traitement._

</td>
<td>

</td>
<td>

</td>
<td>

Ces dispositions seront transposées par décret.

Excepté la marge de manœuvre de la dernière phrase du 1°, ces dispositions sont identiques à celles de l’article 26 du règlement.

</td></tr>
<tr>
<td>

**_Article 22
Sous-traitant_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les États membres prévoient que le responsable du traitement, lorsqu'un traitement doit être effectuée pour son compte, fait uniquement appel à des sous-traitants qui présentent des garanties suffisantes quant à la mise en œuvre de mesures techniques et organisationnelles appropriées de manière à ce que le traitement réponde aux exigences de la présente directive et garantisse la protection des droits de la personne concernée.

2\. Les États membres prévoient que le sous-traitant ne recrute pas un autre sous-traitant sans l'autorisation écrite préalable, spécifique ou générale, du responsable du traitement. Dans le cas d'une autorisation écrite générale, le sous-traitant informe le responsable du traitement de tout changement prévu concernant l'ajout ou le remplacement d'autres sous-traitants, donnant ainsi au responsable du traitement la possibilité d'émettre des objections à l'encontre de ces changements.

3\. Les États membres prévoient que le traitement par un sous-traitant est régi par un contrat ou un autre acte juridique au titre du droit de l'Union ou du droit d'un État membre, qui lie le sous-traitant à l'égard du responsable du traitement et qui définit l'objet et la durée du traitement, la nature et la finalité du traitement, le type de données à caractère personnel et les catégories de personnes concernées et les obligations et les droits du responsable du traitement. __ Ce contrat ou cet autre acte juridique prévoit, notamment, que le sous-traitant:

a) n'agit que sur instruction du responsable du traitement;

_b) veille à ce que les personnes autorisées à traiter les données à caractère personnel s'engagent à respecter la confidentialité ou soient soumises à une obligation légale appropriée de confidentialité;_

_c) aide le responsable du traitement, par tout moyen approprié, à veiller au respect des dispositions relatives aux droits de la personne concernée;_

_d) selon le choix du responsable du traitement, supprime toutes les données à caractère personnel ou les renvoie au responsable du traitement au terme de la prestation des services de traitement des données, et détruit les copies existantes, à moins que le droit de l'Union ou le droit d'un État membre n'exige la conservation des données à caractère personnel;_

_e) met à la disposition du responsable du traitement toutes les informations nécessaires pour apporter la preuve du respect du présent article;_

_f) respecte les conditions visées aux paragraphes 2 et 3 pour recruter un autre sous-traitant._

4\. Le contrat ou l'autre acte juridique visé au paragraphe 3 revêt la forme écrite, y compris la forme électronique.

5\. Si, en violation de la présente directive, un sous-traitant détermine les finalités et les moyens du traitement, il est considéré comme un responsable du traitement pour ce qui concerne ce traitement.

</td>
<td>

**Art. 35** \- Les données à caractère personnel ne peuvent faire l'objet d'une opération de traitement de la part d'un sous-traitant, d'une personne agissant sous l'autorité du responsable du traitement ou de celle du sous-traitant, que sur instruction du responsable du traitement.

Toute personne traitant des données à caractère personnel pour le compte du responsable du traitement est considérée comme un sous-traitant au sens de la présente loi.

Le sous-traitant doit présenter des garanties suffisantes pour assurer la mise en œuvre des mesures de sécurité et de confidentialité mentionnées à l'article 34. Cette exigence ne décharge pas le responsable du traitement de son obligation de veiller au respect de ces mesures.

Le contrat liant le sous-traitant au responsable du traitement comporte l'indication des obligations incombant au sous-traitant en matière de protection de la sécurité et de la confidentialité des données et prévoit que le sous-traitant ne peut agir que sur instruction du responsable du traitement.

</td>
<td>

**Art. 70-10**. _-_ Les données à caractère personnel ne peuvent faire l’objet d’une opération de traitement de la part d’un sous-traitant que dans les conditions prévues aux paragraphes 1, 2, 9 et 10 de l’article 28 et à l’article 29 du règlement (UE) 2016/679 et au présent article.

Les sous-traitants doivent présenter des garanties suffisantes quant à la mise en œuvre de mesures techniques et organisationnelles appropriées de manière que le traitement réponde aux exigences du présent chapitre et garantisse la protection des droits de la personne concernée.

Le traitement par un sous-traitant est régi par un contrat ou un autre acte juridique, qui lie le sous-traitant à l'égard du responsable du traitement, définit l'objet et la durée du traitement, la nature et la finalité du traitement, le type de données à caractère personnel et les catégories de personnes concernées, et les obligations et les droits du responsable du traitement, et qui prévoit que le sous-traitant n’agit que sur instruction du responsable de traitement. Le contenu de ce contrat ou acte juridique est précisé par décret en Conseil d’Etat pris après avis de la Commission nationale de l'informatique et des libertés.

</td>
<td>

L’article 70-10 vient compléter l’article 35 de la LIL afin de transposer les dispositions nouvelles relatives au sous-traitant.

Les précisions relatives au contenu du contrat entre le responsable du traitement et le sous-traitant figureront dans le décret d’application.

</td></tr>
<tr>
<td>

**_Article 23
Traitement effectué sous l'autorité du responsable du traitement ou du sous-traitant_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

Les États membres prévoient que le sous-traitant et toute personne agissant sous l'autorité du responsable du traitement ou sous celle du sous-traitant, qui a accès à des données à caractère personnel, ne les traite que sur instruction du responsable du traitement, à moins d'y être obligé par le droit de l'Union ou le droit d'un État membre.

</td>
<td>

**Art. 35 susvisé**

Les données à caractère personnel ne peuvent faire l'objet d'une opération de traitement de la part d'un sous-traitant, d'une personne agissant sous l'autorité du responsable du traitement ou de celle du sous-traitant, que sur instruction du responsable du traitement.

(…)

Le contrat liant le sous-traitant au responsable du traitement comporte l'indication des obligations incombant au sous-traitant en matière de protection de la sécurité et de la confidentialité des données et prévoit que le sous-traitant ne peut agir que sur instruction du responsable du traitement.

</td>
<td>

**Art. 70-10 susvisé**

</td>
<td>

L’article 23 de la directive est transposé par le renvoi à l’article 29 du règlement.

</td></tr>
<tr>
<td>

**_Article 24
Registre des activités de traitement_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les États membres prévoient que les responsables du traitement tiennent un registre de toutes les catégories d'activités de traitement effectuées sous leur responsabilité. Ce registre comporte toutes les informations suivantes:

a) le nom et les coordonnées du responsable du traitement et, le cas échéant, du responsable conjoint du traitement et du délégué à la protection des données;

b) les finalités du traitement;

c) les catégories de destinataires auxquels les données à caractère personnel ont été ou seront communiquées, y compris les destinataires dans des pays tiers ou des organisations internationales;

d) une description des catégories de personnes concernées et des catégories de données à caractère personnel;

e) le cas échéant, le recours au profilage;

f) le cas échéant, les catégories de transferts de données à caractère personnel vers un pays tiers ou à une organisation internationale;

g) une indication de la base juridique de l'opération de traitement, y compris les transferts, à laquelle les données à caractère personnel sont destinées;

h) dans la mesure du possible, les délais prévus pour l'effacement des différentes catégories de données à caractère personnel;

i) dans la mesure du possible, une description générale des mesures de sécurité techniques et organisationnelles visées à l'article 29, paragraphe 1.

</td>
<td>

</td>
<td>

**Art. 70-14**.- Le responsable du traitement et le sous-traitant tiennent un registre des activités de traitement dans les conditions prévues aux paragraphes 1 à 4 de l’article 30 du règlement (UE) 2016/679. Ce registre contient aussi la description générale des mesures visant à garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l'article 8, l’indication de la base juridique de l’opération de traitement, y compris les transferts, à laquelle les données à caractère personnel sont destinées et, le cas échéant, le recours au profilage.

</td>
<td>

</td></tr>
<tr>
<td>

2\. Les États membres prévoient que chaque sous-traitant tient un registre de toutes les catégories d'activités de traitement effectuées pour le compte du responsable du traitement, comprenant:

a) le nom et les coordonnées du ou des sous-traitants, de chaque responsable du traitement pour le compte duquel le sous-traitant agit et, le cas échéant, du délégué à la protection des données;

b) les catégories de traitements effectués pour le compte de chaque responsable du traitement;

c) le cas échéant, les transferts de données à caractère personnel vers un pays tiers ou à une organisation internationale, lorsqu'il en est expressément chargé par le responsable du traitement, y compris l' identification de ce pays tiers ou de cette organisation internationale;

d) dans la mesure du possible, une description générale des mesures de sécurité techniques et organisationnelles visées à l'article 29, paragraphe 1.

</td>
<td>

</td>
<td>

**Art. 70-14 susvisé**

</td>
<td>

</td></tr>
<tr>
<td>

3\. Les registres visés aux paragraphes 1 et 2 se présentent sous une forme écrite, y compris la forme électronique.

Le responsable du traitement et le sous-traitant mettent ces registres à la disposition de l'autorité de contrôle, sur demande.

</td>
<td>

</td>
<td>

**Art 70-14 susvisé**

</td>
<td>

</td></tr>
<tr>
<td>

**_Article 25
Journalisation_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les États membres prévoient que des journaux sont établis au moins pour les opérations de traitement suivantes dans des systèmes de traitement automatisé: la collecte, la modification, la consultation, la communication, y compris les transferts, l'interconnexion et l'effacement. Les journaux des opérations de consultation et de communication permettent d'établir le motif, la date et l'heure de celles-ci et, dans la mesure du possible, l'identification de la personne qui a consulté ou communiqué les données à caractère personnel, ainsi que l'identité des destinataires de ces données à caractère personnel.

2\. Les journaux sont utilisés uniquement à des fins de vérification de la licéité du traitement, d'autocontrôle, de garantie de l'intégrité et de la sécurité des données à caractère personnel et à des fins de procédures pénales.

3\. Le responsable du traitement et le sous-traitant mettent les journaux à la disposition de l'autorité de contrôle, sur demande.

</td>
<td>

</td>
<td>

**Art. 70-15** _. **-**_ **** Le responsable du traitement ou son sous-traitant établit pour chaque traitement automatisé un journal des opérations de collecte, de modification, de consultation, de communication, y compris les transferts, l’interconnexion et l’effacement, portant sur de telles données.

Les journaux des opérations de consultation et de communication permettent d’en établir le motif, la date et l’heure. Ils permettent également, dans la mesure du possible, d’identifier les personnes qui consultent ou communiquent les données et leurs destinataires.

Ce journal est uniquement utilisé à des fins de vérification de la licéité du traitement, d’autocontrôle, de garantie de l’intégrité et de la sécurité des données et à des fins de procédures pénales.

Ce journal est mis à la disposition de la Commission nationale de l’informatique et des libertés à sa demande.

</td>
<td>

</td></tr>
<tr>
<td>

**_Article 26
Coopération avec l'autorité de contrôle_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

Les États membres prévoient que le responsable du traitement et le sous-traitant coopèrent avec l'autorité de contrôle, à la demande de celle-ci, dans l'exécution de ses missions.

</td>
<td>

</td>
<td>

**Art. 70-16**. - Les articles 31, 33 et 34 du règlement (UE) 2016/679 sont applicables aux traitements des données à caractère personnel relevant du présent chapitre.

</td>
<td>

Mise en conformité avec l’article 26 de la directive par le renvoi à l’article 31 du règlement.

</td></tr>
<tr>
<td>

**_Article 27
Analyse d'impact relative à la protection des données_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Lorsqu'un type de traitement, en particulier par le recours aux nouvelles technologies, et compte tenu de la nature, de la portée, du contexte et des finalités du traitement, est susceptible d'engendrer un risque élevé pour les droits et les libertés des personnes physiques, les États membres prévoient que le responsable du traitement effectue préalablement au traitement une analyse de l'impact des opérations de traitement envisagées sur la protection des données à caractère personnel.

2. _L'analyse visée au paragraphe 1 contient au moins une description générale des opérations de traitement envisagées, une évaluation des risques pour les droits et libertés des personnes concernées, les mesures envisagées pour faire face à ces risques, les garanties, mesures et mécanismes de sécurité visant à assurer la protection des données à caractère personnel et à apporter la preuve du respect de la présente directive, compte tenu des droits et des intérêts légitimes des personnes concernées et des autres personnes touchées._

</td>
<td>

</td>
<td>

**Art. 70-4.** \- Si le traitement est susceptible d’engendrer un risque élevé pour les droits et les libertés des personnes physiques, notamment parce qu’il porte sur des données mentionnées au I de l’article 8, le responsable du traitement effectue une analyse d’impact relative à la protection des données à caractère personnel.

Si le traitement est mis en œuvre pour le compte de l’Etat, cette analyse d’impact est adressée à la Commission nationale de l’informatique et des libertés avec la demande d’avis prévue par l’article 30.

Dans les autres cas, le responsable du traitement ou le sous-traitant consulte la Commission nationale de l’informatique et des libertés préalablement au traitement des données à caractère personnel :

1° Soit lorsque l’analyse d’impact relative à la protection des données indique que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque ;

2° Soit lorsque le type de traitement, en particulier en raison de l’utilisation de nouveaux mécanismes, technologies ou procédures, présente des risques élevés pour les libertés et les droits des personnes concernées.

</td>
<td>

Le contenu de l’analyse d’impact sera précisé dans le décret.

</td></tr>
<tr>
<td>

**_Article 28
Consultation préalable de l'autorité de contrôle_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les États membres prévoient que le responsable du traitement ou le sous-traitant consulte l'autorité de contrôle préalablement au traitement des données à caractère personnel qui fera partie d'un nouveau fichier à créer:

a) lorsqu'une analyse d'impact relative à la protection des données, telle qu'elle est prévue à l'article 27, indique que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque; ou

b) lorsque le type de traitement, en particulier, en raison de l'utilisation de nouveaux mécanismes, technologies ou procédures, présente des risques élevés pour les libertés et les droits des personnes concernées.

2\. Les États membres prévoient que l'autorité de contrôle est consultée dans le cadre de l'élaboration d'une proposition de mesure législative devant être adoptée par un parlement national ou d'une mesure réglementaire fondée sur une telle mesure législative qui se rapporte au traitement.

3\. Les États membres prévoient que l'autorité de contrôle peut établir une liste des opérations de traitement devant faire l'objet d'une consultation préalable conformément au paragraphe 1.

4\. Les Etats membres prévoient que le responsable du traitement fournit à l’autorité de contrôle l’analyse d’impact relative à la protection des données en vertu de l’article 27 et, sur demande, toute autre information afin de permettre à l’autorité de contrôle d’apprécier la conformité du traitement et, en particulier, les risques pour la protection des données à caractère personnel de la personne concernée et les garanties qui s’y rapportent.

_5\. Les États membres prévoient que, lorsque l'autorité de contrôle est d'avis que le traitement prévu, visé au paragraphe 1 du présent article, constituerait une violation des dispositions adoptées en vertu de la présente directive, en particulier lorsque le responsable du traitement n'a pas suffisamment identifié ou atténué le risque, l'autorité de contrôle fournit par écrit, dans un délai maximum de six semaines à compter de la réception de la demande de consultation, un avis écrit au responsable du traitement , et le cas échéant au sous-traitant, et elle peut faire usage des pouvoirs visés à l'article 47. Ce délai peut être prolongé d'un mois, en fonction de la complexité du traitement prévu. L'autorité de contrôle informe le responsable du traitement et, le cas échéant, le sous-traitant de toute prorogation dans un délai d'un mois à compter de la réception de la demande de consultation, ainsi que des motifs du retard._

</td>
<td>

Maintien des formalités préalables : **art. 26, et 28 à 31 LIL** susvisés

</td>
<td>

**Art. 70-4 susvisé**

**Article 1 er du PJL**

L’article 11 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés est ainsi modifié : (…)

9° Après le _h_ du 2°, il est inséré un _i_ ainsi rédigé :

_i)_ Elle peut établir une liste des traitements susceptibles de créer un risque élevé devant faire l’objet d’une consultation préalable conformément à l’article 70-4 » ; (…)

</td>
<td>

</td></tr>
<tr>
<td>

**SECTION 2
SÉCURITÉ DES DONNÉES**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

**_Article 29
Sécurité du traitement_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les États membres prévoient que, compte tenu de l'état des connaissances, des coûts de la mise en œuvre et de la nature, de la portée, du contexte et des finalités du traitement ainsi que des risques, dont le degré de probabilité et de gravité varie, pour les droits et libertés des personnes physiques, le responsable du traitement et le sous-traitant mettent en œuvre les mesures techniques et organisationnelles appropriées afin de garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l'article 10.

</td>
<td>

**Art. 8 et 34 LIL susvisés**

**_Art. 91-3 décret susvisé_**

</td>
<td>

**Art. 70-13 I susvisé** _-_ I. - Afin de démontrer que le traitement est effectué conformément au présent chapitre, le responsable du traitement et le sous-traitant mettent en œuvre les mesures prévues aux paragraphes 1 et 2 de l’article 24 et aux paragraphes 1 et 2 de l’article 25 du règlement (UE) 2016/679 et celles appropriées afin de garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l'article 8.

</td>
<td>

</td></tr>
<tr>
<td>

2\. En ce qui concerne le traitement automatisé, chaque État membre prévoit que le responsable du traitement ou le sous-traitant met en œuvre, à la suite d'une évaluation des risques, des mesures destinées à:

a) empêcher toute personne non autorisée d'accéder aux installations utilisées pour le traitement (contrôle de l'accès aux installations);

b) empêcher que des supports de données puissent être lus, copiés, modifiés ou supprimés de façon non autorisée (contrôle des supports de données);

c) empêcher l'introduction non autorisée de données à caractère personnel dans le fichier, ainsi que l'inspection, la modification ou l'effacement non autorisé de données à caractère personnel enregistrées (contrôle de la conservation);

d) empêcher que les systèmes de traitement automatisé puissent être utilisés par des personnes non autorisées à l'aide d'installations de transmission de données (contrôle des utilisateurs);

e) garantir que les personnes autorisées à utiliser un système de traitement automatisé ne puissent accéder qu'aux données à caractère personnel sur lesquelles porte leur autorisation (contrôle de l'accès aux données);

f) garantir qu'il puisse être vérifié et constaté à quelles instances des données à caractère personnel ont été ou peuvent être transmises ou mises à disposition par des installations de transmission de données (contrôle de la transmission);

g) garantir qu'il puisse être vérifié et constaté a posteriori quelles données à caractère personnel ont été introduites dans les systèmes de traitement automatisé, et à quel moment et par quelle personne elles y ont été introduites (contrôle de l'introduction);

h) empêcher que, lors de la transmission de données à caractère personnel ainsi que lors du transport de supports de données, les données puissent être lues, copiées, modifiées ou supprimées de façon non autorisée (contrôle du transport);

i) garantir que les systèmes installés puissent être rétablis en cas d'interruption (restauration);

j) garantir que les fonctions du système opèrent, que les erreurs de fonctionnement soient signalées (fiabilité) et que les données à caractère personnel conservées ne puissent pas être corrompues par un dysfonctionnement du système (intégrité).

</td>
<td>

</td>
<td>

**Art. 70-13 II susvisé** –

II- En ce qui concerne le traitement automatisé, le responsable du traitement ou le sous-traitant met en œuvre, à la suite d'une évaluation des risques, des mesures destinées à :

1° Empêcher toute personne non autorisée d'accéder aux installations utilisées pour le traitement (contrôle de l'accès aux installations) ;

2° Empêcher que des supports de données puissent être lus, copiés, modifiés ou supprimés de façon non autorisée (contrôle des supports de données) ;

3° Empêcher l'introduction non autorisée de données à caractère personnel dans le fichier, ainsi que l'inspection, la modification ou l'effacement non autorisé de données à caractère personnel enregistrées (contrôle de la conservation) ;

4° Empêcher que les systèmes de traitement automatisé puissent être utilisés par des personnes non autorisées à l'aide d'installations de transmission de données (contrôle des utilisateurs) ;

5° Garantir que les personnes autorisées à utiliser un système de traitement automatisé ne puissent accéder qu'aux données à caractère personnel sur lesquelles porte leur autorisation (contrôle de l'accès aux données) ;

6° Garantir qu'il puisse être vérifié et constaté à quelles instances des données à caractère personnel ont été ou peuvent être transmises ou mises à disposition par des installations de transmission de données (contrôle de la transmission) ;

7° Garantir qu'il puisse être vérifié et constaté a posteriori quelles données à caractère personnel ont été introduites dans les systèmes de traitement automatisé, et à quel moment et par quelle personne elles y ont été introduites (contrôle de l'introduction);

8° Empêcher que, lors de la transmission de données à caractère personnel ainsi que lors du transport de supports de données, les données puissent être lues, copiées, modifiées ou supprimées de façon non autorisée (contrôle du transport) ;

9° Garantir que les systèmes installés puissent être rétablis en cas d'interruption (restauration) ;

10° Garantir que les fonctions du système opèrent, que les erreurs de fonctionnement soient signalées (fiabilité) et que les données à caractère personnel conservées ne puissent pas être corrompues par un dysfonctionnement du système (intégrité).

</td>
<td>

Transposition des prescriptions particulières prévues pour les traitements automatisés par l’article 29 §2 de la directive.

</td></tr>
<tr>
<td>

**_Article 30
Notification à l'autorité de contrôle d'une violation de données à caractère personnel_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les États membres prévoient qu'en cas de violation de données à caractère personnel, le responsable du traitement notifie la violation en question à l'autorité de contrôle dans les meilleurs délais _et, si possible, dans un délai de 72 heures au plus tard après en avoir pris connaissance_ , à moins qu'il soit peu probable que la violation en question n'engendre des risques pour les droits et les libertés d'une personne physique. _Lorsque la notification à l'autorité de contrôle n'a pas lieu dans les 72 heures, elle est accompagnée des motifs du retard._

2\. Le sous-traitant notifie au responsable du traitement toute violation de données à caractère personnel dans les meilleurs délais après en avoir pris connaissance.

</td>
<td>

**Art. 34 bis**

I. - Le présent article s'applique au traitement des données à caractère personnel mis en œuvre dans le cadre de la fourniture au public de services de communications électroniques sur les réseaux de communications électroniques ouverts au public, y compris ceux prenant en charge les dispositifs de collecte de données et d'identification.

Pour l'application du présent article, on entend par violation de données à caractère personnel toute violation de la sécurité entraînant accidentellement ou de manière illicite la destruction, la perte, l'altération, la divulgation ou l'accès non autorisé à des données à caractère personnel faisant l'objet d'un traitement dans le cadre de la fourniture au public de services de communications électroniques.

II. - En cas de violation de données à caractère personnel, le fournisseur de services de communications électroniques accessibles au public avertit, sans délai, la Commission nationale de l'informatique et des libertés.

Lorsque cette violation peut porter atteinte aux données à caractère personnel ou à la vie privée d'un abonné ou d'une autre personne physique, le fournisseur avertit également, sans délai, l'intéressé.

La notification d'une violation des données à caractère personnel à l'intéressé n'est toutefois pas nécessaire si la Commission nationale de l'informatique et des libertés a constaté que des mesures de protection appropriées ont été mises en œuvre par le fournisseur afin de rendre les données incompréhensibles à toute personne non autorisée à y avoir accès et ont été appliquées aux données concernées par ladite violation.

A défaut, la Commission nationale de l'informatique et des libertés peut, après avoir examiné la gravité de la violation, mettre en demeure le fournisseur d'informer également les intéressés.

III. - Chaque fournisseur de services de communications électroniques tient à jour un inventaire des violations de données à caractère personnel, notamment de leurs modalités, de leur effet et des mesures prises pour y remédier et le conserve à la disposition de la commission.

**_Article 91-1 décret_**

_La notification d'une violation des données à caractère personnel prévue au premier alinéa du II de _l'article 34 bis de la loi du 6 janvier 1978_, en ce qu’elles concernent le responsable du traitement, sont applicables aux traitements des données à caractère personnel relevant du présent chapitre.

</td>
<td>

</td></tr>
<tr>
<td>

**_Article 33
Fonction du délégué à la protection des données_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. Les États membres prévoient que le responsable du traitement veille à ce que le délégué à la protection des données soit associé, d'une manière appropriée et en temps utile, à toutes les questions relatives à la protection des données à caractère personnel.

2\. Le responsable du traitement aide le délégué à la protection des données à exercer les missions visées à l'article 34 en fournissant les ressources nécessaires pour exercer ces missions ainsi que l'accès aux données à caractère personnel et aux traitements, et lui permettant d'entretenir ses connaissances spécialisées.

</td>
<td>

</td>
<td>

**Art. 70-17 susvisé**

</td>
<td>

</td></tr>
<tr>
<td>

**_Article 34
Missions du délégué à la protection des données_**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

Les États membres prévoient que le responsable du traitement confie au délégué à la protection des données au moins les missions suivantes:

a) informer et conseiller le responsable du traitement et les employés qui procèdent au traitement sur les obligations qui leur incombent en vertu de la présente directive et d'autres dispositions du droit de l'Union ou du droit des États membres en matière de protection des données;

b) contrôler le respect de la présente directive, d'autres dispositions du droit de l'Union ou du droit États membres en matière de protection des données et des règles internes du responsable du traitement en matière de protection des données à caractère personnel, y compris en ce qui concerne la répartition des responsabilités, la sensibilisation et la formation du personnel participant à des opérations de traitement, et les audits s'y rapportant;

c) dispenser des conseils, sur demande, en ce qui concerne l'analyse d'impact relative à la protection des données et vérifier l'exécution de celle-ci en vertu de l'article 27;

d) coopérer avec l'autorité de contrôle;

e) faire office de point de contact pour l'autorité de contrôle sur les questions relatives au traitement, y compris la consultation préalable visée à l'article 28, et mener des consultations, le cas échéant, sur tout autre sujet.

</td>
<td>

</td>
<td>

**Art. 70-17 susvisé**

</td>
<td>

</td></tr></table>

<table>
<tr>
<td>

**_CHAPITRE V_**

**_Transferts de données à caractère personnel vers des pays tiers ou à des organisations internationales_**

**_Article 35_**

**Principes généraux applicables aux transferts de données à caractère personnel**

1\. Les États membres prévoient qu'un transfert, par des autorités compétentes, de données à caractère personnel qui font ou sont destinées à faire l'objet d'un traitement après leur transfert vers un pays tiers ou à une organisation internationale, y compris des transferts ultérieurs vers un autre pays tiers ou à une autre organisation internationale, n'a lieu, sous réserve du respect des dispositions nationales adoptées en application d'autres dispositions de la présente directive, que lorsque les conditions définies dans le présent chapitre sont respectées, à savoir:

a)

le transfert est nécessaire aux fins énoncées à l'article 1er, paragraphe 1;

b)

les données à caractère personnel sont transférées à un responsable du traitement dans un pays tiers ou à une organisation internationale qui est une autorité compétente aux fins visées à l'article 1er, paragraphe 1;

c)

en cas de transmission ou de mise à disposition de données à caractère personnel provenant d'un autre État membre, celui-ci a préalablement autorisé ce transfert conformément à son droit national;

d)

la Commission a adopté une décision d'adéquation en application de l'article 36, ou, en l'absence d'une telle décision, des garanties appropriées ont été prévues ou existent en application de l'article 37 ou, en l'absence de décision d'adéquation au titre de l'article 36 et de garanties appropriées conformément à l'article 37, des dérogations pour des situations particulières s'appliquent en vertu de l'article 38; et

e)

en cas de transfert ultérieur vers un autre pays tiers ou à une autre organisation internationale, l'autorité compétente qui a procédé au transfert initial ou une autre autorité compétente du même État membre autorise le transfert ultérieur, après avoir dûment pris en considération l'ensemble des facteurs pertinents, y compris la gravité de l'infraction pénale, la finalité pour laquelle les données à caractère personnel ont été transférées initialement et le niveau de protection des données à caractère personnel dans le pays tiers ou au sein de l'organisation internationale vers lequel/laquelle les données à caractère personnel sont transférées ultérieurement.

2\. Les États membres prévoient que les transferts effectués sans l'autorisation préalable d'un autre État membre prévue au paragraphe 1, point c), sont autorisés uniquement lorsque le transfert de données à caractère personnel est nécessaire aux fins de la prévention d'une menace grave et immédiate pour la sécurité publique d'un État membre ou d'un pays tiers ou pour les intérêts essentiels d'un État membre et si l'autorisation préalable ne peut pas être obtenue en temps utile. L'autorité à laquelle il revient d'accorder l'autorisation préalable est informée sans retard.

3\. Toutes les dispositions du présent chapitre sont appliquées de manière que le niveau de protection des personnes physiques assuré par la présente directive ne soit pas compromis.

</td>
<td>

**Article 68 LIL**

Le responsable d'un traitement ne peut transférer des données à caractère personnel vers un Etat n'appartenant pas à la Communauté européenne que si cet Etat assure un niveau de protection suffisant de la vie privée et des libertés et droits fondamentaux des personnes à l'égard du traitement dont ces données font l'objet ou peuvent faire l'objet.

Le caractère suffisant du niveau de protection assuré par un Etat s'apprécie en fonction notamment des dispositions en vigueur dans cet Etat, des mesures de sécurité qui y sont appliquées, des caractéristiques propres du traitement, telles que ses fins et sa durée, ainsi que de la nature, de l'origine et de la destination des données traitées.

**Article 69**

Toutefois, le responsable d'un traitement peut transférer des données à caractère personnel vers un Etat ne répondant pas aux conditions prévues à l'article 68 si la personne à laquelle se rapportent les données a consenti expressément à leur transfert ou si le transfert est nécessaire à l'une des conditions suivantes :

1° A la sauvegarde de la vie de cette personne ;

2° A la sauvegarde de l'intérêt public ;

3° Au respect d'obligations permettant d'assurer la constatation, l'exercice ou la défense d'un droit en justice ;

4° A la consultation, dans des conditions régulières, d'un registre public qui, en vertu de dispositions législatives ou réglementaires, est destiné à l'information du public et est ouvert à la consultation de celui-ci ou de toute personne justifiant d'un intérêt légitime ;

5° A l'exécution d'un contrat entre le responsable du traitement et l'intéressé, ou de mesures précontractuelles prises à la demande de celui-ci ;

6° A la conclusion ou à l'exécution d'un contrat conclu ou à conclure, dans l'intérêt de la personne concernée, entre le responsable du traitement et un tiers.

Il peut également être fait exception à l'interdiction prévue à l'article 68, par décision de la Commission nationale de l'informatique et des libertés ou, s'il s'agit d'un traitement mentionné au I ou au II de l'article 26, par décret en Conseil d'Etat pris après avis motivé et publié de la commission, lorsque le traitement garantit un niveau de protection suffisant de la vie privée ainsi que des libertés et droits fondamentaux des personnes, notamment en raison des clauses contractuelles ou règles internes dont il fait l'objet.

La Commission nationale de l'informatique et des libertés porte à la connaissance de la Commission des Communautés européennes et des autorités de contrôle des autres Etats membres de la Communauté européenne les décisions d'autorisation de transfert de données à caractère personnel qu'elle prend au titre de l'alinéa précédent.

</td>
<td>

**Art. 70-25** _._ \- Le responsable d’un traitement de données à caractère personnel ne peut transférer des données ou autoriser le transfert de données déjà transmises vers un État n'appartenant pas à l’Union européenne que lorsque les conditions suivantes sont respectées :

1° Le transfert de ces données est nécessaire à l’une des finalités énoncées au 1° de l’article 70-1 ;

2° Les données à caractère personnel sont transférées à un responsable dans cet État tiers ou à une organisation internationale qui est une autorité compétente chargée dans cet État des fins relevant en France __ du 1° de l’article 70-1 ;

3° Si les données à caractère personnel proviennent d'un autre État, l’État qui a transmis ces données a préalablement autorisé ce transfert conformément à son droit national.

Toutefois, si l'autorisation préalable ne peut pas être obtenue en temps utile, ces données à caractère personnel peuvent être retransmises sans l’autorisation préalable de l’État qui a transmis ces données lorsque cette retransmission est nécessaire à la prévention d'une menace grave et immédiate pour la sécurité publique d'un autre État ou pour la sauvegarde des intérêts essentiels de la France. L'autorité d’où provenaient ces données personnelles est informée sans retard.

4° L’une au moins des trois conditions suivantes est remplie :

_a)_ La Commission a adopté une décision d'adéquation en application de l'article 36 de la directive (UE) 2016/680 du Parlement et du Conseil du 27 avril 2016 ;

_b)_ A défaut d’une telle décision d’adéquation, des garanties appropriées en ce qui concerne la protection des données à caractère personnel sont fournies dans un instrument juridiquement contraignant ; ces garanties appropriées peuvent soit résulter des garanties relatives à la protection des données mentionnées dans les conventions mises en œuvre avec cet État tiers, soit résulter de dispositions juridiquement contraignantes exigées à l’occasion de l’échange de données ;

_c)_ A défaut d’une telle décision d’adéquation et de garanties appropriées telles que prévues au _b_ , le responsable du traitement a évalué toutes les circonstances du transfert et estime qu'il existe des garanties appropriées au regard de la protection des données à caractère personnel ;

Lorsque le responsable d’un traitement de données à caractère personnel transfère des données à caractère personnel sur le seul fondement de l’existence de garanties appropriées au regard de la protection des données à caractère personnel, autre qu’une juridiction effectuant une activité de traitement dans le cadre de ses activités juridictionnelles, il avise la Commission nationale de l’informatique et des libertés des catégories de transferts relevant de ce fondement.

Dans ce cas, le responsable du traitement des données doit garder trace de la date et l'heure du transfert, des informations sur l'autorité compétente destinataire, et de la justification du transfert et des données à caractère personnel transférées. Cette documentation est mise à la disposition de l'autorité de contrôle, sur sa demande.

Lorsque la commission a abrogé, modifié ou suspendu une décision d'adéquation adoptée en application de l'article 36 de la directive précitée, le responsable d’un traitement de données à caractère personnel peut néanmoins transférer des données personnelles ou autoriser le transfert de données déjà transmises vers un État n'appartenant pas à l’Union européenne si des garanties appropriées en ce qui concerne la protection des données à caractère personnel sont fournies dans un instrument juridiquement contraignant ou s’il estime après avoir évalué toutes les circonstances du transfert qu'il existe des garanties appropriées au regard de la protection des données à caractère personnel.

</td>
<td>

</td></tr>
<tr>
<td>

**_Article 36_**

**Transferts sur la base d'une décision d'adéquation**

1\. Les États membres prévoient qu'un transfert de données à caractère personnel vers un pays tiers ou à une organisation internationale peut avoir lieu lorsque la Commission a constaté par voie de décision que le pays tiers, un territoire ou un ou plusieurs secteurs déterminés dans ce pays tiers, ou l'organisation internationale en question assure un niveau de protection adéquat. Un tel transfert ne nécessite pas d'autorisation spécifique.

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

2\. __ Lorsqu'elle évalue le caractère adéquat du niveau de protection, la Commission tient compte en particulier des éléments suivants:

a)

l'état de droit, le respect des droits de l'homme et des libertés fondamentales, la législation pertinente, tant générale que sectorielle, y compris en ce qui concerne la sécurité publique, la défense, la sécurité nationale et le droit pénal ainsi que l'accès des autorités publiques aux données à caractère personnel, de même que la mise en œuvre de ladite législation, les règles en matière de protection des données, les règles professionnelles et les mesures de sécurité, y compris les règles relatives au transfert ultérieur de données à caractère personnel vers un autre pays tiers ou à une autre organisation internationale qui sont respectées dans le pays tiers ou par l'organisation internationale en question, la jurisprudence, ainsi que les droits effectifs et opposables dont bénéficient les personnes concernées et les recours administratifs et judiciaires que peuvent effectivement introduire les personnes concernées dont les données à caractère personnel sont transférées;

</td>
<td>

</td>
<td>

</td>
<td>

Dispositions concernant la Commission et n’ayant pas lieu d’être transposées dans la législation française.

</td></tr>
<tr>
<td>

b)

l'existence et le fonctionnement effectif d'une ou de plusieurs autorités de contrôle indépendantes dans le pays tiers, ou auxquelles une organisation internationale est soumise, chargées d'assurer le respect des règles en matière de protection des données et de les faire appliquer, y compris par des pouvoirs appropriés d'application desdites règles, d'assister et de conseiller les personnes concernées dans l'exercice de leurs droits et de coopérer avec les autorités de contrôle des États membres; et

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

c)

les engagements internationaux pris par le pays tiers ou l'organisation internationale en question, ou d'autres obligations découlant de conventions ou d'instruments juridiquement contraignants et de sa participation à des systèmes multilatéraux ou régionaux, en particulier en ce qui concerne la protection des données à caractère personnel.

3\. La Commission, après avoir évalué le caractère adéquat du niveau de protection, peut constater au moyen d'un acte d'exécution qu'un pays tiers, un territoire ou un ou plusieurs secteurs déterminés dans un pays tiers en question, ou une organisation internationale, assure un niveau de protection adéquat au sens du paragraphe 2 du présent article. L'acte d'exécution prévoit un mécanisme d'examen périodique, au moins tous les quatre ans, qui prend en compte toutes les évolutions pertinentes dans le pays tiers ou au sein de l'organisation internationale. L'acte d'exécution précise son champ d'application territorial et sectoriel et, le cas échéant, nomme la ou des autorités de contrôle visées au paragraphe 2, point b), du présent article. L'acte d'exécution est adopté en conformité avec la procédure d'examen visée à l'article 58, paragraphe 2.

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

4\. La Commission suit, de manière permanente, les évolutions dans les pays tiers et au sein des organisations internationales qui pourraient porter atteinte au fonctionnement des décisions adoptées en vertu du paragraphe 3.

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

5\. Lorsque les informations disponibles révèlent, en particulier à la suite de l'examen visé au paragraphe 3 du présent article, qu'un pays tiers, un territoire ou un ou plusieurs secteurs déterminés dans un pays tiers, ou une organisation internationale n'assure plus un niveau de protection adéquat au sens du paragraphe 2 du présent article, la Commission abroge, modifie ou suspend, si nécessaire, la décision visée au paragraphe 3 du présent article par voie d'actes d'exécution sans effet rétroactif. Ces actes d'exécution sont adoptés en conformité avec la procédure d'examen visée à l'article 58, paragraphe 2.

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

Pour des raisons d'urgence impérieuses dûment justifiées, la Commission adopte des actes d'exécution immédiatement applicables en conformité avec la procédure visée à l'article 58, paragraphe 3.

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

6\. La Commission engage des consultations avec le pays tiers ou l'organisation internationale en vue de remédier à la situation donnant lieu à la décision adoptée en vertu du paragraphe 5.

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

7 Les États membres prévoient qu'une décision adoptée en vertu du paragraphe 5 est sans préjudice des transferts de données à caractère personnel vers le pays tiers, le territoire ou un ou plusieurs secteurs déterminés dans ce pays tiers, ou à l'organisation internationale en question, effectués en application des articles 37 et 38.

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

8\. La Commission publie au _Journal officiel de l'Union européenne_ et sur son site internet une liste des pays tiers, des territoires et des secteurs déterminés dans un pays tiers et des organisations internationales pour lesquels elle a constaté par voie de décision qu'un niveau de protection adéquat est ou n'est plus assuré.

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

_Article 37_

**Transferts moyennant des garanties appropriées**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. En l'absence de décision en vertu de l'article 36, paragraphe 3, les États membres prévoient qu'un transfert de données à caractère personnel vers un pays tiers ou à une organisation internationale peut avoir lieu lorsque:

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

a)

des garanties appropriées en ce qui concerne la protection des données à caractère personnel sont fournies dans un instrument juridiquement contraignant; ou

b)

le responsable du traitement a évalué toutes les circonstances du transfert et estime qu'il existe des garanties appropriées au regard de la protection des données à caractère personnel.

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

2\. Le responsable du traitement informe l'autorité de contrôle des catégories de transferts relevant du paragraphe 1, point b).

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

3\. Lorsqu'un transfert est effectué sur la base du paragraphe 1, point b), ce transfert est documenté et la documentation est mise à la disposition de l'autorité de contrôle, sur demande, et comporte la date et l'heure du transfert, des informations sur l'autorité compétente destinataire, la justification du transfert et les données à caractère personnel transférées

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

_Article 38_

**Dérogations pour des situations particulières**

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr>
<tr>
<td>

1\. En l'absence de décision d'adéquation en vertu de l'article 36 ou de garanties appropriées en vertu de l'article 37, les États membres prévoient qu'un transfert ou une catégorie de transferts de données à caractère personnel vers un pays tiers ou à une organisation internationale ne peut avoir lieu qu'à condition que le transfert soit nécessaire:

</td>
<td>

**Article 70**

Si la Commission des Communautés européennes a constaté qu'un Etat n'appartenant pas à la Communauté européenne n'assure pas un niveau de protection suffisant à l'égard d'un transfert ou d'une catégorie de transferts de données à caractère personnel, la Commission nationale de l'informatique et des libertés, saisie d'une déclaration déposée en application des articles 23 ou 24 et faisant apparaître que des données à caractère personnel seront transférées vers cet Etat, délivre le récépissé avec mention de l'interdiction de procéder au transfert des données.

Lorsqu'elle estime qu'un Etat n'appartenant pas à la Communauté européenne n'assure pas un niveau de protection suffisant à l'égard d'un transfert ou d'une catégorie de transferts de données, la Commission nationale de l'informatique et des libertés en informe sans délai la Commission des Communautés européennes. Lorsqu'elle est saisie d'une déclaration déposée en application des articles 23 ou 24 et faisant apparaître que des données à caractère personnel seront transférées vers cet Etat, la Commission nationale de l'informatique et des libertés délivre le récépissé et peut enjoindre au responsable du traitement de suspendre le transfert des données. Si la Commission des Communautés européennes constate que l'Etat vers lequel le transfert est envisagé assure un niveau de protection suffisant, la Commission nationale de l'informatique et des libertés notifie au responsable du traitement la cessation de la suspension du transfert. Si la Commission des Communautés européennes constate que l'Etat vers lequel le transfert est envisagé n'assure pas un niveau de protection suffisant, la Commission nationale de l'informatique et des libertés notifie au responsable du traitement l'interdiction de procéder au transfert de données à caractère personnel à destination de cet Etat.

</td>
<td>

Art. 70-26. - Par dérogation aux dispositions de l’article précédent, le responsable d’un traitement de données à caractère personnel ne peut, en l’absence de décision d’adéquation ou de garanties appropriées, transférer ces données ou autoriser le transfert de données déjà transmises vers un État n'appartenant pas à l’Union européenne que lorsque le transfert est nécessaire :

</td>
<td>

</td></tr>
<tr>
<td>

a)

à la sauvegarde des intérêts vitaux de la personne concernée ou d'une autre personne;

</td>
<td>

</td>
<td>

1° A la sauvegarde des intérêts vitaux de la personne concernée ou d'une autre personne;

</td>
<td>

</td></tr>
<tr>
<td>

b)

à la sauvegarde des intérêts légitimes de la personne concernée lorsque le droit de l'État membre transférant les données à caractère personnel le prévoit;

</td>
<td>

</td>
<td>

2° A la sauvegarde des intérêts légitimes de la personne concernée lorsque le droit français le prévoit;

</td>
<td>

</td></tr>
<tr>
<td>

c)

pour prévenir une menace grave et immédiate pour la sécurité publique d'un État membre ou d'un pays tiers;

</td>
<td>

</td>
<td>

3° Pour prévenir une menace grave et immédiate pour la sécurité publique d'un État membre de l’Union européenne ou d'un pays tiers;

</td>
<td>

</td></tr>
<tr>
<td>

d)

dans des cas particuliers, aux fins énoncées à l'article 1er, paragraphe 1; ou

</td>
<td>

</td>
<td>

4° Dans des cas particuliers, à l’une des finalités énoncées au 1° de l’article 70-1 ;

</td>
<td>

</td></tr>
<tr>
<td>

e)

dans un cas particulier, à la constatation, à l'exercice ou à la défense de droits en justice en rapport avec les fins énoncées à l'article 1er, paragraphe 1.

</td>
<td>

</td>
<td>

5° Dans un cas particulier, à la constatation, à l'exercice ou à la défense de droits en justice en rapport avec les mêmes fins.

</td>
<td>

</td></tr>
<tr>
<td>

2\. Les données à caractère personnel ne sont pas transférées si l'autorité compétente qui transfère les données estime que les libertés et droits fondamentaux de la personne concernée l'emportent sur l'intérêt public dans le cadre du transfert visé au paragraphe 1, points d) et e).

</td>
<td>

</td>
<td>

Dans les cas visés aux 4° et 5°, le responsable du traitement de données à caractère personnel ne transfère pas ces données s’il estime que les libertés et droits fondamentaux de la personne concernée l'emportent sur l'intérêt public dans le cadre du transfert envisagé.

« Lorsqu’un transfert est

</td>
<td>

</td></tr>
<tr>
<td>

3\. Lorsqu'un transfert est effectué sur la base du paragraphe 1, point b), ce transfert est documenté et la documentation est mise à la disposition de l'autorité de contrôle, sur demande, et indique la date et l'heure du transfert, donne des informations sur l'autorité compétente destinataire, indique la justification du transfert et les données à caractère personnel transférées

</td>
<td>

</td>
<td>

Lorsqu’un transfert est effectué aux fins de la sauvegarde des intérêts légitimes de la personne concernée, le responsable du traitement garde trace de la date et l'heure du transfert, des informations sur l'autorité compétente destinataire, et de la justification du transfert et les données à caractère personnel transférées. Il met ces informations à la disposition de la Commission nationale de l'informatique et des libertés, à sa demande.

</td>
<td>

</td></tr>
<tr>
<td>

**_Article 39_**

**Transferts de données à caractère personnel à des destinataires établis dans des pays tiers**

1\. Par dérogation à l'article 35, paragraphe 1, point b), et sans préjudice de tout accord international visé au paragraphe 2 du présent article, le droit de l'Union ou le droit d'un État membre peut prévoir que les autorités compétentes au sens de l'article 3, point 7) a), peuvent, dans certains cas particuliers, transférer des données à caractère personnel directement aux destinataires établis dans des pays tiers, uniquement lorsque les autres dispositions de la présente directive sont respectées et que toutes les conditions ci-après sont remplies:

a)

le transfert est strictement nécessaire à l'exécution de la mission de l'autorité compétente qui transfère les données ainsi que le prévoit le droit de l'Union ou le droit d'un État membre aux fins énoncées à l'article 1er, paragraphe 1;

b)

l'autorité compétente qui transfère les données établit qu'il n'existe pas de libertés ni de droits fondamentaux de la personne concernée qui prévalent sur l'intérêt public nécessitant le transfert dans le cas en question;

c)

l'autorité compétente qui transfère les données estime que le transfert à une autorité qui est compétente aux fins visées à l'article 1er, paragraphe 1, dans le pays tiers est inefficace ou inapproprié, notamment parce que le transfert ne peut pas être effectué en temps opportun;

d)

l'autorité qui est compétente aux fins visées à l'article 1er, paragraphe 1, dans le pays tiers est informée dans les meilleurs délais, à moins que cela ne soit inefficace ou inapproprié;

e)

l'autorité compétente qui transfère les données informe le destinataire de la finalité ou des finalités déterminées pour lesquelles les données à caractère personnel ne doivent faire l'objet d'un traitement que par cette dernière, à condition qu'un tel traitement soit nécessaire.

2\. Par accord international visé au paragraphe 1, on entend tout accord international bilatéral ou multilatéral en vigueur entre les États membres et des pays tiers dans le domaine de la coopération judiciaire en matière pénale et de la coopération policière.

3\. L'autorité compétente qui transfère les données informe l'autorité de contrôle des transferts relevant du présent article.

4\. Lorsqu'un transfert est effectué sur la base du paragraphe 1, ce transfert est documenté.

</td>
<td>

</td>
<td>

**Art. 70-27.** \- Toute autorité publique compétente mentionnée au 2° de l’article 70-1 __ peut, dans certains cas particuliers, transférer des données à caractère personnel directement à des destinataires établis dans un Etat n'appartenant pas à l’Union européenne, lorsque les autres dispositions de la présente loi applicables aux traitements relevant de l’article 70-1 sont respectées et que les conditions ci-après sont remplies :

1° Le transfert est nécessaire à l'exécution de la mission de l'autorité compétente qui transfère ces données pour l’une des finalités énoncées à l’article 70-1 ;

2° L'autorité compétente qui transfère ces données établit qu'il n'existe pas de libertés ni de droits fondamentaux de la personne concernée qui prévalent sur l'intérêt public nécessitant le transfert dans le cas considéré ;

3° L'autorité compétente qui transfère ces données estime que le transfert à l’autorité compétente de l’autre État est inefficace ou inapproprié, notamment parce que le transfert ne peut pas être effectué en temps opportun ;

4° L'autorité compétente de l’autre État est informée dans les meilleurs délais, à moins que cela ne soit inefficace ou inapproprié ;

5° L'autorité compétente qui transfère ces données informe le destinataire de la finalité ou des finalités déterminées pour lesquelles les données à caractère personnel transmises doivent exclusivement faire l'objet d'un traitement par ce destinataire, à condition qu'un tel traitement soit nécessaire ;

L'autorité compétente qui transfère des données informe la Commission nationale de l'informatique et des libertés des transferts relevant du présent article.

L'autorité compétente garde trace de la date et l'heure de ce transfert, des informations sur le destinataire, et de la justification du transfert et les données à caractère personnel transférées.

</td>
<td>

</td></tr>
<tr>
<td>

**Article 40**

**Coopération internationale dans le domaine de la protection des données à caractère personnel**

La Commission et les États membres prennent, à l'égard des pays tiers et des organisations internationales, les mesures appropriées pour:

a) élaborer des mécanismes de coopération internationaux destinés à faciliter l'application effective de la législation relative à la protection des données à caractère personnel;

b) se prêter mutuellement assistance sur le plan international dans l'application de la législation relative à la protection des données à caractère personnel, notamment par la notification, la transmission des réclamations, l'entraide pour les enquêtes et l'échange d'informations, sous réserve de garanties appropriées pour la protection des données à caractère personnel et pour d'autres libertés et droits fondamentaux;

c) associer les parties prenantes intéressées aux discussions et activités visant à développer la coopération internationale dans le domaine de l'application de la législation relative à la protection des données à caractère personnel;

d) favoriser l'échange et la documentation de la législation et des pratiques en matière de protection des données à caractère personnel, y compris en ce qui concerne les conflits de compétence avec des pays tiers.

</td>
<td>

</td>
<td>

</td>
<td>

Ces dispositions relèvent du règlement.

</td></tr></table>

<table>
<tr>
<td>

**CHAPITRE VI**

**Autorités de contrôle indépendantes**

**Section 1**

**Statut d'indépendance**

**Article 41**

**Autorité de contrôle**

1\. Chaque État membre prévoit qu'une ou plusieurs autorités publiques indépendantes sont chargées de surveiller l'application de la présente directive, afin de protéger les libertés et droits fondamentaux des personnes physiques à l'égard du traitement et de faciliter le libre flux des données à caractère personnel au sein de l'Union (ci-après dénommées «autorité de contrôle»).

2\. Chaque autorité de contrôle contribue à l'application cohérente de la présente directive dans l'ensemble de l'Union. À cette fin, les autorités de contrôle coopèrent entre elles et avec la Commission conformément au chapitre VII.

3\. Les États membres peuvent prévoir qu'une autorité de contrôle instituée au titre du règlement (UE) 2016/679 est l'autorité de contrôle visée dans la présente directive et prend en charge les missions de l'autorité de contrôle devant être instituée en vertu du paragraphe 1 du présent article.

4\. Lorsqu'un État membre institue plusieurs autorités de contrôle, il désigne celle qui représente ces autorités au comité visé à l'article 51.

**Article 42**

**Indépendance**

1\. Chaque État membre prévoit que chaque autorité de contrôle agit en toute indépendance dans l'exercice de ses missions et des pouvoirs dont elle est investie conformément à la présente directive.

2\. Les États membres prévoient que, dans l'exercice de leurs missions et de leurs pouvoirs conformément à la présente directive, le ou les membres de leurs autorités de contrôle demeurent libres de toute influence extérieure, qu'elle soit directe ou indirecte, et ne sollicitent ni n'acceptent d'instructions de quiconque.

3\. Le ou les membres des autorités de contrôle des États membres s'abstiennent de tout acte incompatible avec leurs fonctions et, pendant la durée de leur mandat, n'exercent aucune activité professionnelle incompatible, rémunérée ou non.

4\. Chaque État membre veille à ce que chaque autorité de contrôle dispose des ressources humaines, techniques et financières ainsi que des locaux et de l'infrastructure nécessaires à l'exercice effectif de ses missions et de ses pouvoirs, y compris lorsque celle-ci doit agir dans le cadre de l'assistance mutuelle, de la coopération et de la participation au comité.

5\. Chaque État membre veille à ce que chaque autorité de contrôle choisisse et dispose de ses propres agents, qui sont placés sous les ordres exclusifs du membre ou des membres de l'autorité de contrôle concernée.

6\. Chaque État membre veille à ce que chaque autorité de contrôle soit soumise à un contrôle financier qui ne menace pas son indépendance et qu'elle dispose d'un budget annuel public propre, qui peut faire partie du budget global national ou d'une entité fédérée.

</td>
<td>

**Article 11 LIL**

La Commission nationale de l'informatique et des libertés est une autorité administrative indépendante. Elle exerce les missions suivantes :

1° Elle informe toutes les personnes concernées et tous les responsables de traitements de leurs droits et obligations ;

2° Elle veille à ce que les traitements de données à caractère personnel soient mis en oeuvre conformément aux dispositions de la présente loi.

A ce titre :

a) Elle autorise les traitements mentionnés à l'article 25, donne un avis sur les traitements mentionnés aux articles 26 et 27 et reçoit les déclarations relatives aux autres traitements ;

b) Elle établit et publie les normes mentionnées au I de l'article 24 et édicte, le cas échéant, des règlements types en vue d'assurer la sécurité des systèmes ;

c) Elle reçoit les réclamations, pétitions et plaintes relatives à la mise en oeuvre des traitements de données à caractère personnel et informe leurs auteurs des suites données à celles-ci ;

d) Elle répond aux demandes d'avis des pouvoirs publics et, le cas échéant, des juridictions, et conseille les personnes et organismes qui mettent en oeuvre ou envisagent de mettre en oeuvre des traitements automatisés de données à caractère personnel ;

e) Elle informe sans délai le procureur de la République, conformément à l'article 40 du code de procédure pénale du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.

**Article 226-16-1-A** Lorsqu'il a été procédé ou fait procéder à un traitement de données à caractère personnel dans les conditions prévues par le I ou le II de l'article 24 de la loi n° 78-17 du 6 janvier 1978 précitée, le fait de ne pas respecter, y compris par négligence, les normes simplifiées ou d'exonération établies à cet effet par la Commission nationale de l'informatique et des libertés est puni de cinq ans d'emprisonnement et de 300 000 euros d'amende.

**Article 226-16-1** Le fait, hors les cas où le traitement a été autorisé dans les conditions prévues par la loi n° 78-17 du 6 janvier 1978 précitée, de procéder ou faire procéder à un traitement de données à caractère personnel incluant parmi les données sur lesquelles il porte le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques est puni de cinq ans d'emprisonnement et de 300 000 euros d'amende.

**Article 226-17** Le fait de procéder ou de faire procéder à un traitement de données à caractère personnel sans mettre en oeuvre les mesures prescrites à l'article 34 de la loi n° 78-17 du 6 janvier 1978 précitée est puni de cinq ans d'emprisonnement et de 300 000 euros d'amende.

**Article 226-17-1** Le fait pour un fournisseur de services de communications électroniques de ne pas procéder à la notification d'une violation de données à caractère personnel à la Commission nationale de l'informatique et des libertés ou à l'intéressé, en méconnaissance des dispositions du II de l'article 34 bis de la loi n° 78-17 du 6 janvier 1978, est puni de cinq ans d'emprisonnement et de 300 000 € d'amende.

**Article 226-18** Le fait de collecter des données à caractère personnel par un moyen frauduleux, déloyal ou illicite est puni de cinq ans d'emprisonnement et de 300 000 euros d'amende.

**Article 226-18-1** Le fait de procéder à un traitement de données à caractère personnel concernant une personne physique malgré l'opposition de cette personne, lorsque ce traitement répond à des fins de prospection, notamment commerciale, ou lorsque cette opposition est fondée sur des motifs légitimes, est puni de cinq ans d'emprisonnement et de 300 000 euros d'amende.

**Article 226-19** Le fait, hors les cas prévus par la loi, de mettre ou de conserver en mémoire informatisée, sans le consentement exprès de l'intéressé, des données à caractère personnel qui, directement ou indirectement, font apparaître les origines raciales ou ethniques, les opinions politiques, philosophiques ou religieuses, ou les appartenances syndicales des personnes, ou qui sont relatives à la santé ou à l'orientation ou identité sexuelle de celles-ci, est puni de cinq ans d'emprisonnement et de 300 000 euros d'amende.

Est puni des mêmes peines le fait, hors les cas prévus par la loi, de mettre ou de conserver en mémoire informatisée des données à caractère personnel concernant des infractions, des condamnations ou des mesures de sûreté.

**Article 226-19-1** En cas de traitement de données à caractère personnel ayant pour fin la recherche dans le domaine de la santé, est puni de cinq ans d'emprisonnement et de 300 000 euros d'amende le fait de procéder à un traitement :

1° Sans avoir préalablement informé individuellement les personnes sur le compte desquelles des données à caractère personnel sont recueillies ou transmises de leur droit d'accès, de rectification et d'opposition, de la nature des données transmises et des destinataires de celles-ci ;

2° Malgré l'opposition de la personne concernée ou, lorsqu'il est prévu par la loi, en l'absence du consentement éclairé et exprès de la personne, ou s'il s'agit d'une personne décédée, malgré le refus exprimé par celle-ci de son vivant.

**Article 226-20** Le fait de conserver des données à caractère personnel au-delà de la durée prévue par la loi ou le règlement, par la demande d'autorisation ou d'avis, ou par la déclaration préalable adressée à la Commission nationale de l'informatique et des libertés, est puni de cinq ans d'emprisonnement et de 300 000 euros d'amende, sauf si cette conservation est effectuée à des fins historiques, statistiques ou scientifiques dans les conditions prévues par la loi.

Est puni des mêmes peines le fait, hors les cas prévus par la loi, de traiter à des fins autres qu'historiques, statistiques ou scientifiques des données à caractère personnel conservées au-delà de la durée mentionnée au premier alinéa.

**Article 226-21** Le fait, par toute personne détentrice de données à caractère personnel à l'occasion de leur enregistrement, de leur classement, de leur transmission ou de toute autre forme de traitement, de détourner ces informations de leur finalité telle que définie par la disposition législative, l'acte réglementaire ou la décision de la Commission nationale de l'informatique et des libertés autorisant le traitement automatisé, ou par les déclarations préalables à la mise en œuvre de ce traitement, est puni de cinq ans d'emprisonnement et de 300 000 euros d'amende.

**Article 226-22** Le fait, par toute personne qui a recueilli, à l'occasion de leur enregistrement, de leur classement, de leur transmission ou d'une autre forme de traitement, des données à caractère personnel dont la divulgation aurait pour effet de porter atteinte à la considération de l'intéressé ou à l'intimité de sa vie privée, de porter, sans autorisation de l'intéressé, ces données à la connaissance d'un tiers qui n'a pas qualité pour les recevoir est puni de cinq ans d'emprisonnement et de 300 000 euros d'amende.

La divulgation prévue à l'alinéa précédent est punie de trois ans d'emprisonnement et de 100 000 euros d'amende lorsqu'elle a été commise par imprudence ou négligence.

Dans les cas prévus aux deux alinéas précédents, la poursuite ne peut être exercée que sur plainte de la victime, de son représentant légal ou de ses ayants droit.

**Article 226-22-1** Le fait, hors les cas prévus par la loi, de procéder ou de faire procéder à un transfert de données à caractère personnel faisant l'objet ou destinées à faire l'objet d'un traitement vers un Etat n'appartenant pas à la Communauté européenne en violation des mesures prises par la Commission des Communautés européennes ou par la Commission nationale de l'informatique et des libertés mentionnées à l'article 70 de la loi n° 78-17 du 6 janvier 1978 précitée est puni de cinq ans d'emprisonnement et de 300 000 euros d'amende.

**Article 226-22-2** Dans les cas prévus aux articles 226-16 à 226-22-1, l'effacement de tout ou partie des données à caractère personnel faisant l'objet du traitement ayant donné lieu à l'infraction peut être ordonné. Les membres et les agents de la Commission nationale de l'informatique et des libertés sont habilités à constater l'effacement de ces données.

**Article 226-23** Les dispositions de l'article 226-19 sont applicables aux traitements non automatisés de données à caractère personnel dont la mise en œuvre ne se limite pas à l'exercice d'activités exclusivement personnelles.

**Article 226-24** Les personnes morales déclarées responsables pénalement, dans les conditions prévues par l'article 121-2, des infractions définies à la présente section encourent, outre l'amende suivant les modalités prévues par l'article 131-38, les peines prévues par les 2° à 5° et 7° à 9° de l'article 131-39.

L'interdiction mentionnée au 2° de l'article 131-39 porte sur l'activité dans l'exercice ou à l'occasion de l'exercice de laquelle l'infraction a été commise.

**_Enfin, toute entrave à l’action de l’autorité de contrôle est pénalement sanctionnée par l’article 51 de la LIL:_**

**Article 51** Est puni d'un an d'emprisonnement et de 15 000 euros d'amende le fait d'entraver l'action de la Commission nationale de l'informatique et des libertés :

1° Soit en s'opposant à l'exercice des missions confiées à ses membres ou aux agents habilités en application du dernier alinéa de l'article 19 ;

2° Soit en refusant de communiquer à ses membres ou aux agents habilités en application du dernier alinéa de l'article 19 les renseignements et documents utiles à leur mission, ou en dissimulant lesdits documents ou renseignements, ou en les faisant disparaître ;

3° Soit en communiquant des informations qui ne sont pas conformes au contenu des enregistrements tel qu'il était au moment où la demande a été formulée ou qui ne présentent pas ce contenu sous une forme directement accessible.

</td>
<td>

</td>
<td>

Conforme

</td></tr>
<tr>
<td>

**CHAPITRE IX**

**Actes d'exécution**

**Article 58**

**Comité**

1\. La Commission est assistée par le comité institué par l'article 93 du règlement (UE) 2016/679. Ledit comité est un comité au sens du règlement (UE) no 182/2011.

2\. Lorsqu'il est fait référence au présent paragraphe, l'article 5 du règlement (UE) no 182/2011 s'applique.

3\. Lorsqu'il est fait référence au présent paragraphe, l'article 8 du règlement (UE) no 182/2011 s'applique, en liaison avec son article 5.

</td>
<td>

</td>
<td>

</td>
<td>

Ne relève pas de la législation des Etats membres

</td></tr>
<tr>
<td>

**CHAPITRE X**

**Dispositions finales**

**Article 59**

**Abrogation de la décision-cadre 2008/977/JAI**

1\. La décision-cadre 2008/977/JAI est abrogée à compter du 6 mai 2018.

2\. Les références faites à la décision abrogée visée au paragraphe 1 s'entendent comme faites à la présente directive.

</td>
<td>

</td>
<td>

</td>
<td>

Ces dispositions sont sans incidence sur la législation française

</td></tr>
<tr>
<td>

**Article 60**

**Actes juridiques de l'Union déjà en vigueur**

Les dispositions spécifiques relatives à la protection des données à caractère personnel figurant dans des actes juridiques de l'Union qui sont entrés en vigueur le 6 mai 2016 ou avant cette date dans le domaine de la coopération judiciaire en matière pénale et de la coopération policière, qui réglementent le traitement entre États membres et l'accès des autorités nationales désignées des États membres aux systèmes d'information créés en vertu des traités, dans le cadre de la présente directive, demeurent inchangées.

</td>
<td>

</td>
<td>

</td>
<td>

Idem

</td></tr>
<tr>
<td>

**Article 61**

**Relation avec les accords internationaux conclus antérieurement dans le domaine de la coopération judiciaire en matière pénale et de la coopération policière**

Les accords internationaux impliquant le transfert de données à caractère personnel vers des pays tiers ou à des organisations internationales qui ont été conclus par les États membres avant le 6 mai 2016 et qui respectent le droit de l'Union tel qu'il est applicable avant cette date restent en vigueur jusqu'à leur modification, leur remplacement ou leur révocation.

</td>
<td>

</td>
<td>

</td>
<td>

Les conventions d’entraide, d’extradition et de transfèrement et les autres conventions (notamment ONU ou du Conseil de l’Europe) restent applicables jusqu’à modification de ces conventions.

Des stipulations relatives à la protection des données (sur le modèle des stipulations introduites dans les conventions récentes) seront introduites progressivement.

</td></tr>
<tr>
<td>

**Article 62**

**Rapports de la Commission**

1\. Au plus tard le 6 mai 2022, et tous les quatre ans par la suite, la Commission présente au Parlement européen et au Conseil un rapport sur l'évaluation et le réexamen de la présente directive. Ces rapports sont publiés.

2\. Dans le cadre de ces évaluations et réexamens visés au paragraphe 1, la Commission examine, en particulier, l'application et le fonctionnement du chapitre V sur le transfert de données à caractère personnel vers des pays tiers ou à des organisations internationales, en accordant une attention particulière aux décisions adoptées en vertu de l'article 36, paragraphe 3, et de l'article 39.

3\. Aux fins des paragraphes 1 et 2, la Commission peut demander des informations aux États membres et aux autorités de contrôle.

4\. Lorsqu'elle procède aux évaluations et réexamens visés aux paragraphes 1 et 2, la Commission tient compte des positions et des conclusions du Parlement européen, du Conseil ainsi que d'autres organismes ou sources pertinents.

5\. La Commission présente, si nécessaire, des propositions législatives visant à modifier la présente directive, en particulier en tenant compte des évolutions en matière de technologie de l'information et de l'état d'avancement de la société de l'information.

6\. Au plus tard le 6 mai 2019, la Commission réexamine d'autres actes juridiques adoptés par l'Union qui réglementent le traitement par les autorités compétentes aux fins énoncées à l'article 1er, paragraphe 1, y compris ceux qui sont visés à l'article 60, afin d'apprécier la nécessité de les mettre en conformité avec la présente directive et de formuler, le cas échéant, les propositions nécessaires en vue de modifier ces actes pour assurer une approche cohérente de la protection des données à caractère personnel dans le cadre de la présente directive.

</td>
<td>

</td>
<td>

</td>
<td>

Ne concerne pas la législation française

</td></tr>
<tr>
<td>

**Article 63**

**Transposition**

1\. Les États membres adoptent et publient, au plus tard le 6 mai 2018, les dispositions législatives, réglementaires et administratives nécessaires pour se conformer à la présente directive. Ils communiquent immédiatement à la Commission le texte de ces dispositions. Ils appliquent ces dispositions à partir du 6 mai 2018. Lorsque les États membres adoptent ces dispositions, celles-ci contiennent une référence à la présente directive ou sont accompagnées d'une telle référence lors de leur publication officielle. Les modalités de cette référence sont arrêtées par les États membres.

2.Par dérogation au paragraphe 1, un État membre peut prévoir que, à titre exceptionnel, lorsque cela exige des efforts disproportionnés, les systèmes de traitement automatisé installés avant le 6 mai 2016 sont mis en conformité avec l'article 25, paragraphe 1, au plus tard le 6 mai 2023.

3.Par dérogation aux paragraphes 1 et 2 du présent article, un État membre peut, dans des circonstances exceptionnelles, mettre un système donné de traitement automatisé visé au paragraphe 2 du présent article, en conformité avec l'article 25, paragraphe 1, dans un délai déterminé après le délai visé au paragraphe 2 du présent article, lorsque, à défaut de cela, de graves difficultés se poseraient pour le fonctionnement du système de traitement automatisé en question. L'État membre concerné notifie à la Commission les raisons de ces graves difficultés et les motifs justifiant le délai déterminé de mise en conformité du système donné de traitement automatisé avec l'article 25, paragraphe 1. Le délai déterminé n'est en aucun cas fixé au-delà du 6 mai 2026.

4\. Les États membres communiquent à la Commission le texte des dispositions essentielles de droit interne qu'ils adoptent dans le domaine régi par la présente directive.

</td>
<td>

</td>
<td>

**Article 24 du PJL**

Les titres Ier à III, et les articles 21 et 22 de la présente loi entrent en vigueur à compter du 25 mai 2018.

Toutefois, les dispositions de l’article 70-15 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés dans leur rédaction résultant de l’article 19 de la présente loi et relatives à l’obligation de journalisation pourront entrer en vigueur à une date ultérieure ne pouvant excéder le 6 mai 2023 lorsqu’une telle obligation exigerait des efforts disproportionnés, et ne pouvant excéder le 6 mai 2026 lorsque, à défaut d’un tel report, il en résulterait de graves difficultés pour le fonctionnement du système de traitement automatisé. La liste des traitements concernés par ces reports et les dates auxquelles, pour ces traitements, l’entrée en vigueur de cette obligation sera reportée seront déterminées par voie réglementaire.

</td>
<td>

La date d’entrée en vigueur retenue est celle du règlement.

La faculté laissée par la directive d’une entrée en vigueur différée pour la journalisation est utilisée.

</td></tr>
<tr>
<td>

**Article 64 Entrée en vigueur**

La présente directive entre en vigueur le jour suivant celui de sa publication au Journal officiel de l'Union européenne.

</td>
<td>

</td>
<td>

Article 24 du PJL susvisé

</td>
<td>

</td></tr>
<tr>
<td>

**Article 65 Destinataires**

Les États membres sont destinataires de la présente directive.

</td>
<td>

</td>
<td>

</td>
<td>

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-v/articles-24" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/annexe-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

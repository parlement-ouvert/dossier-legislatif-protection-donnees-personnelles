<!-- TITLE: ARTICLE 18 ET ARTICLE 19 SECTION 3 -->
<!-- SUBTITLE: DROITS DE LA PERSONNE CONCERNÉE -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-iii/article-19-section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-iii/article-19-section-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

 ** _1\. Etat des lieux et diagnostic_**

 **1.1 Conformité du droit français à la directive sur les droits des personnes concernées par les données personnelles**

 **1.1.1 Texte de la directive**

Le [chapitre III de la directive](/directive-2016-680/chapitre-iii) fixe les règles applicables aux droits des personnes concernées par les données personnelles.

Par principe, l’[article 12 de la directive](/directive-2016-680/chapitre-iii/article-12) impose au responsable du traitement de fournir à la personne concernée diverses informations, de procéder à toute communication nécessaire lorsque cette dernière lui demande l’accès ou la rectification des données la concernant, et de prendre toutes les mesures nécessaires pour faciliter l’exercice de ses droits par la personne concernée.

1.1.1.1 Dispositions spécifiques sur la portée des droits de la personne concernée

Les articles 13 à 17 de la directive traitent du droit de la personne concernée par les données, à disposer de certaines informations, à accéder, rectifier ou effacer du traitement les données personnelles la concernant, ainsi que de la limitation du traitement.

1° Droit à l’information

L’[article 13 de la directive](/directive-2016-680/chapitre-iii/article-13) impose au responsable du traitement de mettre certaines informations à la disposition de la personne concernée.

Cet article consacre ainsi un droit à l’information pour la personne concernée en matière pénale.

Selon le premier paragraphe de l’article 13, ces informations doivent au moins préciser :

\- l’identité et les coordonnées du responsable du traitement

\- le cas échéant, les coordonnées du délégué à la protection des données;

\- les finalités du traitement auquel sont destinées les données à caractère personnel;

\- le droit d'introduire une réclamation auprès d'une autorité de contrôle et les coordonnées de ladite autorité;

\- l'existence du droit de demander au responsable du traitement l'accès aux données à caractère personnel, leur rectification ou leur effacement, et la limitation du traitement des données à caractère personnel relatives à une personne concernée.

Selon le second paragraphe, le responsable du traitement doit également, dans des cas particuliers, fournir d’autres informations à la personne concernée afin de lui permettre d’exercer ses droits, à savoir :

\- la base juridique du traitement,

\- la durée de conservation des données à caractère personnel ou, lorsque ce n'est pas possible, les critères utilisés pour déterminer cette durée;

\- le cas échéant, les catégories de destinataires des données à caractère personnel, y compris dans les pays tiers ou au sein d'organisations internationales;

\- au besoin, des informations complémentaires, en particulier lorsque les données à caractère personnel sont collectées à l'insu de la personne concernée.

Le [considérant 42 de la directive](/directive-2016-680/considerants/considerant-42) prévoit ainsi que « _dans des cas précis et afin de permettre à la personne concernée d'exercer ses droits, celle-ci devrait être informée de la base juridique du traitement et de la durée pendant laquelle les données seront conservées, dans la mesure où ces informations complémentaires sont nécessaires pour assurer un traitement loyal des données à l'égard de la personne concernée, compte tenu des circonstances particulières dans lesquelles les données sont traitées_ ».

Ce principe d’un droit à l’information de la personne concernée peut néanmoins souffrir de certaines restrictions.

Ainsi le troisième paragraphe de l’[article 13 de la directive](/directive-2016-680/chapitre-iii/article-13) permet aux Etats membres d’adopter des mesures législatives visant à retarder ou limiter la fourniture des informations à la personne concernée en application du second paragraphe, ou à ne pas fournir ces informations, dès lors et aussi longtemps qu'une mesure de cette nature constitue une mesure nécessaire et proportionnée dans une société démocratique, en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne physique concernée pour:

\- éviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires;

\- éviter de nuire à la prévention ou à la détection d'infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l'exécution de sanctions pénales;

\- protéger la sécurité publique;

\- protéger la sécurité nationale;

\- protéger les droits et libertés d'autrui.

Le quatrième paragraphe de l’[article 13 de la directive](/directive-2016-680/chapitre-iii/article-13) laisse à la libre appréciation des Etats membres la possibilité de déterminer des catégories de traitement susceptibles de relever des finalités susvisées.

2° Droit d’accès

Les règles relatives au droit d’accès de la personne concernée sont fixées par les articles [14](/directive-2016-680/chapitre-iii/article-14) et [15](/directive-2016-680/chapitre-iii/article-15) de la directive : l’article 14 pose le principe de ce droit, et l’article 15 délimite les restrictions pouvant y être apportées.

Aux termes de l’article 14 de la directive, la personne concernée a le droit d'obtenir du responsable du traitement la confirmation que des données à caractère personnel la concernant sont ou ne sont pas traitées et, lorsqu'elles le sont, l'accès auxdites données ainsi que les informations suivantes:

\- les finalités du traitement ainsi que sa base juridique;

\- les catégories de données à caractère personnel concernées;

\- les destinataires ou catégories de destinataires auxquels les données à caractère personnel ont été communiquées, en particulier les destinataires qui sont établis dans des pays tiers ou les organisations internationales;

\- la durée de conservation des données à caractère personnel envisagée ou, lorsque ce n'est pas possible, les critères utilisés pour déterminer cette durée;

\- l'existence du droit de demander au responsable du traitement la rectification ou l'effacement des données à caractère personnel, ou la limitation du traitement des données à caractère personnel relatives à la personne concernée;

\- le droit d'introduire une réclamation auprès de l'autorité de contrôle et les coordonnées de ladite autorité;

\- la communication des données à caractère personnel en cours de traitement, ainsi que toute information disponible quant à leur source.

Ce droit d’accès de la personne concernée à ses données personnelles peut néanmoins souffrir de certaines restrictions.

Le premier paragraphe de l’[article 15 de la directive](/directive-2016-680/chapitre-iii/article-15) permet ainsi aux Etats membres de limiter, entièrement ou partiellement, ce droit dès lors que cette mesure est, de la même façon que pour le droit à l’information, nécessaire et proportionnée pour éviter de nuire à la prévention ou à la détection d’infractions pénales, aux enquêtes, recherches, procédures, poursuites ou à l’exécution de sanctions pénales ou pour protéger la sécurité publique, nationale ou les droits et libertés d’autrui.

Le second paragraphe de l’article 15 ouvre une autre option aux Etats membres, en leur permettant de déterminer des catégories de traitement susceptibles de relever des finalités susvisées.

En cas de refus ou de limitation d’accès, la directive impose alors au responsable du traitement d’en informer la personne concernée dans les meilleurs délais, y compris en précisant les motifs de cette restriction.

Cette information de la personne concernée en cas de restriction de son droit d’accès par le responsable du traitement peut néanmoins ne pas être fournie lorsqu’elle risque de compromettre l’un des objectifs susvisés, liés à la protection de la sécurité ou des droits et libertés d’autrui ou à la prévention ou à la détection d’infractions pénales, aux enquêtes, recherches, procédures, poursuites ou à l’exécution de sanctions pénales.

Le responsable du traitement doit alors informer la personne concernée de son droit d’introduire une réclamation auprès d’une autorité de contrôle ou de former un recours juridictionnel. Il doit également consigner les motifs de fait ou de droit qui fondent sa décision, et mettre ces informations à la disposition des autorités de contrôle.

3° Droit de rectification ou d’effacement, et limitation du traitement

Par principe, aux termes du premier paragraphe de l’[article 16 de la directive](/directive-2016-680/chapitre-iii/article-16), la personne concernée a le droit, dans les meilleurs délais, d'obtenir du responsable du traitement que les données inexactes ou incomplètes la concernant soient rectifiées ou complétées.

Dans le second paragraphe de l’article 16, la directive complète ce droit de rectification par le droit pour la personne concernée d’obtenir du responsable du traitement l’effacement de certaines données dans les meilleurs délais. Ce droit à l’effacement existe lorsque le traitement ne respecte pas les dispositions des articles [4](/directive-2016-680/chapitre-ii/article-4), [8](/directive-2016-680/chapitre-ii/article-8) ou [10](/directive-2016-680/chapitre-ii/article-10) de la directive autrement dit lorsqu’il ne respecte pas les principes généraux applicables aux traitements tels que ceux de licéité, de sécurité, de compatibilité avec les finalités légitimes pour lesquelles les données ont été collectées, de conservation pendant une durée adaptée, ou lorsqu’il porte sur des données inexactes ou, hors les cas prévus par la loi, sur des données dites sensibles ou enfin lorsqu’une obligation légale l’exige.

Au lieu d’effacer les données, la directive prévoit que le responsable du traitement peut limiter le traitement soit lorsqu’il ne peut pas être déterminé si les données sont ou non inexactes, soit lorsque ces données doivent être conservées à des fins probatoires.

Lorsque le responsable du traitement fait droit à la demande de rectification, il doit en informer l’autorité compétente dont les données proviennent, ainsi que le destinataire de ces données, à charge pour ce dernier de les rectifier sous sa responsabilité.

Lorsque le responsable du traitement fait droit à la demande d’effacement ou limite le traitement, il doit aviser le destinataire de ces données à charge pour lui de les effacer ou d’en limiter le traitement sous sa responsabilité.

Lorsque le responsable du traitement refuse de rectifier, d’effacer les données ou de limiter leur traitement, il doit en informer la personne concernée, en lui précisant les motifs ayant fondé sa décision. Néanmoins, la directive permet aux Etats membres de ne pas obliger le responsable du traitement à fournir ces informations lorsqu’à l’instar des restrictions possibles pour les droits à l’information et d’accès, cette mesure est nécessaire et proportionnée pour éviter de nuire à la prévention ou à la détection d’infractions pénales ou pour protéger la sécurité et les droits et libertés d’autrui.

En cas de refus, le responsable du traitement doit alors informer la personne concernée de son droit d’introduire une réclamation auprès d’une autorité de contrôle ou de former un recours juridictionnel.

1.1.1.2 Dispositions générales relatives aux modalités d’exercice des droits et de transmission des informations

1° Principe d’un exercice direct des droits par la personne concernée, doublé d’un exercice indirect en cas de restriction

Pour exercer ses droits d’information, d’accès et de rectification, d’effacement des données ou de limitation du traitement, la personne concernée s’adresse directement au responsable du traitement sans passer par l’intermédiaire d’une quelconque autorité.

Néanmoins, dans les cas visés par l’[article 13 paragraphe 3](/directive-2016-680/chapitre-iii/article-13), l’[article 15](/directive-2016-680/chapitre-iii/article-15) et l’[article 16 paragraphe 4](/directive-2016-680/chapitre-iii/article-16), autrement dit en cas de restriction des droits justifiées par la nécessité de ne pas nuire à la prévention ou à la détection d'infractions pénales et de protéger la sécurité et les droits et libertés d'autrui, l’[article 17 de la directive](/directive-2016-680/chapitre-iii/article-17) impose que les droits de la personne concernée puissent également être exercés par l’intermédiaire de l’autorité de contrôle.

Afin de permettre à la personne concernée d’exercer pleinement son droit par l’intermédiaire de l’autorité de contrôle, la directive impose alors au responsable du traitement de l’aviser de cette possibilité.

Aux termes de la directive, l’autorité de contrôle doit a minima informer la personne concernée qu’elle a procédé à toutes les vérifications nécessaires ou à un examen, et l’aviser de son droit de former un recours juridictionnel.

2° Principe d’une transmission claire et gratuite des informations à la personne concernée

En cas de doute raisonnable sur l’identité du demandeur, le responsable du traitement peut solliciter des informations complémentaires destinées à la confirmer.

Une fois cette identité confirmée, l’[article 12 de la directive](/directive-2016-680/chapitre-iii/article-12) impose au responsable du traitement de fournir dans les meilleurs délais à la personne concernée les informations relevant de ses différents droits d'une façon concise, compréhensible et aisément accessible, en des termes clairs et simples, et ce sans exiger le moindre paiement.

Néanmoins dès lors que la demande adressée par une personne concernée est manifestement infondée ou excessive, notamment en raison de son caractère répétitif avec d’autres précédentes requêtes, le responsable du traitement est en droit soit d’exiger le paiement de frais raisonnables pour y répondre, soit de refuser d’y donner suite.

Le responsable du traitement dispose donc d’un pouvoir d’appréciation du bien-fondé des demandes, à charge pour lui néanmoins de rapporter la preuve du caractère manifestement infondé ou abusif de la demande en cas de contestation.

 **1.1.2 Conformité partielle du droit national**

1.1.2.1 Dispositions spécifiques à chaque droit de la personne concernée

1° Absence de droit à l’information en matière pénale

L’[article 32 de la loi Informatique et Libertés](/loi-78-17/chapitre-v/section-1/article-32) fixe les informations devant être mis à disposition de la personne concernée par le responsable de traitement. Cet article n’est néanmoins pas applicable, aux termes de son VI, « _aux traitements de données ayant pour objet la prévention, la recherche, la constatation ou la poursuite d'infractions pénales_ ».

En outre, il fait partie dans la loi informatique et libertés de la section relative aux obligations incombant aux responsables de traitements, alors que dans la directive ces informations sont conçues comme faisant partie des droits de la personne concernée.

Devra donc être intégralement transposé l’[article 13 de la directive](/directive-2016-680/chapitre-iii/article-13) qui crée un droit à l’information de la personne concernée en matière pénale.

2° Droit d’accès

La loi « informatique et libertés » permet déjà aux personnes concernées d'accéder aux données personnelles les concernant.

L’[article 39 de la loi modifiée du 6 janvier 1978](/loi-78-17/chapitre-v/section-2/article-39) prévoit ainsi que toute personne physique justifiant de son identité a le droit d’interroger le responsable d’un traitement de données à caractère personnel en vue d’obtenir :

\- la confirmation que des données à caractère personnel la concernant font ou ne font pas l'objet de ce traitement;

\- des informations relatives aux finalités du traitement, aux catégories de données à caractère personnel traitées et aux destinataires ou aux catégories de destinataires auxquels les données sont communiquées ;

\- le cas échéant, des informations relatives aux transferts de données à caractère personnel envisagés à destination d'un Etat non membre de la Communauté européenne ;

\- la communication, sous une forme accessible, des données à caractère personnel qui la concernent ainsi que de toute information disponible quant à l'origine de celles-ci ;

\- les informations permettant de connaître et de contester la logique qui sous-tend le traitement automatisé en cas de décision prise sur le fondement de celui-ci et produisant des effets juridiques à l'égard de l'intéressé.

Le droit français est donc partiellement conforme à la directive en matière d’accès. Il est conforme quant au droit de la personne concernée d’obtenir la confirmation que les données la concernant sont ou ne sont pas traitées, ainsi que les informations relatives à la finalité du traitement, aux catégories de données, aux destinataires et à l’origine de l’information.

En revanche, devront être transposées les dispositions de la directive autorisant l’accès de la personne concernée aux autres informations, notamment à la base juridique du traitement, à la durée de conservation des données, à ses droits de demander la rectification, l’effacement ou la limitation du traitement et d’introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés.

Lorsqu’il est fait droit à la demande d’accès, la délivrance d’une copie des données peut être subordonnée au paiement par la personne concernée d’une somme qui ne peut excéder le coût de la reproduction.

Aux termes des articles [39](/loi-78-17/chapitre-v/section-2/article-39) de la loi, 86 et 94 du décret, les demandes manifestement abusives peuvent être rejetées.

Afin de mettre en conformité le droit français avec la directive, des dispositions garantissant la gratuité des informations transmises dans le cadre du droit d’accès, sauf demande manifestement abusive ou infondée, devront donc être adoptées.

Dans notre législation, le droit d'accès peut s'exercer directement auprès du responsable pour la plupart des traitements, excepté pour certains traitements pour lesquels un régime dérogatoire d’exercice indirect par l’intermédiaire de la Commission nationale de l’informatique et des libertés est prévu par la loi.

Ainsi, l’[article 41 de la loi](/loi-78-17/chapitre-v/section-2/article-41) prévoit que la personne concernée ne peut exercer son droit d’accès que de manière indirecte lorsque ses données personnelles font l'objet d'un traitement qui _“intéresse la sûreté de l'État, la défense ou la sécurité publique”._

Ces dispositions de l’[article 41](/loi-78-17/chapitre-v/section-2/article-41) sont également applicables, en vertu de l’[article 42 **** de la loi](/loi-78-17/chapitre-v/section-2/article-42), aux traitements mis en œuvre par les administrations publiques et les personnes privées chargées d'un service public qui ont pour mission de prévenir, rechercher ou constater des infractions, si un tel droit est prévu dans l’acte autorisant la mise en œuvre du traitement.

A titre d'illustration, sont soumis à un régime de droit d'accès direct les traitements suivants:

\- APPI, traitement mis en œuvre par les SPIP,

\- AGRASC, traitement mis en œuvre par l'Agence de gestion et de recouvrement des avoirs saisis et confisqués aux fins de gestion et du recouvrement des biens saisis et confisqués,

\- PSEM, traitement mis en œuvre par l’administration pénitentiaire aux fins de gestion des placements sous surveillance électronique mobile,

\- GAME 2010, traitement de « gestion de l'activité et des mesures éducatives 2010 » mis en œuvre par la protection judiciaire de la jeunesse pour le suivi des mineurs,

\- Cassiopée, traitement mis en œuvre dans les juridictions.

Toujours à titre d’illustration, sont notamment soumis à un régime de droit d’accès indirect :

\- le TAJ, traitement des antécédents judiciaires,

\- le FNIS, fichier national des interdits de stade,

\- les fichiers EASP (enquêtes administratives relatives à la sécurité publique) et PASP (prévention des atteintes à la sécurité publique) des services de renseignement territorial,

\- le SALVAC, système d’analyse et de liens de la violence associée aux crimes,

\- ANACRIM, logiciel d’analyse criminelle.

Constituent des fichiers mixtes relevant d’un régime d’accès tantôt direct, tantôt indirect, le fichier des personnes recherchées (FPR), le système d’information Schengen (SIS) ou le traitement GENESIS mis en œuvre dans les établissements pénitentiaires.

Dans le cadre des traitements soumis à un régime de droit d’accès indirect, le droit national confère donc un rôle central à la Commission nationale de l’informatique et des libertés, puisque c'est à elle que doivent être transmises les demandes d’accès.

Lorsque la commission constate, en accord avec le responsable du traitement, que la communication des données ne met pas en cause les finalités du traitement ou la sûreté de l’Etat, la défense ou la sécurité publique, les informations peuvent être communiquées au requérant. En cas d’opposition du responsable de traitement, la commission doit simplement informer le requérant qu'il a été procédé aux vérifications nécessaires.

Devront donc être transposées en droit national les dispositions consacrant, pour tous les traitements relevant de son champ d’application y compris ceux de police judiciaire et intéressant la sécurité publique, le principe d’un droit d’accès direct de la personne concernée auprès du responsable du traitement, la possibilité d’un exercice indirect de ses droits n’étant maintenue qu’en cas de restriction de ses droits.

3° Droit de rectification ou d’effacement

L’[article 40 de la loi informatique et libertés](/loi-78-17/chapitre-v/section-2/article-40) permet à toute personne physique justifiant de son identité d’exiger du responsable d’un traitement que soient, selon les cas, rectifiées, complétées, mises à jour, verrouillées ou effacées les données inexactes, incomplètes, équivoques, périmées la concernant ou dont la collecte, l'utilisation, la communication ou la conservation est interdite.

Lorsque l’intéressé en fait la demande, le responsable du traitement doit alors pouvoir justifier, sans frais pour le demandeur, qu’il a procédé aux opérations nécessaires.

En cas de rectification ou d’effacement d’une donnée inexacte, incomplète, équivoque ou périmée auparavant transmise à un tiers, le responsable du traitement doit lui notifier les opérations effectuées.

A l’instar des règles applicables au droit d’accès, ce droit de rectification ou d’effacement s’exerce de manière directe, hors les traitements intéressant la sûreté de l’Etat, la défense ou la sécurité publique et les traitements de police judiciaire prévus par les articles [41](/loi-78-17/chapitre-v/section-2/article-41) et [42](/loi-78-17/chapitre-v/section-2/article-42) de la loi.

Toute demande manifestement abusive peut être rejetée, aux termes des articles 86 et 94 du décret.

Le droit français est donc conforme pour ce qui concerne les possibilités offertes à la personne concernée de solliciter la rectification et la suppression de données personnelles, et d’obtenir gratuitement la réalisation et la communication de ces opérations.

Il ne prévoit en revanche aucune possibilité de limitation du traitement par le responsable.

Des dispositions devront par conséquent être adoptées en ce sens afin de respecter la directive.

Devront également être transposées les dispositions de la directive consacrant :

\- pour tous les traitements relevant de son champ d’application y compris ceux de police judiciaire et intéressant la sécurité publique, le principe d’un droit de rectification, d’effacement ou de limitation s’exerçant de manière directe par la personne concernée auprès du responsable du traitement, la possibilité d’un exercice indirect de ses droits n’étant maintenue qu’en cas de restriction de ses droits.

\- l’obligation pour le responsable du traitement, lorsqu’il fait droit à une demande de rectification, d’informer également l’autorité compétente dont les données proviennent,

\- la possibilité pour le responsable du traitement d’exiger de la personne concernée le paiement de frais raisonnables ou de refuser de donner suite à une demande manifestement infondée ou abusive.

1.1.2.2 Dispositions générales relatives aux modalités de transmission des informations

Quel que soit le droit qu'il entend exercer au titre de [la loi modifiée du 6 janvier 1978](/loi-78-17), l'intéressé doit fournir au responsable de traitement la justification de son identité.

Les règles nationales relatives à la preuve de l’identité de la personne concernée garantissent donc la protection des données personnelles en interdisant l'accès aux tiers non autorisés, conformément aux exigences européennes.

De même, l’article 95 du décret impose que toute référence à des codes, sigles ou abréviations figurant dans les documents délivrés par le responsable de traitement soit explicitée, si nécessaire sous la forme d'un lexique.

L'objectif de cette disposition, d’adresser une réponse à la personne concernée dans un langage clair, simple et aisément compréhensible, correspond pleinement aux dispositions de l’article 12 de la directive.

 ** _1.2. Cadre constitutionnel_**

Le Conseil constitutionnel attache une importance particulière à la protection des données à caractère personnel au regard de l’[article 2 de la Déclaration des droits de l’homme et du citoyen de 1789](https://www.legifrance.gouv.fr/Droit-francais/Constitution/Declaration-des-Droits-de-l-Homme-et-du-Citoyen-de-1789) qui implique le respect de la vie privée.

Le Conseil constitutionnel a eu l’occasion de se prononcer plus précisément sur des dispositions limitant les demandes d’effacement anticipé concernant les personnes mises en cause, à propos du FNAEG. Il a jugé que les garanties prévues par le législateur étaient de nature à assurer, entre le respect de la vie privée et la sauvegarde de l’ordre public, une conciliation qui n’était pas manifestement déséquilibrée. Parmi les garanties relevées, il a notamment souligné « _que l’inscription au fichier concerne, outre les personnes condamnées pour ces infractions, celles à l’encontre desquelles il existe des indices graves ou concordants rendant vraisemblable qu’elles les aient commises ; que, pour ces dernières, les empreintes prélevées dans le cadre d’une enquête ou d’une information judiciaires sont conservées dans le fichier sur décision d’un officier de police judiciaire agissant soit d’office, soit à la demande du procureur de la République ou du juge d’instruction ; qu’une procédure d’effacement est, dans ce cas, prévue par le législateur, lorsque la conservation des empreintes n’apparaît plus nécessaire compte tenu de la finalité du fichier ; que le refus du procureur de la République de procéder à cet effacement est susceptible de recours devant le juge des libertés et de la détention dont la décision peut être contestée devant le président de la chambre de l’instruction_ »122.

Une décision récente du Conseil constitutionnel portant sur le traitement des antécédents judiciaires (TAJ) illustre néanmoins le renforcement de la jurisprudence du Conseil constitutionnel en matière de protection de la vie privée. Le Conseil constitutionnel a en effet jugé qu’« _en privant les personnes mises en cause dans une procédure pénale, autres que celles ayant fait l’objet d’une décision d’acquittement, de relaxe, de non-lieu ou de classement sans suite, de toute possibilité d’obtenir l’effacement de leurs données personnelles inscrites dans le fichier des antécédents judiciaires, les dispositions contestées port[ai]ent une atteinte disproportionnée au droit au respect de la vie privée_ »123.

 ** _1.3. Cadre conventionnel_**

L’[article 8 paragraphe 2 de la Charte des droits fondamentaux de l’Union européenne](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A12012P%2FTXT) prévoit que toute personne a le droit d'accéder aux données collectées la concernant et d'en obtenir la rectification.

L’[article 12 de la directive 95/46/CE du 24 octobre 1995 relative à la protection des personnes physiques à l’égard du traitement des données à caractère personnel](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A31995L0046), transposée en droit national par la [loi n°2004-801 du 6 août 2004](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000441676), impose également aux États membres de garantir notamment à toute personne concernée le droit d'obtenir du responsable du traitement la confirmation que des données la concernant sont ou ne sont pas traitées, ainsi que des informations portant au moins sur les finalités du traitement, les catégories de données sur lesquelles il porte et les destinataires ou les catégories de destinataires auxquels les données sont communiquées, et selon le cas, la rectification, l'effacement ou le verrouillage des données dont le traitement n'est pas conforme à la directive, notamment en raison du caractère incomplet ou inexact des données.

De même, l’[article 8 de la Convention pour la protection des personnes à l’égard du traitement automatisé des données à caractère personnel du 28 janvier 1981](https://www.coe.int/fr/web/conventions/full-list/-/conventions/rms/0900001680078b39) prévoit que toute personne doit pouvoir :

\- connaître l'existence d'un fichier automatisé de données à caractère personnel, ses finalités principales, ainsi que l'identité et la résidence habituelle ou le principal établissement du maître du fichier;

\- obtenir à des intervalles raisonnables et sans délais ou frais excessifs la confirmation de l'existence ou non dans le fichier automatisé, de données à caractère personnel la concernant ainsi que la communication de ces données sous une forme intelligible;

\- obtenir, le cas échéant, la rectification de ces données ou leur effacement lorsqu'elles ont été traitées en violation des dispositions du droit interne donnant effet aux principes de base énoncés dans la Convention;

\- disposer d'un recours s'il n'est pas donné suite à une demande de confirmation ou, le cas échéant, de communication, de rectification ou d'effacement.

La Cour européenne des droits de l’homme attache une importance particulière à la protection des données à caractère personnel, qui joue selon elle « _un rôle fondamental pour l’exercice du droit au respect de la vie privée et familiale consacré par l’article 8 de la Convention_ »124.

Il ressort notamment de la jurisprudence conventionnelle, particulièrement des décisions rendues contre la France125, que le droit interne doit offrir une possibilité concrète de présenter une requête en effacement des données mémorisées126, et que la Cour apprécie le respect de l’[article 8](https://www.coe.int/fr/web/conventions/full-list/-/conventions/rms/0900001680078b39) au regard des possibilités concrètes d’effacement des données dont bénéficie l’intéressé avant l’expiration de leur durée de conservation, en particulier lorsque la durée de conservation est longue127.

La Cour a ainsi récemment affirmé que « _les personnes condamnées devraient […] se voir offrir une possibilité concrète de présenter une requête en effacement des données mémorisées [...], afin que la durée de conservation soit proportionnée à la nature des infractions et aux buts des restrictions_ ».

Elle en a conclu que dans la mesure où « _le régime actuel de conservation des profils ADN dans le FNAEG [...] n’offre pas, en raison tant de sa durée que de l’absence de possibilité d’effacement, une protection suffisante à l’intéressé_ », il « _ne traduit donc pas un juste équilibre entre les intérêts publics et privés concurrents en jeu_ »128.

**_2\. Objectifs poursuivis et necessite de legiferer_**

 ** _2.1 Objectifs poursuivis_**

 **2.1.1. Mettre notre droit en conformité avec la création du droit à l’information de la personne concernée**

Afin de mettre notre droit en conformité avec l’[article 13 de la directive](/directive-2016-680/chapitre-iii/article-13), il est créé un [nouvel article 70-18](/pjl-490/titre-iii/article-19) qui consacre dans notre ordre juridique l’obligation nouvelle pour le responsable de traitement de mettre à disposition de la personne concernée certaines informations en matière pénale.

Les informations devant être mises à disposition aux termes de ce [nouvel article 70-18](/pjl-490/titre-iii/article-19) correspondent stricto sensu à celles prévues par la directive.

L’[article 18 du projet de loi](/pjl-490/titre-iii/article-18) assure les coordinations nécessaires avec l’[actuel article 32 de la loi informatique et libertés](/loi-78-17/chapitre-v/section-1/article-32).

Ainsi, les [I et II](/pjl-490/titre-iii/article-18) abrogent les dérogations prévues par l’[article 32 V](/loi-78-17/chapitre-v/section-1/article-32) pour les traitements ayant pour objet l’exécution de condamnations pénales ou de mesures de sûreté et par le VI pour les traitements de données ayant pour objet la prévention, la recherche, la constatation ou la poursuite d’infractions pénales. Ces précisions sont en effet désormais inutiles au regard des dispositions spécifiques créées aux [articles 70-1 et suivants](/pjl-490/titre-iii/article-19) qui s’appliquent par dérogation aux autres dispositions de la loi informatique et libertés.

Le [I](/pjl-490/titre-iii/article-18) précise par ailleurs l’articulation de cet [article 32](/loi-78-17/chapitre-v/section-1/article-32) avec les dispositions nouvelles des [articles 70-1 et suivants](/pjl-490/titre-iii/article-19) s’agissant des traitements intéressant la sécurité publique, qui peuvent relever du champ d’application de la directive.

**2.1.2. Mettre notre droit en conformité sur le contenu du droit d’accès de la personne concernée**

Afin de mettre notre droit en conformité avec l’[article 14 de la directive](/directive-2016-680/chapitre-iii/article-14) qui diffère en partie sur le contenu du droit d’accès régi par l’[article 39 de la loi](/loi-78-17/chapitre-v/section-2/article-39), il est créé un [nouvel article 70-19](/pjl-490/titre-iii/article-19) qui reprend strictement les informations prévues par la directive, notamment relatives à la base juridique du traitement, à la durée de conservation des données, aux droits de la personne concernée de demander la rectification, l’effacement ou la limitation du traitement et d’introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés.

 **2.1.3. Mettre notre droit en conformité sur le droit de rectification ou d’effacement des données ou de limitation du traitement**

Afin de mettre notre droit en conformité avec l’[article 16 de la directive](/directive-2016-680/chapitre-iii/article-16), l’[article 70-20 I](/pjl-490/titre-iii/article-19) prévoit que la personne concernée a le droit d'obtenir du responsable du traitement :

\- que soit rectifiées dans les meilleurs délais des données à caractère personnel qui sont inexactes ;

\- que soient complétées des données à caractère personnel incomplètes, y compris en fournissant à cet effet une déclaration complémentaire ;

\- que soit effacées dans les meilleurs délais des données à caractère personnel, lorsque le traitement est réalisé en violation des dispositions de la présente loi ou lorsque ces données doivent être effacées pour respecter une obligation légale à laquelle est soumis le responsable du traitement.

Le [II de l’article 70-20](/pjl-490/titre-iii/article-19) impose au responsable du traitement de justifier qu’il a procédé aux rectifications, compléments et effacements exigés, lorsque l’intéressé en fait la demande, conformément à [l’actuel article 40 de la loi informatique et libertés](/loi-78-17/chapitre-v/section-2/article-40).

Le [III de l’article 70-20](/pjl-490/titre-iii/article-19) précise la possibilité nouvelle pour le responsable du traitement de limiter le traitement lorsqu’il ne peut être déterminé si les données sont ou non inexactes ou lorsque ces données doivent être conservées à des fins probatoires.

Le [IV de l’article 70-20](/pjl-490/titre-iii/article-19) transpose l’obligation pour le responsable du traitement d’informer la personne concernée de tout refus de rectifier ou d'effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs du refus.

Enfin, les [V et VI de l’article 70-18](/pjl-490/titre-iii/article-19) imposent au responsable du traitement qui rectifie ou efface des données, ou limite leur traitement, de notifier ces diligences non seulement aux destinataires de ces données qui devront opérer ces mêmes diligences sous leur responsabilité, mais aussi à l’autorité compétente dont les données proviennent.

**_2.1.4. Mettre notre droit en conformité avec le principe d’un exercice direct des droits_**

Afin de respecter le principe consacré par la directive d’un exercice direct des droits, le [III de l’article 18 du projet de loi](/pjl-490/titre-iii/article-18) précise, s’agissant des traitements intéressant la sécurité publique, que l’exercice indirect des droits prévu par l’article 41 de la loi informatique et libertés s’applique sous réserve des dispositions du [nouveau chapitre XIII](/pjl-490/titre-iii/article-19).

De même, le [IV de l’article 18 du projet de loi](/pjl-490/titre-iii/article-18) supprime l’application aux traitements de police judiciaire du régime dérogatoire d’exercice indirect prévu par l’[article 42 de la loi informatique et libertés](/loi-78-17/chapitre-v/section-2/article-42).

Ainsi, la personne concernée pourra directement exercer ses droits auprès du responsable du traitement, sans passer par l’intermédiaire de la Commission nationale de l’informatique et des libertés, pour tout traitement relevant du champ d’application de la directive, y compris ceux intéressant la sécurité publique ou ceux mis en œuvre par des administrations publiques et des personnes privées chargées d’une mission de service public ayant pour mission de prévenir, rechercher ou constater des infractions.

 ** _2.1.5. Mettre notre droit en conformité avec le principe de gratuité de l’ensemble des informations transmises_**

Il est créé un [nouvel article 70-23](/pjl-490/titre-iii/article-19), qui prévoit, pour tous les droits de la personne concernée, le principe de gratuité des informations qui lui sont transmises, sauf exception liée à une demande manifestement infondée ou abusive.

Dans ce cas, l’[article 70-23](/pjl-490/titre-iii/article-19) permet également au responsable du traitement de refuser de donner suite à la demande.

 ** _2.2 Nécessité de légiférer_**

L’objectif immédiat du projet de loi est de mettre notre droit national en conformité avec les exigences énoncées par la directive, plus particulièrement avec :

\- la création en matière pénale d’un droit à l’information de la personne concernée par les données personnelles,

\- la possibilité nouvelle pour le responsable du traitement de procéder dans certains cas à une limitation du traitement, plutôt qu’à un effacement des données,

\- l’obligation nouvelle pour le responsable du traitement, lorsqu’il fait droit à une demande de rectification, d’informer l’autorité compétente dont les données proviennent,

\- l’exercice par principe direct des droits par la personne concernée auprès du responsable du traitement, y compris pour les traitements intéressant la sécurité publique et la police judiciaire,

\- la consécration du principe de gratuité de toutes les informations transmises en application de ces droits par le responsable du traitement à la personne concernée, sauf exception liée à une demande manifestement infondée ou abusive.

**_3\. Options_**

La directive laisse à la libre appréciation des Etats membres la faculté de fixer certaines restrictions aux droits de la personne concernée, ainsi que la détermination des règles applicables à l’exercice des droits par la personne concernée pour certains types de données ou de traitements.

**3.1. Transposer ou non les règles de la directive pour les données figurant dans une décision judiciaire, un casier ou un dossier judiciaire faisant l’objet d’un traitement lors d’une enquête judiciaire et d’une procédure pénale**

 **3.1.1. Option 1 (écartée) : Transposer les règles de la directive**

L’[article 18 de la directive](/directive-2016-680/chapitre-iii/article-18) laisse une option aux Etats membres s’agissant des droits de la personne concernée par des données à caractère personnel figurant dans une décision judiciaire, un casier ou dossier judiciaire faisant l’objet d’un traitement dans le cadre d’une enquête judiciaire et d’une procédure pénale.

Dans ces cas, la directive permet en effet de prévoir que les droits d’information, d’accès, de rectification, d’effacement ou de limitation du traitement prévus par les articles [13](/directive-2016-680/chapitre-iii/article-13), [14](/directive-2016-680/chapitre-iii/article-14) et [16](/directive-2016-680/chapitre-iii/article-16) de la directive sont exercés conformément au droit national.

L’option d’imposer les exigences européennes pour ces données a été écartée.

 **3.1.2. Option 2 (retenue) : Maintenir le droit national**

Cette option a été retenue.

Le Gouvernement a en effet souhaité maintenir la réglementation nationale propre à ces données traitées, compte tenu de l’équilibre nécessaire entre d’une part les intérêts et droits fondamentaux de la personne concernée, et d’autre part les nécessités de l’enquête, du secret de l’instruction et de la publicité des audiences.

Il est ainsi créé un [nouvel article 70-24](/pjl-490/titre-iii/article-19), aux termes duquel les dispositions relatives aux droits des personnes concernées transposées conformément aux exigences européennes ne s’appliquent pas lorsque les données à caractère personnel figurent soit dans une décision judiciaire, soit dans un dossier judiciaire faisant l'objet d'un traitement lors d'une procédure pénale. Dans ces cas, l’accès à ces données ne peut se faire que dans les conditions prévues par le code de procédure pénale.

 **3.2. Fixer ou non des restrictions aux droits de la personne concernée**

 **3.1.1. Option 1 (écartée) : Ne prévoir aucune restriction aux droits des personnes concernée**

L’[article 13 paragraphe 3](/directive-2016-680/chapitre-iii/article-13), l’[article 15 paragraphe 1](/directive-2016-680/chapitre-iii/article-15) et l’[article 16 paragraphe 4](/directive-2016-680/chapitre-iii/article-16) de la directive prévoient que les Etats membres peuvent adopter des mesures législatives restreignant l’ensemble des droits de la personne concernée, dès lors et aussi longtemps qu'une telle restriction constitue une mesure nécessaire et proportionnée dans une société démocratique, en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne physique concernée, pour:

a) éviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires;

b) éviter de nuire à la prévention ou à la détection d'infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l'exécution de sanctions pénales;

c) protéger la sécurité publique

d) protéger la sécurité nationale;

e) protéger les droits et libertés d'autrui.

L’option de ne pas user de cette faculté a été écartée.

Le gouvernement ne souhaite pas en effet que les droits légitimes de la personne concernée puissent s’exercer au détriment de la sécurité nationale ou publique, des droits et libertés d’autrui ou des nécessités de prévenir, rechercher, poursuivre les infractions pénales et d’exécuter les sanctions pénales.

 **3.1.2. Option 2 (écartée) : Déterminer des catégories de traitement susceptibles de donner lieu à des restrictions des droits à l’information et d’accès**

L’[article 13 paragraphe 4](/directive-2016-680/chapitre-iii/article-13) et l’[article 15 paragraphe 2](/directive-2016-680/chapitre-iii/article-15) de la directive permettent aux Etats membres de fixer, par des mesures législatives, des catégories de traitement de données susceptibles de relever des finalités susvisées et ainsi de donner lieu à des restrictions des droits d’information et d’accès pour la personne concernée.

Cette option a été écartée.

En dehors des décisions et dossiers judiciaires qui font déjà l’objet d’une dérogation spéciale (cf supra), il est en effet apparu incohérent que la loi ne puisse déterminer des catégories de traitement qui pourraient nécessiter une dérogation générale, que pour les seuls droits d’information et d’accès, et non pour le droit de rectification, d’effacement ou de limitation.

Au vu de l’option ci-dessous retenue, l’utilisation d’une telle possibilité par la loi aurait conduit à autoriser moins de restrictions par la loi que par l’acte instaurant le traitement, notamment par l’acte réglementaire portant création du traitement.

L’acte instaurant le traitement aurait par ailleurs dû ajouter, pour chaque traitement, la possibilité de restriction pour le droit de rectification ou d’effacement, de sorte que cette option ne présentait qu’un intérêt pratique très limité.

Au surplus, il faut rappeler que si l’actuel [article 42 de la loi informatique et libertés](/loi-78-17/chapitre-v/section-2/article-42) permet un accès indirect pour les fichiers de police judiciaire, il impose toutefois que ce droit d’accès indirect soit prévu par l’acte réglementaire en indiquant _« si un tel droit a été prévu par l’autorisation mentionnée aux articles [25](/loi-78-17/chapitre-iv/section-2/article-25), _[26](/loi-78-17/chapitre-iv/section-2/article-26)_ et [27](/loi-78-17/chapitre-iv/section-2/article-27)_ ».

**3.1.3 Option 3 (retenue) : Prévoir des restrictions aux droits des personnes concernées dans l’acte instaurant le traitement**

Compte tenu du maintien des formalités préalables à la création d’un traitement et de la logique actuelle de l’[article 42 de la loi informatique et libertés](/loi-78-17/chapitre-v/section-2/article-42), a été retenue l’option de laisser à l’acte instaurant le traitement , plutôt qu’à la loi, le soin de déterminer _in concreto_ les traitements dans lesquels l’ensemble des droits, y compris celui de rectification ou d’effacement, pourraient être restreints.

Le [nouvel article 70-21](/pjl-490/titre-iii/article-19) permet ainsi à l’acte instaurant le traitement de déterminer si dans ce traitement, les droits des personnes concernées peuvent ou non faire l’objet de certaines restrictions.

Si le traitement est mis en œuvre pour le compte de l’Etat, l’arrêté ou le décret en Conseil d’Etat pris après avis de la Commission nationale de l’informatique et des libertés détermine les restrictions applicables.

Dans les autres cas, seul un acte clair, précis et d’application prévisible pour les justiciables conformément au [considérant 33 de la directive](/directive-2016-680/considerants/considerant-33) peut fixer des restrictions aux droits des personnes concernées.

A défaut d’un tel acte, aucune restriction ne peut être appliquée aux droits des personnes concernées.

Conformément aux exigences de la directive, l’acte instaurant le traitement peut prévoir des restrictions aux droits de la personne physique concernée dès lors et aussi longtemps qu'une mesure de cette nature constitue une mesure nécessaire et proportionnée dans une société démocratique en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne pour :

\- éviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires;

\- éviter de nuire à la prévention ou à la détection d'infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l'exécution de sanctions pénales ;

\- protéger la sécurité publique ;

\- protéger la sécurité nationale ;

\- protéger les droits et libertés d'autrui.

Lorsque l’acte instaurant le traitement prévoit la possibilité de telles restrictions aux droits, le responsable du traitement est en droit de :

1° Retarder ou limiter la fourniture ou ne pas fournir certaines informations à la personne concernée, à savoir celles mentionnées au [II de l’article 70-18](/pjl-490/titre-iii/article-19);

2° Limiter, entièrement ou partiellement, le droit d'accès de la personne concernée prévu par l’[article 70-19](/pjl-490/titre-iii/article-19) ;

3° Ne pas informer la personne de son refus de rectifier ou d'effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs de cette décision conformément au [IV de l’article 70-20](/pjl-490/titre-iii/article-19).

En cas de refus ou de limitation du droit d’accès, le responsable du traitement doit en informer la personne concernée dans les meilleurs délais, ainsi que des motifs du refus ou de la limitation. Ces informations peuvent néanmoins ne pas être fournies lorsque leur communication risque de compromettre l'un des objectifs susmentionnés de prévention des infractions pénales, de protection de la sécurité ou des droits d’autrui. Le responsable du traitement doit alors consigner les motifs de fait ou de droit sur lesquels se fonde la décision, et mettre ces informations à la disposition de la Commission nationale de l’informatique et des libertés.

En cas de restriction des droits, le responsable du traitement doit informer la personne concernée de la possibilité d'introduire une réclamation en exerçant ses droits par l’intermédiaire de la Commission nationale de l’informatique et des libertés ou de former un recours juridictionnel.

L’esprit de la directive et particulièrement le [considérant 44](/directive-2016-680/considerants/considerant-44) ci-dessous partiellement reproduit commande une appréciation _in concreto_ de la demande par le responsable du traitement : « _Le responsable du traitement devrait apprécier, en examinant chaque cas de façon concrète et individuelle, s'il y a lieu de limiter le droit d'accès partiellement ou complètement_. »

Le gouvernement ayant décidé d’utiliser l’option de fixer certaines restrictions aux droits de la personne concernée, il est nécessaire, au vu de l’[article 17 de la directive](/directive-2016-680/chapitre-iii/article-17), de permettre à cette dernière de les exercer également dans ces cas par l’intermédiaire d’une autorité de contrôle.

Ainsi le [nouvel article 70-22](/pjl-490/titre-iii/article-19) détermine les modalités d’exercice indirect de ses droits par la personne concernée par l’intermédiaire de la Commission nationale de l'informatique et des libertés en cas de restriction de ses droits.

Cet article maintient la législation nationale en vigueur en cas de saisine de la Commission, à savoir les [deuxième et troisième alinéas de l’article 41](/loi-78-17/chapitre-v/section-2/article-41).

Est enfin transposée dans cet article l’obligation pour la Commission d’informer la personne concernée de son droit de former un recours juridictionnel lorsqu’elle l’avise avoir procédé aux vérifications nécessaires.

 ** _4\. analyse des Impacts de la disposition envisagée_**

 ** _4.1. Impacts juridiques_**

L’impact juridique de la disposition consiste dans l’insertion dans le [nouveau chapitre XIII](/pjl-490/titre-iii/article-19) de la loi de 1978 de sept articles, les articles [70-18 à 70-24](/pjl-490/titre-iii/article-19), et dans la modification des actuels articles [32](/loi-78-17/chapitre-v/section-1/article-32), [41](/loi-78-17/chapitre-v/section-2/article-41) et [42](/loi-78-17/chapitre-v/section-2/article-42) de la loi de 1978.

<table>
<tr>
<td>

**DISPOSITIONS DU PROJET DE LOI**

</td>
<td>

[**DIRECTIVE (UE) 2016/680 DU PARLEMENT EUROPÉEN ET DU CONSEIL du 27 avril 2016**](/directive-2016-680)

</td></tr>
<tr>
<td>

[**Article 18 PJL**](/titre-iii/article-18)

</td>
<td>

Suppression, par coordination, des dérogations en matière pénale prévues par l’[article 32](/loi-78-17/chapitre-v/section-1/article-32) relatif au droit à l’information ; et précision de l’articulation avec le nouveau chapitre XIII pour les traitements intéressant la sécurité publique.

La directive créant, dans ses [articles 14 à 16](/directive-2016-680/chapitre-iii), des droits d’accès et de rectification par principe directs en matière pénale, l’accès indirect prévu par l’[article 42 de la LIL](/loi-78-17/chapitre-v/section-2/article-42) pour les traitements de police judiciaire est supprimé par l’[article 21 du PJL](/pjl-490/titre-v/article-21), et celui prévu par l’[article 41 de la LIL](/loi-78-17/chapitre-v/section-2/article-41) s’applique pour les traitements intéressant la sécurité publique sous réserve des dispositions du [chapitre XIII](/loi-78-17/chapitre-xiii).

</td></tr>
<tr>
<td>

[**Article 19 PJL**](/pjl-490/titre-iii/article-19)

</td>
<td>

</td></tr>
<tr>
<td>

[**Article 70-18**](/pjl-490/titre-iii/article-18)

</td>
<td>

[Article 13](/directive-2016-680/chapitre-iii/article-13)

</td></tr>
<tr>
<td>

[**Article 70-19**](/pjl-490/titre-iii/article-19)

</td>
<td>

[Article 14](/directive-2016-680/chapitre-iii/article-14)

</td></tr>
<tr>
<td>

[**Article 70-20**](/pjl-490/titre-iii/article-19)

</td>
<td>

[Article 16](/directive-2016-680/chapitre-iii/article-16)

</td></tr>
<tr>
<td>

[**Article 70-21**](/pjl-490/titre-iii/article-19))

</td>
<td>

Transposition combinée des limitations ou restrictions aux droits d’information, d’accès ou de rectification prévues aux articles [13](/directive-2016-680/chapitre-iii/article-13), [15](/directive-2016-680/chapitre-iii/article-15) et [16](/directive-2016-680/chapitre-iii/article-16) de la directive.

Transposition également de l’obligation pour le responsable du traitement d’informer la personne concernée de la possibilité, prévue par l’[article 17 de la directive](/directive-2016-680/chapitre-iii/article-17), d’exercer ses droits par l’intermédiaire de la CNIL.

</td></tr>
<tr>
<td>

[**Article 70-22**](/pjl-490/titre-iii/article-19)

</td>
<td>

[Article 17](/directive-2016-680/chapitre-iii/article-17)

</td></tr>
<tr>
<td>

[**Article 70-23**](/pjl-490/titre-iii/article-19)

</td>
<td>

[Article 12](/directive-2016-680/chapitre-iii/article-12)

</td></tr>
<tr>
<td>

[**Article 70-24**](/pjl-490/titre-iii/article-19)

</td>
<td>

[Article 18](/directive-2016-680/chapitre-iii/article-18)

</td></tr></table>

**_4.2. Impacts sur les services judiciaires et sur les administrations publiques_**

Le principe d’un accès direct à tous les traitements va entraîner le transfert des demandes actuellement adressées à la Commission nationale de l’informatique et des libertés en sa qualité d’intermédiaire vers le responsable du traitement.

 ** _4.3. Impact sur les finances publiques_**

La gratuité des informations transmises imposée par la directive, en lieu et place de la possible délivrance d’une copie des données personnelles aux frais de la personne concernée dans le cadre de son droit d’accès, peut avoir un impact sur les finances publiques pour les traitements placés sous la responsabilité d’une administration publique.

 ** _4.4. Impacts sur les particuliers_**

Les droits de personnes faisant l’objet d’un traitement relevant de la directive seront renforcés par les nouvelles dispositions.

 ** _5\. CONSULTATION ET MODALITÉS D’APPLICATION_**

La Commission nationale de l’informatique et des libertés a été consultée.

La réforme s’appliquera le 25 mai 2018, conformément à l’[article 24 du projet de loi](/pjl-490/titre-v/article-24).

Ces dispositions seront applicables sur l’ensemble du territoire, hors les collectivités d’outre-mer soumises au principe de spécialité, pour lesquelles l’extension se fera par ordonnance

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-iii/article-19-section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-iii/article-19-section-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: ARTICLE 19 SECTION 2 -->
<!-- SUBTITLE: OBLIGATIONS INCOMBANT AUX AUTORITES COMPETENTES ET AUX RESPONSABLES DE TRAITEMENT -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-iii/article-19-section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-iii/article-18-et-article-19-section-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

 ** _1\. Etat des lieux et diagnostic_**

 ** _1.1. Etat des lieux_**

 **1.1.1 Conformité du droit français à la directive sur les droits des personnes concernées par les données personnelles**

 **1.1.1.1 Texte de la directive**

La [directive](/directive-2016-680) fixe des obligations aux autorités compétentes ainsi qu’aux responsables de traitement et aux sous-traitants.

1.1.1.1.1. Mettre à jour les données

Aux termes de son [article 7 paragraphe 2](/directive-2016-680/chapitre-ii/article-7), la directive impose aux autorités compétentes de prendre toutes les mesures raisonnables pour garantir que les données à caractère personnel qui sont inexactes, incomplètes ou ne sont plus à jour ne soient pas transmises ou mises à disposition.

À cette fin, chaque autorité compétente doit vérifier, dans la mesure du possible, la qualité des données à caractère personnel avant leur transmission ou mise à disposition.

Lors de toute transmission de données à caractère personnel, doivent ainsi être ajoutées, dans la mesure du possible, des informations nécessaires permettant à l'autorité compétente destinataire de juger de l'exactitude, de l'exhaustivité, et de la fiabilité des données à caractère personnel, et de leur niveau de mise à jour.

Le [considérant 32 de la directive](/directive-2016-680/considerants/considerant-32) précise le champ d’application de cette obligation en ces termes : « _afin de garantir la protection des personnes physiques, l'exactitude, et la fiabilité des données à caractère personnel transmises ou mises à disposition ainsi que leur exhaustivité ou leur niveau de mise à jour, les autorités compétentes devraient, dans la mesure du possible, ajouter les informations nécessaires dans tous les transferts de données à caractère personnel_ ».

Aux termes de l’[article 7 paragraphe 3](/directive-2016-680/chapitre-ii/article-7), s'il s'avère que des données à caractère personnel inexactes ont été transmises ou que des données à caractère personnel ont été transmises de manière illicite, le destinataire en est informé sans retard. Dans ce cas, les données à caractère personnel sont rectifiées ou effacées ou leur traitement est limité

1.1.1.1.2. Opérer une distinction selon les catégories de personnes concernées

En vertu de son [article 6](/directive-2016-680/chapitre-ii/article-6), la directive impose au responsable du traitement d’établir, le cas échéant et dans la mesure du possible, une distinction claire entre les données à caractère personnel de différentes catégories de personnes concernées telles que :

\- les personnes à l'égard desquelles il existe des motifs sérieux de croire qu'elles ont commis ou sont sur le point de commettre une infraction pénale ;

\- les personnes reconnues coupables d'une infraction pénale ;

\- les victimes d'une infraction pénale ou les personnes à l'égard desquelles certains faits portent à croire qu'elles pourraient être victimes d'une infraction pénale ;

\- les tiers à une infraction pénale, tels que les personnes pouvant être appelées à témoigner lors d'enquêtes en rapport avec des infractions pénales ou des procédures pénales ultérieures, des personnes pouvant fournir des informations sur des infractions pénales, ou des contacts ou des associés de l'une des personnes visées aux deux premiers points.

1.1.1.1.3 Respecter des règles destinées à assurer la sécurité des données

1° Prendre des mesures techniques et organisationnelles appropriées

Aux termes de son [considérant 53](/directive-2016-680/considerants/considerant-53), la directive rappelle que la protection des droits et libertés des personnes physiques à l'égard du traitement des données à caractère personnel exige l'adoption de mesures techniques et organisationnelles appropriées, qui respectent, en particulier, les principes de protection des données dès la conception et de protection des données par défaut.

Ainsi, les articles [19](/directive-2016-680/chapitre-iv/section-1/article-19) et [20](/directive-2016-680/chapitre-iv/section-1/article-19) prévoient, à titre d’obligations générales, que le responsable du traitement doit, compte tenu de l'état des connaissances, des coûts de la mise en œuvre et de la nature, de la portée, du contexte et des finalités du traitement ainsi que des risques, dont le degré de probabilité et de gravité varie, pour les droits et libertés des personnes physiques, mettre en œuvre les mesures techniques et organisationnelles appropriées, telles que la pseudonymisation :

\- pour s'assurer et être en mesure de démontrer que le traitement est conforme aux exigences de la directive et à la protection des droits des personnes concernées,

\- pour garantir que par défaut, seules les données personnelles nécessaires au regard de chaque finalité spécifique du traitement sont traitées, et que les données ne soient pas rendues accessibles à un nombre indéterminé de personnes sans l’intervention de la personne concernée.

Ces dispositions sont également applicables au sous-traitant.

L’[article 22](/directive-2016-680/chapitre-iv/section-1/article-22) prévoit en effet que le sous-traitant auquel le responsable du traitement fait appel doit présenter des garanties suffisantes quant à la mise en œuvre des mesures techniques et organisationnelles appropriées afin que le traitement soit conforme aux exigences de la directive et à la protection des droits des personnes concernées.

Le [considérant 53](/directive-2016-680/considerants/considerant-53) de la directive précise par ailleurs que « _le sous-traitant devrait tenir compte du principe de protection des données dès la conception et par défaut_ ».

L’[article 29](/directive-2016-680/chapitre-iv/section-2/article-29) de la directive impose également au responsable du traitement et au sous-traitant de mettre en œuvre les mesures techniques et organisationnelles appropriées afin de garantir un niveau de sécurité adapté au risque.

Aux termes de son [considérant 60](/directive-2016-680/considerants/considerant-60), « _il importe que le responsable du traitement ou le sous-traitant évalue les risques inhérents au traitement et mette en œuvre des mesures pour les atténuer, telles que le chiffrement. […]_ _Dans le cadre de l'évaluation des risques pour la sécurité des données, il convient d'apprécier les risques que présente le traitement de données, tels que la destruction, la perte, l'altération ou la divulgation non autorisée de données à caractère personnel transmises, conservées ou traitées d'une autre manière ou l'accès non autorisé à de telles données, de manière accidentelle ou illicite, qui sont susceptibles, notamment, d'entraîner des dommages physiques, matériels ou un préjudice moral_ ».

Ainsi l’[article 29](/directive-2016-680/chapitre-iv/section-2/article-29) de la directive prévoit des dispositions spécifiques au traitement de données sensibles et aux traitements automatisés.

Son premier paragraphe impose ainsi au responsable du traitement et au sous-traitant de mettre en œuvre les mesures techniques et organisationnelles appropriées afin de garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des données personnelles qui révèlent l'origine raciale ou ethnique, les opinions politiques, les convictions religieuses ou philosophiques, l'appartenance syndicale, et le traitement des données génétiques, des données biométriques aux fins d'identifier une personne physique de manière unique, des données concernant la santé ou des données concernant la vie sexuelle ou l'orientation sexuelle d'une personne physique.

Son second paragraphe fixe une liste de mesures à mettre en œuvre par le responsable du traitement ou le sous-traitant dans le cadre d’un traitement automatisé, à savoir :

a) empêcher toute personne non autorisée d'accéder aux installations utilisées pour le traitement (contrôle de l'accès aux installations);

b) empêcher que des supports de données puissent être lus, copiés, modifiés ou supprimés de façon non autorisée (contrôle des supports de données);

c) empêcher l'introduction non autorisée de données à caractère personnel dans le fichier, ainsi que l'inspection, la modification ou l'effacement non autorisé de données à caractère personnel enregistrées (contrôle de la conservation);

d) empêcher que les systèmes de traitement automatisé puissent être utilisés par des personnes non autorisées à l'aide d'installations de transmission de données (contrôle des utilisateurs);

e) garantir que les personnes autorisées à utiliser un système de traitement automatisé ne puissent accéder qu'aux données à caractère personnel sur lesquelles porte leur autorisation (contrôle de l'accès aux données);

f) garantir qu'il puisse être vérifié et constaté à quelles instances des données à caractère personnel ont été ou peuvent être transmises ou mises à disposition par des installations de transmission de données (contrôle de la transmission);

g) garantir qu'il puisse être vérifié et constaté a posteriori quelles données à caractère personnel ont été introduites dans les systèmes de traitement automatisé, et à quel moment et par quelle personne elles y ont été introduites (contrôle de l'introduction);

h) empêcher que, lors de la transmission de données à caractère personnel ainsi que lors du transport de supports de données, les données puissent être lues, copiées, modifiées ou supprimées de façon non autorisée (contrôle du transport);

i) garantir que les systèmes installés puissent être rétablis en cas d'interruption (restauration);

j) garantir que les fonctions du système opèrent, que les erreurs de fonctionnement soient signalées (fiabilité) et que les données à caractère personnel conservées ne puissent pas être corrompues par un dysfonctionnement du système (intégrité).

2° Tenir un registre et un journal des activités et de certaines opérations de traitement et coopérer avec l’autorité de contrôle

Afin de pouvoir démontrer la licéité du traitement, de pratiquer l'autocontrôle et de garantir l'intégrité et la sécurité des données tel que rappelé par les considérants [56](/directive-2016-680/considerants/considerant-56) et [57](/directive-2016-680/considerants/considerant-57), la directive impose deux obligations au responsable du traitement et au sous-traitant.

La première obligation fixée par l’[article 24 de la directive](/directive-2016-680/chapitre-iv/section-1/article-24) est de tenir un registre écrit pour toutes les catégories d'activités de traitement relevant de leur responsabilité. Chaque responsable du traitement et sous-traitant devra être tenu de coopérer avec l'autorité de contrôle et de mettre ces registres à la disposition de cette dernière sur sa demande pour qu'ils puissent servir au contrôle de ces opérations de traitement.

La seconde obligation prévue par l’[article 25 de la directive](/directive-2016-680/chapitre-iv/section-1/article-25) est d’établir des journaux au moins pour les opérations de collecte, de modification, de consultation, de communication, y compris les transferts, l'interconnexion ou l'effacement, effectuées dans des systèmes de traitement automatisé.

Les journaux des opérations de consultation et de communication doivent permettre d'établir le motif, la date et l'heure de celles-ci et, dans la mesure du possible, l'identification de la personne qui a consulté ou communiqué les données à caractère personnel, ainsi que l'identité des destinataires de ces données à caractère personnel.

Ces journaux ne doivent être utilisés qu’à des fins de vérification de la licéité du traitement, d'autocontrôle, de garantie de l'intégrité et de la sécurité des données à caractère personnel et pour les besoins de procédures pénales.

Afin de permettre à l’autorité de contrôle d’exercer pleinement son contrôle, le responsable du traitement et le sous-traitant sont tenus de lui mettre ces journaux à disposition sur demande.

De manière générale aux termes de l’[article 26 de la directive](/directive-2016-680/chapitre-iv/section-1/article-26), le responsable du traitement et le sous-traitant sont tenus de coopérer avec l’autorité de contrôle, à sa demande, dans l’exécution de ses missions.

3° Informer l’autorité de contrôle et la personne concernée en cas de violation de données à caractère personnel

En cas de violation de données à caractère personnel, les articles [30](/directive-2016-680/chapitre-iv/section-2/article-30) et [31](/directive-2016-680/chapitre-iv/section-2/article-31) de la directive fixent deux obligations d’information à la charge du responsable du traitement et destinées l’une à l’autorité de contrôle, l’autre à la personne concernée.

En premier lieu, le responsable du traitement doit notifier la violation à l'autorité de contrôle, à moins qu'il soit peu probable que la violation en question n'engendre des risques pour les droits et les libertés d'une personne physique. Il doit documenter toute violation en indiquant notamment les mesures prises pour y remédier, afin de permettre à l’autorité de contrôle de procéder aux vérifications nécessaires. Le sous-traitant doit également notifier au responsable du traitement toute violation de données à caractère personnel dans les meilleurs délais après en avoir pris connaissance. Enfin, lorsque la violation de données à caractère personnel porte sur des données à caractère personnel qui ont été transmises par le responsable du traitement d'un autre État membre ou à celui-ci, les mêmes informations que celles imposées lors de la notification à l’autorité de contrôle doivent être communiquées au responsable du traitement de cet État membre dans les meilleurs délais.

En second lieu, le responsable du traitement doit, lorsque cette violation de données à caractère personnel est susceptible d'engendrer un risque élevé pour les droits et les libertés d'une personne physique, la communiquer à la personne concernée dans les meilleurs délais.

Par exception, il n’est pas tenu à cette obligation si :

\- il a mis en œuvre les mesures de protection techniques et organisationnelles appropriées et ces dernières ont été appliquées aux données à caractère personnel affectées par ladite violation, en particulier les mesures qui rendent les données à caractère personnel incompréhensibles pour toute personne qui n'est pas autorisée à y avoir accès, telles que le chiffrement;

\- il a pris des mesures ultérieures qui garantissent que le risque élevé pour les droits et les libertés des personnes concernées visé au paragraphe 1 n'est plus susceptible de se matérialiser;

\- cette communication exigerait des efforts disproportionnés. Dans ce cas, il est plutôt procédé à une communication publique ou à une mesure similaire permettant aux personnes concernées d'être informées de manière tout aussi efficace.

Si le responsable du traitement n'a pas déjà communiqué à la personne concernée la violation de données à caractère personnel la concernant, l'autorité de contrôle peut, après avoir examiné si cette violation est susceptible d'engendrer un risque élevé, soit exiger du responsable du traitement qu'il procède à cette communication, soit décider qu’il n’est effectivement pas tenu à cette obligation au vu des exceptions ci-dessus décrites.

La directive prévoit enfin que la communication à la personne concernée peut être retardée, limitée ou omise, dès lors et aussi longtemps qu'une mesure de cette nature constitue une mesure nécessaire et proportionnée dans une société démocratique, en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne physique concernée pour éviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires, éviter de nuire à la prévention ou à la détection d'infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l'exécution de sanctions pénales, protéger la sécurité publique, protéger la sécurité nationale ou protéger les droits et libertés d'autrui.

1.1.1.4. Désigner un délégué à la protection des données

L’[article 32](/directive-2016-680/chapitre-iv/section-3/article-32) impose au responsable du traitement de désigner un délégué à la protection des données, excepté pour les autorités judiciaires agissant dans l’exercice de leur fonction juridictionnelle.

Les articles [33](/directive-2016-680/chapitre-iv/section-3/article-33) et [34](/directive-2016-680/chapitre-iv/section-3/article-34) de la directive détaillent les fonctions et les missions de ce délégué qui comprennent notamment celles d’informer et de conseiller le responsable du traitement et de contrôler le respect de la directive.

 **1.1.1.2. Conformité du droit national**

1.1.1.2.1. Sur l’obligation de mise à jour des données incombant aux autorités compétentes

L’actuelle loi informatique et libertés ne fixe pas d’obligation pour les autorités compétentes de veiller à la qualité des données transmises.

Cette obligation nouvelle fixée par l’[article 7 de la directive](/directive-2016-680/chapitre-ii/article-7) devra donc faire l’objet d’une transposition en droit national.

1.1.1.2.2 Sur les obligations incombant aux responsables de traitement et sous-traitants

1° Sur la distinction entre les différentes catégories de personnes concernées

Le droit français opère déjà des distinctions selon les différentes catégories de personnes concernées.

A titre d’illustration, les conditions d’inscription dans le traitement, les durées de conservations et les conditions d’effacement des données sont modulées dans le traitement des antécédents judiciaires (TAJ), dans le FNAEG et dans le FAED, selon que la personne concernée est une victime, un mis en cause majeur ou mineur.

Néanmoins, notre législation n’impose pas de manière générale au responsable du traitement d’opérer une distinction claire entre les données à caractère personnel de différentes catégories de personnes concernées.

Les dispositions de l’[article 6 de la directive](/directive-2016-680/chapitre-ii/article-6) devront donc être transposées.

2° Sur les règles destinées à assurer la sécurité des données personnelles

S’agissant des règles destinées à assurer l’intégrité et la sécurité des données personnelles, les mesures prises par le responsable de traitement font l’objet d’un examen lors des formalités préalables à la création.

L’[article 30 de la loi informatique et libertés](/loi-78-17/chapitre-iv/section-3/article-30) prévoit en effet au titre de son 9° que les demandes d’avis adressées à la Commission nationale de l’informatique et des libertés doivent notamment préciser « _les dispositions prises pour assurer la sécurité des traitements et des données et la garantie des secrets protégés par la loi_ ». La Commission opère donc un contrôle de la sécurité du traitement au stade de sa création.

Par ailleurs, l’[article 34 de la loi](/loi-78-17/chapitre-v/section-1/article-34) impose de manière générale au responsable du traitement de prendre toutes précautions utiles, au regard de la nature des données et des risques présentés par le traitement, pour préserver la sécurité des données et, notamment, empêcher qu'elles soient déformées, endommagées, ou que des tiers non autorisés y aient accès. Des décrets, pris après avis de la Commission nationale de l'informatique et des libertés, peuvent fixer les prescriptions techniques auxquelles doivent se conformer les traitements mentionnés au [2° et au 6° du II de l'article 8](/loi-78-17/chapitre-ii/section-2/article-8).

La [loi modifiée du 6 janvier 1978](/loi-78-17) impose donc aux responsables de traitement une obligation de sécurité et de confidentialité.

Cette obligation n’apparait néanmoins pas suffisante au regard des exigences européennes, et plus particulièrement à l’aune de la nécessité d’une protection des données par défaut, d’une obligation de sécurité renforcée pour toutes les données sensibles visées à l’[article 8](/loi-78-17/chapitre-ii/section-2/article-8) et de la liste des mesures à mettre en œuvre dans le cadre d’un traitement automatisé.

Les articles [19](/directive-2016-680/chapitre-iv/section-1/article-19), [20](/directive-2016-680/chapitre-iv/section-1/article-20) et [29](/directive-2016-680/chapitre-iv/section-2/article-29) de la directive devront donc faire l’objet d’une transposition.

3° Sur l’obligation de tenue d’un registre et d’un journal

N’ayant pas d’équivalent dans la loi informatique et libertés, les obligations nouvelles pour le responsable du traitement et pour le sous-traitant de tenir un registre de toutes les activités de traitement effectuées, ainsi qu’un journal de certaines opérations effectuées dans des systèmes de traitement automatisé afin de pouvoir démontrer la licéité du traitement et de garantir l’intégrité et la sécurité des données, devront faire l’objet d’une transposition.

4° Sur les obligations d’information en cas de violation de données à caractère personnel

L’[article 34 bis de la loi modifiée du 6 janvier 1978](/loi-78-17/chapitre-v/section-1/article-34-bis) issu de l'[ordonnance n° 2011-1012 du 24 août 2011](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000024502658&dateTexte=20180114) fixe les règles à suivre en cas de violation de données à caractère personnel.

Ces règles ne concernent cependant pas toutes les hypothèses de violation. Elles sont en effet limitées aux violations de données à caractère personnel intervenant dans des traitements des données à caractère personnel mis en œuvre dans le cadre de la fourniture au public de services de communications électroniques sur les réseaux de communications électroniques ouverts au public, y compris ceux prenant en charge les dispositifs de collecte de données et d'identification.

En pratique, sont dès lors uniquement soumis à l'[article 34 bis de la loi modifiée du 6 janvier 1978](/loi-78-17/chapitre-v/section-1/article-34-bis) les fournisseurs d'accès à l'Internet et les opérateurs de téléphonie mobile.

Les exigences européennes fixées par les articles [30](/directive-2016-680/chapitre-iv/section-2/article-30) et [31](/directive-2016-680/chapitre-iv/section-2/article-31) de la directive en cas de violation de données à caractère personnel étant applicables à tous les traitements, elles devront donc être transposées en droit national.

5° Sur l’obligation de désigner un délégué à la protection des données

L’[article 22 III de la loi informatique et libertés](/loi-78-17/chapitre-iv/article-22) et les [articles 42 à 56 du décret du 20 octobre 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000241445) prévoient la possibilité pour le responsable du traitement de désigner un correspondant à la protection des données.

Par cette désignation facultative, le droit français n’est donc pas conforme aux exigences européennes qui prévoient la désignation obligatoire du délégué.

 ** _1.2. Cadre constitutionnel_**

Dans sa [décision n° 2004-499 DC du 29 juillet 2004 portant sur la loi relative à la protection des personnes physiques à l'égard des traitements de données à caractère personnel](http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2004/2004-499-dc/decision-n-2004-499-dc-du-29-juillet-2004.904.html) et modifiant la [loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés](/loi-78-17), le Conseil constitutionnel a conclu que le fait, par la création d’un correspondant à la protection des données, de dispenser le responsable du traitement de certaines formalités préalables sauf exception était conforme à la Constitution.

Le Conseil a notamment considéré que compte tenu de l'ensemble des précautions prises, s'agissant en particulier de la qualification, du rôle et de l'indépendance du correspondant, la dispense de déclaration résultant de sa désignation ne privait de garanties légales aucune exigence constitutionnelle121.

 ** _1.3. Cadre conventionnel_**

[L’article 8 paragraphe 2 de la Charte des droits fondamentaux de l’Union européenne](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A12012P%2FTXT) prévoit que toute personne a le droit à la protection des données à caractère personnel la concernant, et que le respect de cette règle est soumis au contrôle d’une autorité indépendante.

Aux termes de [l’article 7 de Convention pour la protection des personnes à l’égard du traitement automatisé des données à caractère personnel du 28 janvier 1981](http://assiste.com.free.fr/p/droit/europe_convention_108_protection_des_personnes_a_l_egard_du_traitement_automatise_des_donnees.html), des « _mesures de sécurité appropriées sont prises pour la protection des données à caractère personnel enregistrées dans des fichiers automatisés contre la destruction accidentelle ou non autorisée, ou la perte accidentelle, ainsi que contre l'accès, la modification ou la diffusion non autorisés_ ».

 ** _2\. Objectifs poursuivis et necessite de legiferer_**

 ** _2.1 Objectifs poursuivis_**

 **2.2.1. Mettre notre droit en conformité avec l’obligation de veiller à la fiabilité des données personnelles transmises ou mises à disposition**

Afin de respecter l’[article 7 paragraphe 2 de la directive](/directive-2016-680/chapitre-ii/article-7), il est créé un [nouvel article 70-11](/pjl-490/titre-iii/article-19) qui reprend strictement les exigences européennes, et plus particulièrement l’obligation nouvelle pour les autorités compétentes de prendre toutes les mesures raisonnables pour garantir que les données à caractère personnel qui sont inexactes, incomplètes ou ne sont plus à jour soient effacées ou rectifiées sans tarder ou ne soient pas transmises ou mises à disposition.

Le [dernier alinéa de l’article 70-11](/pjl-490/titre-iii/article-19) transpose l’[article 7 paragraphe 3 de la directive](/directive-2016-680/chapitre-ii/article-7), et prévoit ainsi que le destinataire est informé sans retard en cas de transmission de données personnelles inexactes ou de transmission illicite, et que les données sont alors rectifiées ou effacées ou leur traitement limité.

**2.2.2. Mettre notre droit en conformité avec la nécessité pour le responsable du traitement d’établir une distinction claire entre les données à caractère personnel de différentes catégories de personnes concernées**

Pour mettre notre droit en conformité avec l’[article 6 de la directive](/directive-2016-680/chapitre-ii/article-6) qui impose de distinguer les données personnelles selon des catégories de personnes concernées, il est créé un article [__ 70-12 _._](/pjl-490/titre-iii/article-19)

Cet article reprend intégralement les dispositions de la directive et impose ainsi au responsable du traitement de distinguer entre les données à caractère personnel des catégories suivantes de personnes concernées :

1° Les personnes à l'égard desquelles il existe des motifs sérieux de croire qu'elles ont commis ou sont sur le point de commettre une infraction pénale;

2° Les personnes reconnues coupables d'une infraction pénale;

3° Les victimes d'une infraction pénale ou les personnes à l'égard desquelles certains faits portent à croire qu'elles pourraient être victimes d'une infraction pénale; et

4° Les tiers à une infraction pénale, tels que les personnes pouvant être appelées à témoigner lors d'enquêtes en rapport avec des infractions pénales ou des procédures pénales ultérieures, des personnes pouvant fournir des informations sur des infractions pénales, ou des contacts ou des associés de l'une des personnes visées aux 1° et 2°.

 **2.2.3. Mettre notre droit en conformité avec les obligations imposées au responsable du traitement et au sous-traitant en vue de garantir l’intégrité et la sécurité des données**

Pour plus de lisibilité, a été retenue l’option, au sein du [nouvel article 70-13](/pjl-490/titre-iii/article-19), d’une transposition combinée des articles [19](/directive-2016-680/chapitre-iv/section-1/article-19), [20](/directive-2016-680/chapitre-iv/section-1/article-20) et [29](/directive-2016-680/chapitre-iv/section-2/article-29) de la directive, qui imposent au responsable du traitement et au sous-traitant de garantir l’intégrité et la sécurité des données.

Les dispositions figurant dans les articles [19](/directive-2016-680/chapitre-iv/section-1/article-19), [20](/directive-2016-680/chapitre-iv/section-1/article-20) et [29§1](/directive-2016-680/chapitre-iv/section-2/article-29) de la directive étant identiques à celles prévues par les [paragraphes 1 et 2 des articles 24](/reglement-2016-679/chapitre-iv/section-1/article-24) et [25](/reglement-2016-679/chapitre-iv/section-1/article-25) du règlement, un renvoi à ces articles du règlement est opéré dans l’article 70-13.

L’[article 70-13](/pjl-490/titre-iii/article-19) transpose par ailleurs les dispositions de la directive qui n’ont pas d’équivalent ni dans la loi informatique et libertés ni dans le règlement.

Ainsi, le nouvel article 70-13 ajoute que le responsable du traitement et le sous-traitant mettent en œuvre les mesures techniques et organisationnelles appropriées afin degarantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l'[article 8](/loi-78-17/chapitre-ii/section-2/article-8).

En ce qui concerne le traitement automatisé, l’article 70-13 reprend intégralement la liste des mesures devant être mises en œuvre par le responsable du traitement et le sous-traitant aux termes de l’[article 29§2 de la directive](/directive-2016-680/chapitre-iv/section-2/article-29).

 **2.2.4. Mettre notre droit en conformité avec les obligations nouvelles pour le responsable du traitement et le sous-traitant de tenir un registre des activités de traitement effectuées, ainsi qu’un journal de certaines opérations**

Afin de respecter les exigences européennes fixées par l’[article 24 de la directive](/directive-2016-680/chapitre-iv/section-1/article-24), il est créé un [nouvel article 70-14](/pjl-490/titre-iii/article-19) qui prévoit que le responsable du traitement et son sous-traitant tiennent un registre des activités de traitement, et le mettent à la disposition de la Commission nationale de l’informatique et des libertés sur demande.

Les conditions dans lesquelles ce registre doit être tenu sont prévues de manière similaire par l’article 24 de la directive et les paragraphes 1 à 4 de l’article 30 du règlement.

Dès lors, le [nouvel article 70-14](/pjl-490/titre-iii/article-19) opère un renvoi aux [paragraphes 1 à 4 de l’article 30 du règlement](/reglement-2016-679/chapitre-iv/section-1/article-30), et ne réécrit que les dispositions propres à la directive, soit la description générale des mesures visant à garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l'article 8, l’indication de la base juridique de l’opération de traitement, y compris les transferts, à laquelle les données à caractère personnel sont destinées et, le cas échéant, le recours au profilage.

Le [nouvel article 70-15](/pjl-490/titre-iii/article-19) prévoit, conformément à l’[article 25 de la directive](/directive-2016-680/chapitre-iv/section-1/article-25), que le responsable du traitement ou son sous-traitant établit pour chaque traitement automatisé un journal des opérations de collecte, de modification, de consultation, de communication, y compris les transferts, l’interconnexion et l’effacement, portant sur de telles données.

Les journaux des opérations de consultation et de communication permettent d’en établir le motif, la date et l’heure. Ils permettent également, dans la mesure du possible, d’identifier les personnes qui consultent ou communiquent les données et leurs destinataires.

Ce journal est uniquement utilisé à des fins de vérification de la licéité du traitement, d’autocontrôle, de garantie de l’intégrité et de la sécurité des données et à des fins de procédures pénales. Il est mis à disposition de la Commission nationale de l’informatique et des libertés à sa demande.

Enfin, de manière générale, le [nouvel article 70-16](/pjl-490/titre-iii/article-19)  impose au responsable de traitement et au sous-traitant, par le renvoi à l’[article 31 du règlement](/reglement-2016-679/chapitre-iv/section-1/article-31) qui est identique à l’[article 26 de la directive](/directive-2016-680/chapitre-iv/section-1/article-26), de coopérer avec la Commission nationale de l'informatique et des libertés à la demande de celle-ci dans l’exécution de ses missions.

 **2.2.5. Mettre notre droit en conformité avec les règles fixées en cas de violation de données à caractère personnel**

Afin de respecter les exigences européennes en cas de violation de données personnelles dans tout type de traitement relevant du champ d’application de la directive, l’[article 70-16](/pjl-490/titre-iii/article-19) transpose les deux obligations d’information mises à la charge du responsable du traitement par les articles [30](/directive-2016-680/chapitre-iv/section-2/article-30) et [31](/directive-2016-680/chapitre-iv/section-2/article-31) de la directive : la notification de la violation à la Commission nationale de l’informatique et des libertés et la communication de la violation à la personne concernée.

Les articles [30](/directive-2016-680/chapitre-iv/section-2/article-30) et [31](/directive-2016-680/chapitre-iv/section-2/article-31) de la directive étant identiques aux articles [33](/reglement-2016-679/chapitre-iv/section-2/article-33) et [34](/reglement-2016-679/chapitre-iv/section-2/article-34) du règlement, excepté le paragraphe 6 de l’article 30 et le paragraphe 5 de l’article 31, l’[article 70-16](/pjl-490/titre-iii/article-19) opère un renvoi à ces dispositions du règlement.

Les dispositions propres des [paragraphe 6 de l’article 30](/directive-2016-680/chapitre-iv/section-2/article-30) et [paragraphe 5 de l’article 31](/directive-2016-680/chapitre-iv/section-2/article-31) de la directive sont, quant à elles, reproduites aux [deuxième et troisième alinéas de l’article 70-16](/pjl-490/titre-iii/article-19) qui prévoit ainsi que :

\- le responsable du traitement notifie également la violation au responsable du traitement de l’autre Etat membre dans les meilleurs délais si cette violation porte sur des données à caractère personnel qui ont été transmises par le responsable du traitement d’un autre Etat membre ou à celui-ci,

\- la communication d’une violation de données à caractère personnel à la personne concernée peut être retardée, limitée ou ne pas être délivrée aussi longtemps qu’une telle mesure est nécessaire et proportionnée dans une société démocratique, en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne physique concernée, pour ne pas mettre en danger la sécurité publique, la sécurité nationale ou les droits ou libertés d’autrui ou ne pas faire obstacle au bon déroulement des enquêtes et procédures destinées à prévenir, détecter ou poursuivre des infractions pénales ou à exécuter des sanctions pénales.

**2.2.6. Mettre notre droit en conformité avec l’obligation de désigner un délégué à la protection des données**

A cette fin, il est créé un [article 70-17](/pjl-490/titre-iii/article-19) qui transpose les articles [32](/directive-2016-680/chapitre-iv/section-3/article-32) à [34](/directive-2016-680/chapitre-iv/section-3/article-34) de la directive.

Cet article renvoie aux dispositions des [paragraphes 5 et 7 de l’article 37](/reglement-2016-679/chapitre-iv/section-4/article-37), des [paragraphes 1 et 2 de l’article 38](/reglement-2016-679/chapitre-iv/section-4/article-38) et du [paragraphe 1 de l’article 39](/reglement-2016-679/chapitre-iv/section-4/article-39) du règlement qui sont identiques à celles de la directive, à une exception près. Contrairement à la directive qui n’impose les obligations relatives au délégué à la protection des données qu’au responsable du traitement, certaines dispositions du règlement les font également peser sur le sous-traitant. L’[article 70-15](/pjl-490/titre-iii/article-19) prévoit dès lors que ce renvoi au règlement ne concerne que le responsable de traitement.

L’[article 70-15](/pjl-490/titre-iii/article-19) reprend par ailleurs littéralement l’[article 32§3 de la directive](/directive-2016-680/chapitre-iv/section-3/article-32), qui n’a pas d’équivalent dans le règlement, en prévoyant qu’un seul délégué à la protection des données peut être désigné pour plusieurs autorités compétentes, compte tenu de leur structure organisationnelle et de leur taille.

 ** _2.2 Nécessité de legiférer_**

L’objectif immédiat du projet de loi est de mettre notre droit national en conformité avec les exigences énoncées par la directive, plus particulièrement avec l’obligation nouvelle pour les autorités compétentes de veiller à la fiabilité des données personnelles transmises ou mises à disposition ; ainsi que pour le responsable du traitement, et également dans certains cas pour le sous-traitant :

\- d’établir une distinction claire entre les données à caractère personnel de différentes catégories de personnes concernées,

\- de prendre des mesures appropriées en vue de garantir l’intégrité et la sécurité des données,

\- de tenir un registre des activités de traitement effectuées, et un journal de certaines opérations effectuées dans des systèmes de traitement automatisé,

\- d’informer l’autorité de contrôle et la personne concernée en cas de violation de données à caractère personnel dans tout traitement,

\- de désigner un délégué à la protection des données.

**_3\. Options_**

 **3.1. Dispenser ou non les tribunaux et autorités judiciaires indépendantes de l’obligation de désigner un délégué à la protection des données dans l’exercice de leurs fonctions juridictionnelles**

 **3.1.1. Option 1 (écartée) : Imposer la désignation d’un délégué à la protection des données aux tribunaux et autorités judiciaires indépendantes agissant dans l’exercice de leurs fonctions juridictionnelles**

L’[article 32 paragraphe 1](/directive-2016-680/chapitre-iv/section-3/article-32) de la directive laisse à la libre appréciation des Etats membres la faculté de dispenser les tribunaux et d’autres autorités judiciaires indépendantes de l’obligation de désigner un délégué à la protection des données lorsqu’elles agissent dans l’exercice de leur fonction juridictionnelle.

Cette faculté est rappelée par le [considérant 63 de la directive](/directive-2016-680/considerants/considerant-63), qui précise que « _le responsable du traitement devrait désigner une personne qui l'aiderait à vérifier le respect, au niveau interne, des dispositions adoptées en vertu de la présente directive, sauf lorsqu'un État membre décide que des tribunaux et d'autres autorités judiciaires indépendantes en sont dispensés dans l'exercice de leur fonction juridictionnelle_ ».

Cette option a été écartée.

 **3.1.2. Option 2 (retenue) : Les dispenser de désigner un délégué à la protection des données**

S’agissant de traitements mis en œuvre par des autorités judiciaires dans l’exercice de leurs fonctions, l’option de ne pas leur imposer la désignation d’un délégué à la protection des données a été retenue.

 **3.2. Reporter ou non l’obligation de journalisation**

Comme indiqué dans la partie relative à l’entrée en vigueur, il a été décidé de permettre un report de cette obligation, ce que permet l’[article 63 de la directive](/directive-2016-680/chapitre-x/article-63).

**_4\. analyse des impacts de la disposition envisagée_**

 ** _4.1 Impacts juridiques_**

La présente disposition conduit à devoir insérer dans la [loi de 1978](/loi-78-17) de nouveaux articles ([articles 70-11 à 70-17](/pjl-490/titre-iii/article-19)).

<table>
<tr>
<td>

**DISPOSITIONS DU PROJET DE LOI**

</td>
<td>

[**DIRECTIVE (UE) 2016/680 DU PARLEMENT EUROPÉEN ET DU CONSEIL du 27 avril 2016**](/directive-2016-680/chapitre-ii)

</td></tr>
<tr>
<td>

[**Article 70-11**](/pjl-490/titre-iii/article-19)

</td>
<td>

Transposition des articles [4](/directive-2016-680/chapitre-ii/article-4) (« sans tarder ») et [7§2 et §3](/directive-2016-680/chapitre-ii/article-7)

</td></tr>
<tr>
<td>

[**Article 70-12**](/pjl-490/titre-iii/article-19)

</td>
<td>

[Article 6](/directive-2016-680/chapitre-ii/article-6)

</td></tr>
<tr>
<td>

[**Article 70-130**](/pjl-490/titre-iii/article-19)

</td>
<td>

Transposition combinée des articles [19](/directive-2016-680/chapitre-iv/section-1/article-19), [20](/directive-2016-680/chapitre-iv/section-1/article-20), [22§1](/directive-2016-680/chapitre-iv/section-1/article-22) et [29](/directive-2016-680/chapitre-iv/section-2/article-29)

</td></tr>
<tr>
<td>

[**Article 70-14**](/pjl-490/titre-iii/article-19)

</td>
<td>

[Article 24](/directive-2016-680/chapitre-iv/section-1/article-24)

</td></tr>
<tr>
<td>

[**Article 70-15**](/pjl-490/titre-iii/article-19)

</td>
<td>

[Article 25](/directive-2016-680/chapitre-iv/section-1/article-25)

</td></tr>
<tr>
<td>

[**Article 70-16**](/pjl-490/titre-iii/article-19)

</td>
<td>

Transposition combinée des articles [30](/directive-2016-680/chapitre-iv/section-2/article-30
) et [31](/directive-2016-680/chapitre-iv/section-2/article-31)

</td></tr>
<tr>
<td>

[**Article 70-17**](/pjl-490/titre-iii/article-19)

</td>
<td>

Transposition combinée des [articles 32 à 34](/directive-2016-680/chapitre-iv/section-3)

</td></tr></table>

**_4.2. Impacts sur les services judiciaires et autres traitements existants_**

S’agissant notamment de l’obligation de journalisation prévue par l'[article 25 de la directive](/directive-2016-680/chapitre-iv/section-1/article-25) et reprise par le [nouvel article 70-15](/pjl-490/titre-iii/article-19) il peut être fait les observations suivantes.

Actuellement, la loi "Informatique et Libertés" n'a pas de disposition spécifique concernant cette traçabilité. Toutefois l'[article 34](/loi-78-17/chapitre-v/section-1/article-34) de la loi fait peser sur les responsables de traitement des obligations de sécurité:

Cet article prévoit en effet que " _Le responsable du traitement est tenu de prendre toutes précautions utiles, au regard de la nature des données et des risques présentés par le traitement, pour préserver la sécurité des données et, notamment, empêcher qu'elles soient déformées, endommagées, ou que des tiers non autorisés y aient accès_ ".

La CNIL est particulièrement attentive à la mise en œuvre de telles mesures de traçabilité, dans la mesure où cela permet de détecter notamment les détournements de finalité et de s'assurer, a posteriori, que seules les personnes ayant besoin d'en connaître ont eu accès aux données. D'ailleurs, sur certains traitements particulièrement sensibles (Cassiopée notamment), elle a même recommandé ([Délibération n°2011-233 du 21 juillet 2011](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025825985&dateTexte=20120508&categorieLien=cid)) la mise en place d'un mécanisme de remontée d'alerte (outil de détection des usages anormaux).

C'est en application de cet [article 34](/loi-78-17/chapitre-v/section-1/article-34) que le ministère de la justice, pour l'ensemble de ses traitements (ou à tout le moins pour les plus récents), met déjà en œuvre des mesures de traçabilité, qui permettent d'assurer la sécurité des données. Le plus souvent (notamment pour les traitements relevant de l'[article 26 de la loi "Informatique et Libertés"](/loi-78-17/chapitre-iv/section-2/article-26)), ces traces sont conservées par le ministère de la justice pendant 3 ans (durée beaucoup plus longue que dans le secteur privé). Dans la mesure où cette durée correspondait jusqu'à récemment à la durée de prescription de l'infraction de détournement de finalité, prévue à l'[article 226-21 du code pénal](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417981), la Commission considérait que cette durée ne soulevait pas de difficulté.

Par ailleurs, les actes réglementaires entourant la mise en œuvre des traitements mentionnent le plus souvent expressément ces mesures de traçabilité (alors même que cela n'est pas une obligation au titre de l'[article 29 de la loi](/loi-78-17/chapitre-iv/section-2/article-29)). On peut citer comme exemple :

\- Cassiopée: [article R. 15-33-66-13 du CPP](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000025832168&cidTexte=LEGITEXT000006071154&dateTexte=20180115&categorieLien=id&oldAction=&nbResultRech=)

\- Genesis: [article R. 57-9-26 du CPP](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000029018055&cidTexte=LEGITEXT000006071154&dateTexte=20180115&categorieLien=id&oldAction=&nbResultRech=)

Toutefois, l'[article 25 de la directive](/directive-2016-680/chapitre-iv/section-1/article-25), comme le [nouvel article 70-15](/pjl-490/titre-iii/article-19), est précis, en ce sens qu'il décrit limitativement les fins pour lesquelles ces traces pourront être utilisées (vérification de la licéité du traitement, autocontrôle, garantie de l'intégrité et de la sécurité des données, procédure pénale), ce qui n'est pas le cas de la loi "Informatique et Libertés";

Or, actuellement, si les créations, modifications, suppressions et les consultations de données sont les actions le plus souvent tracées, il n'est pas sûr que la communication et l'interconnexion le soient systématiquement. Ce point nécessitera donc une confirmation des services compétents en la matière, afin que ces derniers précisent quelles sont les actions qui sont tracées, les traces générées et les éventuelles modifications informatiques à prévoir afin de s'assurer du respect de l’article 70-15 (il en va de même pour la mise en œuvre de telles mesures sur les applicatifs les plus anciens, qui pourraient éventuellement soulever des difficultés), afin de déterminer dans quelle mesure la possibilité de report doit être retenue pour tels ou tels fichiers.

Cette question concerne également les traitements relevant du ministère de l’intérieur ou d’autres ministères.

 ** _5\. CONSULTATION ET MODALITÉS D’APPLICATION_**

La Commission nationale de l’informatique et des libertés a été consultée.

La réforme s’appliquera le 25 mai 2018, conformément à l’[article 24 du projet de loi](/pjl-490/titre-v/article-24).

Ces dispositions seront applicables sur l’ensemble du territoire, hors les collectivités d’outre-mer soumises au principe de spécialité, pour lesquelles l’extension se fera par ordonnance.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-iii/article-19-section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-iii/article-18-et-article-19-section-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

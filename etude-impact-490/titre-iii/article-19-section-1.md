<!-- TITLE: ARTICLE 19 SECTION 1 -->
<!-- SUBTITLE: DISPOSITIONS GENERALES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-iii/articles-18-et-19" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-iii/article-19-section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

 ** _1\. Etat des lieux_**

 **1.1 Conformité du droit français à la directive sur les droits des personnes concernées par les données personnelles**

 **1.1.1 Texte de la directive**

 **1.1.1.1 Champ d’application de la directive**

La [directive 2016/680](/directive-2016-680) remplace la [décision-cadre 2008/977/JAI du 27 novembre 2008](http://cdre.eu/82-documentation-en-ligne/justice/droit-penal-materiel/231-decision-cadre-2008-977-jai-du-conseil-du-27-novembre-2008-relative-a-la-protection-des-donnees-a-caractere-personnel-traitees-dans-le-cadre-de-la-cooperation-policiere-et-judiciaire-en-matie) relative à la protection des données à caractère personnel traitées dans le cadre de la coopération policière et judiciaire en matière pénale.

Contrairement à l’instrument précité, son champ d’application n’est pas limité aux échanges de données à caractère personnel dans le cadre de la coopération en matière pénale, mais s’étend à tous les traitements de telles données y compris ceux ne présentant aucun caractère transfrontalier.

La directive s’applique, aux termes de ses article [1](/directive-2016-680/chapitre-i/article-premier) et [2](/directive-2016-680/chapitre-i/article-2), aux traitements de données à caractères personnel par les autorités compétentes des Etats membres aux fins de prévention et de détection des infractions pénales, d’enquêtes et de poursuites en la matière ou d’exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces.

Elle ne s’applique en revanche pas, comme le précise son article 2, aux traitements mis en œuvre dans le cadre d’une activité qui se situe hors du champ du droit de l’Union européenne (activités des services de renseignement notamment). La directive ne s’applique pas non plus au traitement de données effectué par les institutions européennes, qui sont soumises au [règlement 45/2001](http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2001:008:0001:0022:FR:PDF).

L’autorité compétente au sens de l’[article 3](/directive-2016-680/chapitre-i/article-3) de la directive correspond à :

\- toute autorité publique compétente pour la prévention et la détection des infractions pénales, les enquêtes et les poursuites en la matière ou l'exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces ;

\- ou à tout autre organisme ou entité à qui le droit d'un État membre confie l'exercice de l'autorité publique et des prérogatives de puissance publique à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces.

L’articulation entre la directive et le règlement (UE) 2016/679 s’opère conformément aux considérants [11](/directive-2016-680/considerants/considerant-11) et [12](/directive-2016-680/considerants/considerant-12) de la directive.

Aux termes du [considérant 11](/directive-2016-680/considerants/considerant-11), « _les autorités compétentes peuvent comprendre non seulement les autorités publiques telles que les autorités judiciaires, la police ou d'autres autorités répressives mais aussi tout autre organisme ou entité à qui le droit d'un État membre confie l'exercice de l'autorité publique et des prérogatives de puissance publique aux fins de la présente directive ». « Lorsqu'un tel organisme ou une telle entité traite des données à caractère personnel à des fins autres que celles prévues dans la présente directive, le règlement (UE) 2016/679 s'applique. Par conséquent, le règlement (UE) 2016/679 s'applique lorsqu'un organisme ou une entité recueille des données à caractère personnel à d'autres fins et les traite ultérieurement pour respecter une obligation légale à laquelle il est soumis. Par exemple, les établissements financiers conservent, à des fins de détection ou de poursuites d'infractions pénales ou d'enquêtes en la matière, certaines données à caractère personnel qu'ils traitent et qu'ils ne transmettent aux autorités nationales compétentes que dans des cas spécifiques et conformément au droit des États membres »._

 _« Un organisme ou une entité qui traite des données à caractère personnel pour le compte de ces autorités dans le cadre du champ d'application de la présente directive devrait être lié par un contrat ou un autre acte juridique et par les dispositions applicables aux sous-traitants en vertu de la présente directive, le règlement (UE) 2016/679 continuant de s'appliquer aux traitements de données à caractère personnel par le sous-traitant en dehors du champ d'application de la présente directive_ ».

Le [considérant 12](/directive-2016-680/considerants/considerant-12) indique notamment que relèvent de la directive les traitements concernant des « _activités menées par la police ou d'autres autorités répressives [qui] sont axées principalement sur la prévention et la détection des infractions pénales et les enquêtes et les poursuites en la matière, y compris les activités de police effectuées sans savoir au préalable si un incident constitue une infraction pénale ou non»._ Il précise que _« ces activités peuvent également comprendre l'exercice de l'autorité par l'adoption de mesures coercitives, par exemple les activités de police lors de manifestations, de grands événements sportifs et d'émeutes»,_ et que _« parmi ces activités figure également le maintien de l'ordre public lorsque cette mission est confiée à la police ou à d'autres autorités répressives lorsque cela est nécessaire à des fins de protection contre les menaces pour la sécurité publique et pour les intérêts fondamentaux de la société protégés par la loi, et de prévention de telles menaces, qui sont susceptibles de déboucher sur une infraction pénale »._ Il indique en revanche, qu’entrent dans le champ d’application du règlement, pour autant qu'ils relèvent du droit de l'Union, les traitements par lesquels _« les États membres [confient] aux autorités compétentes d'autres missions qui ne sont pas nécessairement menées à des fins de prévention et de détection des infractions pénales, d'enquêtes ou de poursuites en la matière, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces_ ».

Le champ d’application de la directive est donc déterminé par deux conditions cumulatives :

\- l’une porte sur l’autorité chargée de traiter les données à caractère personnel, qui doit être une autorité compétente au sens de l’article 3 de la directive ;

\- l’autre sur les finalités du traitement, qui doit être effectué aux fins de prévention et de détection des infractions pénales, d’enquêtes et de poursuites en la matière ou d’exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces.

A défaut de l’une des deux conditions et pour autant que le traitement relève du droit de l’Union, le règlement (UE) 2016/679 s’applique.

L’[article 2](/reglement-2016-679/chapitre-i/article-2) du règlement confirme que cet instrument ne s'applique pas au traitement de données à caractère personnel effectué « _d) par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales, y compris la protection contre des menaces pour la sécurité publique et la prévention de telles menaces_ », terminologie qui correspond très exactement au champ d’application de la directive.

Le [considérant 19](/reglement-2016-679/considerants/considerant-19) du règlement précise cette exclusion en indiquant que « _La protection des personnes physiques à l'égard du traitement des données à caractère personnel par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces et la libre circulation de ces données, fait l'objet d'un acte juridique spécifique de l'Union. Le présent règlement ne devrait dès lors pas s'appliquer aux activités de traitement effectuées à ces fins. Toutefois, les données à caractère personnel traitées par des autorités publiques en vertu du présent règlement devraient, lorsqu'elles sont utilisées à ces fins, être régies par un acte juridique de l'Union plus spécifique, à savoir la directive (UE) 2016/680 du Parlement européen et du Conseil (1). Les États membres peuvent confier à des autorités compétentes au sens de la directive (UE) 2016/680 des missions qui ne sont pas nécessairement effectuées à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces, de manière à ce que le traitement de données à caractère personnel à ces autres fins, pour autant qu'il relève du champ d'application du droit de l'Union, relève du champ d'application du présent règlement »._

Ce paragraphe indique clairement que les traitements pénaux réalisés par les autorités publiques relèvent de la seule directive, et que si ces autorités publiques mettent en œuvre des traitements à d’autres fins que pénales (mais qui relèvent du champ de l’Union), ces traitements relèvent du règlement.

 **1.1.1.2 Licéité du traitement**

Aux termes de l’[article 8](/directive-2016-680/chapitre-ii/article-8) de la directive, le traitement de données à caractère personnel en matière pénale n'est licite que si et dans la mesure où il est nécessaire à l'exécution d'une mission effectuée par une autorité compétente, pour les finalités énoncées à l'article premier de la directive, et où il est fondé sur le droit de l'Union ou le droit d'un État membre.

Par ailleurs, une disposition du droit d'un État membre qui réglemente le traitement relevant du champ d'application de la présente directive précise au moins les objectifs du traitement, les données à caractère personnel devant faire l'objet d'un traitement et les finalités du traitement.

 **1.1.1.3 Principes généraux et conditions spécifiques applicables au traitement**

Aux termes de l’[article 4](/directive-2016-680/chapitre-ii/article-4)§1 de la directive, les données à caractère personnel doivent être :

\- traitées de manière licite et loyale ;

\- collectées pour des finalités déterminées, explicites et légitimes et ne sont pas traitées d'une manière incompatible avec ces finalités ;

\- adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont traitées ;

\- exactes et, si nécessaire, tenues à jour; toutes les mesures raisonnables doivent être prises pour que les données à caractère personnel qui sont inexactes, eu égard aux finalités pour lesquelles elles sont traitées, soient effacées ou rectifiées sans tarder ;

\- conservées sous une forme permettant l'identification des personnes concernées pendant une durée n'excédant pas celle nécessaire au regard des finalités pour lesquelles elles sont traitées ;

\- traitées de façon à garantir une sécurité appropriée des données à caractère personnel, y compris la protection contre le traitement non autorisé ou illicite et contre la perte, la destruction ou les dégâts d'origine accidentelle, à l'aide de mesures techniques ou organisationnelles appropriées.

S’agissant des finalités du traitement, l’article 4§2 et §3 ainsi que l’[article 9](/directive-2016-680/chapitre-ii/article-9) de la directive précisent les règles applicables aux traitements initialement collectés pour l’une des finalités de la directive et ultérieurement effectués à d’autres fins.

L’article 4§2 de la directive prévoit ainsi que le traitement de données à caractère personnel pour une finalité relevant du champ d’application de la directive, autre que celles pour lesquelles les données ont été collectées, est autorisé à la double condition que conformément au droit de l'Union ou au droit national, le responsable du traitement soit autorisé à traiter ces données à caractère personnel pour une telle finalité, et que le traitement soit nécessaire et proportionné à cette autre finalité.

L’article 4§3 précise que ce traitement des données peut comprendre l'archivage dans l'intérêt public, à des fins scientifiques, statistiques ou historiques aux fins énoncées à l'article premier, sous réserve de garanties appropriées pour les droits et libertés de la personne concernée.

L’article 9 de la directive prévoit par principe que les données initialement collectées par les autorités compétentes pour une finalité relevant du champ d’application de la directive ne peuvent pas être traitées à d’autres fins, sauf exception prévue par le droit national ou le droit de l’Union.

L’article 9 précise que dans ce cas, autrement dit lorsque le traitement est effectué à d’autres fins que celles de la directive, y compris à des fins archivistiques dans l'intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques, le règlement (UE) 2016/679 s’applique, et ce même si l’autorité chargée du traitement est effectivement une autorité compétente au sens de la directive.

S’agissant des conditions spécifiques applicables au traitement, le paragraphe 3 de l’article 9 ajoute que, lorsque le droit de l'Union ou le droit d'un État membre applicable à l'autorité compétente qui transmet les données soumet le traitement à des conditions spécifiques, l'autorité compétente qui transmet les données doit informer le destinataire de ces données à caractère personnel de ces conditions et de l'obligation de les respecter.

Les conditions spécifiques sont précisées au [considérant 36 de la directive](/directive-2016-680/considerants/considerant-36), aux termes duquel « _Ces conditions pourraient, par exemple, comprendre une interdiction de transmission ultérieure des données à caractère personnel à autrui, une interdiction d'utilisation desdites données à des fins autres que celles pour lesquelles elles ont été transmises au destinataire, ou une interdiction d'informer la personne concernée lorsque le droit à l'information est limité en l'absence d'autorisation préalable de l'autorité compétente qui transmet les données_ ».

Selon la directive, cette obligation doit également « _s'appliquer aux transferts de données par l'autorité compétente qui transmet les données à des destinataires dans des pays tiers ou des organisations internationales_ ». Ainsi, le paragraphe 4 de l’[article 9](/directive-2016-680/chapitre-ii/article-9) prévoit que « _l'autorité compétente qui transmet les données n'applique pas aux destinataires dans les autres États membres ou aux services, organes et organismes établis en vertu du titre V, chapitres 4 et 5, du traité sur le fonctionnement de l'Union européenne des conditions différentes de celles applicables aux transferts de données similaires à l'intérieur de l'État membre dont relève ladite autorité compétente_ ».

Enfin, afin « _d’appliquer le principe d’exactitude des données_ » visé au [considérant 30](/directive-2016-680/considerants/considerant-30), l’[article 7](/directive-2016-680/chapitre-ii/article-7) de la directive prévoit que les données à caractère personnel fondées sur des faits sont, dans la mesure du possible, distinguées de celles fondées sur des appréciations personnelles.

 **1.1.1.4 Règles particulières à certains traitements ou catégories de données**

Les articles [10](/directive-2016-680/chapitre-ii/article-10) et [11](/directive-2016-680/chapitre-ii/article-11) de la directive fixent des règles particulières à certains traitements ou catégories de données.

L’article 10 traite des données à caractère personnel qui révèlent l'origine raciale ou ethnique, les opinions politiques, les convictions religieuses ou philosophiques, ou l'appartenance syndicale, et le traitement des données génétiques, des données biométriques aux fins d'identifier une personne physique de manière unique, des données concernant la santé ou des données concernant la vie sexuelle ou l'orientation sexuelle d'une personne physique.

Le traitement de telles données peut ainsi être autorisé en cas de nécessité absolue, sous réserve de garanties appropriées pour les droits et libertés de la personne concernée, et uniquement:

\- lorsqu'il est autorisé par le droit de l'Union **** ou le droit d'un État membre ;pour protéger les intérêts vitaux de la personne concernée ou d'une autre personne physique; ou

\- lorsqu’il porte sur des données manifestement rendues publiques par la personne concernée.

L’article 11 prévoit que toute décision fondée exclusivement sur un traitement automatisé, y compris le profilage, qui produit des effets juridiques défavorables pour la personne concernée ou l'affecte de manière significative, est interdite, à moins qu'elle ne soit autorisée par le droit de l'Union ou le droit national auquel le responsable du traitement est soumis et qui fournit des garanties appropriées pour les droits et libertés de la personne concernée, et au minimum le droit d'obtenir une intervention humaine de la part du responsable du traitement.

Ces décisions ne sont pas fondées sur les catégories particulières de données à caractère personnel visées à l'article 10, à moins que des mesures appropriées pour la sauvegarde des droits et des libertés et des intérêts légitimes de la personne concernée ne soient en place.

Tout profilage qui entraîne une discrimination à l'égard des personnes physiques sur la base des catégories particulières de données à caractère personnel visées à l'article 10 est interdit, conformément au droit de l'Union.

 **1.1.1.5 Règles applicables au traitement réalisé par un sous-traitant**

En vertu des articles [22](/directive-2016-680/chapitre-iv/section-1/article-22) et [23](/directive-2016-680/chapitre-iv/section-1/article-23) de la directive, le sous-traitant et toute personne agissant sous l'autorité du responsable du traitement ou sous celle du sous-traitant, qui a accès à des données à caractère personnel, ne les traite que sur instruction du responsable du traitement, à moins d'y être obligé par le droit de l'Union ou le droit national.

De manière générale, l’article 22 prévoit que le sous-traitant n'agit que sur instruction du responsable du traitement. Ainsi, si, en violation de la directive, un sous-traitant détermine les finalités et les moyens du traitement, il est considéré comme un responsable du traitement pour ce qui concerne ce traitement.

Aux termes de cet article, le sous-traitant doit notamment :

\- veiller à ce que les personnes autorisées à traiter les données à caractère personnel s'engagent à respecter la confidentialité ou soient soumises à une obligation légale appropriée de confidentialité ;

\- selon le choix du responsable du traitement, supprimer toutes les données à caractère personnel ou les renvoyer au responsable du traitement au terme de la prestation des services de traitement des données, et détruire les copies existantes, à moins que le droit de l’Union ou le droit national n’exige la conservation des données à caractère personnel.

Par ailleurs, le sous-traitant ne doit pas recruter un autre sous-traitant sans l’autorisation écrite préalable, spécifique ou générale, du responsable du traitement. Dans le cas d’une autorisation écrite générale, le sous-traitant informe le responsable du traitement de tout changement prévu concernant l’ajout ou le remplacement d’autres sous-traitants, donnant ainsi au responsable du traitement la possibilité d’émettre des objections à l’encontre de ces changements.

 **1.1.1.6 Effectuer une analyse d’impact relative à la protection des données**

Lorsqu'un type de traitement, en particulier par le recours aux nouvelles technologies, et compte tenu de la nature, de la portée, du contexte et des finalités du traitement, est susceptible d'engendrer un risque élevé pour les droits et les libertés des personnes physiques, l’[article 27](/directive-2016-680/chapitre-iv/section-1/article-27) de la directive impose que le responsable du traitement effectue préalablement au traitement une analyse de l'impact des opérations de traitement envisagées sur la protection des données à caractère personnel.

Cette analyse contient au moins une description générale des opérations de traitement envisagées, une évaluation des risques pour les droits et libertés des personnes concernées, les mesures envisagées pour faire face à ces risques, les garanties, mesures et mécanismes de sécurité visant à assurer la protection des données à caractère personnel et à apporter la preuve du respect de la directive, compte tenu des droits et des intérêts légitimes des personnes concernées et des autres personnes touchées.

L’[article 28](/directive-2016-680/chapitre-iv/section-1/article-28) de la directive prévoit, dans son premier paragraphe, que le responsable du traitement ou le sous-traitant consulte l'autorité de contrôle préalablement au traitement des données à caractère personnel qui fera partie d'un nouveau fichier à créer, soit lorsqu'une analyse d'impact relative à la protection des données indique que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque; soit lorsque le type de traitement, en particulier, en raison de l'utilisation de nouveaux mécanismes, technologies ou procédures, présente des risques élevés pour les libertés et les droits des personnes concernées.

Ce même article 28 impose, dans son paragraphe 4,que le responsable du traitement fournisse à l'autorité de contrôle l'analyse d'impact réalisée.

 **1.1.2 Conformité du droit national**

 **1.1.2.1 Règles relatives au champ d’application de la directive et à la licéité du traitement**

Les règles relatives au champ d’application de la directive et à la licéité du traitement, qui sont étroitement liées, devront faire l’objet d’une transposition.

 **1.1.2.2. Principes généraux et conditions spécifiques applicables au traitement**

L’[article 6](/loi-78-17/chapitre-ii/section-1/article-6) de loi informatique et libertés prévoit déjà qu’un traitement ne peut porter que sur des données à caractère personnel qui satisfont aux conditions suivantes :

1° Les données sont collectées et traitées de manière loyale et licite ;

2° Elles sont collectées pour des finalités déterminées, explicites et légitimes et ne sont pas traitées ultérieurement de manière incompatible avec ces finalités. Toutefois, un traitement ultérieur de données à des fins statistiques ou à des fins de recherche scientifique ou historique est considéré comme compatible avec les finalités initiales de la collecte des données, s'il est réalisé dans le respect des principes et des procédures prévus au présent chapitre, au [chapitre IV](/loi-78-17/chapitre-iv) et à la [section 1 du chapitre V](/loi-78-17/chapitre-v/section-1) ainsi qu'au [chapitre IX](/loi-78-17/chapitre-ix) et s'il n'est pas utilisé pour prendre des décisions à l'égard des personnes concernées ;

3° Elles sont adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont collectées et de leurs traitements ultérieurs ;

4° Elles sont exactes, complètes et, si nécessaire, mises à jour ; les mesures appropriées doivent être prises pour que les données inexactes ou incomplètes au regard des finalités pour lesquelles elles sont collectées ou traitées soient effacées ou rectifiées ;

5° Elles sont conservées sous une forme permettant l'identification des personnes concernées pendant une durée qui n'excède pas la durée nécessaire aux finalités pour lesquelles elles sont collectées et traitées.

Par ailleurs, l’[article 30](/loi-78-17/chapitre-iv/section-3/article-30) 9° de loi impose que les déclarations, demandes d'autorisation et demandes d'avis adressées à la Commission nationale de l'informatique et des libertés lors de la création d’un traitement précisent les dispositions prises pour assurer la sécurité des traitements et des données et la garantie des secrets protégés par la loi et, le cas échéant, l'indication du recours à un sous-traitant.

Enfin, en vertu de l’[article 34](/loi-78-17/chapitre-v/section-1/article-34) de la loi, le responsable du traitement est tenu de prendre toutes précautions utiles, au regard de la nature des données et des risques présentés par le traitement, pour préserver la sécurité des données et, notamment, empêcher qu'elles soient déformées, endommagées, ou que des tiers non autorisés y aient accès.

Le droit français est donc conforme aux exigences fixées par l’[article 4](/directive-2016-680/chapitre-ii/article-4)§1 de la directive.

En revanche, les dispositions des articles 4§2 et §3 et [9](/directive-2016-680/chapitre-ii/article-9) de la directive relatives aux traitements ultérieurs et aux conditions spécifiques applicables au traitement devront être transposées.

De même, n’ayant pas d’équivalent dans notre législation actuelle, la distinction, exigée par l’[article 7](/directive-2016-680/chapitre-ii/article-7) de la directive, entre les données à caractère personnel fondées sur des faits et sur des appréciations personnelles devra faire l’objet d’une transposition.

S’agissant des règles particulières à certains traitements ou catégories de données, l’article 8 I de loi informatique et libertés prévoit l’interdiction de principe de collecter ou de traiter des données à caractère personnel qui font apparaître, directement ou indirectement, les origines raciales ou ethniques, les opinions politiques, philosophiques ou religieuses ou l'appartenance syndicale des personnes, ou qui sont relatives à la santé ou à la vie sexuelle de celles-ci, sauf exception. Ne sont pas soumis à cette interdiction certains traitements mentionnés au [IV](/loi-78-17/chapitre-iv), notamment ceux qui sont justifiés par l’intérêt public et autorisés dans les conditions prévues au II de l’[article 26](/loi-78-17/chapitre-iv/section-2/article-26), autrement dit les fichiers intéressant la sûreté de l’Etat, la défense ou la sécurité publique ou qui ont pour objet la prévention, la recherche, la constatation ou la poursuite des infractions pénales ou l’exécution des condamnations pénales ou des mesures de sûreté portant sur des données mentionnés au I de l’[article 8](/loi-78-17/chapitre-ii/section-2/article-8).

Les règles nationales applicables aux traitements relevant du champ d’application de la directive ne sont donc pas conformes aux exigences de l’[article 10](/loi-78-17/chapitre-ii/section-2/article-10) de la directive, qui devra être transposé.

S’agissant par ailleurs des décisions individuelles automatisées, l’article 10 de la loi précise qu’aucune décision de justice impliquant une appréciation sur le comportement d'une personne ne peut avoir pour fondement un traitement automatisé de données à caractère personnel destiné à évaluer certains aspects de sa personnalité.

Il ajoute qu’aucune autre décision produisant des effets juridiques à l'égard d'une personne ne peut être prise sur le seul fondement d'un traitement automatisé de données destiné à définir le profil de l'intéressé ou à évaluer certains aspects de sa personnalité.

Il prévoit enfin que ne sont pas regardées comme prises sur le seul fondement d'un traitement automatisé les décisions prises dans le cadre de la conclusion ou de l'exécution d'un contrat et pour lesquelles la personne concernée a été mise à même de présenter ses observations, ni celles satisfaisant les demandes de la personne concernée.

Devra donc être transposé le dernier alinéa de l’[article 11](/loi-78-17/chapitre-iii/article-11) de la directive relatif au profilage, qui n’a pas d’équivalent dans la LIL.

 **1.1.2.3 Règles applicables au traitement réalisé par un sous-traitant**

Aux termes de l’[article 35](/loi-78-17/chapitre-v/section-1/article-35) de la loi informatique et libertés, les données à caractère personnel ne peuvent faire l'objet d'une opération de traitement de la part d'un sous-traitant, d'une personne agissant sous l'autorité du responsable du traitement ou de celle du sous-traitant, que sur instruction du responsable du traitement.

Toute personne traitant des données à caractère personnel pour le compte du responsable du traitement est considérée comme un sous-traitant au sens de la présente loi.

Le sous-traitant doit présenter des garanties suffisantes pour assurer la mise en œuvre des mesures de sécurité et de confidentialité mentionnées à l'article 34. Cette exigence ne décharge pas le responsable du traitement de son obligation de veiller au respect de ces mesures.

Le contrat liant le sous-traitant au responsable du traitement comporte l'indication des obligations incombant au sous-traitant en matière de protection de la sécurité et de la confidentialité des données et prévoit que le sous-traitant ne peut agir que sur instruction du responsable du traitement.

Le droit national est donc partiellement conforme aux exigences fixées par les articles [22](/directive-2016-680/chapitre-iv/section-1/article-22) et [23](/directive-2016-680/chapitre-iv/section-1/article-23) de la directive. Devront notamment être transposés le fait que le sous-traitant doit être considéré comme un responsable du traitement s’il détermine, en violation de la directive, les finalités et les moyens du traitement ; les règles de recours à un autre sous-traitant et certaines obligations mise à la charge du sous-traitant.

 **1.1.2.4 Effectuer une analyse d’impact relative à la protection des données**

N’ayant pas d’équivalent dans la loi informatique et libertés, l’exigence européenne d’effectuer dans certains cas une analyse d’impact relative à la protection des données devra faire l’objet d’une transposition.

 ** _1.2. Cadre constitutionnel_**

En matière de traitement de données, le Conseil constitutionnel est passé d’un contrôle limité à l’absence de disproportion manifeste à un contrôle de proportionnalité plus poussé.

Le Conseil constitutionnel a d’abord admis que les dispositions portant sur les traitements automatisés de données nominatives mis en œuvre par les services de la police nationale et de la gendarmerie nationale dans le cadre de leurs missions, auxquels s’applique la loi du 6 janvier 1978, prévoient un ensemble de garanties « _de nature à assurer, entre le respect de la vie privée et la sauvegarde de l’ordre public, une conciliation qui n’est pas manifestement déséquilibrée_ ». Il a par ailleurs jugé « _qu’aucune norme constitutionnelle ne s’oppose par principe à l’utilisation à des fins administratives de données nominatives recueillies dans le cadre d’activités de police judiciaire ; que, toutefois, cette utilisation méconnaîtrait les exigences résultant des articles 2, 4, 9 et 16 de la [Déclaration de 1789](https://www.legifrance.gouv.fr/Droit-francais/Constitution/Declaration-des-Droits-de-l-Homme-et-du-Citoyen-de-1789) si, par son caractère excessif, elle portait atteinte aux droits ou aux intérêts légitimes des personnes concernées_ »115.

Dans sa [décision n° 2012-652 DC du 22 mars 2012](http://www.conseil-constitutionnel.fr/decision/2012/2012-652-dc/decision-n-2012-652-dc-du-22-mars-2012.105165.html), il a précisé ses exigences en matière de contrôle de fichiers en affirmant que « _la liberté proclamée par l'article 2 de la Déclaration des droits de l'homme et du citoyen de 1789 implique le droit au respect de la vie privée. Par suite, la collecte, l'enregistrement, la conservation, la consultation et la communication de données à caractère personnel doivent être justifiés par un motif d'intérêt général et mis en œuvre de manière adéquate et proportionnée à cet objectif_ »116.

Dans l’exercice de ce contrôle de proportionnalité, le Conseil constitutionnel tient notamment compte du nombre de personnes susceptibles de relever du fichier informatique en cause, de la sensibilité particulière des données personnelles recueillies, des garanties techniques ou juridiques prévues par le législateur et des finalités d’utilisation ou de consultation du fichier.

**_1.3. Cadre conventionnel_**

L’article 8 paragraphe 2 de la [Charte des droits fondamentaux de l’Union européenne](www.europarl.europa.eu/charter/pdf/text_fr.pdf) prévoit que toute personne a le droit à la protection des données à caractère personnel la concernant, et que ces données doivent être traitées loyalement, à des fins déterminées et sur la base du consentement de la personne concernée ou en vertu d'un autre fondement légitime prévu par la loi.

Dans la [Convention pour la protection des personnes à l’égard du traitement automatisé des données à caractère personnel du 28 janvier 1981](https://rm.coe.int/1680078b39), l’article 5 prévoit que « _les données à caractère personnel faisant l'objet d'un traitement automatisé sont:_

 _a) obtenues et traitées loyalement et licitement; _

_b) enregistrées pour des finalités déterminées et légitimes et ne sont pas utilisées de manière incompatible avec ces finalités; _

_c) adéquates, pertinentes et non excessives par rapport aux finalités pour lesquelles elles sont enregistrées; _

_d) exactes et si nécessaire mises à jour; _

_e) conservées sous une forme permettant l'identification des personnes concernées pendant une durée n'excédant pas celle nécessaire aux finalités pour lesquelles elles sont enregistrées_ ».

Son article 6 précise que « _les données à caractère personnel révélant l'origine raciale, les opinions politiques, les convictions religieuses ou autres convictions, ainsi que les données à caractère personnel relatives à la santé ou à la vie sexuelle, ne peuvent être traitées automatiquement à moins que le droit interne ne prévoie des garanties appropriées. Il en est de même des données à caractère personnel concernant des condamnations pénales_. »

La Cour européenne des droits de l’homme attache une importance particulière à la protection des données à caractère personnel, qui joue selon elle un rôle fondamental pour l’exercice du droit au respect de la vie privée et familiale consacré par l’article 8 de la Convention117. Ainsi juge-t-elle que le simple fait de mémoriser des données relatives à la vie privée d’un individu constitue une ingérence au sens de l’article 8.

Selon la Cour, « _le droit interne doit notamment assurer que ces données sont pertinentes et non excessives par rapport aux finalités pour lesquelles elles sont enregistrées, et qu’elles sont conservées sous une forme permettant l’identification des personnes concernées pendant une durée n’excédant pas celle nécessaire aux finalités pour lesquelles elles sont enregistrées._ _ Le droit interne doit aussi contenir des garanties aptes à protéger efficacement les données à caractère personnel enregistrées contre les usages impropres et abusifs, tout en offrant une possibilité concrète de présenter une requête en effacement des données mémorisées_118».

L’appréciation du caractère proportionné de la durée de conservation des données au regard du but poursuivi doit être faite en tenant compte de l’existence d’un contrôle indépendant de la justification de leur maintien dans le fichier, fondé sur des critères précis tels que la gravité de l’infraction, les arrestations antérieures, la force des soupçons pesant sur la personne, ou toute autre circonstance particulière119.

 ** _2\. Objectifs poursuivis et necessite de legiferer_**

 ** _2.1 Objectifs poursuivis_**

 **2.1.1. Définir le champ d’application de la directive**

L’article 19 insère, dans un chapitre unique, l’ensemble des règles applicables au traitement de données à caractère personnel en matière pénale prévues par la directive.

Ces règles fixées par les [nouveaux articles 70-1 à 70-27](/pjl-490/titre-iii/article-19) s’appliquent par dérogation aux autres dispositions de la loi actuelle.

Afin de délimiter le champ d’application de la directive, il est créé un nouvel article 70-1, qui prévoit ainsi que les dispositions du présent chapitre s’appliquent, le cas échéant par dérogation aux autres dispositions de la présente loi, aux traitements des données à caractère personnel mis en œuvre :

1° A des fins de prévention et de détection des infractions pénales, d’enquêtes et de poursuites en la matière ou d’exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces.

2° Par toute autorité publique compétente pour l’une des finalités énoncées au 1°, ou tout autre organisme ou entité à qui a été confié, à ces mêmes fins, l'exercice de l'autorité publique et des prérogatives de puissance publique, ci-après dénommée autorité compétente.

 **2.1.2. Respecter les exigences européennes liées à la licéité du traitement**

Afin de mettre notre droit national en conformité avec l’[article 8](/directive-2016-680/chapitre-ii/article-8) de la directive, le troisième paragraphe de l’article 70-1 prévoit que les traitements relevant de la directive ne sont licites que si et dans la mesure où ils sont nécessaires à l'exécution d'une mission effectuée, pour les finalités énoncées au 1°, par une autorité compétente au sens du 2°, et où sont respectées les dispositions des articles 70-3 et 70-4.

L’option de maintenir les formalités préalables à la création d’un tel traitement, prévue par l’article 70-3, a en effet été retenue.

 **2.1.3 Mettre notre droit en conformité avec les règles particulières applicables aux traitements comportant des données sensibles**

Afin de transposer l’[article 10](/directive-2016-680/chapitre-ii/article-8) de la directive, il est créé un nouvel article 70-2, qui prévoit que le traitement de données mentionnées au I de l’article 8 est uniquement possible en cas de nécessité absolue, sous réserve de garanties appropriées pour les droits et libertés de la personne concernée, et, soit s’il est prévu par un acte législatif ou règlementaire, soit s’il vise à protéger les intérêts vitaux d’une personne physique, soit s’il porte sur des données manifestement rendues publiques par la personne concernée.

Par ailleurs, les formalités préalables à la création d’un traitement portant sur des données mentionnées au I de l’article 8, à savoir un décret en Conseil d’Etat pris après avis de la Commission nationale de l’informatique et des libertés, sont maintenus (cf infra).

 **2.1.4. Mettre notre droit en conformité avec l’exigence nouvelle d’une analyse d’impact relative à la protection des données à caractère personnel**

Afin de respecter les articles [27](/directive-2016-680/chapitre-iv/section-1/article-27) et [28](/directive-2016-680/chapitre-iv/section-1/article-28) de la directive, l’article 70-4 prévoit que si le traitement est susceptible d’engendrer un risque élevé pour les droits et les libertés des personnes physiques, notamment parce qu’il porte sur des données sensibles, à savoir celles mentionnées au I de l’article 8, le responsable du traitement effectue une analyse d’impact relative à la protection des données à caractère personnel.

Lorsque le traitement est effectué pour le compte de l’Etat, cette analyse d’impact est adressée à la Commission nationale de l’informatique et des libertés avec la demande d’avis prévue par l’[article 30](/loi-78-17/chapitre-iv/section-3/article-30) de la loi informatique et libertés compte tenu du maintien des formalités préalables à la création d’un tel traitement.

Dans les autres cas, lorsque le traitement entre dans le champ de la directive sans être effectué pour le compte de l’Etat, les troisième à cinquième alinéas de l’article 70-4 transposent l’article 28 §1 de la directive. Ils imposent ainsi au responsable du traitement ou au sous-traitant de consulter la Commission nationale de l’informatique et des libertés préalablement au traitement des données à caractère personnel :

\- soit lorsque l’analyse d’impact relative à la protection des données indique que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque ;

\- soit lorsque le type de traitement, en particulier en raison de l’utilisation de nouveaux mécanismes, technologies ou procédures, présente des risques élevés pour les libertés et les droits des personnes concernées.

Il a été décidé d’imposer la réalisation de cette étude d’impact lorsque le traitement porte sur des données mentionnées au I de l’article 8, le traitement de ces données sensibles présentant des risques élevés pour les libertés et les droits des personnes concernées.

Parmi les traitements entrant dans le champ de la directive mais non mis en œuvre pour le compte de l’Etat, peuvent être cités les traitements mis en œuvre par la SNCF et par la RATP pour les données à caractère personnel provenant de caméras individuelles fournies aux agents des services de sécurité interne, notamment pour le constat des infractions, qui sont par ailleurs susceptibles de faire apparaître directement ou indirectement des données sensibles au sens de l’article 8120.

 **2.1.5. Transposer les règles applicables aux traitements ultérieurs**

Afin de mettre notre droit en conformité avec les articles [4](/directive-2016-680/chapitre-ii/article-4) et [9](/directive-2016-680/chapitre-ii/article-9) de la directive, [sont créés les articles 70-5 à 70-7](/pjl-490/titre-iii/article-19).

L’article 70-5 reprend strictement l’article 9 de la directive relatif aux conditions spécifiques applicables au traitement, et plus particulièrement aux règles applicables aux traitements ultérieurs à des fins ne relevant plus du champ d’application de la directive.

L’article 70-6 transpose l’article 4§2 et §3 de la directive relatif au traitement ultérieur à des fins demeurant dans le champ d’application de la directive.

L’article 70-7 régit plus spécifiquement la question des traitements à des fins archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques .

 **2.1.6. Mettre notre droit en conformité avec l’obligation de distinguer les données à caractère personnel fondées sur des faits de celles fondées sur des appréciations personnelles**

Afin de respecter l’article 7§1, [il est créé un nouvel article 70-8](/pjl-490/titre-iii/article-19) qui impose cette distinction entre les données à caractère personnel fondées sur des faits et celles fondées sur des appréciations personnelles.

 **2.1.7. Mettre notre droit en conformité avec les règles applicables aux décisions individuelles automatisées**

Afin de mettre le système juridique français en conformité avec l’[article 11](/directive-2016-680/chapitre-ii/article-11) de la directive, l’article 70-9 prévoit, de manière combinée avec l’article 10 de la loi informatique et libertés relatif aux décisions individuelles prises sur le fondement d’un traitement automatisé de données personnelles, que tout profilage qui entraîne une discrimination à l'égard des personnes physiques sur la base des catégories particulières de données à caractère personnel visées à l'article 8 est interdit.

 **2.1.8. Mettre notre droit en conformité avec les règles applicables aux traitements effectués par un sous-traitant**

Le nouvel article 70-10 transpose les exigences européennes fixées par les articles [22](/directive-2016-680/chapitre-iv/section-1/article-22) et [23](/directive-2016-680/chapitre-iv/section-1/article-23) de la directive.

Certaines de ces exigences étant strictement identiques à celles prévues par les paragraphes 1, 2, 9 et 10 de l’[article 28](/reglement-2016-679/chapitre-iv/section-1/article-28) et l’[article 29](/reglement-2016-679/chapitre-iv/section-1/article-29) du règlement (UE) 2016/679, un renvoi à ces dispositions est opéré.

Est en revanche directement transposé le paragraphe 3 de l’article 22 de la directive, compte tenu des différences existantes avec le règlement. L’article 70-10 prévoit ainsi que le traitement par un sous-traitant est régi par un contrat ou un autre acte juridique, qui lie le sous-traitant à l'égard du responsable du traitement, définit l'objet et la durée du traitement, la nature et la finalité du traitement, le type de données à caractère personnel et les catégories de personnes concernées, et les obligations et les droits du responsable du traitement et qui prévoit que le sous-traitant n’agit que sur instruction du responsable de traitement.

**_2.2 Nécessité de légiférer_**

Outre les dispositions sur le champ d’application de la directive qui doivent être transposées, l’objectif du projet de loi est de mettre notre droit national en conformité avec les exigences énoncées par la directive, plus particulièrement avec :

\- l’obligation nouvelle de réaliser, sous certaines conditions, une analyse d’impact relative à la protection des données,

\- les règles relatives aux traitements de données effectués pour d’autres finalités que celles pour lesquelles elles avaient été collectées,

\- la distinction entre les données personnelles fondées sur des faits et celles fondées sur des appréciations personnelles,

\- les règles particulières relatives aux décisions individuelles automatisées,

\- les règles applicables aux traitements effectués par un sous-traitant.

**_3\. Options_**

 **3.1. Maintenir ou supprimer les formalités préalables à la création d’un traitement mis en œuvre pour le compte de l’Etat**

 **3.1.1. Option 1 (écartée) : Supprimer les formalités préalables**

De nombreux traitements relevant du champ d’application de la directive sont actuellement soumis à des formalités préalables contraignantes lors de leur création.

Ainsi, l’[article 26](/loi-78-17/chapitre-iv/section-2/article-26) de la loi Informatique et Libertés du 6 janvier 1978 impose que la création du traitement mis en œuvre pour le compte de l’Etat qui intéressent la sûreté de l’Etat, la défense ou la sécurité publique ou qui a pour objet la prévention, la recherche, la constatation ou la poursuite des infractions pénales ou l’exécution des condamnations pénales ou des mesures de sûreté, soit autorisée par un acte réglementaire, arrêté ou décret en Conseil d’Etat, pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés.

Or la directive n’impose pas aux Etats membres la création de telles formalités préalables pour la création d’un traitement de données entrant dans son champ d’application, sauf dans certains cas. Aux termes de l’[article 28](/directive-2016-680/chapitre-iv/section-1/article-28) de la directive, le responsable du traitement doit en effet uniquement consulter l'autorité de contrôle préalablement au traitement des données à caractère personnel qui fera partie d'un nouveau fichier à créer, soit lorsqu'une analyse d'impact relative à la protection des données indique que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque; soit lorsque le type de traitement, en particulier, en raison de l'utilisation de nouveaux mécanismes, technologies ou procédures, présente des risques élevés pour les libertés et les droits des personnes concernées.

L’option permise par la directive de supprimer les formalités préalables a été écartée.

 **3.1.2. Option 2 (retenue) : Maintenir les formalités préalables**

Les formalités préalables à la création d’un traitement mis en œuvre pour le compte de l’Etat en matière pénale représentent des garanties supplémentaires pour les droits des personnes concernées.

Pour ces raisons et afin d’éviter de remettre en cause un principe mis en œuvre depuis trente-huit ans, l’option retenue a été de les maintenir.

Cette option est en effet permise par l’[article premier](/directive-2016-680/chapitre-i/article-premier) paragraphe 3 de la directive, qui autorise les Etats membres à prévoir des garanties plus étendues pour la protection des droits et des libertés des personnes concernées à l'égard du traitement des données à caractère personnel par les autorités compétentes, ainsi que par le [considérant 15 de la directive](/directive-2016-680/considerants/considerant-15) qui prévoit notamment que « _le rapprochement des législations des États membres ne devrait pas conduire à un affaiblissement de la protection des données à caractère personnel qu'elles offrent mais devrait, au contraire, avoir pour objectif de garantir un niveau élevé de protection dans l'Union. Il convient que les États membres ne soient pas empêchés de prévoir des garanties plus étendues que celles établies dans la présente directive pour la protection des droits et des libertés des personnes concernées à l'égard du traitement des données à caractère personnel par les autorités compétentes_ ».

Ainsi, le [nouvel article 70-3](/pjl-490/titre-iii/article-19) prévoit que si le traitement est mis en œuvre pour le compte de l’Etat pour au moins l’une des finalités prévues au 1° de l’article 70-1, il doit être prévu par un acte règlementaire pris conformément au I de l’article 26 et aux articles 28 à 31, soit par arrêté pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés.

Si le traitement porte sur des données sensibles à savoir celles mentionnées au I de l’article 8, il est prévu par un acte règlementaire pris conformément au II de l’article 26, soit par un décret en Conseil d’Etat pris avis motivé et publié de la Commission nationale de l’informatique et des libertés.

 **3.2. Fixer des délais d’effacement ou de vérification régulière des données**

 **3.2.1. Option 1 (écartée) : Ne pas prévoir de délais de conservation maximums mais imposer la vérification régulière de la nécessité de conserver les données**

L’[article 4](/directive-2016-680/chapitre-ii/article-4) de la directive prévoit par principe que les données doivent être conservées sous une forme permettant l’identification des personnes concernées pendant une durée n’excédant pas celle nécessaire au regard des finalités pour lesquelles elles sont traitées, ce qui correspond à l’[article 6](/loi-78-17/chapitre-ii/section-1/article-6) de la loi Informatique et libertés.

L’[article 5](/directive-2016-680/chapitre-ii/article-5) de la directive impose plus particulièrement aux Etats membres de prévoir des délais appropriés soit pour l’effacement des données, soit pour la vérification régulière de la nécessité de les conserver.

La directive ouvre donc une option permettant de ne pas prévoir de délai maximal de conservation, mais simplement de fixer des modalités pour le responsable du traitement de vérifier régulièrement s’il est toujours nécessaire de les conserver au regard des finalités du fichier.

Cette option n’a pas été retenue, car il paraît plus simple de toujours prévoir, comme actuellement, une durée maximale de conservation.

Cela n’interdit nullement, spécialement si cette durée maximale est particulièrement longue, de permettre des demandes d’effacement par anticipation, de telles possibilités pouvant du reste correspondre à des exigences conventionnelles et/ou constitutionnelles.

 **3.2.2. Option 2 (retenue) : Fixer des délais d’effacement des données**

Le droit français fixe déjà, dans les textes applicables ou lors de la création du traitement, une durée maximale de conservation des données dans les fichiers.

Lors de la création d’un traitement, la demande d’avis adressée à la Commission nationale de l’informatique et des libertés doit en effet préciser la durée de conservation des informations traitées conformément à l’[article 30](/loi-78-17/chapitre-iv/section-3/article-30) I. 5° de la loi Informatique et libertés.

Dans notre législation, et à titre d’illustration, les données ne peuvent être conservées dans le traitement des antécédents judiciaires (TAJ), destiné à faciliter la constatation des infractions à la loi pénale, le rassemblement des preuves de ces infractions et la recherche de leurs auteurs, que pour une durée variant de 5 à 20 ans selon la gravité de l’infraction commise et l’âge de l’auteur aux termes de l’[article R. 40-27](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000025818438&dateTexte=&categorieLien=cid) du code de procédure pénale.

De même, les données figurant au fichier national automatisé des empreintes génétiques (FNAEG), destiné à faciliter l’identification et la recherche des auteurs d’infraction, ne peuvent pas être conservées au-delà de 25 ans ou de 40 ans aux termes de l’article R. 53-14 du code de procédure pénale.

Les données inscrites au fichier automatisé des empreintes digitales (FAED), destiné à faciliter la recherche et l’identification des auteurs de crimes et de délits et de faciliter la poursuite, l’instruction et le jugement des affaires dont l’autorité judiciaire est saisie, ne peuvent pas être conservées au-delà de 10 à 25 ans aux termes de l’article 5 du [décret n°87-249 du 8 avril 1987](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006065909).

L’option plus protectrice pour les droits et libertés de délais butoirs de conservation des données a donc été retenue.

 ** _4\. analyse des impacts de la disposition envisagée_**

La présente disposition conduit à [insérer](/pjl-490/titre-iii/article-19) dans la loi de 1978 dix nouveaux articles (articles 70-1 à 70-10).

<table>
<tr>
<td>

**DISPOSITIONS DU PROJET DE LOI**

</td>
<td>

**DIRECTIVE (UE) 2016/680 DU PARLEMENT EUROPÉEN ET DU CONSEIL du 27 avril 2016**

</td></tr>
<tr>
<td>

[**Article 19 PJL**](/pjl-490/titre-iii/article-19)

**[Article 70](/loi-78-17/chapitre-xii/article-70)-1 LIL**

</td>
<td>

Transposition des articles [1](/chapitre-i/article-premier) et [2](/directive-2016-680/chapitre-i/article-2) (champ d’application), [3](/directive-2016-680/chapitre-i/article-3) (définitions) et [8](/directive-2016-680/chapitre-ii/article-8) (licéité)

</td></tr>
<tr>
<td>

**Article 70-2**

</td>
<td>

[Article 10](/directive-2016-680/chapitre-ii/article-10)

</td></tr>
<tr>
<td>

**Article 70-3**

</td>
<td>

Option permise par l’[article 1](/directive-2016-680/chapitre-i/article-premier)§3 et le [considérant 15 de la directive](/directive-2016-680/considerants/considerant-15)

</td></tr>
<tr>
<td>

**Article 70-4**

</td>
<td>

Articles [27](/directive-2016-680/chapitre-iv/section-1/article-27) et [28](/directive-2016-680/chapitre-iv/section-1/article-28)

</td></tr>
<tr>
<td>

**Article 70-5**

</td>
<td>

[Article 9](/directive-2016-680/chapitre-ii/article-9)

</td></tr>
<tr>
<td>

**Articles 70-6 et 70-7**

</td>
<td>

[Article 4](/directive-2016-680/chapitre-ii/article-4)§2 et §3

</td></tr>
<tr>
<td>

**Article 70-** 8

</td>
<td>

[Article 7](/directive-2016-680/chapitre-ii/article-7)§1

</td></tr>
<tr>
<td>

**Article 70-9**

</td>
<td>

[Article 11](/directive-2016-680/chapitre-ii/article-11)

</td></tr>
<tr>
<td>

**Article 70-10**

</td>
<td>

Transposition combinée des articles [22](/directive-2016-680/chapitre-iv/section-1/article-22) et [23](/directive-2016-680/chapitre-iv/section-1/article-23).

</td></tr></table>

**_5\. CONSULTATION ET MODALITÉS D’APPLICATION_**

La Commission nationale de l’informatique et des libertés a été consultée.

La réforme s’appliquera le 25 mai 2018, conformément à l’[article 24](/loi-78-17/chapitre-iv/section-1/article-24) du projet de loi.

Un décret en Conseil d’Etat, pris après avis de la Commission nationale de l'informatique et des libertés, précisera que le traitement par un sous-traitant est régi par un contrat ou un autre acte juridique, qui lie le sous-traitant à l'égard du responsable du traitement et qui définit l'objet et la durée du traitement, la nature et la finalité du traitement, le type de données à caractère personnel et les catégories de personnes concernées, et les obligations et les droits du responsable du traitement, et qui prévoit que le sous-traitant n’agit que sur instruction du responsable de traitement.

Les présentes dispositions du projet de loi seront applicables sur l’ensemble du territoire, hors les collectivités d’outre-mer soumises au principe de spécialité, pour lesquelles l’extension se fera par ordonnance.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-iii/articles-18-et-19" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-iii/article-19-section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

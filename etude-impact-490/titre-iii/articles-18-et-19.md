<!-- TITLE: ARTICLES 18 ET 19 -->
<!-- SUBTITLE: PRESENTATION GENERALE ET DÉFINITIONS -->

# ARTICLES 18 ET 19

## PRESENTATION GENERALE ET DÉFINITIONS

La transposition de la [directive (UE) 2016/680](/directive-2016-680) appelle les observations générales suivantes, qui seront complétées par des analyses spécifiques aux quatre sections du nouveau [chapitre XIII](/loi-78-17/chapitre-xiii) introduit dans la loi de 1978, et qui sont relatives aux dispositions générales ([section 1](/directive-2016-680/chapitre-i)), aux obligations incombant aux autorités compétentes et aux responsables de traitements ([section 2](/directive-2016-680/chapitre-ii)), aux droits des personnes concernées ([section 3](/directive-2016-680/chapitre-iii)) et aux transferts de données à caractère personnel vers des Etats n'appartenant pas à l’Union européenne ou vers des destinataires établis dans des Etats non membres de l’Union ([section 4](/directive-2016-680/chapitre-iv)).

La présente partie de l’étude d’impact comporte ainsi des observations de nature générale sur la directive, tout en traitant plus particulièrement de la question des définitions et de la technique législative retenue pour procéder à la transposition.

 ** _1\. Etat des lieux et diagnostic_**

 ** _1.1. genese de la directive_**

 ** _1.1.1. historique de l’elaboration de la directive_**

En 1995, l’Union européenne s’est dotée pour la première fois, avec la [directive 95/46](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000697074), d’un cadre juridique destiné à assurer d’une part, la protection des données personnelles des personnes résidant sur son territoire et, d’autre part, la libre circulation de ces données. Le champ d’application de la [directive 95/46](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000697074) était alors circonscrit au domaine communautaire et ne concernait pas le pilier interétatique relatif à la justice et aux affaires intérieures ([JAI](https://europa.eu/european-union/topics/justice-home-affairs_fr)). Elle a par la suite été complétée par plusieurs instruments ayant pour objet d’assurer la protection de catégories de données particulières, ou d’assurer la protection des données personnelles dans le cadre de traitements de données spécifiques.

Dans le cadre du pilier JAI, la nécessité de disposer de règles relatives aux traitements à des fins de prévention, de détection, d’enquêtes, de poursuites ou d’exécutions des sanctions en matière pénale (fichiers de souveraineté) s’est imposée à la suite des attentats londoniens en 2005. Les État membres ont donc adopté la [décision-cadre 2008/977](https://edps.europa.eu/sites/edp/files/publication/dec_2008_977_fr_0.pdf), dont le champ était toutefois limité aux échanges entre États membres ou État membres et États tiers à l’Union tandis que les traitements de fichiers nationaux restaient soumis aux seules législations des États membres.

Le 25 janvier 2012, après avoir hésité à présenter un texte unique, révisant à la fois la [directive 95/46](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000697074) et la [décision-cadre 2008/977](https://edps.europa.eu/sites/edp/files/publication/dec_2008_977_fr_0.pdf), et probablement compte tenu des difficultés juridiques et politiques liées notamment à l’existence des protocoles du Royaume-Uni, de l’Irlande et du Danemark, la Commission a présenté deux projets distincts relatifs à la protection des données : d’une part, une proposition de règlement révisant la [directive 95/46](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000697074) et concernant principalement les fichiers civils et commerciaux et, d’autre part, un projet de directive, révisant la [décision-cadre 2008/977](https://edps.europa.eu/sites/edp/files/publication/dec_2008_977_fr_0.pdf) concernant les fichiers de souveraineté et incluant les traitements nationaux de fichiers des États membres.

**_1.1.2. négociations_**

Le [Conseil européen d’octobre 2013](http://data.consilium.europa.eu/doc/document/ST-169-2013-INIT/fr/pdf) a demandé que les travaux sur le paquet protection des données puissent s’achever avant la fin de l’année 2015. Afin de respecter cette échéance, les présidences grecque, italienne, et lettone ont fait de ces textes une priorité, ce qui a permis d’aboutir à l’adoption d’une orientation générale sur l’ensemble du texte de la proposition de règlement au [Conseil JAI du 15 juin 2015](http://data.consilium.europa.eu/doc/document/ST-9565-2015-INIT/fr/pdf).

Le trilogue avec le Parlement européen, qui a pour sa part débuté ? son mandat en mars 2014, a démarré dès la fin de la Présidence lettone, le 24 juin 2015, et s’est poursuivie sous la Présidence luxembourgeoise, tout au long du second semestre 2015, avec pour objectif de clore le trilogue avant la fin de l’année 2015.

Les discussions sur la proposition de directive étaient nettement moins avancées puisqu’aucune disposition n’avait encore fait l’objet d’un accord politique, les présidences successives ayant largement privilégié les discussions sur la proposition de règlement. Une seule réunion de travail avait ainsi été organisée sous présidence lettone.

**_1.1.2.1.position française lors des negociations_**

La France souhaitait que les travaux sur la directive aboutissent au même titre que ceux sur le règlement, tout en considérant que la réforme de la [directive 95/46](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000697074) et celle de la [décision-cadre 2008/977](https://edps.europa.eu/sites/edp/files/publication/dec_2008_977_fr_0.pdf) devaient rester deux exercices distincts, les enjeux en matière de fichiers de souveraineté étant très différents de ceux en matière de données à usage civil ou commercial. La France s’est montrée à cet égard attentive à la question de la délimitation des champs d’application respectifs du règlement et de la directive, les règles applicables aux traitements de données personnelles étant différentes selon l’instrument dont ils relèvent. Ainsi, certains fichiers de police administrative ayant pour objet la prévention des menaces pour la sécurité publique (par exemple le fichier des interdictions de stades) devaient continuer, à l’issue des trilogues, de relever de la [directive](/directive-2016-680) plutôt que du [règlement](/reglement-2016-679). En outre, les activités relevant de la sécurité nationale et du renseignement devaient rester exclues des champs du règlement et de la directive et demeurer de la compétence exclusive des États membres.

En ce qui concerne la directive relative aux fichiers de souveraineté, la France a toujours milité en faveur d’un haut niveau de protection des données, tout en attachant la plus grande importance à ce que les données relatives à la coopération judiciaire pénale et policière et, plus généralement, celles contenues dans les fichiers dits de souveraineté, fassent l’objet de dispositions particulières, compte tenu d’une part de leur finalité, d’autre part de la nature publique du destinataire final des données transmises.

À cet égard, la France était favorable à la délimitation qui était agréée dans le texte du règlement par renvoi à la directive, et qui était destinée à garantir que la directive s’applique à l’ensemble des activités de police et que les traitements de données dans le domaine de l’asile et de l’immigration, qui relevaient de la [directive 95/46](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000697074), continuent de relever du règlement.

Ainsi, la France a estimé que les dispositions relatives aux droits des personnes concernées, qui étaient calquées sur les dispositions correspondantes de la proposition de règlement, n’étaient pas tout à fait adaptées aux domaines spécifiques des activités policières et judiciaires et ne permettraient donc pas à ces autorités de mener leurs activités efficacement.

Les dispositions qui ont soulevé le plus de difficultés pour la France furent celles relatives aux transferts de données à des pays tiers. En particulier, la France était vigilante à ce que les États membres n’aient pas, comme proposé dans le texte initial, à renégocier tous leurs accords bilatéraux existants, après l’entrée en vigueur de la directive.

**_1.1.2.2. position des autres états membres lors des negociations_**

De nombreuses délégations ont marqué une opposition ferme à l’inclusion dans son champ d’application des traitements de données purement internes, alors que la [décision-cadre 2008/977](https://edps.europa.eu/sites/edp/files/publication/dec_2008_977_fr_0.pdf) ne s’appliquait qu’aux traitements de données transnationaux. Certaines de ces délégations, dont l’Allemagne, ont un temps fait valoir l’absence de base juridique permettant une telle harmonisation.

L’unanimité des délégations s’était également opposée aux dispositions de ce texte relatives aux échanges de données avec des pays tiers, considérées comme trop restrictives car transposant à la matière pénale des règles prévues en matière civile et commerciale.

De même, l’obligation de renégocier tous les accords internationaux et bilatéraux qui n’étaient pas en conformité avec la nouvelle directive était jugée irréaliste et inopportune.

Concernant le champ d’application de cet instrument, lors des dernières discussions techniques sur cette question, un consensus s’est dégagé en faveur d’une rédaction qui permettait d’inclure les traitements de données à des fins de protection des menaces pour la sécurité publique et de prévention de telles menaces dans le champ d’application, ce qui permettait de soumettre les fichiers mixtes des États membres aux mêmes règles de protection des données, tout en maintenant les traitements de données en matière d’asile et d’immigration dans le champ du règlement.

**_1.2. contenu de la directive_**

Ce contenu est plus précisément détaillé dans les analyses consacrées aux 4 sections du nouveau [chapitre XIII](/loi-78-17/chapitre-xiii) inséré dans la loi de 1978.

Toutefois, il convient ici d’observer que la directive, afin d'assurer le même niveau de protection pour les personnes physiques à l'aide de droits opposables dans l'ensemble de l'Union et d'éviter que des divergences n'entravent les échanges de données à caractère personnel, précise dans son [article 3](/directive-2016-680/chapitre-i/article-3) la définition de certains termes, à savoir celle :

\- des «données à caractère personnel» comme toute information se rapportant à une personne physique identifiée ou identifiable alors dénommée « personne concernée », autrement dit qui peut être identifiée, directement ou indirectement, notamment par référence à un identifiant, tel qu'un nom, un numéro d'identification, des données de localisation, un identifiant en ligne, ou à un ou plusieurs éléments spécifiques propres à son identité physique, physiologique, génétique, psychique, économique, culturelle ou sociale;

\- du «traitement» comme toute opération ou tout ensemble d'opérations effectuées ou non à l'aide de procédés automatisés et appliquées à des données à caractère personnel ou des ensembles de données à caractère personnel, telles que la collecte, l'enregistrement, l'organisation, la structuration, la conservation, l'adaptation ou la modification, l'extraction, la consultation, l'utilisation, la communication par transmission, la diffusion ou toute autre forme de mise à disposition, le rapprochement ou l'interconnexion, la limitation, l'effacement ou la destruction;

\- de la «limitation du traitement» comme le marquage de données à caractère personnel conservées en vue de limiter leur traitement futur;

\- du «profilage» comme toute forme de traitement automatisé de données à caractère personnel consistant à utiliser ces données à caractère personnel pour évaluer certains aspects personnels relatifs à une personne physique, notamment pour analyser ou prédire des éléments concernant le rendement au travail, la situation économique, la santé, les préférences personnelles, les intérêts, la fiabilité, le comportement, la localisation ou les déplacements de cette personne;

\- de la «pseudonymisation» comme le traitement de données à caractère personnel de telle façon que celles-ci ne puissent plus être attribuées à une personne concernée précise sans avoir recours à des informations supplémentaires, conservées séparément et garantissant que les données à caractère personnel ne sont pas attribuées à une personne physique identifiée ou identifiable;

\- du «fichier» comme tout ensemble structuré de données à caractère personnel accessibles selon des critères déterminés;

\- d’ « autorité compétente», définie comme :

* toute autorité publique compétente pour la prévention et la détection des infractions pénales, les enquêtes et les poursuites en la matière ou l'exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces; ou

* tout autre organisme ou entité à qui le droit d'un État membre confie l'exercice de l'autorité publique et des prérogatives de puissance publique à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces;

\- du «responsable du traitement» comme étant l'autorité compétente qui, seule ou conjointement avec d'autres, détermine les finalités et les moyens du traitement de données à caractère personnel;

\- du «sous-traitant» comme étant la personne physique ou morale, l'autorité publique, le service ou un autre organisme qui traite des données à caractère personnel pour le compte du responsable du traitement;

\- du «destinataire» comme étant la personne physique ou morale, l'autorité publique, le service ou tout autre organisme qui reçoit communication des données à caractère personnel, qu'il s'agisse ou non d'un tiers, à l’exception néanmoins des autorités publiques qui sont susceptibles de recevoir communication de données à caractère personnel dans le cadre d'une mission d'enquête particulière conformément au droit d'un État membre ;

\- de la «violation de données à caractère personnel» comme étant une violation de la sécurité entraînant, de manière accidentelle ou illicite, la destruction, la perte, l'altération, la divulgation non autorisée de données à caractère personnel transmises, conservées ou traitées d'une autre manière, ou l'accès non autorisé à de telles données;

\- des «données génétiques» comme les données à caractère personnel relatives aux caractéristiques génétiques héréditaires ou acquises d'une personne physique qui donnent des informations uniques sur la physiologie ou l'état de santé de cette personne physique et qui résultent, notamment, d'une analyse d'un échantillon biologique de la personne physique en question;

\- des «données biométriques» comme les données à caractère personnel résultant d'un traitement technique spécifique, relatives aux caractéristiques physiques, physiologiques ou comportementales d'une personne physique, qui permettent ou confirment son identification unique, telles que des images faciales ou des données dactyloscopiques;

\- des «données concernant la santé» comme les données à caractère personnel relatives à la santé physique ou mentale d'une personne physique, y compris la fourniture de soins de santé, qui révèlent des informations sur l'état de santé de cette personne;

\- de l’«autorité de contrôle» comme étant une autorité publique indépendante qui est instituée par un État membre;

\- de l’«organisation internationale» comme étant une organisation internationale et les organismes de droit public international qui en relèvent, ou tout autre organisme qui est créé par un accord entre deux pays ou plus, ou en vertu d'un tel accord.

Des définitions exactement similaires figurent également dans l’[article 4](/reglement-2016-679/chapitre-i/article-4) du règlement (UE) 2016/679 (la directive comportant toutefois des définitions qui lui sont propres comme sur les autorités compétentes, et le règlement comportant un nombre plus important de définitions).

 ** _1.3. conformité au droit national_**

Seule est examinée ici la question des définitions.

L’[article 2](/loi-78-17/chapitre-ier/article-2) de la loi informatique et libertés précise les définitions des notions de donnée à caractère personnel, de traitement et de fichier.

Ainsi, constitue une donnée à caractère personnel toute information relative à une personne physique identifiée ou qui peut être identifiée, directement ou indirectement, par référence à un numéro d'identification ou à un ou plusieurs éléments qui lui sont propres. Il est par ailleurs précisé que pour déterminer si une personne est identifiable, il convient de considérer l'ensemble des moyens en vue de permettre son identification dont dispose ou auxquels peut avoir accès le responsable du traitement ou toute autre personne, et que la personne concernée par un traitement de données à caractère personnel est celle à laquelle se rapportent les données qui font l'objet du traitement.

Constitue un traitement de données à caractère personnel toute opération ou tout ensemble d'opérations portant sur de telles données, quel que soit le procédé utilisé, et notamment la collecte, l'enregistrement, l'organisation, la conservation, l'adaptation ou la modification, l'extraction, la consultation, l'utilisation, la communication par transmission, diffusion ou toute autre forme de mise à disposition, le rapprochement ou l'interconnexion, ainsi que le verrouillage, l'effacement ou la destruction.

Constitue un fichier de données à caractère personnel tout ensemble structuré et stable de données à caractère personnel accessibles selon des critères déterminés.

Les articles [3 I](/loi-78-17/chapitre-ier/article-3) et [35](/loi-78-17/chapitre-v/section-1/article-35) de loi informatique et libertés précisent également les définitions de responsable du traitement, de sous-traitant et de destinataire.

L’article [3 I](/loi-78-17/chapitre-ier/article-3) définit le responsable d'un traitement de données à caractère personnel comme étant, sauf désignation expresse par les dispositions législatives ou réglementaires relatives à ce traitement, la personne, l'autorité publique, le service ou l'organisme qui détermine ses finalités et ses moyens.

Son [II](/loi-78-17/chapitre-ier/article-3)) définit le destinataire d'un traitement de données à caractère personnel comme toute personne habilitée à recevoir communication de ces données autre que la personne concernée, le responsable du traitement, le sous-traitant et les personnes qui, en raison de leurs fonctions, sont chargées de traiter les données. Toutefois, les autorités légalement habilitées, dans le cadre d'une mission particulière ou de l'exercice d'un droit de communication, à demander au responsable du traitement de leur communiquer des données à caractère personnel ne constituent pas des destinataires.

L’[article 35](/loi-78-17/chapitre-v/section-1/article-35) prévoit que toute personne traitant des données à caractère personnel pour le compte du responsable du traitement est considérée comme un sous-traitant.

L’[article 34 bis](/loi-78-17/chapitre-v/section-1/article-34-bis) précise que la violation de données à caractère personnel s’entend, dans le cadre de l’application du présent article, de toute violation de la sécurité entraînant accidentellement ou de manière illicite la destruction, la perte, l'altération, la divulgation ou l'accès non autorisé à des données à caractère personnel faisant l'objet d'un traitement dans le cadre de la fourniture au public de services de communications électroniques.

Enfin, l’[article 11](/loi-78-17/chapitre-iii/article-11) de la loi prévoit, conformément aux exigences européennes d’indépendance de l’autorité de contrôle, que la Commission nationale de l'informatique et des libertés est une autorité administrative indépendante.

Le droit national est donc conforme sur les définitions des données à caractère personnel, du traitement, du fichier, du responsable du traitement, du sous-traitant, du destinataire et de l’autorité de contrôle.

Les quelques variantes, telles que les termes de structuration ou de limitation du traitement, restent en effet de pure forme et sans conséquence sur le fond.

En revanche, la notion française de violation de données à caractère personnel, uniquement applicable aux données personnelles faisant l'objet d'un traitement dans le cadre de la fourniture au public de services de communications électroniques, n’apparaît pas suffisante au regard des exigences européennes.

De même, les notions nouvelles de la directive relatives à la limitation du traitement, au profilage, à la pseudonymisation, aux données génétiques, biométriques et de santé, et à l’organisation internationale devront faire l’objet d’une transposition.

 ** _2\. objectifs et nécessité de légiférer_**

 ** _2.1. objectifs poursuivis_**

Afin de mettre notre droit en conformité avec les notions nouvelles définies par l’[article 3](/directive-2016-680/chapitre-i/article-3) de la directive (notions de limitation du traitement, de profilage, de pseudonymisation, de violation de données à caractère personnel, des données génétiques, biométriques et de santé et d’organisation internationale) , le dernier alinéa du [nouvel article 70-1](/pjl-490/titre-iii/article-19) crée par le présent projet de loi prévoit que les définitions identiques de ces notions prévues par l’[article 4](/reglement-2016-679/chapitre-i/article-4) du règlement (UE) 2016/679 sont alors applicables. .

La définition d’autorité compétente est, quant à elle, reproduite dans le 2° de l’[article 70-1](/pjl-490/titre-iii/article-19), d’une part parce qu’elle est propre à la directive, d’autre part parce qu’elle participe de la délimitation du champ d’application de cet instrument.

S’agissant des règles de fond, elles sont insérées dans un nouveau [chapitre XIII](/loi-78-17/chapitre-xiii) de la loi de 1978, créé par l’[article 19](/pjl-490/titre-iii/article-19) du présent projet de loi.

 ** _2.2 nécessite de légiférer_**

L’objectif du projet de loi tendant à mettre notre droit national en conformité avec les exigences énoncées par la directive, impose à la fois d’adapter les règles de fond concernant les traitements entrant dans son champ d’application.

**_3\. options_**

 **3.1. MODALITES GENERALES DE TRANSPOSITION DE LA DIRECTIVE DANS L’ARCHITECTURE DE LA LOI INFORMATIQUE ET LIBERTES**

 **3.1.1. Option 1 (écartée sauf exception) : Intégrer les règles relatives aux traitements pénaux au fur et à mesure des articles de la loi de 78**

Il aurait pu être envisagé, à l’instar de ce qui est fait pour la mise en conformité de la [loi de 1978](/loi-78-17) avec le [règlement UE](/reglement-2016-679), de « répartir » le contenu des dispositions spécifiques aux traitement pénaux en les « égrenant » tout au long de la [loi de 1978](/loi-78-17), en complétant tel ou tel article de cette loi par des alinéas supplémentaires (mais sans pouvoir procéder par remplacement des dispositions actuelles, car elles devaient continuer de s’appliquer aux traitements qui sont hors du champ du droit de l’Union) ou en insérant après tel ou tel article un nouvel article dérogatoire par rapport au précédent.

Cela aurait imposé, pour chacun de ces ajouts, de préciser qu’il ne s’applique qu’aux traitements pénaux tels que définis par un nouvel [article X](/pjl-490/titre-ii/chapitre-iii/article-10) reprenant les définitions de la directive (ce que fait l’[article 70-1](/pjl-490/titre-iii/article-19) dans la version actuelle du projet de loi).

Une telle solution aurait été particulièrement complexe et la compréhension du droit applicable n’en aurait nullement été améliorée pour les justiciables.

Par ailleurs, d’un point de vue légistique, la dimension des dispositions du projet de loi aurait plus que doublée, puisque pour chacune des dispositions, il aurait dû être non seulement indiqué qu’elle ne concernait que les traitements pénaux, mais également précisé de quelle façon celle-ci était intégrée au sein de la [loi de 1978](/loi-78-17).

Pour toutes ces raisons, cette solution n’a pas été retenue dans son principe.

Toutefois, s’agissant de certaines dispositions par nature communes au règlement et à la directive, les dispositions transposant des articles de la directive ont été insérées dans des articles généraux de la [loi de 1978](/loi-78-17).

Ainsi, l’[article 11](/loi-78-17/chapitre-iii/article-11) de la loi de 1978 qui traite des missions de la CNIL est modifié par l’article 1er du projet de loi pour prévoir dans le i) du II qu’elle peut établir une liste des traitements susceptible de créer un risque devant faire l’objet d’une consultation préalable conformément à l’[article 70-4](/pjl-490/titre-ii/chapitre-vi/article-17) (ce qui est une conséquence de l’[article 28](/directive-2016-680/chapitre-iv/section-1/article-28) de la directive).

De même, l’[article 44](/loi-78-17/chapitre-vi/article-44) de la loi de 1978 qui traite des pouvoirs de la CNIL est modifié par l’[article 4](/pjl-490/titre-i/chapitre-i/article-4) du présent projet pour indiquer que dans l’exercice de son pouvoir de contrôle portant non seulement sur les traitements relevant du [règlement (UE) 2016/679](/reglement-2016-679) mais également sur ceux relevant de la présente [loi](/loi-78-17), et donc relevant de la directive, la Commission nationale de l’informatique et des libertés n’est pas compétente pour contrôler les opérations de traitement effectuées, dans l'exercice de leur fonction juridictionnelle, par les juridictions (conformément à l’[article 45](/directive-2016-680/chapitre-vi/section-2/article-45) de la directive).

De même encore, le nouvel article 43 quater créé par l’[article 16](/pjl-490/titre-ii/chapitre-vi/article-16) du projet prévoit que la personne concernée peut mandater une association ou une organisation mentionnée au IV de l’[article 43 ter](/loi-78-17/chapitre-v/section-2/article-43-ter) aux fins d’exercer ses droits en son nom pour agir devant la Commission nationale de l’informatique et des libertés, contre celle-ci devant un juge ou contre le responsable du traitement ou le sous-traitant devant une juridiction lorsqu’est en cause un traitement relevant du [chapitre XIII](/loi-78-17/chapitre-xiii) (conformément à l’[article 55](/directive-2016-680/chapitre-viii/article-55) de la directive).

Enfin, l’article 43 quinquies, inséré par l’[article 17](/pjl-490/titre-ii/chapitre-vi/article-17) et qui tire les conséquences de l’[arrêt de la Cour de justice de l’Union européenne du 6 octobre 2015](curia.europa.eu/jcms/upload/docs/application/pdf/2015-10/cp150117fr.pdf), étend les recours à la contestation des décisions d’adéquation prises sur le fondement de la directive afin de permettre la saisine du Conseil d’Etat d’une question préjudicielle à la Cour de justice de l’Union européenne permettant d’apprécier la validité d’une décision d’adéquation prise sur le fondement de la directive.

**3.1.2. Option 2 (retenue). Insérer dans la loi de 1978 un chapitre spécifique aux traitements pénaux**

La transposition de la [directive](/directive-2016-680) dans la [loi de 1978](/loi-78-17) ne pouvait raisonnablement pas se faire d’une autre façon que celle retenue par le projet, qui, tout en modifiant ou complétant certains articles de la loi, consiste pour l’essentiel à regrouper les règles spécifiques aux traitements pénaux relevant de la directive dans un chapitre dédié comportant une vingtaine d’articles.

Cette solution est non seulement plus simple et plus lisible, mais elle anticipe la « recodification » globale de la [loi de 1978](/loi-78-17) à laquelle il sera procédé par ordonnance, et qui est indispensable afin d’assurer la meilleure lisibilité possible du droit des traitements (avec un plan comportant nécessairement, et dans cet ordre, au moins les quatre parties suivantes : Dispositions communes ; dispositions concernant les traitements relevant du règlement ; dispositions concernant les traitements relevant de la directive ; dispositions concernant les traitement ne relevant pas du droit de l’Union – ces intitulés n’étant pas nécessairement ceux qui seront retenus pour ces parties mais indiquant ce que sera leur contenu).

**3.2. MODALITES GENERALES DE TRANSPOSITION DE LA DIRECTIVE PAR RAPPORT AU REGLEMENT (UE) 2016/679**

 ** _3.2.1. Option 1 (écartée) : ne pas opérer de renvoi au règlement UE (écartée)_**

Bien que le règlement (UE) 2016/679, aux termes de son [article 2](/reglement-2016-679/chapitre-i/article-2), ne s’applique pas aux traitements de données à caractère personnel relevant du champ de la [directive (UE) 2016/680](/directive-2016-680), certains articles de la directive contiennent des dispositions strictement ou quasiment identiques à celles du règlement.

Si la Cour de justice de l’Union européenne censure le simple renvoi général au droit de l’Union européenne en guise de transposition114, le Gouvernement estime que des renvois précis à certaines dispositions du règlement peuvent valablement transposer des dispositions de la directive.

L’option de ne pas opérer de renvoi au règlement UE a donc été écartée.

.

**_3.2.2. Option 2 (retenue) : opérer certains renvois au règlement UE_** L’option d’opérer des renvois précis aux dispositions strictement identiques de la directive et du règlement a été retenue.

Cette option permet d’éviter des redondances, qui auraient pu à tort laisser penser que les dispositions dérogatoires du [nouveau chapitre XIII](/pjl-490/titre-iii/article-19) de la loi informatique et libertés étaient différentes de celles du règlement.

Ainsi, le [nouveau chapitre XIII](/pjl-490/titre-iii/article-19) qui transpose la directive renvoie à certaines dispositions du règlement pour les définitions des termes employés, les règles relatives au sous-traitant, les obligations imposées au responsable du traitement et au sous-traitant d’assurer la sécurité des données personnelles, de coopérer avec la Commission nationale de l’informatique et des libertés et de notifier à la commission et à la personne concernée des violations de données personnelles.

 ** _4\. analyse des impacts des dispositions envisagées_**

L’impact juridique de la réforme consiste dans l’insertion dans la loi de 1978 d’un [chapitre XIII](/pjl-490/titre-iii/article-19) (qui conduit à décaler l’[actuel chapitre XIII](/loi-78-17/chapitre-xiii)), et dans la modification des actuels articles [32](/loi-78-17/chapitre-v/section-1/article-32), [41](/loi-78-17/chapitre-v/section-2/article-41) et [42](/loi-78-17/chapitre-v/section-2/article-42) de la loi.

 ** _5\. consultation et modalités d’application_**

La Commission nationale de l’informatique et des libertés a été consultée.

La réforme s’appliquera le 25 mai 2018, conformément à l’[article 24](/pjl-490/titre-v/article-24) du projet de loi.

Ces dispositions seront applicables sur l’ensemble du territoire, hors les collectivités d’outre-mer soumises au principe de spécialité, pour lesquelles l’extension se fera par ordonnance

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-iii/article-19-section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

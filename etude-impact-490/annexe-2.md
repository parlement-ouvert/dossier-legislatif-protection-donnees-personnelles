<!-- TITLE: ANNEXE 2 -->
<!-- SUBTITLE: TABLEAU COMPARATIF - PROJET DE LOI RELATIF A LA PROTECTION DES DONNEES PERSONNELLES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/annexe" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <span> </span> </div>

<table>
<tr>
<td>

**Article**

**du projet de loi**

</td>
<td>

**Textes existants**

</td>
<td>

**Modification(s)**

</td>
<td>

**Version consolidée**

</td></tr>
<tr>
<td>

**Titre I**

**_Dispositions communes au règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 et à la directive (UE) 2016/680 du Parlement européen et du Conseil du 27 avril 2016_**

</td></tr>
<tr>
<td>

**Chapitre I – Dispositions relatives à la Commission nationale de l’informatique et des libertés**

</td></tr>
<tr>
<td>

**Article 1 er **_[Dispositions relatives aux missions de la CNIL]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 11_**

La Commission nationale de l'informatique et des libertés est une autorité administrative indépendante. Elle exerce les missions suivantes :

1° Elle informe toutes les personnes concernées et tous les responsables de traitements de leurs droits et obligations ;

2° Elle veille à ce que les traitements de données à caractère personnel soient mis en œuvre conformément aux dispositions de la présente loi.

A ce titre :

a) Elle autorise les traitements mentionnés à l'article 25, donne un avis sur les traitements mentionnés aux articles 26 et 27 et reçoit les déclarations relatives aux autres traitements ;

b) Elle établit et publie les normes mentionnées au I de l'article 24 et édicte, le cas échéant, des règlements types en vue d'assurer la sécurité des systèmes ;

c) Elle reçoit les réclamations, pétitions et plaintes relatives à la mise en œuvre des traitements de données à caractère personnel et informe leurs auteurs des suites données à celles-ci ;

d) Elle répond aux demandes d'avis des pouvoirs publics et, le cas échéant, des juridictions, et conseille les personnes et organismes qui mettent en œuvre ou envisagent de mettre en œuvre des traitements automatisés de données à caractère personnel ;

e) Elle informe sans délai le procureur de la République, conformément à l'article 40 du code de procédure pénale, des infractions dont elle a connaissance, et peut présenter des observations dans les procédures pénales, dans les conditions prévues à l'article 52 ;

f) Elle peut, par décision particulière, charger un ou plusieurs de ses membres ou le secrétaire général, dans les conditions prévues à l'article 44, de procéder ou de faire procéder par les agents de ses services à des vérifications portant sur tous traitements et, le cas échéant, d'obtenir des copies de tous documents ou supports d'information utiles à ses missions ;

g) Elle peut certifier ou homologuer et publier des référentiels ou des méthodologies générales aux fins de certification de la conformité à la présente loi de processus d'anonymisation des données à caractère personnel, notamment en vue de la réutilisation d'informations publiques mises en ligne dans les conditions prévues au titre II du livre III du code des relations entre le public et l'administration.

Il en est tenu compte, le cas échéant, pour la mise en œuvre des sanctions prévues au chapitre VII de la présente loi.

h) Elle répond aux demandes d'accès concernant les traitements mentionnés aux articles 41 et 42 ;

3° A la demande d'organisations professionnelles ou d'institutions regroupant principalement des responsables de traitements :

a) Elle donne un avis sur la conformité aux dispositions de la présente loi des projets de règles professionnelles et des produits et procédures tendant à la protection des personnes à l'égard du traitement de données à caractère personnel, ou à l'anonymisation de ces données, qui lui sont soumis ;

b) Elle porte une appréciation sur les garanties offertes par des règles professionnelles qu'elle a précédemment reconnues conformes aux dispositions de la présente loi, au regard du respect des droits fondamentaux des personnes ;

c) Elle délivre un label à des produits ou à des procédures tendant à la protection des personnes à l'égard du traitement des données à caractère personnel, après qu'elle les a reconnus conformes aux dispositions de la présente loi dans le cadre de l'instruction préalable à la délivrance du label par la commission ; la commission peut également déterminer, de sa propre initiative, les produits et procédures susceptibles de bénéficier d'un label . Le président peut, lorsque la complexité du produit ou de la procédure le justifie, recourir à toute personne indépendante qualifiée pour procéder à leur évaluation. Le coût de cette évaluation est pris en charge par l'entreprise qui demande le label ; elle retire le label lorsqu'elle constate, par tout moyen, que les conditions qui ont permis sa délivrance ne sont plus satisfaites ;

4° Elle se tient informée de l'évolution des technologies de l'information et rend publique le cas échéant son appréciation des conséquences qui en résultent pour l'exercice des droits et libertés mentionnés à l'article 1er ;

A ce titre :

a) Elle est consultée sur tout projet de loi ou de décret ou toute disposition de projet de loi ou de décret relatifs à la protection des données à caractère personnel ou au traitement de telles données. Outre les cas prévus aux articles 26 et 27, lorsqu'une loi prévoit qu'un décret ou un arrêté est pris après avis de la commission, cet avis est publié avec le décret ou l'arrêté ;

b) Elle propose au Gouvernement les mesures législatives ou réglementaires d'adaptation de la protection des libertés à l'évolution des procédés et techniques informatiques ;

c) A la demande d'autres autorités administratives indépendantes, elle peut apporter son concours en matière de protection des données ;

d) Elle peut être associée, à la demande du Premier ministre, à la préparation et à la définition de la position française dans les négociations internationales dans le domaine de la protection des données à caractère personnel. Elle peut participer, à la demande du Premier ministre, à la représentation française dans les organisations internationales et communautaires compétentes en ce domaine ;

e) Elle conduit une réflexion sur les problèmes éthiques et les questions de société soulevés par l'évolution des technologies numériques ;

f) Elle promeut, dans le cadre de ses missions, l'utilisation des technologies protectrices de la vie privée, notamment les technologies de chiffrement des données.

Pour l'accomplissement de ses missions, la commission peut procéder par voie de recommandation et prendre des décisions individuelles ou réglementaires dans les cas prévus par la présente loi.

La commission peut saisir pour avis l'Autorité de régulation des communications électroniques et des postes de toute question relevant de la compétence de celle-ci.

La commission présente chaque année au Président de la République et au Premier ministre un rapport public rendant compte de l'exécution de sa mission.

</td>
<td>

L’article 11 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés est ainsi modifié :

1° Au début du premier alinéa, est insérée la référence : « I. - » ;

2° Après la première phrase du premier alinéa est insérée la phrase suivante :

« Elle est l’autorité de contrôle nationale au sens et pour l’application du règlement (UE) 2016/679 » ;

3° Au _a_ du 2° les mots : « autorise les traitements mentionnés à l’article 25, » et les mots: « et reçoit les déclarations relatives aux autres traitements » sont supprimés ;

4° Après le _a_ du 2°, il est inséré un _a_ _bis_ ainsi rédigé :

« _a bis)_ Elle établit et publie des lignes directrices, recommandations ou référentiels destinés à faciliter la mise en conformité des traitements de données à caractère personnel avec les textes relatifs à la protection des données à caractère personnel et à procéder à l’évaluation préalable des risques par les responsables de traitement et leurs sous-traitants. Elle encourage l’élaboration de codes de conduite définissant les obligations qui incombent aux responsables du traitement et aux sous-traitants, compte tenu du risque inhérent aux traitements de données à caractère personnel pour les droits et libertés des personnes physiques ; elle homologue et publie les méthodologies de référence mentionnées au IV de l’article 54, destinées à favoriser la conformité des traitement de données de santé à caractère personnel » ;

5° Le _b_ du 2° est remplacé par les dispositions suivantes :

« _b)_ Elle établit et publie des règlements types en vue d’assurer la sécurité des systèmes de traitement de données à caractère personnel et de régir les traitements de données de santé relevant du chapitre IX. A ce titre, sauf pour les traitements mis en œuvre pour le compte de l’Etat, agissant dans l’exercice de ses prérogatives de puissance publique, elle peut prescrire des mesures techniques et organisationnelles supplémentaires pour le traitement des données biométriques, génétiques et de santé conformément à l’article 9.4 du règlement (UE) 2016/679 et des garanties complémentaires en matière de traitement de données d’infraction conformément à l’article 10 du même règlement. » ;

6° Après le _f_ du 2°, il est inséré un _f bis_ ainsi rédigé :

« _f bis)_ Elle peut décider de certifier des personnes, des produits, des systèmes de données ou des procédures aux fins de reconnaître qu’ils se conforment au règlement (UE) 2016/679 et la présente loi. Elle agrée, aux mêmes fins, des organismes certificateurs, sur la base, le cas échéant, de leur accréditation par l'instance nationale d'accréditation, mentionnée à l'article 43(1) _b_ du règlement, dans des conditions précisées par décret en Conseil d’Etat pris après avis de la Commission nationale de l'informatique et des libertés. La commission élabore ou approuve les critères des référentiels de certification et d’agrément. Elle peut établir des exigences supplémentaires aux normes d’accréditation. » ;

7° Au _g_ du 2°, après le mot : « certification » sont insérés les mots : « , par des tiers agréés ou accrédités selon les modalités mentionnées au _f bis_ , » ;

8° Au _h_ du 2°, les mots : « d’accès concernant les traitements mentionnés aux articles 41 et 42 » sont remplacés par les mots : « d’exercice des droits prévues aux articles 41, 42 et 70-22 » ;

9° Après le _h_ du 2°, il est inséré un _i_ ainsi rédigé :

« _i)_ Elle peut établir une liste des traitements susceptibles de créer un risque élevé devant faire l’objet d’une consultation préalable conformément à l’article 70-4 » ;

10° Au _a_ du 4°, après la première phrase, il est inséré une phrase ainsi rédigée :

« Elle peut également être consultée par le président de l’Assemblée nationale ou par le président du Sénat sur toute proposition de loi relative à la protection des données à caractère personnel ou au traitement de telles données. » ;

11° Après le _f_ du 4°, est inséré un alinéa ainsi rédigé :

« 5° Elle peut présenter des observations devant toute juridiction à l’occasion d’un litige relatif à l’application du règlement (UE) 2016/679 et de la présente loi » ;

12° Au début du vingt-sixième alinéa, est insérée la référence : « II. - ».

</td>
<td>

**_Article 11_**

**I. -** La Commission nationale de l'informatique et des libertés est une autorité administrative indépendante. **Elle est l’autorité de contrôle nationale au sens et pour l’application du règlement (UE) 2016/679.** Elle exerce les missions suivantes :

1° Elle informe toutes les personnes concernées et tous les responsables de traitements de leurs droits et obligations ;

2° Elle veille à ce que les traitements de données à caractère personnel soient mis en œuvre conformément aux dispositions de la présente loi.

A ce titre :

a) Elle **~~autorise les traitements mentionnés à l'article 25,~~** donne un avis sur les traitements mentionnés aux articles 26 et 27 **~~et reçoit les déclarations relatives aux autres traitements~~** ;

**_a bis)_** **Elle établit et publie des lignes directrices, recommandations ou référentiels destinés à faciliter la mise en conformité des traitements de données à caractère personnel avec les textes relatifs à la protection des données à caractère personnel et à procéder à l’évaluation préalable des risques par les responsables de traitement et leurs sous-traitants. Elle encourage l’élaboration de codes de conduite définissant les obligations qui incombent aux responsables du traitement et aux sous-traitants, compte tenu du risque inhérent aux traitements de données à caractère personnel pour les droits et libertés des personnes physiques ; elle homologue et publie les méthodologies de référence mentionnées au IV de l’article 54, destinées à favoriser la conformité des traitement de données de santé à caractère personnel** **;**

**b)** **~~Elle établit et publie les normes mentionnées au I de l'article 24 et édicte, le cas échéant, des règlements types en vue d'assurer la sécurité des systèmes ;~~**

**_b)_** **Elle établit et publie des règlements types en vue d’assurer la sécurité des systèmes de traitement de données à caractère personnel et de régir les traitements de données de santé relevant du chapitre IX. A ce titre, sauf pour les traitements mis en œuvre pour le compte de l’Etat, agissant dans l’exercice de ses prérogatives de puissance publique, elle peut prescrire des mesures techniques et organisationnelles supplémentaires pour le traitement des données biométriques, génétiques et de santé conformément à l’article 9.4 du règlement (UE) 2016/679 et des garanties complémentaires en matière de traitement de données d’infraction conformément à l’article 10 du même règlement ** ;

c) Elle reçoit les réclamations, pétitions et plaintes relatives à la mise en œuvre des traitements de données à caractère personnel et informe leurs auteurs des suites données à celles-ci ;

d) Elle répond aux demandes d'avis des pouvoirs publics et, le cas échéant, des juridictions, et conseille les personnes et organismes qui mettent en œuvre ou envisagent de mettre en œuvre des traitements automatisés de données à caractère personnel ;

e) Elle informe sans délai le procureur de la République, conformément à l'article 40 du code de procédure pénale, des infractions dont elle a connaissance, et peut présenter des observations dans les procédures pénales, dans les conditions prévues à l'article 52 ;

f) Elle peut, par décision particulière, charger un ou plusieurs de ses membres ou le secrétaire général, dans les conditions prévues à l'article 44, de procéder ou de faire procéder par les agents de ses services à des vérifications portant sur tous traitements et, le cas échéant, d'obtenir des copies de tous documents ou supports d'information utiles à ses missions ;

**_f bis_ ) Elle peut décider de certifier des personnes, des produits, des systèmes de données ou des procédures aux fins de reconnaître qu’ils se conforment au règlement (UE) 2016/679 et la présente loi. Elle agrée, aux mêmes fins, des organismes certificateurs, sur la base, le cas échéant, de leur accréditation par l'instance nationale d'accréditation, mentionnée à l'article 43(1) _b_ du règlement, dans des conditions précisées par décret en Conseil d’Etat pris après avis de la Commission nationale de l'informatique et des libertés. La commission élabore ou approuve les critères des référentiels de certification et d’agrément. Elle peut établir des exigences supplémentaires aux normes d’accréditation ;**

g) Elle peut certifier ou homologuer et publier des référentiels ou des méthodologies générales aux fins de certification **,** **par des tiers agréés ou accrédités selon les modalités mentionnées au f bis,** de la conformité à la présente loi de processus d'anonymisation des données à caractère personnel, notamment en vue de la réutilisation d'informations publiques mises en ligne dans les conditions prévues au titre II du livre III du code des relations entre le public et l'administration.

Il en est tenu compte, le cas échéant, pour la mise en œuvre des sanctions prévues au chapitre VII de la présente loi.

h) Elle répond aux demandes **~~d'accès concernant les traitements mentionnés aux articles 41 et 42~~ d’exercice des droits prévues aux articles 41, 42 et 70-22** ;

**i) Elle peut établir une liste des traitements susceptibles de créer un risque élevé devant faire l’objet d’une consultation préalable conformément à l’article 70-4 ;**

3° A la demande d'organisations professionnelles ou d'institutions regroupant principalement des responsables de traitements :

a) Elle donne un avis sur la conformité aux dispositions de la présente loi des projets de règles professionnelles et des produits et procédures tendant à la protection des personnes à l'égard du traitement de données à caractère personnel, ou à l'anonymisation de ces données, qui lui sont soumis ;

b) Elle porte une appréciation sur les garanties offertes par des règles professionnelles qu'elle a précédemment reconnues conformes aux dispositions de la présente loi, au regard du respect des droits fondamentaux des personnes ;

c) Elle délivre un label à des produits ou à des procédures tendant à la protection des personnes à l'égard du traitement des données à caractère personnel, après qu'elle les a reconnus conformes aux dispositions de la présente loi dans le cadre de l'instruction préalable à la délivrance du label par la commission ; la commission peut également déterminer, de sa propre initiative, les produits et procédures susceptibles de bénéficier d'un label . Le président peut, lorsque la complexité du produit ou de la procédure le justifie, recourir à toute personne indépendante qualifiée pour procéder à leur évaluation. Le coût de cette évaluation est pris en charge par l'entreprise qui demande le label ; elle retire le label lorsqu'elle constate, par tout moyen, que les conditions qui ont permis sa délivrance ne sont plus satisfaites ;

4° Elle se tient informée de l'évolution des technologies de l'information et rend publique le cas échéant son appréciation des conséquences qui en résultent pour l'exercice des droits et libertés mentionnés à l'article 1er ;

A ce titre :

a) Elle est consultée sur tout projet de loi ou de décret ou toute disposition de projet de loi ou de décret relatifs à la protection des données à caractère personnel ou au traitement de telles données. **Elle peut également être consultée par le Président de l’Assemblée nationale ou par le Président du Sénat sur toute proposition de loi relative à la protection des données à caractère personnel ou au traitement de telles données.** Outre les cas prévus aux articles 26 et 27, lorsqu'une loi prévoit qu'un décret ou un arrêté est pris après avis de la commission, cet avis est publié avec le décret ou l'arrêté ;

b) Elle propose au Gouvernement les mesures législatives ou réglementaires d'adaptation de la protection des libertés à l'évolution des procédés et techniques informatiques ;

c) A la demande d'autres autorités administratives indépendantes, elle peut apporter son concours en matière de protection des données ;

d) Elle peut être associée, à la demande du Premier ministre, à la préparation et à la définition de la position française dans les négociations internationales dans le domaine de la protection des données à caractère personnel. Elle peut participer, à la demande du Premier ministre, à la représentation française dans les organisations internationales et communautaires compétentes en ce domaine ;

e) Elle conduit une réflexion sur les problèmes éthiques et les questions de société soulevés par l'évolution des technologies numériques ;

f) Elle promeut, dans le cadre de ses missions, l'utilisation des technologies protectrices de la vie privée, notamment les technologies de chiffrement des données.

**5° Elle peut présenter des observations devant toute juridiction à l’occasion d’un litige relatif à l’application du règlement (UE) 2016/679 et de la présente loi.**

**II. -** Pour l'accomplissement de ses missions, la commission peut procéder par voie de recommandation et prendre des décisions individuelles ou réglementaires dans les cas prévus par la présente loi.

La commission peut saisir pour avis l'Autorité de régulation des communications électroniques et des postes de toute question relevant de la compétence de celle-ci.

La commission présente chaque année au Président de la République et au Premier ministre un rapport public rendant compte de l'exécution de sa mission.

</td></tr>
<tr>
<td>

**Article 2**

_[Composition de la CNIL]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 13_**

I. - La Commission nationale de l'informatique et des libertés est composée de dix-huit membres :

1° Deux députés et deux sénateurs, désignés respectivement par l'Assemblée nationale et par le Sénat de manière à assurer une représentation pluraliste ;

2° Deux membres du Conseil économique, social et environnemental, élus par cette assemblée ;

3° Deux membres ou anciens membres du Conseil d'Etat, d'un grade au moins égal à celui de conseiller, élus par l'assemblée générale du Conseil d'Etat ;

4° Deux membres ou anciens membres de la Cour de cassation, d'un grade au moins égal à celui de conseiller, élus par l'assemblée générale de la Cour de cassation ;

5° Deux membres ou anciens membres de la Cour des comptes, d'un grade au moins égal à celui de conseiller maître, élus par l'assemblée générale de la Cour des comptes ;

6° Trois personnalités qualifiées pour leur connaissance du numérique ou des questions touchant aux libertés individuelles, nommées par décret ;

7° Deux personnalités qualifiées pour leur connaissance du numérique, désignées respectivement par le Président de l'Assemblée nationale et par le Président du Sénat ;

8° Le président de la Commission d'accès aux documents administratifs, ou son représentant.

Elle comprend en outre, avec voix consultative, le Défenseur des droits ou son représentant.

Les deux membres désignés ou élus par une même autorité en application des 1° à 5° sont une femme et un homme. Les trois membres mentionnés au 6° comprennent au moins une femme et un homme.

Les deux membres mentionnés au 7° sont une femme et un homme. Pour l'application de cette règle, le membre succédant à une femme est un homme et celui succédant à un homme, une femme. Toutefois, le nouveau membre désigné est de même sexe que celui qu'il remplace, soit en cas de cessation du mandat avant son terme normal, soit en cas de renouvellement du mandat de l'autre membre mentionné au 7°.

Selon des modalités fixées par décret en Conseil d'Etat, le collège est, à l'exception de son président, renouvelé par moitié tous les deux ans et six mois.

Le président est nommé par décret du Président de la République parmi les membres pour la durée de son mandat. La commission élit en son sein deux vice-présidents, dont un vice-président délégué. Le président et les vice-présidents composent le bureau.

Le président exerce ses fonctions à temps plein. Sa fonction est incompatible avec toute détention, directe ou indirecte, d'intérêts dans une entreprise du secteur des communications électroniques ou de l'informatique.

La durée du mandat de président est de cinq ans.

Le président de la commission reçoit un traitement égal à celui afférent à la seconde des deux catégories supérieures des emplois de l'Etat classés hors échelle.

La formation restreinte de la commission est composée d'un président et de cinq autres membres élus par la commission en son sein. Les membres du bureau ne sont pas éligibles à la formation restreinte.

En cas de partage égal des voix, celle du président est prépondérante.

II. -Le mandat des membres de la commission est de cinq ans ; il est renouvelable une fois, sous réserve des dixième et onzième alinéas du I.

Le règlement intérieur de la commission précise notamment les règles relatives aux délibérations, à l'instruction des dossiers et à leur présentation devant la commission, ainsi que les modalités de mise en œuvre de la procédure de labellisation prévue au c du 3° de l'article 11.

</td>
<td>

Au 7° du I de l’article 13 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, après le mot : « numérique », sont insérés les mots : « ou des questions touchant aux libertés individuelles ».

</td>
<td>

**_Article 13_**

I. - La Commission nationale de l'informatique et des libertés est composée de dix-huit membres :

1° Deux députés et deux sénateurs, désignés respectivement par l'Assemblée nationale et par le Sénat de manière à assurer une représentation pluraliste ;

2° Deux membres du Conseil économique, social et environnemental, élus par cette assemblée ;

3° Deux membres ou anciens membres du Conseil d'Etat, d'un grade au moins égal à celui de conseiller, élus par l'assemblée générale du Conseil d'Etat ;

4° Deux membres ou anciens membres de la Cour de cassation, d'un grade au moins égal à celui de conseiller, élus par l'assemblée générale de la Cour de cassation ;

5° Deux membres ou anciens membres de la Cour des comptes, d'un grade au moins égal à celui de conseiller maître, élus par l'assemblée générale de la Cour des comptes ;

6° Trois personnalités qualifiées pour leur connaissance du numérique ou des questions touchant aux libertés individuelles, nommées par décret ;

7° Deux personnalités qualifiées pour leur connaissance du numérique **ou des questions touchant aux libertés individuelles** , désignées respectivement par le Président de l'Assemblée nationale et par le Président du Sénat ;

8° Le président de la Commission d'accès aux documents administratifs, ou son représentant.

Elle comprend en outre, avec voix consultative, le Défenseur des droits ou son représentant.

Les deux membres désignés ou élus par une même autorité en application des 1° à 5° sont une femme et un homme. Les trois membres mentionnés au 6° comprennent au moins une femme et un homme.

Les deux membres mentionnés au 7° sont une femme et un homme. Pour l'application de cette règle, le membre succédant à une femme est un homme et celui succédant à un homme, une femme. Toutefois, le nouveau membre désigné est de même sexe que celui qu'il remplace, soit en cas de cessation du mandat avant son terme normal, soit en cas de renouvellement du mandat de l'autre membre mentionné au 7°.

Selon des modalités fixées par décret en Conseil d'Etat, le collège est, à l'exception de son président, renouvelé par moitié tous les deux ans et six mois.

Le président est nommé par décret du Président de la République parmi les membres pour la durée de son mandat. La commission élit en son sein deux vice-présidents, dont un vice-président délégué. Le président et les vice-présidents composent le bureau.

Le président exerce ses fonctions à temps plein. Sa fonction est incompatible avec toute détention, directe ou indirecte, d'intérêts dans une entreprise du secteur des communications électroniques ou de l'informatique (1).

La durée du mandat de président est de cinq ans (1).

Le président de la commission reçoit un traitement égal à celui afférent à la seconde des deux catégories supérieures des emplois de l'Etat classés hors échelle (1).

La formation restreinte de la commission est composée d'un président et de cinq autres membres élus par la commission en son sein. Les membres du bureau ne sont pas éligibles à la formation restreinte.

En cas de partage égal des voix, celle du président est prépondérante.

II. -Le mandat des membres de la commission est de cinq ans ; il est renouvelable une fois, sous réserve des dixième et onzième alinéas du I.

Le règlement intérieur de la commission précise notamment les règles relatives aux délibérations, à l'instruction des dossiers et à leur présentation devant la commission, ainsi que les modalités de mise en œuvre de la procédure de labellisation prévue au c du 3° de l'article 11.

</td></tr>
<tr>
<td>

**Article 3 (I et II)**

_[Dispositions relatives au commissaire du Gouvernement]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

_**Article 17** _

La formation restreinte prononce les sanctions à l'encontre des responsables de traitements qui ne respectent pas les obligations découlant de la présente loi dans les conditions prévues au chapitre VII.

Les membres de la formation restreinte ne peuvent participer à l'exercice des attributions de la commission mentionnées aux c, e et f du 2° de l'article 11 et à l'article 44.

</td>
<td>

I. - Au premier alinéa de l’article 17 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, après les mots : « la formation restreinte », sont ajoutés les mots : « prend les mesures et » et après les mots : « obligations découlant » sont ajoutés les mots : « du règlement (UE) 2016/679 et ».

II. - Après le premier alinéa de l’article 17 de la même loi, il est inséré un alinéa ainsi rédigé :

« Les membres délibèrent hors de la présence des agents de la commission, à l’exception de ceux chargés de la tenue de la séance ».

</td>
<td>

_**Article 17** _

La formation restreinte **prend les mesures et** prononce les sanctions à l'encontre des responsables de traitements qui ne respectent pas les obligations découlant **du règlement (UE) 2016/679 et** de la présente loi dans les conditions prévues au chapitre VII.

**Les membres délibèrent hors de la présence des agents de la commission, à l’exception de ceux chargés de la tenue de la séance.**

Les membres de la formation restreinte ne peuvent participer à l'exercice des attributions de la commission mentionnées aux c, e et f du 2° de l'article 11 et à l'article 44.

</td></tr>
<tr>
<td>

**Article 3 (III et IV)**

_[Dispositions relatives au commissaire du Gouvernement]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

_**Article 18** _

Un commissaire du Gouvernement, désigné par le Premier ministre, siège auprès de la commission. Des commissaires adjoints peuvent être désignés dans les mêmes conditions.

Le commissaire du Gouvernement assiste à toutes les délibérations de la commission réunie en formation plénière ou en formation restreinte, ainsi qu'à celles des réunions de son bureau qui ont pour objet l'exercice des attributions déléguées en vertu de l'article 16 ; il est rendu destinataire de tous ses avis et décisions.

Il peut, sauf en matière de sanctions, provoquer une seconde délibération, qui doit intervenir dans les dix jours de la délibération initiale.

</td>
<td>

III. - Le deuxième alinéa de l’article 18 de la même loi est remplacé par les dispositions suivantes :

« Le commissaire du Gouvernement assiste à toutes les délibérations de la commission réunie en formation plénière, ainsi qu’à celles des réunions de son bureau qui ont pour objet l’exercice des attributions déléguées en vertu de l’article 16. Il peut assister aux séances de la formation restreinte, sans être présent au délibéré. Il est rendu destinataire de l’ensemble des avis et décisions de la commission et de la formation restreinte ».

IV. - Le troisième alinéa de l’article 18 de la même loi est remplacé par les dispositions suivantes :

« Sauf en matière de mesures ou de sanctions relevant du chapitre VII, il peut provoquer une seconde délibération de la commission, qui doit intervenir dans les dix jours de la délibération initiale ».

</td>
<td>

**_Article 18_**

Un commissaire du Gouvernement, désigné par le Premier ministre, siège auprès de la commission. Des commissaires adjoints peuvent être désignés dans les mêmes conditions.

**~~Le commissaire du Gouvernement assiste à toutes les délibérations de la commission réunie en formation plénière ou en formation restreinte, ainsi qu'à celles des réunions de son bureau qui ont pour objet l'exercice des attributions déléguées en vertu de l'article 16 ; il est rendu destinataire de tous ses avis et décisions.~~**

**Le commissaire du Gouvernement assiste à toutes les délibérations de la commission réunie en formation plénière, ainsi qu’à celles des réunions de son bureau qui ont pour objet l’exercice des attributions déléguées en vertu de l’article 16. Il peut assister aux séances de la formation restreinte, sans être présent au délibéré. Il est rendu destinataire de l’ensemble des avis et décisions de la commission et de la formation restreinte.**

**~~Il peut, sauf en matière de sanctions, provoquer une seconde délibération, qui doit intervenir dans les dix jours de la délibération initiale.~~**

**Sauf en matière de mesures ou de sanctions relevant du chapitre VII, il peut provoquer une seconde délibération de la commission, qui doit intervenir dans les dix jours de la délibération initiale.**

</td></tr>
<tr>
<td>

**Article 4**

_[Contrôle de la mise en œuvre des traitements]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 44_**

I.-Les membres de la Commission nationale de l'informatique et des libertés ainsi que les agents de ses services habilités dans les conditions définies au dernier alinéa de l'article 19 ont accès, de 6 heures à 21 heures, pour l'exercice de leurs missions, aux lieux, locaux, enceintes, installations ou établissements servant à la mise en œuvre d'un traitement de données à caractère personnel et qui sont à usage professionnel, à l'exclusion des parties de ceux-ci affectées au domicile privé.

Le procureur de la République territorialement compétent en est préalablement informé.

II. - Le responsable de locaux professionnels privés est informé de son droit d'opposition à la visite. Lorsqu'il exerce ce droit, la visite ne peut se dérouler qu'après l'autorisation du juge des libertés et de la détention du tribunal de grande instance dans le ressort duquel sont situés les locaux à visiter, qui statue dans des conditions fixées par décret en Conseil d'Etat. Toutefois, lorsque l'urgence, la gravité des faits à l'origine du contrôle ou le risque de destruction ou de dissimulation de documents le justifie, la visite peut avoir lieu sans que le responsable des locaux en ait été informé, sur autorisation préalable du juge des libertés et de la détention. Dans ce cas, le responsable des lieux ne peut s'opposer à la visite.

La visite s'effectue sous l'autorité et le contrôle du juge des libertés et de la détention qui l'a autorisée, en présence de l'occupant des lieux ou de son représentant qui peut se faire assister d'un conseil de son choix ou, à défaut, en présence de deux témoins qui ne sont pas placés sous l'autorité des personnes chargées de procéder au contrôle.

L'ordonnance ayant autorisé la visite est exécutoire au seul vu de la minute. Elle mentionne que le juge ayant autorisé la visite peut être saisi à tout moment d'une demande de suspension ou d'arrêt de cette visite. Elle indique le délai et la voie de recours. Elle peut faire l'objet, suivant les règles prévues par le code de procédure civile, d'un appel devant le premier président de la cour d'appel. Celui-ci connaît également des recours contre le déroulement des opérations de visite.

III.-Les membres de la commission et les agents mentionnés au premier alinéa du I peuvent demander communication de tous documents nécessaires à l'accomplissement de leur mission, quel qu'en soit le support, et en prendre copie ; ils peuvent recueillir, sur place ou sur convocation, tout renseignement et toute justification utiles ; ils peuvent accéder aux programmes informatiques et aux données, ainsi qu'en demander la transcription par tout traitement approprié dans des documents directement utilisables pour les besoins du contrôle.

Ils peuvent, à la demande du président de la commission, être assistés par des experts désignés par l'autorité dont ceux-ci dépendent.

Seul un médecin peut requérir la communication de données médicales individuelles incluses dans un traitement nécessaire aux fins de la médecine préventive, de la recherche médicale, des diagnostics médicaux, de l'administration de soins ou de traitements, ou à la gestion de service de santé, et qui est mis en œuvre par un membre d'une profession de santé.

En dehors des contrôles sur place et sur convocation, ils peuvent procéder à toute constatation utile ; ils peuvent notamment, à partir d'un service de communication au public en ligne, consulter les données librement accessibles ou rendues accessibles, y compris par imprudence, par négligence ou par le fait d'un tiers, le cas échéant en accédant et en se maintenant dans des systèmes de traitement automatisé de données le temps nécessaire aux constatations ; ils peuvent retranscrire les données par tout traitement approprié dans des documents directement utilisables pour les besoins du contrôle.

Il est dressé procès-verbal des vérifications et visites menées en application du présent article. Ce procès-verbal est dressé contradictoirement lorsque les vérifications et visites sont effectuées sur place ou sur convocation.

IV.-Pour les traitements intéressant la sûreté de l'Etat et qui sont dispensés de la publication de l'acte réglementaire qui les autorise en application du III de l'article 26, le décret en Conseil d'Etat qui prévoit cette dispense peut également prévoir que le traitement n'est pas soumis aux dispositions du présent article.

</td>
<td>

L’article 44 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés est ainsi modifié :

1° Au I, les mots : « et qui sont à usage professionnel » sont supprimés ;

2° A la première phrase du II, les mots : « de locaux professionnels privés » sont remplacés par les mots : « de ces lieux, locaux, enceintes, installations ou établissements » et à la dernière phrase du même II, après le mot : « visite » est ajouté le membre de phrase suivant :

« dont la finalité est l’exercice effectif des missions prévues au III » ;

3° Au III, les trois premiers alinéas sont remplacés par deux alinéas ainsi rédigés :

« Pour l’exercice des missions confiées à la Commission nationale de l’informatique et des libertés par le règlement (UE) 2016/679 et par la présente loi, les membres et agents mentionnés au premier alinéa du I peuvent demander communication de tous documents, quel qu'en soit le support, et en prendre copie. Ils peuvent recueillir, notamment sur place ou sur convocation, tout renseignement et toute justification utiles. Ils peuvent accéder, dans des conditions préservant la confidentialité à l'égard des tiers, aux programmes informatiques et aux données, ainsi qu'en demander la transcription par tout traitement approprié dans des documents directement utilisables pour les besoins du contrôle. Le secret ne peut leur être opposé sauf concernant les informations couvertes par le secret professionnel applicable aux relations entre un avocat et son client, par le secret des sources des traitements journalistiques ou, sous réserve des dispositions de l’alinéa suivant, par le secret médical.

« Le secret médical est opposable s’agissant des informations qui figurent dans un traitement nécessaire aux fins de la médecine préventive, de la recherche médicale, des diagnostics médicaux, de l'administration de soins ou de traitements, ou de la gestion de service de santé. Toutefois la communication des données médicales individuelles incluses dans cette catégorie de traitement peut être faite sous l’autorité et en présence d’un médecin. » ;

4° Après le quatrième alinéa du III, il est inséré un alinéa ainsi rédigé :

« Pour le contrôle de services de communication au public en ligne, les membres et agents mentionnés au premier alinéa du I peuvent réaliser toute opération nécessaire à leur mission sous une identité d'emprunt. L’utilisation d’une identité d’emprunt est sans incidence sur la régularité des constatations effectuées conformément à l’alinéa précédent. Un décret en Conseil d’Etat pris après avis de la Commission nationale de l'informatique et des libertés précise les conditions dans lesquelles ils procèdent dans ces cas à leurs constatations. » ;

5° Il est ajouté un alinéa ainsi rédigé :

« _V_. - Dans l’exercice de son pouvoir de contrôle portant sur les traitements relevant du règlement (UE) 2016/679 et de la présente loi, la Commission nationale de l’informatique et des libertés n’est pas compétente pour contrôler les opérations de traitement effectuées, dans l'exercice de leur fonction juridictionnelle, par les juridictions. »

</td>
<td>

**_Article 44_**

I.-Les membres de la Commission nationale de l'informatique et des libertés ainsi que les agents de ses services habilités dans les conditions définies au dernier alinéa de l'article 19 ont accès, de 6 heures à 21 heures, pour l'exercice de leurs missions, aux lieux, locaux, enceintes, installations ou établissements servant à la mise en œuvre d'un traitement de données à caractère personnel **~~et qui sont à usage professionnel~~** , à l'exclusion des parties de ceux-ci affectées au domicile privé.

Le procureur de la République territorialement compétent en est préalablement informé.

II. - Le responsable **~~de locaux professionnels privés~~** **de ces lieux, locaux, enceintes, installations ou établissements** est informé de son droit d'opposition à la visite. Lorsqu'il exerce ce droit, la visite ne peut se dérouler qu'après l'autorisation du juge des libertés et de la détention du tribunal de grande instance dans le ressort duquel sont situés les locaux à visiter, qui statue dans des conditions fixées par décret en Conseil d'Etat. Toutefois, lorsque l'urgence, la gravité des faits à l'origine du contrôle ou le risque de destruction ou de dissimulation de documents le justifie, la visite peut avoir lieu sans que le responsable des locaux en ait été informé, sur autorisation préalable du juge des libertés et de la détention. Dans ce cas, le responsable des lieux ne peut s'opposer à la visite **dont la finalité est l’exercice effectif des missions prévues au III.**

La visite s'effectue sous l'autorité et le contrôle du juge des libertés et de la détention qui l'a autorisée, en présence de l'occupant des lieux ou de son représentant qui peut se faire assister d'un conseil de son choix ou, à défaut, en présence de deux témoins qui ne sont pas placés sous l'autorité des personnes chargées de procéder au contrôle.

L'ordonnance ayant autorisé la visite est exécutoire au seul vu de la minute. Elle mentionne que le juge ayant autorisé la visite peut être saisi à tout moment d'une demande de suspension ou d'arrêt de cette visite. Elle indique le délai et la voie de recours. Elle peut faire l'objet, suivant les règles prévues par le code de procédure civile, d'un appel devant le premier président de la cour d'appel. Celui-ci connaît également des recours contre le déroulement des opérations de visite.

III.- ** ~~Les membres de la commission et les agents mentionnés au premier alinéa du I peuvent demander communication de tous documents nécessaires à l'accomplissement de leur mission, quel qu'en soit le support, et en prendre copie ; ils peuvent recueillir, sur place ou sur convocation, tout renseignement et toute justification utiles ; ils peuvent accéder aux programmes informatiques et aux données, ainsi qu'en demander la transcription par tout traitement approprié dans des documents directement utilisables pour les besoins du contrôle.~~**

**~~Ils peuvent, à la demande du président de la commission, être assistés par des experts désignés par l'autorité dont ceux-ci dépendent.~~**

**~~Seul un médecin peut requérir la communication de données médicales individuelles incluses dans un traitement nécessaire aux fins de la médecine préventive, de la recherche médicale, des diagnostics médicaux, de l'administration de soins ou de traitements, ou à la gestion de service de santé, et qui est mis en œuvre par un membre d'une profession de santé.~~**

**Pour l’exercice des missions confiées à la Commission nationale de l’informatique et des libertés par le règlement (UE) 2016/679 et par la présente loi, les membres et agents mentionnés au premier alinéa du I peuvent demander communication de tous documents, quel qu'en soit le support, et en prendre copie. Ils peuvent recueillir, notamment sur place ou sur convocation, tout renseignement et toute justification utiles. Ils peuvent accéder, dans des conditions préservant la confidentialité à l'égard des tiers, aux programmes informatiques et aux données, ainsi qu'en demander la transcription par tout traitement approprié dans des documents directement utilisables pour les besoins du contrôle. Le secret ne peut leur être opposé sauf concernant les informations couvertes par le secret professionnel applicable aux relations entre un avocat et son client, par le secret des sources des traitements journalistiques ou, sous réserve des dispositions de l’alinéa suivant, par le secret médical.**

**Le secret médical est opposable s’agissant des informations qui figurent dans un traitement nécessaire aux fins de la médecine préventive, de la recherche médicale, des diagnostics médicaux, de l'administration de soins ou de traitements, ou de la gestion de service de santé. Toutefois la communication des données médicales individuelles incluses dans cette catégorie de traitement peut être faite sous l’autorité et en présence d’un médecin.**

En dehors des contrôles sur place et sur convocation, ils peuvent procéder à toute constatation utile ; ils peuvent notamment, à partir d'un service de communication au public en ligne, consulter les données librement accessibles ou rendues accessibles, y compris par imprudence, par négligence ou par le fait d'un tiers, le cas échéant en accédant et en se maintenant dans des systèmes de traitement automatisé de données le temps nécessaire aux constatations ; ils peuvent retranscrire les données par tout traitement approprié dans des documents directement utilisables pour les besoins du contrôle.

**Pour le contrôle de services de communication au public en ligne, les membres et agents mentionnés au premier alinéa du I peuvent réaliser toute opération nécessaire à leur mission sous une identité d'emprunt. L’utilisation d’une identité d’emprunt est sans incidence sur la régularité des constatations effectuées conformément à l’alinéa précédent. Un décret en Conseil d’Etat pris après avis de la Commission nationale de l'informatique et des libertés précise les conditions dans lesquelles ils procèdent dans ces cas à leurs constatations.**

Il est dressé procès-verbal des vérifications et visites menées en application du présent article. Ce procès-verbal est dressé contradictoirement lorsque les vérifications et visites sont effectuées sur place ou sur convocation.

IV.-Pour les traitements intéressant la sûreté de l'Etat et qui sont dispensés de la publication de l'acte réglementaire qui les autorise en application du III de l'article 26, le décret en Conseil d'Etat qui prévoit cette dispense peut également prévoir que le traitement n'est pas soumis aux dispositions du présent article.

**_V_. - Dans l’exercice de son pouvoir de contrôle portant sur les traitements relevant du règlement (UE) 2016/679 et de la présente loi, la Commission nationale de l’informatique et des libertés n’est pas compétente pour contrôler les opérations de traitement effectuées, dans l'exercice de leur fonction juridictionnelle, par les juridictions. **

</td></tr>
<tr>
<td>

**Article 5 (I)**

_[Coopération de la CNIL avec d’autres autorités de contrôle]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 49_**

La commission peut, à la demande d'une autorité exerçant des compétences analogues aux siennes dans un autre Etat membre de l'Union européenne, procéder à des vérifications dans les mêmes conditions que celles prévues à l'article 44, sauf s'il s'agit d'un traitement mentionné aux I ou II de l'article 26.

Le président de la commission ou la formation restreinte peuvent, à la demande d'une autorité exerçant des compétences analogues aux leurs dans un autre Etat membre de l'Union européenne, prendre les décisions mentionnées aux articles 45 à 47 et dans les conditions prévues par ces mêmes articles, sauf s'il s'agit d'un traitement mentionné aux I ou II de l'article 26.

La commission est habilitée à communiquer les informations qu'elle recueille ou qu'elle détient, à leur demande, aux autorités exerçant des compétences analogues aux siennes dans d'autres Etats membres de la Communauté européenne.

</td>
<td>

L’article 49 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés est remplacé par les dispositions suivantes :

« _Art. 49. -_ Dans les conditions prévues aux articles 60 à 67, du règlement (UE) 2016/679, la Commission nationale de l'informatique et des libertés met en œuvre des procédures de coopération et d’assistance mutuelle avec les autorités de contrôle des autres Etats membres de l’Union européenne, et réalise avec elles des opérations conjointes.

« La commission, le président, le bureau, la formation restreinte et les agents de la commission mettent en œuvre, chacun pour ce qui les concerne, les procédures visées à l’alinéa précédent. »

</td>
<td>

**_Article 49_**

**~~La commission peut, à la demande d'une autorité exerçant des compétences analogues aux siennes dans un autre Etat membre de l'Union européenne, procéder à des vérifications dans les mêmes conditions que celles prévues à l'article 44, sauf s'il s'agit d'un traitement mentionné aux I ou II de l'article 26.~~**

**~~Le président de la commission ou la formation restreinte peuvent, à la demande d'une autorité exerçant des compétences analogues aux leurs dans un autre Etat membre de l'Union européenne, prendre les décisions mentionnées aux articles 45 à 47 et dans les conditions prévues par ces mêmes articles, sauf s'il s'agit d'un traitement mentionné aux I ou II de l'article 26.~~**

**~~La commission est habilitée à communiquer les informations qu'elle recueille ou qu'elle détient, à leur demande, aux autorités exerçant des compétences analogues aux siennes dans d'autres Etats membres de la Communauté européenne.~~**

**_Art 49_ \- Dans les conditions prévues aux articles 60 à 67, du règlement (UE) 2016/679, la Commission nationale de l'informatique et des libertés met en œuvre des procédures de coopération et d’assistance mutuelle avec les autorités de contrôle des autres Etats membres de l’Union européenne, et réalise avec elles des opérations conjointes.**

**La commission, le président, le bureau, la formation restreinte et les agents de la commission mettent en œuvre, chacun pour ce qui les concerne, les procédures visées à l’alinéa précédent.**

</td></tr>
<tr>
<td>

**Article 5 (II)**

_[Coopération de la CNIL avec d’autres autorités de contrôle]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

</td>
<td>

II.- Après l’article 49, sont insérés les articles 49-1 et 49-2 ainsi rédigés :

« _Art. 49-1. -_ I. - La Commission nationale de l'informatique et des libertés coopère avec les autorités de contrôle des autres Etats membres de l’Union européenne en application de l’article 62 du règlement (UE) 2016/679, dans les conditions prévues au présent article. Cette coopération n’est pas applicable aux traitements qui ne relèvent pas du champ d’application du droit de l’Union européenne.

« II. - Qu’elle agisse en tant qu’autorité de contrôle chef de file ou en tant qu’autorité concernée au sens des articles 4 et 56 du règlement (UE) 2016/679, la Commission nationale de l'informatique et des libertés est compétente pour traiter une réclamation ou une éventuelle violation des dispositions du même règlement affectant par ailleurs d’autres Etats membres. Le président de la commission invite les autres autorités de contrôle concernées à participer aux opérations de contrôle conjointes qu’il décide de conduire.

« III. - Lorsqu’une opération de contrôle conjointe se déroule sur le territoire français, des membres ou agents habilités de la commission, agissant en tant qu’autorité de contrôle d’accueil, sont présents aux côtés des membres et agents des autres autorités de contrôle participant, le cas échéant, à l’opération. A la demande de l’autorité de contrôle de l’Etat membre, le président de la commission peut habiliter, par décision particulière, ceux des membres ou agents de l’autorité de contrôle concernée qui présentent des garanties comparables à celles requises des agents de la commission, en application des dispositions de l’article 19, à exercer, sous son autorité, tout ou partie des pouvoirs de vérification et d’enquête dont disposent les membres et les agents de la commission.

« IV. - Lorsque la commission est invitée à contribuer à une opération de contrôle conjointe décidée par une autre autorité compétente, le président de la commission se prononce sur le principe et les conditions de la participation, désigne les membres et agents habilités, et en informe l’autorité requérante dans les conditions prévues à l’article 62 du règlement (UE) 2016/679.

« _Art. 49-2._ \- I. - Les traitements mentionnés à l’article 70-1 font l’objet d’une coopération entre la Commission nationale de l'informatique et des libertés et les autorités de contrôle des autres États membres de l’Union européenne dans les conditions prévues au présent article.

« II. - La commission communique aux autorités de contrôle des autres Etats membres les informations utiles et leur prête assistance en mettant notamment en œuvre, à leur demande, des mesures de contrôle telles que les mesures de consultation, d’inspections et d’enquête.

« La commission répond à une demande d’assistance mutuelle formulée par une autre autorité de contrôle dans les meilleurs délais et au plus tard un mois après réception de la demande contenant toutes les informations nécessaires, notamment sa finalité et ses motifs. Elle ne peut refuser de satisfaire à cette demande que si elle n'est pas compétente pour traiter l'objet de la demande ou les mesures qu'elle est invitée à exécuter, ou si une disposition du droit de l’Union européenne ou du droit français y fait obstacle.

« La Commission informe l’autorité requérante des résultats obtenus ou, selon le cas, de l'avancement du dossier ou des mesures prises pour donner suite à la demande.

« La commission peut, pour l’exercice de ses missions, solliciter l’assistance d’une autorité de contrôle d’un autre Etat membre de l’Union européenne.

« La commission donne les motifs de tout refus de satisfaire une demande lorsqu’elle estime ne pas être compétente ou lorsqu’elle considère que satisfaire à la demande constituerait une violation du droit de l’Union européenne, ou de la législation française.

</td>
<td>

**_Art. 49-1. -_ I. - La Commission nationale de l'informatique et des libertés coopère avec les autorités de contrôle des autres Etats membres de l’Union européenne en application de l’article 62 du règlement (UE) 2016/679, dans les conditions prévues au présent article. Cette coopération n’est pas applicable aux traitements qui ne relèvent pas du champ d’application du droit de l’Union européenne.**

**II. - Qu’elle agisse en tant qu’autorité de contrôle chef de file ou en tant qu’autorité concernée au sens des articles 4 et 56 du règlement (UE) 2016/679, la Commission nationale de l'informatique et des libertés est compétente pour traiter une réclamation ou une éventuelle violation des dispositions du même règlement affectant par ailleurs d’autres Etats membres. Le président de la commission invite les autres autorités de contrôle concernées à participer aux opérations de contrôle conjointes qu’il décide de conduire.**

**III. - Lorsqu’une opération de contrôle conjointe se déroule sur le territoire français, des membres ou agents habilités de la commission, agissant en tant qu’autorité de contrôle d’accueil, sont présents aux côtés des membres et agents des autres autorités de contrôle participant, le cas échéant, à l’opération. A la demande de l’autorité de contrôle de l’Etat membre, le président de la commission peut habiliter, par décision particulière, ceux des membres ou agents de l’autorité de contrôle concernée qui présentent des garanties comparables à celles requises des agents de la commission, en application des dispositions de l’article 19, à exercer, sous son autorité, tout ou partie des pouvoirs de vérification et d’enquête dont disposent les membres et les agents de la commission.**

**IV. - Lorsque la commission est invitée à contribuer à une opération de contrôle conjointe décidée par une autre autorité compétente, le président de la commission se prononce sur le principe et les conditions de la participation, désigne les membres et agents habilités, et en informe l’autorité requérante dans les conditions prévues à l’article 62 du règlement (UE) 2016/679.**

**_Art. 49-2._ \- I. - Les traitements mentionnés à l’article 70-1 font l’objet d’une coopération entre la Commission nationale de l'informatique et des libertés et les autorités de contrôle des autres États membres de l’Union européenne dans les conditions prévues au présent article.**

**II. - La commission communique aux autorités de contrôle des autres Etats membres les informations utiles et leur prête assistance en mettant notamment en œuvre, à leur demande, des mesures de contrôle telles que les mesures de consultation, d’inspections et d’enquête.**

**La commission répond à une demande d’assistance mutuelle formulée par une autre autorité de contrôle dans les meilleurs délais et au plus tard un mois après réception de la demande contenant toutes les informations nécessaires, notamment sa finalité et ses motifs. Elle ne peut refuser de satisfaire à cette demande que si elle n'est pas compétente pour traiter l'objet de la demande ou les mesures qu'elle est invitée à exécuter, ou si une disposition du droit de l’Union européenne ou du droit français y fait obstacle.**

**La Commission informe l’autorité requérante des résultats obtenus ou, selon le cas, de l'avancement du dossier ou des mesures prises pour donner suite à la demande.**

**La commission peut, pour l’exercice de ses missions, solliciter l’assistance d’une autorité de contrôle d’un autre Etat membre de l’Union européenne.**

**La commission donne les motifs de tout refus de satisfaire une demande lorsqu’elle estime ne pas être compétente ou lorsqu’elle considère que satisfaire à la demande constituerait une violation du droit de l’Union européenne, ou de la législation française.**

</td></tr>
<tr>
<td>

**Article 5 (III)**

_[Coopération de la CNIL avec d’autres autorités de contrôle]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

</td>
<td>

III.- Après l’article 49 bis de la même loi, sont insérés les articles 49 3 et 49 4 ainsi rédigés :

« _Art. 49-3._ \- Lorsque la commission agit en tant qu’autorité de contrôle chef de file s’agissant d’un traitement transfrontalier au sein de l’Union européenne, elle communique le rapport du membre rapporteur, ainsi que l’ensemble des informations utiles de la procédure ayant permis d’établir le rapport, aux autres autorités de contrôle concernées sans tarder et avant l’éventuelle audition du responsable du traitement ou du sous-traitant. Les autorités concernées sont mises en mesure d’assister à l’audition par la formation restreinte du responsable de traitement ou du sous-traitant par tout moyen de retransmission approprié, ou de prendre connaissance d’un procès-verbal dressé à la suite de l’audition.

« Après en avoir délibéré, la formation restreinte soumet son projet de décision aux autres autorités concernées conformément à la procédure définie à l’article 60 du règlement (UE) 2016/679. A ce titre, elle se prononce sur la prise en compte des objections pertinentes et motivées émises par les autorités concernées et saisit, si elle décide d’écarter l’une des objections, le comité européen de la protection des données conformément à l’article 65 du règlement.

« Les conditions d’application du présent article sont définies par un décret en Conseil d’Etat, après avis de la Commission nationale de l’informatique et des libertés.

« _Art. 49-4._ \- Lorsque la commission agit en tant qu’autorité concernée, au sens du règlement (UE) 2016/679, le président de la commission est saisi des projets de mesures correctrices soumis à la commission par une autre autorité chef de file.

« Lorsque ces mesures sont d’objet équivalent à celles définies aux I et III de l’article 45, le président décide, le cas échéant, d’émettre une objection pertinente et motivée selon les modalités prévues à l’article 60 de ce règlement.

« Lorsque ces mesures sont d’objet équivalent à celles définies au II de l’article 45 et à l’article 46, le président saisit la formation restreinte. Le président de la formation restreinte ou le membre de la formation restreinte qu’il désigne peut, le cas échéant, émettre une objection pertinente et motivée selon les mêmes modalités. »

</td>
<td>

**_Art. 49-3._ \- Lorsque la commission agit en tant qu’autorité de contrôle chef de file s’agissant d’un traitement transfrontalier au sein de l’Union européenne, elle communique le rapport du membre rapporteur, ainsi que l’ensemble des informations utiles de la procédure ayant permis d’établir le rapport, aux autres autorités de contrôle concernées sans tarder et avant l’éventuelle audition du responsable du traitement ou du sous-traitant. Les autorités concernées sont mises en mesure d’assister à l’audition par la formation restreinte du responsable de traitement ou du sous-traitant par tout moyen de retransmission approprié, ou de prendre connaissance d’un procès-verbal dressé à la suite de l’audition.**

**Après en avoir délibéré, la formation restreinte soumet son projet de décision aux autres autorités concernées conformément à la procédure définie à l’article 60 du règlement (UE) 2016/679. A ce titre, elle se prononce sur la prise en compte des objections pertinentes et motivées émises par les autorités concernées et saisit, si elle décide d’écarter l’une des objections, le comité européen de la protection des données conformément à l’article 65 du règlement.**

**Les conditions d’application du présent article sont définies par un décret en Conseil d’Etat, après avis de la Commission nationale de l’informatique et des libertés.**

**_Art. 49-4._ \- Lorsque la commission agit en tant qu’autorité concernée, au sens du règlement (UE) 2016/679, le président de la commission est saisi des projets de mesures correctrices soumis à la commission par une autre autorité chef de file. **

**Lorsque ces mesures sont d’objet équivalent à celles définies aux I et III de l’article 45, le président décide, le cas échéant, d’émettre une objection pertinente et motivée selon les modalités prévues à l’article 60 de ce règlement.**

**Lorsque ces mesures sont d’objet équivalent à celles définies au II de l’article 45 et à l’article 46, le président saisit la formation restreinte. Le président de la formation restreinte ou le membre de la formation restreinte qu’il désigne peut, le cas échéant, émettre une objection pertinente et motivée selon les mêmes modalités.**

</td></tr>
<tr>
<td>

**Article 6 (I et II)**

_[Mesures correctrices]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Chapitre VII :_** Sanctions prononcées par la formation restreinte de la Commission nationale de l'informatique et des libertés.

**_Article 45_**

I. - Lorsque le responsable d'un traitement ne respecte pas les obligations découlant de la présente loi, le président de la Commission nationale de l'informatique et des libertés peut le mettre en demeure de faire cesser le manquement constaté dans un délai qu'il fixe. En cas d'extrême urgence, ce délai peut être ramené à vingt-quatre heures.

Si le responsable du traitement se conforme à la mise en demeure qui lui est adressée, le président de la commission prononce la clôture de la procédure.

Dans le cas contraire, la formation restreinte de la commission peut prononcer, après une procédure contradictoire, les sanctions suivantes :

1° Un avertissement ;

2° Une sanction pécuniaire, dans les conditions prévues à l'article 47, à l'exception des cas où le traitement est mis en œuvre par l'Etat ;

3° Une injonction de cesser le traitement, lorsque celui-ci relève de l'article 22, ou un retrait de l'autorisation accordée en application de l'article 25.

Lorsque le manquement constaté ne peut faire l'objet d'une mise en conformité dans le cadre d'une mise en demeure, la formation restreinte peut prononcer, sans mise en demeure préalable et après une procédure contradictoire, les sanctions prévues au présent I.

II. - Lorsque la mise en œuvre d'un traitement ou l'exploitation des données traitées entraîne une violation des droits et libertés mentionnés à l'article 1er, la formation restreinte, saisie par le président de la commission, peut, dans le cadre d'une procédure d'urgence définie par décret en Conseil d'Etat, après une procédure contradictoire :

1° Décider l'interruption de la mise en œuvre du traitement, pour une durée maximale de trois mois, si le traitement n'est pas au nombre de ceux qui sont mentionnés aux I et II de l'article 26 ou de ceux mentionnés à l'article 27 mis en œuvre par l'Etat ;

2° Prononcer un avertissement visé au 1° du I ;

3° Décider le verrouillage de certaines des données à caractère personnel traitées, pour une durée maximale de trois mois, si le traitement n'est pas au nombre de ceux qui sont mentionnés aux I et II de l'article 26 ;

4° Informer le Premier ministre pour qu'il prenne, le cas échéant, les mesures permettant de faire cesser la violation constatée, si le traitement en cause est au nombre de ceux qui sont mentionnés aux mêmes I et II de l'article 26 ; le Premier ministre fait alors connaître à la formation restreinte les suites qu'il a données à cette information au plus tard quinze jours après l'avoir reçue.

III. - En cas d'atteinte grave et immédiate aux droits et libertés mentionnés à l'article 1er, le président de la commission peut demander, par la voie du référé, à la juridiction compétente d'ordonner, le cas échéant sous astreinte, toute mesure nécessaire à la sauvegarde de ces droits et libertés.

</td>
<td>

I. - L’intitulé du chapitre VII de la même loi est supprimé et remplacé par l’intitulé suivant :

« Mesures et sanctions prises par la formation restreinte de la Commission nationale de l’informatique et des libertés »

II. - L’article 45 de la même loi est remplacé par les dispositions suivantes :

« _Art. 45. -_ I. - Le président de la Commission nationale de l'informatique et des libertés peut avertir un responsable du traitement ou un sous-traitant du fait que les opérations de traitement envisagées sont susceptibles de violer les dispositions du règlement (UE) 2016/679 ou de la présente loi.

« II. - Lorsque le responsable du traitement ou le sous-traitant ne respecte pas les obligations résultant du règlement (UE) 2016/679 ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut saisir la formation restreinte de la commission en vue du prononcé, après procédure contradictoire, de l’une ou de plusieurs des mesures suivantes :

« 1° Un rappel à l’ordre ;

« 2° Une injonction de mettre en conformité le traitement avec les obligations résultant de la présente loi ou du règlement (UE) 2016/679 ou de satisfaire aux demandes présentées par la personne concernée en vue d'exercer ses droits, qui peut être assortie, sauf dans des cas où le traitement est mis en œuvre par l'Etat, d’une astreinte dont le montant ne peut excéder 100 000 € par jour ;

« 3° A l’exception des traitements qui intéressent la sûreté de l’Etat ou la défense, la limitation temporaire ou définitive du traitement, son interdiction ou le retrait d’une autorisation accordée en application du règlement (UE) 2016/679 ou de la présente loi ;

« 4° Le retrait d’une certification ou l’injonction, à l’organisme concerné, de refuser ou de retirer la certification accordée ;

« 5° La suspension des flux de données adressées à un destinataire situé dans un pays tiers ou à une organisation internationale ;

« 6° Le retrait de la décision d’approbation d’une règle d’entreprise contraignante ;

« 7° A l'exception des cas où le traitement est mis en œuvre par l'Etat, une amende administrative ne pouvant excéder 10 millions d’euros ou, s’agissant d’une entreprise, 2 % du chiffre d’affaires annuel mondial total de l’exercice précédent, le montant le plus élevé étant retenu. Dans les hypothèses mentionnées aux paragraphes 5 et 6 de l’article 83 du règlement (UE) 2016/679, ces plafonds sont portés respectivement à 20 millions d’euros et 4 % du chiffre d’affaires. La formation restreinte prend en compte, dans la détermination du montant de l’amende, les critères précisés à l’article 83 du règlement (UE) 2016/679.

« Lorsque la formation restreinte a prononcé une sanction pécuniaire devenue définitive avant que le juge pénal ait statué définitivement sur les mêmes faits ou des faits connexes, celui-ci peut ordonner que l’amende administrative s'impute sur l'amende pénale qu'il prononce.

« Les sanctions pécuniaires sont recouvrées comme les créances de l'Etat étrangères à l'impôt et au domaine.

« Le projet de mesure est le cas échéant soumis aux autres autorités concernées selon les modalités définies à l’article 60 du règlement (UE) 2016/679.

« III. - Lorsque le responsable d'un traitement ou le sous-traitant ne respecte pas les obligations découlant du règlement (UE) 2016/679 ou de la présente loi, le président de la Commission nationale de l'informatique et des libertés peut également prononcer à son égard une mise en demeure, dans le délai qu’il fixe :

« 1° De satisfaire aux demandes présentées par la personne concernée en vue d'exercer ses droits ;

« 2° De mettre les opérations de traitement en conformité avec les dispositions applicables ;

« 3° A l’exception des traitements qui intéressent la sûreté de l’Etat ou la défense et ceux mentionnées à l’article 27, de communiquer à la personne concernée une violation de données à caractère personnel ;

« 4° De rectifier ou d'effacer des données à caractère personnel, ou de limiter le traitement.

« Dans le cas prévu au 4°, le président peut, dans les mêmes conditions, mettre en demeure le responsable de traitement ou le sous-traitant de notifier aux destinataires des données les mesures qu’il a prises.

« Le délai de mise en conformité peut être fixé à vingt-quatre heures en cas d’extrême urgence.

« Le président prononce, le cas échéant, la clôture de la procédure de mise en demeure.

« Le président peut demander au bureau de rendre publique la mise en demeure. Dans ce cas, la décision de clôture de la procédure de mise en demeure fait l’objet de la même publicité. »

</td>
<td>

**~~Chapitre VII : Sanctions prononcées par la formation restreinte de la Commission nationale de l'informatique et des libertés.~~**

**_Chapitre VII :_ Mesures et sanctions prises par la formation restreinte de la Commission nationale de l’informatique et des libertés**

**_Article 45_**

**~~I. - Lorsque le responsable d'un traitement ne respecte pas les obligations découlant de la présente loi, le président de la Commission nationale de l'informatique et des libertés peut le mettre en demeure de faire cesser le manquement constaté dans un délai qu'il fixe. En cas d'extrême urgence, ce délai peut être ramené à vingt-quatre heures.~~**

**~~Si le responsable du traitement se conforme à la mise en demeure qui lui est adressée, le président de la commission prononce la clôture de la procédure.~~**

**~~Dans le cas contraire, la formation restreinte de la commission peut prononcer, après une procédure contradictoire, les sanctions suivantes :~~**

**~~1° Un avertissement ;~~**

**~~2° Une sanction pécuniaire, dans les conditions prévues à l'article 47, à l'exception des cas où le traitement est mis en œuvre par l'Etat ;~~**

**~~3° Une injonction de cesser le traitement, lorsque celui-ci relève de l'article 22, ou un retrait de l'autorisation accordée en application de l'article 25.~~**

**~~Lorsque le manquement constaté ne peut faire l'objet d'une mise en conformité dans le cadre d'une mise en demeure, la formation restreinte peut prononcer, sans mise en demeure préalable et après une~~** **~~procédure contradictoire, les sanctions prévues au présent I.~~**

**~~II. - Lorsque la mise en œuvre d'un traitement ou l'exploitation des données traitées entraîne une violation des droits et libertés mentionnés à l'article 1er, la formation restreinte, saisie par le président de la commission, peut, dans le cadre d'une procédure d'urgence définie par décret en Conseil d'Etat, après une procédure contradictoire :~~**

**~~1° Décider l'interruption de la mise en œuvre du traitement, pour une durée maximale de trois mois, si le traitement n'est pas au nombre de ceux qui sont mentionnés aux I et II de l'article 26 ou de ceux mentionnés à l'article 27 mis en œuvre par l'Etat ;~~**

**~~2° Prononcer un avertissement visé au 1° du I ;~~**

**~~3° Décider le verrouillage de certaines des données à caractère personnel traitées, pour une durée maximale de trois mois, si le traitement n'est pas au nombre de ceux qui sont mentionnés aux I et II de l'article 26 ;~~**

**~~4° Informer le Premier ministre pour qu'il~~** **~~prenne, le cas échéant, les mesures permettant de faire cesser la violation constatée, si le traitement en cause est au nombre de ceux qui sont mentionnés aux mêmes I et II de l'article 26 ; le Premier ministre fait alors connaître à la formation restreinte les suites qu'il a données à cette information au plus tard quinze jours après l'avoir reçue.~~**

**~~III. - En cas d'atteinte grave et immédiate aux droits et libertés mentionnés à l'article 1er, le président de la commission peut demander, par la voie du référé, à la juridiction compétente d'ordonner, le cas échéant sous astreinte, toute mesure nécessaire à la sauvegarde de ces droits et libertés.~~**

**I. - Le président de la Commission nationale de l'informatique et des libertés peut avertir un responsable du traitement ou un sous-traitant du fait que les opérations de traitement envisagées sont susceptibles de violer les dispositions du règlement (UE) 2016/679 ou de la présente loi.**

**II. - Lorsque le responsable du traitement ou le sous-traitant ne respecte pas les obligations résultant du règlement (UE) 2016/679 ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut saisir la formation restreinte de la commission en vue du prononcé, après procédure contradictoire, de l’une ou de plusieurs des mesures suivantes :**

**1° Un rappel à l’ordre ;**

**2° Une injonction de mettre en conformité le traitement avec les obligations résultant de la présente loi ou du règlement (UE) 2016/679 ou de satisfaire aux demandes présentées par la personne concernée en vue d'exercer ses droits, qui peut être assortie, sauf dans des cas où le traitement est mis en œuvre par l'Etat, d’une astreinte dont le montant ne peut excéder 100 000 € par jour ;**

**3° A l’exception des traitements qui intéressent la sûreté de l’Etat ou la défense, la limitation temporaire ou définitive du traitement, son interdiction ou le retrait d’une autorisation accordée en application du règlement (UE) 2016/679 ou de la présente loi ;**

**4° Le retrait d’une certification ou l’injonction, à l’organisme concerné, de refuser ou de retirer la certification accordée ;**

**5° La suspension des flux de données adressées à un destinataire situé dans un pays tiers ou à une organisation internationale ;**

**6° Le retrait de la décision d’approbation d’une règle d’entreprise contraignante ;**

**7° A l'exception des cas où le traitement est mis en œuvre par l'Etat, une amende administrative ne pouvant excéder 10 millions d’euros ou, s’agissant d’une entreprise, 2 % du chiffre d’affaires annuel mondial total de l’exercice précédent, le montant le plus élevé étant retenu. Dans les hypothèses mentionnées aux paragraphes 5 et 6 de l’article 83 du règlement (UE) 2016/679, ces plafonds sont portés respectivement à 20 millions d’euros et 4 % du chiffre d’affaires. La formation restreinte prend en compte, dans la détermination du montant de l’amende, les critères précisés à l’article 83 du règlement (UE) 2016/679.**

**Lorsque la formation restreinte a prononcé une sanction pécuniaire devenue définitive avant que le juge pénal ait statué définitivement sur les mêmes faits ou des faits connexes, celui-ci peut ordonner que l’amende administrative s'impute sur l'amende pénale qu'il prononce.**

**Les sanctions pécuniaires sont recouvrées comme les créances de l'Etat étrangères à l'impôt et au domaine.**

**Le projet de mesure est le cas échéant soumis aux autres autorités concernées selon les modalités définies à l’article 60 du règlement (UE) 2016/679.**

**III. - Lorsque le responsable d'un traitement ou le sous-traitant ne respecte pas les obligations découlant du règlement (UE) 2016/679 ou de la présente loi , le président de la Commission nationale de l'informatique et des libertés peut également prononcer à son égard une mise en demeure, dans le délai qu’il fixe : **

**1° De satisfaire aux demandes présentées par la personne concernée en vue d'exercer ses droits ;**

**2° De mettre les opérations de traitement en conformité avec les dispositions applicables ;**

**3° A l’exception des traitements qui intéressent la sûreté de l’Etat ou la défense et ceux mentionnées à l’article 27, de communiquer à la personne concernée une violation de données à caractère personnel ;**

**4° De rectifier ou d'effacer des données à caractère personnel, ou de limiter le traitement.**

**Dans le cas prévu au 4°, le président peut, dans les mêmes conditions, mettre en demeure le responsable de traitement ou le sous-traitant de notifier aux destinataires des données les mesures qu’il a prises.**

**Le délai de mise en conformité peut être fixé à vingt-quatre heures en cas d’extrême urgence.**

**Le président prononce, le cas échéant, la clôture de la procédure de mise en demeure.**

**Le président peut demander au bureau de rendre publique la mise en demeure. Dans ce cas, la décision de clôture de la procédure de mise en demeure fait l’objet de la même publicité.**

</td></tr>
<tr>
<td>

**Article 6 (III)**

_[Mesures correctrices]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 46_**

Les sanctions prévues au I et au 1° du II de l'article 45 sont prononcées sur la base d'un rapport établi par l'un des membres de la Commission nationale de l'informatique et des libertés, désigné par le président de celle-ci parmi les membres n'appartenant pas à la formation restreinte. Ce rapport est notifié au responsable du traitement, qui peut déposer des observations et se faire représenter ou assister. Le rapporteur peut présenter des observations orales à la formation restreinte mais ne prend pas part à ses délibérations. La formation restreinte peut entendre toute personne dont l'audition lui paraît susceptible de contribuer utilement à son information, y compris, à la demande du secrétaire général, les agents des services.

La formation restreinte peut rendre publiques les sanctions qu'elle prononce. Elle peut ordonner que les personnes sanctionnées informent individuellement de cette sanction, à leur frais, chacune des personnes concernées. Elle peut également ordonner leur insertion dans des publications, journaux et supports qu'elle désigne aux frais des personnes sanctionnées. Le président de la commission peut demander au bureau de rendre publique la mise en demeure prévue au deuxième alinéa du I de l'article 45. Lorsque le président de la commission prononce la clôture de la procédure dans les conditions définies au troisième alinéa du même I, la clôture fait l'objet de la même mesure de publicité que celle, le cas échéant, de la mise en demeure.

Les décisions prises par la formation restreinte au titre de l'article 45 sont motivées et notifiées au responsable du traitement. Les décisions prononçant une sanction peuvent faire l'objet d'un recours de pleine juridiction devant le Conseil d'Etat.

</td>
<td>

III.- L’article 46 de la même loi est remplacé par les dispositions suivantes :

« _Art. 46._ \- I. - Lorsque le non-respect des dispositions du règlement (UE) 2016/679 ou de la présente loi entraîne une violation des droits et libertés mentionnés à l'article 1er et que le président de la commission considère qu’il est urgent d’intervenir, il saisit la formation restreinte qui peut, dans le cadre d'une procédure d'urgence contradictoire définie par décret en Conseil d’Etat, adopter l’une des mesures suivantes :

« 1° L'interruption provisoire de la mise en œuvre du traitement, y compris d’un transfert de données hors de l’Union européenne, pour une durée maximale de trois mois, si le traitement n'est pas au nombre de ceux qui sont mentionnés aux I et II de l'article 26 et ceux mentionnées à l’article 27 ;

« 2° La limitation du traitement de certaines des données à caractère personnel traitées, pour une durée maximale de trois mois, si le traitement n'est pas au nombre de ceux qui sont mentionnés aux I et II de l'article 26 ;

« 3° La suspension provisoire de la certification délivrée au responsable du traitement ou au sous-traitant ;

« 4° La suspension provisoire de l’agrément délivré à un organisme de certification ou un organisme chargé du respect d’un code de conduite ;

« 5° La suspension provisoire de l’autorisation délivrée sur le fondement du III de l’article 54 du chapitre IX de la présente loi.

« 6° L’injonction de mettre en conformité le traitement avec les obligations résultant du règlement (UE) 2016/679 ou de la présente loi, qui peut être assortie, sauf dans des cas où le traitement est mis en œuvre par l'Etat, d’une astreinte dont le montant ne peut excéder 100 000 € par jour ;

« 7° Un rappel à l’ordre ;

« 8° L’information du Premier ministre pour qu'il prenne, le cas échéant, les mesures permettant de faire cesser la violation constatée, si le traitement en cause est au nombre de ceux qui sont mentionnés aux mêmes I et II de l'article 26. Le Premier ministre fait alors connaître à la formation restreinte les suites qu'il a données à cette information au plus tard quinze jours après l'avoir reçue.

« II. - Dans les circonstances exceptionnelles prévues au 1 de l’article 66 du règlement (UE) 2016/679, lorsque la formation restreinte adopte les mesures provisoires prévues aux 1° à 4° du I du présent article, elle informe sans délai de la teneur des mesures prises et de leurs motifs les autres autorités de contrôle concernées, le Comité européen de la protection des données et la Commission européenne.

« Lorsque la formation restreinte a pris de telles mesures et qu’elle estime que des mesures définitives doivent être prises, elle met en œuvre les dispositions du 2 de l’article 66 du règlement.

« III. - Pour les traitements régis par le chapitre XIII, lorsqu’une autorité de contrôle compétente en vertu du règlement (UE) 2016/679 n’a pas pris de mesure appropriée dans une situation où il est urgent d’intervenir afin de protéger les droits et libertés des personnes concernées, la formation restreinte, saisie par le président de la commission, peut demander au comité européen de la protection des données un avis d’urgence ou une décision contraignante d’urgence dans les conditions et selon les modalités prévues aux 3 et 4 de l’article 66 de ce règlement.

« IV. **-** En cas d'atteinte grave et immédiate aux droits et libertés mentionnés à l’article 1 er, le président de la commission peut en outre demander, par la voie du référé, à la juridiction compétente d'ordonner, le cas échéant sous astreinte, toute mesure nécessaire à la sauvegarde de ces droits et libertés. »

</td>
<td>

**_Article 46_**

**~~Les sanctions prévues au I et au 1° du II de l'article 45 sont prononcées sur la base d'un rapport établi par l'un des membres de la Commission nationale de l'informatique et des libertés, désigné par le président de celle-ci parmi les membres n'appartenant pas à la formation restreinte. Ce rapport est notifié au responsable du traitement, qui peut déposer des observations et se faire représenter ou assister. Le rapporteur~~** **~~peut présenter des observations orales à la formation restreinte mais ne prend pas part à ses délibérations. La formation restreinte peut entendre toute personne dont l'audition lui paraît susceptible de contribuer utilement à son information, y compris, à la demande du secrétaire général, les agents des services.~~**

**~~La formation restreinte peut rendre publiques les sanctions qu'elle prononce. Elle peut ordonner que les personnes sanctionnées informent individuellement de cette sanction, à leur frais, chacune des personnes concernées. Elle peut également ordonner leur insertion dans des publications, journaux et supports qu'elle désigne aux frais des personnes sanctionnées. Le président de la commission peut demander au bureau de rendre publique la mise en demeure prévue au deuxième alinéa du I de l'article 45. Lorsque le président de la commission prononce la clôture de la procédure dans les conditions définies au troisième alinéa du même I, la clôture fait l'objet de la même mesure de publicité que celle, le cas échéant, de la mise en demeure.~~**

**~~Les décisions prises par la formation restreinte au titre de l'article 45 sont~~** **~~motivées et notifiées au responsable du traitement. Les décisions prononçant une sanction peuvent faire l'objet d'un recours de pleine juridiction devant le Conseil d'Etat.~~**

**Article 46**

**I. - Lorsque le non-respect des dispositions du règlement (UE) 2016/679 ou de la présente loi entraîne une violation des droits et libertés mentionnés à l'article 1 er et que le président de la commission considère qu’il est urgent d’intervenir, il saisit la formation restreinte qui peut, dans le cadre d'une procédure d'urgence contradictoire définie par décret en Conseil d’Etat, adopter l’une des mesures suivantes :**

**1° L'interruption provisoire de la mise en œuvre du traitement, y compris d’un transfert de données hors de l’Union européenne, pour une durée maximale de trois mois, si le traitement n'est pas au nombre de ceux qui sont mentionnés aux I et II de l'article 26 et ceux mentionnées à l’article 27 ;**

**2° La limitation du traitement de certaines des données à caractère personnel traitées, pour une durée maximale de trois mois, si le traitement n'est pas au nombre de ceux qui sont mentionnés aux I et II de l'article 26 ;**

**3° La suspension provisoire de la certification délivrée au responsable du traitement ou au sous-traitant ;**

**4° La suspension provisoire de l’agrément délivré à un organisme de certification ou un organisme chargé du respect d’un code de conduite ;**

**5° La suspension provisoire de l’autorisation délivrée sur le fondement du III de l’article 54 du chapitre IX de la présente loi.**

**6° L’injonction de mettre en conformité le traitement avec les obligations résultant du règlement (UE) 2016/679 ou de la présente loi, qui peut être assortie, sauf dans des cas où le traitement est mis en œuvre par l'Etat, d’une astreinte dont le montant ne peut excéder 100 000 € par jour ;**

**7° Un rappel à l’ordre ;**

**8° L’information du Premier ministre pour qu'il prenne, le cas échéant, les mesures permettant de faire cesser la violation constatée, si le traitement en cause est au nombre de ceux qui sont mentionnés aux mêmes I et II de l'article 26. Le Premier ministre fait alors connaître à la formation restreinte les suites qu'il a données à cette information au plus tard quinze jours après l'avoir reçue.**

**II. - Dans les circonstances exceptionnelles prévues au 1 de l’article 66 du règlement (UE) 2016/679, lorsque la formation restreinte adopte les mesures provisoires prévues aux 1° à 4° du I du présent article, elle informe sans délai de la teneur des mesures prises et de leurs motifs les autres autorités de contrôle concernées, le Comité européen de la protection des données et la Commission européenne. **

**Lorsque la formation restreinte a pris de telles mesures et qu’elle estime que des mesures définitives doivent être prises, elle met en œuvre les dispositions du 2 de l’article 66 du règlement.**

**III. - Pour les traitements régis par le chapitre XIII, lorsqu’une autorité de contrôle compétente en vertu du règlement (UE) 2016/679 n’a pas pris de mesure appropriée dans une situation où il est urgent d’intervenir afin de protéger les droits et libertés des personnes concernées, la formation restreinte, saisie par le président de la commission, peut demander au comité européen de la protection des données un avis d’urgence ou une décision contraignante d’urgence dans les conditions et selon les modalités prévues aux 3 et 4 de l’article 66 de ce règlement.**

**IV. - En cas d'atteinte grave et immédiate aux droits et libertés mentionnés à l’article 1 er, le président de la commission peut en outre demander, par la voie du référé, à la juridiction compétente d'ordonner, le cas échéant sous astreinte, toute mesure nécessaire à la sauvegarde de ces droits et libertés. **

</td></tr>
<tr>
<td>

**Article 6 (IV)**

_[Mesures correctrices]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 47_**

Le montant de la sanction pécuniaire prévue au I de l'article 45 est proportionné à la gravité du manquement commis et aux avantages tirés de ce manquement. La formation restreinte de la Commission nationale de l'informatique et des libertés prend notamment en compte le caractère intentionnel ou de négligence du manquement, les mesures prises par le responsable du traitement pour atténuer les dommages subis par les personnes concernées, le degré de coopération avec la commission afin de remédier au manquement et d'atténuer ses effets négatifs éventuels, les catégories de données à caractère personnel concernées et la manière dont le manquement a été porté à la connaissance de la commission.

Le montant de la sanction ne peut excéder 3 millions d'euros.

Lorsque la formation restreinte a prononcé une sanction pécuniaire devenue définitive avant que le juge pénal ait statué définitivement sur les mêmes faits ou des faits connexes, celui-ci peut ordonner que la sanction pécuniaire s'impute sur l'amende qu'il prononce.

Les sanctions pécuniaires sont recouvrées comme les créances de l'Etat étrangères à l'impôt et au domaine.

</td>
<td>

IV.- L’article 47 de la même loi est remplacé par les dispositions suivantes :

« _Art. 47._ \- Les mesures prévues au II de l’article 45 et aux 1° à 6° du I de l’article 46 sont prononcées sur la base d'un rapport établi par l'un des membres de la Commission nationale de l'informatique et des libertés, désigné par le président de celle-ci parmi les membres n'appartenant pas à la formation restreinte. Ce rapport est notifié au responsable du traitement ou au sous-traitant, qui peut déposer des observations et se faire représenter ou assister. Le rapporteur peut présenter des observations orales à la formation restreinte mais ne prend pas part à ses délibérations. La formation restreinte peut entendre toute personne dont l'audition lui paraît susceptible de contribuer utilement à son information, y compris, à la demande du secrétaire général, les agents des services.

« La formation restreinte peut rendre publiques les mesures qu'elle prend. Elle peut également ordonner leur insertion dans des publications, journaux et supports qu'elle désigne aux frais des personnes sanctionnées.

« Sans préjudice des obligations d’information qui leur incombent en application de l’article 34 du règlement (UE) 2016/679, la formation restreinte peut ordonner que le responsable ou le sous-traitant concerné informe individuellement, à ses frais, chacune des personnes concernées de la violation des dispositions de la présente loi ou du règlement précité relevée ainsi que, le cas échéant, de la mesure prononcée. »

</td>
<td>

**_Article 47_**

**~~Le montant de la sanction pécuniaire prévue au I de l'article 45 est proportionné à la gravité du manquement commis et aux avantages tirés de ce manquement. La formation restreinte de la Commission nationale de l'informatique et des libertés prend notamment en compte le caractère intentionnel ou de négligence du manquement, les mesures prises par le responsable du traitement pour atténuer les dommages subis par les personnes concernées, le degré de coopération avec la commission afin de remédier au manquement et d'atténuer ses effets négatifs éventuels, les catégories de données à caractère personnel concernées et la manière dont le manquement a été porté à la connaissance de la commission.~~**

**~~Le montant de la sanction ne peut excéder 3 millions d'euros.~~**

**~~Lorsque la formation restreinte a prononcé une sanction pécuniaire devenue définitive avant que le juge pénal ait statué définitivement sur les mêmes faits ou des faits connexes, celui-ci peut ordonner que la sanction pécuniaire s'impute sur l'amende qu'il prononce.~~**

**~~Les sanctions pécuniaires sont recouvrées comme les créances de l'Etat étrangères à l'impôt et au domaine.~~**

**_Art. 47._ \- Les mesures prévues au II de l’article 45 et aux 1° à 6° du I de l’article 46 sont prononcées sur la base d'un rapport établi par l'un des membres de la Commission nationale de l'informatique et des libertés, désigné par le président de celle-ci parmi les membres n'appartenant pas à la formation restreinte. Ce rapport est notifié au responsable du traitement ou au sous-traitant, qui peut déposer des observations et se faire représenter ou assister. Le rapporteur peut présenter des observations orales à la formation restreinte mais ne prend pas part à ses délibérations. La formation restreinte peut entendre toute personne dont l'audition lui paraît susceptible de contribuer utilement à son information, y compris, à la demande du secrétaire général, les agents des services.**

**La formation restreinte peut rendre publiques les mesures qu'elle prend. Elle peut également ordonner leur insertion dans des publications, journaux et supports qu'elle désigne aux frais des personnes sanctionnées.**

**Sans préjudice des obligations d’information qui leur incombent en application de l’article 34 du règlement (UE) 2016/679, la formation restreinte peut ordonner que le responsable ou le sous-traitant concerné informe individuellement, à ses frais, chacune des personnes concernées de la violation des dispositions de la présente loi ou du règlement précité relevée ainsi que, le cas échéant, de la mesure prononcée. **

</td></tr>
<tr>
<td>

**Article 6 (V)**

_[Mesures correctrices]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 48_**

Les pouvoirs prévus à l'article 44 ainsi qu'au I, au 1° du II et au III de l'article 45 peuvent être exercés à l'égard des traitements dont les opérations sont mises en œuvre, en tout ou partie, sur le territoire national, y compris lorsque le responsable du traitement est établi sur le territoire d'un autre Etat membre de la Communauté européenne.

</td>
<td>

V.- L’article 48 de la même loi est remplacé par les dispositions suivantes :

« _Art. 48. -_ Lorsqu’un organisme de certification ou un organisme chargé du respect d’un code de conduite a manqué à ses obligations ou n’a pas respecté les dispositions du règlement (UE) 2016/679 ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut, le cas échéant après mise en demeure, saisir la formation restreinte de la Commission qui peut prononcer, dans les mêmes conditions que celles prévues aux articles 45 à 47, le retrait de l’agrément qui leur a été délivré. »

</td>
<td>

**_Article 48_**

**~~Les pouvoirs prévus à l'article 44 ainsi qu'au I, au 1° du II et au III de l'article 45 peuvent être exercés à l'égard des traitements dont les opérations sont mises en oeuvre, en tout ou partie, sur le territoire national, y compris lorsque le responsable du traitement est établi sur le territoire d'un autre Etat membre de la Communauté européenne.~~**

**_Art. 48. -_ Lorsqu’un organisme de certification ou un organisme chargé du respect d’un code de conduite a manqué à ses obligations ou n’a pas respecté les dispositions du règlement (UE) 2016/679 ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut, le cas échéant après mise en demeure, saisir la formation restreinte de la Commission qui peut prononcer, dans les mêmes conditions que celles prévues aux articles 45 à 47, le retrait de l’agrément qui leur a été délivré. **

</td></tr>
<tr>
<td>

**Chapitre II – Dispositions relatives à certaines catégories de données**

</td></tr>
<tr>
<td>

**Article 7**

[ _Mise en conformité de l’article 8 de la loi 78-17 avec l’article 9 du règlement_ ]

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 8_**

I. - Il est interdit de collecter ou de traiter des données à caractère personnel qui font apparaître, directement ou indirectement, les origines raciales ou ethniques, les opinions politiques, philosophiques ou religieuses ou l'appartenance syndicale des personnes, ou qui sont relatives à la santé ou à la vie sexuelle de celles-ci.

II. - Dans la mesure où la finalité du traitement l'exige pour certaines catégories de données, ne sont pas soumis à l'interdiction prévue au I :

1° Les traitements pour lesquels la personne concernée a donné son consentement exprès, sauf dans le cas où la loi prévoit que l'interdiction visée au I ne peut être levée par le consentement de la personne concernée ;

2° Les traitements nécessaires à la sauvegarde de la vie humaine, mais auxquels la personne concernée ne peut donner son consentement par suite d'une incapacité juridique ou d'une impossibilité matérielle ;

3° Les traitements mis en oeuvre par une association ou tout autre organisme à but non lucratif et à caractère religieux, philosophique, politique ou syndical :

\- pour les seules données mentionnées au I correspondant à l'objet de ladite association ou dudit organisme ;

\- sous réserve qu'ils ne concernent que les membres de cette association ou de cet organisme et, le cas échéant, les personnes qui entretiennent avec celui-ci des contacts réguliers dans le cadre de son activité ;

\- et qu'ils ne portent que sur des données non communiquées à des tiers, à moins que les personnes concernées n'y consentent expressément ;

4° Les traitements portant sur des données à caractère personnel rendues publiques par la personne concernée ;

5° Les traitements nécessaires à la constatation, à l'exercice ou à la défense d'un droit en justice ;

6° Les traitements nécessaires aux fins de la médecine préventive, des diagnostics médicaux, de l'administration de soins ou de traitements, ou de la gestion de services de santé et mis en oeuvre par un membre d'une profession de santé, ou par une autre personne à laquelle s'impose en raison de ses fonctions l'obligation de secret professionnel prévue par l'article 226-13 du code pénal ;

7° Les traitements statistiques réalisés par l'Institut national de la statistique et des études économiques ou l'un des services statistiques ministériels dans le respect de la loi n° 51-711 du 7 juin 1951 sur l'obligation, la coordination et le secret en matière de statistiques, après avis du Conseil national de l'information statistique et dans les conditions prévues à l'article 25 de la présente loi ;

8° Les traitements nécessaires à la recherche, aux études et évaluations dans le domaine de la santé selon les modalités prévues au chapitre IX.

III. - Si les données à caractère personnel visées au I sont appelées à faire l'objet à bref délai d'un procédé d'anonymisation préalablement reconnu conforme aux dispositions de la présente loi par la Commission nationale de l'informatique et des libertés, celle-ci peut autoriser, compte tenu de leur finalité, certaines catégories de traitements selon les modalités prévues à l'article 25. Les dispositions du chapitre IX ne sont pas applicables.

IV. - De même, ne sont pas soumis à l'interdiction prévue au I les traitements, automatisés ou non, justifiés par l'intérêt public et soit autorisés dans les conditions prévues au I de l'article 25 ou au II de l'article 26, soit déclarés dans les conditions prévues au V de l'article 22.

</td>
<td>

L’article 8 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés est ainsi modifié :

1° Le I est ainsi rédigé :

« _I._ \- Il est interdit de traiter des données à caractère personnel, qui révèlent la prétendue origine raciale ou l’origine ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l'appartenance syndicale ou de traiter des données génétiques, des données biométriques aux fins d’identifier une personne physique de manière unique, des données concernant la santé ou des données concernant vie sexuelle ou l’orientation sexuelle d’une personne physique. » ;

2° Au 7° du II, les mots : « et dans les conditions prévues à l'article 25 de la présente loi » sont supprimés ;

3° Le 8° du II est remplacé par les dispositions suivantes : « _8_ ° Les traitements comportant des données concernant la santé justifiés par l’intérêt public et conformes aux dispositions du chapitre IX. » ;

4° Après le 8° du II, il est inséré un 9° ainsi rédigé :

« _9_ ° Les traitements mis en œuvre par les employeurs ou les administrations qui portent sur des données biométriques nécessaires aux contrôles de l’accès aux lieux de travail ainsi qu’aux appareils et aux applications utilisés dans le cadre des missions confiées aux salariés ou aux agents. » ;

5° Au III, la première phrase est remplacée par la phrase suivante :

« Ne sont également pas soumises à l’interdiction prévue au I les données à caractère personnel mentionnées au I qui sont appelées à faire l’objet, à bref délai, d’un procédé d’anonymisation préalablement reconnu conforme aux dispositions de la présente loi par la Commission nationale de l’informatique et des libertés. »

Et la seconde phrase est supprimée ;

6° Le IV est remplacé par les dispositions suivantes :

« _IV_. - De même, ne sont pas soumis à l’interdiction prévue au I les traitements, automatisés ou non, justifiés par l’intérêt public et autorisés dans les conditions prévues au II de l’article 26. »

</td>
<td>

**_Article 8_**

I. - **~~Il est interdit de collecter ou de traiter des données à caractère personnel qui font apparaître, directement ou indirectement, les origines raciales ou ethniques, les opinions politiques, philosophiques ou religieuses ou l'appartenance syndicale des personnes, ou qui sont relatives à la santé ou à la vie sexuelle de celles-ci.~~**

**Il est interdit de traiter des données à caractère personnel, qui révèlent la prétendue origine raciale ou l’origine ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l'appartenance syndicale ou de traiter des données génétiques, des données biométriques aux fins d’identifier une personne physique de manière unique, des données concernant la santé ou des données concernant vie sexuelle ou l’orientation sexuelle d’une personne physique.**

II. - Dans la mesure où la finalité du traitement l'exige pour certaines catégories de données, ne sont pas soumis à l'interdiction prévue au I :

1° Les traitements pour lesquels la personne concernée a donné son consentement exprès, sauf dans le cas où la loi prévoit que l'interdiction visée au I ne peut être levée par le consentement de la personne concernée ;

2° Les traitements nécessaires à la sauvegarde de la vie humaine, mais auxquels la personne concernée ne peut donner son consentement par suite d'une incapacité juridique ou d'une impossibilité matérielle ;

3° Les traitements mis en oeuvre par une association ou tout autre organisme à but non lucratif et à caractère religieux, philosophique, politique ou syndical :

\- pour les seules données mentionnées au I correspondant à l'objet de ladite association ou dudit organisme ;

\- sous réserve qu'ils ne concernent que les membres de cette association ou de cet organisme et, le cas échéant, les personnes qui entretiennent avec celui-ci des contacts réguliers dans le cadre de son activité ;

\- et qu'ils ne portent que sur des données non communiquées à des tiers, à moins que les personnes concernées n'y consentent expressément ;

4° Les traitements portant sur des données à caractère personnel rendues publiques par la personne concernée ;

5° Les traitements nécessaires à la constatation, à l'exercice ou à la défense d'un droit en justice ;

6° Les traitements nécessaires aux fins de la médecine préventive, des diagnostics médicaux, de l'administration de soins ou de traitements, ou de la gestion de services de santé et mis en oeuvre par un membre d'une profession de santé, ou par une autre personne à laquelle s'impose en raison de ses fonctions l'obligation de secret professionnel prévue par l'article 226-13 du code pénal ;

7° Les traitements statistiques réalisés par l'Institut national de la statistique et des études économiques ou l'un des services statistiques ministériels dans le respect de la loi n° 51-711 du 7 juin 1951 sur l'obligation, la coordination et le secret en matière de statistiques, après avis du Conseil national de l'information statistique **~~et dans les conditions prévues à l'article 25 de la présente loi~~** ;

8° **~~Les traitements nécessaires à la recherche, aux études et évaluations dans le domaine de la santé selon les modalités prévues au chapitre IX.~~** **Les traitements comportant des données concernant la santé justifiés par l’intérêt public et conformes aux dispositions du chapitre IX.**

**_9_ ° Les traitements mis en œuvre par les employeurs ou les administrations qui portent sur des données biométriques nécessaires aux contrôles de l’accès aux lieux de travail ainsi qu’aux appareils et aux applications utilisés dans le cadre des missions confiées aux salariés ou aux agents. **

III. - **~~Si les données à caractère personnel visées au I sont appelées à faire l'objet à bref délai d'un procédé d'anonymisation préalablement reconnu conforme aux dispositions de la présente loi par la Commission nationale de l'informatique et des libertés, celle-ci peut autoriser, compte tenu de leur finalité, certaines catégories de traitements selon les modalités prévues à l'article 25~~**. **Ne sont également pas soumises à l’interdiction prévue au I les données à caractère personnel mentionnées au I qui sont appelées à faire l’objet, à bref délai, d’un procédé d’anonymisation préalablement reconnu conforme aux dispositions de la présente loi par la Commission nationale de l’informatique et des libertés.** **~~Les dispositions du chapitre IX ne sont pas applicables.~~**

IV. - **~~De même, ne sont pas soumis à l'interdiction prévue au I les traitements, automatisés ou non, justifiés par l'intérêt public et soit autorisés dans les conditions prévues au I de l'article 25 ou au II de l'article 26, soit déclarés dans les conditions prévues au V de l'article 22.~~**

**De même, ne sont pas soumis à l’interdiction prévue au I les traitements, automatisés ou non, justifiés par l’intérêt public et autorisés dans les conditions prévues au II de l’article 26.**

</td></tr>
<tr>
<td>

**_Titre II_**

**_Marges de manœuvre permises par le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE_**

</td></tr>
<tr>
<td>

**Chapitre I – Champ d’application territorial des dispositions complétant le règlement (UE) 2016/679**

</td></tr>
<tr>
<td>

**Article 8**

_[Application du critère de résidence aux marges de manœuvre]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

</td>
<td>

Après l’article 5 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, il est inséré un article 5-1 ainsi rédigé :

« _Art. 5-1 -_ Les règles nationales, prises sur le fondement des dispositions du règlement (UE) 2016/679 renvoyant au droit national le soin d’adapter ou de compléter les droits et obligations prévus par ce règlement, s’appliquent dès lors que la personne concernée réside en France, y compris lorsque le responsable de traitement n’est pas établi en France.

« Toutefois, lorsqu'est en cause un des traitements mentionnés au 2 de l'article 85 du même règlement, les règles nationales mentionnées au premier alinéa sont celles dont relève le responsable de traitement, lorsqu’il est établi dans l’Union européenne. »

</td>
<td>

**_Article 5-1_**

**Les règles nationales, prises sur le fondement des dispositions du règlement (UE) 2016/679 renvoyant au droit national le soin d’adapter ou de compléter les droits et obligations prévus par ce règlement, s’appliquent dès lors que la personne concernée réside en France, y compris lorsque le responsable de traitement n’est pas établi en France.**

**Toutefois, lorsqu'est en cause un des traitements mentionnés au 2 de l'article 85 du même règlement, les règles nationales mentionnées au premier alinéa sont celles dont relève le responsable de traitement, lorsqu’il est établi dans l’Union européenne.**

</td></tr>
<tr>
<td>

**_Chapitre II – Dispositions relatives à la simplification des formalités préalables à la mise en œuvre des traitements_**

</td></tr>
<tr>
<td>

**Article 9 (I)**

_[Allègement des formalités préalables]_

</td>
<td>

_**Article 22**_

I. - A l'exception de ceux qui relèvent des dispositions prévues aux articles 25, 26 et 27 ou qui sont visés au deuxième alinéa de l'article 36, les traitements automatisés de données à caractère personnel font l'objet d'une déclaration auprès de la Commission nationale de l'informatique et des libertés.

I bis.-Par dérogation au 1° des I et II de l'article 27, font également l'objet d'une déclaration auprès de la Commission nationale de l'informatique et des libertés les traitements qui portent sur des données à caractère personnel parmi lesquelles figure le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques ou qui requièrent une consultation de ce répertoire, lorsque ces traitements ont exclusivement des finalités de statistique publique, sont mis en œuvre par le service statistique public et ne comportent aucune des données mentionnées au I de l'article 8 ou à l'article 9, à la condition que le numéro d'inscription à ce répertoire ait préalablement fait l'objet d'une opération cryptographique lui substituant un code statistique non signifiant, ainsi que les traitements ayant comme finalité exclusive de réaliser cette opération cryptographique. L'utilisation du code statistique non signifiant n'est autorisée qu'au sein du service statistique public. L'opération cryptographique est renouvelée à une fréquence définie par décret en Conseil d'Etat pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés.

II. - Toutefois, ne sont soumis à aucune des formalités préalables prévues au présent chapitre :

1° Les traitements ayant pour seul objet la tenue d'un registre qui, en vertu de dispositions législatives ou réglementaires, est destiné exclusivement à l'information du public et est ouvert à la consultation de celui-ci ou de toute personne justifiant d'un intérêt légitime ;

2° Les traitements mentionnés au 3° du II de l'article 8.

III. - Les traitements pour lesquels le responsable a désigné un correspondant à la protection des données à caractère personnel chargé d'assurer, d'une manière indépendante, le respect des obligations prévues dans la présente loi sont dispensés des formalités prévues aux articles 23 et 24, sauf lorsqu'un transfert de données à caractère personnel à destination d'un Etat non membre de la Communauté européenne est envisagé.

La désignation du correspondant est notifiée à la Commission nationale de l'informatique et des libertés. Elle est portée à la connaissance des instances représentatives du personnel.

Le correspondant est une personne bénéficiant des qualifications requises pour exercer ses missions. Il tient une liste des traitements effectués immédiatement accessible à toute personne en faisant la demande et ne peut faire l'objet d'aucune sanction de la part de l'employeur du fait de l'accomplissement de ses missions. Il peut saisir la Commission nationale de l'informatique et des libertés des difficultés qu'il rencontre dans l'exercice de ses missions.

En cas de non-respect des dispositions de la loi, le responsable du traitement est enjoint par la Commission nationale de l'informatique et des libertés de procéder aux formalités prévues aux articles 23 et 24. En cas de manquement constaté à ses devoirs, le correspondant est déchargé de ses fonctions sur demande, ou après consultation, de la Commission nationale de l'informatique et des libertés.

IV. - Le responsable d'un traitement de données à caractère personnel qui n'est soumis à aucune des formalités prévues au présent chapitre communique à toute personne qui en fait la demande les informations relatives à ce traitement mentionnées aux 2° à 6° du I de l'article 31.

V. - Les traitements de données de santé à caractère personnel mis en œuvre par les organismes ou les services chargés d'une mission de service public figurant sur une liste fixée par arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l'informatique et des libertés, afin de répondre, en cas de situation d'urgence, à une alerte sanitaire, au sens de l'article L. 1413-1 du code de la santé publique, sont soumis au régime de la déclaration préalable prévu au présent article. Le responsable de traitement rend compte chaque année à la Commission nationale de l'informatique et des libertés des traitements ainsi mis en œuvre.

Les conditions dans lesquelles ces traitements peuvent utiliser le numéro d'inscription au répertoire national d'identification des personnes physiques sont définies par décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés.

</td>
<td>

I. - L’article 22 de la même loi est remplacé par les dispositions suivantes :

« _Art. 22. -_ Un __ décret en Conseil d’Etat, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés __ détermine les catégories de responsables de traitement et les finalités de ces traitements au vu desquelles ces derniers peuvent être mis en œuvre lorsqu’ils portent sur des données comportant le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques. La mise en œuvre des traitements intervient sans préjudice des obligations qui incombent aux responsables de traitement ou aux sous-traitants en vertu de la section 3 du chapitre IV du règlement (UE) 2016/679.

« Ne sont pas soumis aux dispositions du premier alinéa ceux des traitements portant sur des données à caractère personnel parmi lesquelles figure le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques ou qui requièrent une consultation de ce répertoire :

« 1° Qui ont exclusivement des finalités de statistique publique, mis en œuvre par le service statistique public et ne comportent aucune des données mentionnées au I de l'article 8 ou à l'article 9 ;

« 2° Qui ont exclusivement des finalités de recherche scientifique ou historique ;

« 3° Qui mettent à la disposition des usagers de l'administration un ou plusieurs téléservices de l'administration électronique définis à l'article 1er de l'ordonnance n° 2005-1516 du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives, mis en œuvre par l'Etat ou une personne morale de droit public ou une personne morale de droit privé gérant un service public.

« Pour les traitements dont les finalités sont mentionnées aux 1° et 2°, le numéro d'inscription au répertoire national d'identification des personnes physiques fait l'objet préalablement d'une opération cryptographique lui substituant un code statistique non signifiant. Cette opération est renouvelée à une fréquence définie par décret en Conseil d'Etat pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés. Les traitements ayant comme finalité exclusive de réaliser cette opération cryptographique ne sont pas soumis aux dispositions du premier alinéa.

« Pour les traitements dont les finalités sont mentionnées au 1°, l'utilisation du code statistique non signifiant n'est autorisée qu'au sein du service statistique public.

« Pour les traitements dont les finalités sont mentionnées au 2°, l'opération cryptographique et, le cas échéant, l'interconnexion de deux fichiers par l'utilisation du code spécifique non signifiant qui en est issu, ne peuvent être assurés par la même personne ni par le responsable de traitement.

« A l’exception des traitements mentionnés au second alinéa de l’article 55, le présent article n’est pas applicable aux traitements de données à caractère personnel dans le domaine de la santé qui sont régis par les dispositions du chapitre IX. »

</td>
<td>

**_Article 22_**

**~~I. - A l'exception de ceux qui relèvent des dispositions prévues aux articles 25, 26 et 27 ou qui sont visés au deuxième alinéa de l'article 36, les traitements automatisés de données à caractère personnel font l'objet d'une déclaration auprès de la Commission nationale de l'informatique et des libertés.~~**

**~~I bis.-Par dérogation au 1° des I et II de l'article 27, font également l'objet d'une déclaration auprès de la Commission nationale de l'informatique et des libertés les traitements qui portent sur des données à caractère personnel parmi lesquelles figure le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques ou qui requièrent une consultation de ce répertoire, lorsque ces traitements ont exclusivement des finalités de statistique publique, sont mis en œuvre par le service statistique public et ne comportent aucune des données mentionnées au I de l'article 8 ou à l'article 9, à la condition que le numéro d'inscription à ce répertoire ait préalablement fait l'objet d'une opération cryptographique lui substituant un code statistique non signifiant, ainsi que les traitements ayant comme finalité exclusive de réaliser cette opération cryptographique. L'utilisation du code statistique non signifiant n'est autorisée qu'au sein du service statistique public. L'opération cryptographique est renouvelée à une fréquence définie par décret en Conseil d'Etat pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés.~~**

**~~II. - Toutefois, ne sont soumis à aucune des formalités préalables prévues au présent chapitre :~~**

**~~1° Les traitements ayant pour seul objet la tenue d'un registre qui, en vertu de dispositions législatives ou réglementaires, est destiné exclusivement à l'information du public et est ouvert à la consultation de celui-ci ou de toute personne justifiant d'un intérêt légitime ;~~**

**~~2° Les traitements mentionnés au 3° du II de l'article 8.~~**

**~~III. - Les traitements pour lesquels le responsable a désigné un correspondant à la protection des données à caractère personnel chargé d'assurer, d'une manière indépendante, le respect des obligations prévues dans la présente loi sont dispensés des formalités prévues aux articles 23 et 24, sauf lorsqu'un transfert de données à caractère personnel à destination d'un Etat non membre de la Communauté européenne est envisagé.~~**

**~~La désignation du correspondant est notifiée à la Commission nationale de l'informatique et des libertés. Elle est portée à la connaissance des instances représentatives du personnel.~~**

**~~Le correspondant est une personne bénéficiant des qualifications requises pour exercer ses missions. Il tient une liste des traitements effectués immédiatement accessible à toute personne en faisant la demande et ne peut faire l'objet d'aucune sanction de la part de l'employeur du fait de l'accomplissement de ses missions. Il peut saisir la Commission nationale de l'informatique et des libertés des difficultés qu'il rencontre dans l'exercice de ses missions.~~**

**~~En cas de non-respect des dispositions de la loi, le responsable du traitement est enjoint par la Commission nationale de l'informatique et des libertés de procéder aux formalités prévues aux articles 23 et 24. En cas de manquement constaté à ses devoirs, le correspondant est déchargé de ses fonctions sur demande, ou après consultation, de la Commission nationale de l'informatique et des libertés.~~**

**~~IV. - Le responsable d'un traitement de données à caractère personnel qui n'est soumis à aucune des formalités prévues au présent chapitre communique à toute personne qui en fait la demande les informations relatives à ce traitement mentionnées aux 2° à 6° du I de l'article 31.~~**

**~~V. - Les traitements de données de santé à caractère personnel mis en œuvre par les organismes ou les services chargés d'une mission de service public figurant sur une liste fixée par arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l'informatique et des libertés, afin de répondre, en cas de situation d'urgence, à une alerte sanitaire, au sens de l'article L. 1413-1 du code de la santé publique, sont soumis au régime de la déclaration préalable prévu au présent article. Le responsable de traitement rend compte chaque année à la Commission nationale de l'informatique et des libertés des traitements ainsi mis en œuvre.~~**

**~~Les conditions dans lesquelles ces traitements peuvent utiliser le numéro d'inscription au répertoire national d'identification des personnes physiques sont définies par décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés.~~**

**_-_ Un __ décret en Conseil d’Etat, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés __ détermine les catégories de responsables de traitement et les finalités de ces traitements au vu desquelles ces derniers peuvent être mis en œuvre lorsqu’ils portent sur des données comportant le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques. La mise en œuvre des traitements intervient sans préjudice des obligations qui incombent aux responsables de traitement ou aux sous-traitants en vertu de la section 3 du chapitre IV du règlement (UE) 2016/679.**

**Ne sont pas soumis aux dispositions du premier alinéa ceux des traitements portant sur des données à caractère personnel parmi lesquelles figure le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques ou qui requièrent une consultation de ce répertoire :**

**1° Qui ont exclusivement des finalités de statistique publique, mis en œuvre par le service statistique public et ne comportent aucune des données mentionnées au I de l'article 8 ou à l'article 9 ;**

**2° Qui ont exclusivement des finalités de recherche scientifique ou historique ;**

**3° Qui mettent à la disposition des usagers de l'administration un ou plusieurs téléservices de l'administration électronique définis à l'article 1 er de l'ordonnance n° 2005-1516 du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives, mis en œuvre par l'Etat ou une personne morale de droit public ou une personne morale de droit privé gérant un service public.**

**Pour les traitements dont les finalités sont mentionnées aux 1° et 2°, le numéro d'inscription au répertoire national d'identification des personnes physiques fait l'objet préalablement d'une opération cryptographique lui substituant un code statistique non signifiant. Cette opération est renouvelée à une fréquence définie par décret en Conseil d'Etat pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés. Les traitements ayant comme finalité exclusive de réaliser cette opération cryptographique ne sont pas soumis aux dispositions du premier alinéa.**

**Pour les traitements dont les finalités sont mentionnées au 1°, l'utilisation du code statistique non signifiant n'est autorisée qu'au sein du service statistique public.**

**Pour les traitements dont les finalités sont mentionnées au 2°, l'opération cryptographique et, le cas échéant, l'interconnexion de deux fichiers par l'utilisation du code spécifique non signifiant qui en est issu, ne peuvent être assurés par la même personne ni par le responsable de traitement.**

**A l’exception des traitements mentionnés au second alinéa de l’article 55, le présent article n’est pas applicable aux traitements de données à caractère personnel dans le domaine de la santé qui sont régis par les dispositions du chapitre IX.**

****

</td></tr>
<tr>
<td>

**Article 9 (II)**

_[Allègement des formalités préalables]_

</td>
<td>

_**Article 27**_

I.-Sont autorisés par décret en Conseil d'Etat, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés :

1° Sous réserve du I bis de l'article 22 et du 9° du I de l'article 25, les traitements de données à caractère personnel mis en œuvre pour le compte de l'Etat, d'une personne morale de droit public ou d'une personne morale de droit privé gérant un service public, qui portent sur des données parmi lesquelles figure le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques ;

2° Les traitements de données à caractère personnel mis en œuvre pour le compte de l'Etat qui portent sur des données biométriques nécessaires à l'authentification ou au contrôle de l'identité des personnes.

II.-Sont autorisés par arrêté ou, en cas de traitement opéré pour le compte d'un établissement public ou d'une personne morale de droit privé gérant un service public, par décision de l'organe délibérant chargé de leur organisation, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés :

1° Sous réserve du I bis de l'article 22 et du 9° du I de l'article 25, les traitements mis en œuvre par l'Etat ou les personnes morales mentionnées au I qui requièrent une consultation du répertoire national d'identification des personnes physiques sans inclure le numéro d'inscription à ce répertoire ;

2° Sous réserve du 9° du I de l'article 25, ceux des traitements mentionnés au I :

-qui ne comportent aucune des données mentionnées au I de l'article 8 ou à l'article 9 ;

-qui ne donnent pas lieu à une interconnexion entre des traitements ou fichiers correspondant à des intérêts publics différents ;

-et qui sont mis en œuvre par des services ayant pour mission, soit de déterminer les conditions d'ouverture ou l'étendue d'un droit des administrés, soit d'établir l'assiette, de contrôler ou de recouvrer des impositions ou taxes de toute nature, soit d'établir des statistiques ;

3° Les traitements relatifs au recensement de la population, en métropole et dans les collectivités situées outre-mer ;

4° Les traitements mis en œuvre par l'Etat ou les personnes morales mentionnées au I aux fins de mettre à la disposition des usagers de l'administration un ou plusieurs téléservices de l'administration électronique définis à l'article 1er de l'ordonnance n° 2005-1516 du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives, si ces traitements portent sur des données parmi lesquelles figurent le numéro d'inscription des personnes au répertoire national d'identification ou tout autre identifiant des personnes physiques.

III.-Les dispositions du IV de l'article 26 sont applicables aux traitements relevant du présent article.

IV.-Le 1° des I et II du présent article n'est pas applicable :

1° Aux traitements à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé, qui sont soumis au chapitre IX de la présente loi ;

2° Aux traitements mis en œuvre afin de répondre à une alerte sanitaire en cas de situation d'urgence, qui sont soumis au V de l'article 22.

</td>
<td>

L’article 27 de la même loi est ainsi modifié :

1° Au 2° du I :

_a)_ La référence : « 2° » est supprimée ;

_b)_ Après le mot : « Etat » sont insérés les mots : « , agissant dans l’exercice de ses prérogatives de puissance publique, » ;

_c)_ Après les mots : qui portent » sont insérés les mots : « sur des données génétiques ou » ;

2° Le 1° du I ainsi que les II, III et IV sont abrogés.

</td>
<td>

**_Article 27_**

I.-Sont autorisés par décret en Conseil d'Etat, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés :

**~~1° Sous réserve du I bis de l'article 22 et du 9° du I de l'article 25, les traitements de données à caractère personnel mis en œuvre pour le compte de l'Etat, d'une personne morale de droit public ou d'une personne morale de droit privé gérant un service public, qui portent sur des données parmi lesquelles figure le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques ;~~**

**~~2°~~** Les traitements de données à caractère personnel mis en œuvre pour le compte de l'Etat **, agissant dans l’exercice de ses prérogatives de puissance publique,** qui portent **sur des données génétiques ou** sur des données biométriques nécessaires à l'authentification ou au contrôle de l'identité des personnes.

**~~II.-Sont autorisés par arrêté ou, en cas de traitement opéré pour le compte d'un établissement public ou d'une personne morale de droit privé gérant un service public, par décision de l'organe délibérant chargé de leur organisation, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés :~~**

**~~1° Sous réserve du I bis de l'article 22 et du 9° du I de l'article 25, les traitements mis en œuvre par l'Etat ou les personnes morales mentionnées au I qui requièrent une consultation du répertoire national d'identification des personnes physiques sans inclure le numéro d'inscription à ce répertoire ;~~**

**~~2° Sous réserve du 9° du I de l'article 25, ceux des traitements mentionnés au I :~~**

**~~-qui ne comportent aucune des données mentionnées au I de l'article 8 ou à l'article 9 ;~~**

**~~-qui ne donnent pas lieu à une interconnexion entre des traitements ou fichiers correspondant à des intérêts publics différents ;~~**

**~~-et qui sont mis en œuvre par des services ayant pour mission, soit de déterminer les conditions d'ouverture ou l'étendue d'un droit des administrés, soit d'établir l'assiette, de contrôler ou de recouvrer des impositions ou taxes de toute nature, soit d'établir des statistiques ;~~**

**~~3° Les traitements relatifs au recensement de la population, en métropole et dans les collectivités situées outre-mer ;~~**

**~~4° Les traitements mis en œuvre par l'Etat ou les personnes morales mentionnées au I aux fins de mettre à la disposition des usagers de l'administration un ou plusieurs téléservices de l'administration électronique définis à l'article 1er de l'ordonnance n° 2005-1516 du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives, si ces traitements portent sur des données parmi lesquelles figurent le numéro d'inscription des personnes au répertoire national d'identification ou tout autre identifiant des personnes physiques.~~**

**~~III.-Les dispositions du IV de l'article 26 sont applicables aux traitements relevant du présent article.~~**

**~~IV.-Le 1° des I et II du présent article n'est pas applicable :~~**

**~~1° Aux traitements à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé, qui sont soumis au chapitre IX de la présente loi ;~~**

**~~2° Aux traitements mis en œuvre afin de répondre à une alerte sanitaire en cas de situation d'urgence, qui sont soumis au V de l'article 22.~~**

</td></tr>
<tr>
<td>

**Article 9 (III)**

_[Allègement des formalités préalables]_

</td>
<td>

_**Article 24**_

I. - Pour les catégories les plus courantes de traitements de données à caractère personnel, dont la mise en œuvre n'est pas susceptible de porter atteinte à la vie privée ou aux libertés, la Commission nationale de l'informatique et des libertés établit et publie, après avoir reçu le cas échéant les propositions formulées par les représentants des organismes publics et privés représentatifs, des normes destinées à simplifier l'obligation de déclaration.

Ces normes précisent :

1° Les finalités des traitements faisant l'objet d'une déclaration simplifiée ;

2° Les données à caractère personnel ou catégories de données à caractère personnel traitées ;

3° La ou les catégories de personnes concernées ;

4° Les destinataires ou catégories de destinataires auxquels les données à caractère personnel sont communiquées ;

5° La durée de conservation des données à caractère personnel.

Les traitements qui correspondent à l'une de ces normes font l'objet d'une déclaration simplifiée de conformité envoyée à la commission, le cas échéant par voie électronique.

II. - La commission peut définir, parmi les catégories de traitements mentionnés au I, celles qui, compte tenu de leurs finalités, de leurs destinataires ou catégories de destinataires, des données à caractère personnel traitées, de la durée de conservation de celles-ci et des catégories de personnes concernées, sont dispensées de déclaration.

Dans les mêmes conditions, la commission peut autoriser les responsables de certaines catégories de traitements à procéder à une déclaration unique selon les dispositions du II de l'article 23.

**_Article 25_**

I. - Sont mis en œuvre après autorisation de la Commission nationale de l'informatique et des libertés, à l'exclusion de ceux qui sont mentionnés aux articles 26 et 27 :

1° Les traitements, automatisés ou non, mentionnés au 7° du II, au III et au IV de l'article 8 ;

2° Les traitements automatisés portant sur des données génétiques, à l'exception de ceux d'entre eux qui sont mis en oeuvre par des médecins ou des biologistes et qui sont nécessaires aux fins de la médecine préventive, des diagnostics médicaux ou de l'administration de soins ou de traitements ;

3° Les traitements, automatisés ou non, portant sur des données relatives aux infractions, condamnations ou mesures de sûreté, sauf ceux qui sont mis en œuvre par des auxiliaires de justice pour les besoins de leurs missions de défense des personnes concernées ;

4° Les traitements automatisés susceptibles, du fait de leur nature, de leur portée ou de leurs finalités, d'exclure des personnes du bénéfice d'un droit, d'une prestation ou d'un contrat en l'absence de toute disposition législative ou réglementaire ;

5° Les traitements automatisés ayant pour objet :

\- l'interconnexion de fichiers relevant d'une ou de plusieurs personnes morales gérant un service public et dont les finalités correspondent à des intérêts publics différents ;

\- l'interconnexion de fichiers relevant d'autres personnes et dont les finalités principales sont différentes ;

6° Les traitements portant sur des données parmi lesquelles figure le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques et ceux qui requièrent une consultation de ce répertoire sans inclure le numéro d'inscription à celui-ci des personnes ;

7° Les traitements automatisés de données comportant des appréciations sur les difficultés sociales des personnes ;

8° Les traitements automatisés comportant des données biométriques nécessaires au contrôle de l'identité des personnes ;

9° Par dérogation au 1° du I et aux 1° et 2° du II de l'article 27, les traitements qui portent sur des données à caractère personnel parmi lesquelles figure le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques ou qui requièrent une consultation de ce répertoire, lorsque ces traitements ont exclusivement des finalités de recherche scientifique ou historique, à la condition que le numéro d'inscription à ce répertoire ait préalablement fait l'objet d'une opération cryptographique lui substituant un code spécifique non signifiant, propre à chaque projet de recherche, ainsi que les traitements ayant comme finalité exclusive de réaliser cette opération cryptographique. L'opération cryptographique et, le cas échéant, l'interconnexion de deux fichiers par l'utilisation du code spécifique non signifiant qui en est issu ne peuvent être assurés par la même personne ni par le responsable de traitement. L'opération cryptographique est renouvelée à une fréquence définie par décret en Conseil d'Etat pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés.

II. - Pour l'application du présent article, les traitements qui répondent à une même finalité, portent sur des catégories de données identiques et ont les mêmes destinataires ou catégories de destinataires peuvent être autorisés par une décision unique de la commission. Dans ce cas, le responsable de chaque traitement adresse à la commission un engagement de conformité de celui-ci à la description figurant dans l'autorisation.

III. - La Commission nationale de l'informatique et des libertés se prononce dans un délai de deux mois à compter de la réception de la demande. Toutefois, ce délai peut être renouvelé une fois sur décision motivée de son président. Lorsque la commission ne s'est pas prononcée dans ces délais, la demande d'autorisation est réputée rejetée.

</td>
<td>

Les articles 24 et 25 de la même loi n° 78-17 sont abrogés.

</td>
<td>

**~~Article 24~~**

**~~I. - Pour les catégories les plus courantes de traitements de données à caractère personnel, dont la mise en oeuvre n'est pas susceptible de porter atteinte à la vie privée ou aux libertés, la Commission nationale de l'informatique et des libertés établit et publie, après avoir reçu le cas échéant les propositions formulées par les représentants des organismes publics et privés représentatifs, des normes destinées à simplifier l'obligation de déclaration.~~**

**~~Ces normes précisent :~~**

**~~1° Les finalités des traitements faisant l'objet d'une déclaration simplifiée ;~~**

**~~2° Les données à caractère personnel ou catégories de données à caractère personnel traitées ;~~**

**~~3° La ou les catégories de personnes concernées ;~~**

**~~4° Les destinataires ou catégories de destinataires auxquels les données à caractère personnel sont communiquées ;~~**

**~~5° La durée de conservation des données à caractère personnel.~~**

**~~Les traitements qui correspondent à l'une de ces normes font l'objet d'une déclaration simplifiée de conformité envoyée à la commission, le cas échéant par voie électronique.~~**

**~~II. - La commission peut définir, parmi les catégories de traitements mentionnés au I, celles qui, compte tenu de leurs finalités, de leurs destinataires ou catégories de destinataires, des données à caractère personnel traitées, de la durée de conservation de celles-ci et des catégories de personnes concernées, sont dispensées de déclaration.~~**

**~~Dans les mêmes conditions, la commission peut autoriser les responsables de certaines catégories de traitements à procéder à une déclaration unique selon les dispositions du II de l'article 23.~~**

**~~Article 25~~**

**~~I. - Sont mis en oeuvre après autorisation de la Commission nationale de l'informatique et des libertés, à l'exclusion de ceux qui sont mentionnés aux articles 26 et 27 :~~**

**~~1° Les traitements, automatisés ou non, mentionnés au 7° du II, au III et au IV de l'article 8 ;~~**

**~~2° Les traitements automatisés portant sur des données génétiques, à l'exception de ceux d'entre eux qui sont mis en oeuvre par des médecins ou des biologistes et qui sont nécessaires aux fins de la médecine préventive, des diagnostics médicaux ou de l'administration de soins ou de traitements ;~~**

**~~3° Les traitements, automatisés ou non, portant sur des données relatives aux infractions, condamnations ou mesures de sûreté, sauf ceux qui sont mis en oeuvre par des auxiliaires de justice pour les besoins de leurs missions de défense des personnes concernées ;~~**

**~~4° Les traitements automatisés susceptibles, du fait de leur nature, de leur portée ou de leurs finalités, d'exclure des personnes du bénéfice d'un droit, d'une prestation ou d'un contrat en l'absence de toute disposition législative ou réglementaire ;~~**

**~~5° Les traitements automatisés ayant pour objet :~~**

**~~\- l'interconnexion de fichiers relevant d'une ou de plusieurs personnes morales gérant un service public et dont les finalités correspondent à des intérêts publics différents ;~~**

**~~\- l'interconnexion de fichiers relevant d'autres personnes et dont les finalités principales sont différentes ;~~**

**~~6° Les traitements portant sur des données parmi lesquelles figure le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques et ceux qui requièrent une consultation de ce répertoire sans inclure le numéro d'inscription à celui-ci des personnes ;~~**

**~~7° Les traitements automatisés de données comportant des appréciations sur les difficultés sociales des personnes ;~~**

**~~8° Les traitements automatisés comportant des données biométriques nécessaires au contrôle de l'identité des personnes ;~~**

**~~9° Par dérogation au 1° du I et aux 1° et 2° du II de l'article 27, les traitements qui portent sur des données à caractère personnel parmi lesquelles figure le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques ou qui requièrent une consultation de ce répertoire, lorsque ces traitements ont exclusivement des finalités de recherche scientifique ou historique, à la condition que le numéro d'inscription à ce répertoire ait préalablement fait l'objet d'une opération cryptographique lui substituant un code spécifique non signifiant, propre à chaque projet de recherche, ainsi que les traitements ayant comme finalité exclusive de réaliser cette opération cryptographique. L'opération cryptographique et, le cas échéant, l'interconnexion de deux fichiers par l'utilisation du code spécifique non signifiant qui en est issu ne peuvent être assurés par la même personne ni par le responsable de traitement. L'opération cryptographique est renouvelée à une fréquence définie par décret en Conseil d'Etat pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés.~~**

**~~II. - Pour l'application du présent article, les traitements qui répondent à une même finalité, portent sur des catégories de données identiques et ont les mêmes destinataires ou catégories de destinataires peuvent être autorisés par une décision unique de la commission. Dans ce cas, le responsable de chaque traitement adresse à la commission un engagement de conformité de celui-ci à la description figurant dans l'autorisation.~~**

**~~III. - La Commission nationale de l'informatique et des libertés se prononce dans un délai de deux mois à compter de la réception de la demande. Toutefois, ce délai peut être renouvelé une fois sur décision motivée de son président. Lorsque la commission ne s'est pas prononcée dans ces délais, la demande d'autorisation est réputée rejetée.~~**

</td></tr>
<tr>
<td>

**Chapitre III – Obligations incombant aux responsables de traitements et sous-traitants**

</td></tr>
<tr>
<td>

**Article 10**

_[Dispositions relatives aux sous-traitants]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 35_**

Les données à caractère personnel ne peuvent faire l'objet d'une opération de traitement de la part d'un sous-traitant, d'une personne agissant sous l'autorité du responsable du traitement ou de celle du sous-traitant, que sur instruction du responsable du traitement.

Toute personne traitant des données à caractère personnel pour le compte du responsable du traitement est considérée comme un sous-traitant au sens de la présente loi.

Le sous-traitant doit présenter des garanties suffisantes pour assurer la mise en oeuvre des mesures de sécurité et de confidentialité mentionnées à l'article 34. Cette exigence ne décharge pas le responsable du traitement de son obligation de veiller au respect de ces mesures.

Le contrat liant le sous-traitant au responsable du traitement comporte l'indication des obligations incombant au sous-traitant en matière de protection de la sécurité et de la confidentialité des données et prévoit que le sous-traitant ne peut agir que sur instruction du responsable du traitement.

</td>
<td>

L’article 35 de la même loi est complété par l'alinéa suivant :

« Toutefois, dans le champ d'application du règlement (UE) 2016/679, le sous-traitant respecte les conditions prévues au chapitre IV de ce règlement. »

</td>
<td>

**_Article 35_**

Les données à caractère personnel ne peuvent faire l'objet d'une opération de traitement de la part d'un sous-traitant, d'une personne agissant sous l'autorité du responsable du traitement ou de celle du sous-traitant, que sur instruction du responsable du traitement.

Toute personne traitant des données à caractère personnel pour le compte du responsable du traitement est considérée comme un sous-traitant au sens de la présente loi.

Le sous-traitant doit présenter des garanties suffisantes pour assurer la mise en oeuvre des mesures de sécurité et de confidentialité mentionnées à l'article 34. Cette exigence ne décharge pas le responsable du traitement de son obligation de veiller au respect de ces mesures.

Le contrat liant le sous-traitant au responsable du traitement comporte l'indication des obligations incombant au sous-traitant en matière de protection de la sécurité et de la confidentialité des données et prévoit que le sous-traitant ne peut agir que sur instruction du responsable du traitement.

**Toutefois, dans le champ d'application du règlement (UE) 2016/679, le sous-traitant respecte les conditions prévues au chapitre IV de ce règlement.**

</td></tr>
<tr>
<td>

**Chapitre IV – Dispositions relatives à certaines catégories particulières de traitement**

</td></tr>
<tr>
<td>

**Article 11** _mise en œuvre des traitements relatifs aux infractions]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 9_**

Les traitements de données à caractère personnel relatives aux infractions, condamnations et mesures de sûreté ne peuvent être mis en oeuvre que par :

1° Les juridictions, les autorités publiques et les personnes morales gérant un service public, agissant dans le cadre de leurs attributions légales ;

2° Les auxiliaires de justice, pour les stricts besoins de l'exercice des missions qui leur sont confiées par la loi ;

3°  _Dispositions déclarées non conformes à la Constitution par décision du Conseil constitutionnel n° 2004-499 DC du 29 juillet 2004_ ;]

4° Les personnes morales mentionnées aux _articles L. 321-1_ sont déterminées par le code du patrimoine et les autres dispositions législatives et réglementaires applicables aux archives publiques. Elles sont également assurées par le respect des normes conformes à l'état de l'art en matière d'archivage électronique. »

</td>
<td>

**_Article 36_**

Les données à caractère personnel ne peuvent être conservées au-delà de la durée prévue au 5° de l'article 6 qu'en vue d'être traitées à des fins **~~historiques, statistiques ou scientifiques~~** **archivistiques dans l'intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques** ; le choix des données ainsi conservées est opéré dans les conditions prévues à l'article _L. 212-3 du code du patrimoine_ sont déterminées par le code du patrimoine et les autres dispositions législatives et réglementaires applicables aux archives publiques. Elles sont également assurées par le respect des normes conformes à l'état de l'art en matière d'archivage électronique. **

</td></tr>
<tr>
<td>

**Article 13**

_[traitements de données à caractère personnel dans le domaine de la santé]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**Chapitre IX : Traitements de données à caractère personnel à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé.**

**_Article 53_**

Les traitements automatisés de données à caractère personnel ayant pour finalité la recherche ou les études dans le domaine de la santé ainsi que l'évaluation ou l'analyse des pratiques ou des activités de soins ou de prévention sont soumis à la présente loi, à l'exception des articles 23 et 24, du I de l'article 25 et des articles 26,32 et 38.

Toutefois, le présent chapitre n'est pas applicable :

1° Aux traitements de données à caractère personnel ayant pour fin le suivi thérapeutique ou médical individuel des patients ;

2° Aux traitements permettant d'effectuer des études à partir des données recueillies en application du 1° lorsque ces études sont réalisées par les personnels assurant ce suivi et destinées à leur usage exclusif ;

3° Aux traitements effectués à des fins de remboursement ou de contrôle par les organismes chargés de la gestion d'un régime de base d'assurance maladie ;

4° Aux traitements effectués au sein des établissements de santé par les médecins responsables de l'information médicale, dans les conditions prévues au deuxième alinéa de l'article L. 6113-7 du code de la santé publique ;

5° Aux traitements effectués par les agences régionales de santé, par l'Etat et par la personne publique désignée par lui en application du premier alinéa de l'article L. 6113-8 du même code, dans le cadre défini au même article ;

6° Aux traitements mis en œuvre par les organismes ou les services chargés d'une mission de service public figurant sur une liste fixée par arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l'informatique et des libertés, afin de répondre à une alerte sanitaire, dans les conditions prévues au V de l'article 22.

**_Article 54_**

I.-Les traitements de données à caractère personnel ayant une finalité d'intérêt public de recherche, d'étude ou d'évaluation dans le domaine de la santé sont autorisés par la Commission nationale de l'informatique et des libertés, dans le respect des principes définis par la présente loi et en fonction de l'intérêt public que la recherche, l'étude ou l'évaluation présente.

II.-La Commission nationale de l'informatique et des libertés prend sa décision après avis :

1° Du comité compétent de protection des personnes mentionné à l' article L. 1123-6 du code de la santé publique , pour les demandes d'autorisation relatives aux recherches impliquant la personne humaine mentionnées à l'article L. 1121-1 du même code ;

2° Du comité d'expertise pour les recherches, les études et les évaluations dans le domaine de la santé, pour les demandes d'autorisation relatives à des études ou à des évaluations ainsi qu'à des recherches n'impliquant pas la personne humaine, au sens du 1° du présent II.

Le comité d'expertise est composé de personnes choisies en raison de leur compétence, dans une pluralité de disciplines. Un décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, précise la composition du comité et définit ses règles de fonctionnement. Il peut prévoir l'existence de plusieurs sections au sein du comité, compétentes en fonction de la nature ou de la finalité du traitement. Le comité d'expertise est soumis à l'article L. 1451-1 du code de la santé publique.

Le comité d'expertise émet, dans un délai d'un mois à compter de sa saisine, un avis sur la méthodologie retenue, sur la nécessité du recours à des données à caractère personnel, sur la pertinence de celles-ci par rapport à la finalité du traitement et, s'il y a lieu, sur la qualité scientifique du projet. Le cas échéant, le comité recommande aux demandeurs des modifications de leur projet afin de le mettre en conformité avec les obligations prévues par la présente loi. A défaut d'avis du comité dans le délai d'un mois, l'avis est réputé favorable. En cas d'urgence, ce délai peut être ramené à quinze jours.

Dans des conditions définies par décret en Conseil d'Etat, l'Institut national des données de santé, prévu à l' article L. 1462-1 du code de la santé publique , peut être saisi par la Commission nationale de l'informatique et des libertés ou le ministre chargé de la santé sur le caractère d'intérêt public que présente la recherche, l'étude ou l'évaluation justifiant la demande de traitement ; il peut également évoquer le cas de sa propre initiative. Dans tous les cas, il rend un avis dans un délai d'un mois à compter de sa saisine.

Les dossiers présentés dans le cadre du présent chapitre, à l'exclusion des recherches mentionnées aux 1° et 2° de l'article L. 1121-1 du code de la santé publique et de celles mentionnées au 3° du même article L. 1121-1 portant sur des produits mentionnés à l'article L. 5311-1 du même code, sont déposés auprès d'un secrétariat unique, qui assure leur orientation vers les instances compétentes .

III.-Pour chaque demande, la Commission nationale de l'informatique et des libertés vérifie les garanties présentées par le demandeur pour l'application des présentes dispositions et la conformité de sa demande à ses missions ou à son objet social. Si le demandeur n'apporte pas d'éléments suffisants pour attester la nécessité de disposer de certaines informations parmi l'ensemble des données à caractère personnel dont le traitement est envisagé, la commission peut interdire la communication de ces informations par l'organisme qui les détient et n'autoriser le traitement que pour ces données réduites.

La commission statue sur la durée de conservation des données nécessaires au traitement et apprécie les dispositions prises pour assurer leur sécurité et la garantie des secrets protégés par la loi.

IV.-Pour les catégories les plus usuelles de traitements automatisés de données de santé à caractère personnel à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé, la Commission nationale de l'informatique et des libertés peut homologuer et publier des méthodologies de référence destinées à simplifier la procédure d'examen. Celles-ci sont établies en concertation avec le comité d'expertise et des organismes publics et privés représentatifs des acteurs concernés.

V.-Des jeux de données agrégées ou des échantillons, issus des traitements des données de santé à caractère personnel pour des finalités et dans des conditions reconnues conformes à la présente loi par la Commission nationale de l'informatique et des libertés, peuvent faire l'objet d'une mise à disposition, dans des conditions préalablement homologuées par la commission, sans que l'autorisation prévue au I du présent article soit requise.

VI.-La commission peut, par décision unique, délivrer à un même demandeur une autorisation pour des traitements répondant à une même finalité, portant sur des catégories de données identiques et ayant des catégories de destinataires identiques.

**_Article 55_**

Nonobstant les règles relatives au secret professionnel, les membres des professions de santé peuvent transmettre les données à caractère personnel qu'ils détiennent dans le cadre d'un traitement de données autorisé en application de l'article 53.

Lorsque ces données permettent l'identification des personnes, leur transmission doit être effectuée dans des conditions de nature à garantir leur confidentialité. La Commission nationale de l'informatique et des libertés peut adopter des recommandations ou des référentiels sur les procédés techniques à mettre en œuvre.

La présentation des résultats du traitement de données ne peut en aucun cas permettre l'identification directe ou indirecte des personnes concernées.

Les données sont reçues par le responsable désigné à cet effet par la personne physique ou morale autorisée à mettre en œuvre le traitement. Ce responsable veille à la sécurité des informations et de leur traitement, ainsi qu'au respect de la finalité de celui-ci.

Les personnes appelées à mettre en œuvre le traitement de données ainsi que celles qui ont accès aux données sur lesquelles il porte sont astreintes au secret professionnel sous les peines prévues à l'article 226-13 du code pénal.

**_Article 56_**

Toute personne a le droit de s'opposer à ce que des données à caractère personnel la concernant fassent l'objet de la levée du secret professionnel rendue nécessaire par un traitement de la nature de ceux qui sont visés à l'article 53.

Dans le cas où la recherche nécessite le recueil de prélèvements biologiques identifiants, le consentement éclairé et exprès des personnes concernées doit être obtenu préalablement à la mise en œuvre du traitement de données.

Les informations concernant les personnes décédées, y compris celles qui figurent sur les certificats des causes de décès, peuvent faire l'objet d'un traitement de données, sauf si l'intéressé a, de son vivant, exprimé son refus par écrit.

**_Article 57_**

I. - Les personnes auprès desquelles sont recueillies des données à caractère personnel ou à propos desquelles de telles données sont transmises sont, avant le début du traitement de ces données, individuellement informées :

1° De la nature des informations transmises ;

2° De la finalité du traitement de données ;

3° Des personnes physiques ou morales destinataires des données ;

4° Du droit d'accès et de rectification institué aux articles 39 et 40 ;

5° Du droit d'opposition institué aux premier et troisième alinéas de l'article 56 ou, dans le cas prévu au deuxième alinéa de cet article, de l'obligation de recueillir leur consentement.

Toutefois, ces informations peuvent ne pas être délivrées si, pour des raisons légitimes que le médecin traitant apprécie en conscience, le malade est laissé dans l'ignorance d'un diagnostic ou d'un pronostic grave.

II. - Lorsque les données à caractère personnel ont été initialement recueillies pour un autre objet que la recherche, l'étude ou l'évaluation, il peut être dérogé, sous réserve du III, à l'obligation d'information définie au I :

1° Pour les traitements nécessaires à la conservation de ces données à des fins historiques, statistiques ou scientifiques, dans les conditions prévues au livre II du code du patrimoine ;

2° Lorsque l'information individuelle se heurte à la difficulté de retrouver les personnes concernées.

Les demandes de dérogation à l'obligation d'informer les personnes de l'utilisation de données les concernant à des fins de recherche, d'étude ou d'évaluation sont justifiées dans le dossier de demande d'autorisation transmis à la Commission nationale de l'informatique et des libertés, qui statue sur ce point.

III. - Par dérogation au I, quand les recherches, les études ou les évaluations recourent à des données de santé à caractère personnel non directement identifiantes recueillies à titre obligatoire et destinées aux services ou aux établissements de l'Etat ou des collectivités territoriales ou aux organismes de sécurité sociale, l'information des personnes concernées quant à la réutilisation possible de ces données, à des fins de recherche, d'étude ou d'évaluation, et aux modalités d'exercice de leurs droits est assurée selon des modalités définies par décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés.

**_Article 58_**

Sont destinataires de l'information et exercent les droits prévus aux articles 56 et 57 les titulaires de l'exercice de l'autorité parentale, pour les mineurs, ou le représentant légal, pour les personnes faisant l'objet d'une mesure de tutelle.

Par dérogation au premier alinéa du présent article, pour les traitements de données à caractère personnel réalisés dans le cadre de recherches mentionnées aux 2° et 3° de l'article L. 1121-1 du code de la santé publique ou d'études ou d'évaluations dans le domaine de la santé, ayant une finalité d'intérêt public et incluant des personnes mineures, l'information préalable prévue au I de l'article 57 de la présente loi peut être effectuée auprès d'un seul des titulaires de l'exercice de l'autorité parentale, s'il est impossible d'informer l'autre titulaire ou s'il ne peut être consulté dans des délais compatibles avec les exigences méthodologiques propres à la réalisation de la recherche, de l'étude ou de l'évaluation au regard de ses finalités. Le présent alinéa ne fait pas obstacle à l'exercice ultérieur, par chaque titulaire de l'exercice de l'autorité parentale, des droits d'accès, de rectification et d'opposition.

Pour les mêmes traitements, le mineur âgé de quinze ans ou plus peut s'opposer à ce que les titulaires de l'exercice de l'autorité parentale aient accès aux données le concernant recueillies au cours de la recherche, de l'étude ou de l'évaluation. Le mineur reçoit alors l'information prévue aux articles 56 et 57 et exerce seul ses droits d'accès, de rectification et d'opposition.

Pour les traitements mentionnés au deuxième alinéa du présent article, le mineur âgé de quinze ans ou plus peut s'opposer à ce que les titulaires de l'exercice de l'autorité parentale soient informés du traitement de données si le fait d'y participer conduit à révéler une information sur une action de prévention, un dépistage, un diagnostic, un traitement ou une intervention pour laquelle le mineur s'est expressément opposé à la consultation des titulaires de l'autorité parentale en application des articles L. 1111-5 et L. 1111-5-1 du code de la santé publique ou si les liens de famille sont rompus et que le mineur bénéficie à titre personnel du remboursement des prestations en nature de l'assurance maladie et maternité et de la couverture complémentaire mise en place par la loi n° 99-641 du 27 juillet 1999 portant création d'une couverture maladie universelle. Il exerce alors seul ses droits d'accès, de rectification et d'opposition.

**_Article 59_**

Une information relative aux dispositions du présent chapitre doit être assurée dans tout établissement ou centre où s'exercent des activités de prévention, de diagnostic et de soins donnant lieu à la transmission de données à caractère personnel en vue d'un traitement visé à l'article 53.

**_Article 60_**

La mise en œuvre d'un traitement de données en violation des conditions prévues par le présent chapitre entraîne le retrait temporaire ou définitif, par la Commission nationale de l'informatique et des libertés, de l'autorisation délivrée en application des dispositions de l'article 54.

Il en est de même en cas de refus de se soumettre aux vérifications prévues par le f du 2° de l'article 11.

**_Article 61_**

La transmission vers un Etat n'appartenant pas à l'Union européenne de données à caractère personnel non codées faisant l'objet d'un traitement à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé n'est autorisée, dans les conditions prévues à l'article 54, que sous réserve du respect des règles énoncées au chapitre XII.

</td>
<td>

Le chapitre IX de la même loi est ainsi rédigé :

« _Chapitre IX_

_**« Traitements de données à caractère personnel dans le domaine de la santé.**_

« _Section 1_

_**« Dispositions générales**_

« _Art. 53_. - Outre les dispositions du règlement (UE) 2016/679, les traitements contenant des données concernant la santé des personnes sont soumis aux dispositions du présent chapitre, à l’exception des catégories de traitements suivantes :

« 1° Les traitements relevant des 1° à 6° du II de l’article 8 ;

« 2° Les traitements permettant d'effectuer des études à partir des données recueillies en application du 6° du II de l’article 8 lorsque ces études sont réalisées par les personnels assurant ce suivi et destinées à leur usage exclusif ;

« 3° Les traitements effectués à des fins de remboursement ou de contrôle par les organismes chargés de la gestion d'un régime de base d'assurance maladie ;

« 4° Les traitements effectués au sein des établissements de santé par les médecins responsables de l'information médicale, dans les conditions prévues au deuxième alinéa de l'article L. 6113-7 du code de la santé publique ;

« 5° Les traitements effectués par les agences régionales de santé, par l'Etat et par la personne publique désignée par lui en application du premier alinéa de l'article L. 6113-8 du même code, dans le cadre défini au même article.

« _Art. 54. -_ I. - Les traitements relevant du présent chapitre ne peuvent être mis en œuvre qu’en considération de la finalité d’intérêt public qu’ils présentent.

« II. - Des référentiels et règlements types, au sens des _a_ bis et _b_ du 2° de l’article 11, s’appliquant aux traitements relevant du présent chapitre sont établis par la Commission nationale de l’informatique et des libertés en concertation avec l’Institut national des données de santé mentionné à l’article L. 1462-1 du code de la santé publique et des organismes publics et privés représentatifs des acteurs concernés.

« Les traitements conformes à ces référentiels et règlements types peuvent être mis en œuvre à la condition que leurs responsables adressent préalablement à la Commission nationale de l’informatique une déclaration attestant de cette conformité.

« Ces référentiels, peuvent également porter sur la description et les garanties de procédure permettant la mise à disposition en vue de leur traitement de jeux de données de santé présentant un faible risque d’impact sur la vie privée.

« III. - Les traitements mentionnés au premier alinéa du I qui ne sont pas conformes à un référentiel ou à un règlement type mentionné au II ne peuvent être mis en œuvre qu’après autorisation par la Commission nationale de l’informatique et des libertés.

« L’Institut national des données de santé mentionné à l’article L. 1462-1 du code de la santé publique peut se saisir ou être saisi, dans des conditions définies par décret en Conseil d'Etat, par la Commission nationale de l'informatique et des libertés ou le ministre chargé de la santé sur le caractère d'intérêt public que présente le traitement.

« IV. - La commission peut, par décision unique, délivrer à un même demandeur une autorisation pour des traitements répondant à une même finalité, portant sur des catégories de données identiques et ayant des catégories de destinataires identiques.

« V. - La Commission nationale de l'informatique et des libertés se prononce dans un délai de deux mois à compter de la réception de la demande. Toutefois, ce délai peut être renouvelé une fois sur décision motivée de son président ou lorsque l’Institut national des données de santé est saisi en application du II du présent article.

« Lorsque la commission ne s'est pas prononcée dans ces délais, la demande d'autorisation est réputée acceptée. Cette disposition n’est toutefois pas applicable si l’autorisation fait l’objet d’un avis préalable en vertu des dispositions du présent chapitre et que l’avis ou les avis rendus ne sont pas expressément favorables.

« _Art. 55_. - Par dérogation à l’article 54, les traitements de données de santé à caractère personnel mis en œuvre par les organismes ou les services chargés d'une mission de service public figurant sur une liste fixée par arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l'informatique et des libertés, ayant pour seule finalité de répondre, en cas de situation d'urgence, à une alerte sanitaire et d’en gérer les suites, au sens de la section 1 du chapitre III du titre I er du livre IV du code de la santé publique, sont soumis aux seules dispositions de la section 3 du chapitre IV du règlement (UE) 2016/79.

« Les traitements mentionnés au premier alinéa qui utilisent le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques sont mis en œuvre dans les conditions prévues à l’article 22.

« Les dérogations régies par le premier alinéa du présent article prennent fin un an après la création du traitement s’il continue à être mis en œuvre.

« _Art. 56_. - Nonobstant les règles relatives au secret professionnel, les membres des professions de santé peuvent transmettre au responsable d'un traitement de données autorisé en application de l'article 54 les données à caractère personnel qu'ils détiennent.

« Lorsque ces données permettent l'identification des personnes, leur transmission doit être effectuée dans des conditions de nature à garantir leur confidentialité. La Commission nationale de l'informatique et des libertés peut adopter des recommandations ou des référentiels sur les procédés techniques à mettre en œuvre.

« Lorsque le résultat du traitement de données est rendu public, l'identification directe ou indirecte des personnes concernées doit être impossible.

« Les personnes appelées à mettre en œuvre le traitement de données ainsi que celles qui ont accès aux données sur lesquelles il porte sont astreintes au secret professionnel sous les peines prévues à l'article 226-13 du code pénal.

« _Art. 57._ \- Toute personne a le droit de s'opposer à ce que des données à caractère personnel la concernant fassent l'objet de la levée du secret professionnel rendue nécessaire par un traitement de la nature de ceux qui sont visés à l’article 53.

« Dans le cas où la recherche nécessite le recueil de prélèvements biologiques identifiants, le consentement éclairé et exprès des personnes concernées doit être obtenu préalablement à la mise en œuvre du traitement de données.

« Les informations concernant les personnes décédées, y compris celles qui figurent sur les certificats des causes de décès, peuvent faire l'objet d'un traitement de données, sauf si l'intéressé a, de son vivant, exprimé son refus par écrit.

« _Art. 58_. - Les personnes auprès desquelles sont recueillies des données à caractère personnel ou à propos desquelles de telles données sont transmises sont individuellement informées conformément aux dispositions du règlement (UE) 2016/679.

« Toutefois, ces informations peuvent ne pas être délivrées si la personne concernée a entendu faire usage du droit qui lui est reconnu par l’article L. 1111-2 du code de la santé d’être laissée dans l'ignorance d'un diagnostic ou d'un pronostic.

_« Art. 59_. - Sont destinataires de l'information et exercent les droits de la personne concernée par le traitement les titulaires de l'exercice de l'autorité parentale, pour les mineurs, ou la personne chargée d’une mission de représentation dans le cadre d'une tutelle, d'une habilitation familiale ou d'un mandat de protection future, pour les majeurs protégés dont l'état ne leur permet pas de prendre seul une décision personnelle éclairée.

« Par dérogation au premier alinéa du présent article, pour les traitements de données à caractère personnel réalisés dans le cadre de recherches mentionnées aux 2° et 3° de l'article L. 1121-1 du code de la santé publique ou d'études ou d'évaluations dans le domaine de la santé, ayant une finalité d'intérêt public et incluant des personnes mineures, l'information peut être effectuée auprès d'un seul des titulaires de l'exercice de l'autorité parentale, s'il est impossible d'informer l'autre titulaire ou s'il ne peut être consulté dans des délais compatibles avec les exigences méthodologiques propres à la réalisation de la recherche, de l'étude ou de l'évaluation au regard de ses finalités. Le présent alinéa ne fait pas obstacle à l'exercice ultérieur, par chaque titulaire de l'exercice de l'autorité parentale, des droits mentionnés au premier alinéa.

« Pour ces traitements, le mineur âgé de quinze ans ou plus peut s'opposer à ce que les titulaires de l'exercice de l'autorité parentale aient accès aux données le concernant recueillies au cours de la recherche, de l'étude ou de l'évaluation. Le mineur reçoit alors l'information et exerce seul ses droits.

« Pour ces mêmes traitements, le mineur âgé de quinze ans ou plus peut s'opposer à ce que les titulaires de l'exercice de l'autorité parentale soient informés du traitement de données si le fait d'y participer conduit à révéler une information sur une action de prévention, un dépistage, un diagnostic, un traitement ou une intervention pour laquelle le mineur s'est expressément opposé à la consultation des titulaires de l'autorité parentale en application des articles L. 1111-5 et L. 1111-5-1 du code de la santé publique ou si les liens de famille sont rompus et que le mineur bénéficie à titre personnel du remboursement des prestations en nature de l'assurance maladie et maternité et de la couverture complémentaire mise en place par la loi n° 99-641 du 27 juillet 1999 portant création d'une couverture maladie universelle. Il exerce alors seul ses droits.

« _Art. 60_. - Une information relative aux dispositions du présent chapitre doit notamment être assurée dans tout établissement ou centre où s'exercent des activités de prévention, de diagnostic et de soins donnant lieu à la transmission de données à caractère personnel en vue d'un traitement visé au présent chapitre.

« _Section 2_

_**« Dispositions particulières aux traitements à des fins de recherche, d’étude**_

**_« ou d’évaluation dans le domaine de la santé._**

« _Art. 61_. - Les traitements automatisés de données à caractère personnel dont la finalité est ou devient la recherche ou les études dans le domaine de la santé ainsi que l'évaluation ou l'analyse des pratiques ou des activités de soins ou de prévention sont soumis aux dispositions de la section 1 du présent chapitre, sous réserve de celles de la présente section.

« _Art. 62_. - Des méthodologies de référence sont homologuées et publiées, par la Commission nationale de l’informatique et des libertés. Elles sont établies en concertation avec l’Institut national des données de santé mentionné à l’article L. 1462-1 du code de la santé publique et des organismes publics et privés représentatifs des acteurs concernés.

« Lorsque le traitement est conforme à une méthodologie de référence, il peut être mis en œuvre, sans autorisation mentionnée à l’article 54, à la condition que son responsable adresse préalablement à la Commission nationale de l’informatique une déclaration attestant de cette conformité.

« _Art. 63_. - L’autorisation du traitement est accordée par la Commission nationale de l'informatique et des libertés dans les conditions définies à l’article 54 et après avis :

« 1° Du comité compétent de protection des personnes mentionné à l'article L. 1123-6 du code de la santé publique, pour les demandes d'autorisation relatives aux recherches impliquant la personne humaine mentionnées à l'article L. 1121-1 du même code ;

« 2° Du comité d'expertise pour les recherches, les études et les évaluations dans le domaine de la santé, pour les demandes d'autorisation relatives à des études ou à des évaluations ainsi qu'à des recherches n'impliquant pas la personne humaine, au sens du 1° du présent article. Un décret en Conseil d’Etat, pris après avis de la Commission nationale de l'informatique et des libertés, fixe la composition de ce comité et définit ses règles de fonctionnement. Le comité d’expertise est soumis à l’article L. 1451-1 du code de la santé publique.

« Les dossiers présentés dans le cadre de la présente section, à l'exclusion des recherches impliquant la personne humaine, sont déposés auprès d'un secrétariat unique assuré par l’Institut national des données de santé, qui assure leur orientation vers les instances compétentes. »

</td>
<td>

**~~Chapitre IX : Traitements de données à caractère personnel à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé.~~**

**~~Article 53~~**

**~~Les traitements automatisés de données à caractère personnel ayant pour finalité la recherche ou les études dans le domaine de la santé ainsi que l'évaluation ou l'analyse des pratiques ou des activités de soins ou de prévention sont soumis à la présente loi, à l'exception des articles 23 et 24, du I de l'article 25 et des articles 26,32 et 38.~~**

**~~Toutefois, le présent chapitre n'est pas applicable :~~**

**~~1° Aux traitements de données à caractère personnel ayant pour fin le suivi thérapeutique ou médical individuel des patients ;~~**

**~~2° Aux traitements permettant d'effectuer des études à partir des données recueillies en application du 1° lorsque ces études sont réalisées par les personnels assurant ce suivi et destinées à leur usage exclusif ;~~**

**~~3° Aux traitements effectués à des fins de remboursement ou de contrôle par les organismes chargés de la gestion d'un régime de base d'assurance maladie ;~~**

**~~4° Aux traitements effectués au sein des établissements de santé par les médecins responsables de l'information médicale, dans les conditions prévues au deuxième alinéa de l'article L. 6113-7 du code de la santé publique ;~~**

**~~5° Aux traitements effectués par les agences régionales de santé, par l'Etat et par la personne publique désignée par lui en application du premier alinéa de l'article L. 6113-8 du même code, dans le cadre défini au même article ;~~**

**~~6° Aux traitements mis en œuvre par les organismes ou les services chargés d'une mission de service public figurant sur une liste fixée par arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l'informatique et des libertés, afin de répondre à une alerte sanitaire, dans les conditions prévues au V de l'article 22.~~**

**~~Article 54~~**

**~~Modifié par LOI n°2016-41 du 26 janvier 2016 - art. 193~~**

**~~I.-Les traitements de données à caractère personnel ayant une finalité d'intérêt public de recherche, d'étude ou d'évaluation dans le domaine de la santé sont autorisés par la Commission nationale de l'informatique et des libertés, dans le respect des principes définis par la présente loi et en fonction de l'intérêt public que la recherche, l'étude ou l'évaluation présente.

II.-La Commission nationale de l'informatique et des libertés prend sa décision après avis :

1° Du comité compétent de protection des personnes mentionné à l' article L. 1123-6 du code de la santé publique , pour les demandes d'autorisation relatives aux recherches impliquant la personne humaine mentionnées à l'article L. 1121-1 du même code ;

2° Du comité d'expertise pour les recherches, les études et les évaluations dans le domaine de la santé, pour les demandes d'autorisation relatives à des études ou à des évaluations ainsi qu'à des recherches n'impliquant pas la personne humaine, au sens du 1° du présent II.

Le comité d'expertise est composé de personnes choisies en raison de leur compétence, dans une pluralité de disciplines. Un décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, précise la composition du comité et définit ses règles de fonctionnement. Il peut prévoir l'existence de plusieurs sections au sein du comité, compétentes en fonction de la nature ou de la finalité du traitement. Le comité d'expertise est soumis à l' article L. 1451-1 du code de la santé publique .

Le comité d'expertise émet, dans un délai d'un mois à compter de sa saisine, un avis sur la méthodologie retenue, sur la nécessité du recours à des données à caractère personnel, sur la pertinence de celles-ci par rapport à la finalité du traitement et, s'il y a lieu, sur la qualité scientifique du projet. Le cas échéant, le comité recommande aux demandeurs des modifications de leur projet afin de le mettre en conformité avec les obligations prévues par la présente loi. A défaut d'avis du comité dans le délai d'un mois, l'avis est réputé favorable. En cas d'urgence, ce délai peut être ramené à quinze jours.

Dans des conditions définies par décret en Conseil d'Etat, l'Institut national des données de santé, prévu à l' article L. 1462-1 du code de la santé publique , peut être saisi par la Commission nationale de l'informatique et des libertés ou le ministre chargé de la santé sur le caractère d'intérêt public que présente la recherche, l'étude ou l'évaluation justifiant la demande de traitement ; il peut également évoquer le cas de sa propre initiative. Dans tous les cas, il rend un avis dans un délai d'un mois à compter de sa saisine.

Les dossiers présentés dans le cadre du présent chapitre, à l'exclusion des recherches mentionnées aux 1° et 2° de l'article L. 1121-1 du code de la santé publique et de celles mentionnées au 3° du même article L. 1121-1 portant sur des produits mentionnés à l'article L. 5311-1 du même code, sont déposés auprès d'un secrétariat unique, qui assure leur orientation vers les instances compétentes .

III.-Pour chaque demande, la Commission nationale de l'informatique et des libertés vérifie les garanties présentées par le demandeur pour l'application des présentes dispositions et la conformité de sa demande à ses missions ou à son objet social. Si le demandeur n'apporte pas d'éléments suffisants pour attester la nécessité de disposer de certaines informations parmi l'ensemble des données à caractère personnel dont le traitement est envisagé, la commission peut interdire la communication de ces informations par l'organisme qui les détient et n'autoriser le traitement que pour ces données réduites.

La commission statue sur la durée de conservation des données nécessaires au traitement et apprécie les dispositions prises pour assurer leur sécurité et la garantie des secrets protégés par la loi.

IV.-Pour les catégories les plus usuelles de traitements automatisés de données de santé à caractère personnel à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé, la Commission nationale de l'informatique et des libertés peut homologuer et publier des méthodologies de référence destinées à simplifier la procédure d'examen. Celles-ci sont établies en concertation avec le comité d'expertise et des organismes publics et privés représentatifs des acteurs concernés.

V.-Des jeux de données agrégées ou des échantillons, issus des traitements des données de santé à caractère personnel pour des finalités et dans des conditions reconnues conformes à la présente loi par la Commission nationale de l'informatique et des libertés, peuvent faire l'objet d'une mise à disposition, dans des conditions préalablement homologuées par la commission, sans que l'autorisation prévue au I du présent article soit requise.

VI.-La commission peut, par décision unique, délivrer à un même demandeur une autorisation pour des traitements répondant à une même finalité, portant sur des catégories de données identiques et ayant des catégories de destinataires identiques.~~**

**~~Article 55~~**

**~~Modifié par LOI n°2016-41 du 26 janvier 2016 - art. 193~~**

**~~Nonobstant les règles relatives au secret professionnel, les membres des professions de santé peuvent transmettre les données à caractère personnel qu'ils détiennent dans le cadre d'un traitement de données autorisé en application de l'article 53.~~**

**~~Lorsque ces données permettent l'identification des personnes, leur transmission doit être effectuée dans des conditions de nature à garantir leur confidentialité. La Commission nationale de l'informatique et des libertés peut adopter des recommandations ou des référentiels sur les procédés techniques à mettre en œuvre.~~**

**~~La présentation des résultats du traitement de données ne peut en aucun cas permettre l'identification directe ou indirecte des personnes concernées.~~**

**~~Les données sont reçues par le responsable désigné à cet effet par la personne physique ou morale autorisée à mettre en œuvre le traitement. Ce responsable veille à la sécurité des informations et de leur traitement, ainsi qu'au respect de la finalité de celui-ci.~~**

**~~Les personnes appelées à mettre en œuvre le traitement de données ainsi que celles qui ont accès aux données sur lesquelles il porte sont astreintes au secret professionnel sous les peines prévues à l'article 226-13 du code pénal.~~**

**~~Article 56~~**

**~~Créé par Loi n°2004-801 du 6 août 2004 - art. 9 JORF 7 août 2004~~**

**~~Toute personne a le droit de s'opposer à ce que des données à caractère personnel la concernant fassent l'objet de la levée du secret professionnel rendue nécessaire par un traitement de la nature de ceux qui sont visés à l'article 53.~~**

**~~Dans le cas où la recherche nécessite le recueil de prélèvements biologiques identifiants, le consentement éclairé et exprès des personnes concernées doit être obtenu préalablement à la mise en oeuvre du traitement de données.~~**

**~~Les informations concernant les personnes décédées, y compris celles qui figurent sur les certificats des causes de décès, peuvent faire l'objet d'un traitement de données, sauf si l'intéressé a, de son vivant, exprimé son refus par écrit.~~**

**~~Article 57~~**

**~~Modifié par LOI n°2016-41 du 26 janvier 2016 - art. 193~~**

**~~I. - Les personnes auprès desquelles sont recueillies des données à caractère personnel ou à propos desquelles de telles données sont transmises sont, avant le début du traitement de ces données, individuellement informées :~~**

**~~1° De la nature des informations transmises ;~~**

**~~2° De la finalité du traitement de données ;~~**

**~~3° Des personnes physiques ou morales destinataires des données ;~~**

**~~4° Du droit d'accès et de rectification institué aux articles 39 et 40 ;~~**

**~~5° Du droit d'opposition institué aux premier et troisième alinéas de l'article 56 ou, dans le cas prévu au deuxième alinéa de cet article, de l'obligation de recueillir leur consentement.~~**

**~~Toutefois, ces informations peuvent ne pas être délivrées si, pour des raisons légitimes que le médecin traitant apprécie en conscience, le malade est laissé dans l'ignorance d'un diagnostic ou d'un pronostic grave.~~**

**~~II. - Lorsque les données à caractère personnel ont été initialement recueillies pour un autre objet que la recherche, l'étude ou l'évaluation, il peut être dérogé, sous réserve du III, à l'obligation d'information définie au I :~~**

**~~1° Pour les traitements nécessaires à la conservation de ces données à des fins historiques, statistiques ou scientifiques, dans les conditions prévues au livre II du code du patrimoine ;~~**

**~~2° Lorsque l'information individuelle se heurte à la difficulté de retrouver les personnes concernées.~~**

**~~Les demandes de dérogation à l'obligation d'informer les personnes de l'utilisation de données les concernant à des fins de recherche, d'étude ou d'évaluation sont justifiées dans le dossier de demande d'autorisation transmis à la Commission nationale de l'informatique et des libertés, qui statue sur ce point.~~**

**~~III. - Par dérogation au I, quand les recherches, les études ou les évaluations recourent à des données de santé à caractère personnel non directement identifiantes recueillies à titre obligatoire et destinées aux services ou aux établissements de l'Etat ou des collectivités territoriales ou aux organismes de sécurité sociale, l'information des personnes concernées quant à la réutilisation possible de ces données, à des fins de recherche, d'étude ou d'évaluation, et aux modalités d'exercice de leurs droits est assurée selon des modalités définies par décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés.~~**

**~~Article 58~~**

**~~Modifié par LOI n°2016-1321 du 7 octobre 2016 - art. 56~~**

**~~Sont destinataires de l'information et exercent les droits prévus aux articles 56 et 57 les titulaires de l'exercice de l'autorité parentale, pour les mineurs, ou le représentant légal, pour les personnes faisant l'objet d'une mesure de tutelle.~~**

**~~Par dérogation au premier alinéa du présent article, pour les traitements de données à caractère personnel réalisés dans le cadre de recherches mentionnées aux 2° et 3° de l'article L. 1121-1 du code de la santé publique ou d'études ou d'évaluations dans le domaine de la santé, ayant une finalité d'intérêt public et incluant des personnes mineures, l'information préalable prévue au I de l'article 57 de la présente loi peut être effectuée auprès d'un seul des titulaires de l'exercice de l'autorité parentale, s'il est impossible d'informer l'autre titulaire ou s'il ne peut être consulté dans des délais compatibles avec les exigences méthodologiques propres à la réalisation de la recherche, de l'étude ou de l'évaluation au regard de ses finalités. Le présent alinéa ne fait pas obstacle à l'exercice ultérieur, par chaque titulaire de l'exercice de l'autorité parentale, des droits d'accès, de rectification et d'opposition.~~**

**~~Pour les mêmes traitements, le mineur âgé de quinze ans ou plus peut s'opposer à ce que les titulaires de l'exercice de l'autorité parentale aient accès aux données le concernant recueillies au cours de la recherche, de l'étude ou de l'évaluation. Le mineur reçoit alors l'information prévue aux articles 56 et 57 et exerce seul ses droits d'accès, de rectification et d'opposition.~~**

**~~Pour les traitements mentionnés au deuxième alinéa du présent article, le mineur âgé de quinze ans ou plus peut s'opposer à ce que les titulaires de l'exercice de l'autorité parentale soient informés du traitement de données si le fait d'y participer conduit à révéler une information sur une action de prévention, un dépistage, un diagnostic, un traitement ou une intervention pour laquelle le mineur s'est expressément opposé à la consultation des titulaires de l'autorité parentale en application des articles L. 1111-5 et L. 1111-5-1 du code de la santé publique ou si les liens de famille sont rompus et que le mineur bénéficie à titre personnel du remboursement des prestations en nature de l'assurance maladie et maternité et de la couverture complémentaire mise en place par la loi n° 99-641 du 27 juillet 1999 portant création d'une couverture maladie universelle. Il exerce alors seul ses droits d'accès, de rectification et d'opposition.~~**

**~~Article 59~~**

**~~Créé par Loi n°2004-801 du 6 août 2004 - art. 9 JORF 7 août 2004~~**

**~~Une information relative aux dispositions du présent chapitre doit être assurée dans tout établissement ou centre où s'exercent des activités de prévention, de diagnostic et de soins donnant lieu à la transmission de données à caractère personnel en vue d'un traitement visé à l'article 53.~~**

**~~Article 60~~**

**~~Créé par Loi n°2004-801 du 6 août 2004 - art. 9 JORF 7 août 2004~~**

**~~La mise en oeuvre d'un traitement de données en violation des conditions prévues par le présent chapitre entraîne le retrait temporaire ou définitif, par la Commission nationale de l'informatique et des libertés, de l'autorisation délivrée en application des dispositions de l'article 54.~~**

**~~Il en est de même en cas de refus de se soumettre aux vérifications prévues par le f du 2° de l'article 11.~~**

**~~Article 61~~**

**~~Modifié par LOI n°2016-41 du 26 janvier 2016 - art. 193~~**

**~~La transmission vers un Etat n'appartenant pas à l'Union européenne de données à caractère personnel non codées faisant l'objet d'un traitement à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé n'est autorisée, dans les conditions prévues à l'article 54, que sous réserve du respect des règles énoncées au chapitre XII.~~**

**_Chapitre IX : Traitements de données à caractère personnel dans le domaine de la santé._**

**_Section 1_**

**_Dispositions générales_**

**_Art. 53_. - Outre les dispositions du règlement (UE) 2016/679, les traitements contenant des données concernant la santé des personnes sont soumis aux dispositions du présent chapitre, à l’exception des catégories de traitements suivantes :**

**1° Les traitements relevant des 1° à 6° du II de l’article 8 ;**

**2° Les traitements permettant d'effectuer des études à partir des données recueillies en application du 6° du II de l’article 8 lorsque ces études sont réalisées par les personnels assurant ce suivi et destinées à leur usage exclusif ;**

**3° Les traitements effectués à des fins de remboursement ou de contrôle par les organismes chargés de la gestion d'un régime de base d'assurance maladie ;**

**4° Les traitements effectués au sein des établissements de santé par les médecins responsables de l'information médicale, dans les conditions prévues au deuxième alinéa de l'article L. 6113-7 du code de la santé publique ;**

**5° Les traitements effectués par les agences régionales de santé, par l'Etat et par la personne publique désignée par lui en application du premier alinéa de l'article L. 6113-8 du même code, dans le cadre défini au même article.**

**_Art. 54. -_ I. - Les traitements relevant du présent chapitre ne peuvent être mis en œuvre qu’en considération de la finalité d’intérêt public qu’ils présentent.**

**II. - Des référentiels et règlements types, au sens des _a_ bis et _b_ du 2° de l’article 11, s’appliquant aux traitements relevant du présent chapitre sont établis par la Commission nationale de l’informatique et des libertés en concertation avec l’Institut national des données de santé mentionné à l’article L. 1462-1 du code de la santé publique et des organismes publics et privés représentatifs des acteurs concernés.**

**Les traitements conformes à ces référentiels et règlements types peuvent être mis en œuvre à la condition que leurs responsables adressent préalablement à la Commission nationale de l’informatique une déclaration attestant de cette conformité.**

**Ces référentiels, peuvent également porter sur la description et les garanties de procédure permettant la mise à disposition en vue de leur traitement de jeux de données de santé présentant un faible risque d’impact sur la vie privée.**

**III. - Les traitements mentionnés au premier alinéa du I qui ne sont pas conformes à un référentiel ou à un règlement type mentionné au II ne peuvent être mis en œuvre qu’après autorisation par la Commission nationale de l’informatique et des libertés.**

**L’Institut national des données de santé mentionné à l’article L. 1462-1 du code de la santé publique peut se saisir ou être saisi, dans des conditions définies par décret en Conseil d'Etat, par la Commission nationale de l'informatique et des libertés ou le ministre chargé de la santé sur le caractère d'intérêt public que présente le traitement.**

**IV. - La commission peut, par décision unique, délivrer à un même demandeur une autorisation pour des traitements répondant à une même finalité, portant sur des catégories de données identiques et ayant des catégories de destinataires identiques.**

**V. - La Commission nationale de l'informatique et des libertés se prononce dans un délai de deux mois à compter de la réception de la demande. Toutefois, ce délai peut être renouvelé une fois sur décision motivée de son président ou lorsque l’Institut national des données de santé est saisi en application du II du présent article.**

**Lorsque la commission ne s'est pas prononcée dans ces délais, la demande d'autorisation est réputée acceptée. Cette disposition n’est toutefois pas applicable si l’autorisation fait l’objet d’un avis préalable en vertu des dispositions du présent chapitre et que l’avis ou les avis rendus ne sont pas expressément favorables.**

**_Art. 55_. - Par dérogation à l’article 54, les traitements de données de santé à caractère personnel mis en œuvre par les organismes ou les services chargés d'une mission de service public figurant sur une liste fixée par arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l'informatique et des libertés, ayant pour seule finalité de répondre, en cas de situation d'urgence, à une alerte sanitaire et d’en gérer les suites, au sens de la section 1 du chapitre III du titre I er du livre IV du code de la santé publique, sont soumis aux seules dispositions de la section 3 du chapitre IV du règlement (UE) 2016/79. **

**Les traitements mentionnés au premier alinéa qui utilisent le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques sont mis en œuvre dans les conditions prévues à l’article 22.**

**Les dérogations régies par le premier alinéa du présent article prennent fin un an après la création du traitement s’il continue à être mis en œuvre.**

**_Art. 56_. - Nonobstant les règles relatives au secret professionnel, les membres des professions de santé peuvent transmettre au responsable d'un traitement de données autorisé en application de l'article 54 les données à caractère personnel qu'ils détiennent. **

**Lorsque ces données permettent l'identification des personnes, leur transmission doit être effectuée dans des conditions de nature à garantir leur confidentialité. La Commission nationale de l'informatique et des libertés peut adopter des recommandations ou des référentiels sur les procédés techniques à mettre en œuvre.**

**Lorsque le résultat du traitement de données est rendu public, l'identification directe ou indirecte des personnes concernées doit être impossible.**

**Les personnes appelées à mettre en œuvre le traitement de données ainsi que celles qui ont accès aux données sur lesquelles il porte sont astreintes au secret professionnel sous les peines prévues à l'article 226-13 du code pénal.**

**_Art. 57._ \- Toute personne a le droit de s'opposer à ce que des données à caractère personnel la concernant fassent l'objet de la levée du secret professionnel rendue nécessaire par un traitement de la nature de ceux qui sont visés à l’article 53.**

**Dans le cas où la recherche nécessite le recueil de prélèvements biologiques identifiants, le consentement éclairé et exprès des personnes concernées doit être obtenu préalablement à la mise en œuvre du traitement de données.**

**Les informations concernant les personnes décédées, y compris celles qui figurent sur les certificats des causes de décès, peuvent faire l'objet d'un traitement de données, sauf si l'intéressé a, de son vivant, exprimé son refus par écrit.**

**_Art. 58_. - Les personnes auprès desquelles sont recueillies des données à caractère personnel ou à propos desquelles de telles données sont transmises sont individuellement informées conformément aux dispositions du règlement (UE) 2016/679.**

**Toutefois, ces informations peuvent ne pas être délivrées si la personne concernée a entendu faire usage du droit qui lui est reconnu par l’article L. 1111-2 du code de la santé d’être laissée dans l'ignorance d'un diagnostic ou d'un pronostic.**

**_Art. 59_. - Sont destinataires de l'information et exercent les droits de la personne concernée par le traitement les titulaires de l'exercice de l'autorité parentale, pour les mineurs, ou la personne chargée d’une mission de représentation dans le cadre d'une tutelle, d'une habilitation familiale ou d'un mandat de protection future, pour les majeurs protégés dont l'état ne leur permet pas de prendre seul une décision personnelle éclairée.**

**Par dérogation au premier alinéa du présent article, pour les traitements de données à caractère personnel réalisés dans le cadre de recherches mentionnées aux 2° et 3° de l'article L. 1121-1 du code de la santé publique ou d'études ou d'évaluations dans le domaine de la santé, ayant une finalité d'intérêt public et incluant des personnes mineures, l'information peut être effectuée auprès d'un seul des titulaires de l'exercice de l'autorité parentale, s'il est impossible d'informer l'autre titulaire ou s'il ne peut être consulté dans des délais compatibles avec les exigences méthodologiques propres à la réalisation de la recherche, de l'étude ou de l'évaluation au regard de ses finalités. Le présent alinéa ne fait pas obstacle à l'exercice ultérieur, par chaque titulaire de l'exercice de l'autorité parentale, des droits mentionnés au premier alinéa.**

**Pour ces traitements, le mineur âgé de quinze ans ou plus peut s'opposer à ce que les titulaires de l'exercice de l'autorité parentale aient accès aux données le concernant recueillies au cours de la recherche, de l'étude ou de l'évaluation. Le mineur reçoit alors l'information et exerce seul ses droits.**

**Pour ces mêmes traitements, le mineur âgé de quinze ans ou plus peut s'opposer à ce que les titulaires de l'exercice de l'autorité parentale soient informés du traitement de données si le fait d'y participer conduit à révéler une information sur une action de prévention, un dépistage, un diagnostic, un traitement ou une intervention pour laquelle le mineur s'est expressément opposé à la consultation des titulaires de l'autorité parentale en application des articles L. 1111-5 et L. 1111-5-1 du code de la santé publique ou si les liens de famille sont rompus et que le mineur bénéficie à titre personnel du remboursement des prestations en nature de l'assurance maladie et maternité et de la couverture complémentaire mise en place par la loi n° 99-641 du 27 juillet 1999 portant création d'une couverture maladie universelle. Il exerce alors seul ses droits.**

**_Art. 60_. - Une information relative aux dispositions du présent chapitre doit notamment être assurée dans tout établissement ou centre où s'exercent des activités de prévention, de diagnostic et de soins donnant lieu à la transmission de données à caractère personnel en vue d'un traitement visé au présent chapitre.**

**_Section 2_**

**_Dispositions particulières aux traitements à des fins de recherche, d’étude ou d’évaluation dans le domaine de la santé._**

**_Art. 61_. - Les traitements automatisés de données à caractère personnel dont la finalité est ou devient la recherche ou les études dans le domaine de la santé ainsi que l'évaluation ou l'analyse des pratiques ou des activités de soins ou de prévention sont soumis aux dispositions de la section 1 du présent chapitre, sous réserve de celles de la présente section. **

**_Art. 62_. - Des méthodologies de référence sont homologuées et publiées, par la Commission nationale de l’informatique et des libertés. Elles sont établies en concertation avec l’Institut national des données de santé mentionné à l’article L. 1462-1 du code de la santé publique et des organismes publics et privés représentatifs des acteurs concernés.**

**Lorsque le traitement est conforme à une méthodologie de référence, il peut être mis en œuvre, sans autorisation mentionnée à l’article 54, à la condition que son responsable adresse préalablement à la Commission nationale de l’informatique une déclaration attestant de cette conformité.**

**_Art. 63_. - L’autorisation du traitement est accordée par la Commission nationale de l'informatique et des libertés dans les conditions définies à l’article 54 et après avis :**

**1° Du comité compétent de protection des personnes mentionné à l'article L. 1123-6 du code de la santé publique, pour les demandes d'autorisation relatives aux recherches impliquant la personne humaine mentionnées à l'article L. 1121-1 du même code ;**

**2° Du comité d'expertise pour les recherches, les études et les évaluations dans le domaine de la santé, pour les demandes d'autorisation relatives à des études ou à des évaluations ainsi qu'à des recherches n'impliquant pas la personne humaine, au sens du 1° du présent article. Un décret en Conseil d’Etat, pris après avis de la Commission nationale de l'informatique et des libertés, fixe la composition de ce comité et définit ses règles de fonctionnement. Le comité d’expertise est soumis à l’article L. 1451-1 du code de la santé publique.**

**Les dossiers présentés dans le cadre de la présente section, à l'exclusion des recherches impliquant la personne humaine, sont déposés auprès d'un secrétariat unique assuré par l’Institut national des données de santé, qui assure leur orientation vers les instances compétentes.**

</td></tr>
<tr>
<td>

**Chapitre IV – Dispositions particulières relatives aux droits des personnes concernées**

</td></tr>
<tr>
<td>

**Article 14**

_[Décision individuelle automatisée]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 10_**

Aucune décision de justice impliquant une appréciation sur le comportement d'une personne ne peut avoir pour fondement un traitement automatisé de données à caractère personnel destiné à évaluer certains aspects de sa personnalité.

Aucune autre décision produisant des effets juridiques à l'égard d'une personne ne peut être prise sur le seul fondement d'un traitement automatisé de données destiné à définir le profil de l'intéressé ou à évaluer certains aspects de sa personnalité.

Ne sont pas regardées comme prises sur le seul fondement d'un traitement automatisé les décisions prises dans le cadre de la conclusion ou de l'exécution d'un contrat et pour lesquelles la personne concernée a été mise à même de présenter ses observations, ni celles satisfaisant les demandes de la personne concernée.

</td>
<td>

L’article 10 de la même loi est ainsi modifié :

1° Au deuxième alinéa :

_a)_ Les mots : « Outre les cas mentionnés aux _a_ et _c_ sous le 2 de l’article 22 du règlement 2016/679 » sont introduits au début de la première phrase ;

_b)_ Les mots : « définir le profil de l’intéressé » sont remplacés par le mot : « prévoir » ;

_c)_ Les mots : « de sa personnalité » sont remplacés par les mots : « personnels relatifs à la personne concernée, à l’exception des décisions administratives individuelles prises dans le respect de l’article L. 311-3-1 et du chapitre I er du titre Ier du livre IV du code des relations du public et de l’administration, à condition que le traitement ne porte pas sur des données mentionnées au I de l’article 8, » ;

2° Le troisième alinéa est remplacé par les dispositions suivantes :

« Pour les décisions administratives mentionnées à l’alinéa précédent, le responsable du traitement s’assure de la maîtrise du traitement algorithmique et de ses évolutions ».

</td>
<td>

**_Article 10_**

Aucune décision de justice impliquant une appréciation sur le comportement d'une personne ne peut avoir pour fondement un traitement automatisé de données à caractère personnel destiné à évaluer certains aspects de sa personnalité.

**Outre les cas mentionnés aux _a_ et _c_ sous le 2 de l’article 22 du règlement 2016/679** aucune autre décision produisant des effets juridiques à l'égard d'une personne ne peut être prise sur le seul fondement d'un traitement automatisé de données destiné à **~~définir le profil de l'intéressé~~** **prévoir** ou à évaluer certains aspects **~~de sa personnalité.~~ personnels relatifs à la personne concernée, à l’exception des décisions administratives individuelles prises dans le respect de l’article L. 311-3-1 et du chapitre Ier du titre Ier du livre IV du code des relations du public et de l’administration, à condition que le traitement ne porte pas sur des données mentionnées au I de l’article 8.**

**~~Ne sont pas regardées comme prises sur le seul fondement d'un traitement automatisé les décisions prises dans le cadre de la conclusion ou de l'exécution d'un contrat et pour lesquelles la personne concernée a été mise à même de présenter ses observations, ni celles satisfaisant les demandes de la personne concernée.~~**

**Pour les décisions administratives mentionnées à l’alinéa précédent, le responsable du traitement s’assure de la maîtrise du traitement algorithmique et de ses évolutions**

</td></tr>
<tr>
<td>

**Article 15**

_[Age de consentement des mineurs : marge de 13 à 16 ans]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 40 _**

[…]

II. — Sur demande de la personne concernée, le responsable du traitement est tenu d'effacer dans les meilleurs délais les données à caractère personnel qui ont été collectées dans le cadre de l'offre de services de la société de l'information lorsque la personne concernée était mineure au moment de la collecte. Lorsqu'il a transmis les données en cause à un tiers lui-même responsable de traitement, il prend des mesures raisonnables, y compris d'ordre technique, compte tenu des technologies disponibles et des coûts de mise en œuvre, pour informer le tiers qui traite ces données que la personne concernée a demandé l'effacement de tout lien vers celles-ci, ou de toute copie ou de toute reproduction de celles-ci.

En cas de non-exécution de l'effacement des données à caractère personnel ou en cas d'absence de réponse du responsable du traitement dans un délai d'un mois à compter de la demande, la personne concernée peut saisir la Commission nationale de l'informatique et des libertés, qui se prononce sur cette demande dans un délai de trois semaines à compter de la date de réception de la réclamation.

Les deux premiers alinéas du présent II ne s'appliquent pas lorsque le traitement de données à caractère personnel est nécessaire :

1° Pour exercer le droit à la liberté d'expression et d'information ;

2° Pour respecter une obligation légale qui requiert le traitement de ces données ou pour exercer une mission d'intérêt public ou relevant de l'exercice de l'autorité publique dont est investi le responsable du traitement ;

3° Pour des motifs d'intérêt public dans le domaine de la santé publique ;

4° A des fins archivistiques dans l'intérêt public, à des fins de recherche scientifique ou historique ou à des fins statistiques, dans la mesure où le droit mentionné au présent II est susceptible de rendre impossible ou de compromettre gravement la réalisation des objectifs du traitement ;

5° A la constatation, à l'exercice ou à la défense de droits en justice.

</td>
<td>

Après le II de l’article 40 de la même loi sont insérées les dispositions suivantes :

« _III_. - Un décret en Conseil d’Etat, pris après avis de la Commission nationale de l’informatique et des libertés, fixe la liste des traitements et des catégories de traitements autorisés à déroger au droit à la communication d’une violation de données régi par l’article 34 du, règlement (UE) 2016/679 lorsque la notification d’une divulgation ou d’un accès non autorisé à ces données est susceptible de représenter un risque pour la sécurité nationale, la défense nationale ou la sécurité publique. La dérogation prévue au présent alinéa n’est applicable qu’aux seuls traitements de données à caractère personnel nécessaires au respect d’une obligation légale qui requiert le traitement de ces données ou nécessaires à l’exercice d’une mission d’intérêt public dont est investi le responsable de traitement. »

</td>
<td>

**_Article 40 _**

[…]

II. — Sur demande de la personne concernée, le responsable du traitement est tenu d'effacer dans les meilleurs délais les données à caractère personnel qui ont été collectées dans le cadre de l'offre de services de la société de l'information lorsque la personne concernée était mineure au moment de la collecte. Lorsqu'il a transmis les données en cause à un tiers lui-même responsable de traitement, il prend des mesures raisonnables, y compris d'ordre technique, compte tenu des technologies disponibles et des coûts de mise en œuvre, pour informer le tiers qui traite ces données que la personne concernée a demandé l'effacement de tout lien vers celles-ci, ou de toute copie ou de toute reproduction de celles-ci.

En cas de non-exécution de l'effacement des données à caractère personnel ou en cas d'absence de réponse du responsable du traitement dans un délai d'un mois à compter de la demande, la personne concernée peut saisir la Commission nationale de l'informatique et des libertés, qui se prononce sur cette demande dans un délai de trois semaines à compter de la date de réception de la réclamation.

Les deux premiers alinéas du présent II ne s'appliquent pas lorsque le traitement de données à caractère personnel est nécessaire :

1° Pour exercer le droit à la liberté d'expression et d'information ;

2° Pour respecter une obligation légale qui requiert le traitement de ces données ou pour exercer une mission d'intérêt public ou relevant de l'exercice de l'autorité publique dont est investi le responsable du traitement ;

3° Pour des motifs d'intérêt public dans le domaine de la santé publique ;

4° A des fins archivistiques dans l'intérêt public, à des fins de recherche scientifique ou historique ou à des fins statistiques, dans la mesure où le droit mentionné au présent II est susceptible de rendre impossible ou de compromettre gravement la réalisation des objectifs du traitement ;

5° A la constatation, à l'exercice ou à la défense de droits en justice.

**_III_. - Un décret en Conseil d’Etat, pris après avis de la Commission nationale de l’informatique et des libertés, fixe la liste des traitements et des catégories de traitements autorisés à déroger au droit à la communication d’une violation de données régi par l’article 34 du, règlement (UE) 2016/679 lorsque la notification d’une divulgation ou d’un accès non autorisé à ces données est susceptible de représenter un risque pour la sécurité nationale, la défense nationale ou la sécurité publique. La dérogation prévue au présent alinéa n’est applicable qu’aux seuls traitements de données à caractère personnel nécessaires au respect d’une obligation légale qui requiert le traitement de ces données ou nécessaires à l’exercice d’une mission d’intérêt public dont est investi le responsable de traitement.**

</td></tr>
<tr>
<td>

**Chapitre V – Voies de recours**

</td></tr>
<tr>
<td>

**Article 16**

_Introduction d’une action de groupe avec mandat]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

</td>
<td>

Après l’article 43 _ter_ de la même loi, il est inséré un article 43 _quater_ ainsi rédigé :

« _Art. 43 quater_. - La personne concernée peut mandater une association ou une organisation mentionnée au IV de l’article 43 _ter_ aux fins d’exercer en son nom les droits visés aux articles 77 à 79 du règlement (UE) 2016/679. Elle peut également les mandater pour agir devant la Commission nationale de l’informatique et des libertés, contre celle-ci devant un juge ou contre le responsable du traitement ou le sous-traitant devant une juridiction lorsqu’est en cause un traitement relevant du chapitre XIII. »

</td>
<td>

**_Art. 43 quater_. - La personne concernée peut mandater une association ou une organisation mentionnée au IV de l’article 43 _ter_ aux fins d’exercer en son nom les droits visés aux articles 77 à 79 du règlement (UE) 2016/679. Elle peut également les mandater pour agir devant la Commission nationale de l’informatique et des libertés, contre celle-ci devant un juge ou contre le responsable du traitement ou le sous-traitant devant une juridiction lorsqu’est en cause un traitement relevant du chapitre XIII. **

</td></tr>
<tr>
<td>

**Article 17**

_Aménagement d’une voie de recours définie par l’arrêt CJUE - C-362/14 §65]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

</td>
<td>

La section 2 du chapitre V de la même loi est complétée par un article 43 _quinquies_ ainsi rédigé :

« _Art. 43 quinquies_. - Dans le cas où, saisie d’une réclamation dirigée contre un responsable de traitement ou un sous-traitant, la Commission nationale de l'informatique et des libertés estime fondés les griefs avancés relatifs à la protection des droits et libertés d’une personne à l’égard du traitement de ses données à caractère personnel, ou de manière générale afin d’assurer la protection de ces droits et libertés dans le cadre de sa mission, elle peut demander au Conseil d’Etat d’ordonner la suspension ou la cessation du transfert de données en cause, le cas échéant sous astreinte, et assortit alors ses conclusions d’une demande de question préjudicielle à la Cour de justice de l’Union européenne en vue d’apprécier la validité de la décision d’adéquation de la Commission européenne prise sur le fondement de l’article 45 du règlement (UE) 2016/679 ainsi que de tous les actes pris par la Commission européenne autorisant ou approuvant les garanties appropriées dans le cadre des transferts de données pris sur le fondement de l’article 46 du même règlement. Lorsque le transfert de données en cause ne constitue pas une opération de traitement effectuée par une juridiction dans l'exercice de sa fonction juridictionnelle, la Commission nationale de l’informatique et des libertés peut saisir dans les mêmes conditions le Conseil d’Etat pour obtenir la suspension du transfert de données fondé sur une décision d’adéquation de la Commission européenne prise sur le fondement de l’article 36 de la directive (UE) 2016/680 dans l’attente de l’appréciation par la Cour de justice de l’Union européenne de la validité de cette décision d’adéquation. »

</td>
<td>

**_Art. 43 quinquies_. - Dans le cas où, saisie d’une réclamation dirigée contre un responsable de traitement ou un sous-traitant, la Commission nationale de l'informatique et des libertés estime fondés les griefs avancés relatifs à la protection des droits et libertés d’une personne à l’égard du traitement de ses données à caractère personnel, ou de manière générale afin d’assurer la protection de ces droits et libertés dans le cadre de sa mission, elle peut demander au Conseil d’Etat d’ordonner la suspension ou la cessation du transfert de données en cause, le cas échéant sous astreinte, et assortit alors ses conclusions d’une demande de question préjudicielle à la Cour de justice de l’Union européenne en vue d’apprécier la validité de la décision d’adéquation de la Commission européenne prise sur le fondement de l’article 45 du règlement (UE) 2016/679 ainsi que de tous les actes pris par la Commission européenne autorisant ou approuvant les garanties appropriées dans le cadre des transferts de données pris sur le fondement de l’article 46 du même règlement. Lorsque le transfert de données en cause ne constitue pas une opération de traitement effectuée par une juridiction dans l'exercice de sa fonction juridictionnelle, la Commission nationale de l’informatique et des libertés peut saisir dans les mêmes conditions le Conseil d’Etat pour obtenir la suspension du transfert de données fondé sur une décision d’adéquation de la Commission européenne prise sur le fondement de l’article 36 de la directive (UE) 2016/680 dans l’attente de l’appréciation par la Cour de justice de l’Union européenne de la validité de cette décision d’adéquation. **

</td></tr>
<tr>
<td>

**_Titre III_**

**_Dispositions portant transposition de la directive (UE) 2016/680 du Parlement européen et du Conseil du 27 avril 2016 relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales, et à la libre circulation de ces données_**

</td></tr>
<tr>
<td>

**Article 18**

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 32 _**

I.-La personne auprès de laquelle sont recueillies des données à caractère personnel la concernant est informée, sauf si elle l'a été au préalable, par le responsable du traitement ou son représentant :

1° De l'identité du responsable du traitement et, le cas échéant, de celle de son représentant ;

2° De la finalité poursuivie par le traitement auquel les données sont destinées ;

3° Du caractère obligatoire ou facultatif des réponses ;

4° Des conséquences éventuelles, à son égard, d'un défaut de réponse ;

5° Des destinataires ou catégories de destinataires des données ;

6° Des droits qu'elle tient des dispositions de la section 2 du présent chapitre dont celui de définir des directives relatives au sort de ses données à caractère personnel après sa mort ;

7° Le cas échéant, des transferts de données à caractère personnel envisagés à destination d'un Etat non membre de la Communauté européenne ;

8° De la durée de conservation des catégories de données traitées ou, en cas d'impossibilité, des critères utilisés permettant de déterminer cette durée.

Lorsque de telles données sont recueillies par voie de questionnaires, ceux-ci doivent porter mention des prescriptions figurant aux 1°, 2°, 3° et 6°.

II. - Tout abonné ou utilisateur d'un service de communications électroniques doit être informé de manière claire et complète, sauf s'il l'a été au préalable, par le responsable du traitement ou son représentant :

\- de la finalité de toute action tendant à accéder, par voie de transmission électronique, à des informations déjà stockées dans son équipement terminal de communications électroniques, ou à inscrire des informations dans cet équipement ;

\- des moyens dont il dispose pour s'y opposer.

Ces accès ou inscriptions ne peuvent avoir lieu qu'à condition que l'abonné ou la personne utilisatrice ait exprimé, après avoir reçu cette information, son accord qui peut résulter de paramètres appropriés de son dispositif de connexion ou de tout autre dispositif placé sous son contrôle.

Ces dispositions ne sont pas applicables si l'accès aux informations stockées dans l'équipement terminal de l'utilisateur ou l'inscription d'informations dans l'équipement terminal de l'utilisateur :

\- soit a pour finalité exclusive de permettre ou faciliter la communication par voie électronique ;

\- soit est strictement nécessaire à la fourniture d'un service de communication en ligne à la demande expresse de l'utilisateur.

III.-Lorsque les données à caractère personnel n'ont pas été recueillies auprès de la personne concernée, le responsable du traitement ou son représentant doit fournir à cette dernière les informations énumérées au I dès l'enregistrement des données ou, si une communication des données à des tiers est envisagée, au plus tard lors de la première communication des données.

Lorsque les données à caractère personnel ont été initialement recueillies pour un autre objet, les dispositions de l'alinéa précédent ne s'appliquent pas aux traitements nécessaires à la conservation de ces données à des fins historiques, statistiques ou scientifiques, dans les conditions prévues au livre II du code du patrimoine ou à la réutilisation de ces données à des fins statistiques dans les conditions de l'article 7 bis de la _loi n° 51-711_ sont applicables.

« _Art. 70-2._ \- Le traitement de données mentionnées au I de l’article 8 est possible uniquement en cas de nécessité absolue, sous réserve de garanties appropriées pour les droits et libertés de la personne concernée, et, soit s’il est prévu par un acte législatif ou règlementaire, soit s’il vise à protéger les intérêts vitaux d’une personne physique, soit s’il porte sur des données manifestement rendues publiques par la personne concernée.

« _Art. 70-3._ \- Si le traitement est mis en œuvre pour le compte de l’Etat pour au moins l’une des finalités prévues au 1° de l’article 70-1, il doit être prévu par un acte règlementaire pris conformément au I de l’article 26 et aux articles 28 à 31.

« Si le traitement porte sur des données mentionnées au I de l’article 8, il est prévu par un acte règlementaire pris conformément au II de l’article 26.

« _Art. 70-4._ \- Si le traitement est susceptible d’engendrer un risque élevé pour les droits et les libertés des personnes physiques, notamment parce qu’il porte sur des données mentionnées au I de l’article 8, le responsable du traitement effectue une analyse d’impact relative à la protection des données à caractère personnel.

« Si le traitement est mis en œuvre pour le compte de l’Etat, cette analyse d’impact est adressée à la Commission nationale de l’informatique et des libertés avec la demande d’avis prévue par l’article 30.

« Dans les autres cas, le responsable du traitement ou le sous-traitant consulte la Commission nationale de l’informatique et des libertés préalablement au traitement des données à caractère personnel :

« 1° Soit lorsque l’analyse d’impact relative à la protection des données indique que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque ;

« 2° Soit lorsque le type de traitement, en particulier en raison de l’utilisation de nouveaux mécanismes, technologies ou procédures, présente des risques élevés pour les libertés et les droits des personnes concernées.

« _Art. 70-5._ \- Les données à caractère personnel collectées par les autorités compétentes pour les finalités énoncées au 1° de l’article 70-1, ne peuvent être traitées pour d’autres finalités, à moins qu'un tel traitement ne soit autorisé par des dispositions législatives ou réglementaires, ou par le droit de l'Union européenne. Lorsque des données à caractère personnel sont traitées à de telles autres fins, le règlement (UE) 2016/679 s'applique, à moins que le traitement ne soit effectué dans le cadre d'une activité ne relevant pas du champ d'application du droit de l'Union européenne.

« Lorsque les autorités compétentes sont chargées d'exécuter des missions autres que celles exécutées pour les finalités énoncées au 1° de l’article 70-1, le règlement (UE) 2016/679 s'applique au traitement effectué à de telles fins, y compris à des fins archivistiques dans l'intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques, à moins que le traitement ne soit effectué dans le cadre d'une activité ne relevant pas du champ d'application du droit de l'Union européenne.

« Si le traitement est soumis à des conditions spécifiques, l'autorité compétente qui transmet les données informe le destinataire de ces données à caractère personnel de ces conditions et de l'obligation de les respecter.

« L'autorité compétente qui transmet les données n'applique pas aux destinataires dans les autres Etats membres ou aux services, organes et organismes établis en vertu des chapitres 4 et 5 du titre V du traité sur le fonctionnement de l'Union européenne des conditions en vertu du paragraphe 3 différentes de celles applicables aux transferts de données similaires à l'intérieur de l'Etat membre dont relève l'autorité compétente qui transmet les données.

« _Art. 70-6. -_ Les traitements effectués pour l’une des finalités énoncées au 1° de l’article 70-1 autre que celles pour lesquelles les données ont été collectées sont autorisés sous réserve du respect des principes prévus au chapitre I er de la présente loi et au présent chapitre.

« Ces traitements peuvent comprendre l’archivage dans l’intérêt public, à des fins scientifiques, statistiques ou historiques, aux fins énoncées à l’article 70-1.

« _Art. 70-7._ \- Les traitements à des fins archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques sont mis en œuvre dans les conditions de l’article 36 de la présente loi.

« _Art. 70-8. -_ **** Les données à caractère personnel fondées sur des faits sont dans la mesure du possible distinguées de celles fondées sur des appréciations personnelles.

« _Art. 70-9. -_ Aucune décision de justice impliquant une appréciation sur le comportement d'une personne ne peut avoir pour fondement un traitement automatisé de données à caractère personnel destiné à évaluer certains aspects de sa personnalité.

« Aucune autre décision produisant des effets juridiques à l'égard d'une personne ne peut être prise sur le seul fondement d'un traitement automatisé de données destiné à prévoir ou à évaluer certains aspects personnels relatifs à la personne concernée.

« Tout profilage qui entraîne une discrimination à l'égard des personnes physiques sur la base des catégories particulières de données à caractère personnel visées à l'article 8 est interdit.

« _Art. 70-10. -_ Les données à caractère personnel ne peuvent faire l’objet d’une opération de traitement de la part d’un sous-traitant que dans les conditions prévues aux paragraphes 1, 2, 9 et 10 de l’article 28 et à l’article 29 du règlement (UE) 2016/679 et au présent article.

« Les sous-traitants doivent présenter des garanties suffisantes quant à la mise en œuvre de mesures techniques et organisationnelles appropriées de manière que le traitement réponde aux exigences du présent chapitre et garantisse la protection des droits de la personne concernée.

« Le traitement par un sous-traitant est régi par un contrat ou un autre acte juridique, qui lie le sous-traitant à l'égard du responsable du traitement, définit l'objet et la durée du traitement, la nature et la finalité du traitement, le type de données à caractère personnel et les catégories de personnes concernées, et les obligations et les droits du responsable du traitement, et qui prévoit que le sous-traitant n’agit que sur instruction du responsable de traitement. Le contenu de ce contrat ou acte juridique est précisé par décret en Conseil d’Etat pris après avis de la Commission nationale de l'informatique et des libertés.

_« Section 2_

_**« Obligations incombant aux autorités compétentes et aux responsables de traitements**_

« _Art. 70-11. -_ Les autorités compétentes prennent toutes les mesures raisonnables pour garantir que les données à caractère personnel qui sont inexactes, incomplètes ou ne sont plus à jour soient effacées ou rectifiées sans tarder ou ne soient pas transmises ou mises à disposition. A cette fin, chaque autorité compétente vérifie, dans la mesure du possible, la qualité des données à caractère personnel avant leur transmission ou mise à disposition.

« Dans la mesure du possible, lors de toute transmission de données à caractère personnel, sont ajoutées des informations nécessaires permettant à l'autorité compétente destinataire de juger de l'exactitude, de l'exhaustivité, et de la fiabilité des données à caractère personnel, et de leur niveau de mise à jour.

« S'il s'avère que des données à caractère personnel inexactes ont été transmises ou que des données à caractère personnel ont été transmises de manière illicite, le destinataire en est informé sans retard. Dans ce cas, les données à caractère personnel sont rectifiées ou effacées ou leur traitement est limité conformément à l'article 70-20.

« _Art. 70-12. -_ Le responsable du traitement établit dans la mesure du possible et le cas échéant une distinction claire entre les données à caractère personnel de différentes catégories de personnes concernées, telles que :

« 1° Les personnes à l'égard desquelles il existe des motifs sérieux de croire qu'elles ont commis ou sont sur le point de commettre une infraction pénale ;

« 2° Les personnes reconnues coupables d'une infraction pénale ;

« 3° Les victimes d'une infraction pénale ou les personnes à l'égard desquelles certains faits portent à croire qu'elles pourraient être victimes d'une infraction pénale ;

« 4° Les tiers à une infraction pénale, tels que les personnes pouvant être appelées à témoigner lors d'enquêtes en rapport avec des infractions pénales ou des procédures pénales ultérieures, des personnes pouvant fournir des informations sur des infractions pénales, ou des contacts ou des associés de l'une des personnes visées aux 1° et 2°.

« _Art. 70-13. -_ I. - Afin de démontrer que le traitement est effectué conformément au présent chapitre, le responsable du traitement et le sous-traitant mettent en œuvre les mesures prévues aux paragraphes 1 et 2 de l’article 24 et aux paragraphes 1 et 2 de l’article 25 du règlement (UE) 2016/679 et celles appropriées afin de garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l'article 8.

« II. - En ce qui concerne le traitement automatisé, le responsable du traitement ou le sous-traitant met en œuvre, à la suite d'une évaluation des risques, des mesures destinées à :

« 1° Empêcher toute personne non autorisée d'accéder aux installations utilisées pour le traitement (contrôle de l'accès aux installations) ;

« 2° Empêcher que des supports de données puissent être lus, copiés, modifiés ou supprimés de façon non autorisée (contrôle des supports de données) ;

« 3° Empêcher l'introduction non autorisée de données à caractère personnel dans le fichier, ainsi que l'inspection, la modification ou l'effacement non autorisé de données à caractère personnel enregistrées (contrôle de la conservation) ;

« 4° Empêcher que les systèmes de traitement automatisé puissent être utilisés par des personnes non autorisées à l'aide d'installations de transmission de données (contrôle des utilisateurs) ;

« 5° Garantir que les personnes autorisées à utiliser un système de traitement automatisé ne puissent accéder qu'aux données à caractère personnel sur lesquelles porte leur autorisation (contrôle de l'accès aux données) ;

« 6° Garantir qu'il puisse être vérifié et constaté à quelles instances des données à caractère personnel ont été ou peuvent être transmises ou mises à disposition par des installations de transmission de données (contrôle de la transmission) ;

« 7° Garantir qu'il puisse être vérifié et constaté a posteriori quelles données à caractère personnel ont été introduites dans les systèmes de traitement automatisé, et à quel moment et par quelle personne elles y ont été introduites (contrôle de l'introduction) ;

« 8° Empêcher que, lors de la transmission de données à caractère personnel ainsi que lors du transport de supports de données, les données puissent être lues, copiées, modifiées ou supprimées de façon non autorisée (contrôle du transport) ;

« 9° Garantir que les systèmes installés puissent être rétablis en cas d'interruption (restauration) ;

« 10° Garantir que les fonctions du système opèrent, que les erreurs de fonctionnement soient signalées (fiabilité) et que les données à caractère personnel conservées ne puissent pas être corrompues par un dysfonctionnement du système (intégrité).

« _Art. 70-14. -_ Le responsable du traitement et le sous-traitant tiennent un registre des activités de traitement dans les conditions prévues aux paragraphes 1 à 4 de l’article 30 du règlement (UE) 2016/679. Ce registre contient aussi la description générale des mesures visant à garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l'article 8, l’indication de la base juridique de l’opération de traitement, y compris les transferts, à laquelle les données à caractère personnel sont destinées et, le cas échéant, le recours au profilage.

_« Art. 70-15. **** -_ Le responsable du traitement ou son sous-traitant établit pour chaque traitement automatisé un journal des opérations de collecte, de modification, de consultation, de communication, y compris les transferts, l’interconnexion et l’effacement, portant sur de telles données.

« Les journaux des opérations de consultation et de communication permettent d’en établir le motif, la date et l’heure. Ils permettent également, dans la mesure du possible, d’identifier les personnes qui consultent ou communiquent les données et leurs destinataires.

« Ce journal est uniquement utilisé à des fins de vérification de la licéité du traitement, d’autocontrôle, de garantie de l’intégrité et de la sécurité des données et à des fins de procédures pénales.

« Ce journal est mis à la disposition de la Commission nationale de l’informatique et des libertés à sa demande.

« _Art. 70-16. **-**_ **** Les articles 31, 33 et 34 du règlement (UE) 2016/679 sont applicables aux traitements des données à caractère personnel relevant du présent chapitre.

« Si la violation de données à caractère personnel porte sur des données à caractère personnel qui ont été transmises par le responsable du traitement d’un autre Etat membre ou à celui-ci, le responsable du traitement notifie également la violation au responsable du traitement de l’autre Etat membre dans les meilleurs délais.

« La communication d’une violation de données à caractère personnel à la personne concernée peut être retardée, limitée ou ne pas être délivrée, dès lors et aussi longtemps qu’une mesure de cette nature constitue une mesure nécessaire et proportionnée dans une société démocratique, en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne physique concernée, lorsque sa mise en œuvre est de nature à mettre en danger la sécurité publique, la sécurité nationale ou les droits ou libertés d’autrui ou à faire obstacle au bon déroulement des enquêtes et procédures destinées à prévenir, détecter ou poursuivre des infractions pénales ou à exécuter des sanctions pénales.

« _Art. 70-17._ \- I. - Sauf pour les juridictions agissant dans l’exercice de leur fonction juridictionnelle, le responsable du traitement désigne un délégué à la protection des données.

« Un seul délégué à la protection des données peut être désigné pour plusieurs autorités compétentes, compte tenu de leur structure organisationnelle et de leur taille.

« Les dispositions des paragraphes 5 et 7 de l’article 37, des paragraphes 1 et 2 de l’article 38 et du paragraphe 1 de l’article 39 du règlement (UE) 2016/679, en ce qu’elles concernent le responsable du traitement, sont applicables aux traitements des données à caractère personnel relevant du présent chapitre.

_« Section 3_

_**« Droits de la personne concernée**_

« _Art. 70-18._ \- I. - Le responsable du traitement met à la disposition de la personne concernée les informations suivantes :

« 1° L'identité et les coordonnées du responsable du traitement, et le cas échéant celles de son représentant ;

« 2° Le cas échéant, les coordonnées du délégué à la protection des données ;

« 3° Les finalités poursuivies par le traitement auquel les données sont destinées ;

« 4° Le droit d'introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés et les coordonnées de la commission ;

« 5° L'existence du droit de demander au responsable du traitement l'accès aux données à caractère personnel, leur rectification ou leur effacement, et la limitation du traitement des données à caractère personnel relatives à une personne concernée.

« II. - En plus des informations visées au I, le responsable du traitement fournit à la personne concernée, dans des cas particuliers, les informations additionnelles suivantes afin de lui permettre d'exercer ses droits :

« 1° La base juridique du traitement ;

« 2° La durée de conservation des données à caractère personnel ou, lorsque ce n'est pas possible, les critères utilisés pour déterminer cette durée ;

« 3° Le cas échéant, les catégories de destinataires des données à caractère personnel, y compris dans les Etats non membres de l’Union européenne ou au sein d'organisations internationales ;

« 4° Au besoin, des informations complémentaires, en particulier lorsque les données à caractère personnel sont collectées à l'insu de la personne concernée.

« _Art. 70-19._ \- La personne concernée a le droit d'obtenir du responsable du traitement la confirmation que des données à caractère personnel la concernant sont ou ne sont pas traitées et, lorsqu'elles le sont, l'accès auxdites données ainsi que les informations suivantes :

« 1° Les finalités du traitement ainsi que sa base juridique ;

« 2° Les catégories de données à caractère personnel concernées ;

« 3° Les destinataires ou catégories de destinataires auxquels les données à caractère personnel ont été communiquées, en particulier les destinataires qui sont établis dans des Etats non membres de l’Union européenne ou les organisations internationales ;

« 4° Lorsque cela est possible, la durée de conservation des données à caractère personnel envisagée ou, lorsque ce n'est pas possible, les critères utilisés pour déterminer cette durée ;

« 5° L’existence du droit de demander au responsable du traitement la rectification ou l'effacement des données à caractère personnel, ou la limitation du traitement de ces données ;

« 6° Le droit d'introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés et les coordonnées de la commission ;

« 7° La communication des données à caractère personnel en cours de traitement, ainsi que toute information disponible quant à leur source.

« _Art. 70-20. -_ I. - La personne concernée a le droit d'obtenir du responsable du traitement :

« 1° Que soit rectifiées dans les meilleurs délais des données à caractère personnel la concernant qui sont inexactes ;

« 2° Que soient complétées des données à caractère personnel la concernant incomplètes, y compris en fournissant à cet effet une déclaration complémentaire ;

« 3° Que soit effacées dans les meilleurs délais des données à caractère personnel la concernant lorsque le traitement est réalisé en violation des dispositions de la présente loi ou lorsque ces données doivent être effacées pour respecter une obligation légale à laquelle est soumis le responsable du traitement.

« II. - Lorsque l'intéressé en fait la demande, le responsable du traitement doit justifier qu'il a procédé aux opérations exigées en vertu du I.

« III. - Au lieu de procéder à l'effacement, le responsable du traitement limite le traitement lorsque :

« 1° Soit l'exactitude des données à caractère personnel est contestée par la personne concernée et il ne peut être déterminé si les données sont exactes ou non ;

« 2° Soit les données à caractère personnel doivent être conservées à des fins probatoires.

« Lorsque le traitement est limité en vertu du 1°, le responsable du traitement informe la personne concernée avant de lever la limitation du traitement.

« IV. - Le responsable du traitement informe la personne concernée de tout refus de rectifier ou d'effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs du refus.

« V. - Le responsable du traitement communique la rectification des données à caractère personnel inexactes à l'autorité compétente dont elles proviennent.

« VI. - Lorsque des données à caractère personnel ont été rectifiées ou effacées ou que le traitement a été limité au titre des I, II et III, le responsable du traitement le notifie aux destinataires afin que ceux-ci rectifient ou effacent les données ou limitent le traitement des données sous leur responsabilité.

« _Art. 70-21. -_ I. - Les droits de la personne physique concernée peuvent faire l’objet de restrictions selon les modalités prévues au II du présent article dès lors et aussi longtemps qu'une telle restriction constitue une mesure nécessaire et proportionnée dans une société démocratique en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne pour :

« 1° Eviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires :

« 2° Eviter de nuire à la prévention ou à la détection d'infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l'exécution de sanctions pénales ;

« 3° Protéger la sécurité publique ;

« 4° Protéger la sécurité nationale ;

« 5° Protéger les droits et libertés d'autrui.

« Ces restrictions sont prévues par l’acte instaurant le traitement.

« II. - Lorsque les conditions prévues au I sont remplies, le responsable du traitement peut :

« 1° Retarder ou limiter la fourniture à la personne concernée des informations mentionnées au II de l’article 70-18, ou ne pas fournir ces informations ;

« 2° Limiter, entièrement ou partiellement, le droit d'accès de la personne concernée prévu par l’article 70-19 ;

« 3° Ne pas informer la personne de son refus de rectifier ou d'effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs de cette décision conformément au IV de l’article 70-20.

« III. - Dans les cas visés au 2° du II, le responsable du traitement informe la personne concernée, dans les meilleurs délais, de tout refus ou de toute limitation d'accès, ainsi que des motifs du refus ou de la limitation. Ces informations peuvent ne pas être fournies lorsque leur communication risque de compromettre l'un des objectifs énoncés au I. Le responsable du traitement consigne les motifs de fait ou de droit sur lesquels se fonde la décision, et met ces informations à la disposition de la Commission nationale de l’informatique et des libertés.

« IV. - En cas de restriction des droits de la personne concernée intervenue en application du II ou du III, le responsable du traitement informe la personne concernée de la possibilité d’exercer ses droits par l’intermédiaire de la Commission nationale de l’informatique et des libertés ou de former un recours juridictionnel.

« _Art. 70-22._ \- En cas de restriction des droits de la personne concernée intervenue en application du II ou du III de l’article 70-21, la personne concernée peut saisir la Commission nationale de l'informatique et des libertés.

« Les dispositions des deuxième et troisième alinéas de l’article 41 sont alors applicables.

« Lorsque la commission informe la personne concernée qu'il a été procédé aux vérifications nécessaires, elle l’informe également de son droit de former un recours juridictionnel.

« _Art. 70-23._ \- Aucun paiement n’est exigé pour prendre les mesures et fournir les informations visées aux articles 70-18 à 70-20, sauf en cas de demande manifestement infondée ou abusive.

« Dans ce cas, le responsable du traitement peut également refuser de donner suite à la demande.

« En cas de contestation, la charge de la preuve du caractère manifestement infondé ou abusif des demandes incombe au responsable du traitement auprès duquel elles sont adressées.

« _Art 70-24._ \- Les dispositions de la présente sous-section ne s’appliquent pas lorsque les données à caractère personnel figurent soit dans une décision judiciaire, soit dans un dossier judiciaire faisant l'objet d'un traitement lors d'une procédure pénale. Dans ces cas, l’accès à ces données ne peut se faire que dans les conditions prévues par le code de procédure pénale.

_« Section 4_

_**« Transferts de données à caractère personnel vers des Etats n'appartenant pas**_

**_« à l’Union européenne ou vers des destinataires établis dans des Etats non membres_**

**_« de l’Union européenne_**

« _Art. 70-25._ \- Le responsable d’un traitement de données à caractère personnel ne peut transférer des données ou autoriser le transfert de données déjà transmises vers un Etat n'appartenant pas à l’Union européenne que lorsque les conditions suivantes sont respectées :

« 1° Le transfert de ces données est nécessaire à l’une des finalités énoncées au 1° de l’article 70-1 ;

« 2° Les données à caractère personnel sont transférées à un responsable dans cet Etat tiers ou à une organisation internationale qui est une autorité compétente chargée dans cet Etat des fins relevant en France __ du 1° de l’article 70-1 ;

« 3° Si les données à caractère personnel proviennent d'un autre Etat, l’Etat qui a transmis ces données a préalablement autorisé ce transfert conformément à son droit national.

« Toutefois, si l'autorisation préalable ne peut pas être obtenue en temps utile, ces données à caractère personnel peuvent être retransmises sans l’autorisation préalable de l’Etat qui a transmis ces données lorsque cette retransmission est nécessaire à la prévention d'une menace grave et immédiate pour la sécurité publique d'un autre Etat ou pour la sauvegarde des intérêts essentiels de la France. L'autorité d’où provenaient ces données personnelles est informée sans retard.

« 4° L’une au moins des trois conditions suivantes est remplie :

« _a)_ La commission a adopté une décision d'adéquation en application de l'article 36 de la directive (UE) 2016/680 du Parlement et du Conseil du 27 avril 2016 ;

« _b)_ A défaut d’une telle décision d’adéquation, des garanties appropriées en ce qui concerne la protection des données à caractère personnel sont fournies dans un instrument juridiquement contraignant ; ces garanties appropriées peuvent soit résulter des garanties relatives à la protection des données mentionnées dans les conventions mises en œuvre avec cet Etat tiers, soit résulter de dispositions juridiquement contraignantes exigées à l’occasion de l’échange de données ;

« _c)_ A défaut d’une telle décision d’adéquation et de garanties appropriées telles que prévues au _b_ , le responsable du traitement a évalué toutes les circonstances du transfert et estime qu'il existe des garanties appropriées au regard de la protection des données à caractère personnel ;

« Lorsque le responsable d’un traitement de données à caractère personnel transfère des données à caractère personnel sur le seul fondement de l’existence de garanties appropriées au regard de la protection des données à caractère personnel, autre qu’une juridiction effectuant une activité de traitement dans le cadre de ses activités juridictionnelles, il avise la Commission nationale de l’informatique et des libertés des catégories de transferts relevant de ce fondement.

« Dans ce cas, le responsable du traitement des données doit garder trace de la date et l'heure du transfert, des informations sur l'autorité compétente destinataire, et de la justification du transfert et des données à caractère personnel transférées. Cette documentation est mise à la disposition de l'autorité de contrôle, sur sa demande.

« Lorsque la commission a abrogé, modifié ou suspendu une décision d'adéquation adoptée en application de l'article 36 de la directive précitée, le responsable d’un traitement de données à caractère personnel peut néanmoins transférer des données personnelles ou autoriser le transfert de données déjà transmises vers un Etat n'appartenant pas à l’Union européenne si des garanties appropriées en ce qui concerne la protection des données à caractère personnel sont fournies dans un instrument juridiquement contraignant ou s’il estime après avoir évalué toutes les circonstances du transfert qu'il existe des garanties appropriées au regard de la protection des données à caractère personnel.

« _Art. 70-26._ \- Par dérogation aux dispositions de l’article précédent, le responsable d’un traitement de données à caractère personnel ne peut, en l’absence de décision d’adéquation ou de garanties appropriées, transférer ces données ou autoriser le transfert de données déjà transmises vers un Etat n'appartenant pas à l’Union européenne que lorsque le transfert est nécessaire :

« 1° A la sauvegarde des intérêts vitaux de la personne concernée ou d'une autre personne ;

« 2° A la sauvegarde des intérêts légitimes de la personne concernée lorsque le droit français le prévoit ;

« 3° Pour prévenir une menace grave et immédiate pour la sécurité publique d'un Etat membre de l’Union européenne ou d'un pays tiers ;

« 4° Dans des cas particuliers, à l’une des finalités énoncées au 1° de l’article 70-1 ;

« 5° Dans un cas particulier, à la constatation, à l'exercice ou à la défense de droits en justice en rapport avec les mêmes fins.

« Dans les cas visés aux 4° et 5°, le responsable du traitement de données à caractère personnel ne transfère pas ces données s’il estime que les libertés et droits fondamentaux de la personne concernée l'emportent sur l'intérêt public dans le cadre du transfert envisagé.

« Lorsqu’un transfert est effectué aux fins de la sauvegarde des intérêts légitimes de la personne concernée, le responsable du traitement garde trace de la date et l'heure du transfert, des informations sur l'autorité compétente destinataire, et de la justification du transfert et les données à caractère personnel transférées. Il met ces informations à la disposition de la Commission nationale de l'informatique et des libertés, à sa demande.

« _Art. 70-27._ \- Toute autorité publique compétente mentionnée au 2° de l’article 70-1 __ peut, dans certains cas particuliers, transférer des données à caractère personnel directement à des destinataires établis dans un Etat n'appartenant pas à l’Union européenne, lorsque les autres dispositions de la présente loi applicables aux traitements relevant de l’article 70-1 sont respectées et que les conditions ci-après sont remplies :

« 1° Le transfert est nécessaire à l'exécution de la mission de l'autorité compétente qui transfère ces données pour l’une des finalités énoncées à l’article 70-1 ;

« 2° L'autorité compétente qui transfère ces données établit qu'il n'existe pas de libertés ni de droits fondamentaux de la personne concernée qui prévalent sur l'intérêt public nécessitant le transfert dans le cas considéré ;

« 3° L'autorité compétente qui transfère ces données estime que le transfert à l’autorité compétente de l’autre Etat est inefficace ou inapproprié, notamment parce que le transfert ne peut pas être effectué en temps opportun ;

« 4° L'autorité compétente de l’autre Etat est informée dans les meilleurs délais, à moins que cela ne soit inefficace ou inapproprié ;

« 5° L'autorité compétente qui transfère ces données informe le destinataire de la finalité ou des finalités déterminées pour lesquelles les données à caractère personnel transmises doivent exclusivement faire l'objet d'un traitement par ce destinataire, à condition qu'un tel traitement soit nécessaire ;

« L'autorité compétente qui transfère des données informe la Commission nationale de l'informatique et des libertés des transferts relevant du présent article.

« L'autorité compétente garde trace de la date et l'heure de ce transfert, des informations sur le destinataire, et de la justification du transfert et les données à caractère personnel transférées. »

</td>
<td>

**~~Chapitre XIII : Dispositions diverses.~~**

**~~Article 71~~**

**~~Des décrets en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, fixent les modalités d'application de la présente loi. L'avis rendu sur les décrets relatifs à l'application du I bis de l'article 22 et du 9° du I de l'article 25 est motivé et publié.~~**

**~~Article 72~~**

**~~La présente loi est applicable, dans sa rédaction résultant de la _loi n° 2016-1321 du 7 octobre 2016_ sont applicables.**

**_Art. 70-2._ \- Le traitement de données mentionnées au I de l’article 8 est possible uniquement en cas de nécessité absolue, sous réserve de garanties appropriées pour les droits et libertés de la personne concernée, et, soit s’il est prévu par un acte législatif ou règlementaire, soit s’il vise à protéger les intérêts vitaux d’une personne physique, soit s’il porte sur des données manifestement rendues publiques par la personne concernée.**

****

**_Art. 70-3._ \- Si le traitement est mis en œuvre pour le compte de l’Etat pour au moins l’une des finalités prévues au 1° de l’article 70-1, il doit être prévu par un acte règlementaire pris conformément au I de l’article 26 et aux articles 28 à 31.**

**Si le traitement porte sur des données mentionnées au I de l’article 8, il est prévu par un acte règlementaire pris conformément au II de l’article 26.**

**_Art. 70-4._ \- Si le traitement est susceptible d’engendrer un risque élevé pour les droits et les libertés des personnes physiques, notamment parce qu’il porte sur des données mentionnées au I de l’article 8, le responsable du traitement effectue une analyse d’impact relative à la protection des données à caractère personnel. **

**Si le traitement est mis en œuvre pour le compte de l’Etat, cette analyse d’impact est adressée à la Commission nationale de l’informatique et des libertés avec la demande d’avis prévue par l’article 30.**

**Dans les autres cas, le responsable du traitement ou le sous-traitant consulte la Commission nationale de l’informatique et des libertés préalablement au traitement des données à caractère personnel :**

**1° Soit lorsque l’analyse d’impact relative à la protection des données indique que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque ;**

**2° Soit lorsque le type de traitement, en particulier en raison de l’utilisation de nouveaux mécanismes, technologies ou procédures, présente des risques élevés pour les libertés et les droits des personnes concernées.**

**_Art. 70-5._ \- Les données à caractère personnel collectées par les autorités compétentes pour les finalités énoncées au 1° de l’article 70-1, ne peuvent être traitées pour d’autres finalités, à moins qu'un tel traitement ne soit autorisé par des dispositions législatives ou réglementaires, ou par le droit de l'Union européenne. Lorsque des données à caractère personnel sont traitées à de telles autres fins, le règlement (UE) 2016/679 s'applique, à moins que le traitement ne soit effectué dans le cadre d'une activité ne relevant pas du champ d'application du droit de l'Union européenne.**

**Lorsque les autorités compétentes sont chargées d'exécuter des missions autres que celles exécutées pour les finalités énoncées au 1° de l’article 70-1, le règlement (UE) 2016/679 s'applique au traitement effectué à de telles fins, y compris à des fins archivistiques dans l'intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques, à moins que le traitement ne soit effectué dans le cadre d'une activité ne relevant pas du champ d'application du droit de l'Union européenne.**

**Si le traitement est soumis à des conditions spécifiques, l'autorité compétente qui transmet les données informe le destinataire de ces données à caractère personnel de ces conditions et de l'obligation de les respecter.**

**L'autorité compétente qui transmet les données n'applique pas aux destinataires dans les autres Etats membres ou aux services, organes et organismes établis en vertu des chapitres 4 et 5 du titre V du traité sur le fonctionnement de l'Union européenne des conditions en vertu du paragraphe 3 différentes de celles applicables aux transferts de données similaires à l'intérieur de l'Etat membre dont relève l'autorité compétente qui transmet les données.**

**_Art. 70-6. -_ Les traitements effectués pour l’une des finalités énoncées au 1° de l’article 70-1 autre que celles pour lesquelles les données ont été collectées sont autorisés sous réserve du respect des principes prévus au chapitre I er de la présente loi et au présent chapitre.**

**Ces traitements peuvent comprendre l’archivage dans l’intérêt public, à des fins scientifiques, statistiques ou historiques, aux fins énoncées à l’article 70-1.**

**_Art. 70-7._ \- Les traitements à des fins archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques sont mis en œuvre dans les conditions de l’article 36 de la présente loi.**

**_Art. 70-8. -_ Les données à caractère personnel fondées sur des faits sont dans la mesure du possible distinguées de celles fondées sur des appréciations personnelles.**

**_Art. 70-9. -_ Aucune décision de justice impliquant une appréciation sur le comportement d'une personne ne peut avoir pour fondement un traitement automatisé de données à caractère personnel destiné à évaluer certains aspects de sa personnalité.**

**Aucune autre décision produisant des effets juridiques à l'égard d'une personne ne peut être prise sur le seul fondement d'un traitement automatisé de données destiné à prévoir ou à évaluer certains aspects personnels relatifs à la personne concernée.**

**Tout profilage qui entraîne une discrimination à l'égard des personnes physiques sur la base des catégories particulières de données à caractère personnel visées à l'article 8 est interdit.**

**_Art. 70-10. -_ Les données à caractère personnel ne peuvent faire l’objet d’une opération de traitement de la part d’un sous-traitant que dans les conditions prévues aux paragraphes 1, 2, 9 et 10 de l’article 28 et à l’article 29 du règlement (UE) 2016/679 et au présent article.**

**Les sous-traitants doivent présenter des garanties suffisantes quant à la mise en œuvre de mesures techniques et organisationnelles appropriées de manière que le traitement réponde aux exigences du présent chapitre et garantisse la protection des droits de la personne concernée.**

**Le traitement par un sous-traitant est régi par un contrat ou un autre acte juridique, qui lie le sous-traitant à l'égard du responsable du traitement, définit l'objet et la durée du traitement, la nature et la finalité du traitement, le type de données à caractère personnel et les catégories de personnes concernées, et les obligations et les droits du responsable du traitement, et qui prévoit que le sous-traitant n’agit que sur instruction du responsable de traitement. Le contenu de ce contrat ou acte juridique est précisé par décret en Conseil d’Etat pris après avis de la Commission nationale de l'informatique et des libertés.**

**_Section 2_**

**_Obligations incombant aux autorités compétentes et aux responsables de traitements_**

**_Art. 70-11. -_ Les autorités compétentes prennent toutes les mesures raisonnables pour garantir que les données à caractère personnel qui sont inexactes, incomplètes ou ne sont plus à jour soient effacées ou rectifiées sans tarder ou ne soient pas transmises ou mises à disposition. A cette fin, chaque autorité compétente vérifie, dans la mesure du possible, la qualité des données à caractère personnel avant leur transmission ou mise à disposition.**

**Dans la mesure du possible, lors de toute transmission de données à caractère personnel, sont ajoutées des informations nécessaires permettant à l'autorité compétente destinataire de juger de l'exactitude, de l'exhaustivité, et de la fiabilité des données à caractère personnel, et de leur niveau de mise à jour.**

**S'il s'avère que des données à caractère personnel inexactes ont été transmises ou que des données à caractère personnel ont été transmises de manière illicite, le destinataire en est informé sans retard. Dans ce cas, les données à caractère personnel sont rectifiées ou effacées ou leur traitement est limité conformément à l'article 70-20.**

**_Art. 70-12. -_ Le responsable du traitement établit dans la mesure du possible et le cas échéant une distinction claire entre les données à caractère personnel de différentes catégories de personnes concernées, telles que :**

**1° Les personnes à l'égard desquelles il existe des motifs sérieux de croire qu'elles ont commis ou sont sur le point de commettre une infraction pénale ;**

**2° Les personnes reconnues coupables d'une infraction pénale ;**

**3° Les victimes d'une infraction pénale ou les personnes à l'égard desquelles certains faits portent à croire qu'elles pourraient être victimes d'une infraction pénale ;**

**4° Les tiers à une infraction pénale, tels que les personnes pouvant être appelées à témoigner lors d'enquêtes en rapport avec des infractions pénales ou des procédures pénales ultérieures, des personnes pouvant fournir des informations sur des infractions pénales, ou des contacts ou des associés de l'une des personnes visées aux 1° et 2°.**

**_Art. 70-13. -_ I. - Afin de démontrer que le traitement est effectué conformément au présent chapitre, le responsable du traitement et le sous-traitant mettent en œuvre les mesures prévues aux paragraphes 1 et 2 de l’article 24 et aux paragraphes 1 et 2 de l’article 25 du règlement (UE) 2016/679 et celles appropriées afin de garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l'article 8.**

**II. - En ce qui concerne le traitement automatisé, le responsable du traitement ou le sous-traitant met en œuvre, à la suite d'une évaluation des risques, des mesures destinées à :**

**1° Empêcher toute personne non autorisée d'accéder aux installations utilisées pour le traitement (contrôle de l'accès aux installations) ;**

**2° Empêcher que des supports de données puissent être lus, copiés, modifiés ou supprimés de façon non autorisée (contrôle des supports de données) ;**

**3° Empêcher l'introduction non autorisée de données à caractère personnel dans le fichier, ainsi que l'inspection, la modification ou l'effacement non autorisé de données à caractère personnel enregistrées (contrôle de la conservation) ;**

**4° Empêcher que les systèmes de traitement automatisé puissent être utilisés par des personnes non autorisées à l'aide d'installations de transmission de données (contrôle des utilisateurs) ;**

**5° Garantir que les personnes autorisées à utiliser un système de traitement automatisé ne puissent accéder qu'aux données à caractère personnel sur lesquelles porte leur autorisation (contrôle de l'accès aux données) ;**

**6° Garantir qu'il puisse être vérifié et constaté à quelles instances des données à caractère personnel ont été ou peuvent être transmises ou mises à disposition par des installations de transmission de données (contrôle de la transmission) ;**

**7° Garantir qu'il puisse être vérifié et constaté a posteriori quelles données à caractère personnel ont été introduites dans les systèmes de traitement automatisé, et à quel moment et par quelle personne elles y ont été introduites (contrôle de l'introduction) ;**

**8° Empêcher que, lors de la transmission de données à caractère personnel ainsi que lors du transport de supports de données, les données puissent être lues, copiées, modifiées ou supprimées de façon non autorisée (contrôle du transport) ;**

**9° Garantir que les systèmes installés puissent être rétablis en cas d'interruption (restauration) ;**

**10° Garantir que les fonctions du système opèrent, que les erreurs de fonctionnement soient signalées (fiabilité) et que les données à caractère personnel conservées ne puissent pas être corrompues par un dysfonctionnement du système (intégrité).**

**_Art. 70-14. -_ Le responsable du traitement et le sous-traitant tiennent un registre des activités de traitement dans les conditions prévues aux paragraphes 1 à 4 de l’article 30 du règlement (UE) 2016/679. Ce registre contient aussi la description générale des mesures visant à garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l'article 8, l’indication de la base juridique de l’opération de traitement, y compris les transferts, à laquelle les données à caractère personnel sont destinées et, le cas échéant, le recours au profilage.**

**_Art. 70-15. -_ Le responsable du traitement ou son sous-traitant établit pour chaque traitement automatisé un journal des opérations de collecte, de modification, de consultation, de communication, y compris les transferts, l’interconnexion et l’effacement, portant sur de telles données.**

**Les journaux des opérations de consultation et de communication permettent d’en établir le motif, la date et l’heure. Ils permettent également, dans la mesure du possible, d’identifier les personnes qui consultent ou communiquent les données et leurs destinataires.**

**Ce journal est uniquement utilisé à des fins de vérification de la licéité du traitement, d’autocontrôle, de garantie de l’intégrité et de la sécurité des données et à des fins de procédures pénales.**

**Ce journal est mis à la disposition de la Commission nationale de l’informatique et des libertés à sa demande.**

**_Art. 70-16. -_ Les articles 31, 33 et 34 du règlement (UE) 2016/679 sont applicables aux traitements des données à caractère personnel relevant du présent chapitre.**

**Si la violation de données à caractère personnel porte sur des données à caractère personnel qui ont été transmises par le responsable du traitement d’un autre Etat membre ou à celui-ci, le responsable du traitement notifie également la violation au responsable du traitement de l’autre Etat membre dans les meilleurs délais.**

**La communication d’une violation de données à caractère personnel à la personne concernée peut être retardée, limitée ou ne pas être délivrée, dès lors et aussi longtemps qu’une mesure de cette nature constitue une mesure nécessaire et proportionnée dans une société démocratique, en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne physique concernée, lorsque sa mise en œuvre est de nature à mettre en danger la sécurité publique, la sécurité nationale ou les droits ou libertés d’autrui ou à faire obstacle au bon déroulement des enquêtes et procédures destinées à prévenir, détecter ou poursuivre des infractions pénales ou à exécuter des sanctions pénales.**

**_Art. 70-17._ \- I. - Sauf pour les juridictions agissant dans l’exercice de leur fonction juridictionnelle, le responsable du traitement désigne un délégué à la protection des données.**

**Un seul délégué à la protection des données peut être désigné pour plusieurs autorités compétentes, compte tenu de leur structure organisationnelle et de leur taille.**

**Les dispositions des paragraphes 5 et 7 de l’article 37, des paragraphes 1 et 2 de l’article 38 et du paragraphe 1 de l’article 39 du règlement (UE) 2016/679, en ce qu’elles concernent le responsable du traitement, sont applicables aux traitements des données à caractère personnel relevant du présent chapitre.**

**_Section 3_**

**_Droits de la personne concernée_**

**_Art. 70-18._ \- I. - Le responsable du traitement met à la disposition de la personne concernée les informations suivantes :**

**1° L'identité et les coordonnées du responsable du traitement, et le cas échéant celles de son représentant ;**

**2° Le cas échéant, les coordonnées du délégué à la protection des données ;**

**3° Les finalités poursuivies par le traitement auquel les données sont destinées ;**

**4° Le droit d'introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés et les coordonnées de la commission ;**

**5° L'existence du droit de demander au responsable du traitement l'accès aux données à caractère personnel, leur rectification ou leur effacement, et la limitation du traitement des données à caractère personnel relatives à une personne concernée.**

**II. - En plus des informations visées au I, le responsable du traitement fournit à la personne concernée, dans des cas particuliers, les informations additionnelles suivantes afin de lui permettre d'exercer ses droits :**

**1° La base juridique du traitement ;**

**2° La durée de conservation des données à caractère personnel ou, lorsque ce n'est pas possible, les critères utilisés pour déterminer cette durée ;**

**3° Le cas échéant, les catégories de destinataires des données à caractère personnel, y compris dans les Etats non membres de l’Union européenne ou au sein d'organisations internationales ;**

**4° Au besoin, des informations complémentaires, en particulier lorsque les données à caractère personnel sont collectées à l'insu de la personne concernée.**

**_Art. 70-19._ \- La personne concernée a le droit d'obtenir du responsable du traitement la confirmation que des données à caractère personnel la concernant sont ou ne sont pas traitées et, lorsqu'elles le sont, l'accès auxdites données ainsi que les informations suivantes :**

**1° Les finalités du traitement ainsi que sa base juridique ;**

**2° Les catégories de données à caractère personnel concernées ;**

**3° Les destinataires ou catégories de destinataires auxquels les données à caractère personnel ont été communiquées, en particulier les destinataires qui sont établis dans des Etats non membres de l’Union européenne ou les organisations internationales ;**

**4° Lorsque cela est possible, la durée de conservation des données à caractère personnel envisagée ou, lorsque ce n'est pas possible, les critères utilisés pour déterminer cette durée ;**

**5° L’existence du droit de demander au responsable du traitement la rectification ou l'effacement des données à caractère personnel, ou la limitation du traitement de ces données ;**

**6° Le droit d'introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés et les coordonnées de la commission ;**

**7° La communication des données à caractère personnel en cours de traitement, ainsi que toute information disponible quant à leur source.**

**_Art. 70-20. -_ I. - La personne concernée a le droit d'obtenir du responsable du traitement :**

**1° Que soit rectifiées dans les meilleurs délais des données à caractère personnel la concernant qui sont inexactes ;**

**2° Que soient complétées des données à caractère personnel la concernant incomplètes, y compris en fournissant à cet effet une déclaration complémentaire ;**

**3° Que soit effacées dans les meilleurs délais des données à caractère personnel la concernant lorsque le traitement est réalisé en violation des dispositions de la présente loi ou lorsque ces données doivent être effacées pour respecter une obligation légale à laquelle est soumis le responsable du traitement.**

**II. - Lorsque l'intéressé en fait la demande, le responsable du traitement doit justifier qu'il a procédé aux opérations exigées en vertu du I.**

**III. - Au lieu de procéder à l'effacement, le responsable du traitement limite le traitement lorsque :**

**1° Soit l'exactitude des données à caractère personnel est contestée par la personne concernée et il ne peut être déterminé si les données sont exactes ou non ;**

**2° Soit les données à caractère personnel doivent être conservées à des fins probatoires.**

**Lorsque le traitement est limité en vertu du 1°, le responsable du traitement informe la personne concernée avant de lever la limitation du traitement.**

**IV. - Le responsable du traitement informe la personne concernée de tout refus de rectifier ou d'effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs du refus.**

**V. - Le responsable du traitement communique la rectification des données à caractère personnel inexactes à l'autorité compétente dont elles proviennent.**

**VI. - Lorsque des données à caractère** **personnel ont été rectifiées ou effacées ou que le traitement a été limité au titre des I, II et III, le responsable du traitement le notifie aux destinataires afin que ceux-ci rectifient ou effacent les données ou limitent le traitement des données sous leur responsabilité.**

**_Art. 70-21. -_ I. - Les droits de la personne physique concernée peuvent faire l’objet de restrictions selon les modalités prévues au II du présent article dès lors et aussi longtemps qu'une telle restriction constitue une mesure nécessaire et proportionnée dans une société démocratique en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne pour :**

**1° Eviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires :**

**2° Eviter de nuire à la prévention ou à la détection d'infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l'exécution de sanctions pénales ;**

**3° Protéger la sécurité publique ;**

**4° Protéger la sécurité nationale ;**

**5° Protéger les droits et libertés d'autrui.**

**Ces restrictions sont prévues par l’acte instaurant le traitement.**

**II. - Lorsque les conditions prévues au I sont remplies, le responsable du traitement peut :**

**1° Retarder ou limiter la fourniture à la personne concernée des informations mentionnées au II de l’article 70-18, ou ne pas fournir ces informations ;**

**2° Limiter, entièrement ou partiellement, le droit d'accès de la personne concernée prévu par l’article 70-19 ;**

**3° Ne pas informer la personne de son refus de rectifier ou d'effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs de cette décision conformément au IV de l’article 70-20.**

**III. - Dans les cas visés au 2° du II, le responsable du traitement informe la personne concernée, dans les meilleurs délais, de tout refus ou de toute limitation d'accès, ainsi que des motifs du refus ou de la limitation. Ces informations** **peuvent ne pas être fournies lorsque leur communication risque de compromettre l'un des objectifs énoncés au I. Le responsable du traitement consigne les motifs de fait ou de droit sur lesquels se fonde la décision, et met ces informations à la disposition de la Commission nationale de l’informatique et des libertés.**

**IV. - En cas de restriction des droits de la personne concernée intervenue en application du II ou du III, le responsable du traitement informe la personne concernée de la possibilité d’exercer ses droits par l’intermédiaire de la Commission nationale de l’informatique et des libertés ou de former un recours juridictionnel.**

**_Art. 70-22._ \- En cas de restriction des droits de la personne concernée intervenue en application du II ou du III de l’article 70-21, la personne concernée peut saisir la Commission nationale de l'informatique et des libertés.**

**Les dispositions des deuxième et troisième alinéas de l’article 41 sont alors applicables.**

**Lorsque la commission informe la personne concernée qu'il a été procédé aux vérifications nécessaires, elle l’informe également de son droit de former un recours juridictionnel.**

**_Art. 70-23._ \- Aucun paiement n’est exigé pour prendre les mesures et fournir les informations visées aux articles 70-18 à 70-20, sauf en cas de demande manifestement infondée ou abusive.**

**Dans ce cas, le responsable du traitement peut également refuser de donner suite à la demande.**

**En cas de contestation, la charge de la preuve du caractère manifestement infondé ou abusif des demandes incombe au responsable du traitement auprès duquel elles sont adressées.**

**_Art 70-24._ \- Les dispositions de la présente sous-section ne s’appliquent pas lorsque les données à caractère personnel figurent soit dans une décision judiciaire, soit dans un dossier judiciaire faisant l'objet d'un traitement lors d'une procédure pénale. Dans ces cas, l’accès à ces données ne peut se faire que dans les** **conditions prévues par le code de procédure pénale.**

**_Section 4_**

**_Transferts de données à caractère personnel vers des Etats n'appartenant pas à l’Union européenne ou vers des destinataires établis dans des Etats non membres de l’Union européenne_**

**_Art. 70-25._ \- Le responsable d’un traitement de données à caractère personnel ne peut transférer des données ou autoriser le transfert de données déjà transmises vers un Etat n'appartenant pas à l’Union européenne que lorsque les conditions suivantes sont respectées :**

**1° Le transfert de ces données est nécessaire à l’une des finalités énoncées au 1° de l’article 70-1 ;**

**2° Les données à caractère personnel sont transférées à un responsable dans cet Etat tiers ou à une organisation internationale qui est une autorité compétente chargée dans cet Etat des fins relevant en France __ du 1° de l’article 70-1 ;**

**3° Si les données à caractère** **personnel proviennent d'un autre Etat, l’Etat qui a transmis ces données a préalablement autorisé ce transfert conformément à son droit national.**

**Toutefois, si l'autorisation préalable ne peut pas être obtenue en temps utile, ces données à caractère personnel peuvent être retransmises sans l’autorisation préalable de l’Etat qui a transmis ces données lorsque cette retransmission est nécessaire à la prévention d'une menace grave et immédiate pour la sécurité publique d'un autre Etat ou pour la sauvegarde des intérêts essentiels de la France. L'autorité d’où provenaient ces données personnelles est informée sans retard.**

**4° L’une au moins des trois conditions suivantes est remplie :**

**_a)_ La commission a adopté une décision d'adéquation en application de l'article 36 de la directive (UE) 2016/680 du Parlement et du Conseil du 27 avril 2016 ;**

**_b)_ A défaut d’une telle décision d’adéquation, des garanties appropriées en ce qui concerne la protection des données à caractère personnel sont** **fournies dans un instrument juridiquement contraignant ; ces garanties appropriées peuvent soit résulter des garanties relatives à la protection des données mentionnées dans les conventions mises en œuvre avec cet Etat tiers, soit résulter de dispositions juridiquement contraignantes exigées à l’occasion de l’échange de données ;**

**_c)_ A défaut d’une telle décision d’adéquation et de garanties appropriées telles que prévues au _b_ , le responsable du traitement a évalué toutes les circonstances du transfert et estime qu'il existe des garanties appropriées au regard de la protection des données à caractère personnel ;**

**Lorsque le responsable d’un traitement de données à caractère personnel transfère des données à caractère personnel sur le seul fondement de l’existence de garanties appropriées au regard de la protection des données à caractère personnel, autre qu’une juridiction effectuant une activité de traitement dans le cadre de ses activités juridictionnelles, il avise la Commission nationale de l’informatique et des libertés des catégories de transferts relevant de ce fondement.**

**Dans ce cas, le responsable du traitement des données doit garder trace de la date et l'heure du transfert, des informations sur l'autorité compétente destinataire, et de la justification du transfert et des données à caractère personnel transférées. Cette documentation est mise à la disposition de l'autorité de contrôle, sur sa demande.**

**Lorsque la commission a abrogé, modifié ou suspendu une décision d'adéquation adoptée en application de l'article 36 de la directive précitée, le responsable d’un traitement de données à caractère personnel peut néanmoins transférer des données personnelles ou autoriser le transfert de données déjà transmises vers un Etat n'appartenant pas à l’Union européenne si des garanties appropriées en ce qui concerne la protection des données à caractère personnel sont fournies dans un instrument juridiquement contraignant ou s’il estime après avoir évalué toutes les circonstances du transfert qu'il existe des garanties appropriées au regard de la protection des données à caractère personnel.**

**_Art. 70-26._ \- Par dérogation aux dispositions de l’article précédent, le** **responsable d’un traitement de données à caractère personnel ne peut, en l’absence de décision d’adéquation ou de garanties appropriées, transférer ces données ou autoriser le transfert de données déjà transmises vers un Etat n'appartenant pas à l’Union européenne que lorsque le transfert est nécessaire :**

**1° A la sauvegarde des intérêts vitaux de la personne concernée ou d'une autre personne ;**

**2° A la sauvegarde des intérêts légitimes de la personne concernée lorsque le droit français le prévoit ;**

**3° Pour prévenir une menace grave et immédiate pour la sécurité publique d'un Etat membre de l’Union européenne ou d'un pays tiers ;**

**4° Dans des cas particuliers, à l’une des finalités énoncées au 1° de l’article 70-1 ;**

**5° Dans un cas particulier, à la constatation, à l'exercice ou à la défense de droits en justice en rapport avec les mêmes fins.**

**Dans les cas visés aux 4° et 5°,** **le responsable du traitement de données à caractère personnel ne transfère pas ces données s’il estime que les libertés et droits fondamentaux de la personne concernée l'emportent sur l'intérêt public dans le cadre du transfert envisagé.**

**Lorsqu’un transfert est effectué aux fins de la sauvegarde des intérêts légitimes de la personne concernée, le responsable du traitement garde trace de la date et l'heure du transfert, des informations sur l'autorité compétente destinataire, et de la justification du transfert et les données à caractère personnel transférées. Il met ces informations à la disposition de la Commission nationale de l'informatique et des libertés, à sa demande.**

**_Art. 70-27._ \- Toute autorité publique compétente mentionnée au 2° de l’article 70-1 __ peut, dans certains cas particuliers, transférer des données à caractère personnel directement à des destinataires établis dans un Etat n'appartenant pas à l’Union européenne, lorsque les autres dispositions de la présente loi applicables aux traitements relevant de l’article 70-1 sont respectées et que les conditions ci-après sont remplies :**

**1° Le transfert est nécessaire à l'exécution de la mission de l'autorité compétente qui transfère ces données pour l’une des finalités énoncées à l’article 70-1 ;**

**2° L'autorité compétente qui transfère ces données établit qu'il n'existe pas de libertés ni de droits fondamentaux de la personne concernée qui prévalent sur l'intérêt public nécessitant le transfert dans le cas considéré ;**

**3° L'autorité compétente qui transfère ces données estime que le transfert à l’autorité compétente de l’autre Etat est inefficace ou inapproprié, notamment parce que le transfert ne peut pas être effectué en temps opportun ;**

**4° L'autorité compétente de l’autre Etat est informée dans les meilleurs délais, à moins que cela ne soit inefficace ou inapproprié ;**

**5° L'autorité compétente qui transfère ces données informe le destinataire de la finalité ou des finalités déterminées pour lesquelles les données à caractère personnel transmises doivent exclusivement faire l'objet d'un traitement par ce destinataire, à condition qu'un tel traitement soit nécessaire ;**

**L'autorité compétente qui transfère des données informe la Commission nationale de l'informatique et des libertés des transferts relevant du présent article.**

**L'autorité compétente garde trace de la date et l'heure de ce transfert, des informations sur le destinataire, et de la justification du transfert et les données à caractère personnel transférées.**

Chapitre **~~XIII~~** **XIV** : Dispositions diverses

[…]

</td></tr>
<tr>
<td>

**_Titre IV_**

**_Habilitation à mettre le droit national en conformité_**

**_avec le droit européen de la protection des données personnelles_**

</td></tr>
<tr>
<td>

**Article 20**

_[Habilitation]_

</td>
<td>

</td>
<td>

I. - Dans les conditions prévues à l'article 38 de la Constitution, le Gouvernement est autorisé à prendre par voie d’ordonnance les mesures relevant du domaine de la loi nécessaires :

1° A la réécriture de l’ensemble de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés afin d’apporter les corrections formelles et les adaptations nécessaires à la simplification et à la cohérence ainsi qu’à la simplicité de la mise en œuvre par les personnes concernées des dispositions qui mettent le droit national en conformité avec le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 et transposent la directive (UE) 2016/680 du Parlement européen et du Conseil du 27 avril 2016, telles que résultant de la présente loi ;

2° Pour mettre en cohérence avec ces changements l’ensemble de la législation applicable à la protection des données à caractère personnel, apporter les modifications qui seraient rendues nécessaires pour assurer le respect de la hiérarchie des normes et la cohérence rédactionnelle des textes, harmoniser l'état du droit, remédier aux éventuelles erreurs et omissions résultant de la présente loi, et abroger les dispositions devenues sans objet ;

3° A l’adaptation et aux extensions à l’outre-mer des dispositions prévues aux 1° et 2°, ainsi qu’à l’application en Nouvelle-Calédonie, à Wallis-et-Futuna en Polynésie française, à Saint-Barthélemy, à Saint-Pierre-et-Miquelon et dans les Terres australes et antarctique françaises.

II. - Cette ordonnance est prise, après avis de la Commission nationale de l’informatique et des libertés, dans un délai de six mois à compter de la promulgation de la présente loi.

III. - Un projet de loi de ratification est déposé devant le Parlement dans un délai de six mois à compter de la publication de l’ordonnance.

</td>
<td>

</td></tr>
<tr>
<td>

**_Titre V_**

**_Dispositions diverses et finales_**

</td></tr>
<tr>
<td>

**Article 21 (1°)**

_[Mesures de coordination]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 15_**

Sous réserve des compétences du bureau et de la formation restreinte, la commission se réunit en formation plénière.

En cas de partage égal des voix, la voix du président est prépondérante.

La commission peut charger le président ou le vice-président délégué d'exercer celles de ses attributions mentionnées :

\- au troisième alinéa du I de l'article 23 ;

\- aux e et f du 2° de l'article 11 ;

\- au c du 2° de l'article 11 ;

\- au d du 4° de l'article 11 ;

\- aux articles 41 et 42 ;

\- à l'article 54 ;

\- aux deux derniers alinéas de l'article 69, à l'exception des traitements mentionnés aux I ou II de l'article 26 ;

\- au premier alinéa de l'article 70.

</td>
<td>

La loi n°78-17 du 6 janvier 1978 relative à l’informatique et aux libertés est ainsi modifiée :

1° A l'article 15, le quatrième alinéa est supprimé ;

</td>
<td>

**_Article 15_**

Sous réserve des compétences du bureau et de la formation restreinte, la commission se réunit en formation plénière.

En cas de partage égal des voix, la voix du président est prépondérante.

La commission peut charger le président ou le vice-président délégué d'exercer celles de ses attributions mentionnées :

**~~\- au troisième alinéa du I de l'article 23~~** ;

\- aux e et f du 2° de l'article 11 ;

\- au c du 2° de l'article 11 ;

\- au d du 4° de l'article 11 ;

\- aux articles 41 et 42 ;

\- à l'article 54 ;

\- aux deux derniers alinéas de l'article 69, à l'exception des traitements mentionnés aux I ou II de l'article 26 ;

\- au premier alinéa de l'article 70.

</td></tr>
<tr>
<td>

**Article 21 (2°)**

_[Mesures de coordination]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 16_**

Le bureau peut être chargé par la commission d'exercer les attributions de celle-ci mentionnées :

\- au dernier alinéa de l'article 19 ;

\- à l'article 25, en cas d'urgence ;

\- au second alinéa de l'article 70.

</td>
<td>

2° A l’article 16, le troisième alinéa est supprimé ;

</td>
<td>

**_Article 16_**

Le bureau peut être chargé par la commission d'exercer les attributions de celle-ci mentionnées :

\- au dernier alinéa de l'article 19 ;

**~~\- à l'article 25, en cas d'urgence ;~~**

\- au second alinéa de l'article 70.

</td></tr>
<tr>
<td>

**Article 21 (3°)**

_[Mesures de coordination]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 29_**

Les actes autorisant la création d'un traitement en application des articles 25, 26 et 27 précisent :

1° La dénomination et la finalité du traitement ;

2° Le service auprès duquel s'exerce le droit d'accès défini au chapitre VII ;

3° Les catégories de données à caractère personnel enregistrées ;

4° Les destinataires ou catégories de destinataires habilités à recevoir communication de ces données ;

5° Le cas échéant, les dérogations à l'obligation d'information prévues au V de l'article 32.

</td>
<td>

3° A l’article 29, le mot : « 25, » est supprimé ;

</td>
<td>

**_Article 29_**

Les actes autorisant la création d'un traitement en application des articles **~~25,~~** 26 et 27 précisent :

1° La dénomination et la finalité du traitement ;

2° Le service auprès duquel s'exerce le droit d'accès défini au chapitre VII ;

3° Les catégories de données à caractère personnel enregistrées ;

4° Les destinataires ou catégories de destinataires habilités à recevoir communication de ces données ;

5° Le cas échéant, les dérogations à l'obligation d'information prévues au V de l'article 32.

</td></tr>
<tr>
<td>

**Article 21 (4°)**

_[Mesures de coordination]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 30_**

I. - Les déclarations, demandes d'autorisation et demandes d'avis adressées à la Commission nationale de l'informatique et des libertés en vertu des dispositions des sections 1 et 2 précisent :

1° L'identité et l'adresse du responsable du traitement ou, si celui-ci n'est établi ni sur le territoire national ni sur celui d'un autre Etat membre de la Communauté européenne, celle de son représentant et, le cas échéant, celle de la personne qui présente la demande ;

2° La ou les finalités du traitement, ainsi que, pour les traitements relevant des articles 25, 26 et 27, la description générale de ses fonctions ;

3° Le cas échéant, les interconnexions, les rapprochements ou toutes autres formes de mise en relation avec d'autres traitements ;

4° Les données à caractère personnel traitées, leur origine et les catégories de personnes concernées par le traitement ;

5° La durée de conservation des informations traitées ;

6° Le ou les services chargés de mettre en œuvre le traitement ainsi que, pour les traitements relevant des articles 25, 26 et 27, les catégories de personnes qui, en raison de leurs fonctions ou pour les besoins du service, ont directement accès aux données enregistrées ;

7° Les destinataires ou catégories de destinataires habilités à recevoir communication des données ;

8° La fonction de la personne ou le service auprès duquel s'exerce le droit d'accès prévu à l'article 39, ainsi que les mesures relatives à l'exercice de ce droit ;

9° Les dispositions prises pour assurer la sécurité des traitements et des données et la garantie des secrets protégés par la loi et, le cas échéant, l'indication du recours à un sous-traitant ;

10° Le cas échéant, les transferts de données à caractère personnel envisagés à destination d'un Etat non membre de la Communauté européenne, sous quelque forme que ce soit, à l'exclusion des traitements qui ne sont utilisés qu'à des fins de transit sur le territoire français ou sur celui d'un autre Etat membre de la Communauté européenne au sens des dispositions du 2° du I de l'article 5.

Les demandes d'avis portant sur les traitements intéressant la sûreté de l'Etat, la défense ou la sécurité publique peuvent ne pas comporter tous les éléments d'information énumérés ci-dessus. Un décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, fixe la liste de ces traitements et des informations que les demandes d'avis portant sur ces traitements doivent comporter au minimum.

II. - Le responsable d'un traitement déjà déclaré ou autorisé informe sans délai la commission :

\- de tout changement affectant les informations mentionnées au I ;

\- de toute suppression du traitement.

</td>
<td>

4° Au I de l’article 30, le mot : « déclarations, » et les références à l’article 25 sont supprimées ;

</td>
<td>

**_Article 30_**

I. - Les **~~déclarations,~~** demandes d'autorisation et demandes d'avis adressées à la Commission nationale de l'informatique et des libertés en vertu des dispositions des sections 1 et 2 précisent :

1° L'identité et l'adresse du responsable du traitement ou, si celui-ci n'est établi ni sur le territoire national ni sur celui d'un autre Etat membre de la Communauté européenne, celle de son représentant et, le cas échéant, celle de la personne qui présente la demande ;

2° La ou les finalités du traitement, ainsi que, pour les traitements relevant des articles **~~25,~~** 26 et 27, la description générale de ses fonctions ;

3° Le cas échéant, les interconnexions, les rapprochements ou toutes autres formes de mise en relation avec d'autres traitements ;

4° Les données à caractère personnel traitées, leur origine et les catégories de personnes concernées par le traitement ;

5° La durée de conservation des informations traitées ;

6° Le ou les services chargés de mettre en œuvre le traitement ainsi que, pour les traitements relevant des articles **~~25,~~** 26 et 27, les catégories de personnes qui, en raison de leurs fonctions ou pour les besoins du service, ont directement accès aux données enregistrées ;

7° Les destinataires ou catégories de destinataires habilités à recevoir communication des données ;

8° La fonction de la personne ou le service auprès duquel s'exerce le droit d'accès prévu à l'article 39, ainsi que les mesures relatives à l'exercice de ce droit ;

9° Les dispositions prises pour assurer la sécurité des traitements et des données et la garantie des secrets protégés par la loi et, le cas échéant, l'indication du recours à un sous-traitant ;

10° Le cas échéant, les transferts de données à caractère personnel envisagés à destination d'un Etat non membre de la Communauté européenne, sous quelque forme que ce soit, à l'exclusion des traitements qui ne sont utilisés qu'à des fins de transit sur le territoire français ou sur celui d'un autre Etat membre de la Communauté européenne au sens des dispositions du 2° du I de l'article 5.

Les demandes d'avis portant sur les traitements intéressant la sûreté de l'Etat, la défense ou la sécurité publique peuvent ne pas comporter tous les éléments d'information énumérés ci-dessus. Un décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, fixe la liste de ces traitements et des informations que les demandes d'avis portant sur ces traitements doivent comporter au minimum.

II. - Le responsable d'un traitement déjà déclaré ou autorisé informe sans délai la commission :

\- de tout changement affectant les informations mentionnées au I ;

\- de toute suppression du traitement.

</td></tr>
<tr>
<td>

**Article 21 (5°)**

_[Mesures de coordination]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 31_**

I. - La commission met à la disposition du public, dans un format ouvert et aisément réutilisable, la liste des traitements automatisés ayant fait l'objet d'une des formalités prévues par les articles 23 à 27, à l'exception de ceux mentionnés au III de l'article 26.

Cette liste précise pour chacun de ces traitements :

1° L'acte décidant la création du traitement ou la date de la déclaration de ce traitement ;

2° La dénomination et la finalité du traitement ;

3° L'identité et l'adresse du responsable du traitement ou, si celui-ci n'est établi ni sur le territoire national ni sur celui d'un autre Etat membre de la Communauté européenne, celles de son représentant ;

4° La fonction de la personne ou le service auprès duquel s'exerce le droit d'accès prévu à l'article 39 ;

5° Les catégories de données à caractère personnel faisant l'objet du traitement, ainsi que les destinataires et catégories de destinataires habilités à en recevoir communication ;

6° Le cas échéant, les transferts de données à caractère personnel envisagés à destination d'un Etat non membre de la Communauté européenne.

II. - La commission tient à la disposition du public ses avis, décisions ou recommandations.

III. - La Commission nationale de l'informatique et des libertés publie la liste des Etats dont la Commission des Communautés européennes a établi qu'ils assurent un niveau de protection suffisant à l'égard d'un transfert ou d'une catégorie de transferts de données à caractère personnel.

</td>
<td>

5° Au I de l’article 31, les mots : « 23 à » sont remplacés par les mots : « 26 et » et les mots : « ou la date de la déclaration de ce traitement » sont supprimés ;

</td>
<td>

**_Article 31_**

I. - La commission met à la disposition du public, dans un format ouvert et aisément réutilisable, la liste des traitements automatisés ayant fait l'objet d'une des formalités prévues par les articles **~~23 à~~** **26 et** 27, à l'exception de ceux mentionnés au III de l'article 26.

Cette liste précise pour chacun de ces traitements :

1° L'acte décidant la création du traitement **~~ou la date de la déclaration de ce traitement~~** ;

2° La dénomination et la finalité du traitement ;

3° L'identité et l'adresse du responsable du traitement ou, si celui-ci n'est établi ni sur le territoire national ni sur celui d'un autre Etat membre de la Communauté européenne, celles de son représentant ;

4° La fonction de la personne ou le service auprès duquel s'exerce le droit d'accès prévu à l'article 39 ;

5° Les catégories de données à caractère personnel faisant l'objet du traitement, ainsi que les destinataires et catégories de destinataires habilités à en recevoir communication ;

6° Le cas échéant, les transferts de données à caractère personnel envisagés à destination d'un Etat non membre de la Communauté européenne.

II. - La commission tient à la disposition du public ses avis, décisions ou recommandations.

III. - La Commission nationale de l'informatique et des libertés publie la liste des Etats dont la Commission des Communautés européennes a établi qu'ils assurent un niveau de protection suffisant à l'égard d'un transfert ou d'une catégorie de transferts de données à caractère personnel.

</td></tr>
<tr>
<td>

**Article 21 (6°)**

_[Mesures de coordination]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 39_**

I.-Toute personne physique justifiant de son identité a le droit d'interroger le responsable d'un traitement de données à caractère personnel en vue d'obtenir :

1° La confirmation que des données à caractère personnel la concernant font ou ne font pas l'objet de ce traitement ;

2° Des informations relatives aux finalités du traitement, aux catégories de données à caractère personnel traitées et aux destinataires ou aux catégories de destinataires auxquels les données sont communiquées ;

3° Le cas échéant, des informations relatives aux transferts de données à caractère personnel envisagés à destination d'un Etat non membre de la Communauté européenne ;

4° La communication, sous une forme accessible, des données à caractère personnel qui la concernent ainsi que de toute information disponible quant à l'origine de celles-ci ;

5° Les informations permettant de connaître et de contester la logique qui sous-tend le traitement automatisé en cas de décision prise sur le fondement de celui-ci et produisant des effets juridiques à l'égard de l'intéressé. Toutefois, les informations communiquées à la personne concernée ne doivent pas porter atteinte au droit d'auteur au sens des dispositions du livre Ier et du titre IV du livre III du code de la propriété intellectuelle.

Une copie des données à caractère personnel est délivrée à l'intéressé à sa demande. Le responsable du traitement peut subordonner la délivrance de cette copie au paiement d'une somme qui ne peut excéder le coût de la reproduction.

En cas de risque de dissimulation ou de disparition des données à caractère personnel, le juge compétent peut ordonner, y compris en référé, toutes mesures de nature à éviter cette dissimulation ou cette disparition.

II.-Le responsable du traitement peut s'opposer aux demandes manifestement abusives, notamment par leur nombre, leur caractère répétitif ou systématique. En cas de contestation, la charge de la preuve du caractère manifestement abusif des demandes incombe au responsable auprès duquel elles sont adressées.

Les dispositions du présent article ne s'appliquent pas lorsque les données à caractère personnel sont conservées sous une forme excluant manifestement tout risque d'atteinte à la vie privée des personnes concernées et pendant une durée n'excédant pas celle nécessaire aux seules finalités d'établissement de statistiques ou de recherche scientifique ou historique. Hormis les cas mentionnés au deuxième alinéa de l'article 36, les dérogations envisagées par le responsable du traitement sont mentionnées dans la demande d'autorisation ou dans la déclaration adressée à la Commission nationale de l'informatique et des libertés.

</td>
<td>

6° Au dernier alinéa de l’article 39, les mots : « ou dans la déclaration » sont supprimés ;

</td>
<td>

**_Article 39_**

I.-Toute personne physique justifiant de son identité a le droit d'interroger le responsable d'un traitement de données à caractère personnel en vue d'obtenir :

1° La confirmation que des données à caractère personnel la concernant font ou ne font pas l'objet de ce traitement ;

2° Des informations relatives aux finalités du traitement, aux catégories de données à caractère personnel traitées et aux destinataires ou aux catégories de destinataires auxquels les données sont communiquées ;

3° Le cas échéant, des informations relatives aux transferts de données à caractère personnel envisagés à destination d'un Etat non membre de la Communauté européenne ;

4° La communication, sous une forme accessible, des données à caractère personnel qui la concernent ainsi que de toute information disponible quant à l'origine de celles-ci ;

5° Les informations permettant de connaître et de contester la logique qui sous-tend le traitement automatisé en cas de décision prise sur le fondement de celui-ci et produisant des effets juridiques à l'égard de l'intéressé. Toutefois, les informations communiquées à la personne concernée ne doivent pas porter atteinte au droit d'auteur au sens des dispositions du livre Ier et du titre IV du livre III du code de la propriété intellectuelle.

Une copie des données à caractère personnel est délivrée à l'intéressé à sa demande. Le responsable du traitement peut subordonner la délivrance de cette copie au paiement d'une somme qui ne peut excéder le coût de la reproduction.

En cas de risque de dissimulation ou de disparition des données à caractère personnel, le juge compétent peut ordonner, y compris en référé, toutes mesures de nature à éviter cette dissimulation ou cette disparition.

II.-Le responsable du traitement peut s'opposer aux demandes manifestement abusives, notamment par leur nombre, leur caractère répétitif ou systématique. En cas de contestation, la charge de la preuve du caractère manifestement abusif des demandes incombe au responsable auprès duquel elles sont adressées.

Les dispositions du présent article ne s'appliquent pas lorsque les données à caractère personnel sont conservées sous une forme excluant manifestement tout risque d'atteinte à la vie privée des personnes concernées et pendant une durée n'excédant pas celle nécessaire aux seules finalités d'établissement de statistiques ou de recherche scientifique ou historique. Hormis les cas mentionnés au deuxième alinéa de l'article 36, les dérogations envisagées par le responsable du traitement sont mentionnées dans la demande d'autorisation **~~ou dans la déclaration~~** adressée à la Commission nationale de l'informatique et des libertés.

</td></tr>
<tr>
<td>

**Article 21 (7°)**

_[Mesures de coordination]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 67_**

Le 5° de l'article 6, les articles 8, 9, 22, les 1° et 3° du I de l'article 25, les articles 32, et 39, le I de l'article 40 et les articles 68 à 70 ne s'appliquent pas aux traitements de données à caractère personnel mis en œuvre aux seules fins :

1° D'expression littéraire et artistique ;

2° D'exercice, à titre professionnel, de l'activité de journaliste, dans le respect des règles déontologiques de cette profession.

Toutefois, pour les traitements mentionnés au 2°, la dispense de l'obligation de déclaration prévue par l'article 22 est subordonnée à la désignation par le responsable du traitement d'un correspondant à la protection des données appartenant à un organisme de la presse écrite ou audiovisuelle, chargé de tenir un registre des traitements mis en œuvre par ce responsable et d'assurer, d'une manière indépendante, l'application des dispositions de la présente loi. Cette désignation est portée à la connaissance de la Commission nationale de l'informatique et des libertés.

En cas de non-respect des dispositions de la loi applicables aux traitements prévus par le présent article, le responsable du traitement est enjoint par la Commission nationale de l'informatique et des libertés de se mettre en conformité avec la loi. En cas de manquement constaté à ses devoirs, le correspondant est déchargé de ses fonctions sur demande, ou après consultation, de la Commission nationale de l'informatique et des libertés.

Les dispositions des alinéas précédents ne font pas obstacle à l'application des dispositions du code civil, des lois relatives à la presse écrite ou audiovisuelle et du code pénal, qui prévoient les conditions d'exercice du droit de réponse et qui préviennent, limitent, réparent et, le cas échéant, répriment les atteintes à la vie privée et à la réputation des personnes.

</td>
<td>

7° A l’article 67, sont supprimés :

_a)_ Au premier alinéa, les mots : « 22, les 1° et 3° du I de l’article 25, les articles » ;

_b)_ Le quatrième alinéa ;

_c)_ Au cinquième alinéa, les mots : « En cas de manquement constaté à ses devoirs, le correspondant est déchargé de ses fonctions sur demande, ou après consultation, de la Commission nationale de l'informatique et des libertés » ;

</td>
<td>

**_Article 67_**

Le 5° de l'article 6, les articles 8, 9, **~~22, les 1° et 3° du I de l'article 25, les articles~~** 32, et 39, le I de l'article 40 et les articles 68 à 70 ne s'appliquent pas aux traitements de données à caractère personnel mis en œuvre aux seules fins :

1° D'expression littéraire et artistique ;

2° D'exercice, à titre professionnel, de l'activité de journaliste, dans le respect des règles déontologiques de cette profession.

**~~Toutefois, pour les traitements mentionnés au 2°, la dispense de l'obligation de déclaration prévue par l'article 22 est subordonnée à la désignation par le responsable du traitement d'un correspondant à la protection des données appartenant à un organisme de la presse écrite ou audiovisuelle, chargé de tenir un registre des traitements mis en œuvre par ce responsable et d'assurer, d'une manière indépendante, l'application des dispositions de la présente loi. Cette désignation est portée à la connaissance de la Commission nationale de l'informatique et des libertés.~~**

En cas de non-respect des dispositions de la loi applicables aux traitements prévus par le présent article, le responsable du traitement est enjoint par la Commission nationale de l'informatique et des libertés de se mettre en conformité avec la loi. **~~En cas de manquement constaté à ses devoirs, le correspondant est déchargé de ses fonctions sur demande, ou après consultation, de la Commission nationale de l'informatique et des libertés.~~**

Les dispositions des alinéas précédents ne font pas obstacle à l'application des dispositions du code civil, des lois relatives à la presse écrite ou audiovisuelle et du code pénal, qui prévoient les conditions d'exercice du droit de réponse et qui préviennent, limitent, réparent et, le cas échéant, répriment les atteintes à la vie privée et à la réputation des personnes.

</td></tr>
<tr>
<td>

**Article 21 (8°)**

_[Mesures de coordination]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 70_**

Si la Commission des Communautés européennes a constaté qu'un Etat n'appartenant pas à la Communauté européenne n'assure pas un niveau de protection suffisant à l'égard d'un transfert ou d'une catégorie de transferts de données à caractère personnel, la Commission nationale de l'informatique et des libertés, saisie d'une déclaration déposée en application des articles 23 ou 24 et faisant apparaître que des données à caractère personnel seront transférées vers cet Etat, délivre le récépissé avec mention de l'interdiction de procéder au transfert des données.

Lorsqu'elle estime qu'un Etat n'appartenant pas à la Communauté européenne n'assure pas un niveau de protection suffisant à l'égard d'un transfert ou d'une catégorie de transferts de données, la Commission nationale de l'informatique et des libertés en informe sans délai la Commission des Communautés européennes. Lorsqu'elle est saisie d'une déclaration déposée en application des articles 23 ou 24 et faisant apparaître que des données à caractère personnel seront transférées vers cet Etat, la Commission nationale de l'informatique et des libertés délivre le récépissé et peut enjoindre au responsable du traitement de suspendre le transfert des données.

Si la Commission des Communautés européennes constate que l'Etat vers lequel le transfert est envisagé assure un niveau de protection suffisant, la Commission nationale de l'informatique et des libertés notifie au responsable du traitement la cessation de la suspension du transfert. Si la Commission des Communautés européennes constate que l'Etat vers lequel le transfert est envisagé n'assure pas un niveau de protection suffisant, la Commission nationale de l'informatique et des libertés notifie au responsable du traitement l'interdiction de procéder au transfert de données à caractère personnel à destination de cet Etat.

</td>
<td>

8° A l’article 70, les premier et troisième alinéas sont supprimés et au deuxième alinéa, les mots : « saisie d'une déclaration déposée en application des articles 23 ou 24 et faisant apparaître que des données à caractère personnel seront transférées vers cet Etat, la Commission nationale de l'informatique et des libertés délivre le récépissé et » sont remplacés par les mots : « consultée en application de l’article 36 du règlement (UE) 2016/679 et en cas de transfert de données à caractère personnel vers cet Etat, la Commission

</td>
<td>

**_Article 70_**

**~~Si la Commission des Communautés européennes a constaté qu'un Etat n'appartenant pas à la Communauté européenne n'assure pas un niveau de protection suffisant à l'égard d'un transfert ou d'une catégorie de transferts de données à caractère personnel, la Commission nationale de l'informatique et des libertés, saisie d'une déclaration déposée en application des articles 23 ou 24 et faisant apparaître que des données à caractère personnel seront transférées vers cet Etat, délivre le récépissé avec mention de l'interdiction de procéder au transfert des données.~~**

Lorsqu'elle estime qu'un Etat n'appartenant pas à la Communauté européenne n'assure pas un niveau de protection suffisant à l'égard d'un transfert ou d'une catégorie de transferts de données, la Commission nationale de l'informatique et des libertés en informe sans délai la Commission des Communautés européennes. Lorsqu'elle est **~~saisie d'une déclaration déposée en application des articles 23 ou 24 et faisant apparaître que des données à caractère personnel seront transférées vers cet Etat, la Commission nationale de l'informatique et des libertés délivre le récépissé et~~** **consultée en application de l’article 36 du règlement (UE) 2016/679 et en cas de transfert de données à caractère personnel vers cet Etat, la Commission** peut enjoindre au responsable du traitement de suspendre le transfert des données.

**~~Si la Commission des Communautés européennes constate que l'Etat vers lequel le transfert est envisagé assure un niveau de protection suffisant, la Commission nationale de l'informatique et des libertés notifie au responsable du traitement la cessation de la suspension du transfert. Si la Commission des Communautés européennes constate que l'Etat vers lequel le transfert est envisagé n'assure pas un niveau de protection suffisant, la Commission nationale de l'informatique et des libertés notifie au responsable du traitement l'interdiction de procéder au transfert de données à caractère personnel à destination de cet Etat.~~**

</td></tr>
<tr>
<td>

**Article 21 (9°)**

_[Mesures de coordination]_

Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

</td>
<td>

**_Article 71_**

Des décrets en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, fixent les modalités d'application de la présente loi. L'avis rendu sur les décrets relatifs à l'application du I bis de l'article 22 et du 9° du I de l'article 25 est motivé et publié.

</td>
<td>

9° La deuxième phrase de l’article 71 est supprimée.

</td>
<td>

**_Article 71_**

Des décrets en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, fixent les modalités d'application de la présente loi. **~~L'avis rendu sur les décrets relatifs à l'application du I bis de l'article 22 et du 9° du I de l'article 25 est motivé et publié~~**.

</td></tr>
<tr>
<td>

**Article 22**

_[Open data des traitements ayant fait l’objet de formalités]_

</td>
<td>

.

</td>
<td>

Pour les traitements ayant fait l’objet de formalités antérieurement à l’entrée en vigueur de la présente loi, la liste mentionnée à l’article 31 de la loi n° 78-17 précitée, arrêtée à cette date, est mise à la disposition du public, dans un format ouvert et aisément réutilisable pour une durée de dix ans.

</td>
<td>

</td></tr>
<tr>
<td>

**Article 23 (1°)**

_[Disposition relative aux TAJ]_

Code de procédure pénale

</td>
<td>

**_Article 230-8_**

Le traitement des données à caractère personnel est opéré sous le contrôle du procureur de la République territorialement compétent qui demande qu'elles soient effacées, complétées ou rectifiées, notamment en cas de requalification judiciaire. La rectification pour requalification judiciaire est de droit. Le procureur de la République se prononce sur les suites qu'il convient de donner aux demandes d'effacement ou de rectification dans un délai d'un mois. En cas de décision de relaxe ou d'acquittement devenue définitive, les données personnelles concernant les personnes mises en cause sont effacées, sauf si le procureur de la République en prescrit le maintien, auquel cas elle fait l'objet d'une mention. Lorsque le procureur de la République prescrit le maintien des données personnelles relatives à une personne ayant bénéficié d'une décision d'acquittement ou de relaxe devenue définitive, il en avise la personne concernée. Les décisions de non-lieu et de classement sans suite font l'objet d'une mention, sauf si le procureur de la République ordonne l'effacement des données personnelles. Lorsqu'une décision fait l'objet d'une mention, les données relatives à la personne concernée ne peuvent faire l'objet d'une consultation dans le cadre des enquêtes administratives prévues aux articles L. 114-1, L. 234-1 à L. 234-3 du code de la sécurité intérieure et à l'article 17-1 de la loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité. Les décisions du procureur de la République prévues au présent alinéa ordonnant le maintien ou l'effacement des données personnelles sont prises pour des raisons liées à la finalité du fichier au regard de la nature ou des circonstances de commission de l'infraction ou de la personnalité de l'intéressé.

Les décisions d'effacement ou de rectification des informations nominatives prises par le procureur de la République sont portées à la connaissance des responsables de tous les traitements automatisés pour lesquels, sous réserve des règles d'effacement ou de rectification qui leur sont propres, ces mesures ont des conséquences sur la durée de conservation des données personnelles.

Les décisions du procureur de la République en matière d'effacement ou de rectification des données personnelles sont susceptibles de recours devant le président de la chambre de l'instruction.

Le procureur de la République dispose pour l'exercice de ses fonctions d'un accès direct aux traitements automatisés de données à caractère personnel mentionnés à l'article 230-6.

</td>
<td>

L’article 230-8 du code de procédure pénale est ainsi modifié :

1° Le premier alinéa est remplacé par les dispositions suivantes :

« Le traitement des données à caractère personnel est opéré sous le contrôle du procureur de la République territorialement compétent qui, d’office ou à la demande de la personne concernée, demande qu'elles soient effacées, complétées ou rectifiées, notamment en cas de requalification judiciaire, ou qu’elles fassent l’objet d’une mention. La rectification pour requalification judiciaire est de droit. Le procureur de la République se prononce dans un délai de deux mois sur les suites qu'il convient de donner aux demandes qui lui sont adressées. La personne concernée peut former cette demande sans délai à la suite d’une décision devenue définitive de relaxe, d'acquittement, de condamnation avec dispense de peine ou dispense de mention au casier judiciaire, ou de non-lieu, ou décision de classement sans suite. Dans les autres cas, la personne ne peut former sa demande, à peine d’irrecevabilité, que lorsque ne figure plus aucune mention dans le bulletin n° 2 de son casier judiciaire. En cas de décision de relaxe ou d'acquittement, les données personnelles concernant les personnes mises en cause sont effacées, sauf si le procureur de la République en prescrit le maintien, auquel cas elle fait l'objet d'une mention. Lorsque le procureur de la République prescrit le maintien des données personnelles relatives à une personne ayant bénéficié d'une décision d'acquittement ou de relaxe, il en avise la personne concernée. Les décisions de non-lieu ou de classement sans suite, font l'objet d'une mention, sauf si le procureur de la République ordonne l'effacement des données personnelles. Lorsqu'une décision fait l'objet d'une mention, les données relatives à la personne concernée ne peuvent faire l'objet d'une consultation dans le cadre des enquêtes administratives prévues aux articles L. 114-1, L. 234-1 à L. 234-3 du code de la sécurité intérieure et à l'article 17-1 de la loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité. Les décisions du procureur de la République prévues au présent alinéa ordonnant le maintien ou l'effacement des données personnelles ou ordonnant qu’elles fassent l’objet d’une mention sont prises pour des raisons liées à la finalité du fichier au regard de la nature ou des circonstances de commission de l'infraction ou de la personnalité de l'intéressé. » ;

2° Au troisième alinéa, les mots : « en matière d'effacement ou de rectification des données personnelles » sont supprimés.

II. – Le premier alinéa de l'article 804 du même code est ainsi rédigé :

« Le présent code est applicable, dans sa rédaction résultant de loi n° xxx du xxx d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, en Nouvelle-Calédonie, en Polynésie française et dans les îles Wallis et Futuna, sous réserve des adaptations prévues au présent titre et aux seules exceptions : ».

</td>
<td>

**_Article 230-8_**

**~~Le traitement des données à caractère personnel est opéré sous le contrôle du procureur de la République territorialement compétent qui demande qu'elles soient effacées, complétées ou rectifiées, notamment en cas de requalification judiciaire. La rectification pour requalification judiciaire est de droit. Le procureur de la République se prononce sur les suites qu'il convient de donner aux demandes d'effacement ou de rectification dans un délai d'un mois. En cas de décision de relaxe ou d'acquittement devenue définitive, les données personnelles concernant les personnes mises en cause sont effacées, sauf si le procureur de la République en prescrit le maintien, auquel cas elle fait l'objet d'une mention. Lorsque le procureur de la République prescrit le maintien des données personnelles relatives à une personne ayant bénéficié d'une décision d'acquittement ou de relaxe devenue définitive, il en avise la personne concernée. Les décisions de non-lieu et de classement sans suite font l'objet d'une mention, sauf si le procureur de la République ordonne l'effacement des données personnelles. Lorsqu'une décision fait l'objet d'une mention, les données relatives à la personne concernée ne peuvent faire l'objet d'une consultation dans le cadre des enquêtes administratives prévues aux articles L. 114-1, L. 234-1 à L. 234-3 du code de la sécurité intérieure et à l'article 17-1 de la loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité. Les décisions du procureur de la République prévues au présent alinéa ordonnant le maintien ou l'effacement des données personnelles sont prises pour des raisons liées à la finalité du fichier au regard de la nature ou des circonstances de commission de l'infraction ou de la personnalité de l'intéressé.~~**

**Le traitement des données à caractère personnel est opéré sous le contrôle du procureur de la République territorialement compétent qui, d’office ou à la demande de la personne concernée, demande qu'elles soient effacées, complétées ou rectifiées, notamment en cas de requalification judiciaire, ou qu’elles fassent l’objet d’une mention. La rectification pour requalification judiciaire est de droit. Le procureur de la République se prononce dans un délai de deux mois sur les suites qu'il convient de donner aux demandes qui lui sont adressées. La personne concernée peut former cette demande sans délai à la suite d’une décision devenue définitive de relaxe, d'acquittement, de condamnation avec dispense de peine ou dispense de mention au casier judiciaire, ou de non-lieu, ou décision de classement sans suite. Dans les autres cas, la personne ne peut former sa demande, à peine d’irrecevabilité, que lorsque ne figure plus aucune mention dans le bulletin n° 2 de son casier judiciaire. En cas de décision de relaxe ou d'acquittement, les données personnelles concernant les personnes mises en cause sont effacées, sauf si le procureur de la République en prescrit le maintien, auquel cas elle fait l'objet d'une mention. Lorsque le procureur de la République prescrit le maintien des données personnelles relatives à une personne ayant bénéficié d'une décision d'acquittement ou de relaxe, il en avise la personne concernée. Les décisions de non-lieu ou de classement sans suite, font l'objet d'une mention, sauf si le procureur de la République ordonne l'effacement des données personnelles. Lorsqu'une décision fait l'objet d'une mention, les données relatives à la personne concernée ne peuvent faire l'objet d'une consultation dans le cadre des enquêtes administratives prévues aux articles L. 114-1, L. 234-1 à L. 234-3 du code de la sécurité intérieure et à l'article 17-1 de la loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité. Les décisions du procureur de la République prévues au présent alinéa ordonnant le maintien ou l'effacement des données personnelles ou ordonnant qu’elles fassent l’objet d’une mention sont prises pour des raisons liées à la finalité du fichier au regard de la nature ou des circonstances de commission de l'infraction ou de la personnalité de l'intéressé.**

Les décisions d'effacement ou de rectification des informations nominatives prises par le procureur de la République sont portées à la connaissance des responsables de tous les traitements automatisés pour lesquels, sous réserve des règles d'effacement ou de rectification qui leur sont propres, ces mesures ont des conséquences sur la durée de conservation des données personnelles.

Les décisions du procureur de la République **~~en matière d'effacement ou de rectification des données personnelles~~** sont susceptibles de recours devant le président de la chambre de l'instruction.

Le procureur de la République dispose pour l'exercice de ses fonctions d'un accès direct aux traitements automatisés de données à caractère personnel mentionnés à l'article 230-6.

</td></tr>
<tr>
<td>

**Article 23 (2°)**

_[Disposition relative aux TAJ]_

Code de procédure pénale

</td>
<td>

**_Article 804_**

Le présent code est applicable, dans sa rédaction résultant de la loi n° 2017-1510 du 30 octobre 2017 renforçant la sécurité intérieure et la lutte contre le terrorisme, en Nouvelle-Calédonie, en Polynésie française et dans les îles Wallis et Futuna, sous réserve des adaptations prévues au présent titre et aux seules exceptions :

1° Pour la Nouvelle-Calédonie et la Polynésie française, du cinquième alinéa de l'article 398 et des articles 529-3 à 529-6 ;

2° Pour les îles Wallis et Futuna, des articles 52-1,83-1 et 83-2, du cinquième alinéa de l'article 398 et des articles 529-3 à 529-6.

</td>
<td>

II. – Le premier alinéa de l'article 804 du même code est ainsi rédigé :

« Le présent code est applicable, dans sa rédaction résultant de loi n° xxx du xxx d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, en Nouvelle-Calédonie, en Polynésie française et dans les îles Wallis et Futuna, sous réserve des adaptations prévues au présent titre et aux seules exceptions : ».

</td>
<td>

**_Article 804_**

**~~Le présent code est applicable, dans sa rédaction résultant de la loi n° 2017-1510 du 30 octobre 2017 renforçant la sécurité intérieure et la lutte contre le terrorisme, en Nouvelle-Calédonie, en Polynésie française et dans les îles Wallis et Futuna, sous réserve des adaptations prévues au présent titre et aux seules exceptions :~~**

**Le présent code est applicable, dans sa rédaction résultant de loi n° xxx du xxx d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, en Nouvelle-Calédonie, en Polynésie française et dans les îles Wallis et Futuna, sous réserve des adaptations prévues au présent titre et aux seules exceptions :**

1° Pour la Nouvelle-Calédonie et la Polynésie française, du cinquième alinéa de l'article 398 et des articles 529-3 à 529-6 ;

2° Pour les îles Wallis et Futuna, des articles 52-1,83-1 et 83-2, du cinquième alinéa de l'article 398 et des articles 529-3 à 529-6.

</td></tr>
<tr>
<td>

**Article 24**

_[Dispositions d’entrée en vigueur de la présente loi]_

</td>
<td>

</td>
<td>

Les titres Ier à III, et les articles 21 et 22 de la présente loi entrent en vigueur à compter du 25 mai 2018.

Toutefois, les dispositions de l’article 70-15 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés dans leur rédaction résultant de l’article 19 de la présente loi et relatives à l’obligation de journalisation pourront entrer en vigueur à une date ultérieure ne pouvant excéder le 6 mai 2023 lorsqu’une telle obligation exigerait des efforts disproportionnés, et ne pouvant excéder le 6 mai 2026 lorsque, à défaut d’un tel report, il en résulterait de graves difficultés pour le fonctionnement du système de traitement automatisé. La liste des traitements concernés par ces reports et les dates auxquelles, pour ces traitements, l’entrée en vigueur de cette obligation sera reportée seront déterminées par voie réglementaire.

</td>
<td>

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/annexe" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <span> </span> </div>

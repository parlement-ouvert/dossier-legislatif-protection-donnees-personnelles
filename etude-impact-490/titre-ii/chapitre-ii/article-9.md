<!-- TITLE: ARTICLE 9 -->
<!-- SUBTITLE: ALLEGEMENT DES FORMALITES PREALABLES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

 ** _1\. Etat des lieux et diagnostic_**

 ** _1.1. Etat des lieux_**

La [loi n° 78-17 du 6 janvier 1978](/loi-78-17) relative à l'informatique, aux fichiers et aux libertés consacre un chapitre aux formalités préalables à la mise en œuvre des traitements (Chapitre IV, articles 22 à 31) qui donne un rôle central à la Commission nationale de l’informatique et des libertés.

Pour être mis en œuvre, les traitements doivent soit être déclarés à la Commission nationale de l’informatique et des libertés ([article 22](/loi-78-17/chapitre-iv/article-22)) soit autorisés par celle-ci ([article 25](/loi-78-17/chapitre-iv/section-2/article-25)) soit encore autorisés par arrêté ou décret en Conseil d’Etat après avis de la Commission (article [26](/loi-78-17/chapitre-iv/section-2/article-26) et [27](/loi-78-17/chapitre-iv/section-2/article-27)).

Cette gradation dans les formalités préalables tient compte à la fois de la finalité du traitement, de sa sensibilité en termes de type de données traitées et de l'identité de son responsable.

Exemples de traitements soumis aux formalités prévues par les articles 25 à 27 de la loi n° 78-17 :

<table>
<tr>
<td>

**Loi n° 78-17 actuelle**

</td>
<td>

**Exemples**

</td></tr>
<tr>
<td>

**[Article 25](/loi-78-17/chapitre-iv/section-2/article-25)**

</td>
<td>

</td></tr>
<tr>
<td>

I. - Sont mis en œuvre après autorisation de la Commission nationale de l'informatique et des libertés, à l'exclusion de ceux qui sont mentionnés aux articles [26](/chapitre-iv/section-2/article-26) et [27](/loi-78-17/chapitre-iv/section-2/article-27) :

</td>
<td>

</td></tr>
<tr>
<td>

1° Les traitements, automatisés ou non, mentionnés au 7° du II [statistiques INSEE], au III [données rapidement anonymisées] et au IV [intérêt public] de l'[article 8](/loi-78-17/chapitre-ii/section-2/article-8) ;

</td>
<td>

\- _Délibération n° 2008-005_. Il s’agit du _décret 2016-1930_ du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés (en son article 2 : fichiers non publiés).

</td></tr>
<tr>
<td>

IV. - Pour l'application du présent article, les traitements qui répondent à une même finalité, portent sur des catégories de données identiques et ont les mêmes destinataires ou catégories de destinataires peuvent être autorisés par un acte réglementaire unique. Dans ce cas, le responsable de chaque traitement adresse à la commission un engagement de conformité de celui-ci à la description figurant dans l'autorisation.

</td>
<td>

\- _Arrêté du 2 mars 2007_ ne sont pas concernés par les modifications induites par le règlement. Ainsi, demeureront autorisés par arrêté du ou des ministres compétents, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés, les traitements de données à caractère personnel mis en œuvre pour le compte de l'Etat qui intéressent la sûreté de l'Etat, la défense ou la sécurité publique ou qui ont pour objet la prévention, la recherche, la constatation ou la poursuite des infractions pénales ou l'exécution des condamnations pénales ou des mesures de sûreté.

**_3\. Options_**

 **3.1. Suppression du régime déclaratoire de l’[article 22](/loi-78-17/chapitre-iv/article-22)**

 **3.1.1. Option 1 (écartée) : Maintien d’un tel régime**

Ce régime déclaratif qui constitue actuellement la formalité préalable de droit commun, n’apparait plus compatible avec le règlement.

En outre, ainsi qu’a pu le souligner le rapport du Conseil d’Etat sur le numérique et les droits fondamentaux, ce système déclaratif s’expose à différents reproches :

« _Il conduit à transmettre à la CNIL une masse considérable d’informations, d’inégal intérêt en raison des enjeux très variables pour la protection des données personnelles. Si l’obligation de déclaration est assez simple à respecter pour des entreprises ne mettant en œuvre que des traitements courants (par exemple, un fichier du personnel et un fichier des clients), elle peut s’avérer plus lourde pour des acteurs, notamment ceux de l’économie numérique, qui ont une activité intense et évolutive de traitement des données. Les « startups», en particulier, sont susceptibles de voir évoluer au cours de leurs premières années leur modèle d’affaires et, corrélativement, leurs modalités d’utilisation des données personnelles ; en raison de leur petite taille, elles peuvent avoir plus de difficulté à appréhender leurs obligations de déclaration et sont ainsi exposées à une certaine insécurité juridique. Pour autant, l’obligation de déclaration ne met pas à disposition de la CNIL toutes les informations nécessaires pour évaluer le degré de sensibilité d’un traitement ; en particulier, la CNIL n’est informée ni du nombre de personnes dont les données sont traitées, ni du volume des données transmises à des tiers_ »68.

 **3.1.2. Option 2 (écartée) : Passage d’un régime de déclaration à celui d’une autorisation de certains traitements**

Il aurait pu être envisagé que certains traitements contenant le numéro d'inscription des personnes au répertoire national d'identification (NIR) et soumis actuellement à un régime de déclaration soient basculés dans un régime plus contraignant, à savoir celui de la consultation préalable obligatoire de l’autorité de contrôle.

Cette option est contraire à la logique du règlement.

 **3.1.3. Option 3 (retenue) : Remplacement des dispositions relatives à la déclaration et abrogation des articles [23](/loi-78-17/chapitre-iv/section-1/article-23) et [24](/loi-78-17/chapitre-iv/section-1/article-24) de la loi de 1978**

Le projet de loi abroge le régime déclaratif prévu aux articles 22 à 24 de la loi n° 78-17. Par mesure de lisibilité, l’[article 9 du projet de loi](/pjl-490/titre-ii/chapitre-ii/article-9) rappelle le principe de la consultation préalable qui existe dans le règlement : « _Conformément à l’[article 36](/reglement-2016-679/chapitre-iv/section-3/article-36) du règlement (UE) 2016/679, le responsable du traitement consulte la Commission nationale de l’informatique et des libertés préalablement au traitement lorsqu'une analyse d'impact relative à la protection des données effectuée au titre de l'[article 35](/reglement-2016-679/chapitre-iv/section-3/article-35) dudit règlement indique que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque_ ».

Il est en effet important de rappeler cette notion de « risque élevé » d’autant que le projet de loi supprime les autorisations de l’article 25 et la plupart de celles de l’article 27 de la loi n° 78-17 (cf. infra mesure 3.2).

 **3.2. Suppression des régimes d’autorisation**

L’article 25 de la loi n°78-17 prévoit des autorisations préalables de la Commission nationale de l’informatique et des libertés, à l’exclusion des traitements mentionnés aux articles 26 et 27.

**3.2.1. Option 1 (écartée) : Maintenir en l’état l’ensemble des régimes d’autorisation**

Le règlement ne prévoit pas de possibilité pour les Etats membres de conserver un régime d’autorisation, sauf dans l’hypothèse de l’article 36.5 du règlement ou au titre des conditions supplémentaires pour les données sensibles (art. 9), des garanties complémentaires pour les données d’infraction (art. 10) et des conditions spécifiques du traitement du numéro d’identification national (article 87).

Cette option consistant au maintien d’un régime général d’autorisation auprès de la Commission nationale de l’informatique et des libertés n’a donc pas été retenue.

 ** _3.2.2. Option 2 (retenue) : Suppression de la plupart des autorisations (articles 25 et 27 )_**

Le règlement n’oblige pas d’abroger l’ensemble des régimes d’autorisation existants. Le projet de loi épouse la logique du règlement en évitant des obligations supplémentaires inutiles. Toutefois, au regard de l’enjeu de la protection des données, le projet de loi conserve un régime d’autorisation préalable pour deux types de données.

En premier lieu, les traitements mis en œuvre pour le compte de l’Etat qui portent sur des génétiques, ou des données biométriques nécessaires à l'authentification ou au contrôle de l'identité des personnes conservent un régime d’autorisation par décret après avis de la Commission nationale de l’informatique et des libertés.

En effet, les données génétiques constituent des données sensibles en vertu de l’article 9 du règlement. Il s’agit de données « relatives aux caractéristiques génétiques héréditaires ou acquises d'une personne physique qui donnent des informations uniques sur la physiologie ou l'état de santé de cette personne physique et qui résultent, notamment, d'une analyse d'un échantillon biologique de la personne physique en question » ([art. 4](/reglement-2016-679/chapitre-i/article-4) du règlement).

De telles données peuvent indiquer des prédispositions génétiques concernant des individus et à ce que ces données peuvent être utilisées pour d’autres finalités. Ce type de données ne relève pas uniquement des traitements dans le domaine de la santé (test génétique) qui fait l’objet d’un régime particulier (nouveau chapitre IX), mais peut également être utilisées à des fins d’identification d’une personne (empreintes génétiques), de son ascendance ou descendance (test de paternité) ou de recherche scientifique (étude génétique de populations)69.

De même, la donnée biométrique n’est pas une donnée d’identité comme les autres : elle n’est pas attribuée par un tiers ou choisie par la personne, mais elle est produite par le corps lui-même et le désigne de façon définitive (caractère non révocable). Dans ces conditions, confier ses données biométriques à un tiers n’est pas un acte anodin. La Commission nationale de l’informatique et des libertés est à cet égard très vigilante sur les risques liés à l’usage de la biométrie, en particulier dans le cadre des technologies biométriques « à traces » telles que l’empreinte digitale, dont l’usage, en cas de conservation dans une base centralisée, peut être détourné : usurpation d’identité, violation des données, détournement de leur finalité70. Ce risque est d’autant plus important en cas d’interconnexion de fichiers : compte tenu de l’interopérabilité croissante des technologies d’identification biométrique, le stockage centralisé des données biométriques augmente le risque que des données biométriques soient utilisées pour relier différentes base entre elles71.

Toutefois, le projet de loi prévoit de maintenir le régime d’autorisation à l’égard de ce type de traitement, uniquement lorsqu’ils sont mis en œuvre par l’Etat agissant dans l’exercice de ses prérogatives de puissance publique. Lorsque l’Etat agit comme toute autre personne ou organisme (contrôle d’accès aux locaux de ses services par exemple, comme le ferait tout employeur ou toute entreprise), une autorisation par décret en Conseil d’Etat ne serait plus exigée.

En second lieu, les traitements qui mettent en œuvre le Numéro d'Inscription au Répertoire (NIR) par des personnes publiques ou privées, conservent une formalité préalable.

En effet, le NIR constitue un numéro particulièrement signifiant en ce qu’il comporte 13 caractères permettant de déterminer le sexe, l'année et le mois de naissance, et dans la majorité des cas, le département et la commune de naissance en France ou l'indication d'une naissance à l'étranger et enfin le numéro d'ordre qui permet de distinguer les personnes nées au même lieu à la même période. Il s’agit d’un numéro unique et pérenne, qui est susceptible de faire l’objet d’interconnexion dans différents fichiers sociaux, fiscaux…. De très nombreux traitements d’organismes de sécurité sociale (DSS, CNAMTS, CNAV, MDPH…) mais également d’autres organismes utilisent le NIR ainsi que des acteurs privés pour la gestion de la paie, des déclarations sociales, des déclarations d'embauche.

La CNIL contrôle de manière très stricte la pertinence de l’utilisation du NIR. A titre d’exemple, la commission a refusé la création d’un traitement par une société financière spécialisée dans le crédit à la consommation qui souhaitait utiliser le NIR à des fins de détection d’incohérences dans les demandes de crédit72 (en l’espèce, la société souhaitait pouvoir identifier les fausses fiches de paie en généralisant automatiquement le numéro de sécurité sociale à partir des données de d’état civil connues du demandeur de crédit et en comparant de manière automatisée le numéro ainsi généré avec celui figurant sur les feuilles de paie).

Le projet de loi prévoit toutefois un allègement des formalités liées à la mise en œuvre de traitements mettant en œuvre le NIR. Il propose ainsi de prévoir un décret cadre, pris après avis motivé et publié de la commission, pour autoriser l'utilisation du NIR par catégorie de responsables de traitement et pour des finalités précises ** _,_** dans la même logique que ce prévoyait la loi n° 78-1773 , avant sa modification par la [loi n° 2004-801 du 6 août 2004](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000441676)74.

Cette procédure d’autorisation de traitements utilisant le NIR ne sera pas applicable aux traitements bénéficiant d’ores et déjà d’une absence de formalité ou de formalités allégées75 dès lors que le numéro d'inscription au répertoire national d'identification des personnes physiques fait l'objet préalablement d'une opération cryptographique lui substituant un code statistique non signifiant. Il s’agit des traitements qui ont exclusivement des finalités de statistique publique, de recherche scientifique ou historique ou qui mettent à la disposition des usagers de l'administration un ou plusieurs téléservices de l'administration électronique.

Le régime de décret-cadre sera en revanche applicable aux traitements portant sur les alertes sanitaires lorsqu’ils portent sur NIR (cf. infra,).

En dernier lieu, les autres autorisations prévues aux articles 25 et 27 de la loi n° 78-17 sont abrogées. Ces traitements seront soumis au droit commun prévu par le règlement, à savoir, une consultation de la CNIL préalablement au traitement lorsqu’une analyse d’impact révèle que le traitement présenterait un risque élevé si le responsable ne prend pas les mesures pour atténuer ce risque(article 36). ****

**3.3. Dispositions relatives à l’analyse d’impact**

 **3.3.1. Option 1 (écartée) : Apporter une précision**

L’[article 35](/reglement-2016-679/chapitre-iv/section-3/article-35) du règlement est d’application directe. Par conséquent le responsable du traitement effectue préalablement à la mise en œuvre du traitement, une analyse d’impact relative à la protection des données. Toutefois, le 10 de l’article 35 du règlement prévoit une marge de manœuvre qu’il serait possible de réserver.

En effet, lorsque le traitement effectué « _est nécessaire au respect d'une obligation légale à laquelle le responsable du traitement est soumis »_ \- ou _à « l'exécution d'une mission d'intérêt public ou relevant de l'exercice de l'autorité publique dont est investi le responsable du traitement »_ , a une base juridique nationale, que ce droit règlemente l'opération de traitement spécifique ou l'ensemble des opérations de traitement en question et qu'une analyse d'impact relative à la protection des données a déjà été effectuée dans le cadre d'une analyse d'impact générale réalisée dans le cadre de l'adoption de la base juridique en question, les paragraphes 1 à 7 (de l’article 35) ne s'appliquent pas, à moins que les États membres n'estiment qu'il est nécessaire d'effectuer une telle analyse avant les activités de traitement.

Il serait possible de renvoyer à un décret en Conseil d’Etat après avis de la Commission nationale de l’informatique et des libertés pour préciser la liste des traitements spécifiques et ensembles d’opérations de traitement pour lesquels une analyse d’impact est obligatoire.

Or ce renvoi ne semble ni obligatoire ni utile. Par ailleurs, il ne semble pas possible de prévoir en amont cette liste de traitements spécifiques.

**3.3.2. Option 2 (retenue) : Ne rien mentionner**

Le mécanisme mis en place par l’article 35.10 du règlement semble être le suivant : soit la norme qui met en place un traitement effectué en application des [articles 6](/reglement-2016-679/chapitre-ii/article-6)(1) c) et e) n’a pas fait l’objet d’une analyse d’impact, alors une telle analyse d’impact sera obligatoire avant les activités de traitement ; soit la norme a fait l’objet d’une telle analyse d’impact et par principe, les traitements ne sont pas soumis à une telle analyse sauf – et c’est là la marge de manœuvre – le droit de l’Etat membre l’indique de façon précise pour tel ou tel traitement.

Il semble plus pertinent, dans la logique du règlement (UE) 2016/679, de prévoir que c’est lorsque la norme règlemente le traitement que le législateur ou le pouvoir règlementaire doit s’interroger sur la nécessité ou non de prévoir une nouvelle analyse d’impact.

Par conséquent, la modification de la loi n° 78-17 ne semble pas être l’option pertinente76.

**_4\. Analyse des impacts de la disposition envisagée_**

 ** _4.1. Impacts juridiques_**

Si le projet de loi supprime la plupart des formalités préalables lorsque les traitements relèvent du champ d’application du règlement (UE) 2016/679, il crée un régime d’autorisation allégé en ce qui concerne les traitements utilisant le Numéro d'Inscription au Répertoire (NIR).

**_4.2. Impacts sur les services judiciaires_**

L’allégement des formalités préalables est de nature à réduire le risque contentieux lié au refus d’autorisation de traitement de données par la Commission nationale de l’informatique et des libertés. L’impact pour les services judiciaires devrait toutefois être minime dans la mesure où le nombre de contentieux en la matière est marginal (à titre d’exemple, la Commission a opposé 9 refus à des demandes d’autorisation en matière de biométrie sur les 316 demandes reçues en 2016).

 ** _4.3. Impacts sur les collectivités territoriales_**

Les collectivités territoriales traitent chaque jour de nombreuses données à caractère personnel, que ce soit pour assurer la gestion administrative de leur structure (fichiers de ressources humaines), la sécurisation de leurs locaux (contrôle d’accès par badge, vidéosurveillance) ou la gestion des différents services publics et activités dont elles ont la charge.

Les collectivités territoriales sont également concernées par l’allègement des formalités préalables, s’agissant en particulier de la suppression des déclarations auprès de la Commission nationale de l’informatique et des libertés et du régime d’autorisation par arrêté des traitements destinés à mettre à la disposition des usagers de l'administration un ou plusieurs téléservices de l'administration électronique, ou de l’assouplissement du régime d’autorisation des traitements utilisant le Numéro d'Inscription au Répertoire (NIR) par un décret-cadre.

En tant que responsables de traitement, les collectivités territoriales sont toutefois soumises à l’obligation de tenir un registre des activités de traitement effectuées sous leur responsabilité. A l’instar des petites et moyennes entreprises (cf. infra), le règlement tient compte toutefois de la situation des petites collectivités territoriales en prévoyant que sont dispensées d’une telle obligation les organisations comptant moins de 250 employés, sauf si le traitement est susceptible de comporter un risque pour les droits et libertés des personnes concernées, s'il n'est pas occasionnel ou s’il porte sur des données sensibles (article 30.5).

 ** _4.4. Impact sur les entreprises_**

Actuellement, le régime déclaratif présente une certaine lourdeur : la déclaration doit obligatoirement comporter les informations qui conditionnent l'appréciation de la licéité du traitement, notamment l’identité du responsable de traitement, la finalité du traitement, les données collectées, les destinataires, les éventuels transferts des données vers des pays non membres de l'Union européenne et la durée de conservation.

En outre, dans la mesure où la déclaration constitue une formalité préalable obligatoire, cela emporte des conséquences juridiques importantes. Ainsi, dès lors qu'un fichier client informatisé, n'ayant pas été déclaré, n'est pas dans le commerce, sa vente à un tiers a un objet illicite77 .

Le projet de loi allège les formalités préalables auxquelles sont actuellement soumises les entreprises lorsqu’elles souhaitent mettre en œuvre un traitement.

Le responsable de traitement n’a plus d’obligation de déclarer les traitements qu’il souhaite mettre en œuvre ou solliciter l’autorisation préalable de la Commission nationale de l’informatique et des libertés, sauf en cas de risque élevé.

En revanche, les entreprises, en tant que responsables de traitement ou sous-traitants, resteront soumises aux obligations prévues par le règlement (tenue d’un registre d’activités des traitements78, conduite d’une analyse d’impact,…). La situation des petites et moyennes entreprises est toutefois prise en compte. Ainsi, le [considérant 13 du règlement](/reglement-2016-679/considerants/considerant-13) précise que : « _Pour tenir compte de la situation particulière des micro, petites et moyennes entreprises, le présent règlement comporte une dérogation pour les organisations occupant moins de 250 employés en ce qui concerne la tenue de registres. Les institutions et organes de l'Union, et les États membres et leurs autorités de contrôle sont en outre encouragés à prendre en considération les besoins spécifiques des micro, petites et moyennes entreprises dans le cadre de l'application du présent règlement._ »79.

En cas de non-respect des obligations prévues par le règlement ou la loi, les entreprises s’exposent à des amendes pouvant aller jusqu’à 10 millions d’euros ou 2% du chiffre d’affaires annuel mondial total de l’exercice précédent (20 millions d’euros ou 4% du chiffre d’affaires en cas de réitération du manquement80.

Selon le syndicat professionnel du numérique (Syntec Numérique), les projets de mise en conformité au règlement européen sur la protection des données vont tirer le secteur de l'édition logicielle et du conseil informatique. Ce syndicat anticipe en 2018 une croissance du chiffre d'affaires du secteur de 3,6%, par rapport à 2017, en partie liée aux budgets engagés par leurs clients pour préparer leur mise en conformité au règlement81.

 ** _5\. Consultations et modalités d’application_**

 ** _5.1. Consultations_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

Le Conseil national d’évaluation des normes a également été consulté, en application de l’[article L. 1212-2 du code général des collectivités territoriales](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019983190&cidTexte=LEGITEXT000006070633) et a rendu un avis favorable lors de sa séance du 30 novembre 2017.

**_5.2. modalités d’application_**

Les textes réglementaires suivants devront être pris sur le fondement de la loi :

<table>
<tr>
<td>

**Articles du projet de loi renvoyant à des mesures réglementaires**

</td>
<td>

**Nature du texte réglementaire**

</td>
<td>

**Objet du texte réglementaire**

</td></tr>
<tr>
<td>

Article 9 (I, 2° alinéa)

</td>
<td>

Décret en Conseil d’Etat, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés

</td>
<td>

Détermination des catégories de responsables de traitement et des finalités des traitements au vu desquelles ces derniers peuvent être mis en œuvre lorsqu’ils portent sur des données comportant le NIR

</td></tr>
<tr>
<td>

Article 9 (I, 7° alinéa

</td>
<td>

Décret en Conseil d’Etat, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés

</td>
<td>

Détermination de la fréquence de l’opération cryptographique substituant un code statistique non signifiant au NIR

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

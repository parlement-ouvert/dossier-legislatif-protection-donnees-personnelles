<!-- TITLE: ARTICLE 14 -->
<!-- SUBTITLE: DECISION ADMINISTRATIVE AUTOMATISEE -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**_1\. Etat des lieux et diagnostic_**

 ** _1.1. Etat des lieux_**

La [loi n° 78-17 du 6 janvier 1978](/loi-78-17) a instauré un régime de protection des personnes à l'égard des décisions prises sur le fondement d'un traitement automatisé de données à caractère personnel (articles [10](/loi-78-17/chapitre-ii/section-2/article-10) et [39](/loi-78-17/chapitre-v/section-2/article-39)). Ce régime de protection inscrit dans la loi, à la suite du rapport Tricot de 1975 qui insistait sur « _la nécessité de ne compter sur l'analyse d'un système que comme un instrument de travail parmi d'autres et de ne s'en remettre jamais à ses seules conclusions_ », assure la garantie d’une informatique humaine.

La loi proscrit ainsi le profilage automatique pour les décisions de justice. Cet usage est encadré pour les autres décisions produisant des effets juridiques. Ainsi, lorsqu’une décision implique une appréciation sur un comportement humain fondée sur un algorithme établissant le profil d’un individu la personne peut obtenir que l’évaluation soit vérifiée ou contredite par une intervention humaine.

La Commission nationale de l’informatique et des libertés, au travers de ses avis et mises en demeure, a établi une doctrine concernant l’article 10 de la loi n° 78-17. Elle ne prohibe pas l’utilisation d’algorithmes dans la prise de décision, notamment par les administrations, dès lors qu’ils ne sont qu’un simple outil d’aide à la décision : « _compte tenu des enjeux éthiques qu’ils soulèvent, le législateur a prévu que l’utilisation des algorithmes ne pouvait exclure toute intervention humaine_ ». Le recours à des algorithmes « _ne [doit] constitu[er] qu'un outil d'aide et d'orientation des travaux des agents_ » (délibération n° 2017-226 du 20 juillet 2017).

Dans la mesure où l'Académie française définit un algorithme comme étant une " _méthode de calcul qui indique la démarche à suivre pour résoudre une série de problèmes équivalents en appliquant dans un ordre précis une suite finie de règle_ s", les algorithmes peuvent être très divers : une feuille de calcul (type calcul des impôts) ou de _scoring_ (par exemple en matière de logement social). La complexité est encore plus grande si l'on considère que la notion juridique de « décision automatisée » est aussi très large.

En complément de la loi du 6 janvier 1978, la [loi n° 2016-1321 du 7 octobre 2016](https://www.legifrance.gouv.fr/eli/loi/2016/10/7/ECFI1524250L/jo/texte) pour une République numérique a prévu des mesures assurant la transparence des décisions individuelles prises sur le fondement d'un traitement algorithmique. L'[article L. 312-1-3](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033205516&cidTexte=LEGITEXT000031366350&dateTexte=20180114) du code des relations entre le public et l'administration prévoit une obligation générale de publication en ligne des règles définissant les principaux traitements algorithmiques utilisés dans l'accomplissement de leurs missions lorsqu'ils fondent des décisions individuelles. L’[article L. 311-3-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033205535&cidTexte=LEGITEXT000031366350&dateTexte=20180114) du même code a créé, parallèlement, un régime d'information individuelle l’administration doit informer l’usager, par une mention explicite dans la décision, que ladite décision a été prise sur le fondement d'un traitement algorithmique et qu’il peut en demander les règles, y compris leur application dans son cas particulier ; les règles définissant le traitement algorithmique ainsi que les principales caractéristiques de sa mise en œuvre sont communiquées par l'administration à l'intéressé s'il en fait la demande.

L'[article R. 311-3-1-3](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000034195881&cidTexte=LEGITEXT000031366350&dateTexte=20180114 "coquille") de ce code précise les informations qui doivent être communiquées à l'intéressé s'il en fait la demande :

_« 1° Le degré et le mode de contribution du traitement algorithmique à la prise de décision ;_

 _2° Les données traitées et leurs sources ;_

 _3° Les paramètres de traitement et, le cas échéant, leur pondération, appliqués à la situation de l'intéressé ;_

 _4° Les opérations effectuées par le traitement »._

Cette logique de transparence permet de comprendre et d’auditer le traitement et, grâce aux informations obtenues au titre des droits de l'intéressé mentionnés plus haut, de rejouer les opérations effectuées par le traitement.

 ** _1.2. Cadre constitutionnel_**

Le Conseil constitutionnel s’est prononcé sur l’article 10 de la loi n° 78-17, dans une rédaction antérieure à la modification et à la renumérotation effectuées par la loi n° 2004-801 du 6 août 2004, dans sa [décision du 13 mars 2003](http://www.conseil-constitutionnel.fr/conseil-con..c/decision-n-2003-467-dc-du-13-mars-2003.855.html)96 :« _Considérant, en outre, qu'en vertu de l'[article 2](/loi-78-17/chapitre-ier/article-2) de la loi du 6 janvier 1978 susvisée, que ne remettent pas en cause les dispositions contestées : " Aucune décision administrative ou privée impliquant une appréciation sur un comportement humain ne peut avoir pour seul fondement un traitement automatisé d'informations donnant une définition du profil ou de la personnalité de l'intéressé " ; que les données recueillies dans les fichiers ne constitueront donc, dans chaque cas, qu'un élément de la décision prise, sous le contrôle du juge, par l'autorité administrative_ ».

Le commentaire de cette décision précise que « _l'intéressé, dûment prévenu de l'accès au fichier, pourra contester la décision de refus qui lui serait opposée devant le juge compétent_ » et que le Conseil d’Etat vérifie la conformité d’une telle décision à l’[article 10](/loi-78-17/chapitre-ii/section-2/article-10) de la loi n° 78-17.

Le projet de loi conserve de tels droits évoqués pour la personne concernée : celle-ci est informée de l’utilisation d’un algorithme et dispose des droits de recours, administratif (s’agissant de décisions administratives individuelles) et juridictionnel, de droit commun.

 ** _1.3. Cadre conventionnel_**

L’[article 22](/reglement-2016-679/chapitre-iii/section-4/article-22) du règlement (UE) 2016/679, reprenant largement la rédaction de la directive 95/46/CE qu’il abroge, dispose que : _« la personne concernée a le droit de ne pas faire l’objet d’une décision fondée exclusivement sur un traitement automatisé, y compris le profilage, produisant des effets juridiques la concernant ou l’affectant de manière significative »_ (point 1).

Ce droit subjectif de ne pas faire l’objet d’une décision automatisée a largement été interprété en France comme prohibant toute décision ayant un effet juridique prise automatiquement par un traitement sans intervention humaine.

Toutefois, ce droit accordé à la personne n'est pas absolu. Le point 2 de l’article 22 du règlement prévoit trois dérogations, lorsque la décision :

\- est nécessaire à la conclusion ou à l’exécution d’un contrat entre la personne concernée et un responsable du traitement (a) ;

\- est autorisée par le droit de l’Union ou le droit de l’Etat membre auquel le responsable du traitement est soumis et qui prévoit également des mesures appropriées pour la sauvegarde des droits et libertés et des intérêts légitimes de la personne concernée (b) ;

\- est fondée sur le consentement explicite de la personne concernée (c).

Dans les cas a) et c), des garanties pour la sauvegarde des droits et libertés et des intérêts légitimes de la personne concernée doivent être également apportées: intervention humaine de la part du responsable du traitement, droit d’exprimer son point de vue et de contester la décision (point 3).

A ce titre, il convient de souligner que le [considérant 71 du règlement](/reglement-2016-679/considerants/considerant-71) précise que : « _Toutefois, la prise de décision fondée sur un tel traitement [automatisé], y compris le profilage, devrait être permise lorsqu'elle est expressément autorisée par le droit de l'Union ou le droit d'un État membre auquel le responsable du traitement est soumis, y compris aux fins de contrôler et de prévenir les fraudes et l'évasion fiscale conformément aux règles, normes et recommandations des institutions de l'Union ou des organes de contrôle nationaux, et d'assurer la sécurité et la fiabilité d'un service fourni par le responsable du traitement »._

Les décisions ne peuvent être fondées sur les catégories de données prévues à l’[article 9](/reglement-2016-679/chapitre-ii/article-9) du règlement (point 4), c’est-à-dire les données sensibles (données biométriques, génétiques, de santé, ethniques, politiques, syndicales, vie sexuelle, religieuse, philosophique), sauf si la personne y consent (point a) du paragraphe 2 de l’article 9) ou que le traitement est nécessaire pour des motifs d’intérêt public important (point g) du même paragraphe).

Les « mesures appropriées pour la sauvegarde des droits et libertés » de la personne ne sont pas définies par le règlement. Le [considérant 71](/reglement-2016-679/considerants/considerant-71) apporte quelques précisions ainsi que les lignes directrices dédiées du G29 :

\- droit à l’information (logique sous-jacente, conséquences pour la personne) et à l’explication ;

\- droit au recours, y compris, le cas échéant, droit à une intervention humaine et à exprimer son point de vue.

Au-delà de ces droits subjectifs, les responsables de traitement sont invités à contrôler régulièrement pour des biais discriminants (dans la mesure, notamment, où le règlement n’interdit pas strictement le traitement de données « sensibles »).

**_1.4. Eléments de droit comparé_**

1.4.1 Au Royaume Uni, les traitements automatisés sont autorisés dans le cadre de la loi (pour la fraude fiscale par exemple). L’article 13 du projet de loi d’adaptation du droit national au règlement prévoit de manière transversale les garanties offertes en cas de décisions automatisées prises en application du b) du 2 de l’article 22 :

\- notification de la personne dans les meilleurs délais ;

\- faculté dans les 3 semaines suivantes de demander un réexamen de la situation ou la prise d’une nouvelle décision qui n’est pas fondée sur le seul algorithme ;

\- obligation dans les 3 semaines suivantes de répondre à la demande, ce qui peut impliquer la faculté pour la personne de fournir de nouvelles informations.

Cet article est conforme aux dispositions du _Data Protection Act_ de 1998 ([article 12](http://www.legislation.gov.uk/ukpga/1998/29/section/12)(2)b) qui prévoyait le réexamen plutôt que des garanties de transparence, voire un « droit à l’explication », comparable notamment à l’[article 39](/loi-78-17/chapitre-v/section-2/article-39) de la loi française du 6 janvier 1978.

1.4.2 En Allemagne, le paragraphe 6a) du _[Bundesdatenschutzgesetz](https://www.gesetze-im-internet.de/bdsg_1990/)_ reprend largement le règlement : la section (2) prévoit la possibilité de déroger par une mesure législative. Le (3) permet aux personnes concernées d’avoir connaissance du mécanisme qui fonde la décision, au sens de la logique sous-jacente.

 ** _2\. Objectifs poursuivis et nécessité de légiférer_**

 ** _2.1 Objectifs poursuivis_**

Alors que certaines décisions sont d’ores et déjà prises avec une aide algorithmique par les administrations (fraude, affectation, calcul des droits), l’objectif poursuivi est de fixer un cadre juridique protecteur des citoyens mais permettant, dans certains cas, de prendre des décisions administratives individuelles automatisées qui ne requerront plus une intervention humaine a priori et de renforcer la sécurité juridique.

Il convient de prévoir la possibilité d’un recours à une décision automatisée pour les décisions administratives individuelles, à condition que :

\- la personne soit clairement informée, par une mention explicite, indiquant la finalité du traitement algorithmique et le droit d'obtenir la communication des règles définissant ce traitement et des principales caractéristiques de sa mise en œuvre, ainsi que les modalités d'exercice de ce droit97 ;

\- pour être à même d'assurer pleinement cette information, le responsable de traitement maitrise le traitement algorithmique ;

\- le droit au recours soit garanti, c’est-à-dire une intervention humaine a posteriori ;

\- l’utilisation des données sensibles (qui comprennent notamment les données de santé), permise cependant, sous conditions, par le règlement, soit proscrite.

**_2.2 Nécessité de légiférer_**

L’[article 22](/reglement-2016-679/chapitre-iii/section-4/article-22) du règlement offre une marge de manœuvre en droit national (point 2. b), pour autoriser la prise de décision automatisée, à condition de prévoir des mesures appropriées pour la sauvegarde des droits et libertés et des intérêts légitimes de la personne concernée,. Le [considérant 71 du règlement](/reglement-2016-679/considerants/considerant-71), par sa formulation (« _la prise de décision fondée sur un tel traitement, y compris le profilage, devrait être permise_ ») permet aux Etats membres d’intégrer dans leur législation des règles de ce type dès lors que les garanties appropriées sont prévues.

Or, l’automatisation de décisions administratives constitue un levier essentiel pour une administration modernisée, permettant de conforter la continuité du service public, en sécurisant sa délivrance, et de sécuriser juridiquement les calculs de droits ou de prélèvements. Les algorithmes apportent une aide pour le traitement et l’analyse de l’information de plus en plus numérisée dans la sphère publique.

L’apparition d’automates hier a permis de renforcer l’efficacité de l’administration tout en permettant aux agents de traiter les situations les plus complexes. Aujourd’hui, les algorithmes sont partout dans la vie quotidienne numérique des Français. Ils déterminent ce que l’on voit sur les plates-formes d’achat en ligne, les réseaux sociaux ou les moteurs de recherche comme l’indique Nohza Boujemaa98. Ils sont utilisés par exemple pour fixer une tarification volatile ou organiser les recommandations personnalisées. L’administration utilise déjà largement cette technologie dans bien des situations qui ne donnent pas lieu à polémique tel le calcul des impôts

Ainsi, il apparait nécessaire que la France se dote d'un cadre juridique équilibré permettant aux administrations d’innover en automatisant certaines prises de décisions tout en assurant une totale transparence sur les traitements algorithmiques qu'elle utilise et que les usagers en soient correctement informés, ce qui implique que :

\- les algorithmes soient ouverts, auditables et transparents ;

\- ils soient « explicables », c’est-à-dire composés de règles dissociables et intelligibles.

Le principal enjeu, comme le relevait le rapport du Conseil d’Etat de 201499, est la prohibition d’algorithmes « boîtes noires », qui privent de sens la décision. Sur ce point, d’ailleurs, dans la sphère publique, le secret industriel et commercial ne saurait être opposé, comme c’est le cas dans la sphère privée, ainsi que le soulève l’étude annuelle 2017 du Conseil d’Etat100. C’est notamment à cette fin que la plate-forme TransAlgo chargée d’évaluer la responsabilité et la transparence des systèmes algorithmiques mise en œuvre par l'INRIA en collaboration avec le CNRS, a été créée. C’est également, dans cette logique, la [loi n° 2016-1321 du 7 octobre 2016](https://www.legifrance.gouv.fr/eli/loi/2016/10/7/ECFI1524250L/jo/texte) pour une République numérique avait prévu que, dans le cas de décisions administratives individuelles prises sur le fondement d’un traitement algorithmique, l’administration devait être à même de rendre compte précisément des règles utilisées.

 ** _3\. Options_**

 **3.1. Option 1 (écartée) : Maintien de l’article 10 dans sa rédaction actuelle**

La rédaction en vigueur de l’[article 10](/loi-78-17/chapitre-ii/section-2/article-10) de la loi n°78-17 ne permet pas de prendre des décisions administratives individuelles sur le seul fondement d’un traitement automatisé de données, une intervention humaine étant requise, entre l’évaluation algorithmique et l’édiction de la décision.

Les algorithmes sont très utilisés dans l’aide à la décision administrative. Une intervention humaine ne semble pas toujours nécessaire pour assurer les droits des personnes concernées et des usagers des services publics, comme par exemple pour le calcul de l’impôt sur le revenu.

Le maintien de cette interdiction absolue ne permet pas de répondre aux évolutions de l’activité administrative qui a de plus en plus recours à des traitements algorithmiques, notamment pour les décisions de masse que la règlementation encadre précisément et dont l’édiction rapide permet la bonne délivrance du service public.

Par ailleurs, l’article doit être adapté au règlement :

\- le troisième alinéa de l’article en est la recopie non nécessaire ;

\- la notion de profilage est adaptée à la définition du règlement.

Pour l’ensemble de ces raisons, l’écriture actuelle de l’article 10 de la loi n°78-17 n’apparaît plus adaptée.

 **3.2. Option 2 retenue : Modification de l’[article 10](/loi-78-17/chapitre-ii/section-2/article-10) de la loi n° 78-17 pour prévoir dans certaines conditions la prise de décisions administratives individuelles automatisées **

Il est proposé **** d’utiliser la marge de manœuvre prévue au b) du 2 de l’[article 22](/reglement-2016-679/chapitre-iii/section-4/article-22) du règlement (UE) 2016/679 pour ouvrir plus largement la possibilité pour l’administration de recourir à des décisions automatisées (prises sur le fondement d’un algorithme), dans le seul champ des décisions administratives individuelles (et non pour toute décision ayant un effet significatif sur la personne) et à la condition d’offrir d’importantes garanties en contrepartie, en matière d’information pleine et entière des personnes, de maîtrise des traitements de droit au recours et de données traitées (exception des données dites « sensibles » de ce cadre).

L’obligation d’information, issue de l’[article L. 311-3-1](https://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000033218946&idSectionTA=LEGISCTA000031367696&cidTexte=LEGITEXT000031366350&dateTexte=20180114) du code des relations du public et de l’administration, ne constitue pas simplement une règle de transparence, mais aussi, d’une part, une information proactive de l’usager et, de l’autre, par voie de conséquence directe, une maîtrise sur les règles qui doivent pouvoir être communiquées loyalement à la personne concernée, y compris en cas d’actualisations. Elle est complétée par l’[article 39](/loi-78-17/chapitre-v/section-2/article-39) de la loi de 78 et le règlement qui prévoient également une information de la personne concernée.

Le droit au recours hiérarchique ou gracieux de droit commun implique une garantie d’intervention humaine _a posteriori_101.

L’enjeu principal, en définitive, n’est pas celui d’une intervention humaine entre la décision algorithmique et sa notification, dont on peut interroger la réelle effectivité, mais :

\- d’une intervention humaine _ab initio_ , dans l’édiction des règles et dans leur implémentation par l’algorithme : c’est le sens de l’obligation de maîtrise de l’algorithme insérée dans l’article du projet de loi (et qui était déjà affirmée par l’[article 16](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=JORFARTI000033203039&cidTexte=JORFTEXT000033202746&dateTexte=29990101&categorieLien=id) de la loi pour une République numérique) ;

\- et d’une intervention humaine a posteriori pour réformer des décisions dans certaines situations particulières qui seront portées le cas échéant à la connaissance de l’administration.

S’agissant du risque de discrimination ou de biais, l’impossibilité de traiter des données sensibles telles que l’origine ethnique, la religion, l’opinion politique, l’affiliation syndicale ou l’orientation sexuelle apporte une première garantie, que vient compléter l’obligation de maîtrise susmentionnée.

 ** _4\. Analyse des impacts de la disposition envisagée_**

 ** _4.1. Impacts juridiques_**

Le principal impact attendu est la faculté de prendre des décisions administratives automatisées sans intervention humaine par une modification de l’article 10 de la loi n° 78-17, ce qui apportera la sécurisation juridique de certaines décisions pour lesquelles l’intervention humaine est résiduelle.

**_4.2. Impact sur les finances publiques_**

Ainsi que le souligne Nozha Boujemaa102, les services numériques définis et gérés par les algorithmes permettent l’autonomisation du traitement de l’information au-delà de ce que l’humain serait capable de gérer dans un temps comparable, ce qui est une véritable opportunité en termes de gain de performance et d’efficacité de l’exécution de l’action publique.

L’usage des algorithmes est relativement répandu dans le champ des finances publiques, sous deux aspects :

\- le calcul des prélèvements comme des droits ;

\- la lutte contre la fraude.

La sécurisation des décisions liées au calcul sera améliorée.

 ** _4.3. Impacts sur les collectivités territoriales_**

Les collectivités territoriales ne recourent que peu aux algorithmes dans leur prise de décision.

Elles sont toutefois susceptibles de le faire, notamment, en matière de _scoring_ pour l’attribution de logements sociaux ou d’affectation (petite enfance).

**_4.5. Impact sur l’égalité entre les femmes et les hommes et sur les personnes handicapées_**

L’automatisation de certaines décisions est susceptible d’avoir un impact positif sur les situations de discriminations, à la condition que les règles des algorithmes prennent en compte _ab initio_ le principe de non-discrimination et ses tempéraments particuliers (mise en place de garanties de procédure et de transparence, développement de contrôle des résultats produits par les algorithmes,…).

**_5\. Consultations_**

La Commission nationale de l'informatique et des libertés a été consulté sur cet article.

Le Conseil national d’évaluation des normes a été consulté et a rendu un avis favorable lors de sa séance du 30 novembre 2017.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

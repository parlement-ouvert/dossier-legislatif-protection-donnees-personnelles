<!-- TITLE: ARTICLE 13 -->
<!-- SUBTITLE: TRAITEMENTS DE DONNEES DE SANTE -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

 ** _1\. Etat des lieux et d iagnostic_**

 ** _1.1. Etat des lieux_**

L’accès aux données de santé est régi par l’ensemble législatif suivant.

La [loi n° 78-17 du 6 janvier 1978](/loi-78-17) relative à l'informatique, aux fichiers et aux libertés encadre les traitements de données à caractère personnel, impose des conditions générales de loyauté et de nécessité de la collecte des données, de leur utilisation pour des finalités déterminées, d’exactitude et de limitation dans le temps de leur conservation et reconnaît aux personnes dont les données font l’objet d’un traitement un droit d’information préalable, un droit d’opposition un droit d’accès et un droit de rectification. Cette loi pose, par ailleurs, en son [article 8](/loi-78-17/chapitre-ii/section-2/article-8), le principe d’une interdiction de traitement des données personnelles de santé (et d’autres données dites « sensibles »), sauf si les intéressés y ont consenti ou s’il s’agit de dossiers médicaux ou de traitements d’intérêt public dont la nécessité ou les conditions sont prévues par la loi. Sauf dispositions spéciales, ces derniers sont soumis à une autorisation de la Commission nationale de l’informatique et des libertés en vertu de l’[article 25](/loi-78-17/chapitre-iv/section-2/article-25). Le [chapitre IX](/loi-78-17/chapitre-ix) de cette loi fixe un régime spécial d’autorisation pour les traitements de données à caractère personnel à des fins de recherche, d’étude ou d’évaluation dans le domaine de la santé ; les projets de recherche sont examinés en amont de la CNIL par l’Institut national des données de santé et un comité consultatif composé de chercheurs (le comité d’expertise pour les recherches, les études, et les évaluations dans le domaine de la santé).

La [loi n° 78-753 du 17 juillet 1978](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000339241) portant diverses mesures d’amélioration des relations entre l’administration et le public et diverses dispositions d’ordre administratif, social et fiscal complète ces dispositions en matière de réutilisation des données personnelles détenues par l’administration. Cette réutilisation est permise dans les conditions de l’[article 13](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000020566920&cidTexte=LEGITEXT000006068643&dateTexte=20180114 "abrogé") de ce texte : ou bien parce que les personnes intéressées y ont consenti, ou bien parce que ces données ont été rendues anonymes, ou bien si une disposition légale ou réglementaire le permet.

La [loi n°2016-41 du 26 janvier 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031912641&categorieLien=id) de modernisation de notre système de santé instaure :

\- le système national des données de santé (SNDS) qui met à disposition :

« 1° Les données issues des systèmes d'information mentionnés à l'[article L. 6113-7](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032480354&cidTexte=LEGITEXT000006072665&dateTexte=20180114 "Code de la santé publique") du présent code ;

« 2° Les données du système national d'information inter régimes de l'assurance maladie mentionné à l'[article L. 161-28-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000006741264&dateTexte=&categorieLien=cid) du code de la sécurité sociale ;

« 3° Les données sur les causes de décès mentionnées à l'[article L. 2223-42](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000021940466&dateTexte=20110214) du code général des collectivités territoriales ;

« 4° Les données médico-sociales du système d'information mentionné à l'[article L. 247-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031728718&cidTexte=LEGITEXT000006074069&dateTexte=20180114) du code de l'action sociale et des familles ;

« 5° Un échantillon représentatif des données de remboursement par bénéficiaire transmises par des organismes d'assurance maladie complémentaire et défini en concertation avec leurs représentants.

Le SNDS est basé sur les principes suivants : confidentialité et intégrité des données, traçabilité des accès et des autres traitements.

\- l’Institut national des données de santé (INDS) qui est chargé de :

« 1° De veiller à la qualité des données de santé et aux conditions générales de leur mise à disposition, garantissant leur sécurité et facilitant leur utilisation dans le respect de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés ;

« 2° D'assurer le secrétariat unique mentionné à l'[article 54](/loi-78-17/chapitre-ix/article-54) de la même loi ;

« 3° D'émettre un avis sur le caractère d'intérêt public que présente une recherche, une étude ou une évaluation, dans les conditions prévues au même article 54 ;

« 4° De faciliter la mise à disposition d'échantillons ou de jeux de données agrégées mentionnées au V dudit article 54, dans des conditions préalablement homologuées par la Commission nationale de l'informatique et des libertés ;

« 5° De contribuer à l'expression des besoins en matière de données anonymes et de résultats statistiques, en vue de leur mise à la disposition du public.

« Il publie chaque année un rapport transmis au Parlement. ».

Le comité d’expertise pour les recherches, les études et les évaluations (CEREES) dans le domaine de la santé pour les demandes d’autorisation relatives à des études, évaluations, recherches n’impliquant pas la personne humaine.

\- les [articles L.1461-1 à L1461-7](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031923880&cidTexte=LEGITEXT000006072665&dateTexte=20180114) du code de la santé publique régissent le SNDS.

\- l’[article L.1462-1](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031923955&cidTexte=LEGITEXT000006072665&dateTexte=20180114) du code de la santé publique régit l’INDS.

\- les articles L.1121-1 et suivants du CSP issus de la [loi n° 2012-300 du 5 mars 2012](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025441587) régissent les recherches impliquant la personne humaine en vue du développement des connaissances biologiques ou médicales. La procédure d’accès aux données suppose, dans ce cas, l’avis d’un comité de protection des personnes (CPP), en amont de la décision de la CNIL.

La [loi n°2016-1321 du 7 octobre 2016](https://www.legifrance.gouv.fr/eli/loi/2016/10/7/ECFI1524250L/jo/texte) pour une République numérique [est venue modifier](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=JORFARTI000033202858&cidTexte=JORFTEXT000033202746&dateTexte=29990101&categorieLien=id "Cf. article 34 de la présente loi") les conditions d’autorisation des traitements de données de santé (articles [25](/loi-78-17/chapitre-iv/section-2/article-25) et [27](/loi-78-17/chapitre-iv/section-2/article-27) de la loi informatique et libertés).

De plus, des textes particuliers interviennent :

\- les articles [L. 161-29](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031931962&cidTexte=LEGITEXT000006073189&dateTexte=20180114) et [L. 161-33](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006741272&cidTexte=LEGITEXT000006073189&dateTexte=20180114) du code de la sécurité sociale prévoient la transmission à l’assurance maladie, par les professionnels et établissements de santé, d’informations médicales telles que les codes des actes et des prestations via les feuilles de soins ou les bordereaux de facturation ;

\- une réglementation européenne organise les conditions d’accès et d’utilisation des données personnelles, dans le respect de la vie privée. En effet, outre la [directive 95/46/CE du 24 octobre 1995](http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:31995L0046) [transposée](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000697074) dans la loi n° 78-17 par la [loi n° 2004-801 du 6 août 2004](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000441676&categorieLien=id) relative à la protection des personnes physiques à l'égard des traitements de données à caractère personnel, des dispositions similaires quant à la protection des données personnelles sont consacrées à la fois par l’article 8 de la [Charte des droits fondamentaux de l’Union européenne](http://www.europarl.europa.eu/charter/default_fr.htm) en vigueur dans les États membres et par la [convention n° 108 du Conseil de l’Europe](https://rm.coe.int/1680078b39).

Le [règlement (UE) 2016/579](/reglement-2016-679 "(coquilles)") du Parlement Européen et du conseil du 17 avril 2016 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données entre en vigueur le 25 mai 2018, abrogeant ainsi la [directive 95/46/CE](http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:31995L0046).

L’[article L. 1110-4](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000032924980&cidTexte=LEGITEXT000006072665&dateTexte=20180114) régit l’échange et le partage des données entre professionnels :

\- l’échange est autorisé entre professionnels mais limité au strict nécessaire à l’action de soin considérée (coordination, continuité des soins, prévention, suivi) ;

\- le partage entre professionnels appartenant à la même équipe de soin est autorisé ;

\- le partage entre professionnels n’appartenant pas à la même équipe de soin nécessite le consentement préalable de la personne concernée ;

\- concernant le partage et l’échange des données la concernant, la personne doit être dûment informée et peut exercer un droit d’opposition.

L’article L. 1111-8 régit les hébergeurs de données de santé à caractère personnel : une obligation d’agrément des prestataires d’hébergement s’applique et une obligation de certification est à venir.

L’article L. 1111-8 interdit la cession à titre onéreux de données de santé identifiantes directement ou indirectement, y compris avec l’accord des personnes concernées.

L’[article L.1111-8-1](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000034299702&cidTexte=LEGITEXT000006072665&dateTexte=20180114) stipule : un identifiant national unique (numéro d’inscription au répertoire national d’identification des personnes physiques, RNIPP) est utilisé comme identifiant de santé.

Les [articles L. 1111-14 et suivants](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031929174&cidTexte=LEGITEXT000006072665&dateTexte=20180114) du code de la santé publique concernent le dossier médical partagé :

\- créé sous réserve du consentement exprès de la personne concernée ;

\- hébergé conformément à la réglementation sur l’hébergement ;

\- alimenté par les professionnels de santé ;

\- la personne concernée peut rendre inaccessibles certaines informations ;

\- ensemble du dossier accessible au médecin traitant de la personne concernée ;

\- dossier accessible aux autres professionnels de santé avec le consentement de la personne concernée ;

\- la personne concernée a accès à la totalité de son dossier ainsi qu’aux traces d’accès ;

\- l’accès au dossier médical est interdit lors de la conclusion d’un contrat relatif à une protection en matière de santé et ne peut être exigé lors de la conclusion d’un contrat ou de son application.

L’[article L. 1111-23](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031929145&cidTexte=LEGITEXT000006072665&dateTexte=20180114) concerne le dossier pharmaceutique :

\- créé sous réserve du consentement exprès de la personne concernée ;

\- hébergé conformément à la réglementation sur l’hébergement ;

\- sauf opposition de la personne concernée, tout pharmacien peut alimenter, consulter le dossier ;

\- sauf opposition de la personne concernée, dûment informée, le médecin qui la prend en charge peut consulter le dossier pharmaceutique ;

\- la personne concernée a droit à l’obtention d’une copie du contenu de son dossier ainsi qu’aux traces d’accès ;

\- la personne concernée a droit à la rectification et à la clôture de son dossier.

**_1.2. Diagnostic_**

L’accès aux données de santé est actuellement régi par de nombreuses dispositions entrainant des difficultés d’interprétation entre l’Etat et la CNIL, et entravant de fait l’accès aux données.

D’une façon générale, cette situation conduit à :

\- délivrer des autorisations d’accès au terme de délais trop longs (prorogation systématique du délai de deux mois par la CNIL jusqu’à des délais pouvant atteindre 18 mois, le silence de la CNIL valant refus) et incompatibles avec, d’une part les exigences du monde économique actuel, et d’autre part la performance publique,

\- autoriser la libération de données dans des conditions susceptibles de porter atteinte à la vie privée des citoyens, lesquels s’avèrent le plus souvent insuffisamment informés de l’utilisation de leurs données personnelles.

S’agissant de la procédure d’accès aux données de santé pour les études, recherches, évaluations n’impliquant pas la personne humaine, il s’agit de la conforter d’autant qu’elle est en cours de mise en place. À ce jour, les délais légaux imposés à l’[INDS](http://www.institut-des-donnees-de-sante.fr/) et au [CEREES](https://www.snds.gouv.fr/SNDS/Actualites/Actu-2 "Comité d’expertise pour les recherches, études et évaluations en santé") sont tenus avec un flux de dossiers relativement limité. Il est à craindre, à l’avenir :

\- une difficulté d’absorption du flux de dossiers,

\- un engorgement de la CNIL (cette dernière ne s’est pas encore prononcée sur les premiers dossiers de la nouvelle procédure).

Les procédures simplifiées actuellement prévues par les textes (méthodologies de références, décisions uniques et facilitations d’accès aux jeux de données agrégées et aux échantillons) sont en cours de construction et mériteraient d’être développées pour permettre une fluidification de l’accès aux des données.

 ** _2\. Objectifs poursuivis et necessite de legiferer_**

 ** _2.1. Objectifs poursuivis_**

La présente disposition ambitionne de libérer le potentiel d’exploitation des données de santé tout en respectant la vie privée des citoyens.

Il s’agit en particulier de sécuriser les modalités d’accès aux données :

\- en simplifiant les procédures d’accès aux données de santé, par le recours plus systématique aux procédures simplifiées, notamment aux référentiels et règlements types pour les traitements de données à caractère personnel dans le domaine de la santé et aux méthodologies de référence pour les traitements à des fins de recherche, d’étude ou d’évaluation dans le domaine de la santé,

\- en unifiant le cadre juridique d’accès à toutes les données de santé,

\- en apportant des garanties supplémentaires aux citoyens,

\- en réduisant les délais d’obtention des autorisations délivrées par la CNIL, lorsque le recours à des procédures simplifiées ne peut pas être activé.

 ** _2.2. Nécessité de légiférer_**

L’application du règlement (UE) 2016/579 conduit le Gouvernement à mettre la législation française en conformité.

 ** _3\. Options et dispositif retenus_**

 ** _3.1. Options écartées_**

Ont été écartées les options suivantes :

\- une simplification excessive issue de la suppression de tous les régimes d’autorisation préalable actuels en raison des risques d’atteinte à la vie privée qui découlent de la manipulation des données de santé, laquelle doit être encadrée ;

\- une reprise inchangée des dispositions actuelles.

**_3.2. Option retenue_**

L’application, le 25 mai 2018, du règlement conduit le Gouvernement à simplifier et clarifier les procédures d’accès aux données de santé en supprimant les articles 25 et 27 de la loi n° 78-17 lesquels prévoyaient des régimes d’autorisation différents selon les types de données mobilisées (autorisation de la CNIL, décret en conseil d’État pris après motivé de la CNIL, arrêté pris après avis motivé de la CNIL). Le règlement consacre le principe de l’analyse d’impact sur la vie privée laquelle s’applique de fait à tous les responsables de traitement qui manipulent des données de santé.

Les présentes modifications proposées dans le projet de loi visent, d’une part à conserver les procédures d’accès en vigueur pour les traitements à fin de recherche observationnelle (notamment ceux mobilisant le SNDS) et les traitements de données impliquant la personne humaine, et d’autre part à introduire un dispositif général permettant de couvrir tous les traitements de données à caractère personnel dans le domaine de la santé impliquant un intérêt public, une saisine éventuelle de l’INDS et une autorisation de la CNIL.

D’une façon générale, le recours aux référentiels et règlements types et aux méthodologies de référence, homologuées par la CNIL, devient le principe et les demandes de traitement s’inscrivant dans une démarche d’obtention de l’autorisation de la CNIL, l’exception.

Par ailleurs, les procédures simplifiées couvrent désormais des jeux de données plus larges (seuls les jeux de données agrégés et les échantillons y étaient éligibles à ce jour), et les autorisations uniques sont maintenues.

Enfin, le Gouvernement propose de modifier la règle régissant le délai de délivrance de l’autorisation de la CNIL en substituant à la règle du « silence vaut refus » la règle du «silence vaut acceptation ».

 ** _4\. Analyse des impacts de la disposition envisagée_**

Le développement de l’utilisation des données de santé doit permettre d’améliorer la connaissance du système de santé par les élus et les citoyens de façon à optimiser la prise en charge sanitaire et sociale.

Il doit également permettre la rationalisation des dépenses de santé grâce à l’optimisation des parcours de soins et des processus de prise en charge des patients.

Enfin, au regard des enjeux du _big data_ et de l’intelligence artificielle, l’accès facilité aux données de santé constitue un atout pour la France en terme de positionnement international et d’attractivité économique.

 ** _5\. Consultation et modalités d’application_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

Les dispositions réglementaires d’application de la loi informatique et libertés devront être adaptées en conséquence, notamment le [décret n° 2016-1872 du 26 décembre 2016](https://www.legifrance.gouv.fr/eli/decret/2016/12/26/AFSE1625288D/jo/texte) modifiant le [décret n° 2005-1309 du 20 octobre 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006052581&dateTexte=20090319) pris pour l’application de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.

Les textes réglementaires suivants devront être pris sur le fondement de la loi :

<table>
<tr>
<td>

**Articles du PJL renvoyant à des mesures réglementaires**

</td>
<td>

**Nature du texte réglementaire**

</td>
<td>

**Objet du texte réglementaire**

</td></tr>
<tr>
<td>

Article 13 (article 54 de la loi n° 78-17)

</td>
<td>

Décret en Conseil d’Etat après avis de la commission nationale de l’informatique et des libertés

</td>
<td>

Modalités de saisine de l’Institut national des données de santé

</td></tr>
<tr>
<td>

Article 13 (article 55 de la loi n° 78-17)

</td>
<td>

Arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l'informatique et des libertés

</td>
<td>

Liste des organismes ou services chargés d’une mission de service public mettant en œuvre des traitements de données de santé à caractère personnel afin de répondre à une situation d’alerte sanitaire

</td></tr>
<tr>
<td>

Article 13 (article 63 de la loi n° 78-17)

</td>
<td>

Décret en Conseil d’Etat après avis de la commission nationale de l’informatique et des libertés

</td>
<td>

Composition et fonctionnement du comité d’expertise pour les recherches, les études et les évaluations dans le domaine de la santé

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: ARTICLE 15 -->
<!-- SUBTITLE: LIMITATION DES DROITS -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**1\. Etat des lieux et diagnostic**

 **1.1. Etat des lieux**

La [loi 78-17 du 6 janvier 1978](/loi-78-17) relative à l'informatique, aux fichiers et aux libertés consacre la [section 2](/loi-78-17/chapitre-v/section-2) du chapitre V aux droits des personnes à l’égard des traitements de données à caractère personnel.

Elle ne prévoit pas de restriction générale aux droits prévus pour la protection des données à caractère personnel. Des limitations à certains droits sont en revanche prévues à :

\- l’[article 38](/loi-78-17/chapitre-v/section-2/article-38) : limitation au droit d’opposition « _lorsque le traitement répond à une obligation légale ou lorsque l'application de ces dispositions a été écartée par une disposition expresse de l'acte autorisant le traitement_ » ;

\- l’[article 39](/loi-78-17/chapitre-v/section-2/article-39) en matière de droit d’accès, le responsable de traitement pouvant « _s'opposer aux demandes manifestement abusives, notamment par leur nombre, leur caractère répétitif ou systématique._ ». Le droit d’accès ne s’applique pas non plus « _lorsque les données à caractère personnel sont conservées sous une forme excluant manifestement tout risque d'atteinte à la vie privée des personnes concernées et pendant une durée n'excédant pas celle nécessaire aux seules finalités d'établissement de statistiques ou de recherche scientifique ou historique_ » ;

\- l’[article 40](/loi-78-17/chapitre-v/section-2/article-40) qui prévoit que le droit de rectification et droit à l’effacement ne s’appliquent pas lorsque le traitement de données à caractère personnel est nécessaire pour exercer le droit à la liberté d'expression et d'information, pour respecter une obligation légale qui requiert le traitement de ces données ou pour exercer une mission d'intérêt public ou relevant de l'exercice de l'autorité publique dont est investi le responsable du traitement, pour des motifs d'intérêt public dans le domaine de la santé publique, à des fins archivistiques dans l'intérêt public, à des fins de recherche scientifique ou historique ou à des fins statistiques, dans la mesure où le droit à l’effacement est susceptible de rendre impossible ou de compromettre gravement la réalisation des objectifs du traitement, et enfin à la constatation, à l'exercice ou à la défense de droits en justice.

Ces restrictions sont reprises, précisées ou complétées dans les textes autorisant la création de traitements de données. Par exemple, l’[article L. 4123-9-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000032646880&dateTexte=&categorieLien=cid) du code de la défense prévoit, s’agissant des traitements dont la finalité est fondée sur la qualité de militaires des personnes qui y figurent, que les personnes concernées ne sont averties, en cas de divulgation ou d'accès non autorisé à leurs données, qu’après l’accord du ministère compétent.

 **1.2. Cadre conventionnel**

L’[article 23](/reglement-2016-679/chapitre-iii/section-5/article-23) du règlement (UE) 2016/679103 prévoit une marge de manœuvre importante qui permet aux Etats membres « _par la voie de mesures législatives, limiter la portée des obligations et des droits prévus aux articles 12 à 22 et à l'article 34, ainsi qu'à l'article 5 dans la mesure où les dispositions du droit en question correspondent aux droits et obligations prévus aux articles 12 à 22, lorsqu'une telle limitation respecte l'essence des libertés et droits fondamentaux et qu'elle constitue une mesure nécessaire et proportionnée dans une société démocratique pour garantir_ » certaines finalités, missions ou objectifs listés104.

Les droits visés sont l’ensemble des droits définis à la [section 1 du chapitre III](/reglement-2016-679/chapitre-iii/section-1) du règlement, à savoir, outre les droits déjà prévus par la loi n° 78-17, le droit à l’effacement ([article 17](reglement-2016-679/chapitre-iii/section-3/article-17) du règlement) et le droit à la portabilité des données ([article 20](/reglement-2016-679/chapitre-iii/section-3/article-20)).

Le [considérant 73 du règlement](/reglement-2016-679/considerants/considerant-73) précise que : « _Des limitations à certains principes spécifiques ainsi qu'au droit à l'information, au droit d'accès aux données à caractère personnel, au droit de rectification ou d'effacement de ces données, au droit à la portabilité des données, au droit d'opposition, aux décisions fondées sur le profilage, ainsi qu'à la communication d'une violation de données à caractère personnel à une personne concernée et à certaines obligations connexes des responsables du traitement peuvent être imposées par le droit de l'Union ou le droit d'un État membre, dans la mesure nécessaire et proportionnée dans une société démocratique pour garantir la sécurité publique, y compris la protection de la vie humaine, particulièrement en réponse à des catastrophes d'origine naturelle ou humaine, la prévention des infractions pénales, les enquêtes et les poursuites en la matière ou l'exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces ou de manquements à la déontologie des professions réglementées, et pour garantir d'autres objectifs d'intérêt public importants de l'Union ou d'un État membre, notamment un intérêt économique ou financier important de l'Union ou d'un État membre, la tenue de registres publics conservés pour des motifs d'intérêt public général, le traitement ultérieur de données à caractère personnel archivées pour fournir des informations spécifiques relatives au comportement politique dans le cadre des régimes des anciens États totalitaires ou la protection de la personne concernée ou des droits et libertés d'autrui, y compris la protection sociale, la santé publique et les finalités humanitaires. Il y a lieu que ces limitations respectent les exigences énoncées par la Charte et par la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales._ »

Par ailleurs, l’[article 23](/reglement-2016-679/chapitre-iii/section-5/article-23) du règlement précité utilise la notion de « mesures législatives ». Le [considérant 41 du règlement](/reglement-2016-679/considerants/considerant-41) précise à cet égard que : « _Lorsque le présent règlement fait référence à une base juridique ou à une mesure législative, cela ne signifie pas nécessairement que l'adoption d'un acte législatif par un parlement est exigée, sans préjudice des obligations prévues en vertu de l'ordre constitutionnel de l'État membre concerné. Cependant, cette base juridique ou cette mesure législative devrait être claire et précise et son application devrait être prévisible pour les justiciables, conformément à la jurisprudence de la Cour de justice de l'Union européenne (ci-après dénommée «Cour de justice») et de la Cour européenne des droits de l'homme._ ».

La Cour européenne des droits de l’homme retient également une acception "matérielle" et non "formelle" du terme de « loi ». A ce titre elle y inclut des textes de rang infralégislatif105.

**_2\. Objectifs poursuvis et necessite de légiferer_**

 ** _2.1 Objectifs poursuivis_**

Le projet de loi vise également un objectif de clarification et d’intelligibilité du droit en définissant les conditions dans lesquelles certains traitements ou catégories de traitements sont autorisés à déroger au droit à la communication d’une violation de données.

 ** _2.2 Nécessité de legiferer_**

Les droits reconnus en matière de traitements de données à caractère personnel sont définis par la loi 78-17 tout comme les dérogations à ces droits. L’article 23 du règlement permet une marge de manœuvre sur les limitations des droits des personnes concernées..

Le présent projet de loi souhaite compléter les dérogations prévues par la loi n° 78-17, pour les cas dans lesquels la communication d’une divulgation ou d’un accès non autorisé à des données est susceptible de présenter un risque pour la sécurité nationale, la défense nationale ou la sécurité publique, et lorsque sont en cause des traitements ou catégories de traitements nécessaires au respect d’une obligation légale ou à l’exercice d’une mission d’intérêt public.

En effet, dans le contexte actuel, les démarches et intentions d’organisations terroristes visant à dérober des données personnelles sensibles constituent une menace grave et crédible. Il est donc souhaitable de prévoir l’absence de communication à la personne concernée de la divulgation de ses données afin que puisse être évalué si la diffusion d'une information sur cette divulgation ou cet accès non autorisé est susceptible de représenter un risque pour la sécurité des personnes, la sécurité publique ou la sûreté de l'Etat. Ce n’est que si ce n’était pas le cas que l’intéressé pourrait être informé de la violation de ces données personnelles.

Des dispositions ont d’ailleurs déjà été adoptées en ce sens dans le code de la défense (cf. articles [L. 4123-9-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000018717558&cidTexte=LEGITEXT000006071307&dateTexte=20180114) et [R. 4123-51](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033824531&cidTexte=LEGITEXT000006071307&dateTexte=20180114)) pour les traitements dont la finalité exige l’enregistrement de la qualité de militaire.

La reconnaissance par le législateur d’un droit à la communication d’une violation de données et l’encadrement du champ et la portée des exceptions à un tel droit participent de la définition des garanties fondamentales accordées aux citoyens pour l’exercice des libertés publiques au sens de l’article 34 de la Constitution.

 ** _3\. Options_**

 **3.1. Option 1 (écartée) : Ne rien inscrire dans la loi**

Le règlement étant d’application directe, le législateur ne peut se prononcer sur cette mesure et considérer implicitement que c’est directement chaque acte règlementaire qui autorise ou crée un traitement qui peut prévoir les limitations à certains droits.

**3.2. Option 2 (retenue) : Prévoir une possibilité de limitation de certains droits dans le respect de l’[article 23](/reglement-2016-679/chapitre-iii/section-5/article-23) du règlement tout en rattachant les dérogations aux traitements ou catégories de traitement**

Cette option fixe le principe de l’existence de dérogations au droit à la communication d’une violation de données mentionné à l’[article 34](/reglement-2016-679/chapitre-iv/section-2/article-34) du règlement, en précisant la portée de l’article 23 à un triple titre :

\- en proposant de limiter le champ de la dérogation à certaines violations de données (et non à l’ensemble de celles définies à l’[article 4](/reglement-2016-679/chapitre-i/article-4).12 du règlement106), à savoir la divulgation de données et l’accès non autorisé à celles-ci (uniquement les manœuvres frauduleuses et non les défaillances dont les administrations seraient responsables) ;

\- en mentionnant le fait que le besoin de garantir la sécurité nationale, la défense nationale et la sécurité publique ne résulte pas de la nature de certaines données du traitement mais de la communication de la violation des données elle-même. En d’autres termes, ce n’est pas le vol de données qui impose l’absence de notification mais le fait que les personnes « volées » soient averties de ce vol, que le voleur en soit informé et qu’il prenne conscience de la sensibilité des informations détenues ;

\- en limitant le champ d’application de cette dérogation aux traitements ou catégories de traitement nécessaires au respect d’une obligation légale ou à l’exercice d’une mission d’intérêt public.

Ce choix reflète celui qui existe actuellement dans la loi n° 78-17 en matière de limitation des droits et celui qui a été retenu pour la transposition de la directive (UE) 280/2016, en son article 70-21 créé par le projet de loi. Il va toutefois au-delà en prévoyant l’édiction d’un décret en Conseil d’Etat pris après avis de la CNIL afin d’encadrer le champ et la portée de ces dérogations.

Cette option permet enfin de clarifier la notion de « _mesures législatives_ » qui figure dans les deux textes européens, laquelle renvoie, ainsi qu’il a été dit précédemment, à des mesures qui ne sont pas nécessairement adoptées par le Parlement, ainsi que le précisent les considérants [41 du règlement](/reglement-2016-679/considerants/considerant-41) et [33 de la directive](/directive-2016-680/considerants/considerant-33).

**_4\. Analyse des impacts de la disposition envisagée_**

Le projet de loi, en complétant l’[article 40](/loi-78-17/chapitre-v/section-2/article-40) de la loi n° 78-17, prévoit la possibilité d’une limitation à être informé des violations d'un traitement de données personnelles, régi par l'[article 34](/reglement-2016-679/chapitre-iv/section-2/article-34) du règlement pour les seuls traitements répondant à une obligation légale, et aux seules fins de protection de la sécurité nationale, de la défense nationale ou de la sécurité publique, .

Il s’agit de permettre aux responsables du traitement de ne pas prévenir la personne dont les données ont fait l'objet d'une violation par un tiers dans des conditions mettant en cause, à raison de données ou à raison de l'emploi de la personne (par exemple s'il s'agit d'un agent des forces de sécurité ou d’un militaire), la sécurité ou la défense, afin de mieux assurer la lutte contre les auteurs de ces violations.

Sont notamment visés par ces dispositions, les traitements comportant des données personnelles à caractère sensible relatives, en particulier, à la qualité de militaire ([article 117](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032627231&categorieLien=id#JORFARTI000032628147) de la loi n° 2016-731 du 3 juin 2016) ou à des agents du ministère de la défense occupant des fonctions sensibles (traitements de données de ressources humaines ou du service de santé des armées).

 ** _5\. Consultations et modalités d’application_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

Les textes réglementaires suivants devront être pris sur le fondement de la loi :

<table>
<tr>
<td>

**Articles du PJL renvoyant à des mesures réglementaires**

</td>
<td>

**Nature du texte réglementaire**

</td>
<td>

**Objet du texte réglementaire**

</td></tr>
<tr>
<td>

[Article 15](/pjl-490/titre-ii/chapitre-v/article-15)

</td>
<td>

Décret en conseil d’Etat pris après avis de la Commission nationale de l'informatique et des libertés

</td>
<td>

Fixation de la liste des traitements et des catégories de traitements autorisés à déroger au droit à la communication d’une violation de données régi par l’[article 34](/reglement-2016-679/chapitre-iv/section-2/article-34) du, règlement (UE) 2016/679.

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

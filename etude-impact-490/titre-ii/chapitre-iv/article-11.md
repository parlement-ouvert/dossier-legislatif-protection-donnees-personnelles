<!-- TITLE: ARTICLE 11 -->
<!-- SUBTITLE: DONNEES D’INFRACTION -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

 ** _1\. Etat des lieux et diagnostic_**

 ** _1.1. Etat des lieux_**

L’[article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés constitue, avec l’[article 8](/loi-78-17/chapitre-ii/section-2/article-8), le cadre qui régit le traitement des données intrinsèquement sensibles, lesquelles ne peuvent être collectées que dans des conditions particulières.

Cet article concerne les données d'infractions, condamnations et mesures de sûreté. Ces données peuvent en effet emporter de graves conséquences si elles sont révélées à autrui, notamment lorsqu'elles sont conservées au-delà des durées de prescription légale.

Le traitement de telles données est doublement encadré :

\- d’une part, en termes de formalités préalables, de tels traitements étant actuellement soumis au régime d’autorisation prévu par l’[article 25-3°](/loi-78-17/chapitre-iv/section-2/article-25) de la loi n° 78-17 ;

\- d’autre part, le nombre limité de responsables qui peuvent mettre en œuvre ces traitements.

Ainsi, outre les juridictions, les autorités publiques et les personnes morales gérant un service public, agissant dans le cadre de leurs attributions légales, peuvent également mettre en œuvre de tels traitement, les auxiliaires de justice, pour les stricts besoins de l'exercice des missions qui leur sont confiées par la loi et, depuis la [loi n° 2004-801 du 6 août 2004](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000441676), les personnes morales mentionnées aux [_articles L. 321-1_86](https://www.legifrance.gouv.fr/eli/loi/2004/8/6/JUSX0100026L/jo#JORFARTI000002057492) qui prévoyaient la possibilité accordée à une personne morale de droit privé, mandatée par plusieurs autres personnes morales victimes d'agissements pénalement sanctionnés – ou estimant en avoir été victimes ou pensant être susceptibles d'en être victimes – pour les besoins de la prévention et de la lutte contre la fraude ainsi que de la réparation du préjudice subi de rassembler un grand nombre d'informations nominatives relatives à des infractions, condamnations et mesures de sûreté, en raison de l’absence de garanties appropriées et spécifiques87. Le Conseil constitutionnel a en effet considéré que, compte tenu du champ, très large, couvert par une telle faculté, il appartenait au législateur de fixer les conditions dans lesquelles elle pouvait être exercée, sans pouvoir renvoyer à des lois ordinaires ultérieures.

Le Conseil a également formulé une réserve d’interprétation de l’[article 9](/loi-78-17/chapitre-ii/section-2/article-9), tel que résultant de la déclaration d'inconstitutionnalité de son 3°. Pour le Conseil constitutionnel, cet article ne saurait être interprété comme privant d'effectivité le droit d'exercer un recours juridictionnel dont dispose toute personne physique ou morale s'agissant des infractions dont elle a été victime, afin de ne pas priver de base légale les traitements légitimement mis en œuvre par chaque personne morale pour suivre les dossiers contentieux relatifs aux infractions dont elle a été elle-même victime.

Le projet de loi, tenant compte de la censure du Conseil constitutionnel et de la réserve d’interprétation associée à sa décision, propose de permettre aux personnes physiques ou morales de pouvoir mettre en œuvre des traitements de données d’infraction afin de leur permettre de préparer et le cas échéant, d'exercer et de suivre une action en justice en tant que victime, mise en cause, ou pour le compte de ceux-ci et de faire exécuter la décision rendue, pour une durée proportionnée à cette finalité.

 ** _1.3. Cadre conventionnel_**

Les traitements de données de nature pénale sont régis par la [directive (UE) 2016/680 du 27 avril 2016](/directive-2016-680) lorsqu’elles sont traitées par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d’enquêtes et de poursuites en la matière ou d’exécution de sanctions pénales, y compris la protection la protection contre les menaces à la sécurité publique et la prévention de telles menaces. Les autorités compétentes en question peuvent comprendre non seulement les autorités publiques telles que les autorités judiciaires, la police ou d'autres autorités répressives mais aussi tout autre organisme ou entité à qui le droit d'un État membre confie l'exercice de l'autorité publique et des prérogatives de puissance publique aux fins de la présente directive (considérant l'[article 11](/directive-2016-680/chapitre-ii/article-11) de la directive).

Le [règlement (UE) 2016/679](/reglement-2016-679) s'applique en revanche lorsqu'un organisme ou une entité recueille des données à caractère personnel à d'autres fins que celles prévues par la directive et les traite ultérieurement pour respecter une obligation légale à laquelle il est soumis. Par exemple, les établissements financiers conservent, à des fins de détection ou de poursuites d'infractions pénales ou d'enquêtes en la matière, certaines données à caractère personnel qu'ils traitent et qu'ils ne transmettent aux autorités nationales compétentes que dans des cas spécifiques et conformément au droit des États membres (considérant l'[article 11](/directive-2016-680/chapitre-ii/article-11) de la directive).

De même, les États membres peuvent confier aux autorités compétentes d'autres missions qui ne sont pas nécessairement menées à des fins de prévention et de détection des infractions pénales, d'enquêtes ou de poursuites en la matière, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces, de sorte que le traitement de données à caractère personnel à ces autres fins, pour autant qu'il relève du champ d'application du droit de l'Union, relève du champ d'application du [règlement (UE) 2016/679](/reglement-2016-679).

Le règlement prévoit d’ailleurs une disposition spécifique pour les données relatives aux condamnations pénales, aux infractions et aux mesures de sûreté connexes dont leur traitement, compte tenu de la nature de ces données, est strictement encadré.

L’[article 10](/reglement-2016-679/chapitre-ii/article-10) du règlement prévoit ainsi que : _« Le traitement des données à caractère personnel relatives aux condamnations pénales et aux infractions ou aux mesures de sûreté connexes (…), ne peut être effectué que sous le contrôle de l'autorité publique, ou si le traitement est autorisé par le droit de l'Union ou par le droit d'un État membre qui prévoit des garanties appropriées pour les droits et libertés des personnes concernées. Tout registre complet des condamnations pénales ne peut être tenu que sous le contrôle de l'autorité publique. »_.

Enfin, l’[article 86](/reglement-2016-679/chapitre-ix/article-86) du règlement prévoit que : « _Les données à caractère personnel figurant dans des documents officiels détenus par une autorité publique ou par un organisme public ou un organisme privé pour l'exécution d'une mission d'intérêt public peuvent être communiquées par ladite autorité ou ledit organisme conformément au droit de l'Union ou au droit de l'État membre auquel est soumis l'autorité publique ou l'organisme public, afin de concilier le droit d'accès du public aux documents officiels et le droit à la protection des données à caractère personnel au titre du présent règlement._ ».

Cet article autorise ainsi le droit national à prévoir la réutilisation des données publiques contenues dans les décisions de justice, dans le respect du droit à la protection des données, ainsi que le prévoit l’[article 11](/pjl-490/titre-ii/chapitre-iv/article-11) du projet de loi.

 ** _1.4. Eléments de droit comparé_**

Dans son projet de loi organique, l’Espagne prévoit que les traitements de données de nature pénale à d’autres finalités que pénales ne pourront être mis en œuvre que sur la base d’une disposition au moins de rang législatif, d’une disposition de cette même loi organique ou soit sur la base d’un texte de l’Union européenne.

L’Irlande a prévu d’autres possibilités de traitements des données à caractère personnel relatives aux condamnations pénales et aux infractions que les seuls cas visés par le [règlement (UE) 2016/679](/reglement-2016-679). A l’instar du présent projet de loi, l’Irlande a fait le choix d’utiliser la marge de manœuvre de l’article 11 du règlement, en ouvrant cette possibilité à 9 catégories de traitements88.

Le projet de loi britannique prévoit 7 catégories de traitements possibles. Par exemple, les organisations non-lucratives à finalité politique, philosophique, religieuse ou syndicale, pourront sous certaines conditions, traiter des données de nature pénale de leurs membres ou anciens membres.

**_2\. Objectifs et necessite de legiferer_**

 ** _2.1. Objectifs poursuivis_**

Les modifications proposées à l’[article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n° 78-17 par le projet de loi poursuivent plusieurs objectifs :

\- un objectif de clarté et d’intelligibilité du droit en s’alignant sur la rédaction de l’[article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n°78-17 sur celle prévue par l’[article 10](/reglement-2016-679/chapitre-ii/article-10) du règlement (UE) 2016/679 ;

\- un renforcement de la protection des droits fondamentaux dès lors que le champ des données de données de nature pénale encadré par la loi est élargi, en couvrant par exemple les mesures de sûreté connexes ;

\- un objectif de sécurité juridique, en permettant à des personnes qui ont une nécessité particulière de traiter des données de nature pénale de pouvoir le faire, dans un cadre strict afin que l’atteinte aux droits fondamentaux ne soit pas excessive ;

\- une conciliation de la protection des données à caractère personnel avec la transparence de la justice, et le droit d’accès à l’information.

**_2.2. Nécessité de legiferer_**

Si l’[article 10](/reglement-2016-679/chapitre-ii/article-10) du règlement (UE) 2016/679 est directement applicable en droit national, il est nécessaire d’harmoniser les écritures dans un objectif de clarté et intelligibilité du droit. Pour cette raison, le premier alinéa de l’[article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés doit être modifié pour épouser la rédaction du règlement. Les données d’infractions sont entendues au sens large, incluant les mesures de sûreté connexes. L’insertion du critère « sous le contrôle de l’autorité publique », en plus de « mis en œuvre par » vise les traitements mis en œuvre pour le compte de l’autorité publique comme l’hébergement des données sur un serveur.

Le projet de loi prévoit également d’élargir le champ des personnes susceptibles de mettre en œuvre de tels traitements dans le cadre notamment du suivi des dossiers contentieux relatifs aux infractions, afin de tenir compte de la censure du Conseil constitutionnel dans sa [décision du 29 juillet 2004](/avis-conseil-etat-393836).

En effet, actuellement, de nombreux traitements89 ont été autorisés par la Commission nationale de l’informatique et des libertés en s’appuyant sur la réserve d’interprétation formulée par le Conseil constitutionnel dans sa  [décision du 29 juillet 2004](/avis-conseil-etat-393836). La Commission est d’ailleurs régulièrement saisie de demandes d’autorisation pour des fichiers « contentieux » ou « précontentieux » par des organismes privés ou publics, parmi lesquels figurent des infractions et des condamnations pénales.

Par conséquent, une réécriture du troisième paragraphe de l’[article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n° 78-17 apparaît nécessaire en termes de sécurité juridique.

Enfin, il est apparu nécessaire, suite à l’adoption de la [loi n° 2016-1321 du 7 octobre 2016 pour une République numérique](/loi-2016-1321) qui permet désormais la mise à disposition du public à titre gratuit ( _open data_ ) des décisions de justice, d’encadrer la réutilisation des données publiques de nature pénale contenues dans ces décisions.

**_3\. Options_**

 **3.1. Elargissement du champ des personnes concourant au service public de la justice autorisées à mettre en œuvre des traitements de donnes d’infraction**

 **3.1.1. Option 1 (écartée) : Maintien du droit existant**

La rédaction actuelle du 1° l’[article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n° 78-17 qui limite le champ d’application aux personnes seules personnes publiques et personnes morales gérant un service public, agissant dans le cadre de leurs attributions légales, ne permet pas de prendre en compte les besoins d’autres personnes morales qui, sans avoir cette qualité, concourent au service public de la justice.

La rédaction actuelle du 1° de l’[article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n° 78-17 apparaît par conséquent trop restrictive.

 **3.1.2. Option 2 (écartée) : Etendre à l’ensemble des personne concourant à un service public**

Cette option consistant à donner la possibilité de mettre en œuvre de traitement des traitements de données d’infraction à l’ensemble des personnes morales « concourant à un service public » est apparue excessive eu égard à la sensibilité des données concernées.

Pour cette raison, cette option n’a pas été retenue.

**3.1.3. Option 3 (retenue) : Limiter l’extension aux seules personnes morales de droit privé collaborant au service public de la justice**

L’ajout proposé permet à des acteurs limités, dans un cadre circonscrit notamment en termes de finalité, de traiter des données de nature pénale, en tenant compte de la réserve d’interprétation formulée par le Conseil constitutionnel.

Il s’agit de maintenir l’interdiction de principe mais de permettre des personnes morales de droit privé (associations par exemple) œuvrant à partir de données d’infractions dans une mission de service public de la justice de mener à bien leur mission principale ou conformément à leur objet social.

Cette disposition vise notamment les associations d’aide aux victimes90, ou les associations d’aide à la réinsertion des personnes placées sous main de justice91. L’impossibilité actuelle pour de tels acteurs qui ne gèrent pas un service public mais collaborent à celui-ci, de traiter des données de nature pénale nécessaire à leur activité nuit à la réalisation de leur mission.

 **3.2. Possibilité pour les personnes physiques ou morales de mettre en œuvre de tels traitements dans le cadre d’une action en justice en tant que victime**

 **3.2.1. Option 1 (écartée) : Maintien du droit existant**

Ainsi qu’il a été dit précédemment, plusieurs traitements autorisés par la Commission nationale de l’informatique et des libertés trouvent leur base légale sur la seule réserve d’interprétation du Conseil constitutionnel dans sa décision du 29 juillet 2004, aux termes de laquelle l’[article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n° 78-17 « _ne saurait être interprété comme privant d'effectivité le droit d'exercer un recours juridictionnel dont dispose toute personne physique ou morale s'agissant des infractions dont elle a été victime_ ».

Cette situation n’est pas satisfaisante au regard du principe de sécurité juridique. Il appartient en effet au législateur de préciser davantage les conditions dans lesquelles un traitement de données d’infraction peut être mise en œuvre par une personne physique ou morale pour assurer sa défense en qualité de victime.

 **3.2.2. Option 2 (retenue) : Prévoir un encadrement des traitements relatifs à la gestion du contentieux par des personnes privées**

Afin de répondre à un réel besoin tant pratique (la Commission est régulièrement saisie de demandes d’autorisation pour des fichiers « contentieux » ou « précontentieux » par des organismes privés) qu’en termes de sécurité juridique, il est proposé de rétablir le 3° de l’[article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n° 78-17 qui avait été censuré par le Conseil Constitutionnel, dans une rédaction compatible avec sa décision du 29 juillet 2004, en prévoyant les garanties légales nécessaires.

L’[article 11](/loi-78-17/chapitre-iii/article-11) du projet de loi précise ainsi que, par principe, les données d’infraction ne peuvent pas être « partagées » ou « cédées » mais qu’elles peuvent être traitées par un tiers autre que la victime ou le mis en cause pour son compte. Cette rédaction vise à couvrir le cas d’une société mère qui dispose d’un service juridique afin qu’elle puisse traiter des données d’infractions pour le compte de ses filiales victimes d’infractions, de même pour les sociétés d’assurances ou certaines association pouvant se constituer partie civile.

Les traitements ne pourront en outre pas avoir pour autre finalité que celle de préparer, exercer, suivre une action en justice, ou de faire exécuter une décision de justice rendue. La durée ne pourra excéder cette finalité (résolution amiable, extinction des voies de recours). La communication à un tiers, comme à un avocat, devra obéir aux mêmes conditions.

 **3.3. Réutilisation des données publiques de nature pénale contenues dans les décisions de justice**

 **3.3.1. Option 1 (écartée) : Maintien du droit existant**

Actuellement l’[article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n° 78-17 ne permet pas à des entreprises du secteur de la technologie juridique ( _Legal technology_ ) proposant des logiciels de services juridiques de traiter des données pénales à des fins commerciales par exemple.

Il apparaît par conséquence de prévoir une base légale pour prévoir une telle réutilisation au regard des besoins croissants de ce secteur d’activité92.

 **3.3.2. Option 2 (retenue) : Permettre une réutilisation des données de nature pénale dans le respect du droit à la protection des données**

Pour les motifs énoncés ci-dessus, il est fait le choix de clarifier dans une disposition législative le cadre de la réutilisation des données contenues dans les décisions de justice.

Il s’agit d’apporter de la sécurité juridique aux réutilisateurs, tout en rappelant les obligations qui leur incombent en tant que responsables de traitement par l’insertion de cette disposition dans le texte fondateur en matière de protection de données à caractère personnel.

Il est en outre rappelé la mention de l’interdiction de traitement visant à la ré-identification des personnes concernées comme limite à la réutilisation, ainsi que le prévoit déjà la loi pour une République numérique du 27 octobre 2016.

**_4\. Analyse des Impacts de la disposition envisagée_**

 ** _4.1. Impacts juridiques_**

Le présent article du projet de loi prévoit, à l’[article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n° 78-17, trois nouvelles dérogations à l’interdiction de traitement des données de nature pénale par des personnes autres que l’autorité publique :

\- personnes collaborant au service public de la justice (article 9-1°) ;,

\- personnes physiques ou morales, en tant que victimes ou mises en cause, aux fins de leur permettre de préparer et le cas échant, d'exercer et de suivre une action en justice (article 9-2°) ;

\- réutilisateurs des informations publiques figurant dans des décisions de justice (article 9-5°).

**_4.2. Impacts sur les services judiciaires_**

L’impact sur les services judiciaires devrait _a priori_ est neutre puisque les traitements pour la gestion du contentieux étaient déjà mis en œuvre en pratique grâce à la réserve d’interprétation du Conseil constitutionnel dans sa décision du 29 juillet 2004.

**_4.3. Impacts sur les particuliers_**

L’ouverture des décisions de justice prévue par la loi pour une République numérique trouvera à s’appliquer aux données d’infractions. Cette disposition législative permettra aux acteurs de la _Legaltech_ de réutiliser ces données. Les décisions de justice n’étant pas encore toutes diffusées en open data, il n’est pas possible d’évaluer les impacts précis de la réutilisation des données d’infraction.

En tout état de cause, la connaissance du droit par les citoyens justiciables ne pourra en être qu’améliorée, les réutilisateurs évolueront dans un cadre juridique sécurisé et surtout, la protection des personnes dont les données à caractère personnel apparaissent dans des décisions de justice sera davantage garantie.

**_4.4.Impact sur les entreprises_**

La possibilité pour des personnes morales de droit privé de mettre en œuvre des traitements de données d’infraction afin de leur permettre de préparer et le cas échant, d'exercer et de suivre une action en justice en tant que victime est de nature en renforcer le suivi de leurs dossiers contentieux et précontentieux. La fonction juridique de leur organisation en sera renforcée.

En outre, l’ouverture des décisions de justice prévue par la loi pour une [République numérique du 6 octobre 2016](/loi-2016-1321) trouvera à s’appliquer aux données d’infractions. Cette disposition législative permettra aux acteurs du secteur de la technologie juridique de réutiliser ces données. Les décisions de justice n’étant pas encore toutes diffusées en _open data_ , il n’est pas possible d’évaluer les impacts précis de la réutilisation des données d’infraction.

**_5\. Consultation et modalités d’application_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

Les textes réglementaires suivants devront être pris sur le fondement de la loi :

<table>
<tr>
<td>

**Articles du PJL renvoyant à des mesures réglementaires**

</td>
<td>

**Nature du texte réglementaire**

</td>
<td>

**Objet du texte réglementaire**

</td></tr>
<tr>
<td>

[Article 11](/pjl-490/titre-ii/chapitre-iv/article-11)

</td>
<td>

[Décret en Conseil d’Etat après avis de la CNIL](/avis-conseil-etat-393836)

</td>
<td>

Définition des catégories de personnes morales de droit privé collaborant au service public de la justice pouvant traiter des données mentionnées à l’[article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n° 78-17

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

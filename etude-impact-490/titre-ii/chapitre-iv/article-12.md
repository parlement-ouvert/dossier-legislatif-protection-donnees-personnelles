<!-- TITLE: ARTICLE 12 -->
<!-- SUBTITLE: TRAITEMENTS ARCHIVISTIQUES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**_1\. Cadre général_**

 ** _1.1. Etat des lieux et diagnostic_**

L’actuel [article 36](/loi-78-17/chapitre-v/section-1/article-36) de la loi n° 78-17 du 6 janvier 1978 modifiée permet la conservation des données à caractère personnel au-delà de la « durée de conservation » dans le traitement initial, lorsque celle-ci s’inscrit dans des finalités historiques, statistiques ou scientifiques.

Lorsque ces données ont la qualité d’archives publiques au regard de l’[article L. 211-4](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019202826&cidTexte=LEGITEXT000006074236&dateTexte=20180114) du code du patrimoine, l’article 36 de la loi du 6 janvier 1978 renvoie à l’[article L. 212-3](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019202832&cidTexte=LEGITEXT000006074236&dateTexte=20180114) du même code. Cet article précise les conditions de la sélection des données devant être conservées à titre définitif ou pouvant être éliminées. La sélection se fait d’un commun accord entre l’organisme détenteur des données et l’administration des archives du ministère de la culture. A l’issue de cette sélection, les données destinées à être conservées à titre définitif sont transférées dans les services publics d’archives compétents.

Ces données y sont conservées et traitées (classement, inventaire). Elles sont communiquées dans les conditions prévues aux [articles L. 213-1 à L. 213-3](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006159942&cidTexte=LEGITEXT000006074236&dateTexte=20180114) du code du patrimoine aux personnes qui en font la demande dans le cadre de recherches historiques, statistiques, scientifiques ou administratives. Les modalités d’accès sont alignées sur celles qui s’appliquent aux documents administratifs en vertu des dispositions du code des relations entre le public et l’administration. Les documents couverts par des secrets ou mettant en cause des intérêts protégés par la loi ne sont communicables qu’à l’expiration de délais prévus à l’article L. 213-2 du code du patrimoine, par exemple 75 ans à compter de la date des documents pour les minutes des notaires ; 25 ans après le décès ou 120 ans après la naissance pour les documents couverts par le secret médical, etc.

Ces traitements sont dispensés de formalités préalables dès lors que leur finalité se limite à assurer la gestion des archives dans le cadre du livre II du code du patrimoine, relatif aux archives.

 ** _1.2. Cadre constitutionnel_**

Dans sa [décision du 9 juillet 2008](http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2008/2008-566-dc/decision-n-2008-566-dc-du-9-juillet-2008.17216.html)93, le Conseil constitutionnel a jugé, à propos de l’[article 58](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006069199#LEGIARTI000019200604) de l’ordonnance n° 58-1067 du 7 novembre 1958 modifiée portant loi organique sur le Conseil constitutionnel, que :« _Les articles L. 211-3, L. 212-1, L. 212-2, L. 212-3, L. 212-4, L. 213-3, L. 214-1, L. 214-3, L. 214-4, L. 214-5, L. 214-9 et L. 214-10 du code du patrimoine s’appliquent aux archives qui procèdent de l’activité du Conseil constitutionnel. Ces archives peuvent être librement consultées à l’expiration du délai fixé au 1° du I de l’article L. 213-2 du même code_ ».

Récemment, le Conseil constitutionnel a consacré le droit d’accès aux documents d’archives publiques découlant de l'article 15 de la [Déclaration des droits de l’homme et du citoyen de 1789](https://www.legifrance.gouv.fr/Droit-francais/Constitution/Declaration-des-Droits-de-l-Homme-et-du-Citoyen-de-1789). Il a précisé qu’ _: «Il est loisible au législateur d'apporter à ce droit des limitations liées à des exigences constitutionnelles ou justifiées par l'intérêt général, à la condition qu'il n'en résulte pas d'atteintes disproportionnées au regard de l'objectif poursuivi._ (…) » 94.

 ** _1.3. Cadre conventionnel_**

Le [règlement (UE) 2016/679](/reglement-2016-679) autorise la conservation des données au-delà de la durée du traitement initial lorsqu’elles sont traitées à des fins archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique ou à des fins statistiques, sous réserve de la mise en œuvre de conditions et garanties appropriées ([article 5](/reglement-2016-679/chapitre-ii/article-5).1.e).

Les traitements à des fins de recherche scientifique ou historique, ou à des fins statistiques, sont mis en œuvre par les chercheurs, les universités et les organismes de recherche.

Ils peuvent également être mis en œuvre par des organismes de droit privé qui produisent ou collectent de la documentation et des archives privées afin d’en assurer la transmission aux générations futures et d’en permettre l’exploitation dans le cadre de recherches historiques, scientifiques ou statistiques. Entrent dans cette dernière catégorie des organismes tels que le Mémorial de la Shoah, la Fondation Jean Jaurès, les services d’archives des Églises, de partis politiques ou d’associations et fondations telles ATD-Quart Monde, la fondation Abbé Pierre ou l’association Génériques, association qui se consacre à l’étude de l’histoire de l’immigration.

Les traitements à des fins archivistiques ne peuvent être mis en œuvre que par des services qui ont « _l’obligation légale de collecter, de conserver, d’évaluer, d’organiser, de décrire, de communiquer, de mettre en valeur, de diffuser des archives qui sont à conserver à titre définitif dans l’intérêt public général et d’y donner accès_ » ([considérant 158 du règlement](/reglement-2016-679/considerants/considerant-158)). En France, ils sont mis en œuvre par les services publics d’archives (Archives nationales, régionales, départementales, communales, service d’archives du ministère des affaires étrangères et du ministère des armées, etc.), qui conservent des archives publiques ou des archives d’origine privée entrées par achat, dépôt, don, legs ou dation.

Les services publics d’archives ont été juridiquement définis par le [décret n° 2017-719 du 2 mai 2017](https://www.legifrance.gouv.fr/eli/decret/2017/5/2/MCCB1632109D/jo/texte) relatif aux services publics d’archives, aux conditions de mutualisation des archives numériques et aux conventions de dépôt d’archives communales95.

L’[article L. 211-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006845560&cidTexte=LEGITEXT000006074236&dateTexte=20180114) du code du patrimoine précise que « _la conservation des archives est organisée dans l’intérêt public tant pour les besoins de la gestion et de la justification des droits des personnes physiques ou morales, publiques ou privées, que pour la documentation historique de la recherche_ ». Les documents et données archivés constituent donc des sources pour la recherche en histoire, en sciences humaines et dans les autres secteurs de la recherche, mais peuvent aussi être nécessaires dans le cadre de recherches administratives. En effet, des documents, même anciens, peuvent avoir sur le temps long ou retrouver à la faveur d’événements ou de décisions une valeur administrative et probatoire. C’est ainsi par exemple que les archives de la Seconde Guerre mondiale, qui n’étaient depuis plusieurs décennies plus exploitées que par les historiens, ont été nécessaires pour indemniser, dans les années 1990 et 2000, les personnes spoliées pendant le conflit.

Les données traitées à ces fins dérogent au droit à l’effacement prévu à l’[article 17](/reglement-2016-679/chapitre-iii/section-3/article-17) du règlement, dérogation d’application directe.

Conformément à l’[article 89](/reglement-2016-679/chapitre-ix/article-89) du règlement, les Etats membres peuvent également prévoir, pour les traitements à des fins de recherche scientifique ou historique, ou à des fins statistiques, des dérogations au droit d’accès de la personne concernée ([article 15](/reglement-2016-679/chapitre-iii/section-2/article-15) du même règlement), au droit de rectification ([article 16](/reglement-2016-679/chapitre-iii/section-3/article-16)), au droit à la limitation du traitement ([article 18](/reglement-2016-679/chapitre-iii/section-3/article-18)), au droit d’opposition ([article 21](/reglement-2016-679/chapitre-iii/section-4/article-21)). Les traitements archivistiques pour l’intérêt public bénéficient des mêmes dérogations, auxquelles s’ajoutent les dérogations à l’obligation de notification de l’effacement de données ([article 19](/reglement-2016-679/chapitre-iii/section-3/article-19)) et au droit à la portabilité ([article 20](/reglement-2016-679/chapitre-iii/section-3/article-20)).

**_2\. Objectifs et necessite de legiferer_**

 ** _2.1. Objectifs poursuivis_**

Les dérogations qui figurent à l’[article 89](/reglement-2016-679/chapitre-ix/article-89) du règlement permettent aux services publics d’archives de continuer à assurer leurs missions, de conserver et de mettre à disposition des chercheurs, et notamment des historiens et des archives intègres.

La dérogation au droit de rectification (([article 16](/reglement-2016-679/chapitre-iii/section-3/article-16))) préserve le caractère intègre et authentique des données. Elle implique également les dérogations au droit d’opposition ([article 21](/reglement-2016-679/chapitre-iii/section-4/article-21)) et au droit à la limitation du traitement ([article 18](/reglement-2016-679/chapitre-iii/section-3/article-18)) qui pourraient sinon empêcher le traitement de fonds d’archives, dossiers ou documents (collecte, inventaire), et en interdire l’utilisation.

Ces dérogations sont nécessaires à l’exercice de leurs missions par les services publics d’archives dont les spécificités sont rappelées par la [déclaration de l’UNESCO de 2011](https://www.ica.org/sites/default/files/UDA_June2012_web_FR.pdf) : « _Les archives consignent les décisions, les actions et les mémoires. Les archives constituent un patrimoine unique et irremplaçable transmis de génération en génération. Les documents sont gérés dès leur création pour en préserver la valeur et le sens. Sources d’informations fiables pour une gouvernance responsable et transparente, les archives jouent un rôle essentiel dans le développement des sociétés en contribuant à la constitution et à la sauvegarde de la mémoire individuelle et collective. L’accès le plus large aux archives doit être maintenu et encouragé pour l’accroissement des connaissances, le maintien et l’avancement de la démocratie et des droits de la personne, la qualité de vie des citoyens (…) C’est pourquoi nous nous engageons à travailler de concert, pour que : (…) les archives soient gérées et conservées dans des conditions qui en assurent l’authenticité, l’intégrité et la plus grande marge d’utilisation (…)_. »

Elles sont également nécessaires aux chercheurs à qui elles garantissent l’accès à une documentation intègre, non modifiée, soumise à l’analyse critique et à l’exploitation que leur imposent leurs méthodes et leur éthique professionnelles.

La dérogation au droit d’accès ([article 15](/reglement-2016-679/chapitre-iii/section-2/article-15)) a été inscrite dans le règlement dans la mesure où ce droit implique de pouvoir identifier les personnes mentionnées dans l’ensemble des documents et données, ce qui est impossible pour les milliers de kilomètres de dossiers papier archivés et pour les données versées dans les services d’archives dans des formats et selon des modalités qui ne permettent pas de les interroger dans les mêmes conditions que celles qui prévalent lorsque ces données sont exploitées dans la finalité initiale. En effet, les données sont souvent transférées dans les services d’archives « à plat », hors contexte logiciel d’origine.

Si les personnes concernées ne disposent pas du droit d’accès au sens du règlement, elles bénéficient en revanche du droit d’accès déterminé par le [code des relations entre le public et l’administration et le code du patrimoine](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000031366350&dateTexte=20180114). L’[article L. 311-6](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033218964&cidTexte=LEGITEXT000031366350&dateTexte=20180114) de ce code donne aux personnes un droit d’accès aux documents administratifs qui les concernent, même si ceux-ci sont couverts par un secret ou un intérêt protégé par la loi.

Dans les services publics d’archives, ce droit d’accès est permis par l’existence d’instruments de recherche qui décrivent, de manière synthétique, le contenu des fonds d’archives, dossiers et fichiers en s’appuyant sur des normes internationales de description archivistique (ISAD (G), ISAAR (CPF), ICA-ISDF, ICA-ISDIAH).

La dérogation au droit à la portabilité ([article 20](/reglement-2016-679/chapitre-iii/section-3/article-20)) est nécessaire dans la mesure où ce droit représenterait une charge excessivement lourde pour les services d’archives, dont les données sont conservées « à plat » dans des formats différents de ceux qui sont utilisés au stade du traitement initial.

La dérogation à l’obligation de notification en ce qui concerne la rectification ou l’effacement de données à caractère personnel ou la limitation du traitement ([article 19](/reglement-2016-679/chapitre-iii/section-3/article-19)) est la conséquence des dérogations au droit à l’effacement, à la rectification et à la limitation du traitement.

**_2.2. Nécessité de legiferer_**

Il convient d’inscrire en droit national les dérogations d’application non directe relatives aux traitements à des fins archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques. Ces dérogations, prévues à l’[article 89](/reglement-2016-679/chapitre-ix/article-89) du règlement, ont été défendues par la France lors de la négociation de ce dernier.

Les traitements à des fins archivistiques dans l’intérêt public, mis en œuvre par les services publics d’archives en application du code du patrimoine, sont aujourd’hui dispensés de formalités préalables en application de l’actuel [article 36](/loi-78-17/chapitre-v/section-1/article-36) de la loi n° 78-17.

Les dérogations nécessaires à la mise en œuvre de ces traitements doivent être inscrites dans la loi pour pouvoir leur apporter des garanties quant aux modalités de leur mise en œuvre.

**_3\. Options_**

Les conditions et garanties appropriées relatives aux traitements à des fins archivistiques dans l’intérêt public, mis en œuvre par les services publics d’archives, sont constituées par l’ensemble des dispositions législatives et réglementaires applicables aux archives publiques et aux archives d’origine privée conservées par les services publics d’archives (plus de 300 dispositions) : règles de sélection, de collecte, de mutualisation ; conditions et modalités d’accès (notamment délais de communicabilité) ; conditions de diffusion sur Internet et de réutilisation des données.

Ces conditions sont fixées par le [livre II](https://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000032860025&idSectionTA=LEGISCTA000006159940&cidTexte=LEGITEXT000006074236&dateTexte=20180114) du code du patrimoine, par le code des relations entre le public et l’administration, ainsi que par plusieurs dizaines de dispositions législatives et réglementaires sectorielles relatives à la communication et à la réutilisation des données (code électoral, code de la santé publique, livre des procédures fiscales, code de l’environnement, etc.).

Ces conditions sont également apportées par le respect d’un large corpus de normes, notamment :

\- Normes de description signalées au point 2.1

\- NF Z42-013 – Archivage électronique.

\- NF Z44-022 relative à la modélisation des données pour l’archivage.

\- PR NF EN 16893 - Conservation du patrimoine culturel - Nouveaux sites et bâtiments destinés au stockage et à l'utilisation de collections

\- NF ISO 11799 Mai 2016 - Information et documentation - Exigences pour le stockage des documents d'archives et de bibliothèques - Information et documentation - Prescriptions pour le stockage des documents d'archives et de bibliothèques

\- [ISO/TR 19814:2017](https://www.iso.org/fr/standard/66263.html) - Information et documentation — Gestion des fonds et collections pour les archives et les bibliothèques

Ces conditions sont suffisantes pour ne pas devoir être précisées par décret, s’agissant de traitements mis en œuvre les seuls services publics d’archives dans le cadre de leur mission de service public.

Les traitements à des fins de recherche scientifique ou historique, ou à des fins statistiques, sont en revanche hétérogènes, mis en œuvre par des responsables de traitement aux profils variés et peuvent avoir des finalités différentes (intérêt public, intérêt privé). Ces traitements doivent pouvoir bénéficier de dérogations partielles ou totales aux articles 15, 16, 18 et 21, dont les conditions et garanties appropriées devront être définies précisément. Un décret en Conseil d’État, pris après avis de la CNIL, pourra préciser les dérogations nécessaires, ainsi que les conditions et garanties appropriées.

Ce décret précisera les mesures techniques et organisationnelles qui permettront d’assurer le respect du principe de minimisation des données.

 ** _4\. analyse des impacts de la disposition envisagée_**

 ** _4.1. Impacts juridiques_**

La disposition projetée permet de maintenir l’état actuel du droit en matière de gestion et d’exploitation des archives. Il modifie l’[article 36](/loi-78-17/chapitre-v/section-1/article-36) de la loi n° 78-17 pour faire référence aux conditions d’application de la limitation des droits prévues par le règlement (UE) 2016/679.

**_4.2. Impact sur les finances publiques_**

La disposition projetée garantit le maintien des conditions actuelles de collecte, de conservation, de traitement et de communication des archives, et, de ce fait, n’induit pas de charges supplémentaires.

**_4.3. Impacts sur les particuliers_**

La disposition projetée garantit aux particuliers et aux générations futures l’accès à des archives intègres.

**_4.4. Impacts sur les collectivités territoriales_**

Les dispositions projetées garantissent le maintien des conditions actuelles d’exercice de la fonction archives par les collectivités territoriales. Elles ne généreront de ce fait pas de charge supplémentaire pour les services d’archives dont se sont dotées les collectivités (101 services d’archives départementales, 13 services d’archives régionales et plus de 700 services d’archives communales et intercommunales).

**_4.5. Impact sur les entreprises_**

S’agissant d’archives publiques « définitives » dont la gestion ne peut pas être confiée à un opérateur privé, il n’y a pas d’impact sur les entreprises.

**_5\. Consultations et modalités d’application_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

Le Conseil national d’évaluation des normes a été consulté et a rendu un avis favorable lors de sa séance du 30 novembre 2017.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-iv/article-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: ARTICLES 16 ET 17 -->
<!-- SUBTITLE: MODALITES D’EXERCICE DES VOIES DE RECOURS -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**_1\. Etat des lieux et Diagnostic_**

 ** _1.1. Etat des lieux_**

 **1.1.1 Les voies de recours offertes à la personne concernée**

Une personne concernée par un traitement de données dispose de deux voies de recours pour faire valoir ses droits : elle peut déposer un recours auprès de la Commission nationale de l’informatique et des libertés ou auprès de la juridiction compétente (pénale, civile ou administrative).

L’[article 11](/loi-78-17/chapitre-iii/article-11) 2° c) de la loi n°78-17 prévoit ainsi que la Commission nationale de l’informatique et des libertés reçoit les réclamations, pétitions et plaintes relatives à la mise en œuvre des traitements et informe leurs auteurs des suites données à celles-ci. Ce droit d’introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés est désormais consacré à l’[article 77](/reglement-2016-679/chapitre-viii/article-77) du règlement (UE) 2016/679.

Les décisions de la Commission nationale de l’informatique et des libertés au titre de ses missions de contrôle ou de régulation sont susceptibles d’être l’objet d’un recours devant le Conseil d’Etat statuant en premier et dernier ressort ([article R.311-1 4°](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006449855&cidTexte=LEGITEXT000006070933&dateTexte=20100223) du code de justice administrative). L’[article 46](/loi-78-17/chapitre-vii/article-46) de la loi n°78-17 précise à cet égard que : « _les décisions prononçant une sanction peuvent faire l'objet d'un recours de pleine juridiction devant le Conseil d'Etat._ ».

La [loi n° 2016-1547 du 18 novembre 2016](https://www.legifrance.gouv.fr/eli/loi/2016/11/18/JUSX1515639L/jo) de modernisation de la justice du XXIe siècle a également introduit une nouvelle modalité de recours juridictionnel en créant une action de groupe en matière de protection des données à caractère personnel ouverte devant le juge judiciaire et le juge administratif. Ce recours, sans mandat, permet de solliciter du juge la cessation d’un manquement ([article 43 ter](/loi-78-17/chapitre-v/section-2/article-43-ter) de la loi n°78-17). Ce recours ne permet pas, en revanche, d’exercer une action en réparation.

L’[article 80](/reglement-2016-679/chapitre-viii/article-80).1 du règlement (UE) 2016/679 permet aux personnes concernées de mandater des organisations, des organismes ou des associations pour exercer leurs droits visées aux articles [77](/reglement-2016-679/chapitre-viii/article-77), [78](/reglement-2016-679/chapitre-viii/article-78) et [79](/reglement-2016-679/chapitre-viii/article-79) avec la possibilité de l’étendre au droit d’obtenir réparation si le droit de l’État membre le prévoit.

**1.1.2 Les voies de recours offertes à la Commission nationale de l’informatique et des libertés**

La Commission nationale de l’informatique et des libertés a également la faculté de saisir la justice, cette faculté est cependant limitée.

Ainsi, le e du 2° de l’[article 11](/loi-78-17/chapitre-iii/article-11) de la loi n°78-17 prévoit qu’elle peut répondre aux demandes d’avis formulées par les juridictions. L’article 1er du projet de loi vise à compléter cet article pour prévoir que la CNIL peut présenter des observations devant toute juridiction à l’occasion d’un litige relatif à l’application du règlement et de la loi du 6 janvier 1978.

Elle a également la possibilité de dénoncer au procureur de la République les infractions à la loi informatique et libertés, prévues aux _articles [226-16](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033219732&cidTexte=LEGITEXT000006070719&dateTexte=20180114) à [226-24](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000020630874) du code pénal_ (article 11, 2°, e).

Enfin, le président de la Commission nationale de l’informatique et des libertés peut demander, par référé, à la juridiction compétente d’ordonner, le cas échéant sous astreinte, toute mesure de sécurité nécessaire en cas d’atteinte grave et immédiate aux droits et libertés ([article 45](/loi-78-17/chapitre-vii/article-45)-III de la loi n°78-17).

**1.1.3 La compétence de la CNIL en cas de transferts de données vers des pays tiers**

L’[article 70](/loi-78-17/chapitre-xii/article-70) de la loi n°78-17 octroie à la Commission nationale de l’informatique et des libertés une prérogative en matière de transfert de données vers des Etats non membres de l’Union européenne.

Si la Commission des Communautés européennes a constaté qu'un Etat non membre de l’Union européenne n'assure pas un niveau de protection suffisant à l'égard d'un transfert, la Commission nationale de l'informatique et des libertés, saisie d'une déclaration doit délivrer un récépissé avec la mention de l'interdiction de procéder au transfert des données vers cet Etat.

Lorsqu'elle estime qu'un Etat non membre de l’Union européenne n'assure pas un niveau de protection suffisant à l'égard d'un transfert de données, la Commission nationale de l'informatique et des libertés doit en informer sans délai la Commission européenne et peut enjoindre au responsable du traitement de suspendre le transfert des données vers cet Etat. Si la Commission européenne constate que l'Etat vers lequel le transfert est envisagé assure un niveau de protection suffisant, la Commission nationale de l'informatique et des libertés notifie au responsable du traitement la cessation de la suspension du transfert. Si la Commission européenne constate que l'Etat vers lequel le transfert est envisagé n'assure pas un niveau de protection suffisant, la Commission nationale de l'informatique et des libertés notifie au responsable du traitement l'interdiction de procéder au transfert de données à caractère personnel à destination de cet Etat.

Ces dispositions ont vocation à perdre leur portée dès lors que le régime des déclarations auprès de la Commission nationale de l’informatique et des libertés est supprimé dans le cadre de la mise en conformité de la loi n° 78-17 au règlement (UE) 2016/679.

En revanche, il est proposé, dans le cadre du présent projet de loi, de créer une nouvelle voie de recours pour la Commission nationale de l’informatique et des libertés dans le cadre des transferts de données internationaux, afin de tirer les conséquences de l’arrêt du 6 octobre 2015, [par lequel la Cour de justice de l’Union européenne a jugé]((https://curia.europa.eu/jcms/upload/docs/application/pdf/2015-10/cp150117fr.pdf)) : « _il incombe au législateur national de prévoir des voies de recours permettant à l’autorité nationale de contrôle concernée de faire valoir les griefs qu’elle estime fondés devant les juridictions nationales afin que ces dernières procèdent, si elles partagent les doutes de cette autorité quant à la validité de la décision de la Commission, à un renvoi préjudiciel aux fins de l’examen de la validité de cette décision_ »107.

 ** _1.2. Cadre constitutionnel_**

La nouvelle voie de recours instituée en faveur de la Commission nationale de l’informatique et des libertés tire les conséquences d’un arrêt de la Cour de justice de l’Union européenne.

Il s’agit de permettre à la Commission, lorsqu’elle est saisie d’une réclamation dirigée contre un responsable de traitement ou un sous-traitant dans le cadre d’un transfert de données vers un Etat non membre de l’Union européenne, de demander au Conseil d’État d’ordonner, dans l’attente de l’appréciation par la Cour de justice de l’Union européenne de la validité de l’acte approuvé par la Commission européenne ayant permis le transfert, la suspension ou la cessation du transfert de données en cause, le cas échéant sous astreinte, lorsque la Commission nationale de l'informatique et des libertés estime fondés les griefs avancés relatifs à la protection des droits et libertés d’une personne à l’égard du traitement de ses données. Si le Conseil d’Etat partage les doutes de la Commission sur la validité de cet acte, il devra poser une question préjudicielle à la Cour de justice de l’Union européenne en application de l’article 267 du [Traité sur le fonctionnement de l’Union européenne](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=uriserv:OJ.C_.2016.202.01.0001.01.FRA&toc=OJ:C:2016:202:TOC#C_2016202FR.01004701).

La disposition ainsi prévue est conforme au cadre constitutionnel national. En effet, le Conseil constitutionnel a rappelé à plusieurs reprises la compétence de la Cour de justice de l’Union européenne pour se prononcer sur la validité et l’interprétation d’un acte adopté par une institution de l’Union européenne108.

**_1.3. Cadre conventionnel_**

1.3.1 L’[article 58](/reglement-2016-679/chapitre-vi/section-2/article-58).5 du règlement (UE) 2016/679 dispose que : « _Chaque État membre prévoit, par la loi, que son autorité de contrôle a le pouvoir de porter toute violation du présent règlement à l'attention des autorités judiciaires et, le cas échéant, d'ester en justice d'une manière ou d'une autre, en vue de faire appliquer les dispositions du présent règlement_ ».

Le [chapitre VIII](/reglement-2016-679/chapitre-viii) relatif aux voies de recours, à la responsabilité et aux sanctions prévoit notamment le droit d'introduire une réclamation auprès d'une autorité de contrôle (article 77), le droit à un recours juridictionnel effectif contre une autorité de contrôle (article 78), le droit à un recours juridictionnel effectif contre un responsable du traitement ou un sous-traitant (article 79), la représentation des personnes concernées (article 80) et un droit à réparation et responsabilité (article 82).

L’[article 55](/directive-2016-680/chapitre-viii/article-55) de la directive prévoit également le droit pour les personnes concernées de mandater un organisme, une organisation ou une association à but non lucratif pour exercer les mêmes réclamation et recours que ceux prévus par le règlement.

1.3.2 La Cour de justice de l’Union européenne a consacré une nouvelle voie de droit par le [considérant 65 de la décision Maximillian Schrems du 6 octobre 2015, C-362/14](http://curia.europa.eu/juris/document/document.jsf?text=&docid=169195&pageIndex=0&doclang=FR&mode=lst&dir=&occ=first&part=1&cid=322520) :

« _63 […] lorsqu’une personne, dont les données à caractère personnel ont été ou pourraient être transférées vers un pays tiers ayant fait l’objet d’une décision de la Commission au titre de l’article 25, paragraphe 6, de la [directive 95/46](http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:31995L0046), saisit une autorité nationale de contrôle d’une demande relative à la protection de ses droits et libertés à l’égard du traitement de ces données et conteste, à l’occasion de cette demande, comme dans l’affaire au principal, la compatibilité de cette décision avec la protection de la vie privée et des libertés et droits fondamentaux des personnes, il incombe à cette autorité d’examiner ladite demande avec toute la diligence requise. _

_64\. Dans l’hypothèse où ladite autorité parvient à la conclusion que les éléments avancés au soutien d’une telle demande sont dépourvus de fondement et rejette, de ce fait, cette dernière, la personne ayant introduit ladite demande doit, ainsi qu’il résulte de l’article 28, paragraphe 3, second alinéa, de la directive 95/46, lu à la lumière de l’article 47 de la Charte, avoir accès aux voies de recours juridictionnelles lui permettant de contester une telle décision lui faisant grief devant les juridictions nationales. Eu égard à la jurisprudence citée aux points 61 et 62 du présent arrêt, ces juridictions sont tenues de surseoir à statuer et de saisir la Cour d’une procédure de renvoi préjudiciel en appréciation de validité lorsqu’elles considèrent qu’un ou plusieurs moyens d’invalidité avancés par les parties ou, le cas échéant, soulevés d’office sont fondés (voir, en ce sens, arrêt T & L Sugars et Sidul Açúcares/Commission, [C-456/13 P, EU:C:2015:284](http://curia.europa.eu/juris/celex.jsf?celex=62013CJ0456&lang1=fr&type=TXT&ancre=), point 48 et jurisprudence citée)._

 _65\. Dans l’hypothèse contraire, où ladite autorité estime fondés les griefs avancés par la personne l’ayant saisie d’une demande relative à la protection de ses droits et libertés à l’égard du traitement de ses données à caractère personnel, cette même autorité doit, conformément à l’article 28, paragraphe 3, premier alinéa, troisième tiret, de la directive 95/46, lu à la lumière notamment de l’article 8, paragraphe 3, de la Charte, pouvoir ester en justice. À cet égard, il incombe au législateur national de prévoir des voies de recours permettant à l’autorité nationale de contrôle concernée de faire valoir les griefs qu’elle estime fondés devant les juridictions nationales afin que ces dernières procèdent, si elles partagent les doutes de cette autorité quant à la validité de la décision de la Commission, à un renvoi préjudiciel aux fins de l’examen de la validité de cette décision._ »

Les arrêts rendus par la Cour de justice de l’Union, notamment lorsqu’elle statue à titre préjudiciel comme c’est le cas dans l’arrêt précité, ont une force exécutoire en vertu de l’article 280 du Traité sur le fonctionnement de l’Union européenne.

**_1.4. Eléments de droit comparé_**

La loi allemande de protection des données109 a prévu deux dispositions spécifiques, l’une (section 20) concernant le droit au recours juridictionnel prévu à l’article 78 du règlement110, l’autre (section 21) aménageant la voie de recours définie dans l’arrêté Schrems précité. Dans ce dernier cas, si l’autorité de contrôle estime qu'une décision d'adéquation de la Commission européenne dont dépend la validité d'une décision de l'autorité de contrôle méconnaît la loi, l'autorité de contrôle suspend sa procédure et introduit une demande de décision de justice. Si le juge estime que la décision de la commission est valide, il doit en juger ainsi. Dans les autres cas, il doit pouvoir poser une question préjudicielle en application de l’article 267 du Traité sur le fonctionnement de l’union européenne111.

L’article 173 de la loi anglaise de protection des données112 rappelle l’existence de l’action de groupe avec mandat prévu à l’article 80 du règlement. Elle utilise la marge de manœuvre prévue à l’article 80.1 pour permettre une telle action de groupe afin d’exercer un droit à réparation113.

**_2\. Objectifs et necessite de legiferer_**

 ** _2.1 Objectifs poursuivis_**

Les dispositions du présent projet de loi doivent permettre au système juridique français de se conformer aux obligations découlant du droit de l’Union européenne, tant le règlement que la jurisprudence de la Cour de justice de l’Union européenne. Cette mise en conformité doit notamment permettre de garantir des voies de droit cohérentes et effectives pour les personnes concernées et renforcer le pouvoir de contrôle de la CNIL.

Ces dispositions, combinées à la possibilité pour la CNIL introduite par l’article 1er du projet de loi de présenter des observations devant toute juridiction à l’occasion d’un litige relatif à l’application du règlement et de la loi du 6 janvier 1978 (cf. supra), visent ainsi à renforcer la capacité d’action en justice de la CNIL et garantir le respect effectif des droits des personnes concernées.

**_2.2 Nécessité de legiferer_**

D’une part, l’[article 80](/reglement-2016-679/chapitre-viii/article-80) du règlement impose aux Etats membres de prévoir un droit pour les personnes concernées de mandater un organisme, une organisation ou une association afin d'introduire une réclamation auprès d'une autorité de contrôle ([article 77](/reglement-2016-679/chapitre-viii/article-77)), d’exercer un recours juridictionnel contre une autorité de contrôle ([article 78](/reglement-2016-679/chapitre-viii/article-78)) ou contre un responsable du traitement ou un sous-traitant ([article 79](/reglement-2016-679/chapitre-viii/article-79)). Une telle obligation s’impose également en vertu de l’article 55 de la directive.

D’autres modalités de recours relèvent des marges de manœuvre des Etats membres : possibilité d’étendre le mandat défini précédemment afin d’exercer une action en réparation ([article 80](/reglement-2016-679/chapitre-viii/article-80).1), possibilité pour un organisme, une organisation ou un association, d’exercer les droits définis ci-dessus, indépendamment de tout mandat confié par une personne concernée (article 80.2).

D’autre part, l’arrêt de la Cour de Justice du 6 octobre 2015 précité impose en outre au législateur national de prévoir une voie de droit spécifique permettant à l’autorité de contrôle de faire valoir les griefs qu’elle estime fondés devant les juridictions nationales afin que ces dernières procèdent, si elles partagent les doutes de cette autorité quant à la validité d’une décision d’adéquation de la Commission européenne, à un renvoi préjudiciel aux fins de l’examen de la validité de cette décision.

**_3\. Options_**

 **3.1. Introduction d’une action de groupe avec mandat devant l’autorité de contrôle**

L’article 80.1 du règlement prévoit une obligation pour les Etats membre d’assurer le droit pour une personne concernée de mandater un organisme, une organisation ou une association à but non lucratif, qui a été valablement constitué conformément au droit national pour exercer les droits prévus aux articles 77, 78 et 79 du règlement. Une marge de manœuvre est ouverte d’étendre ce droit aux actions en réparation visé à l’article 82 du même règlement.

**3.1.1. Option 1 (écartée) : Application directe du règlement sans précision**

Afin de permettre aux personnes concernées de connaître l’existence de cette voie de recours, il a semblé important de réaffirmer la possibilité de l’action permise par le règlement, bien que d’application directe. En outre, l’article 80.1 du règlement renvoie au droit de l’Etat membre le soin de déterminer selon quelles modalités une association peut agir avec le mandat.

**3.1.2. Option 2 (écartée) : Utiliser la marge de manœuvre de l’article 80.1 du règlement (UE) 2016/679 pour introduire une action avec mandat aux fins de réparation**

La loi de modernisation de la justice du XXIème siècle qui a créé une action de groupe en cessation de manquement n’a pas prévu d’étendre le champ de cette action aux fins de réparation. Les débats parlementaires soulignent l’opposition du législateur national d’étendre l’action de groupe aux demandes en réparation.

Compte-tenu du caractère récent de l’introduction de l’action de groupe dans le droit national, il a donc été décidé de ne pas utiliser la marge de manœuvre prévue à l’article 80.2 du règlement.

**3.1.3. Option 3 (retenue) : Ouvrir l’action de groupe obligatoire prévue à l’article 80.1 aux associations déjà prévues au IV de l’[article 43 ter](/loi-78-17/chapitre-v/section-2/article-43-ter)**

Ainsi qu’il a été dit au point 3.1.1, il revient au droit national de préciser les conditions selon lesquelles un organisme, une organisation ou une association peut, par mandat, exercer les droits prévus aux articles 77, 78 et 79 du règlement. Il en est de même en vertu de l’[article 55](/directive-2016-680/chapitre-viii/article-55) de la directive, s’agissant des traitements relevant du champ d’application de cette dernière (nouveau [chapitre XIII](/loi-78-17/chapitre-xiii) de la loi n°78-17).

Afin de ne pas complexifier le régime, il est proposé de renvoyer aux organismes et associations déjà mentionnés au IV de l’article 43 ter de la loi n°78-17.

**3.2. Introduction d’une représentation sans mandat pour exercer le droit de réclamation et les droits de recours juridictionnel**

 **3.2.1. Option 1 (écartée) : Utiliser la marge de manœuvre prévue à l’article 80.2 du règlement pour l’ensemble des droits**

L’article 43 ter de la loi n° 78-17, introduit par la loi n° 2016-1547 du 18 novembre 2016 de modernisation de la justice du XXIe siècle, en prévoyant une action de groupe, sans mandat, pour faire cesser un manquement par un responsable de traitement subi par plusieurs personnes physiques, organise déjà la marge de manœuvre prévue à l’article 80.2 du règlement s’agissant du droit à un recours effectif contre un responsable de traitement ou un sous-traitant (article 78).

Compte-tenu du caractère récent de ces nouvelles dispositions, il n’a pas été décidé d’étendre la marge de manœuvre prévue à l’article 80.2 du règlement pour l’exercice du droit de réclamation auprès de l’autorité de contrôle (article 77) et du droit de recours juridictionnel contre l’autorité de contrôle (article 78).

Le droit actuel permet déjà, en tout état de cause, à une personne concernée d’introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés (article 11 2°-c) et à toute personne physique ou morale, de contester une décision prise par la Commission au titre de sa mission de contrôle devant le Conseil d’Etat (article R. 311-1 du code de justice administrative).

**3.2.2. Option 1 (retenue) : Ne pas modifier le droit existant**

Au regard des éléments évoqués précédemment, il a été décidé de ne pas faire usage de la marge de manœuvre prévue à l’article 80.2 du règlement, l’article 43 ter de la loi n° 78-17 permettant déjà de satisfaire une partie des possibilités offertes par le règlement.

Pour résumer, les choix opérés par le présent projet de loi sont les suivants :

<table>
<tr>
<td>

**Type de recours**

</td>
<td>

**Représentation des parties avec mandat**
(art. 80.1)

</td>
<td>

**Représentation des parties sans mandat**
(art. 80.2)

</td></tr>
<tr>
<td>

**Droit de réclamation** (art. 77)

</td>
<td>

Nouvel article art. 43 quater

</td>
<td>

X

</td></tr>
<tr>
<td>

**Droit de recours contre l’autorité de contrôle** (art. 78)

</td>
<td>

Nouvel article art. 43 quater

</td>
<td>

X

</td></tr>
<tr>
<td>

**Droit de recours contre un responsable de traitement** (art. 79)

</td>
<td>

Nouvel article art. 43 quater

</td>
<td>

Actuel art. 43 ter

</td></tr>
<tr>
<td>

**Droit à réparation** (art. 82)

</td>
<td>

X

</td>
<td>

Sans objet

</td></tr></table>

**3.2 : Aménagement d’une voie de recours définie par l’arrêt CJUE - C-362/14**

 **3.2.1. Option 1: Absence de mesure législative**

Le droit actuel ne prévoit pas la voie de recours définie dans l’arrêt Maximillian Schrems du 6 octobre 2015 précité, à savoir la possibilité pour la Commission nationale de l’informatique et des libertés de faire valoir les griefs qu’elle estime fondés devant les juridictions nationales afin que ces dernières procèdent, si elles partagent les doutes de cette autorité quant à la validité de la décision d’adéquation de la Commission européenne, à un renvoi préjudiciel aux fins de l’appréciation de la validité de cette décision par la Cour de justice de l’Union européenne.

Une absence de mesure législative exposerait la France à une procédure en manquement, en application des articles 258 à 260 du Traité sur le fonctionnement de l’Union européenne, dès lors qu’il s’agirait d’une méconnaissance d’une obligation découlant d’un texte européen (en l’occurrence, la directive 95/46/CE du Parlement européen et du Conseil du 24 octobre 1995 relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données), telle qu’interprétée par la Cour de justice.

 **3.2.2. Option 2 (écartée) : Donner un pouvoir global d’ester en justice à la Commission nationale de l’informatique et des libertés**

L’[article 58](/reglement-2016-679/chapitre-vi/section-2/article-58).5 du règlement permet de doter l’autorité de contrôle d’un pouvoir global d’ester en justice. Or la Commission nationale de l’informatique et des libertés, autorité administrative indépendante et non autorité publique indépendante, ne dispose pas de la personnalité morale (article 2 de la [loi n° 2017-55 du 20 janvier 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033897475&categorieLien=id) portant statut général des autorités administratives indépendantes et des autorités publiques indépendantes) lui permettant de droit d’agir en justice.

Toutefois, ainsi qu’il a été rappelé dans la première partie, la Commission nationale de l’informatique et des libertés dispose des moyens pour faire appliquer les dispositions du règlement et de la loi n°78-17 : outre les mesures de sanction qu’elle peut prendre pour faire cesser la méconnaissance par les responsables de traitement de leurs obligations, sanctions qui sont sensiblement renforcées par le règlement, elle peut, en cas d’atteinte grave et immédiate aux droits et libertés agir par la voie du référé auprès de la juridiction compétente.

Par ailleurs, outre la situation évoquée dans l’arrêt Schrems précité nécessitant l’instauration d’une voie de recours _ad hoc_ , il n’a pas été identifié d’autres situations qui nécessiteraient un pouvoir plus étendu accordé à la Commission nationale de l’informatique et des libertés.

**3.2.3. Option 3 (retenue) : Introduction d’une nouvelle voie de recours dans le cadre des transferts internationaux de données**

Le [nouvel article 45 quinquies](/pjl-490/titre-ii/chapitre-vi/article-17 "typo, 43") de la loi n° 78-17 aménage ainsi une voie de recours pour la Commission nationale de l'informatique et des libertés dans le cadre des transferts de données vers un Etat membre de l’Union européenne, conformément à l’arrêt Schrems précité.

Ainsi, lorsqu’elle sera saisie d’une réclamation dirigée contre un responsable de traitement ou un sous-traitant, la Commission nationale de l'informatique et des libertés pourra demander au Conseil d’Etat d’ordonner, dans l’attente de l’appréciation par la Cour de justice de l’Union européenne, qu’il aura saisi à titre préjudiciel s’il partage des doutes de la CNIL, de la validité d’une décision d’adéquation de la Commission européenne prise sur le fondement de l’[article 45](reglement-2016-679/chapitre-v/article-45) du règlement (UE) 2016/679, la suspension ou la cessation du transfert de données en cause, le cas échéant sous astreinte. La CNIL devra alors assortir ses conclusions d’une demande de question préjudicielle à la Cour de justice de l’Union européenne en vue d’apprécier la validité de tels actes adoptés par la Commission européenne.

Afin de donner un plein effet utile à cette voie de recours, et dans un souci de cohérence et de protection accrue des données à caractère personnel, il a été fait le choix d’élargir cette voie de recours à l’ensemble des actes pris par la Commission européenne autorisant ou approuvant les garanties appropriées dans le cadre des transferts de données pris sur le fondement de l’[article 46](/reglement-2016-679/chapitre-v/article-46) du règlement (UE) 2016/679, et pas uniquement les décisions d’adéquation : clause-type de protection des données, code de conduite, mécanisme de certification. Cette nouvelle voie de droit est également étendue à l’égard des décisions d’adéquation de la Commission européenne prises sur le fondement de l’[article 36](/directive-2016-680/chapitre-v/article-36) de la directive (UE) 2016/680, lorsque le transfert de données en cause ne constitue pas une opération de traitement effectuée par une juridiction dans l'exercice de sa fonction juridictionnelle.

Le choix du Conseil d’Etat pour connaître de cette nouvelle voie de recours, par dérogation à la compétence de premier ressort des tribunaux administratifs, a été fait en considération d’une bonne administration de la justice. Actuellement, le Conseil d’État est compétent pour connaître, en premier et dernier ressort, des décisions prises par la Commission nationale de l’informatique et des libertés au titre de sa mission de contrôle. La décision prise par la Commission, après que la Cour de justice de l’Union européenne se sera prononcée sur la question préjudicielle dont elle aura été saisie, relèvera de la compétence du Conseil d’Etat en vertu de l’[article R. 311-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006449855&cidTexte=LEGITEXT000006070933&dateTexte=20100223) du code de justice administrative. En outre, ce nouveau recours, qui porte sur une question sensible relative à la protection des données à caractère personnel transférées vers un pays tiers, suppose qu’il soit statué rapidement.

**_4\. Analyse des impacts des dispositions envisagées_**

 ** _4.1. Impacts juridiques_**

L’action de groupe avec mandat devant la Commission nationale de l’informatique et des libertés est de nature à limiter le nombre de réclamations individuelles lorsque la violation d’un responsable de traitement ou d’un sous-traitant concerne de nombreuses personnes concernées.

Le projet de loi crée une nouvelle voie de recours devant le Conseil d’État. Cet aménagement permettra une voie de recours effective et rapide dans l’attente de la décision de la Cour de justice de l’Union européenne se prononçant sur la validité d’un acte de la Commission européenne permettant un transfert de données international.

 ** _4.2. Impacts sur les services judiciaires_**

L’action de groupe avec mandat devant la Commission nationale de l’informatique et des libertés est de nature à diminuer le nombre de décisions de la Commission nationale de l’informatique et des libertés, puisque plusieurs personnes concernées par un traitement feront l’objet d’une seule décision. Par conséquent, le nombre de recours contentieux devrait être diminué, puisqu’une seule décision de la Commission nationale de l’informatique et des libertés et non plus des dizaines, voire des centaines sur le même traitement fera l’objet d’un seul et même recours devant le juge. L’impact sur les services judiciaires devrait donc être une diminution du nombre de contentieux. A ce stade, en l’absence d’action de groupe en matière de protection des données, il n’est pas possible d’évaluer l’impact de la création de cette action par la loi n° 2016-1547 du 18 novembre 2016 de modernisation de la justice du XXIème siècle.

La création de la voie de recours prévue par l’arrêté Schrems précité qui concerne les transferts de données internationaux devrait concerner un contentieux exceptionnel. En effet, à ce jour, la Cour de justice de l’Union européenne n’a eu à se prononcer sur la validité d’une décision d’adéquation suite à un renvoi par une juridiction nationale dans le cadre d’une demande d’une autorité de contrôle qu’une seule fois, en l’occurrence dans le cadre de l’arrêt Schrems. L’impact sur les services judiciaires devrait donc être minime puisque exceptionnel.

**_4.3 Impacts sur les particuliers_**

Ces mesures créant de nouvelles voies de recours permettront de faire respecter les droits et obligations issus du règlement (UE) 2016/679 et de la loi n°78-17 relative à l’informatique et aux libertés.

 ** _4.4 Impact sur les entreprises_**

Les dispositions, en renforçant les voies de recours offertes aux personnes concernées, augmentent en contrepartie le risque contentieux pour les entreprises responsables de traitement ou leurs sous-traitants.

En revanche, ainsi qu’il a été dit précédemment, le projet de loi n’a pas prévu de faire usage de la marge de manœuvre prévue à l’[article 80](/reglement-2016-679/chapitre-viii/article-80).1 du règlement en étendant l’action du groupe dans le cadre du droit à un recours juridictionnel contre un responsable de traitement ou un sous-traitant au droit à réparation prévu à l’[article 82](/reglement-2016-679/chapitre-viii/article-82) du même règlement.

**_5\. Consultations_**

La Commission de l’informatique et des libertés a été consultée sur ces articles.

Le Conseil supérieur des tribunaux administratifs et des cours administratives d’appel a été saisi en application de l’[article L. 232-3](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000034333474&cidTexte=LEGITEXT000006070933&dateTexte=20180114) du code de justice administrative, dès lors que la nouvelle voie de recours prévue par l’arrêté Schrems, en confiant la compétence au Conseil d’Etat pour connaître de ce recours, déroge à la compétence de premier ressort des tribunaux administratifs. Il a émis un avis favorable lors de sa séance du 7 novembre 2017.

La commission supérieure du Conseil d’Etat a également été saisie sur cette même disposition, en application de l’[article L. 312-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033340848&cidTexte=LEGITEXT000006070933&dateTexte=20180114) du code de justice, dès lors que la nouvelle voie de recours confie au Conseil d’Etat un nouveau type de contentieux. Elle a émis un avis favorable le 1er décembre 2017.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: ARTICLE 10 -->
<!-- SUBTITLE: SOUS-TRAITANT -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**_1\. etat des lieux et Diagnostic_**

 ** _1.1. Etat des lieux_**

Le [chapitre V](/loi-78-17/chapitre-v) de la loi du 6 janvier 1978 décrit les obligations qui incombent aux responsables de traitements ([article 32 à 37](/loi-78-17/chapitre-v/section-1)), au-delà du simple respect des droits des personnes concernées consacrées dans le même chapitre ([articles 38 à 43 _ter_](/loi-78-17/chapitre-v/section-2)). Les obligations incombant aux responsables de traitement sont détaillées dans une première section.

Les obligations incombant au sous-traitant, généralement le prestataire technique en charge de la mise en œuvre technique du traitement de données, sont précisées à l’[article 35](/loi-78-17/chapitre-v/section-1/article-35) de la loi n° 78-17 _82_. En vertu de cet article, le sous-traitant est regardé comme étant celui qui traite des données à caractère personnel pour le compte du responsable de traitement ([article 3](/loi-78-17/chapitre-ier/article-3)). La principale obligation du sous-traitant, au-delà de ses engagements contractuels, est le respect des conditions de sécurité, posées par l'[article 34](/loi-78-17/chapitre-v/section-1/article-34), sans pour autant que cela décharge le responsable de traitement de sa responsabilité d'y veiller. Les autres obligations pèsent, en droit, sur le responsable de traitement, à charge pour celui-ci d'exiger le respect des garanties posées par la loi par son sous-traitant dans un cadre contractuel.

L'émergence de nouveaux outils numériques, à l'instar du _cloud computing_ , a conduit cependant la Commission européenne comme les autorités de contrôle européennes, chacune en ce qui la concerne, à s'interroger sur les obligations particulières des sous-traitants.

Le [règlement (UE) 2016/679](/reglement-2016-679) et la [directive (UE) 2016/680](/directive-2016-680) qui renforcent les obligations pensant sur les responsables de traitement dans le cadre de la logique de responsabilisation, ont étendu le champ de ces obligations au sous-traitant défini comme « _la personne physique ou morale, l'autorité publique, le service ou un autre organisme qui traite des données à caractère personnel pour le compte du responsable du traitement_ »83.

A cet égard, le [considérant 81](/reglement-2016-679/considerants/considerant-81)  du règlement précise que : « _Afin que les exigences du présent règlement soient respectées dans le cadre d'un traitement réalisé par un sous-traitant pour le compte du responsable du traitement, lorsque ce dernier confie des activités de traitement à un sous-traitant, le responsable du traitement ne devrait faire appel qu'à des sous-traitants présentant des garanties suffisantes, notamment en termes de connaissances spécialisées, de fiabilité et de ressources, pour la mise en œuvre de mesures techniques et organisationnelles qui satisferont aux exigences du présent règlement, y compris en matière de sécurité du traitement._ »

 ** _1.2. Cadre conventionnel_**

Le [règlement (UE) 2016/679](/reglement-2016-679) et la [directive (UE) 2016/680](/directive-2016-680) renforcent la responsabilisation des responsables de traitement ainsi que des sous-traitants. Le [chapitre IV](/reglement-2016-679/chapitre-iv) du règlement (UE) 2016/679 précise les obligations des responsables de traitement et sous-traitants. L’[article 28](/reglement-2016-679/chapitre-iv/section-1/article-28) du règlement (UE) 2016/679 précise les principales obligations applicables spécifiquement au sous-traitant.

 ** _2\. OBJECTIFS POURSUIVIS ET NECESSITE DE LEGIFERER_**

 ** _2.1. Objectifs poursuivis_**

Le projet de loi a pour objectif de clarifier la distinction entre les dispositions d’application de la [directive (UE) 2016/680](/directive-2016-680) et les obligations directement applicables du [règlement (UE) 2016/680](/reglement-2016-679) à la charge des sous-traitants.

En complétant l’[article 35](/loi-78-17/chapitre-v/section-1/article-35) de la loi n° 78-17 par une référence aux obligations prévues par le [chapitre IV](/reglement-2016-679/chapitre-iv) du règlement (UE) 2016/679 pour les sous-traitants intervenant dans le cadre de traitement relevant de ce règlement, la disposition répond également à l’objectif de valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi84.

 ** _2.2. Nécessité de legiferer_**

La disposition proposée résulte d’une nécessité de clarification afin de préciser l’articulation des dispositions nationales de transposition de la directive (UE) 2016/680 ([article 22](/pjl-490/titre-v/article-22) du projet de loi) avec les obligations du [règlement (UE) 2016/680](/reglement-2016-679).

 ** _3\. Options_**

 **3.1.1. Option 1 (écartée) : Maintenir uniquement les dispositions de transposition**

Il aurait pu être envisagé que seules les dispositions de transposition de la [directive (UE) 2016/680](/directive-2016-680) soient présentes dans la loi ([article 19](/pjl-490/titre-iii/article-19) du projet de loi).

Le texte aurait manqué de lisibilité pour les personnes concernées et organismes quant aux obligations des sous-traitants.

 **3.1.2. Option 2 (écartée) : Etendre les obligations prévues par le règlement aux sous-traitants agissant dans le cadre de traitement relevant de la directive**

Il aurait pu être envisagé d’appliquer les mêmes obligations aux sous-traitants dans le champ de la [directive (UE) 2016/680](/directive-2016-680). Toutefois, cette directive impose une transposition qui prévoit d’autres obligations.

Cette option a donc été écartée, un article spécifique introduit par l’[article 19](/pjl-490/titre-iii/article-19) du projet de loi (article 70-10) précisant le régime applicable aux sous-traitants dans le cadre des traitements relevant de la directive.

 **3.1.3. Option 2 (retenue) : Faire une simple référence aux obligations du règlement qui s’imposent aux sous-traitants pour les traitements relevant de celui-ci**

Par souci de lisibilité, le projet de loi rappelle, à l’[article 35](/loi-78-17/chapitre-v/section-1/article-35) de la loi n° 78-17 que dans le champ d'application du règlement (UE) 2016/679, le sous-traitant respecte les conditions prévues au [chapitre IV](/reglement-2016-679/chapitre-iv) de ce règlement.

Pour les traitements ne relevant ni du règlement, ni de la directive, les dispositions de l’actuel [article 35](/loi-78-17/chapitre-v/section-1/article-35) de la loi n°78-17 trouveront à s’appliquer.

Il est enfin précisé que les mesures correctrices et sanctions susceptibles d’être prises par la CNIL en vertu de l’[article 6](/pjl-490/titre-i/chapitre-i/article-6) du projet de loi ont vocation à s’appliquer également aux sous-traitants, quel que soit le champ d’application dont relève le traitement dont ils assurent la prestation.

 ** _4\. ANALYSE DES IMPACTS DE LA DISPOSITION ENVISAGÉE_**

 ** _4.1. Impacts juridiques_**

La disposition précise l’articulation entre les dispositions de la [loi n° 78-17](/loi-78-17) et le [règlement (UE) 2016/679](/reglement-2016-679) concernant le régime de la sous-traitance des responsables de traitement.

 ** _4.2. Impacts sur les particuliers_**

Les dispositions proposées assurent une meilleure lisibilité et permettent donc aux particuliers de mieux connaitre les obligations des organismes traitant leurs données à caractère personnel lorsque ces derniers ont recours à des prestataires extérieurs.

 ** _4.3. Impacts sur les collectivités territoriales_**

Les dispositions proposées assurent une meilleure lisibilité et permettent donc aux collectivités territoriales de mieux comprendre les obligations qui pèsent sur les sous-traitants auxquels elles peuvent avoir recours dans le cadre de la mise en œuvre de traitement.

 ** _4.4. Impact sur les entreprises_**

Les dispositions proposées assurent une meilleure lisibilité et permettent donc aux entreprises de mieux comprendre leurs obligations en tant que sous-traitant d’un responsable de traitement.

Les entreprises, en tant que responsable de traitement, lorsqu’elles auront recours à un prestataire, devront respecter les dispositions de l’[article 28](/reglement-2016-679/chapitre-iv/section-1/article-28) du règlement.

**_5\. Consultation_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

Le Conseil national d’évaluation des normes a été consulté et a rendu un avis favorable sur ce projet de texte lors de sa [séance du 30 novembre 2017](/deliberation-cnil-2017-299).

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

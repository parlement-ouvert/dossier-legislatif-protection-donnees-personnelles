<!-- TITLE: ARTICLE 8 -->
<!-- SUBTITLE: CRITERE D’APPLICATION DU DROIT -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-ier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

 ** _1\. Etat des lieux et diagnostic_**

 ** _1.1. Etat des lieux_**

La question du champ d’application du [règlement (UE) 2016/679](/reglement-2016-679) constitue l’une des innovations majeures par rapport à la [directive 95/46/CE](http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:31995L0046). L’[article 3](/reglement-2016-679/chapitre-i/article-3) du règlement prévoit un double champ d’application.

D'une part, il est applicable aux traitements effectués dans le cadre des activités d'un établissement, d'un responsable du traitement ou d'un sous-traitant dès lors que celui-ci se trouve sur le territoire de l'Union, peu importe que le traitement ait lieu ou non dans l'Union. Il s’agit en quelque sorte d’un critère organique (article 3-1).

D'autre part, le règlement est également applicable selon un « critère matériel », peu important que l'établissement soit sur le territoire de l’Union, dès lors que le traitement est effectué à l'égard des résidents européens. Il suffit alors que l’offre de biens ou de services à des personnes concernées se déroule dans l’Union ou que leur comportement dans l’Union soit suivi (article 3-2).

Le règlement a donc une cohérence globale vis-à-vis des responsables de traitements présents dans ou en dehors de l'Union. Il précise à cet égard qu’il « _devrait s’appliquer aux personnes physiques, indépendamment de leur nationalité ou de leur lieu de résidence, en ce qui concerne le traitement de leurs données_ ».

Si le champ d’application du règlement (UE) 2016/679 est déjà défini par celui-ci, en revanche, le champ d’application des marges de manœuvres des États membres octroyées par le règlement doit être défini afin d’éviter des conflits de normes en cas de dispositions divergentes selon les législations nationales.

En effet, se pose la question du critère à retenir pour la législation applicable entre États membres de l’Union en cas de divergences compte tenu des choix différents dans l’exercice des marges de manœuvre permises par le règlement. Cette question n’est pas abordée par ce dernier. Les spécificités nationales peuvent concerner les droits de la personne concernée, le responsable du traitement et le sous-traitant, le transfert de données à caractère personnel vers des pays tiers ou à des organisations internationales, les autorités de contrôle indépendantes, la coopération et la cohérence, ainsi que les situations particulières de traitement des données.

Par exemple, l’[article 9](/reglement-2016-679/chapitre-ii/article-9)(4) du règlement permet une marge de manœuvre plus protectrice en ce qui concerne les traitements de données génétiques, biométriques ou concernant la santé. Si la France est plus protectrice qu’un autre Etat membre, se posera la question du droit applicable au traitement réalisé dans cet autre Etat membre et relatif à un résident en France.

Cette question pose des enjeux en termes de protection des droits fondamentaux des personnes concernées (par exemple s’agissant de l’âge du consentement des mineurs), mais également d’attractivité du territoire dès lors que la législation applicable peut constituer un critère important pour une entreprise qui souhaite s’implanter à l’étranger.

Actuellement, l’[article 5](/loi-78-17/chapitre-ier/article-5) de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés prévoit le champ d’application suivant :

_« I. - Sont soumis à la présente loi les traitements de données à caractère personnel :_

 _1° Dont le responsable est établi sur le territoire français. Le responsable d'un traitement qui exerce une activité sur le territoire français dans le cadre d'une installation, quelle que soit sa forme juridique, y est considéré comme établi ;_

 _2° Dont le responsable, sans être établi sur le territoire français ou sur celui d'un autre Etat membre de la Communauté européenne, recourt à des moyens de traitement situés sur le territoire français, à l'exclusion des traitements qui ne sont utilisés qu'à des fins de transit sur ce territoire ou sur celui d'un autre Etat membre de la Communauté européenne._

 _II. - Pour les traitements mentionnés au 2° du I, le responsable désigne à la Commission nationale de l'informatique et des libertés un représentant établi sur le territoire français, qui se substitue à lui dans l'accomplissement des obligations prévues par la présente loi ; cette désignation ne fait pas obstacle aux actions qui pourraient être introduites contre lui. »_

En vertu de cet article, le droit national s’applique lorsque le responsable est établi en France (I-1°) ou à défaut, en absence d’établissement en France ou dans l’Union européenne, lorsque le responsable recourt à des moyens de traitement sur le territoire français (I-2°). Ces critères de détermination du droit applicable ne tiennent pas compte du lieu de résidence de la personne concernée.

L’[article 8 du présent projet de loi](/pjl-490/titre-ii/chapitre-i/article-8) doit se lire en cohérence avec l’[article 3 du règlement précité](/reglement-2016-679/chapitre-i/article-3), directement applicable à compter du 25 mai 2018.

L’article propose ensuite, pour l’application des marges de manœuvres ainsi que les autres dispositions, de retenir le critère de résidence de la personne concernée, à l’exception des traitements mentionnés à l'[article 85-2 du règlement](/reglement-2016-679/chapitre-ix/article-85), en matière de la liberté d’expression et d’information qui relèveraient du critère d’établissement du responsable de traitement.

**_1.2. Cadre conventionnel_**

Le règlement a une cohérence globale vis-à-vis des responsables de traitements présents dans ou en dehors de l'Union. Il précise à cet égard qu’il « _devrait s’appliquer aux personnes physiques, indépendamment de leur nationalité ou de leur lieu de résidence, en ce qui concerne le traitement de leurs données_ » ([considérant 14](/reglement-2016-679/considerants/considerant-14)).

En ce qui concerne les marges de manœuvre, le règlement est en revanche silencieux, à l’exception de son [considérant 153](/reglement-2016-679/considerants/considerant-153) qui indique, s’agissant des exemptions au nom de la liberté d’expression et d’information (article 85), que _« Dans le cadre du traitement de données à caractère personnel uniquement à des fins journalistiques ou à des fins d'expression universitaire, artistique ou littéraire, il y a lieu de prévoir des dérogations ou des exemptions à certaines dispositions du présent règlement si cela est nécessaire pour concilier le droit à la protection des données à caractère personnel et le droit à la liberté d'expression et d'information, consacré par l'article 11 de la Charte. Tel devrait notamment être le cas des traitements de données à caractère personnel dans le domaine de l'audiovisuel et dans les documents d'archives d'actualités et bibliothèques de la presse. / (…) Lorsque ces exemptions ou dérogations diffèrent d'un État membre à l'autre, le droit de l'État membre dont relève le responsable du traitement devrait s'appliquer. Pour tenir compte de l'importance du droit à la liberté d'expression dans toute société démocratique, il y a lieu de retenir une interprétation large des notions liées à cette liberté, telles que le journalisme. »_. __ [L’article 85.1 du règlement](/reglement-2016-679/chapitre-ix/article-85), en écho à ce considérant, prévoit que les Etats membres peuvent prévoir des exemptions ou dérogations si elles sont nécessaires pour concilier le droit la protection des données et la liberté d’expression et d’information.

C’est la raison pour laquelle le projet de loi réserve en particulier cette situation qui trouvera à s’appliquer par exemple dans le domaine de la presse. __

**_1.3. Eléments de droit comparé_**

Lors des réunions relatives à la mise en œuvre du règlement qui ont eu lieu à la Commission européenne, sont apparues des positions divergentes entre les Etats membres. Certains ont annoncé qu’ils retiendraient un critère d’établissement unique pour l’ensemble des marges de manœuvres, d’autres semblent privilégier des critères combinant l’établissement et la résidence. Enfin, certains Etat préfèrent ne pas inscrire de critères dans leur projet de loi afin de laisser les règles classiques de conflits de normes s’appliquer.

Par exemple, la loi allemande (section 1 (4))62 semble privilégier formellement un critère d’établissement qui ne serait pas nécessairement celui de l’établissement principal. Outre la situation des organismes publics, la loi allemande est applicable pour les organismes privés si :

\- le responsable de traitement ou le sous-traitant traite des données personnelles en Allemagne ;

\- les données à caractère personnel sont traitées dans le contexte d’une activité d’un établissement en Allemagne ;

\- si le responsable de traitement ou le sous-traitant, bien que n’ayant pas d’établissement dans un Etat membre, relève du champ d’application du règlement.

Or l’[article 4 du règlement](/reglement-2016-679/chapitre-i/article-4) définit le « traitement » comme « _toute opération ou tout ensemble d'opérations effectuées ou non à l'aide de procédés automatisés et appliquées à des données ou des ensembles de données à caractère personnel, telles que la collecte, ** __** l'enregistrement, l'organisation, la structuration, la conservation, l'adaptation ou la modification, l'extraction, la consultation, l'utilisation, la communication par transmission, la diffusion ou toute autre forme de mise à disposition, le rapprochement ou l'interconnexion, la limitation, l'effacement ou la destruction _».

Par conséquent, dès lors qu’un responsable de traitement traite une donnée d’une personne présente en Allemagne (y compris la collecte) alors le droit allemand s’appliquera, ce qui revient à appliquer un critère semblable à celui proposé par le projet de loi.

**_2\. Objectifs et necessite de legiferer_**

 ** _2.1. Objectifs poursuivis_**

La précision dans la loi du critère d’application a pour objet de garantir une sécurité juridique pour les responsables de traitement afin de connaître quelle est la législation applicable en cas de législation différente entre Etats membres de l’Union européenne. Une telle sécurité juridique participe de l’attractivité du territoire national.

Il s’agit également de garantir la protection des données à caractère personnel des personnes présentes sur le territoire français.

**_2.2. Nécessité de legiferer_**

Le règlement ne contient aucune disposition relative aux critères d’applicabilité du droit lorsque des règles nationales précisant des marges de manœuvre permises par le règlement conduisent à des divergences de législation entre Etats membres de l’Union européenne.

Dans ces conditions, la loi nationale pourrait être silencieuse, laissant le soin aux tribunaux d’appliquer le droit applicable en cas de contentieux. Toutefois, cette option ne semble pas satisfaisante au regard, d’une part, du principe de sécurité juridique, d’autre part, des enjeux liés aux droits des personnes concernées.

**_3\. Options_**

 **3.1. Inscrire dans la loi un critère d’application**

 **3.1.1. Option 1 (écartée) : Ne rien prévoir dans la loi**

Il aurait pu être décidé de ne rien indiquer dans la loi et laisser les juridictions, en cas de contentieux, appliquer les règles classiques en cas de conflit de normes internationales. Cette option ne semblait pas satisfaisante au regard des objectifs de sécurité juridique, ce d’autant que le règlement ne contient aucune disposition sur ce point.

 **3.1.2. Option 2 (retenue) : Fixer dans la loi des critères d’application**

Par souci de sécurité juridique, il a été préféré de prévoir expressément dans la loi les critères d’application en cas de divergences de législations entre Etats membres liées aux marges de manœuvre prévues par le règlement.

A titre d’exemple, s’agissant de la marge de manœuvre permise par l’article 8 du règlement relatif à l’âge du consentement des enfants en ce qui concerne les services de la société de l'information, l'[article 3 alinéa 3 du code civil](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006419282&cidTexte=LEGITEXT000006070721) dispose que : « _Les lois concernant l'état et la capacité des personnes régissent les Français, même résidant en pays étranger_ ».

La définition de la majorité, et corrélativement de la minorité, est indubitablement à rattacher à la capacité des personnes. La détermination des actes susceptibles d'être réalisés, par exception, par le mineur semble devoir l'être également. En effet, dans le code civil, l'article 388-2 par exemple relève du titre X relatif à la minorité, la tutelle et l'émancipation, et non du titre IX relatif à l'autorité parentale, bien que l'intervention des administrateurs légaux soit nécessaire. Le critère d'application du droit français fondé sur la nationalité de l'article 3 alinéa 3 du code civil est donc actuellement applicable.

Si l'on souhaite qu'un autre critère lui soit substitué pour la question de la capacité du consentement d'un mineur au traitement des données personnelles, en l’occurrence le critère de la résidence, une disposition légale spécifique est nécessaire pour déroger à ce principe, comme par exemple l'article 311-14 du code civil pour la loi applicable à la filiation.

 **3.2. Choix du critère d’application**

 **3.2.1. Option 1 (écartée) : critère de l’établissement du responsable de traitement**

Les principes du marché intérieur du droit de l’Union européenne conduiraient à privilégier le critère de l'établissement principal du responsable du traitement, c’est-à-dire le lieu de son administration centrale dans l'Union, à moins que les décisions quant aux finalités et aux moyens du traitement de données soient prises dans un autre établissement du responsable du traitement dans l'Union et que ce dernier établissement ait le pouvoir de faire appliquer ces décisions, auquel cas l'établissement ayant pris de telles décisions est considéré comme l'établissement principal.

Le critère d’établissement aurait pour avantage premier de permettre au responsable de traitement de n’appliquer qu’un seul droit. Cela permet de réduire les charges administratives et la complexité juridique pour ce responsable.

Dans une telle hypothèse, le droit applicable aux personnes concernées pourrait varier en fonction du lieu d'établissement principal du responsable de traitement ou de son sous-traitant.

Cela reviendrait à faire application du droit d’autres Etats membres pour des traitements de données qui touchent des résidents français. Par exemple, si l’Irlande choisit, comme âge de consentement des mineurs en ce qui concerne l’offre directe de services de la société de l’information, l’âge de 13 ans, cette disposition s’appliquerait aux résidents français pour ce qui est de l’utilisation de services tels que Google ou Facebook dont le siège de leur filiale européenne se trouve en Irlande.

**3.2.2. Option 3 (retenue) : Lieu de résidence de la personne concernée**

Le critère du lieu de la collecte de la donnée est opportun lorsqu’il est préférable d’appliquer le régime national à des domaines spécifiques, notamment lorsque ce droit est plus protecteur.

Ce critère est plus protecteur pour les personnes physiques concernées, qui n’ont alors pas à s’interroger sur le droit applicable dans un autre Etat membre de l’Union, lequel n’est bien souvent pas accessible dans leur langue. Le [considérant 14](/reglement-2016-679/considerants/considerant-14) du règlement précise à cet égard que : « _La protection conférée par le présent règlement devrait s'appliquer aux personnes physiques, indépendamment de leur nationalité ou de leur lieu de résidence, en ce qui concerne le traitement de leurs données à caractère personnel._ »

En outre, une telle option est plus respectueuse de la souveraineté du droit national : dès lors qu’il s’agit d’une marge de manœuvre, les États membres appliquent leur droit lorsqu’il s’agit d’adapter ou de compléter les droits et obligations prévus par le règlement.

Dans le cas où une autorité de contrôle d’un autre Etat membre de l’Union serait autorité de contrôle chef de file (logique de « guichet unique »), celle-ci serait tenue d’appliquer le droit français s’il s’agit de constater un manquement pour un résident. Le juge du pays de l’autorité de contrôle concernée pourrait ainsi potentiellement se prononcer sur l’application et l’interprétation du droit français.

Dès lors que les Etats membres ne se sont pas entendus sur certaines dispositions et que des marges de manœuvre existent, il est cohérent que chaque Etat puisse appliquer les choix politiques que le législateur national a faits.

**3.3. Exclusion pour les traitements relatifs à la liberté d’expression et d’information**

 **3.3.1. Option 1 (écartée) : Aucune dérogation**

Il aurait pu être proposé de ne prévoir aucune dérogation pour ce type de traitement, ce qui aurait conduit à faire application du critère du lieu de résidence de la personne comme pour les autres marges de manœuvre.

Cette option a été écartée pour deux raisons.

D’une part, le [considérant 153 du règlement](/reglement-2016-679/considerants/considerant-153) précise que _« lorsque ces exemptions ou dérogations diffèrent d'un État membre à l'autre, le droit de l'État membre dont relève le responsable du traitement devrait s'appliquer »_.

D’autre part, en matière de traitements à des fins journalistiques, il est important que le droit applicable soit celui du lieu d’établissement eu égard aux exigences liées à la liberté de la presse et des médias. Ainsi, une entreprise de presse établie en France n’aura pas à respecter des prescriptions particulières d’un autre Etat membre lorsqu’elle traite des données à caractère personnel.

**3.3.2. Option 2 (retenue) : Prévoir une dérogation pour les traitements relatifs à la liberté d’expression et d’information**

Prenant en compte le [considérant 153 du règlement](/reglement-2016-679/considerants/considerant-153), le projet de loi prévoit une dérogation au critère de résidence au droit applicable qui dépendra du lieu de l’établissement.

Cette hypothèse a vocation à concerner par exemple les traitements de données à caractère personnel à des fins de journalisme et leur cession à des fins commerciales. A cet titre, la Cour de justice a jugé que : _« L’[article 9 de la directive 95/46](http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:31995L0046) doit être interprété en ce sens que les activités mentionnées à la première question, sous a) à d), concernant des données provenant de documents publics selon la législation nationale, doivent être considérées comme des activités de traitement de données à caractère personnel exercées “aux seules fins de journalisme“ au sens de cette disposition, si lesdites activités ont pour seule finalité la divulgation au public d’informations, d’opinions ou d’idées, ce qu’il appartient à la juridiction nationale d’apprécier_ »63.

Ainsi, par exemple, un site internet de presse établi en France n’aura pas à respecter des prescriptions particulières d’un autre Etat membre lorsqu’elle traite des données à caractère personnel. Dans le cadre d’une procédure de coopération engagée dans le cadre d’un traitement transfrontalier à l’égard d’un responsable de traitement établi en France, c’est le droit national qui s’appliquerait, notamment s’agissant des pouvoirs de la CNIL. Ainsi, le responsable de traitement pourrait opposer le secret des sources journalistiques qui s’appliquera en France (comme le prévoit l’[article 4 du projet de loi](/pjl-490/titre-i/chapitre-i/article-4)), ce qui ne serait pas forcément le cas dans un autre Etat membre de l’Union européenne, l’article 90 du règlement permettant à chaque Etat membre d’adopter des règles spécifiques en matière d’obligations de secret.

 ** _4\. analyse des impacts de la disposition envisagée_**

 ** _4.1. Impacts juridiques_**

La disposition proposée permet de garantir une plus grande sécurité et une meilleure accessibilité juridique, tant pour les entreprises et les particuliers, qui connaîtront le critère permettant de définir le droit applicable en cas de législations nationales divergentes au sein de l’Union européenne, que pour les juridictions, appelées à trancher un conflit de normes qui pourrait se présenter dans le cadre de recours dont elles seraient saisies.

**_4.2. Impacts sur les particuliers_**

La disposition proposée vise à assurer une protection plus importante des droits des personnes concernées lorsque le législateur ou le pouvoir règlementaire décide de mettre en œuvre une marge de manœuvre.

**_4.3. Impact sur les entreprises_**

Pour les responsables de traitement et sous-traitants ayant un établissement en France, cette mesure n’a aucun impact.

Pour les autres responsables de traitement ayant un établissement à l’étranger (sur le territoire de l’Union européenne ou à l’extérieur), il leur appartiendra de respecter les dispositions de la loi française lorsque celle-ci aura prévu d’adapter ou de compléter les droits et obligations prévus par le règlement.

**_5\. Consultation_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ii/chapitre-ier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

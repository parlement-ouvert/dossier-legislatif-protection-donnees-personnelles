<!-- TITLE: TABLEAU SYNOPTIQUE DES CONSULTATIONS OBLIGATOIRES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/tableau-recapitulatif-des-textes-dapplication-du-projet-de-loi" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<table>
<tr>
<td>

**Articles**

</td>
<td>

**Objet de l'article**

</td>
<td>

**Consultations menées**

</td></tr>
<tr>
<td>

Ensemble des dispositions du projet de loi

</td>
<td>

</td>
<td>

Commission nationale de l’informatique et des libertés

Avis du 30 novembre 2017

</td></tr>
<tr>
<td>

Article 6

Article 9

Article 10

Article 12

Article 14

</td>
<td>

Mesures correctrices et sanctions

Simplification des formalités préalables à la mise en œuvre des traitements

Sous-traitants

Traitements archivistiques

Déclaration administrative automatisée

</td>
<td>

Conseil national d’évaluation des normes

Avis favorable du 30 novembre 2017

</td></tr>
<tr>
<td>

Article 17

</td>
<td>

Dispositions relatives à l’aménagement d’une voie de recours définie par l’arrêt CJUE –C-362/14

</td>
<td>

Conseil supérieur des tribunaux administratifs et cours administratives d’appel

Avis favorable du 7 novembre 2017

</td></tr>
<tr>
<td>

Article 17

</td>
<td>

Dispositions relatives à l’aménagement d’une voie de recours définie par l’arrêt CJUE –C-362/14

</td>
<td>

Commission supérieure du Conseil d’État

Avis favorable du 1er décembre 2017

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/tableau-recapitulatif-des-textes-dapplication-du-projet-de-loi" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

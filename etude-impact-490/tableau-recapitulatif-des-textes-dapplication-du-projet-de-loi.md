<!-- TITLE: TABLEAU RÉCAPITULATIF DES TEXTES D’APPLICATION DU PROJET DE LOI -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/introduction-generale" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/tableau-synoptique-des-consultations-obligatoires" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<table>
<tr>
<td>

**ARTICLES**

</td>
<td>

**TEXTES D’APPLICATION**

</td>
<td>

**ADMINISTRATION COMPETENTE**

</td></tr>
<tr>
<td>

Article 1er (6°)

</td>
<td>

Décret en Conseil d'État pris après avis de la Commission nationale de l’informatique et des libertés

</td>
<td>

Ministère de la Justice

</td></tr>
<tr>
<td>

Article 4 (4°)

</td>
<td>

Décret en Conseil d'État pris après avis de la Commission nationale de l’informatique et des libertés

</td>
<td>

Ministère de la Justice

</td></tr>
<tr>
<td>

Article 5 II (14e alinéa)

</td>
<td>

Décret en Conseil d'État pris après avis de la Commission nationale de l’informatique et des libertés

</td>
<td>

Ministère de la Justice

</td></tr>
<tr>
<td>

Article 6 III

</td>
<td>

Décret en Conseil d'État

</td>
<td>

Ministère de la Justice

</td></tr>
<tr>
<td>

Article 9 I, (2e alinéa)

</td>
<td>

Décret en Conseil d'État pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés

</td>
<td>

Ministère de la Justice

</td></tr>
<tr>
<td>

Article 9 (I, 7e alinéa)

</td>
<td>

Arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l'informatique et des libertés,

</td>
<td>

Ministère de l’Economie et des Finances

</td></tr>
<tr>
<td>

Article 11 (2°)

</td>
<td>

Décret en Conseil d'État après avis de la Commission nationale de l’informatique et des libertés

</td>
<td>

Ministère de la Justice

</td></tr>
<tr>
<td>

Article 13 (15e alinéa)

</td>
<td>

Décret en Conseil d'État

</td>
<td>

Ministère des Solidarités et de la Santé

</td></tr>
<tr>
<td>

Article 13 (19e alinéa)

</td>
<td>

Arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l'informatique et des libertés

</td>
<td>

Ministère des Solidarités et de la Santé

</td></tr>
<tr>
<td>

Article 13 (42e alinéa)

</td>
<td>

Décret en Conseil d'État pris après avis de la Commission nationale de l’informatique et des libertés

</td>
<td>

Ministère des Solidarités et de la Santé

</td></tr>
<tr>
<td>

Article 15

</td>
<td>

Décret en Conseil d’État pris après avis de la Commission nationale de l’informatique et des libertés

</td>
<td>

Ministère des Armées

</td></tr>
<tr>
<td>

Article 19 (30e alinéa)

</td>
<td>

Décret en Conseil d’État pris après avis de la Commission nationale de l’informatique et des libertés

</td>
<td>

Ministère de la Justice

</td></tr>
<tr>
<td>

Article 20

</td>
<td>

Ordonnance (article 38 de la Constitution)

</td>
<td>

Ministère de la Justice

</td></tr>
<tr>
<td>

Article 24

</td>
<td>

Arrêté

</td>
<td>

Ministère de la Justice

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/introduction-generale" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/tableau-synoptique-des-consultations-obligatoires" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: ARTICLE 20 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

 ** _1\. Etat des lieux et diagnostic_**

 ** _1.1. Etat des lieux_**

La [loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés](/loi-78-17) constitue le texte fondateur et unique en matière de protection des données à caractère personnel.

La mise en conformité de cette loi avec le « paquet européen » des données à caractère personnel adopté le 27 avril 2016 conduit à intégrer dans ce texte des dispositions qui relèvent de champs d’application différents ([règlement (UE) 2016/679](/reglement-2016-679) d’une part, [directive (UE) 2016/680](/directive-2016-680) d’autre part), tout en maintenant des dispositions qui concernent également des traitements ne relevant pas du droit de l’Union européenne (fichiers relatifs à la défense nationale par exemple). L’[article 21](/pjl-490/titre-v/article-21) du présent projet de loi contient déjà certaines mesures de coordination rendues directement nécessaires par les modifications apportées à [la loi n° 78-17](/loi-78-17).

Le présent projet de loi permet, à travers les titres I à III, au Parlement de débattre sur les mesures les plus importantes relatives à la transposition de la directive et aux marges de manœuvres permise par le règlement afin de laisser la représentation nationale se prononcer sur les grandes orientations en matière de protection des données.

Une fois ces choix opérés et la loi promulguée, un travail plus technique de mise en cohérence et de coordination de la loi n° 78-17 sera nécessaire, afin notamment de mieux articuler les dispositions selon le champ d’application dont elles relèvent). Il est proposé d’effectuer ce travail par ordonnance.

A cet égard, il sera rappelé que la loi n° 78-17 a fait l’objet de nombreuses modifications par ordonnance136.

Les modifications apportées par ordonnances ont ainsi par exemple permis la mise en conformité du droit national aux dispositions de la [directive 2009/136/CE](http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2009:337:0011:0036:fr:PDF) concernant le service universel et les droits des utilisateurs au regard des réseaux et services de communications électronique et de la [irective 2002/58/CE](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32002L0058) concernant le traitement des données à caractère personnel et la protection de la vie privée dans le secteur des communications électroniques.

 ** _1.2. Cadre constitutionnel_**

Le Conseil Constitutionnel considère que le législateur peut autoriser le Gouvernement à tirer les conséquences, par ordonnances, de la loi qu’il a adoptée et assurer ainsi la coordination des dispositions législatives en vigueur avec celles de cette loi137.

Il juge également de manière constante que « _si l'[article 38 de la Constitution](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000571356&idArticle=LEGIARTI000006527512&dateTexte=&categorieLien=cid) fait obligation au Gouvernement d'indiquer avec précision au Parlement, afin de justifier la demande qu'il présente, la finalité des mesures qu'il se propose de prendre par voie d'ordonnance ainsi que leur domaine d'intervention, il n'impose pas au Gouvernement de faire connaître au Parlement la teneur des ordonnances qu'il prendra en vertu de cette habilitation ;_ »138.

Le Conseil constitutionnel a estimé qu’est assez précise une demande autorisant le Gouvernement à transposer des directives, y compris si elles sont susceptibles d’être adoptées au cours du délai d’habilitation139.

Enfin, il sera rappelé que l’objectif d'intelligibilité et d'accessibilité de la loi que l’article d’habilitation souhaite atteindre par une réécriture par ordonnance de la loi n° 78-17 constitue un objectif de valeur constitutionnelle que le Conseil constitutionnel rattache aux [articles 4, 5, 6 et 16 de la Déclaration des droits de l’homme et du citoyen de 1789](https://www.legifrance.gouv.fr/Droit-francais/Constitution/Declaration-des-Droits-de-l-Homme-et-du-Citoyen-de-1789)140, à l’instar de la technique de codification. Le Conseil constitutionnel considère en effet que : « _la codification répond à l'objectif de valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi, qui découle des articles 4, 5, 6 et 16 de la Déclaration de 1789 ; qu'en effet l'égalité devant la loi énoncée par l'article 6 de la Déclaration et « la garantie des droits » requise par son article 16 pourraient ne pas être effectives si les citoyens ne disposaient pas d'une connaissance suffisante des normes qui leur sont applicables ; qu'une telle connaissance est en outre nécessaire à l'exercice des droits et libertés garantis tant par l'article 4 de la Déclaration, en vertu duquel cet exercice n'a de bornes que celles déterminées par la loi, que par son article 5, aux termes duquel « tout ce qui n'est pas défendu par la loi ne peut être empêché, et nul ne peut être contraint à faire ce qu'elle n'ordonne pas_ »141.

**_2\. Objectifs poursuivis et necessite de legiferer_**

 ** _2.1 Objectifs poursuivis_**

L’habilitation que le Gouvernement sollicite par le présent projet de loi répond à un objectif de qualité de la législation et de meilleure lisibilité et intelligibilité de la [loi n° 78-17](/loi-78-17) grâce à une nouvelle architecture qui sera divisée en titres, dispositions communes et selon la finalité des traitements (règlement, directive, hors champ du droit de l’Union européenne).

Il s’agit également d’assurer la conformité du droit national avec le règlement et la directive, en permettant d’une part, d’abroger les dispositions redondantes dans le règlement et la loi, sauf si la mesure est commandée pour la clarté, d’autre part, de modifier les dispositions de la loi n° 78-17 ou d’autres textes qui ne seraient plus en conformité avec le nouveau droit de la protection des données.

**_2.2 Nécessité de legiferer_**

Le « paquet européen protection des données » constitue une modification importante du droit de la protection des données à caractère personnel.

Dans le respect des dispositions votées aux titres I à III du projet de loi, et sans préjudice des mesures de coordination déjà prévues par l’[article 21](/pjl-490/titre-v/article-21) du projet de loi, la [loi n°78-17](/loi-78-17) nécessite une réécriture afin d’apporter les corrections formelles et les adaptations nécessaires à la simplification, à la cohérence et à l'intelligibilité de cette loi consécutive à sa mise en conformité au [règlement (UE) 2016/679](/reglement-2016-679) et à la transposition de la [directive (UE) 2016/280](/directive-2016-680).

Il conviendra aussi, sur le fondement des dispositions votées par le Parlement et des dispositions du règlement directement applicable de mettre en cohérence avec ces changements l’ensemble de la législation applicable à la protection des données à caractère personnel. Il s’agit également d’apporter les modifications qui seraient rendues nécessaires pour assurer le respect de la hiérarchie des normes et la cohérence rédactionnelle des textes, harmoniser l'état du droit, de remédier aux éventuelles erreurs et omissions résultant de la présente loi, et d’abroger les dispositions devenues sans objet.

Enfin, l’habilitation est prévue pour l’adaptation et les extensions éventuelles à l’outre-mer des dispositions résultant de ces modifications.

**_3\. Options_**

 **3.1. Habilitation à prendre par ordonnance des mesures législatives de réécriture de la loi n°78-17**

 **3.1.1. Option 1 (écartée) Maintenir la loi n° 78-17 dans sa rédaction qui résultera de l’adoption du présent projet de loi**

Les [titres I à III du présent projet de loi](/pjl-490) assurent un niveau de transposition et de mise en conformité minimum au regard du « paquet européen ».

Certaines dispositions, en particulier celles relatives aux définitions, aux droits des personnes concernées et des exceptions ne correspondront plus à l’état du droit en mai 2018, date d’application du « paquet européen ».

Certes, le principe de primauté du droit de l’Union européenne142 permettrait au juge national et à l’autorité de contrôle d’écarter, pour les traitements relevant du champ du règlement et de la directive, les dispositions de la loi nationale contraires aux textes européens.

Cette solution n’apparaît toutefois pas satisfaisante au regard de la prévisibilité et de l’intelligibilité de la norme.

**3.1.2. Option 2 (écartée) Mettre en conformité l’ensemble de la législation nationale par ordonnance**

Cette option qui avait été envisagée par amendement lors de la première lecture du projet de loi pour une République numérique a été écartée.

Le Gouvernement a en effet fait le choix de soumettre à la représentation nationale les choix permis par le droit européen s’agissant d’un domaine aussi important que la protection des données à caractère à caractère personnel

 **3.1.3. Option 3 (retenue) : Habilitation à réécrire la loi n° 78-17 dans le respect des dispositions des titres I à III du texte proposé**

Afin d’améliorer l’intelligibilité de la loi, la réécriture de l’ensemble de [la loi n° 78-17](/loi-78-17) en cohérence avec le règlement et la [directive (UE) 2016/680](/directive-2016-680) semble nécessaire, sans préjudice des mesures de coordination déjà prévues par le projet de loi.

A ce titre, le Conseil constitutionnel a jugé que répondait à cet objectif, l’élaboration d’un nouveau plan d’un code afin de le rendre plus accessible à ses utilisateurs, en regroupant dans des blocs homogènes des dispositions jusqu'alors éparses143.

Différents textes spécifiques doivent également être mis en cohérence, une grande partie des traitements de données à caractère personnel étant en effet prévus dans des textes sectoriels renvoyant à des dispositions de la [la loi n° 78-17](/loi-78-17) amenées à être modifiées ou abrogées par le règlement à compter du 25 mai 2018.

Enfin, afin d’assurer l’effectivité du droit à la protection des données à caractère personnel sur l’ensemble du territoire, il apparait nécessaire de procéder à l’adaptation et aux extensions utiles dans les collectivités d’outre-mer.

 ** _4\. Analyse des impacts de la disposition envisagée_**

 ** _4.1 Impacts juridiques_**

L’[article 20 du projet de loi](/pjl-490/titre-iv/article-20) autorise le Gouvernement, dans le respect des dispositions prévues aux [titres I à III du projet de loi](/pjl-490), à prendre par voie d’ordonnance les mesures relevant du domaine de la loi nécessaires :

\- pour réécrire l’ensemble de la [loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés](/loi-78-17) afin d’apporter les corrections formelles et les adaptations nécessaires à la simplification, à la cohérence et à l'intelligibilité de cette loi consécutive à sa mise en conformité au [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 et à la transposition de la [directive (UE) 2016/680](/directive-2016-680) du Parlement européen et du Conseil du 27 avril 2016 telle que résultant de la présente loi ;

\- pour mettre en cohérence avec ces changements l’ensemble de la législation applicable à la protection des données à caractère personnel, apporter les modifications qui seraient rendues nécessaires pour assurer le respect de la hiérarchie des normes et la cohérence rédactionnelle des textes, harmoniser l'état du droit, remédier aux éventuelles erreurs et omissions résultant de la présente loi, et abroger les dispositions devenues sans objet ;

\- À l’adaptation et aux extensions à l’outre-mer des dispositions prévues aux 1° et 2°, ainsi qu’à l’application en Nouvelle-Calédonie, à Wallis-et-Futuna en Polynésie française, à Saint-Barthélemy, à Saint-Pierre-et-Miquelon et dans les Terres australes et antarctique françaises.

**_4.2 Impacts sur les particuliers_**

La réécriture de la [loi n° 78-17](/loi-78-17) doit permettre une meilleure accessibilité et donc une meilleure compréhension des droits des personnes concernées et des obligations incombant aux responsables de traitement.

**_4.3 Impacts sur les entreprises_**

A l’instar de l’impact attendu pour les particuliers, cette disposition doit permettre une meilleure accessibilité et donc une meilleure compréhension des obligations incombant aux responsables de traitement et aux sous-traitants.

**_5\. Consultation et modalités d’application_**

 ** _5.1. Consultation_**

La Commission nationale de l’informatique et des libertés a été consultée sur cet article.

Elle le sera également sur le projet d’ordonnance prévue par l’[article 20](/pjl-490/titre-iv/article-20) du projet de loi.

 ** _5.2. Modalités d’application_**

Les textes réglementaires suivants devront être pris sur le fondement de la loi :

<table>
<tr>
<td>

**Articles du PJL renvoyant à des mesures réglementaires**

</td>
<td>

**Nature du texte réglementaire**

</td>
<td>

**Objet du texte réglementaire**

</td></tr>
<tr>
<td>

Article 20

</td>
<td>

Ordonnance

</td>
<td>

Habilitation à améliorer l’intelligibilité de la législation applicable à la protection des données

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

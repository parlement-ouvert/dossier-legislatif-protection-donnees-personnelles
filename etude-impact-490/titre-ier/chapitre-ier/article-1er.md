<!-- TITLE: ARTICLE 1ER -->
<!-- SUBTITLE: MISSIONS DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ier/chapitre-ier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ier/chapitre-ier/articles-2-et-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**_1\. Etat des lieux et diagnostic_**

 **1.1. Cadre général**

La Commission nationale de l'informatique et des libertés (CNIL), créée par la [loi n° 78-17 du 6 janvier 1978](/loi-78-17) relative à l'informatique, aux fichiers et aux libertés, a été la première autorité administrative indépendante dont la France s’est dotée et l’une des premières à travers le monde en matière de protection des données à caractère personnel.

La Commission nationale de l'informatique et des libertés occupe un rôle central dans la protection des données à caractère personnel en France. Ses missions, prévues à l’[article 11](/loi-78-17/chapitre-iii/article-11) de la loi n°78-17, sont les suivantes :

\- information des personnes concernées et des responsables de traitement ;

\- fonction consultative auprès du Gouvernement ;

\- participation à la définition du cadre normatif sur la protection des données personnelles ;

\- mise en conformité des traitements ;

\- avis, approbation ou création d'instruments de « droit souple » ;

\- certification de méthodologies d'anonymisation, notamment en vue de la réutilisation des bases de données publiques mises en ligne ;

\- conduite de la réflexion sur les problèmes éthiques et les questions de société soulevés par l'évolution des technologies numériques ;

\- promotion de l'utilisation des technologies protectrices de la vie privée, notamment les technologies de chiffrement des données.

**1.2. Cadre du droit de l’Union européenne**

 **1.2.1** Le [considérant 117 du règlement (UE) 2016/679](/reglement-2016-679/considerants/considerant-117) et le [considérant 75 de la directive (UE) 2016/680](/directive-2016-680/considerants/considerant-75) ** __** indiquent que l’autorité de contrôle est « _un élément essentiel de la protection des personnes physiques à l'égard du traitement des données à caractère personnel_ ». Cette autorité de contrôle exerce ses missions et pouvoirs en toute indépendance. L’[article 41 (3) de la directive](/directive-2016-680/chapitre-vi/section-1/article-41) prévoit en outre que l’autorité de contrôle instituée au titre du règlement peut également être compétente dans le champ d’application de la directive (UE) 2016/680. Au regard de ces différents éléments, la Commission nationale de l'informatique et des libertés constitue ainsi une autorité de contrôle au sens du « paquet européen » de la protection des données.

 **1.2.2** Les [articles 57 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vi/section-2/article-57) et [46 de la directive (UE) 2016/680](/directive-2016-680/chapitre-vi/section-2/article-46) prévoient les missions des autorités de contrôle. L’ensemble des missions prévues par la directive se retrouve dans la liste prévue par le règlement à l’exception du g) du 1 de l’article 46 de la directive. Cet exception permet, dans le champ de la directive, à l’autorité de contrôle de vérifier la licéité du traitement et d’informer la personne concernée dans un délai raisonnable de l'issue de la vérification, ou des motifs ayant empêché sa réalisation. D’une manière globale, le règlement est relativement large sur les missions de l’autorité de contrôle dès lors que son article 57(1)v prévoit que cette autorité « _s'acquitte de toute autre mission relative à la protection des données_ ». En ce qui concerne la possibilité pour l’autorité de contrôle de prendre des mesures de droit souple, l’article 35.5 du règlement prévoit que : « _L'autorité de contrôle peut aussi établir et publier une liste des types d'opérations de traitement pour lesquelles aucune analyse d'impact relative à la protection des données n'est requise. L'autorité de contrôle communique cette liste au comité_ ».

Le présent projet de loi modifie l’[article 11 de loi n° 78-17](/loi-78-17/chapitre-iii/article-11) pour confier à la CNIL de nouvelles missions prévues par le règlement, ou compléter certaines missions déjà exercées.

 **1.2.2.1. Sur les mesures de droit souple**

La Commission nationale de l’informatique et des libertés peut déjà prendre plusieurs mesures de droit souple, ce qui permet de la considérer comme une « autorité de régulation » de la protection des données personnelles. L’[article 11 de loi n° 78-17](/loi-78-17/chapitre-iii/article-11) prévoit qu’elle peut délivrer des labels7 et d’une manière générale, « _pour l'accomplissement de ses missions, la commission peut procéder par voie de recommandation et prendre des décisions individuelles ou réglementaires dans les cas prévus par la présente loi_ »8.

Comme l’indique M. Jean-Marc Sauvé dans l’avant-propos de l’Etude du Conseil d’Etat sur le droit souple : « _Il semble, tout au contraire, que le droit souple puisse être l’oxygénation du droit et favoriser sa respiration dans les interstices du corset parfois un peu trop serré des sources traditionnelles de la règle. Il peut accompagner la mise en œuvre du “droit dur“, comme il peut dans certains cas s’y substituer pour la mise en œuvre de politiques publiques suffisamment définies et encadrées par la loi_. »9.

Ainsi, les autorités administratives indépendantes utilisent largement le droit souple, sous forme de recommandations ou de lignes directrices, dans le cadre de leur rôle de régulation.

Pour Mme Isabelle Falque-Pierrotin, présidente de la CNIL10, le droit souple est nécessaire en matière de protection des données à caractère personnel pour trois raisons :

\- compte tenu des limites même de la norme générale dans un environnement en pleine mutation ;

\- le traitement des données personnelles est de plus en plus déterritorialisé ;

\- la protection des données personnelles est en train de changer de paradigme.

Sur ce dernier point, elle indique, à propos du règlement (UE) 2016/679 : « _initialement, le régime juridique était fondé sur un système relativement binaire : formalités préalables (déclarations, autorisations, etc.) d’une part ; plaintes, contrôles et éventuellement sanctions, d’autre part. Cependant aujourd’hui, face à l’explosion des données personnelles et à leur circulation généralisée, la protection des données personnelles ne peut plus passer uniquement par ces deux volets sauf à accepter une action de régulation limitée et peu efficiente._ »

Parmi les instruments de droit souple, figurent les recommandations qui : « _sans être juridiquement contraignantes par elles-mêmes, précisent les conditions d’application de la loi dans un secteur donné. Si, en règle générale, la CNIL examine chaque situation en tenant compte de ses caractéristiques propres, les recommandations visent une approche plus générale de la règle de droit_. ».

La labellisation permet quant à elle « _à un responsable de traitement qui le souhaite d’obtenir un label “CNIL“, à condition de s’engager à respecter une série d’obligations définies par le régulateur, qui vont au-delà de la loi mais lui permettent en retour de se prévaloir de ce haut niveau de conformité à l’égard de ses clients._ »11 Il est également possible de parler de référentiel.

**1.2.2.2. Sur les règlements types en vue d’assurer la sécurité des systèmes**

La CNIL établit et publie actuellement, en application du b) de l’article 11 de la loi n° 78-17, des normes destinées à simplifier l’obligation de déclaration prévue à l'[article 23](/loi-78-17/chapitre-iv/section-1/article-23) __ et édicte, le cas échéant, des règlements types en vue d'assurer la sécurité des systèmes.

Ces règlements types ont la même valeur que les recommandations, instrument de droit souple.

**1.2.2.3. Sur la certification**

Les certifications, labels et marques permettant d’assurer une certaine transparence des traitements de données à caractère personnel, le processus de délivrance doit lui aussi être transparent ([article 42](/reglement-2016-679/chapitre-iv/section-5/article-42)(3) et [43](/reglement-2016-679/chapitre-iv/section-5/article-43)(2)d) du règlement). Ils peuvent parfois être le résultat d’une autorégulation au sein des organismes qui identifient eux-mêmes les limites, la portée de la certification, des labels ou marques.

L’article 42(1) du règlement évoque l’encouragement de la certification par les _« États membres, les autorités de contrôle, le comité et la Commission_ » « _[…] aux fins de démontrer que les opérations de traitement effectuées par les responsables du traitement et les sous-traitants respectent le règlement […]»_. La certification est en effet volontaire (article 42(3)) et a une durée limitée de 3 ans maximum (article 42(6)).

Ainsi, le règlement confie un pouvoir de certification (directe) aux autorités de contrôle (article 58(3)f). Ce pouvoir de certification permet de certifier des responsables du traitement ou des sous-traitants soumis ou non au règlement (article 42(2)). Il ne s'agit toutefois pas d'un rescrit permettant de certifier le respect du règlement, puisqu' _« une certification [...] ne diminue par la responsabilité du responsable du traitement ou du sous-traitant quant au respect du présent règlement et est sans préjudice des missions et des pouvoirs des autorités de contrôle qui sont compétentes en vertu de l'article 55 ou 56. »_ (article 42(4)).

Le règlement prévoit aussi que les autorités de contrôle approuvent les critères de certification et que les États membres veillent à ce que les organismes de certification soient agréés par l’autorité de contrôle et/ou l’organisme national d’accréditation désigné conformément au règlement (CE) n° 765/2008, en France, le Comité Français d’Accréditation (COFRAC) (article 43(1)). Le législateur national est ainsi tenu de retenir une option, peu importe laquelle.

Dans ce cadre, un mécanisme à deux niveaux est également possible : l’organisme d’accréditation accrédite les certificateurs tiers, qui certifient eux-mêmes divers objets. Dans ce cas, le règlement laisse aux États membres un choix de confier ce pouvoir d’agrément / accréditation soit à la Commission nationale de l'informatique et des libertés, soit à l’organisme national d’accréditation, soit aux deux.

 **1.2.3. Sur le rôle de conseil de la CNIL auprès des pouvoirs publics**

Actuellement, en application du a) du 4 de l’[article 11 de la loi n° 78-17](/loi-78-17/chapitre-iii/article-11) la Commission nationale de l’informatique et des libertés est consultée sur tout projet de loi ou de décret ou toute disposition de projet de loi ou de décret relatifs à la protection des données à caractère personnel ou au traitement de telles données. Son avis est également requis dans le cadre des régimes d’autorisation prévus aux articles 26 et 27 de la même loi.

L’[article 57 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vi/section-2/article-57) prévoit que l’autorité de contrôle « _conseille, conformément au droit de l'État membre, le parlement national, le gouvernement et d'autres institutions et organismes au sujet des mesures législatives et administratives relatives à la protection des droits et libertés des personnes physiques à l'égard du traitement_ ».

Le projet de loi prévoit ainsi d’étendre le rôle consultatif de la Commission nationale de l'informatique et des libertés aux propositions de loi, sur saisine du président d’une des deux chambres du Parlement.

**1.3. Eléments de droit comparé**

Certains Etats membres qui ont déjà adopté une loi de mise en conformité au « paquet européen » de la protection des données, ont également doté, à cette occasion, leur autorité de protection des données de nouvelles compétences.

Par exemple, l’article 14 de la loi allemande sur la protection des données à caractère personnel prévoit un certain nombre de missions « _en plus des missions listées dans le règlement (UE) 2016/679_ »12 , telles que :

\- promouvoir la sensibilisation et la compréhension du public aux risques, règles, garanties et droits au traitement des données personnelles, en accordant une attention particulière aux mesures spécifiques aux enfants ;

\- conseiller le Bundestag allemand, le Bundesrat, le gouvernement fédéral et d'autres institutions et organes sur les mesures législatives et administratives relatives à la protection des droits et libertés des personnes physiques à l'égard du traitement des données à caractère personnel ;

\- sur demande, fournir des informations concernant l'exercice des droits conférés par la loi allemande relative à la protection des données et, le cas échéant, coopérer avec les autorités des autres États membres à cette fin.

La Belgique13 ainsi que le Luxembourg14 ont présenté des projets de loi spécifiques pour leur autorité de contrôle. Pour la Belgique, il s’agit de passer d’un organe d’avis à une autorité de contrôle et de sanction. Une articulation avec les compétences des entités fédérées est prévue. Le Luxembourg semble opérer un renvoi au règlement (UE) 2016/679 sans doter de missions supplémentaires son autorité de contrôle, dans le domaine couvert par le règlement précité.

L’Irlande semble s’orienter vers la création d’une commission (Data Protection Commission), et non plus un commissaire à la protection des données15.

 ** _2\. Objectifs poursuivis et nécessité de légiferer_**

 **2.1. Objectifs poursuivis**

La présente disposition ambitionne de maintenir l’efficacité de l’exercice des missions de la Commission nationale de l'informatique et des libertés dans un environnement juridique renouvelé. Dans le cadre de l’harmonisation des règles relatives aux données à caractère personnel au niveau de l’Union européenne et de concurrence internationale, il s’agit de prendre en compte la question de l’attractivité économique et juridique de notre territoire vis-à-vis des opérateurs qui souhaiteraient s’y implanter.

Les nouvelles missions confiées à la Commission doivent lui permettre de maintenir un standard de la protection des données à caractère personnel des personnes tout en répondant à ces nouvelles exigences en termes d’attractivité économique et juridique.

**2.2. Nécessité de légiférer**

Le présent article du projet de loi modifie l’article 11 de la loi n°78-17 afin de mettre en conformité le droit national avec le règlement (UE) 2016/679 et de transposer la directive (UE) 2016/680.

Cette disposition procède également à quelques modifications nécessaires au regard de la pratique actuelle de la Commission nationale de l'informatique et des libertés.

**_3\. Options envisagées_**

 **3.1. Inscrire la mesure dans un chapitre commun**

L’existence de deux textes (règlement et directive) aurait pu conduire le législateur à voter deux lois distinctes ou à prévoir une séparation stricte des missions de « l’autorité de contrôle » selon qu’il s’agit du champ d’application du règlement ou de celui de la directive.

Toutefois, l’[article 41 (3) de la directive](/directive-2016-680/chapitre-vi/section-1/article-41) prévoit que l’autorité de contrôle instituée au titre du règlement peut également être compétente dans le champ d’application de la directive (UE) 2016/680. L’article 1er du projet de loi précise expressément que la CNIL est l’autorité de contrôle nationale au sens et pour l’application du règlement (UE) 2016/679.

En outre, la Commission nationale de l'informatique et des libertés a été instituée en 1978 pour connaître de l’ensemble des traitements de données, quelles que soient leurs finalités. Certains traitements qui ne relèvent pas du droit de l’Union européenne doivent pouvoir également faire l’objet de contrôle de la part de la Commission.

Pour les raisons évoquées précédemment, le Gouvernement a décidé de maintenir une compétence de la Commission nationale de l'informatique et des libertés à l’égard de l’ensemble des traitements, quels que soient les domaines dont ils relèvent.

 **3.2. Etendue des missions de la Commission nationale de l'informatique et des libertés**

 **3.2.1. Option 1 (écartée) : se borner à une mise en conformité _a minima_ par rapport au règlement et à une transposition de la directive**

La Commission nationale de l'informatique et des libertés est l’autorité de contrôle nationale au sens et pour l’application du règlement (UE) 2016/679. Il est proposé de le rappeler par mesure de clarté.

Le règlement confie un pouvoir de certification (directe) aux autorités de contrôle ([article 58](/reglement-2016-679/chapitre-vi/section-2/article-58).3.f16). Le règlement prévoit aussi que les Etats membres peuvent retenir en plus un mécanisme à deux niveaux dans lequel un organisme d’accréditation accrédite les certificateurs tiers, qui certifient eux-mêmes divers objets : dans ce cas, le règlement laisse aux Etats membres un choix de confier ce pouvoir d’agrément / accréditation soit à l’autorité de contrôle, soit à l’organisme national d’accréditation (en France, le Comité Français d’Accréditation (COFRAC)), soit aux deux ([article 43.1 du règlement](/reglement-2016-679/chapitre-iv/section-5/article-43))17. Le règlement prévoit donc une marge de manœuvre qui est précisée, de façon souple, par le [nouvel alinéa f bis) de l’article 11](/pjl-490/titre-i/chapitre-i/article-1 "Cf. article 1 du PJL, 6°"). Il est précisé que la commission peut établir des exigences supplémentaires aux normes d’accréditation.

Par ailleurs, afin d’être agréés, les organismes de certification doivent démontrer leur indépendance et leur expertise au regard de l’objet de la certification (article 43(2)a du règlement), respecter les critères fixés par l’autorité de contrôle (article 43(2)b)), mettre en œuvre des procédures en vue de la délivrance, de l’examen périodique et du retrait d’une certification (article 43(2)c)), établir des procédures et des structures pour traiter les réclamations et démontrer que leurs tâches et leurs missions n’entraînent pas de conflits d’intérêts.

L’autorité de contrôle (article 58(2)h)) garde toutefois la possibilité de retirer une certification ou ordonner à l’organisme de certification de retirer une certification, ou lui ordonner de ne pas en délivrer si les exigences applicables à la certification ne sont pas ou plus satisfaites.

La Commission nationale de l'informatique et des libertés doit aussi pouvoir agréer / faire accréditer par le Comité Français d’Accréditation (COFRAC)18 des tiers certificateurs, et concernant les processus d’anonymisation. Il s’agit du renvoi du point 2 g) au point 2 f bis) créé.

Il est proposé d’indiquer, afin de se conformer aux [articles 57.1.c19 du règlement](/reglement-2016-679/chapitre-vi/section-2/article-57) et [46.1.c20 de la directive](/directive-2016-680/chapitre-vi/section-2/article-46) que la Commission nationale de l'informatique et des libertés puisse également être consultée par le Président de l’Assemblée nationale ou par le Président du Sénat sur toute proposition de loi relative à la protection des données à caractère personnel ou au traitement de telles données.

Ainsi que souligné ci-dessus, la directive prévoit une mission particulière au g) du 1 de l’article 46 de la directive par rapport au règlement et qui n’est pas déjà prévue par la loi n° 78-17. Cette disposition implique une [modification du h) du 1 de l’article 11 de cette loi](/pjl-490/titre-i/chapitre-i/article-1 "Cf. article 1 du PJL, 8°") afin d’inclure cette mission qui permet à la Commission nationale de l'informatique et des libertés de répondre à une demande de la personne concernée en cas de restriction de ses droits.

Le projet de texte propose de ne pas se limiter à ces modifications obligatoires.

**3.2.2. Option 2 (retenue) : Confier d’autres missions à la CNIL, en sus de celles nécessaires à la mise en conformité au « paquet européen de protection des données » pour prendre en compte la pratique**

En plus des trois missions évoquées ci-dessus, il est proposé de rappeler expressément les mesures de droit souple à la disposition de la Commission nationale de l'informatique et des libertés (lignes directrices, recommandations ou référentiels) destinées à faciliter la mise en conformité et l’évaluation préalable des risques par les responsables de traitement et des sous-traitants. Il est également prévu que la Commission encourage l’élaboration des codes de conduite et homologue et publie des méthodologies de référence destinés à favoriser la conformité des traitements de données de santé.

Ces mesures de droit souple ont vocation à fixer un cadre pour certaines catégories de traitements relativement courants et de faciliter les démarches des organismes, en particulier des PME/PMI21.

Ces mesures doivent permettre d’accompagner les responsables de traitement dans leur démarche de conformité en leur permettant de se « situer » au regard du niveau d’exigence attendu d’eux ; de fonder, le cas échéant, des dispenses d’analyse d’impact, pour des traitements conformes au référentiel22 ; de permettre l’évaluation du risque résiduel après étude d’impact afin d’aider les responsables de traitement à déterminer s’ils sont ou non soumis à l’obligation de consultation préalable de la Commission nationale de l'informatique et des libertés en application de l’[article 36 du règlement](/reglement-2016-679/chapitre-iv/section-3/article-36).

Le [2 b) de l’actuel article 11 est modifié](/pjl-490/titre-i/chapitre-i/article-1 "Cf. article 1 du PJL, 5°") pour supprimer les normes simplifiées, conséquence de la suppression du régime de la déclaration.

En outre, au-delà des mesures de droit souple, la Commission nationale de l'informatique et des libertés disposera de la possibilité d’établir des règlements types pour assurer la sécurité des systèmes de traitement et de régir les traitements des données de santé (chapitre IX). Elle pourra prescrire des règles d’ordre techniques et organisationnelles sauf pour les traitements mis en œuvre pour le compte de l’État, agissant dans l’exercice de ses prérogatives de puissance publique, pour renforcer les conditions de traitement des données biométriques, génétiques et de santé conformément à l’[article 9.4 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-ii/article-9) et des garanties complémentaires en matière de traitement de données d’infraction conformément à l’[article 10 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-ii/article-10).

Enfin, la Commission nationale de l'informatique et des libertés pourra présenter des observations toutes les juridictions à l’occasion de litiges relatifs à l’application du règlement et de la loi n°78-17 (cf. infra, articles du projet de loi relatifs aux voies de recours).

**_4\. Analyse des impacts de la disposition envisagée_**

 **4.1. Impacts juridiques**

La présente disposition modifie la rédaction de l’article 11 de la loi n° 78-17 pour conférer à la Commission nationale de l'informatique et des libertés de nouvelles compétences.

Il en est ainsi des certifications et agréments qu’elle pourra délivrer, le cas échéant, conjointement avec le Comité Français d’Accréditation (COFRAC).

Si la plupart des instruments de droit souple que la Commission nationale de l'informatique et des libertés sera amenée à prendre ne sont dotés d’aucune force contraignante, ils permettent toutefois d’orienter les comportements des responsables de traitement.

Ainsi, une recommandation de la Commission qui se borne à donner une interprétation de la loi n° 78-17 ne constitue pas un acte faisant grief et est, par suite, insusceptible de recours ([CE 27 septembre 1989, SA Chopin et Cie, nos 74548, 74549 et 74550)](https://www.legifrance.gouv.fr/affichJuriAdmin.do?idTexte=CETATEXT000007768025)23.

D’autres instruments constituent en revanche des actes juridiques contraignants, tels que les prescriptions techniques et organisationnelles concernant les traitements des données biométriques, génétiques et de santé et les traitements de données d’infraction que la Commission pourra adopter.

La procédure législative est également concernée par ces modifications. En effet, le Président de l’une des deux chambres du Parlement pourra désormais consulter la Commission nationale de l'informatique et des libertés sur toute proposition de loi relative à la protection des données à caractère personnel ou au traitement de telles données, à l’instar de l’[article 4 _bis_ de l’ordonnance n° 58-1100 du 17 novembre 1958](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000705067#LEGIARTI000028288243) relative au fonctionnement des assemblées parlementaires qui prévoit que le président d'une assemblée parlementaire peut saisir le Conseil d'Etat d'une proposition de loi déposée par un membre de cette assemblée, avant l'examen de cette proposition en commission.

Enfin, la capacité de la CNIL d’agir en justice est renforcée dans la mesure où elle pourra présenter des observations devant toute juridiction à l’occasion d’un litige relatif à l’application du règlement (UE) 2016/679 et de la présente loi.

**4.2. Impacts sur les particuliers**

L’impact de la certification sur les personnes concernées varie selon le label ou la marque.

Ainsi que le précise le [considérant 100 du règlement](/reglement-2016-679/considerants/considerant-100), afin de favoriser la transparence et le respect du règlement, la mise en place de mécanismes de certification ainsi que de labels et de marques en matière de protection des données devrait être encouragée pour permettre aux personnes concernées d'évaluer rapidement le niveau de protection des données offert par les produits et services en question.

Un label clair et transparent pourra faciliter l’information des personnes sur le traitement des données à caractère personnel les concernant.

 **4.3. Impact sur les entreprises**

Le renforcement des mesures de droit souple que peut prendre la Commission nationale de l'informatique et des libertés est destiné à garantir une meilleure application du droit de la protection des données à caractère personnel et d’offrir aux responsables de traitement, en particulier pour les PME, un cadre juridique sécurisé.

En effet, les représentants des acteurs du numérique (associations, entreprises) ont indiqué que ne pas se sentir encore prêts pour l’application du règlement et des nouvelles obligations qui leur incomberont. L’action combinée du Gouvernement et de la CNIL devra donc leur permettre de comprendre leurs droits et de mettre en œuvre les obligations prévues par les textes européens.

A cet égard, l’[article 40 du règlement](/reglement-2016-679/chapitre-iv/section-5/article-40) prévoit que : « _Les États membres, les autorités de contrôle, le comité et la Commission encouragent l'élaboration de codes de conduite destinés à contribuer à la bonne application du présent règlement, compte tenu de la spécificité des différents secteurs de traitement et des besoins spécifiques des micro, petites et moyennes entreprises_ ». De même, en matière de certification, l’[article 42](/reglement-2016-679/chapitre-iv/section-5/article-42) précise que : « _Les besoins spécifiques des micro, petites et moyennes entreprises sont pris en considération_. ».

La certification ne diminue pas la responsabilité du responsable de traitement (article 42(4) du règlement). Elle a cependant un impact à l’égard des personnes concernées et du public : les responsables de traitements ou les sous-traitants peuvent voir leur image valorisée. La certification aura un impact différent selon l’organisme **** concerné. Elle permet toutefois de s’assurer que le niveau de protection des données apporté est plus important, rassurant ainsi sur le risque encouru. A titre d’exemple, dans une étude réalisée en 201624, certains organismes décrivent la norme ISO 27001 (Management de la Sécurité de l'Information) comme ayant permis selon eux d’améliorer la sécurité de leur organisme, leur information en la matière, de gagner un avantage compétitif, d’assurer une conformité légale voire même d’obtenir de nouveaux contrats.

 ** _5\. Consultation et modalités d’application_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

Les textes réglementaires suivants devront être pris sur le fondement de la loi :

<table>
<tr>
<td>

**Articles du projet de loi renvoyant à des mesures réglementaires**

</td>
<td>

**Nature du texte réglementaire**

</td>
<td>

**Objet du texte réglementaire**

</td></tr>
<tr>
<td>

Article 1er (6°)

</td>
<td>

Décret en Conseil d’Etat pris après avis de la Commission nationale de l'informatique et des libertés

</td>
<td>

La Commission nationale de l'informatique et des libertés agrée des organismes certificateurs le cas échéant, de leur accréditation ou concourt à leur agrément par l'instance nationale d'accréditation, mentionnée à l'article 43(1) b du règlement (UE) 2016/679.

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ier/chapitre-ier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ier/chapitre-ier/articles-2-et-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

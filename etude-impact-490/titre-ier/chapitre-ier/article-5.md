<!-- TITLE: ARTICLE 5 -->
<!-- SUBTITLE: PROCÉDURE DE COOPÉRATION DE LA CNIL AVEC LES AUTRES AUTORITÉS DE CONTRÔLE -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ier/chapitre-ier/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ier/chapitre-ier/article-6" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**_1\. Etat des lieux et diagnostic_**

 ** _1.1. Etat des lieux_**

La [loi n° 78-17 du 6 janvier 1978](/loi-78-17) contient peu de dispositions relatives à la coopération de la Commission nationale de l’informatique et des libertés avec les autorités de contrôle européennes.

L'[article 49](/loi-78-17/chapitre-vii/article-49), tel qu’issu de la [loi n° 2004-801 du 6 août 2004](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000441676) qui transposait la [directive 95/46/CE](http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:31995L0046), prévoit que chaque autorité peut être appelée à exercer ses pouvoirs sur demande d'une autorité d'un autre Etat membre et que les autorités de contrôle coopèrent entre elles dans la mesure nécessaire à l'accomplissement de leurs missions, notamment en échangeant toute information utile.

Cet article prévoit actuellement que la CNIL peut, à la demande d'une autorité exerçant des compétences analogues dans un autre Etat membre, procéder à des vérifications dans les mêmes conditions, selon les mêmes procédures et sous les mêmes sanctions, sauf s'il s'agit d'un traitement de « souveraineté » (traitement qui intéresse la sûreté de l'Etat, la défense ou la sécurité publique) ou qui ont pour objet la prévention, la recherche, la constatation ou la poursuite des infractions pénales ou l'exécution des condamnations pénales ou des mesures de sûreté ([article 26 de la loi n° 78-17](/loi-78-17/chapitre-iv/section-2/article-26)).

**_1.2. Cadre constitutionnel_**

Le [chapitre VII du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vii) organise une procédure de coopération entre les autorités de contrôle de l’Union européenne. L’[article 62.3](/reglement-2016-679/chapitre-vii/section-1/article-62) prévoit que : « _Une autorité de contrôle peut, conformément au droit d'un État membre, et avec l'autorisation de l'autorité de contrôle d'origine, conférer des pouvoirs, notamment des pouvoirs d'enquête, aux membres ou aux agents de l'autorité de contrôle d'origine participant à des opérations conjointes ou accepter, pour autant que le droit de l'État membre dont relève l'autorité de contrôle d'accueil le permette, que les membres ou les agents de l'autorité de contrôle d'origine exercent leurs pouvoirs d'enquête conformément au droit de l'État membre dont relève l'autorité de contrôle d'origine. Ces pouvoirs d'enquête ne peuvent être exercés que sous l'autorité et en présence de membres ou d'agents de l'autorité de contrôle d'accueil. Les membres ou agents de l'autorité de contrôle d'origine sont soumis au droit de l'État membre de l'autorité de contrôle d'accueil._ ».

La possibilité de prévoir que des agents d’autorités de contrôle étrangères puissent participer à des opérations conjointes avec des agents de la Commission nationale de l’informatique et des libertés doit s’exercer dans le respect du cadre constitutionnel et des exigences liées à la souveraineté nationale de la France.

Dans sa [décision du 17 janvier](http://www.conseil-constitutionnel.fr/conseil-con..decision-n-80-116-dc-du-17-juillet-1980.7771.html) 198035, le Conseil constitutionnel a jugé, à propos de la convention franco-allemande additionnelle à la [Convention européenne d'entraide judiciaire en matière pénale du 20 avril 1959](https://www.coe.int/fr/web/conventions/full-list/-/conventions/treaty/030), que : « _considérant que la convention franco-allemande n'apporte aucune atteinte à la règle qui découle du principe de la souveraineté nationale, selon laquelle les autorités judiciaires françaises, telles qu'elles sont définies par la loi française, sont seules compétentes pour accomplir en France, dans les formes prescrites par cette loi, les actes qui peuvent être demandés par une autorité étrangère au titre de l'entraide judiciaire en matière pénale; que les garanties de l'indépendance de ces autorités demeurent pour l'accomplissement de ces actes les mêmes que celles dont elles disposent dans l'exécution d'actes analogues demandés par les autorités françaises; que, dans ces conditions, la convention additionnelle n'est pas contraire à l'[article 64 de la Constitution](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000571356&idArticle=&dateTexte=20180114#LEGIARTI000006527555)_ ».

Le [Conseil constitutionnel a également jugé, à propos de l'accord de Schengen du 14 juin 1985](http://www.conseil-constitutionnel.fr/conseil-con..decision-n-91-294-dc-du-25-juillet-1991.8764.html "Cf. considérants 38 et 39")36, que : « _Considérant que la procédure de poursuite transfrontalière régie par l'article 41 de la convention et dont les modalités d'exercice ont fait l'objet d'une déclaration du Gouvernement de la République sur le fondement du paragraphe 9 de l'article 41, n'est ni générale, ni discrétionnaire ; que cette procédure n'est applicable qu'à des hypothèses où il y a soit des infractions flagrantes d'une particulière gravité, soit une volonté de la part de la personne poursuivie de se soustraire à la justice de son pays ; que les agents poursuivants ne disposent en aucun cas du droit d'interpellation ; que l'entrée dans les domiciles et les lieux non accessibles au public leur est interdite ; considérant qu'en raison des modalités de son exercice, la procédure de poursuite transfrontalière ne procède pas à un "transfert de souveraineté"_ ; ».

En l’espèce, l’intervention prévue des agents d’autorités de contrôle étrangères est subordonnée à l’acceptation des autorités françaises, sauf cas particuliers : flagrance, volonté d’échapper à sa justice nationale etc. Le respect du droit français est un impératif et les autorités étrangères ne bénéficient pas de tous les pouvoirs : pas d’interpellation par exemple. Il s’agit par ailleurs de cas très particuliers pour la coopération policière comme un fait punissable pouvant donner lieu à extradition.

La convention Schengen n’est pas assimilable à la procédure de coopération entre autorités de contrôle en matière de protection des données prévue par le règlement. Il ne s’agit pas de coopération policière ou judiciaire mais d’agents d’autorité de contrôle. La jurisprudence du Conseil constitutionnel sur l’Accord de Schengen porte sur des arrestations, la poursuite par des agents étrangers au-delà de leurs frontières nationales, mais pas sur une coopération entre autorités administratives indépendantes ou des autorités de contrôle concernant des enquêtes de nature administrative.

Sur ce dernier point, [la Cour de cassation a jugé](https://www.legifrance.gouv.fr/affichJuriJudi.do?oldAction=rechJuriJudi&idTexte=JURITEXT000030142815&fastPos=1), à propos du mécanisme de coopération entre autorités de la concurrence des Etats membres de l’Union européenne, prévu par le [règlement (CE) n° 1/2003 du Conseil du 16 décembre 2002](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX:32003R0001) relatif à la mise en œuvre des règles de concurrence prévues aux articles 81 et 82 du traité, que les dispositions de l'article 22 de ce règlement ne subordonnent pas l'exécution de la mesure d'enquête sollicitée à l'autorisation préalable d'un juge de l'État pour le compte duquel elle est effectuée, et que l'autorisation et le déroulement de l'enquête sont régis par le droit national applicable dans l'État destinataire de la demande d'assistance sous le contrôle des juridictions compétentes de cet État et qu'il n'appartient pas au juge français d'autoriser ou de contrôler le déroulement de mesures d'enquête sur d'autres territoires que son territoire national37.

Au regard de ces éléments, l’[article 5 du projet de loi](/pjl-490/titre-i/chapitre-i/article-5) prévoit de mettre en œuvre un système d’autorisation de l’autorité nationale (accord préalable de la CNIL), et les agents des autorités de contrôle étrangers seront sous le contrôle de la CNIL. Il s’agit d’un double contrôle par la CNIL et du respect de la souveraineté nationale.

La procédure est unifiée et applicable à l’ensemble des Etats membres de l’Union européenne. Le droit national, lors des contrôles opérés par des agents d’autorités de contrôle étrangères, trouvera à s’appliquer (par exemple, l’interdiction de contrôler un domicile).

En outre, ne sont pas concernés par la procédure de coopération les fichiers de souveraineté (intéressant par exemple la défense nationale).

Le projet de loi ne porte donc pas atteinte à la souveraineté nationale.

 ** _1.3. Cadre conventionnel_**

La [section 1 du chapitre VII du règlement](/reglement-2016-679/chapitre-vii/section-1) (articles 60 à 62) est consacrée à la coopération entre les autorités de contrôle. Le [considérant 133](/reglement-2016-679/considerants/considerant-133) précise notamment que : « _Les autorités de contrôle devraient s'entraider dans l'accomplissement de leurs missions et se prêter mutuellement assistance afin de faire appliquer le présent règlement et de contrôler son application de manière cohérente dans le marché intérieur. [...]_ ».

Un mécanisme de contrôle de la cohérence est prévu sous l’égide du comité européen à la protection des données qui regroupera l’ensemble des autorités de contrôle des Etats membres de l’Union européenne et le contrôleur européen à la protection des données.

L'ensemble du [chapitre VII du règlement](/reglement-2016-679/chapitre-vii), de par son objectif d'une meilleure coopération et cohérence entre les autorités de contrôle, est très détaillé. Peu de marge de manœuvre sont laissées aux Etats membres sur ce point, sauf en matière de définition des pouvoirs d’enquête qui peuvent être confiés aux membres et agents associés aux opérations conjointes.

**_1.4 Éléments de droit comparé_**

Le projet de loi belge apporte des précisions d’ordre procédural sur le mécanisme de coopération (voir en ce sens l’article 67).

Le projet de loi luxembourgeois prévoit la possibilité pour les membres du collège, les membres suppléants et agents de la commission nationale pour la protection des données (CNPD) de communiquer des informations relevant du secret professionnel aux autorités de contrôle des autres États membres, au comité européen à la protection des données et à la Commission européenne les informations nécessaires à ceux-ci pour l’exercice de leur surveillance, à conditions qu’ils tombent sous un secret professionnel équivalent (article 45).

**_2\. Objectifs poursuivis et nécessite de legiferer_**

 ** _2.1. Objectifs poursuivis_**

La mise en conformité du droit national doit s'inscrire dans les limites de ce que prévoit le règlement (UE) 2016/679 et permettre une coopération efficace entre les autorités de contrôle des Etats membres de l’Union européenne chargées de la protection des données à caractère personnel.

 ** _2.2. Nécessité de legiferer_**

Les relations entre la Commission nationale de l’informatique et des libertés et les autorités de contrôle des autres Etats membres de l’Union européenne nécessitent d’être profondément revues au regard des nouvelles dispositions du règlement (UE) 2016/679.

Des précisions législatives sont nécessaires afin notamment de répartir les rôles entre les formations collégiales de la Commission et son président.

En revanche, l’[article 49 bis de la loi n° 78-17](/loi-78-17/chapitre-vii/article-49-bis), qui concerne les relations entre la CNIL et une autorité de contrôle d’Etats non membres de l’Union européenne, n’est pas modifié par le présent projet de loi.

 ** _3\. Options_**

Bien que le règlement soit directement applicable, il est proposé de modifier l’[article 49 de la loi n° 78-17](/loi-78-17/chapitre-vii/article-49) qui figure dans les dispositions communes du projet de loi, pour préciser les modalités de coopération en dehors du champ d’application du règlement.

 **3.1. Répartition des compétences au sein de la CNIL en ce qui concerne la coopération (article 49-1)**

 **3.1.1. Option 1 (écartée) : Compétence du collège, du bureau ou de la formation restreinte de la CNIL**

Le règlement encadre la procédure de coopération dans des délais courts. A ce titre, il s'agit de savoir qui décide d’inviter d’autres autorités de contrôle à participer à des opérations conjointes décidées par la CNIL ou qui détermine l'étendue de la participation des agents de la CNIL auprès des autorités étrangères.

Il a été exclu de confier cette compétence à des organes collégiaux de la Commission (collège, bureau ou formation restreinte) au regard de la nécessaire réactivité que supposent de telles décisions.

 **3.1.2. Option 2 (retenue) : Compétence du président de la CNIL**

Le projet de loi prévoit que c'est le président de la Commission qui invite les autres autorités de contrôle concernées à participer aux opérations de contrôle conjointes, décide de conduire ou se prononce sur le principe et les conditions de la participation, désigne les membres et agents habilités, et en informe l’autorité requérante.

Il s’agit de répondre ainsi à la nécessité d’une prise de décision rapide que la convocation d’organes collégiaux (bureau, formation restreinte) ne permettrait pas d’assurer systématiquement.

**3.2. Participation des membres et agents des autres autorités de contrôle étrangères (article 49-1)**

 **3.2.1. Option 1 (écartée) : Prévoir une simple participation passive des membres et agents des autorités de contrôle étrangères en cas de contrôle sur le territoire français**

L'[article 62.3 du règlement](/reglement-2016-679/chapitre-vii/section-1/article-62) permet une marge de manœuvre pour déterminer l'étendue des pouvoirs qui peuvent être octroyés aux membres et agents des autorités de contrôle étrangères.

L’option constituant à limiter le rôle des membres ou agents des autorités de contrôle étrangères à une simple présence passive aux côtés des agents de la CNIL semble contraire à l’esprit du règlement qui invite à envisager une procédure intégrée et renforcée.

 **3.2.2. Option 2 (retenue) : Permettre au président de la CNIL d’habiliter les agents de l’autorité de contrôle étrangère pour exercer tout ou partie des pouvoirs de vérification et d’enquête des agents de la CNIL**

Le projet de loi précise, d'une part, que des membres et agents de la Commission nationale de l’informatique et des libertés doivent être présents aux côtés des agents et membres étrangers lors des contrôles. Ils sont ainsi nécessairement soumis aux obligations qui incombent aux membres et agents de la CNIL.

D'autre part, il permet au président d’habiliter, par décision particulière, ceux des membres ou agents de l’autorité de contrôle concernée qui présentent des garanties comparables à celles requises des agents de la CNIL. Les contrôles auxquels participeront ces agents habilités, qui ne disposeront pas de pouvoirs plus étendus que ceux confiés à leurs homologues français, seront exercés sous l’autorité de la CNIL.

 **3.3. Cas de coopération lorsque la France est autorité chef de file (article 49-3)**

Sur les éléments que la formation restreinte doit transmettre aux autres autorités concernées, il a été retenu que la transmission se fasse avant l’audition éventuelle du responsable de traitement, du sous-traitant ou d'un autre organisme par la formation restreinte.

S’agissant de la procédure contradictoire exigée par l’article 6§1 de la [convention européenne des droits de l'homme](https://www.coe.int/fr/web/conventions/full-list/-/conventions/rms/0900001680063776) dans le cadre de la procédure de coopération/ cohérence, le projet de loi prévoit que le contradictoire ait lieu selon les mêmes modalités qu’actuellement, c’est-à-dire avant l’audition par la formation restreinte (possibilité de répondre par écrit au rapport notifié) et au cours de l’audition (par oral). En revanche, après l’audition, s’ouvre une phase de délibéré qui, bien qu’élargie aux autres autorités concernées, reste une phase de délibéré interne à l’administration (une administration elle aussi « élargie »). Autrement dit, il n’est pas nécessaire de soumettre au contradictoire les éventuelles objections pertinentes et motivées ni l’avis ou la décision du comité européen de la protection des données.

Sur la participation des autres autorités à l’audience de la formation restreinte : dès lors qu’il y a codécision, les autorités concernées doivent être mises en mesure d’assister (vidéoconférence) ou de prendre connaissance de la teneur de l’audience de la formation restreinte (grâce à un procès-verbal). En revanche il n’a pas semblé nécessaire de prévoir une participation « active » (possibilité de poser des questions à l’organisme). Le règlement semble en effet ériger l’autorité chef de file en « guichet unique »38, vis-à-vis de l’organisme et de la collégialité des autorités.

Les conditions dans lesquelles le caractère contradictoire de la procédure est garanti à l’égard du ou des responsables de traitement et sous-traitants et les modalités de communication des pièces du dossier durant la procédure seront définies par un décret en Conseil d’Etat.

 **3.4. Répartition des compétences au sein de la CNIL lorsque la CNIL sera autorité concernée appelée à se prononcer sur un projet de mesure correctrice que lui transmettra une autre autorité chef de file (article 49-4)**

Le schéma proposé repose sur deux principes : d'une part, l’aiguillage est centralisé au niveau du président ; d'autre part, les mesures sont ensuite fléchées, en fonction de leur objet, vers l’organe (président ou formation restreinte) qui aurait été compétent pour prendre la mesure équivalente.

Cet aiguillage est conforme au modèle français répartissant le pouvoir décisionnel entre des organes distincts de la CNIL.

Il est _a priori_ nécessaire de prévoir les détails de cette répartition au niveau de la loi. Toutefois, et afin de tenir les délais restreints de réponse à un projet de mesure (4 semaines en application du 3 de l'[article 60 du règlement](/reglement-2016-679/chapitre-vii/section-1/article-60)), il conviendra d’envisager, dans le décret d’application, les solutions permettant de donner à la formation restreinte une plus grande souplesse et une plus grande réactivité qu’aujourd’hui.

**_4\. Analyse des impacts de la disposition envisagée_**

La présente disposition modifie l’[article 49 de la loi n°78-17](/loi-78-17/chapitre-vii/article-49) et créé quatre nouveaux articles qui :

\- précise la coopération entre la CNIL et les autorités de contrôle des autres Etats membres de l’Union européenne dans le cadre d’opérations conjointes dans le cadre du champ d’application du règlement (UE) 2016/679 (art. 49-1) ;

\- précise la coopération entre la CNIL et les autorités de contrôle des autres Etats membres de l’Union européenne pour les traitements relevant des dispositions de la loi n° 78-17 transposant la directive (UE) 2016/680 (art. 49-2) ;

\- précise la procédure lorsque la CNIL agit en tant qu’autorité chef de file (art. 49-3) ;

\- précise la répartition des compétences au sein de la CNIL lorsque cette dernière agit en tant qu’autorité concernée par un contrôle mené dans un autre Etat membre de l’Union européenne (art. 49-4).

**_5\. Consultation et modalités d’application_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

Les textes réglementaires suivants devront être pris sur le fondement de la loi :

<table>
<tr>
<td>

**Articles du PJL renvoyant à des mesures réglementaires**

</td>
<td>

**Nature du texte réglementaire**

</td>
<td>

**Objet du texte réglementaire**

</td></tr>
<tr>
<td>

Article 5

</td>
<td>

Décret en conseil d’Etat pris après avis de la Commission nationale de l'informatique et des libertés

</td>
<td>

Conditions d’application de l’article 49-3 de la loi n°78-17.

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ier/chapitre-ier/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ier/chapitre-ier/article-6" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: ARTICLE 4 -->
<!-- SUBTITLE: POUVOIRS DE CONTRÔLE DE LA CNIL -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ier/chapitre-ier/articles-2-et-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ier/chapitre-ier/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

 ** _1\. Etat des lieux et diagnostic_**

 ** _1.1. Etat des lieux_**

1.1.1 Avant l’adoption de la [loi n° 2004-801 du 6 août 2004](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000441676) relative à la protection des personnes physiques à l'égard des traitements de données à caractère personnel et modifiant la [loi n° 78-17 du 6 janvier 1978](/loi-78-17) relative à l'informatique, aux fichiers et aux libertés, l’[article 21 de la loi n° 78-17](/loi-78-17/chapitre-iii/article-21) définissait la mission de contrôle de la Commission nationale de l'informatique et des libertés. Cette dernière pouvait notamment : « _par décision particulière, charger un ou plusieurs de ses membres ou de ses agents, assistés, le cas échéant, d'experts, de procéder, à l'égard de tout traitement, à des vérifications sur place et de se faire communiquer tous renseignements et documents utiles à sa mission_ ».

1.1.2 Depuis 2004, il est désormais renvoyé à l’[article 44 de la loi n° 78-17](/loi-78-17/chapitre-vi/article-44) relatif aux contrôles par la Commission nationale de l'informatique et des libertés de la mise en œuvre des traitements25. Cet article contient quatre sous-divisions.

Le I de cet article prévoit les limites de temps et de lieux relatives aux contrôles que les membres et agents et de la commission peuvent effectuer. Il s'agit de contrôles sur place et sur pièce, qui complètent le contrôle _a priori_ , opéré dans le cadre des formalités préalables et permettent soit de constater l'existence de traitements n'ayant pas fait l'objet de telles formalités, soit de s'assurer du respect effectif de la loi par les traitements déclarés ou autorisés. C'est également dans le cadre de ces dispositions que s'exerce le contrôle par la Commission des dispositifs de vidéo protection26. Les contrôles sur place ne sont possibles que dans les lieux ou locaux professionnels privés servant à la mise en œuvre d'un traitement de données personnelles, et non dans les domiciles privés. Les pouvoirs de contrôle incluent la possibilité de recueil et de copie, sur place, de tous les renseignements ou documents utiles. Ce pouvoir de contrôle sur place se double de la possibilité pour les membres et agents de la Commission de procéder à des auditions. Il est également prévu une information préalable du procureur de la République en cas de contrôle.

1.1.3 La [loi n° 2011-334 du 29 mars 2011](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000023781252) a modifié le II de l’article 44 de la loi n° 78-17 pour tirer les conséquences de décisions du Conseil d’Etat27, en prévoyant notamment que « _Le responsable de locaux professionnels privés est informé de son droit d'opposition à la visite_ », ainsi que l’intervention du juge des libertés et de la détention. Ainsi, le responsable des locaux – qui n'est pas nécessairement le responsable de traitement – est informé, avant le début du contrôle, de son droit d'opposition et peut s'opposer à la visite des lieux privés, qui est alors subordonnée à l'autorisation préalable du juge des libertés et de la détention. Lorsque l'urgence, la gravité des faits ou le risque de destruction le justifie, la Commission nationale de l'informatique et des libertés peut toutefois être autorisée par le même juge à visiter les locaux sans information préalable des responsables de ces locaux ni possible opposition de leur part.

1.1.4 La [loi n° 2014-344 du 17 mars 2014](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028738036&categorieLien=id) a modifié le III de l’article 44 de la loi n° 78-17 afin de permettre, en plus des contrôles sur place et sur convocation, des contrôles en ligne, afin notamment de lui permettre de constater d'éventuelles failles dans les données à caractère personnel.

Par ailleurs, pour compléter ce dispositif, le second alinéa de l’[article 21 de la loi 78-17](/loi-78-17/chapitre-iii/article-21) précise que les personnes interrogées dans le cadre des vérifications faites par la commission sont tenues de fournir les renseignement demandés « _sauf dans les cas où elles sont astreintes au secret professionnel_ ». Il est par ailleurs prévu à l’article 44 de cette loi que les agents de la Commission nationale de l'informatique et des libertés ont accès aux « _programmes informatiques et aux données_ » et qu’ils peuvent se faire communiquer « _tous documents nécessaires_ » mais que cependant seul un médecin peut requérir la communication de données médicales individuelles.

1.1.5 Enfin, il est précisé que les traitements intéressant la sûreté de l’Etat peuvent être exclus des opérations de contrôle (article 44-IV de la loi n° 78-17).

En 2016, la Commission nationale de l’informatique et des libertés a opéré 430 contrôles, dont 100 contrôles en ligne et 94 concernaient les dispositifs de vidéo protection28.

 ** _1.2. Cadre constitutionnel_**

Dans sa [décision du 29 juillet 2004](http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2004/2004-499-dc/decision-n-2004-499-dc-du-29-juillet-2004.904.html), le Conseil constitutionnel a examiné la constitutionnalité du dernier alinéa de l’[article 21 de la loi n° 78-17](/loi-78-17/chapitre-iii/article-21). Il a jugé que « _dans le silence des dispositions de la loi du 6 janvier 1978 antérieures à la loi déférée, les personnes interrogées par la Commission nationale de l'informatique et des libertés étaient déjà soumises au secret professionnel_ » et que « _l'invocation injustifiée du secret professionnel pourrait constituer une entrave passible des peines prévues par l'[article 51 nouveau de la loi du 6 janvier 1978](/loi-78-17/chapitre-viii/article-51)_ »29.

 ** _1.3. Cadre conventionnel_**

En premier lieu, les articles [57](/reglement-2016-679/chapitre-vi/section-2/article-57) et [58](/reglement-2016-679/chapitre-vi/section-2/article-58) du règlement (UE) 2016/679 précisent les missions et pouvoirs des autorités de contrôle, en particulier les pouvoirs d’enquête dont earticle 58lles disposent. Le point 6 de l’article 58 du règlement prévoit que chaque Etat membre peut prévoir, par la loi, que son autorité de contrôle dispose de pouvoirs additionnels à ceux prévus par le règlement.

L'[article 90](/reglement-2016-679/chapitre-ix/article-90)(1) du règlement prévoit en outre une marge de manœuvre pour les États membres en matière d’obligation de secret. Ceux-ci _« peuvent adopter des règles spécifiques afin de définir les pouvoirs des autorités de contrôle à l'égard des responsables du traitement ou des sous-traitants qui sont soumis, en vertu du droit de l'Union ou du droit d'un État membre ou de règles arrêtées par les organismes nationaux compétents, à une obligation de secret professionnel ou à d'autres obligations de secret équivalentes, lorsque cela est nécessaire et proportionné pour concilier le droit à la protection des données à caractère personnel et l'obligation de secret. »._

Cette disposition permet de maintenir la plupart des dispositions de l’actuel article 44 de la loi n° 78-17, notamment l’accès aux locaux des responsables de traitement et aux données nécessaires à l’accomplissement des missions de la commission.

En deuxième lieu, dans une [décision du 6 novembre 2009](https://www.legifrance.gouv.fr/affichJuriAdmin.do?oldAction=rechJuriAdmin&idTexte=CETATEXT000021242880), le Conseil d’Etat a jugé, au regard de l’[article 8 de la Convention européenne de sauvegarde des droits de l’homme et des libertés fondamentales](https://www.coe.int/fr/web/conventions/full-list/-/conventions/rms/0900001680063776) que : « _si le droit au respect du domicile que ces stipulations protègent s'applique également, dans certaines circonstances, aux locaux professionnels où des personnes morales exercent leurs activités, il doit être concilié avec les finalités légitimes du contrôle, par les autorités publiques, du respect des règles qui s'imposent à ces personnes morales dans l'exercice de leurs activités professionnelles ; que le caractère proportionné de l'ingérence que constitue la mise en œuvre, par une autorité publique, de ses pouvoirs de visite et de contrôle des locaux professionnels résulte de l'existence de garanties effectives et appropriées, compte tenu, pour chaque procédure, de l'ampleur et de la finalité de ces pouvoirs_ »30.

Cette décision a notamment conduit le législateur à modifier le II de l’article 44 de la loi n° 78-17. Le projet de loi ne modifie pas non plus les garanties qui entourent la procédure de contrôle sur place : droit d'opposition à la visite des lieux, locaux, enceintes, installations ou établissements qui servent à la mise en œuvre des traitements, exclusion des locaux affectés au domicile privé, contrôle du juge des libertés et de la détention et respect des droits de la défense avec la possibilité pour l'occupant des lieux de se faire assister d'un conseil de son choix. La suite de la procédure reste également contradictoire, puisque le responsable de traitement est mis en mesure de produire des observations quant au procès-verbal dressé par les membres ou agents ayant conduit le contrôle.

En dernier lieu, les [articles 55 du règlement](/reglement-2016-679/chapitre-vi/section-2/article-55) et [45 de la directive](/directive-2016-680/chapitre-vi/section-2/article-45) prévoient que les autorités de contrôle ne sont pas compétentes pour contrôler les opérations de traitement effectuées par les juridictions dans l'exercice de leur fonction juridictionnelle. Le [considérant 20](/reglement-2016-679/considerants/considerant-20) du règlement précise : « _La compétence des autorités de contrôle ne devrait pas s'étendre au traitement de données à caractère personnel effectué par les juridictions dans l'exercice de leur fonction juridictionnelle, afin de préserver l'indépendance du pouvoir judiciaire dans l'accomplissement de ses missions judiciaires, y compris lorsqu'il prend des décisions_ ».

L’[article 4 du projet de loi](/loi-78-17/chapitre-ier/article-4) propose d’apporter cette précision.

 ** _1.4 Éléments de droit comparé_**

Le projet de loi luxembourgeois prévoit une section sur le secret professionnel auquel sont astreints les membres et agents de l’autorité de contrôle, sans apporter de précision sur les secrets professionnels précisément protégés.

Le projet de loi belge précise, à son article 67 §2, que « (…) _Les informations concernant des données médicales de nature personnelle ne peuvent être communiquées et utilisées que dans le respect du secret médical._ (…)». Des aménagements procéduraux sont également prévus pour le secret professionnel à l’article 78 « (…) _Sauf accord écrit de la personne concernée ou autorisation du juge d’instruction, l’inspecteur général et les inspecteurs ne peuvent, sans la présence d’un représentant de l’ordre professionnel, pénétrer dans les locaux d’un professionnel qui est soumis au secret professionnel et pour qui une règlementation légale est prévue concernant des examens sur place et l’accès à leurs locaux professionnels_ ».

 ** _2\. Objectifs poursuivis et necessite de légiferer_**

 ** _2.1. Objectifs poursuivis_**

Les objectifs poursuivis à travers les mesures envisagées par cette disposition du projet de loi sont :

\- d’une part, une extension et un renforcement des contrôles a posteriori de la Commission nationale de l'informatique et des libertés ;

\- d’autre part, une clarification du régime de l’opposabilité des secrets professionnels lors des demandes de communication effectuées dans le cadre de vérification.

**_2.2. Nécessité de légiférer_**

L’[article 44 de loi n° 78-17](/loi-78-17/chapitre-vi/article-44) contient les dispositions détaillant le contrôle _a posteriori_ de la Commission nationale de l'informatique et des libertés. Il se combine actuellement avec les contrôles _a priori_ réalisés par la commission dans le cadre des formalités préalables.

Dès lors que le règlement change de paradigme et que le projet de loi épouse la nouvelle logique de responsabilisation en diminuant de façon importante les formalités préalables, il est nécessaire de modifier certaines dispositions de l’article 44 de loi n° 78-17 afin de renforcer l’effectivité des contrôles de la commission.

**_3\. Options_**

 **3.1. Suppression de la précision « usage professionnel » des locaux contrôles**

En l’état actuel, les membres de la commission ainsi que les agents habilités ont, en application I de l’article 44 de la loi n° 78-17, accès « _aux locaux, enceintes, installations ou établissements servant à la mise en œuvre d'un traitement de données à caractère personnel et qui sont à usage professionnel, à l'exclusion des parties de ceux-ci affectées au domicile privé_ ». La précision de l’usage professionnel peut conduire à exclure certaines zones telles que des halls d’immeuble ou des couloirs qui comprennent des photocopieuses.

La suppression de la précision concernant l’usage professionnel des locaux au I de l’article 44 de la loi n° 78-17, et l’adaptation du II de cet article, permet d’inclure les parties communes d'immeubles.

Cette modification est permise en application de l'[article 58]((/reglement-2016-679/chapitre-vi/section-2/article-58))(1)f __ du règlement qui permet à la __ Commission nationale de l'informatique et des libertés __ « _d'obtenir l'accès à tous les locaux du responsable du traitement et du sous-traitant, notamment à toute installation et à tout moyen de traitement, conformément au droit de l'Union ou au droit procédural des États membres._ ».

Cette suppression permet d’accroître l’efficacité des contrôles sur place effectués par la Commission. Comme indiqué précédemment, cette suppression ne remet pas en cause la protection prévue du domicile privé qui reste garantie.

**3.2. Précision relative au régime d’opposabilité des secrets professionnels**

 **3.2.1. Option 1 (écartée) : Maintenir le droit existant**

Actuellement, la question de l’opposabilité des secrets protégés par la loi, lors de la demande de communication des documents par les membres et agents de la Commission nationale de l'informatique et des libertés, n’est pas traitée expressément dans la loi n° 78-17. Seul l’article 21 prévoit une protection du secret professionnel des personnes interrogées dans le cadre des vérifications faites par la commission.

L’article 44 de la loi n° 78-17 prévoit que les agents de la Commission nationale de l'informatique et des libertés ont accès aux « _programmes informatiques et aux données_ » et qu’ils peuvent se faire communiquer « _tous documents nécessaires_ », mais que cependant seul un médecin peut requérir la communication de données médicales individuelles.

Cet état du droit peut paraître ambigu. Une première interprétation pourrait indiquer que les agents de contrôle ne peuvent se voir opposer aucun secret, à l’exception du secret médical. Une seconde interprétation conduirait au contraire à considérer qu’en l’absence de disposition prévoyant expressément l’inopposabilité des secrets protégés par la loi aux agents de contrôle, tout secret pourrait leur être opposé (secret bancaire, des affaires, …).

Si aucune difficulté ne semble s’être présentée en pratique, il est toutefois apparu pertinent, compte tenu notamment de l'augmentation du niveau des sanctions prévu par le règlement, d’affirmer expressément l’inopposabilité des principaux secrets auxquels la commission pourrait être confrontée. L’option d’un maintien du droit existant a donc été écartée.

**3.2.2. Option 2 (écartée) : Supprimer l’opposabilité du secret médical**

Se pose la question de l’opportunité de maintenir l’obligation d’avoir recours à un médecin pour obtenir la communication des données de santé nominatives. Cette procédure, prévue actuellement par le III de l’article 44 de la loi n° 78-1731, peut s’avérer complexe dans la pratique.

L’absence de nécessité du recours à un médecin ne serait pas inédite dans les textes. La levée du secret médical pour des agents de contrôle n’ayant pas la qualité de médecin est ainsi prévue pour des agents dont la santé publique constitue l’objet même de la mission de contrôle32.

La même problématique de l’extension de l’inopposabilité du secret médical à des non-médecins s’est rencontrée pour les agents des autorités régionales de santé. Jusqu’à la [loi n° 2016-41 du 26 janvier 2016](https://www.legifrance.gouv.fr/eli/loi/2016/1/26/AFSX1418355L/jo), l'[article L. 1435-6 du code de la santé publique](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000020897700&cidTexte=LEGITEXT000006072665&dateTexte=20180114) réservait aux agents des autorités régionales de santé ayant la qualité de médecin l'accès aux données à caractère personnel nécessaires à l'accomplissement de leur mission. L'[article 193 de la loi du 26 janvier 2016](https://www.legifrance.gouv.fr/eli/loi/2016/1/26/AFSX1418355L/jo#JORFARTI000031914480) a ouvert cette possibilité à tous les agents des autorités régionales de santé.

Toutefois, le projet de loi ne propose pas la disparition de l’opposabilité complète du secret médical pour les données de santé.

**3.2.3. Option 3 (retenue) : Clarification du régime d’opposabilité des secrets professionnels**

Il est proposé de clarifier l’état du droit et d’indiquer expressément que le secret ne peut être opposé aux membres et agents de la commission sauf concernant les informations couvertes par le secret professionnel applicable aux relations entre un avocat et son client, par le secret des sources des traitements journalistiques ou, dans certaines situations, le secret médical.

En ce qui concerne le secret médical attaché aux traitements nécessaires aux fins de la médecine préventive, de la recherche médicale, des diagnostics médicaux, de l'administration de soins ou de traitements, ou de la gestion de service de santé, il ne sera plus exigé que ce soit seul un médecin qui puisse requérir la communication des données mais que cette communication ne peut être faite que sous l’autorité et en présence de ce dernier. Ainsi, il s’agit d’un allègement de la procédure tout en maintenant une garantie importante relative au secret médical dès lors que l’agent de la commission sera sous la responsabilité fonctionnelle du médecin.

Enfin, il est précisé que seuls les agents et membres habilités au secret de la défense, conformément au code de la défense, ont accès aux documents contenant des informations relevant de ce régime33.

Par ailleurs, le projet de loi rappelle, en application du règlement et de la directive, que la commission n’est pas compétente pour contrôler les opérations de traitement effectuées, dans l’exercice de leur fonction juridictionnelle, par les juridictions, permettant ainsi de préserver le secret de l’instruction.

**3.3. Possibilité de faire usage d’une identité d’emprunt**

La [loi n° 2014-344 du 17 mars 2014](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028738036&categorieLien=id) susmentionnée a modifié le III de l’article 44 de la loi n° 78-17 afin de permettre, en plus des contrôles sur place et sur convocation, des contrôles en ligne. Ces contrôles sont très utiles et sont appelés à se multiplier. Toutefois, lorsque la Commission nationale de l'informatique et des libertés utilise une adresse électronique @cnil.fr pour exercer des droits, le responsable de traitement peut modifier son traitement uniquement pour les besoins du contrôle.

Il s’agit de prévoir l'utilisation par les agents de contrôle d’une identité d'emprunt, en prévoyant la possibilité de créer une fausse identité d’un utilisateur lambda pour un contrôle effectif. Il est précisé que l’utilisation d’une identité d’emprunt est sans incidence sur la régularité des constatations effectuées, afin de prémunir ces contrôles du risque de contestations fondées sur la violation du principe de loyauté dans la collecte des preuves.

Cette mesure est de nature à renforcer l’efficacité des contrôles en ligne. Actuellement, l’autorité des marchés financiers dispose d’une telle possibilité (article L. 621-10-1 du code monétaire et financier34).

Il n’est toutefois pas proposé d’ajouter une disposition sur l’irresponsabilité pénale des agents.

D’une part, parce qu’il n’est pas prévu que les agents de la commission pourront commettre des infractions sur internet, et que le fait d’utiliser une identité d’emprunt pour se connecter à internet ne constitue aucune infraction pénale.

D’autre part, parce que dès lors que la loi permet d’utiliser une identité d’emprunt, à supposer que cela puisse constituer un délit, puisque la loi l’autorise, il ne peut exister aucune responsabilité pénale en application du code pénal.

 ** _4. analyse des impacts de la disposition envisagée_**

La modification de l’article 44 de la loi n° 78-17 permettra d’étendre et de préciser le champ des contrôles possibles de la Commission nationale de l'informatique et des libertés et le cadre dans lesquels ils s’opèrent.

**_5\. Consultation et modalités d’application_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

Les textes réglementaires suivants devront être pris sur le fondement de la loi :

<table>
<tr>
<td>

**Articles du PJL renvoyant à des mesures réglementaires**

</td>
<td>

**Nature du texte réglementaire**

</td>
<td>

**Objet du texte réglementaire**

</td></tr>
<tr>
<td>

Article 4 (4°)

</td>
<td>

Décret en conseil d’Etat pris après avis de la Commission nationale de l'informatique et des libertés

</td>
<td>

Conditions dans lesquelles les membres et agents habilités de la Commission nationale de l’informatique et des libertés procèdent à l’utilisation d’identité d’emprunt.

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ier/chapitre-ier/articles-2-et-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ier/chapitre-ier/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: ARTICLES 2 et 3 -->
<!-- SUBTITLE: COMMISSAIRE DU GOUVERNEMENT ET MEMBRES DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ier/chapitre-ier/article-1er" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ier/chapitre-ier/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**_1\. ETAT DES LIEUX ET DIAGNOSTIC_**

 **1.1. CADRE GENERAL**

 **• Sur la composition de la Commission nationale de l’informatique et des libertés **

L’[article 13 de la loi n° 78-17](/loi-78-17/chapitre-iii/article-13) relatif à la composition de la Commission nationale de l'informatique et des libertés prévoit que, parmi ses dix-huit membres, cette dernière compte :

\- trois personnalités qualifiées pour leur connaissance de l'informatique ou des questions touchant aux libertés individuelles, nommées par décret ;

\- deux personnalités qualifiées pour leur connaissance du numérique, désignées respectivement par le Président de l'Assemblée nationale et par le Président du Sénat.

Avant la [loi n° 2004-801 du 6 août 2004](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000441676), la notion de personnalité qualifiée n’était appliquée qu’aux deux personnes nommées par décret sur proposition respectivement du président de l'Assemblée nationale et du président du Sénat. Les trois autres personnalités étaient désignées, en raison de leur autorité et de leur compétence, par décret en Conseil des ministres.

La substitution du terme « numérique » à celui d’« informatique », plus représentatif de l’évolution des nouvelles technologies, est intervenue suite à un amendement voté dans le cadre du projet de loi pour une République numérique.

**• Sur le commissaire du Gouvernement auprès de la Commission nationale de l’informatique et des libertés **

Le commissaire du Gouvernement représente le Gouvernement auprès de la Commission nationale de l’informatique et des libertés. Le statut et les missions de ce dernier sont prévus à l’[article 18 de la loi n° 78-17](/loi-78-17/chapitre-iii/article-18).

Présent à l'ensemble des séances de la Commission en formation plénière, il a ainsi compétence pour présenter en séance toute observation reflétant le point de vue du gouvernement sur un projet de délibération.

L'ensemble des dispositions relatives au commissaire du Gouvernement (modalités d'information, présence, etc.) vaut pour toutes les délibérations de la Commission, qu'elles concernent des personnes publiques ou des personnes privées. Le commissaire du Gouvernement ne peut pas participer au vote sur la délibération, mais il peut provoquer une nouvelle délibération dans les dix jours. Il assiste enfin aux réunions du bureau, sauf lorsque ce dernier se prononce sur l'éventuelle publicité d'une mise en demeure adoptée par son président.

Les conditions de participation du commissaire du Gouvernent auprès de la Commission nationale de l’informatique et des libertés sont précisées par le [décret n° 2005-1309](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000241445). Son [article 77](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000241445#LEGIARTI000025082770) prévoit notamment que : « _Lors de la séance, le rapporteur peut présenter des observations orales sur l'affaire. Le responsable du traitement et, le cas échéant, son conseil sont invités à présenter des observations orales à l'appui de leurs conclusions écrites. Le commissaire du Gouvernement est invité à donner son avis sur l'affaire. La formation restreinte peut entendre toute personne dont elle estime l'audition utile. Dans tous les cas, le responsable du traitement et, le cas échéant, son conseil doivent pouvoir prendre la parole en dernier. Lorsque la formation restreinte s'estime insuffisamment éclairée, elle peut demander au rapporteur de poursuivre ses diligences. La formation restreinte statue hors la présence du rapporteur et du commissaire du Gouvernement_. »

 **1.2. CADRE CONSTITUTIONNEL**

Dans sa [décision du 19 janvier 2017](http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2017/2017-746-dc/decision-n-2017-746-dc-du-19-janvier-2017.148502.html), le Conseil constitutionnel a rappelé qu’il appartient au législateur de fixer les règles relatives à la composition et les attributions ainsi que la détermination des principes fondamentaux de l’organisation et du fonctionnement des autorités administratives indépendantes.

Le Conseil constitutionnel est par ailleurs vigilant sur les garanties d’impartialité des autorités administratives indépendantes au regard notamment de leurs pouvoirs de sanction. Ainsi, dans sa [décision n° 2012-280 QPC du 12 octobre 2012](http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2012/2012-280-qpc/decision-n-2012-280-qpc-du-12-octobre-2012.115745.html), Société Groupe Canal Plus et autre, le Conseil constitutionnel a rappelé que les dispositions relatives à la composition, aux règles de délibération et aux modalités de saisine de l'Autorité de la concurrence doivent respecter les principes d'indépendance et d'impartialité découlant de l'article 16 de la Déclaration de 1789.

 **1.3. CADRE CONVENTIONNEL**

L’[article 53 (2) du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vi/section-1/article-53) précise que : « _Chaque membre a les qualifications, l'expérience et les compétences nécessaires, notamment dans le domaine de la protection des données à caractère personnel, pour l'exercice de ses fonctions et de ses pouvoirs._ »

Le choix d’unifier les connaissances requises pour l’ensemble des personnalités qualifiées des membres de la Commission nationale de l’informatique et des libertés en incluant la protection des libertés individuelles à celles-ci s’accorde parfaitement à cet article.

**1.4. ELEMENTS DE DROIT COMPARE**

La fonction du commissaire du Gouvernement auprès de la Commission nationale de l’informatique et des libertés, autorité de contrôle en matière de protection des données, constitue une spécificité nationale qui n’a pas d’équivalent dans les autres Etats membres de l’Union européenne.

 ** __**

**_2\. OBJECTIFS POURSUIVIS ET NECESSITE DE LEGIFERER_**

 **2.1. OBJECTIFS POURSUIVIS**

L’[article 2 du projet de loi](/pjl-490/titre-i/chapitre-i/article-2) vise à clarifier les compétences requises pour être une personnalité qualifiée de la Commission nationale de l’informatique et des libertés. Il s’agit également d’une mesure de cohérence entre les personnalités qualifiées désignées par le Président de l'Assemblée nationale et le Président du Sénat et celles désignées par le Gouvernement.

L’[article 3](/pjl-490/titre-i/chapitre-i/article-3) poursuit l’objectif d’alléger la présence du commissaire du Gouvernement aux délibérations de la Commission nationale de l’informatique et des libertés en formation restreinte afin qu’il puisse se consacrer d’autant plus à sa mission d’information auprès des administrations pour que les prescriptions en matière d’informatique et libertés soient correctement comprises et appliquées.

Les modifications apportées par cet article sont également guidées par le souci de rehausser au niveau de la loi les garanties d’impartialité exigées par la jurisprudence constitutionnelle à propos du pouvoir de sanction des autorités administratives indépendantes, avec un double objet :

\- prévoir d’une part que le commissaire du Gouvernement, s’il peut être présent aux séances de la formation restreinte compétente pour prononcer des injonctions ou imposer des sanctions, ne peut assister au délibéré qui les suit ;

\- exclure d’autre part de ce délibéré les agents de la commission, à la seule exception de ceux chargés du secrétariat de la séance.

 **2.2. NECESSITE DE LEGIFERER**

L’article 2 du projet de loi permet d’unifier le standard de qualifications nécessaires pour être membre de la Commission nationale de l’informatique et des libertés en tant que personnalité qualifiée.

L’article 3 permet de rendre facultative la présence du commissaire du Gouvernement aux délibérations de la Commission nationale de l’informatique et des libertés en formation restreinte uniquement. Cette faculté permettra au commissaire du Gouvernement de consacrer plus de temps à sa mission d’information auprès des administrations pour veiller au respect des délibérations de la Commission nationale de l’informatique et des libertés.

Il s’agit également de tenir compte de la jurisprudence récente du Conseil constitutionnel sur les garanties d’impartialité des autorités administratives indépendantes en prévoyant, dans la loi, que les membres de la CNIL délibèrent hors de la présence des agents de la commission, à l’exception de ceux chargés de la tenue de la séance, et hors de celle du commissaire du Gouvernement.

 ** _3\. OPTIONS_**

 **3.1. ARTICLE 2 : COMPETENCES REQUISES POUR LES PERSONNES QUALIFIEES**

 **3.1.1. Option 1 (écartée) : Maintien du droit existant**

Il aurait pu être envisagé de ne pas modifier le droit existant sur les compétences requises des personnalités qualifiées.

La dichotomie de qualifications entre les personnalités qualifiées au sein de la Commission nationale de l’informatique et des libertés, selon qu’elles ont été désignées par les présidents des assemblées parlementaires ou par le Gouvernement, n’apparaît pas satisfaisante car elle pourrait laisser entendre une prédominance du numérique sur la protection des libertés individuelles, ce qui ne correspond pas aux missions de la Commission nationale de l’informatique et des libertés.

 **3.1.2. Option 2 (retenue) : Unification des connaissances requises pour être désigné en qualité de personne qualifiée**

Il n’y a pas lieu de faire de distinction de qualifications selon l’autorité qui désigne la personnalité qualifiée. Par ailleurs, l’ajout de la connaissance des questions touchant aux libertés individuelles comme critère de désignation pour l’ensemble des personnalités qualifiées est cohérent avec les missions de la Commission nationale de l’informatique et des libertés qui protège la vie privée et les données à caractère personnel tout en prenant en compte les évolutions technologiques et leurs impacts ([article 11 de la loi n° 78-17](/loi-78-17/chapitre-iii/article-11)).

 **3.2. ARTICLE 3 : PRECISION DES REGLES RELATIVES AU DELIBERE DE LA COMMISSION**

Compte tenu de la jurisprudence récente du Conseil constitutionnel sur les garanties d’impartialité dont doivent disposer les autorités administratives indépendantes dans le cadre de leur pouvoir de sanction, il a été décidé de prévoir expressément dans la loi que, d’une part, les membres de la CNIL délibèrent hors de la présence des agents de la commission à l’exception de ceux chargés de la tenue de la séance, d’autre part, le commissaire du Gouvernement n’assiste pas au délibéré de la formation restreinte de la CNIL.

 **3.3. ARTICLE 3 : PRESENCE DU COMMISSAIRE DU GOUVERNEMENT AUPRES DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES**

 **3.3.1. Option 1 (écartée) : Suppression de la fonction de Commissaire du Gouvernement**

La suppression de la fonction de commissaire du Gouvernement auprès de la Commission nationale de l’informatique et des libertés aurait pu être envisagée à l’occasion du présent projet de loi.

Toutefois, une telle fonction apparaît pertinente. En effet, le commissaire du Gouvernement permet d’informer la Commission nationale de l’informatique et des libertés sur les intentions du Gouvernement afin que celle-ci délibère en toute connaissance de cause.

Sa présence aux délibérations lui permet de comprendre les raisonnements et les motifs des délibérations de la commission afin de s’assurer que les textes règlementaires respecteront les raisonnements de l’autorité de contrôle dégagés lors de ses délibérations.

Ainsi, la présence du commissaire du Gouvernement aux délibérations de la Commission nationale de l’informatique et des libertés contribue ainsi au renforcement de la protection des personnes et des organismes concernés, objectif visé par la garantie d’indépendance accordée aux autorités de contrôle par le « paquet européen ».

Le commissaire du Gouvernement contribue donc indubitablement à la cohérence et à la qualité des prises de décisions de la Commission nationale de l’informatique et des libertés mais aussi à une meilleure protection du droit à la protection des données à caractère personnel et à la vie privée.

La présence du commissaire du Gouvernement est donc également nécessaire pour assurer la présence de la puissance publique pour suivre le fonctionnement d’une activité qui, même si elle est confiée à une autorité indépendante, est susceptible in fine d’engager la responsabilité de l’Etat.

**3.3.2. Option 2 (écartée) : Maintien du droit constant**

La modification n’est pas strictement nécessaire au bon fonctionnement de la Commission nationale de l’informatique et des libertés.

Toutefois, la participation obligatoire aux délibérations en formation restreinte de la Commission nationale de l’informatique et des libertés fait peser une charge de travail importante sur le commissaire du Gouvernement, alors même que toutes les délibérations de cette formation ne concernent pas l’activité administrative.

**3.3.3. Option 3 (retenue) : Rendre facultative la présence du commissaire du Gouvernement aux délibérations de la Commission nationale de l’informatique et des libertés**

Il est proposé de rendre facultative la présence du commissaire du Gouvernement aux délibérations de la Commission nationale de l’informatique et des libertés en formation restreinte. Cette faculté permettra au commissaire du Gouvernement de consacrer plus de temps à sa mission d’information auprès des administrations pour veiller au respect des délibérations de la Commission nationale de l’informatique et des libertés.

Son information sur les délibérations de la Commission nationale de l’informatique et des libertés n’est pas mise en cause par la seule faculté et non plus l’obligation d’y assister puisque les délibérations de la commission sont publiées au journal officiel.

Ainsi qu’il a été précédemment, le commissaire du Gouvernement n’assiste pas en revanche au délibéré de la formation restreinte chargée de prononcer des sanctions.

 ** _4\. ANALYSE DES IMPACTS DES DISPOSITIONS ENVISAGÉES_**

 ** _4.1. IMPACTS JURIDIQUES_**

L’article 2 du projet de loi aura pour effet que les personnalités désignées par les présidents des deux chambres du Parlement le seront au regard de leur connaissance du numérique, mais également des questions touchant aux libertés individuelles.

Concernant l’article 3 du projet de loi, le caractère facultatif et non plus obligatoire de la présence du commissaire du Gouvernement aux séances de la formation restreinte de la Commission nationale de l’informatique et des libertés ou des réunions de son bureau n’aura aucune incidence juridique sur les décisions prises par ces organes. Il en est de même pour la précision dans la loi des garanties d’impartialité déjà présentes dans le décret et respectées par la Commission nationale de l’informatique et des libertés, selon lesquelles la formation restreinte délibère hors la présence des agents de la CNIL et du commissaire du Gouvernement.

 ** _4.2. IMPACTS SUR LES PARTICULIERS_**

L’article 2 du projet de loi améliore la protection des données à caractère personnel. En effet, il sera désormais possible pour les présidents des deux chambres du Parlement de désigner une personnalité qualifiée pour ses connaissances des libertés individuelles ou du numérique, et pas uniquement sur ses connaissances des questions numériques.

**_5\. Consultation_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ier/chapitre-ier/article-1er" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ier/chapitre-ier/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

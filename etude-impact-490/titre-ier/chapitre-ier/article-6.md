<!-- TITLE: ARTICLE 6 -->
<!-- SUBTITLE: MESURES CORRECTRICES ET SANCTIONS -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ier/chapitre-ier/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ier/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**_1\. Etat des lieux et diagnostic_**

 ** _1.1. Etat des lieux_**

La Commission nationale de l’informatique et des libertés dispose d’un pouvoir de sanction prévu au [chapitre VII de la loi n° 78-17 du 6 janvier 1978](/loi-78-17/chapitre-vii) relative à l'informatique, aux fichiers et aux libertés, destiné essentiellement à la mise en conformité des responsables de traitement (articles 45 à 49 bis).

En vertu de l’[article 45 de la loi n° 78-17](/loi-78-17/chapitre-vii/article-45), la procédure de sanction devant la CNIL s’articule entre les pouvoirs d'instruction et de mise en demeure du président de la Commission et les pouvoirs de sanction de la formation restreinte.

Lorsqu'un manquement à la loi est constaté, le président de la CNIL peut mettre en demeure le responsable de traitement de se mettre en conformité (article 45-I). La mise en demeure prescrit alors les mesures à adopter, dans le délai qu'elle fixe et qui ne peut être supérieur à trois mois, renouvelable une fois. La mise en demeure ne présente pas le caractère d'une sanction39. Cette mise en demeure peut être rendue publique sur décision du bureau. Si, à l'expiration du délai ainsi accordé pour se mettre en conformité, le responsable de traitement ne s’est pas conformé à la mise en demeure, le président de la CNIL peut saisir la formation restreinte (composée de six membres élus par leurs pairs), les membres du bureau ne pouvant plus siéger dans cette formation pour prononcer une sanction (article 45-II).

Le président de la Commission peut toutefois saisir directement la formation restreinte, lorsque le manquement ne peut plus faire l'objet d'une mise en conformité dans le cadre d'une mise en demeure. Dans ce cas, qui correspond notamment à des hypothèses de manquements limités dans le temps et n'appelant pas ou plus de mesures de correction, comme des failles de sécurité, la formation restreinte peut prononcer directement une sanction (article 45-I alinéa 7).

Les sanctions peuvent consister en un avertissement, une sanction pécuniaire d'un montant maximal de 3 millions d'euros depuis la loi pour une République numérique du 7 octobre 2016 (contre 150 000 € auparavant), et/ou, le cas échéant, une injonction de cesser le traitement ou un retrait de l'autorisation accordée (article 47). La décision de sanction peut être rendue publique à l'initiative de la formation restreinte (article 46).

Les décisions prononçant une sanction peuvent faire l’objet d’un recours de pleine juridiction devant le Conseil d’Etat (article 46) qui statue en premier et dernier ressort ([article R. 311-1 4° du code de justice administrative](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006449855&cidTexte=LEGITEXT000006070933&dateTexte=20100223)).

L'article 45-III de la loi n° 78-17 prévoit également une procédure d'urgence. Dans ce cas, la formation restreinte peut prendre des mesures adaptées telles que l'interruption de la mise en œuvre du traitement ou le verrouillage des données.

En 2016, sur les 430 contrôles effectués, la CNIL a procédé à 82 mises en demeure et infligé 13 sanctions, dont 4 sanctions financières, toutes publiques et 9 avertissements40.

**_1.2. Cadre constitutionnel_**

Le Conseil constitutionnel considère que « _le principe de la séparation des pouvoirs, non plus qu'aucun autre principe ou règle de valeur constitutionnelle, ne fait obstacle à ce qu'une autorité administrative indépendante, agissant dans le cadre de prérogatives de puissance publique, puisse exercer un pouvoir de sanction dans la mesure nécessaire à l'accomplissement de sa mission, dès lors que l'exercice de ce pouvoir est assorti par la loi de mesures destinées à assurer la protection des droits et libertés constitutionnellement garantis ; qu'en particulier, doivent être respectés le principe de la légalité des délits et des peines ainsi que les droits de la défense, principes applicables à toute sanction ayant le caractère d'une punition, même si le législateur a laissé le soin de la prononcer à une autorité de nature non juridictionnelle ; que doivent également être respectés les principes d'indépendance et d'impartialité découlant de l'article 16 de la [Déclaration de 1789](https://www.legifrance.gouv.fr/Droit-francais/Constitution/Declaration-des-Droits-de-l-Homme-et-du-Citoyen-de-1789)_ »41.

Le projet de loi maintient l'articulation actuelle entre les pouvoirs d'instruction et de mise en demeure du président de la CNIL et les pouvoirs de sanction de la formation restreinte.

Le Conseil d’Etat a jugé, à cet égard, que cette séparation des fonctions d'enquête et de poursuite de celles de sanction au sein de la CNIL, compte tenu de la distinction organique entre le président d’une part, et la formation restreinte d’autre part, est conforme aux droits et libertés garantis par la Constitution42 et ne méconnaît pas les principes d'indépendance et d'impartialité, faute de séparation des fonctions de poursuite et de sanction au sein de la CNIL43 .

Enfin, il est précisé que la Commission nationale de l’informatique et des libertés n’est pas compétente pour contrôler les opérations de traitement de données à caractère personnel effectués par les juridictions agissant dans l’exercice de leur fonction juridictionnelle44. Le principe de séparation des pouvoirs et d’indépendance de l’autorité judiciaire sont ainsi respectés.

 ** _1.3. Cadre conventionnel_**

Le [règlement (UE) 2016/679](/reglement-2016-679) se caractérise par la mise en place d’une approche de responsabilisation des organismes traitant des données à caractère personnel. Ce règlement prévoit ainsi de passer d’un système de contrôle _ex ante_ de la CNIL à un système de contrôle _ex post_. Ce changement de paradigme permettra aux autorités de contrôle de concentrer davantage leurs actions sur la mission de sensibilisation et d’accompagnement des responsables de traitement et de disposer de moyens de contrôle et mesures correctrices plus conséquentes et dissuasives en cas de violation constatée des règles applicables.

L’[article 58](/reglement-2016-679/chapitre-vi/section-2/article-58) du règlement (UE) 2016/679 et [47](/directive-2016-680/chapitre-vi/section-2/article-47) de la directive (UE) 2016/680 prévoient les pouvoirs des autorités de contrôle45 en précisant leurs pouvoirs d’enquête (article 58.1 du règlement) et les mesures correctrices qu’elles peuvent prendre (article 58.2).

L’[article 83](/reglement-2016-679/chapitre-viii/article-83) du règlement définit les conditions générales selon lesquelles les autorités de contrôle peuvent imposer des amendes administratives. Celles-ci peuvent s'élever jusqu'à 10 millions euros ou, dans le cas d'une entreprise, jusqu'à 2 % du chiffre d'affaires annuel mondial total de l'exercice précédent (20 millions d’euros ou 4 % du chiffre d'affaires en cas de non-respect d’une injonction).

Enfin, l’[article 84](/reglement-2016-679/chapitre-viii/article-84) du règlement et l’[article 57](/directive-2016-680/chapitre-viii/article-57) de la directive prévoient que les Etats membres déterminent le régime des autres sanctions applicables en cas de violations du règlement, en particulier pour les violations qui ne font pas l'objet des amendes administratives, et prennent toutes les mesures nécessaires pour garantir leur mise en œuvre. Ces sanctions doivent être effectives, proportionnées et dissuasives.

En outre, ainsi qu’il a été dit précédemment, le projet de loi ne modifie pas la séparation entre les pouvoirs d'instruction et de mise en demeure du président de la CNIL et les pouvoirs de sanction de la formation restreinte. Cette dernière, qui peut prendre des sanctions, est soumise aux exigences qui découlent de l’article 6 de la [convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales](https://www.coe.int/fr/web/conventions/full-list/-/conventions/rms/0900001680063776), notamment en termes de respect des droits de la défense et du caractère contradictoire de la procédure46 .

 ** _1.4. Eléments de droit comparé_**

Certains États membres de l’Union européenne ne permettent pas à leur autorité de protection des données d’adopter des mesures correctrices. L’entrée en vigueur du règlement (UE) 2016/679 conduira ces États à modifier les compétences de leurs autorités de contrôle.

Alors que les autorités de contrôle de l’Allemagne, d’Espagne, de la France, de l’Italie ou du Royaume-Uni peuvent déjà adopter des mesures correctrices, certains Etats membres modifient actuellement les pouvoirs de leurs autorités de contrôle afin de leur attribuer des pouvoirs de sanction à l’image de la Belgique47 et du Luxembourg48.

D’autres États membres complètent les pouvoirs dont disposaient déjà leurs autorités actuellement comme l’Allemagne49 dans le cadre de la loi générale sur la protection des données adoptée en juin 2017 (mais dont les dispositions relatives aux autorités de protection des données relèvent toutefois majoritairement des Länders) ou encore le Royaume-Uni50.

Le projet de loi organique espagnol renvoie expressément aux pouvoirs prévus aux articles 57 et 58 du règlement (UE) 2016/67951. Il n’attribue cependant pas de pouvoirs supplémentaires à l’autorité de protection des données nationale, mais des autorités de protection des données existent au sein des communautés autonomes.

 ** _2\. Objectifs poursuivis et necessite de légiferer_**

 ** _2.1. Objectifs poursuivis_**

Le présent projet de loi remplace les articles [45](/loi-78-17/chapitre-vii/article-45), [46](/loi-78-17/chapitre-vii/article-46), [47](/loi-78-17/chapitre-vii/article-47) et [48](/loi-78-17/chapitre-vii/article-48) de la loi n° 78-17 pour permettre à la CNIL de prendre les mesures correctrices prévues par le règlement (UE) 2016/679 ou par la directive (UE) 2016/680 et compléter celles déjà existantes.

Ces dispositions visent principalement à attribuer à la Commission nationale de l’informatique et des libertés le pouvoir d’adopter les mesures correctrices rendues nécessaires par l’application du règlement, et le changement de paradigme qu’il opère.

Il s’agit de maintenir l’efficacité des mesures correctrices de la Commission nationale de l’informatique et des libertés dans le contexte de responsabilisation des acteurs. Il s’agit également de garantir une protection efficace des personnes concernées face aux traitements qui violeraient les dispositions de la loi ou du règlement européen.

Les mesures correctrices envisagées sont attribuées, selon qu’elles ont ou non le caractère de sanction, au président de la Commission nationale de l’informatique et des libertés ou à la formation restreinte de cette Commission. Elles permettront à la Commission d’agir en vue d’assurer le respect de la protection des données à caractère personnel tout en répondant aux nouvelles exigences du règlement.

 ** _2.2. Nécessité de legiferer_**

Le règlement met en place une approche de responsabilisation. Ce changement de paradigme nécessite, en contrepartie, d’adapter les missions et pouvoirs des autorités de contrôle, en particulier en matière de sanction.

Le texte proposé, en se conformant au règlement, adapte les mesures correctrices, plus conséquentes et dissuasives en cas de violation constatée des règles applicables.

 ** _3\. Options_**

 **3.1. Remplacement de l’article 45 de la loi n° 78-17**

 **3.1.1. Option 1 (écartée) : Ne pas prévoir une répartition des compétences entre le président et la formation restreinte en matière de mesures correctrices**

Le règlement ne précise pas la réparation des pouvoirs au sein de l’autorité de contrôle.

Dans ces conditions, il aurait pu être envisagé de renvoyer au règlement intérieur de la Commission nationale de l’informatique et des libertés à la répartition des pouvoirs entre la présidence et la formation restreinte. Ce choix n’a pas été fait dans la loi n° 78-17 par le passé, il n’a pas été fait non plus dans le cadre de la [loi n° 2017-55](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033897475&categorieLien=id) relative aux autorités administratives indépendantes.

Cette possibilité a également été écartée afin de préserver le droit constant, en respectant ainsi la volonté récemment exprimée du législateur. Il s’agit en effet d’un élément important qui permet de définir le régime de poursuite et de sanction au sein de CNIL au regard des exigences constitutionnelles et conventionnelles précédemment rappelées.

 **3.1.2. Option 2 (écartée) : Procéder à un simple renvoi au règlement**

Un simple renvoi aux dispositions de l’[article 58.2 du règlement](/reglement-2016-679/chapitre-vi/section-2/article-58) n’apparaît pas suffisant dans la mesure où ce dernier ne précise pas l’articulation qui doit s’opérer au sein de l’autorité de contrôle entre les fonctions d’instruction et de mise en demeure, et celles de sanction.

En outre, il a été décidé de doter la CNIL des mesures correctrices prévues par le règlement, y compris pour les traitements ne relevant pas de ce dernier (c’est-à-dire ceux relevant du champ d’application de la directive et ou qui sont hors du champ d’application du droit de l’Union européenne).

 **3.1.3. Option 3 (retenue) : Modification de l’article 45 de la loi n° 78-17 en renforçant les mesures que peut prendre la formation restreinte**

Le projet de loi confère à la Commission nationale de l’informatique et des libertés la possibilité de prononcer les mesures correctrices de l’article 58.2 du règlement. Il adapte également la terminologie employée aux termes du règlement. Par exemple, le règlement évoque l’injonction de « mettre en conformité » et non la « mise en demeure ».

Le projet de loi précise la répartition des pouvoirs entre la présidence de la Commission nationale de l’informatique et des libertés et la formation restreinte.

Il attribue au président de la Commission nationale de l’informatique et des libertés un pouvoir d’avertissement du responsable de traitement ou sous-traitant qu’il est susceptible de violer le règlement. Il s’agit d’un avertissement qui n’a pas le caractère de sanction comme c’est le cas actuellement.

La formation restreinte peut, quant à elle, prononcer un « rappel à l’ordre » équivalent de l’actuel « avertissement ».

En outre, dans la mesure où l’article 84 du règlement et l’article 57 de la directive permettent de prévoir des sanctions supplémentaires, dès lors que celles-ci sont proportionnées et dissuasives au sens de ces articles par rapport à l’objectif de mise en conformité aux obligations des responsables de traitement52 il est proposé d’assortir d’une astreinte l’injonction à un responsable de traitement de se mettre en conformité avec la loi ou le règlement (UE) 2016/679 ou de satisfaire aux demandes présentées par une personne concernée en vue d'exercer ses droits. L’astreinte permettra ainsi de faire cesser rapidement l’atteinte aux droits des personnes concernées, alors qu’il pourrait apparaître économiquement rentable à certains organismes de perpétuer des traitements qui ne respectent pas le règlement ou la loi.

D’autres pouvoirs sont ajoutés, comme par exemple la possibilité, non-prévue par le règlement, de retirer la décision d’approbation d’une règle d’entreprise contraignante.

Il est à noter que plusieurs de ces mesures (par exemple le retrait de la certification et le retrait de la décision d’approbation d’une règle d’entreprise contraignante) sont prononcées par la formation restreinte à titre de sanction, sans préjudice de la possibilité pour l’organe qui aura délivré la certification ou la décision d’approbation (formation plénière) de les retirer lorsqu’il constatera (sans visée répressive) que les conditions légales ne seront objectivement plus remplies.

 **3.2. Remplacement de l’article 46 de la loi n° 78-17 relatif à la procédure d’urgence**

 **3.2.1. Option 1 (écartée) : Ne pas préciser les modalités applicables à la procédure d’urgence**

L’articulation entre l’actuelle procédure d’urgence de la loi n° 78-17 devant la formation restreinte et celle prévue à l’article 66 du règlement (qui permet à une autorité de protection des données de se dispenser provisoirement du mécanisme de coopération et de contrôle de cohérence prévu par ce règlement) est apparue complexe.

En effet, ces deux approches diffèrent sur plusieurs points :

\- quant à la nature des mesures, puisque l’article 45.II de la loi n° 78-17 prévoit des mesures non-provisoires (comme l’avertissement), alors que l’article 66 du règlement ne permet à l’autorité concernée que de prendre des mesures provisoires ;

\- quant au critère de l’urgence, l’article 45 de la loi n° 78-17 mentionne l’urgence à agir en présence d’une violation des droits et libertés, tandis que l’article 66 du règlement évoque des « circonstances exceptionnelles » et une « urgence à intervenir pour protéger les droits des personnes concernées ».

Cette option a donc été écartée.

 **3.2.2. Option 2 (retenue) : Prévoir une procédure d’urgence propre dans le cadre de la loi nationale**

L’option retenue a été de prévoir une procédure d’urgence « de droit commun » pour les cas n’appelant ni coopération ni cohérence entre autorités de contrôle ou pour les procédures hors du champ d’application du règlement.

Le projet de loi prévoit ainsi de maintenir les mesures que peut prendre actuellement la formation restreinte (1°, 2°, 6° et 7° du I de l’[article 46](/loi-78-17/chapitre-vii/article-46) de la loi n° 78-17), en ajoutant d’autres types de mesures correctrices qui seraient susceptibles d’être prononcées dans le nouveau contexte juridique (suspension de la certification ou d’un agrément, suspension provisoire de l’autorisation délivrée au titre du chapitre IX concernant les traitements de données de santé) ou encore qui apparaissent opportunes (injonction).

Les II et III résultent de la nécessité de mettre en œuvre les paragraphes 2 à 4 de l’[article 66](/reglement-2016-679/chapitre-vii/section-2/article-66) du règlement.

Le [IV de l’article 46 proposé](/pjl-490/titre-i/chapitre-i/article-6 "Cf.  article 6 du PJL, III.") reprend l’[article 45.III actuel de la loi n° 78-17](/chapitre-vii/article-45).

 **3.3. Modification de l’[article 47](/chapitre-vii/article-47) de la loi n° 78-17**

 **3.3.1. Option 1 (écartée) : Suppression de la publicité des mesures prises par la formation restreinte**

Le second alinéa aurait pu supprimer la publication de la mesure prise par la formation restreinte. Cette option a cependant été écartée en raison du caractère dissuasif de cette mesure et de la sensibilisation qu’apporte une telle publication.

 **3.3.2. Option 2 (retenue) : Maintien du droit existant**

Le projet de loi propose de maintenir le droit existant concernant la procédure de sanction ([article 46](/loi-78-17/chapitre-vii/article-46) de la loi n° 78-17).

Le second alinéa prévoit la publicité de la sanction. Cette publicité n’est pas prévue par le règlement mais est ajoutée compte tenu de la marge de manœuvre précitée de l’article 84 du règlement dès lors que la décision de rendre publique la sanction prononcée a le caractère d’une sanction complémentaire53. Ce pouvoir permet une meilleure harmonisation du droit. Elle permet également la sensibilisation des personnes concernées, des responsables de traitement et sous-traitant permettant à ces publics de prendre des mesures appropriées pour les traitements de données qu’ils opèrent.

 **3.4. Remplacement de l’[article 48](loi-78-17/chapitre-vii/article-48) de la loi n° 78-17 pour introduire un mécanisme de retrait d’agrément **

**3.4.1. Option 1 (écartée) : Se limiter aux manquements des seuls responsables de traitement et sous-traitants**

Les articles 45 et 46, tels que proposés par le projet de loi, prévoient des mesures correctrices à l’encontre des responsables de traitement et sous-traitants, ainsi que le garantit le règlement. Il aurait dès lors été possible de ne pas prévoir d’autres possibilités de mises en demeure ou sanction. Cette possibilité a été écartée.

 **3.4.2. Option 2 (retenue) : Prévoir un mécanisme de retrait d’agrément**

Il apparaît nécessaire de prévoir des pouvoirs répressifs contre d’autres types d’opérateurs que le règlement place dans le champ de compétence de la Commission nationale de l’informatique et des libertés, à savoir les organismes de certification de l’article 43 du règlement et les organismes chargés du respect d’un code de conduite de l’article 41 de ce même règlement.

En effet, dans le cadre de la nouvelle logique de responsabilisation prévue par le règlement, ces opérateurs vont jouer un rôle important pour accompagner les responsables de traitement ou sous-traitants. Il est donc apparu pertinent de pouvoir prononcer à leur encontre un retrait d’agrément lorsque ces derniers ont manqué à leur obligation ou n’ont pas respecté les dispositions de la loi ou du règlement.

Pour la procédure, il est renvoyé à celle applicable aux responsables de traitements pour souci de clarté.

 ** _4\. analyse des impacts de la disposition envisagée_**

 ** _4.1. Impacts juridiques_**

La présente disposition vise à remplacer et à adapter les [articles 45 à 48](/loi-78-17/chapitre-vii) de la loi n° 78-17 pour conférer à la Commission nationale de l’informatique et des libertés de nouveaux pouvoirs. Certains d’entre eux ont un impact juridique direct dès lors que ces mesures, même si elles ne présentent pas toutes le caractère de sanction, constituent des décisions susceptibles de recours54.

Le texte proposé permet une meilleure articulation avec les procédures de cohérence, de coopération et d’urgence prévues par le règlement.

 ** _4.2. Impacts sur les services judiciaires_**

Dans la mesure où le règlement est construit sur une logique de renforcement du contrôle _a posteriori_ des autorités de contrôle, accompagné d’un accroissement substantiel du montant des amendes, en contrepartie de l’allègement du contrôle _ex ante_ du fait de la suppression de la plupart des formalités préalables pour les responsables de traitement, une augmentation des recours dirigés contre les décisions prises par la Commission nationale de l’informatique et des libertés au titre de sa mission de contrôle pourrait intervenir.

Toutefois, cette augmentation pourrait rester mesurée au vu du nombre de sanctions infligées par la CNIL en 2016 (13 sanctions, dont 4 sanctions financières et 9 avertissements).

 ** _4.3. Impacts sur les finances publiques_**

Les sanctions envisagées par le règlement étant plus proportionnées aux bénéfices des responsables de traitement, particulièrement les grands groupes multinationaux (jusqu’à 20 millions euros ou 4 % du chiffre d'affaires annuel, contre 3 millions d’euros actuellement) et les sous-traitants étant également passibles de certaines mesures correctrices, elles pourraient avoir un effet positif sur les finances publiques dans certains cas de non-respect du règlement, les sanctions pécuniaires étant recouvrées comme les créances de l'État étrangères à l'impôt et au domaine.

Il est précisé par ailleurs que le projet de loi, maintenant sur ce point l’article 45 de loi n° 78-17, exclut la possibilité de prononcer des sanctions pécuniaires dans le cas de traitements mis en œuvre par l’Etat.

 ** _4.4. Impacts sur les collectivités territoriales_**

Les mesures proposées, qui renforcent les pouvoirs de sanction et les mesures correctrices que peut prendre la Commission nationale de l’informatique et des libertés, peuvent concerner les collectivités territoriales, en tant qu’elles sont responsables de traitement.

Il convient de relever à cet égard que les contrôles opérés concernent majoritairement les opérateurs de secteur privé. Ainsi, en 2016, comme l’année précédente, 70 % des missions de contrôle réalisées ont concerné le secteur privé, 30 % le secteur public. S’agissant plus spécifiquement des contrôles en ligne, ce sont 90 % des vérifications qui ont été menées dans le secteur privé55.

Les sanctions prononcées par la Commission à l’encontre des collectivités territoriales restent également peu nombreuses : un seul avertissement non public à une collectivité territoriale (en raison de données inadéquates, non pertinentes et excessives et un défaut d'information concernant un traitement de gestion des inscriptions scolaires) et deux mises en demeure non publiques à des communes (pour défaut d’information des personnes et défaut de sécurité et de confidentialité en matière de demande d’état civil en ligne, et défaut d’information et d’accord préalable des personnes en matière de dépôt de cookies) en 201556. En revanche, aucune sanction financière n’a été prononcée à l’encontre d’une collectivité territoriale en 2015 et en 2016.

**_4.5. Impacts sur les particuliers_**

L’impact des mesures proposées est très important puisqu’elles sont de nature à assurer une réelle effectivité des droits des personnes concernées par des mesures dissuasives et proportionnées. En particulier, l’astreinte envisagée assurera le caractère dissuasif des mesures correctrices dans le temps.

Enfin, la publication des mesures permettra une meilleure information des personnes concernées quant aux comportements relatifs à leurs données.

 ** _4.6. Impacts sur les entreprises_**

Les mesures proposées ont pour objet d’avoir un effet dissuasif sur les entreprises, responsables de traitement, mais également sous-traitants de ces derniers.

Dans le cas d’un avertissement, les entreprises qui seraient susceptibles de violer les dispositions pourraient éviter des amendes administratives importantes, lesquelles sont substantiellement renforcées par le règlement.

 ** _5\. Consultations et modalités d’application_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

Le Conseil national d’évaluation des normes a également été consulté, en application de l’[article L. 1212-2 du code général des collectivités territoriales](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033745626&cidTexte=LEGITEXT000006070633&dateTexte=20180114), et a rendu un avis favorable lors de sa séance du 30 novembre 2017.

Les textes réglementaires suivants devront être pris sur le fondement de la loi :

<table>
<tr>
<td>

**Articles du projet de loi renvoyant à des mesures réglementaires**

</td>
<td>

**Nature du texte réglementaire**

</td>
<td>

**Objet du texte réglementaire**

</td></tr>
<tr>
<td>

Article 6 (III)

</td>
<td>

Décret en Conseil d’Etat

</td>
<td>

Définition de la procédure d’urgence contradictoire.

</td></tr></table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ier/chapitre-ier/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ier/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

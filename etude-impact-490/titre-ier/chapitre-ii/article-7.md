<!-- TITLE: ARTICLE 7 -->
<!-- SUBTITLE: DONNÉES SENSIBLES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ier/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**_1\. DIAGNOSTIC_**

 ** _1.1. ETAT DES LIEUX_**

Le premier alinéa de l’[article 8 de la loi n° 78-17](/loi-78-17/chapitre-ii/section-2/article-8) relative à l'informatique, aux fichiers et aux libertés, issu de la transposition de la [directive 95/46/CE du 24 octobre 1995](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000697074) relative à la protection des personnes physiques à l'égard du traitement de données à caractère personnel et à la libre circulation de ces données, pose le principe de l’interdiction de collecter ou de traiter « _des données à caractère personnel qui font apparaître, directement ou indirectement, les origines raciales ou ethniques, les opinions politiques, philosophiques ou religieuses ou l'appartenance syndicale des personnes, ou qui sont relatives à la santé ou à la vie sexuelle de celles-ci._ ». Il s’agit de données qui touchent à l'intimité de la personne ou sont susceptibles de donner lieu à des discriminations.

Des dérogations à cette interdiction sont toutefois prévues, tenant soit à l'objet du traitement soit au consentement de l'intéressé, pour :

\- Les traitements pour lesquels la personne concernée a donné son consentement exprès, sauf dans le cas où la loi prévoit que l'interdiction ne peut être levée par le consentement de la personne concernée (consentement susceptible d'être affecté par un lien de subordination, une situation particulièrement éprouvante ou contrainte, etc.) ;

\- Les traitements nécessaires à la sauvegarde de la vie humaine, mais auxquels la personne concernée ne peut donner son consentement par suite d'une incapacité juridique ou d'une impossibilité matérielle ;

\- Les traitements mis en œuvre par une association ou tout autre organisme à but non lucratif et à caractère religieux, philosophique, politique ou syndical, sous certaines conditions ; il s’agit du cas où la donnée constitue l'objet même de l'association ou de l'organisme avec lequel la personne entretient volontairement une relation ;

\- Les traitements portant sur des données à caractère personnel rendues publiques par la personne concernée ;

\- Les traitements nécessaires à la constatation, à l'exercice ou à la défense d'un droit en justice ;

\- Les traitements nécessaires aux fins de la médecine préventive, des diagnostics médicaux, de l'administration de soins ou de traitements, ou de la gestion de services de santé et mis en œuvre par un membre d'une profession de santé, ou par une autre personne à laquelle s'impose en raison de ses fonctions l'obligation de secret professionnel prévue par l'[article 226-13 du code pénal](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417944&dateTexte&categorieLien=cid) ;

\- Les traitements statistiques réalisés par l'Institut national de la statistique et des études économiques ou l'un des services statistiques ministériels ;

\- Les traitements nécessaires à la recherche, aux études et évaluations dans le domaine de la santé.

Deux autres dérogations sont également prévues : l’une fondée sur « l'anonymisation à bref délai », qui permet, dès lors que les conditions de sécurité sont remplies, de considérer le risque d'exploitation de la donnée sensible comme nul ; l’autre, tenant à « l'intérêt public » du traitement mis en cause, qui peut justifier la collecte et le traitement des données sensibles. Dans ce cas, les traitements doivent être autorisés par la Commission nationale de l’informatique et des libertés sur le fondement des articles 25 ou 26 de la loi n° 78-17 selon les cas de figure.

L’énumération actuelle des données dites « sensibles » à l’[article 8 de la loi n° 78-17](/loi-78-17/chapitre-ii/section-2/article-8) ne correspond pas tout à fait aux catégories particulières de données à caractère personnel énoncées à l’[article 9 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-ii/article-9) ou à celles prévues à l’[article 10 de la directive (UE) 2016/680](/directive-2016-680/chapitre-ii/article-10).

Par rapport à la rédaction du premier alinéa de l’article 8 de la loi n° 78-17, les deux textes européens prévoient également, dans le champ des données sensibles, les données génétiques et les données biométriques. Ils contiennent par ailleurs des différences sémantiques : l’origine au singulier et non au pluriel, le terme de « convictions » religieuses ou philosophiques plutôt que celui d’« opinions ». Il en va, enfin de même concernant les données sur « la vie sexuelle ou l’orientation sexuelle », tandis que le droit national mentionne seulement la « vie sexuelle ».

**_1.2. CADRE CONSTITUTIONNEL_**

Le Conseil constitutionnel, dans [sa décision du 29 juillet 2004](http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2004/2004-499-dc/decision-n-2004-499-dc-du-29-juillet-2004.904.html), se prononçant sur l’article 8 de la loi n° 78-17, a jugé que ces dispositions se bornent à tirer les conséquences nécessaires des dispositions inconditionnelles et précises du e) du 2 de l'[article 8 de la directive 95/46/CE du 24 octobre 1995](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000697074) sur lesquelles il ne lui appartient pas de se prononcer. Dès lors, et conformément à sa jurisprudence relative à la transposition des directives européenne, le Conseil a jugé conforme à la Constitution l’article 8 de la loi du 6 janvier 1978, après avoir considéré que le grief tiré de l'atteinte au respect de la vie privée, invoqué à l’encontre de la dérogation prévue à l’interdiction de traitement de données sensibles pour les traitements nécessaires à la constatation, à l'exercice ou à la défense d'un droit en justice, ne peut être utilement présenté devant lui57.

Il a confirmé sa jurisprudence à propos de la dérogation également prévues pour les traitements nécessaires aux fins de la médecine préventive, des diagnostics médicaux, de l'administration de soins ou de traitements, ou de la gestion de services de santé58.

 ** _1.3. CADRE CONVENTIONNEL_**

L'article 6 de la [convention n° 108 du Conseil de l'Europe du 28 janvier 1981](https://www.coe.int/fr/web/conventions/full-list/-/conventions/treaty/108), pour la protection des personnes à l'égard du traitement automatisé des données à caractère personnel, prévoit que : _« Les données à caractère personnel révélant l'origine raciale, les opinions politiques, les convictions religieuses ou autres convictions, ainsi que les données à caractère personnel relatives à la santé ou à la vie sexuelle, ne peuvent être traitées automatiquement à moins que le droit interne ne prévoie des garanties appropriées. Il en est de même des données à caractère personnel concernant des condamnations pénales_ ».

Le « paquet européen de protection des données » pose également, de façon similaire pour le règlement (UE) 2016/679 ([article 9](/reglement-2016-679/chapitre-ii/article-9)) et pour la directive UE 2016/680 ([article 10](/directive-2016-680/chapitre-ii/article-10)), un principe d’interdiction de traitement des données sensibles, assorties de certaines exceptions: « _Le traitement des données à caractère personnel qui révèle l'origine raciale ou ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l'appartenance syndicale, ainsi que le traitement des données génétiques, des données biométriques aux fins d'identifier une personne physique de manière unique, des données concernant la santé ou des données concernant la vie sexuelle ou l'orientation sexuelle d'une personne physique sont interdits. […]_ ».

**_2\. OBJECTIFS POURSUIVIS ET NECESSITE DE LEGIFERER_**

 ** _2.1. OBJECTIFS POURSUIVIS_**

L’objectif poursuivi par cette disposition est la mise en cohérence de l’ensemble du droit national afin de proposer un cadre juridique unifié pour l’encadrement des traitements des données dites « sensibles ».

**_2.2. NECESSITE DE LEGIFERER_**

Il est nécessaire de modifier les dispositions de la loi n° 78-17 qui entrent dans le champ d’application du règlement général de protection des données et de la directive en ajoutant les données génétiques et les données biométriques aux fins d'identifier une personne physique de manière unique à cette liste. Ces données font déjà l’objet d’une protection particulière en droit national de par les formalités préalables qui leur est appliqué (régimes d’autorisation prévus aux articles [25](/loi-78-17/chapitre-iv/section-2/article-25) et [27](/loi-78-17/chapitre-iv/section-2/article-27) de la loi n° 78-17).

Dans un souci de cohérence, il est proposé d’appliquer une énumération unique des données sensibles à l’ensemble du projet de loi.

**_3\. OPTIONS_**

 **3.1. Option 1 (écartée) : Ne pas modifier l’énumération des catégories particulières de données pour les traitements qui ne relèvent pas du champ du droit de l’Union européenne**

Il pourrait être envisagé de ne modifier la liste des catégories particulières de données que pour les traitements relevant de la directive. Le règlement étant d’application directe, cette définition trouvera à s’appliquer sans nécessité de modifier la loi n° 78-17 pour les traitements entrant dans le champ du règlement.

Ainsi, deux listes distinctes mais proches subsisteraient dans l’ordre juridique interne. Cela créerait une difficulté d’appréhension du droit par les acteurs d’une part, conduirait à une baisse du niveau de protection des données à caractère personnel les plus sensibles dans certains traitements mis en œuvre par l’Etat (défense nationale par exemple) et pour les données sensibles des personnes décédées, d’autre part.

Dans un souci de cohérence du droit de la protection des données à caractère personnel, cette option n’a pas été retenue.

**3.2. Option 2 (retenue) : Définition unique des catégories particulières de données pour l’ensemble du droit national**

Il est proposé d’augmenter le niveau de protection en intégrant dans le champ des données sensibles les données biométriques et génétiques pour l’ensemble du droit national, y compris pour les traitements ne relevant pas du droit de l’Union européenne.

En effet, la France a toujours protégé ces deux catégories particulières de données (notamment par la mise en place de la formalité préalable d’autorisation) même si elles n’apparaissaient pas dans la liste énoncée au premier alinéa de l’[article 8](/loi-78-17/chapitre-ii/section-2/article-8) de la loi n° 78-17. Cette insertion permet d’interdire en principe leur traitement.

Dans un souci de cohérence du droit national, ainsi que de sécurité juridique tant pour les responsables de traitement que pour les personnes concernées, il est proposé d’étendre la qualification de catégorie particulière de données aux données génétiques et biométriques, à l’instar du droit européen applicable, à l’ensemble des dispositions nationales.

Dans la mesure où les données biométriques constituent des données sensibles dont le traitement est en principe interdit, il est apparu nécessaire de maintenir une dérogation à l’interdiction posée au premier alinéa de l’article 8 pour les traitements mis en œuvre par les employeurs ou les administrations qui portent sur des données biométriques nécessaires aux contrôles de l’accès aux lieux de travail ainsi qu’aux appareils et aux applications utilisés dans le cadre des missions confiées aux salariés ou aux agents, actuellement autorisés par la CNIL59 sur le fondement de l’[article 25 de la loi n° 78-17](/loi-78-17/chapitre-iv/section-2/article-25) abrogé par le présent projet de loi.

La dérogation également prévue pour les traitements mis en œuvre par l’Etat, dans l’exercice de ses prérogatives de puissance publique, portant sur des données génétiques ou biométrique aux fins d’authentification ou d’identification, est quant à elle maintenue (cf. infra, article 9).

Ces dérogations complètent les cas dans lesquels des données sensibles peuvent être exceptionnellement traitées en vertu du deuxième alinéa de l’article 8 de la loi n° 78-17, outre les cas prévus par l’[article 9.2 du règlement](/reglement-2016-679/chapitre-ii/article-9).

Enfin, la [loi n° 2017-86 du 27 janvier 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033934948&categorieLien=id) relative à l'égalité et à la citoyenneté a remplacé dans le code pénal et d’autres textes l’expression « en raison de la race » par « en raison de la prétendue race ». Il est apparu cohérent de faire de même pour la notion « d’origine raciale », et de parler de « prétendue origine raciale » dans le cadre de la loi n°78-1760.

 ** _4\. ANALYSE DES IMPACTS DE LA DISPOSITION ENVISAGÉE_**

 ** _4.1. IMPACTS JURIDIQUES_**

L’impact juridique est l’insertion de deux nouvelles catégories particulières de données dont le traitement est en principe interdit : les données génétiques et les données biométriques aux fins d'identifier une personne physique de manière unique.

Les modifications sémantiques sont quant à elles mineures et ne devraient pas emporter d’impact juridique par rapport au droit existant. En effet, et à titre d’exemple, si l’article 8 de la loi n° 78-17, dans sa rédaction actuelle, ne mentionne pas expressément l’orientation sexuelle parmi les données sensibles, le juge fait déjà une interprétation extensive des données relatives à la vie sexuelle pour y intégrer cette notion. Ainsi, le Conseil d’Etat a jugé que les données relatives aux signataires de pacte civil de solidarité, si elles n'ont pas pour objet de révéler les orientations sexuelles des personnes concernées, peuvent néanmoins permettre de déduire l'existence d'une vie de couple entre des personnes de même sexe, ce qui justifie la limitation à certaines catégories de tiers de la communication de ces informations61.

 ** _4.2. IMPACTS SUR LES PARTICULIERS_**

Du fait de l’interdiction de principe de traiter ces catégories particulières de données, le droit à la protection des données à caractère personnel est renforcé à l’égard des personnes concernées pour les traitements ne relevant pas du droit de l’Union européenne.

**_5\. Consultation_**

La Commission nationale de l'informatique et des libertés a été consultée sur cet article.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-ier/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

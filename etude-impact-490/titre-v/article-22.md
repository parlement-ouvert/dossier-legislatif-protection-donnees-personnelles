<!-- TITLE: ARTICLE 22 -->
<!-- SUBTITLE: MISE A DISPOSITION DE LA LISTE DES TRAITEMENTS AYANT FAIT L’OBJET DE FORMALITES PREALABLES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-v/article-23" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**_1\. Etat de lieux et diagnostic_**

 ** _1.1. Etat des lieux_**

Depuis la [loi n° 2004-801 du 6 août 2004 relative à la protection des personnes physiques à l'égard des traitements de données à caractère personnel et modifiant la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000441676), la Commission nationale de l’informatique et des libertés tient à la disposition du public la liste des traitements automatisés ayant fait l'objet d'une des formalités prévues par les articles 23 à 27, à l'exception de ceux mentionnés au [III de l'article 26](/loi-78-17/chapitre-iv/section-2/article-26) (traitements dits de « souveraineté » ne faisant pas l’objet d’une publication).

La [loi n° 2016-1321 du 7 octobre 2016 pour une République numérique](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746&dateTexte=20180114) a fait basculer ce registre des formalités préalables dans l’ _open data_ en prévoyant une mise à disposition du public, dans un format ouvert et aisément réutilisable145.

Lorsqu’un responsable de traitement dispose d’un correspondant à la protection des données, ce droit s'exerce directement auprès de ce dernier, qui doit tenir la liste des traitements mis en œuvre ([article 22-III de la loi n° 78-17](/loi-78-17/chapitre-iv/article-22)).

Cette disposition législative permet une mise à disposition du public, dans un format ouvert et aisément réutilisable la liste des traitements ayant fait l’objet d’une formalité auprès de la Commission nationale de l’informatique et des libertés. Ce droit de communication ne correspond pas au droit d’accès aux documents administratifs prévu par le code des relations entre le public et l’administration dès lors que la liste en question constitue, par nature, un document inachevé, puisqu’il évolue avec les nouvelles formalités préalables qui interviennent auprès de la commission et qu’il contient des données à caractère personnel (identité du responsable de traitement, adresse,), deux caractéristiques qui font obstacle au droit à l’accès des documents administratifs146.

Enfin, au titre du II de l’[article 31 de la loi n° 78-17](/loi-78-17/chapitre-iv/section-3/article-31), la Commission nationale de l’informatique et des libertés tient à la disposition du public l'intégralité de ses avis, décisions ou recommandations.

 ** _1.2 Cadre conventionnel_**

Le [règlement (UE) 2016/679](/reglement-2016-679) ne prévoit pas d’obligation de mise à disposition d’un tel registre puisque la logique de responsabilisation du règlement (UE) 2016/679 permet la suppression de la grande majorité des formalités préalables et que le droit d’accès apporte une réponse aux personnes concernées désireuses de connaître ces informations (articles [13](/reglement-2016-679/chapitre-iii/section-2/article-13) et [14](/reglement-2016-679/chapitre-iii/section-2/article-13)).

Pour autant, le règlement ne s’oppose pas non plus à une mesure d’information du public et des personnes concernées, puisque le droit à l’information est une composante majeure de la protection des données à caractère personnel ([article 15](/reglement-2016-679/chapitre-iii/section-2/article-15)).

**_2\. Objectifs poursuivis et necessite de legiferer_**

 ** _2.1 Objectifs poursuivis_**

Cette disposition vise à permettre la réutilisation des informations concernant les traitements ayant fait l’objet de formalités préalables actuellement mise à disposition du public par la Commission nationale de l’informatique et des libertés, lesquelles ne pourraient pas être réutilisées dans le cadre du droit commun d’accès aux documents administratifs compte tenu des données contenues dans le registre.

Cette disposition assure également une pérennité à ce document, puisque de nombreux traitements sont actuellement en cours, contribuant ainsi à l’information des personnes concernées.

**_2.2 Nécessité de legiferer_**

L’entrée en application du [règlement (UE) 2016/679](/reglement-2016-679) et sa logique de responsabilisation, ainsi que la suppression de la majorité des formalités préalables devrait conduire à la disparition du registre des traitements actuellement mis à disposition par la Commission nationale de l’informatique et des libertés au titre de l’[article 31 de la loi n° 78-17](/loi-78-17/chapitre-iv/section-3/article-31).

De nombreux traitements en cours se poursuivront après le 25 mai 2018, date d’entrée en application du règlement, il serait préjudiciable pour le droit à l’information des personnes concernées de ne plus avoir connaissance de l’ensemble des traitements mis en œuvre.

Il apparaît dès lors nécessaire de prévoir un nouveau registre des autorisations existantes, en plus des publications officielles.

Ce registre constitue un outil pertinent pour les actuels correspondants à la protection des données, il le sera également pour les futurs délégués à la protection des données. Cette disposition apparaît cohérente avec l’[article 10](/pjl-490/titre-ii/chapitre-iii/article-10) du projet de loi qui prévoit par ailleurs que ce délégué pourra communiquer la liste des traitements effectués par le responsable de traitement qui l’a désigné à toute personne qui lui en fait la demande.

 ** _3\. Options_**

 **Option 1 (écartée) : Absence de mesure législative**

L’obligation de tenue à disposition du public du registre prévu à l’[article 31](/loi-78-17/chapitre-iv/section-3/article-31) aurait vocation à disparaître dès lors que le règlement ne comporte pas une telle obligation pour les autorités de contrôle. Cette liste des traitements déclarés ou autorisés aurait ainsi probablement disparue faute de maintenance.

Cette option a été écartée.

 **Option 2 (écartée) : Maintien du registre prévu à l’article 31 pour une durée d’un an**

Il aurait pu être envisagé de maintenir ce registre dans un format ouvert et aisément réutilisable pendant une durée d’un an, afin de faire la transition entre le droit existant sous l’empire de la [directive 95/46/CE](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A31995L0046) et le [règlement (UE) 2016/679](/reglement-2016-679). Cette durée semble insuffisante compte tenu du nombre de traitements recensés qui continueront à être mis en œuvre au-delà du 25 mai 2018 et même du 25 mai 2019. L’effet utile de ce registre en serait considérablement réduit.

**Option 3 (retenue) : Maintien du registre prévu à l’article 31 pour une durée de dix ans**

Il est proposé, que pour les traitements ayant fait l’objet de formalités antérieurement à l’entrée en vigueur de la future loi, l’obligation pour la Commission nationale de l’informatique et libertés, prévue à l’[article 31 de la loi n°78-17](/loi-78-17/chapitre-iv/section-3/article-31), de mettre à la disposition du public, dans un format ouvert et aisément réutilisable, la liste des traitements arrêtée à cette date, pour une durée de dix ans.

Le droit à l’information des personnes concernées, mais également des usagers des services publics à connaître des traitements mis en œuvre par les administrations, sera prolongé couvrant ainsi des traitements existants.

**_4\. Analyse des impacts de la disposition envisagée_**

 ** _4.1. Impacts juridiques_**

Cette disposition vise à maintenir en l’état l’obligation faite à la Commission nationale de l’informatique et des libertés de mettre à disposition un registre en _open data_ la liste des traitements ayant fait l’objet de formalités préalables, prévue par la loi pour une République numérique, pour une durée de 10 ans.

**_4.2 Impacts sur les particuliers_**

La présente disposition aura un impact sur les particuliers puisqu’ils pourront continuer à consulter la liste des traitements, mis en œuvre avant le 25 mai 2018, ayant fait l’objet de formalités préalables publiques.

Il s’agit d’une mesure qui contribue à l’exercice effectif du droit à l’information, tant au titre de l’accès à l’information publique pour les traitements mis en œuvre par des administrations, qu’à l’égard des personnes concernées.

**_4.3 Impact sur les entreprises_**

Les entreprises auront la possibilité de réutiliser les informations contenues dans ce registre, dans le respect de la [loi n°78-17](/loi-78-17).

**_5\. Consultation_**

La Commission nationale de l’informatique et des libertés a été consultée sur cet article.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-v/article-23" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: ARTICLE 23 -->
<!-- SUBTITLE: MODIFICATION DE L’ARTICLE 230-8 DU CODE DE PROCEDURE PENALE -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-v/article-22" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-v/articles-24" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

L’article 23 du présent projet de loi réécrit le premier alinéa de l’article 230-8 afin de tirer les conséquences de la décision 2017-670 QPC du 27 octobre 2017 par laquelle le Conseil constitutionnel a censuré cet alinéa.

 ** _1\. ETAT DES LIEUX ET DIAGNOSTIC_**

 **1.1. ETAT DES LIEUX**

L’article 230-8 du CPP est issu de la loi n° 2011-267 du 14 mars 2011 d’orientation et de programmation pour la performance de la sécurité intérieure, dite « LOPPSI », qui a inséré au sein de ce code un chapitre dédié aux « fichiers de police judiciaire », dont la première section traite des « fichiers d’antécédents » (articles 230-6 à 230-9).

L’article 230-6 du CPP autorise les services de police et de gendarmerie, afin de faciliter la constatation des infractions à la loi pénale, le rassemblement des preuves de ces infractions et la recherche de leurs auteurs, à inscrire dans un fichier les données recueillies dans deux cadres :

\- d’une part, au cours des enquêtes préliminaires ou de flagrance ou des investigations exécutées sur commission rogatoire et concernant tout crime ou délit ainsi que les contraventions de la cinquième classe sanctionnant un trouble à la sécurité ou à la tranquillité publiques ou une atteinte aux personnes, aux biens ou à l’autorité de l’État ;

\- d’autre part, au cours des procédures de recherche des causes de la mort mentionnées à l’article 74 du CPP ou de recherche des causes d’une disparition mentionnées à l’article 74-1.

En application de l’article 230-7 du CPP, trois catégories de personnes sont susceptibles de voir leurs données personnelles inscrites dans un fichier d’antécédents judiciaires :

\- les personnes, sans limitation d’âge, à l’encontre desquelles il existe des indices graves ou concordants rendant vraisemblable qu’elles aient pu participer, comme auteurs ou complices, à la commission des infractions mentionnées précédemment ;

\- les victimes de ces infractions ;

\- les personnes faisant l’objet d’une enquête ou d’une instruction pour recherche des causes de la mort ou d’une disparition.

À l’instar de la loi n° 2003-239 du 18 mars 2003 pour la sécurité intérieure, concernant les anciens fichiers « système de traitement des infractions constatées » (STIC) et « système judiciaire de documentation et d’exploitation » (JUDEX), la loi du 14 mars 2011 a placé les fichiers de traitement des antécédents judiciaires, destinés à remplacer ces deux fichiers, sous le contrôle du procureur de la République territorialement compétent. Elle a cependant innové en confiant également ce contrôle à un « magistrat référent »1 doté des mêmes pouvoirs pour le suivi des données contenues dans les fichiers d’antécédents judiciaires (article 230-9 CPP). Les personnes mises en cause peuvent donc s’adresser à deux magistrats différents pour obtenir la même décision.

Le texte renvoie à un décret le soin de fixer la durée de conservation des données.

Toutefois, il a prévu deux mécanismes d’effacement anticipé des données concernant les personnes mises en cause :

\- un effacement de principe en cas de décision de relaxe ou d’acquittement devenue définitive, sauf si le procureur de la République ou le magistrat référent décide d’en prescrire le maintien « pour des raisons liées à la finalité du fichier » (cette formule a été modifiée par la loi du 3 juin 2016). Dans ce dernier cas, la décision doit faire l’objet d’une mention dans le fichier – ce qui exclut l’accès aux données personnelles dans le cadre d’enquêtes administratives – et la personne en est avisée ;

\- une possibilité d’effacement en cas de décision de non-lieu ou de classement sans suite motivée par une insuffisance de charges (le périmètre de cette faculté a ensuite été étendu par la loi du 3 juin 2016 précitée). Contrairement au cas précédent, le principe est ici le maintien des données, assorti d’une mention au fichier (cette mention interdisant l’utilisation des données personnelles correspondantes dans le cadre d’enquêtes administratives), et l’exception l’effacement, discrétionnairement ordonné par le procureur de la République ou le magistrat référent.

Corrélativement, les personnes mises en cause dont les données ont été inscrites dans un fichier de traitement d’antécédents judiciaires se sont vues reconnaître la possibilité d’exiger une rectification en cas de requalification judiciaire. Cette rectification est de droit.

Pour les autres hypothèses d’inscription dans un fichier d’antécédents judiciaires, telles qu’une condamnation pénale ou un classement sans suite fondé sur un autre motif que l’insuffisance de charges, aucune possibilité expresse d’effacement anticipé n’avait en revanche été prévue à l’article 230-8 du CPP. Dans un avis rendu le 30 mars 20163, le Conseil d’État a cependant considéré que si l’effacement n’était pas possible dans le premier cas, il l’était malgré tout dans le second. En effet, le Conseil d’État a tout d’abord fait valoir que « _les dispositions de l’article 230-8 du code de procédure pénale […] ne prévoyant de règles particulières relatives au maintien ou à l’effacement des données du traitement des antécédents judiciaires qu’en cas de décisions de relaxe, d’acquittement, de non-lieu ou de classement sans suite, le législateur doit être regardé comme n’ayant entendu ouvrir la possibilité d’effacement que dans les cas où les poursuites pénales sont, pour quelque motif que ce soit, demeurées sans suite. Hors cette hypothèse, les données ne peuvent être effacées qu’à l’issue de la durée de conservation fixée par voie réglementaire et le procureur de la République ne peut alors que refuser une demande d’effacement avant ce terme_ ». Puis, s’attachant au second cas, il a estimé que « Lorsque les faits à l’origine de l’enregistrement des données dont l’effacement est demandé ont fait l’objet d’un classement sans suite pour un autre motif que l’insuffisance de charges, les données sont assorties d’une mention et les dispositions précitées de l’article 230-8 du code de procédure pénale, si elles ne le prévoient pas expressément, ne font pas obstacle à ce que le procureur de la République ou le magistrat référent décide d’accueillir une demande d’effacement ».

Enfin, en application de l’article 230-10 du CPP : « _Les personnels spécialement habilités des services de la police et de la gendarmerie nationales désignés à cet effet ainsi que les personnels spécialement habilités de l’État investis par la loi d’attributions de police judiciaire, notamment les agents des douanes, peuvent accéder aux informations, y compris nominatives, figurant dans les traitements de données personnelles prévus par la présente section_ », soit les fichiers d’antécédents judiciaires. Cet accès est également ouvert aux magistrats du parquet et aux magistrats instructeurs.

 **1.2. Application de l’article 230-8 dans le fichier « traitement des antécédents judiciaires »**

Avant la loi du 14 mars 2011, des fichiers d’antécédents judiciaires étaient déjà utilisés par les services de police et de gendarmerie, mais leur existence était restée officieuse jusqu’à la fin des années 1990. L’officialisation intervint d’abord avec la création du STIC pour la police nationale et du JUDEX pour la gendarmerie nationale, qui donnèrent une assise réglementaire à ces fichiers de police, puis avec la loi du 18 mars 2003 précitée qui les dota d’une base légale.

À la suite de la LOPPSI, le STIC et le JUDEX ont été abrogés par le décret n° 2012-652 du 4 mai 2012 pour laisser place à un fichier commun aux services de police et de gendarmerie, dénommé « traitement des antécédents judiciaires » (TAJ), entré en vigueur le 31 décembre 2013.

Ce nouveau fichier, réglementé par les articles R. 40-23 et suivants du CPP, s’inscrit dans le cadre défini par les articles 230-6 à 230-9 du CPP. Ce fichier met ainsi en œuvre les finalités mentionnées à l’article 230-6 du CPP.

Le TAJ n’est toutefois pas seulement conçu comme un outil d’aide aux enquêtes judiciaires puisqu’il peut également être consulté dans le cadre des enquêtes administratives menées en vue de l’acquisition de la nationalité française et de la délivrance de titres pour les étrangers, de la promotion dans les ordres nationaux et de l’accès à certains emplois, notamment ceux participant à l’exercice des missions de souveraineté de l’État ou relevant du domaine de la sécurité ou de la défense.

Les informations susceptibles d’être collectées ainsi que leur durée de conservation varient en fonction des catégories d’individus en cause. Si l’on s’en tient aux seules personnes physiques mises en cause dans une affaire pénale, peuvent figurer au sein du TAJ : l’identité (nom – y compris le nom marital ou le nom d’emprunt officiel –, prénoms, surnom, sexe, date et lieu de naissance), la situation familiale, la filiation, la nationalité, les adresses postales, la profession, l’état de la personne, le signalement et toute photographie utile (article R. 40-26 CPP).

En mutualisant les anciens fichiers STIC et JUDEX, le TAJ a permis aux services de police et de gendarmerie de se doter d’une banque de données considérable puisqu’en 2015, la CNIL estimait à 9,5 millions le nombre de personnes enregistrées en qualité de « mises en cause ».

Les données concernant les individus majeurs mis en cause dans une affaire pénale sont conservées dans le TAJ pendant vingt ans, sous réserve des infractions pour lesquelles la durée est abaissée à cinq ans (tel est par exemple le cas du délit d’usage de stupéfiants) ou augmentée à quarante ans. La durée de conservation des données concernant les mineurs est de cinq ans, sous réserve des infractions pour lesquelles elle peut atteindre dix, voire vingt ans

Les victimes d’infractions ne peuvent voir leurs données conservées pendant plus de quinze ans. Quant aux données concernant les personnes faisant l’objet d’une enquête ou d’une instruction pour recherche des causes de la mort, de blessures graves ou d’une disparition, elles doivent être effacées dès lors que l’enquête a permis de retrouver la personne disparue ou d’écarter toute suspicion de crime ou délit.

L’accès aux données contenues dans le TAJ pour les besoins des enquêtes judiciaires est ouvert en totalité aux acteurs investis par la loi d’attributions de police judiciaire. Dans le cadre d’enquêtes administratives, il est réservé à des personnels investis de missions de police administrative et ne permet pas l’accès aux données à caractère personnel se rapportant à des procédures judiciaires où sont intervenues des mesures ou décisions de classement sans suite, de non-lieu, de relaxe ou d’acquittement devenues définitives, ainsi que des données relatives aux victimes.

 ** _1.3. CADRE CONSTITUTIONNEL_**

Le cadre légal résultant de la LOPPSI a été jugé dans son ensemble conforme à la Constitution par le Conseil constitutionnel dans sa décision n° 2011-625 DC.

Le Conseil a notamment considéré que la différence de régime de conservation des données collectées à l’occasion d’une procédure ayant donné lieu à un classement sans suite, selon qu’un tel classement était motivé par une insuffisance de charges ou un autre motif, était justifiée par l’absence d’intérêt de conserver, dans le premier cas, de telles données dans le fichier.

Le Conseil s’est par ailleurs régulièrement prononcé sur des dispositions relatives à des traitements de données à caractère personnel, accessibles aux seules autorités administratives ou à des professionnels intéressés, notamment au regard du droit au respect de la vie privée qui découle de l’article 2 de la Déclaration des droits de l’homme et du citoyen de 1789 :.

Dans sa décision n° 2012-652 DC du 22 mars 2012, il a précisé ses exigences en matière de contrôle de fichiers en affirmant que « la collecte, l’enregistrement, la conservation, la consultation et la communication de données à caractère personnel doivent être justifiés par un motif d’intérêt général et mis en œuvre de manière adéquate et proportionnée à cet objectif ». Il est ainsi passé, en matière de traitement de données, d’un contrôle limité à l’absence de disproportion manifeste à un contrôle de proportionnalité plus poussé.

Dans l’exercice de ce contrôle de proportionnalité, le Conseil constitutionnel tient notamment compte du nombre de personnes susceptibles de relever du fichier informatique en cause, de la sensibilité particulière des données personnelles recueillies, des garanties techniques ou juridiques prévues par le législateur et des finalités d’utilisation ou de consultation du fichier. C’est ce qui l’a conduit à censurer les dispositions instaurant un fichier d’identité biométrique portant sur la quasi-totalité de la population française, au motif que « compte tenu de son objet, ce traitement de données à caractère personnel est destiné à recueillir les données relatives à la quasi-totalité de la population de nationalité française ; que les données biométriques enregistrées dans ce fichier, notamment les empreintes digitales, étant par elles-mêmes susceptibles d’être rapprochées de traces physiques laissées involontairement par la personne ou collectées à son insu, sont particulièrement sensibles ; que les caractéristiques techniques de ce fichier définies par les dispositions contestées permettent son interrogation à d’autres fins que la vérification de l’identité d’une personne ; que les dispositions de la loi déférée autorisent la consultation ou l’interrogation de ce fichier non seulement aux fins de délivrance ou de renouvellement des titres d’identité et de voyage et de vérification de l’identité du possesseur d’un tel titre, mais également à d’autres fins de police administrative ou judiciaire » (Décision n° 2012-652 DC).

Avant cette décision du 22 mars 2012, par laquelle il a précisé l’office de son contrôle en matière de traitements de données personnelles, le Conseil constitutionnel s’est prononcé à plusieurs reprises sur des fichiers de traitements d’antécédents judiciaires. Il a d’abord admis que les dispositions portant sur les traitements automatisés de données nominatives mis en œuvre par les services de la police nationale et de la gendarmerie nationale dans le cadre de leurs missions prévoient un ensemble de garanties « de nature à assurer, entre le respect de la vie privée et la sauvegarde de l’ordre public, une conciliation qui n’est pas manifestement déséquilibrée » (Décision n° 2003-467 DC du 13 mars 2003 précitée, cons. 21 à 27). Le Conseil a formulé comme seule réserve d’interprétation fondée sur le droit au respect de la vie privée la nécessité de garantir l’application de la loi du 6 janvier 1978 aux traitements en cause. Sans faire à proprement parler une réserve d’interprétation, le Conseil constitutionnel a par ailleurs relevé qu’il appartiendra à l’autorité judiciaire saisie d’une demande d’effacement « d’apprécier dans chaque cas, compte tenu des motifs de la décision prise, si les nécessités de l’ordre public justifient ou non le maintien des données en cause ». S’agissant de l’utilisation de ces fichiers à des fins administratives, il a par ailleurs jugé « qu’aucune norme constitutionnelle ne s’oppose par principe à l’utilisation à des fins administratives de données nominatives recueillies dans le cadre d’activités de police judiciaire ; que, toutefois, cette utilisation méconnaîtrait les exigences résultant des articles 2, 4, 9 et 16 de la Déclaration de 1789 si, par son caractère excessif, elle portait atteinte aux droits ou aux intérêts légitimes des personnes concernées ».

Dans sa décision précitée n° 2011-625 DC, le Conseil constitutionnel a ensuite estimé, à propos des fichiers d’antécédents judiciaires, que « les modifications apportées aux dispositions de l’article 21 de la loi du 18 mars 2003 à l’occasion de leur introduction aux articles 230-6 à 230-11 du code de procédure pénale renforcent le contrôle de l’autorité judiciaire sur les données enregistrées dans les fichiers d’antécédents ; que l’article 230-8 du code de procédure pénale prévoit que le procureur de la République ou le magistrat chargé de suivre la mise en œuvre et la mise à jour des traitements se prononce, dans un délai d’un mois, sur les suites qu’il convient de donner aux demandes d’effacement ou de rectification ; que cet article prévoit également que toutes les données relatives à des personnes mises en cause et maintenues dans les fichiers d’antécédents en dépit d’une décision de relaxe, d’acquittement, de non-lieu ou de classement sans suite, quel qu’en soit le motif, font l’objet d’une mention qui interdit l’accès à ces données dans le cadre d’une enquête administrative ; que la différence de régime de conservation des données, qui résulte de la faculté donnée au procureur de la République d’ordonner l’effacement lorsque le classement sans suite de la procédure est motivé par une insuffisance de charges, est fondée sur l’absence d’intérêt de conserver, dans ce cas, de telles données dans le fichier ; - […] qu’il résulte de ce qui précède que, sous les mêmes réserves que celles [formulées dans la décision n° 2003-467 DC], les dispositions des articles 230-6 à 230-11 du code de procédure pénale, qui ne sont ni obscures ni ambiguës, sont conformes à la Constitution ».

Dans cette même décision, au sujet des logiciels de rapprochement judiciaire, le Conseil constitutionnel a censuré la disposition permettant aux enquêteurs de prolonger, au-delà de trois ans, la conservation des données personnelles révélées par l’exploitation des enquêtes et des investigations réalisées au moyen de ces logiciels.

Le Conseil constitutionnel a par ailleurs eu l’occasion de se prononcer plus précisément sur des dispositions limitant les demandes d’effacement anticipé concernant les personnes mises en cause, à propos du FNAEG. Il a d’une part jugé que les garanties prévues par le législateur étaient de nature à assurer, entre le respect de la vie privée et la sauvegarde de l’ordre public, une conciliation qui n’est pas manifestement déséquilibrée. Parmi les garanties relevées, il a notamment souligné « que l’inscription au fichier concerne, outre les personnes condamnées pour ces infractions, celles à l’encontre desquelles il existe des indices graves ou concordants rendant vraisemblable qu’elles les aient commises ; que, pour ces dernières, les empreintes prélevées dans le cadre d’une enquête ou d’une information judiciaires sont conservées dans le fichier sur décision d’un officier de police judiciaire agissant soit d’office, soit à la demande du procureur de la République ou du juge d’instruction ; qu’une procédure d’effacement est, dans ce cas, prévue par le législateur, lorsque la conservation des empreintes n’apparaît plus nécessaire compte tenu de la finalité du fichier ; que le refus du procureur de la République de procéder à cet effacement est susceptible de recours devant le juge des libertés et de la détention dont la décision peut être contestée devant le président de la chambre de l’instruction ».

Il a, d’autre part, considéré que le renvoi au décret pour fixer la durée de conservation des empreintes génétiques dans le FNAEG n’était pas contraire au principe de la présomption d’innocence, sous réserve notamment que le pouvoir réglementaire veille à « proportionner la durée de conservation de ces données personnelles, compte tenu de l’objet du fichier, à la nature ou à la gravité des infractions concernées ».

 **1.4 CADRE CONVENTIONNEL**

Dans l’affaire Brunet contre France du 18 septembre 2014, la CEDH a jugé que la conservation pendant vingt ans, dans le STIC, des données d’un individu ayant bénéficié d’un classement sans suite pour un motif autre que l’insuffisance de charges était contraire au droit au respect de la vie privée garanti par l’article 8 de la Convention européenne de sauvegarde des droits de l’homme et des libertés fondamentales (CESDHLF), en raison de l’impossibilité pour l’autorité judiciaire d’ordonner l’effacement des données personnelles dans une telle hypothèse.

En réponse à ces décisions, l’article 68 de la loi n° 2016-731 du 3 juin 2016 a modifié l’article 230-8 du CPP afin, selon l’amendement présenté par le Gouvernement à cet effet, de « mettre le droit interne en conformité avec la jurisprudence de la CEDH ».

Désormais, le premier alinéa de l’article 230-8 prévoit que l’ensemble des décisions de classement sans suite peuvent donner lieu à un effacement anticipé des données figurant dans un traitement d’antécédents judiciaires, qu’elles soient ou non fondées sur une insuffisance de charges. Si le principe reste celui de la conservation des données assortie d’une mention de la décision dans le fichier, le procureur de la République ou le magistrat référent peut donc apprécier, d’office ou sur la demande de l’intéressé, l’opportunité de les maintenir quel que soit le motif du classement sans suite.

*Le même alinéa précise ensuite que les décisions du procureur de la République ou du magistrat référent tendant au maintien ou à l’effacement des données sont prises pour des raisons liées à la finalité du fichier « au regard de la nature ou des circonstances de commission de l’infraction ou de la personnalité de l’intéressé ».

L’article 230-8 ne limite ainsi plus l’exigence de motivation aux seules décisions de maintien des données en cas de relaxe ou d’acquittement définitifs.

Enfin, le troisième alinéa introduit un recours contre les décisions du procureur de la République devant le président de la chambre de l’instruction. Cette faculté est également ouverte contre les décisions du magistrat référent par le dernier alinéa de l’article 230-9, qui prévoit alors qu’elle s’exerce devant le président de la chambre de l’instruction de la cour d’appel de Paris.

 ** _2\. OBJECTIFS ET NECESSITE DE LEGIFERER_**

 ** _2.1 OBJECTIFS POURSUIVIS_**

Le projet de loi réécrit le premier alinéa de l’article 230-8 afin de prévoir que les demandes d’effacement faites auprès du procureur de la République pourront intervenir sans délai à la suite d’une condamnation assortie d’une dispense de peine ou d’une dispense d’inscription au casier judiciaire, comme c’est déjà le cas pour les décisions de relaxe, d'acquittement, de non-lieu ou de classement sans suite, et qu’elles pourront intervenir dans les autres cas lorsqu’aucune mention ne figurera dans le bulletin n° du casier judiciaire.

Ainsi, dès qu’une condamnation sera réhabilitée ou non avenue, l’article 775 du code de procédure pénale prévoyant son exclusion du bulletin n° 2, la personne pourra demander l’effacement du TAJ des données la concernant, ou l’ajout à ces données d’une mention interdisant que le TAJ soit utilisé à des fins de police administrative.

 ** _2.2 NÉCESSITÉ DE LEGIFERER_**

Dans sa décisions n° 2017-670 QPC du 27 octobre 2017 du Conseil constitutionnel a déclaré contraire à la Constitution le premier alinéa de l’article 230-8 du code de procédure pénale, au motif que ces dispositions portaient une atteinte disproportionnée au droit au respect de la vie privée parce qu’elles privaient les personnes mises en cause dans une procédure pénale, autres que celles ayant fait l’objet d’une décision d’acquittement, de relaxe, de non-lieu ou de classement sans suite, de toute possibilité d’obtenir l’effacement de leurs données personnelles inscrites dans ce fichier.

Après avoir déterminé la portée exacte des dispositions contestées, et avoir considéré qu’il résultait d’une jurisprudence constante qu’aucune personne mise en cause autre que celles ayant fait l’objet d’une décision d’acquittement, de relaxe, de non-lieu ou de classement sans suite ne peut obtenir, sur le fondement des dispositions contestées, l’effacement des données qui la concernent » (paragr. 9), le Conseil constitutionnel a examiné leur conformité au droit au respect de la vie privée.

Comme il l’a fait dans de nombreuses décisions confrontant des dispositions relatives à un fichier aux exigences constitutionnelles en matière de vie privée, il a recherché les objectifs poursuivis par le législateur lors de la création de ce fichier. Sur ce point, il a jugé : « En autorisant la création de traitements de données à caractère personnel recensant des antécédents judiciaires et l’accès à ces traitements par des autorités investies par la loi d’attributions de police judiciaire et par certains personnels investis de missions de police administrative, le législateur a entendu leur confier un outil d’aide à l’enquête judiciaire et à certaines enquêtes administratives. Il a ainsi poursuivi les objectifs de valeur constitutionnelle de recherche des auteurs d’infractions et de prévention des atteintes à l’ordre public » (paragr. 9).

Le Conseil constitutionnel a ensuite apprécié le caractère proportionné à ces objectifs de l’atteinte à la vie privée résultant de l’inscription dans un fichier d’antécédents judiciaires.

Pour ce faire, il a d’abord examiné la nature des données pouvant être enregistrées dans un tel fichier, ainsi que le nombre de personnes susceptibles d’être inscrites dans le fichier. En effet, ces éléments entrent en ligne de compte dans la détermination de l’intensité de l’atteinte susceptible d’être portée à la vie privée.

Le Conseil a ainsi tout d’abord relevé que, « en prévoyant que les fichiers d’antécédents judiciaires peuvent contenir les informations recueillies au cours d’une enquête ou d’une instruction concernant une personne à l’encontre de laquelle il existe des indices graves ou concordants rendant vraisemblable qu’elle ait pu participer à la commission de certaines infractions, le législateur a permis que figurent dans ce fichier des données particulièrement sensibles » (paragr. 10).

À titre d’exemple, les dispositions réglementaires encadrant le TAJ prévoient ainsi que « peuvent être enregistrés, les éléments d’état civil, la profession ou la situation familiale de la personne, une photographie comportant des caractéristiques techniques permettant de recourir à un dispositif de reconnaissance faciale » (même paragr.).

Le Conseil constitutionnel a ensuite rappelé qu’un nombre important de personnes mises en cause dans une procédure pénale, et non pas seulement celles mises en cause pour les faits les plus graves, peuvent être inscrites dans un fichier d’antécédents judiciaires (paragr. 11).

Puis le Conseil constitutionnel a relevé l’absence, dans la loi, de durée maximum de conservation des informations enregistrées dans un fichier d’antécédents judiciaires. Le législateur a en effet renvoyé au pouvoir réglementaire la détermination de cette durée. Ainsi, l’article R. 40-27 du CPP prévoit qu’elles sont conservées pendant une durée comprise entre cinq ans et quarante ans, selon l’âge de l’individu et la nature de l’infraction (paragr. 12).

Enfin, dans l’appréciation de l’atteinte à la vie privée, le Conseil constitutionnel a pris en compte les cas dans lesquels ces fichiers peuvent être consultés : « ces informations peuvent être consultées non seulement aux fins de constatation des infractions à la loi pénale, de rassemblement des preuves de ces infractions et de recherche de leurs auteurs, mais également à d’autres fins de police administrative » (paragr. 13).

Après avoir mis ces éléments en balance, le Conseil constitutionnel en a conclu qu’« en privant les personnes mises en cause dans une procédure pénale, autres que celles ayant fait l’objet d’une décision d’acquittement, de relaxe, de non-lieu ou de classement sans suite, de toute possibilité d’obtenir l’effacement de leurs données personnelles inscrites dans le fichier des antécédents judiciaires, les dispositions contestées portent une atteinte disproportionnée au droit au respect de la vie privée » (paragr. 14).

Le Conseil constitutionnel a donc jugé que les personnes mises en cause dans une procédure pénale inscrites dans un fichier d’antécédents judicaires doivent pouvoir solliciter, et éventuellement obtenir, l’effacement de leurs données avant la fin de la durée normale de conservation. Il n’a pas pour autant reconnu « un droit à l’effacement », puisqu’il reviendra à l’autorité judiciaire d’apprécier le bien-fondé de cette demande, selon des critères définis par le législateur

Le Conseil a reporté les effets de sa décision au 1er mai 2018.

Cette décision impose de réécrire le premier alinéa de l’article 230-8 en respectant les exigences constitutionnelles, donc en prévoyant des possibilités d’effacement anticipé.

 ** _3\. OPTIONS_**

 **3.1. DELAIS DANS LEQUEL UNE DEMANDE D’EFFACEMENT POURRA ETRE FORMEE**

 **3.1.1. Option 1 (écartée) : Ne fixer aucun délai**

Afin d’éviter une charge de travail excessive pour les parquets, il est proposé de ne pas permettre une possibilité immédiate de demande d’effacement (que n’exige du reste pas la décision QPC), sauf dans les cas déjà prévus de relaxe, acquittement, non-lieu et CSS, auxquels seraient ajoutés les cas de condamnations avec dispense de peine ou dispense d’inscription au casier.

 **3.1.2. Option 2 retenue : Prévoir un délai par référence à l’existence de mention au B2 du casier judiciaire**

Dans cas autres que ceux de relaxe, acquittement, non-lieu, classement sans suite, et condamnations avec dispense de peine ou dispense d’inscription au casier., il est proposé de prévoir que la demande ne sera recevable que s’il n’existe plus de mention au B2 de la personne, ce qui est principalement le cas lorsque la condamnation est réhabilitée ou non avenue, et qu’elle disparaît donc du B2 en application des 4° et 5° de l’article 775 du CPP.

Cette solution paraît préférable à celle consistant à prévoir dans la loi un délai ou plusieurs délais spécifiques (alors que les délais de conservation dans le TAJ relèvent du décret). La référence au B2 est par ailleurs cohérente puisque le TAJ peut être utilisé à des fins administratives, comme le B2 (et qu’il est parfois peu cohérent qu’une personne puisse se voir refuser un emploi en raison d’une mention au TAJ alors même que son B2 est néant).

Il résulte du renvoi aux règles du B2 pour permettre les demandes d’effacement du TAJ que les délais de recevabilité de ces demandes seront variables selon la nature des faits et de la peine prononcée (dans les cas les moins graves, notamment en cas de condamnation à une peine d’amende, ils seront de 3 ans après le paiement de celle-ci, conformément au 1° de l’article 133-13 du CP qui prévoit la réhabilitation de droit dans un tel délai ; dans les cas les plus graves de peines criminelles, ils seront au moins de 5 ans ou de 10 ans après l’exécution de la peine, selon qu’il y ou non récidive, en application des articles 786 et 787 du CPP, si la personne a obtenu une réhabilitation judiciaire ; il peut être précisé que ces délais ne s’appliquent qu’à la condition que la personne n’ait pas été de nouveau condamnée).

Le tableau figurant dans la partie impact précise les conséquences pratiques du renvoi aux règles du B2 du casier judiciaire.

 **3.2. NATURE DE LA DECISION POUVANT ETRE PRISE PAR LE PROCUREUR**

 **3.2.1. Option 1 (écartée) : ne permettre au procureur que de décider d’effacer les données**

Il aurait été incohérent de donner un seul pouvoir d’effacement au procureur, sans lui permettre de maintenir les données à des fins de police judiciaire, tout en les écartant des fins administratives..

**3.2.2. Option 2 (retenue) : permettre au procureur d’ordonner l’effacement ou d’ordonner une mention**

Il a logiquement été prévu que le procureur pourra décider de ne pas effacer les données du TAJ, mais d’ajouter une mention interdisant son utilisation à des fins administratives.

 ** _4\. ANALYSE DES IMPACTS DE LA DISPOSITION ENVISAGÉE_**

 ** _4.1. IMPACTS JURIDIQUES_**

La présente disposition conduit à réécrire l’article 230-8 du code de procédure pénale.

 ** _4.2. IMPACTS SUR LES SERVICES JUDICIAIRES_**

Les parquets pourront être plus fréquemment saisis de demande d’effacement concernant le TAJ

 ** _4.3. IMPACTS SUR LES PARTICULIERS_**

Les droits des personnes inscrites dans le TAJ seront augmentés, puisqu’elles pourront demander l’effacement des données avant les délais de conservations.

<table>
<tr>
<td>

**REGLES D’INSCRIPTION ET D’EFFACEMENT DU BULLETIN N°2**

**et de possibilité de déposer une**

**REQUETE EN EFFACEMENT DU TAJ**

</td></tr>
<tr>
<td>

**Condamnés mineurs**

</td>
<td>

Les condamnations pour mineurs ne sont jamais inscrites au bulletin n°2 du casier judiciaire (art 775 1° CPP).

En conséquence, les condamnés mineurs pourront solliciter immédiatement un effacement du TAJ.

</td></tr>
<tr>
<td>

**Condamnés majeurs**

</td>
<td>

**_En matière contraventionnelle :_**

Les condamnations pour des contraventions de police ne sont jamais inscrites au bulletin n°2 du casier judiciaire (art 775 3° CPP).

En conséquence, les condamnés majeurs pourront solliciter immédiatement un effacement de la condamnation à une contravention de 5ème classe du TAJ

</td></tr>
<tr>
<td>

**_En matière correctionnelle :_**

Toutes les condamnations correctionnelles, sauf celles prévues à l’article 706-47 CPP (nature sexuelle) peuvent faire **au moment du prononcé de la décision, puis à tout moment sur requête de la personne condamnée l’objet d’une décision de non inscription ou d’exclusion du bulletin N°2** par la juridiction de jugement (art 775 2° et 775-1 CPP).

Si la décision est exclue ou effacée par la juridiction, la personne condamnée pourra logiquement solliciter son effacement du TAJ.

Hors de cette hypothèse, les condamnations correctionnelles sont effacées du bulletin n°2 dans les délais suivants :

_Déclaration de culpabilité assortie d’une dispense de peine ou d’un ajournement du prononcé de celle-ci :_

Ces condamnations ne sont pas inscrites au bulletin n°2 (art 775 12° CPP) et pourront donc faire l’objet d’une requête en effacement du TAJ **immédiate.**

_Amende :_

Les amendes sont effacées du bulletin n°2 à compter de leur réhabilitation légale (art 775 5° CPP), soit dans un délai de **trois ans** à compter du paiement (art 133-13 CP).

_Peines alternatives :_

Les jours-amende sont effacés du bulletin n°2 dans un délai de **trois ans** (art 775 11° CPP) à compter du caractère définitif de la condamnation.

Les autres peines alternatives prononcées sans sursis en application des articles 131-5 à 131-11 du code pénal sont effacés du bulletin n°2 dans un délai de **cinq ans** (art 775 11° CPP) à compter du caractère définitif de la condamnation.

_Peines d’emprisonnement assorties du sursis :_

Les condamnations assorties du sursis, avec ou sans mise à l’épreuve, sont effacées du bulletin n°2 lorsqu’elles sont considérées comme **non avenues** , (sauf suivi socio-judiciaire, interdiction d’exercer une activité en lien avec les mineurs, interdictions incapacités et déchéances prononcées à titre définitif, peine complémentaire d’inéligibilité qui persistent jusqu’en fin de mesure ou d’interdiction).

_Peines d’emprisonnement ferme réhabilitables :_

Les peines correctionnelles sont effacées du bulletin n°2 du casier judiciaire à compter de leur réhabilitation légale (art 775 5° CPP), laquelle intervient dans les délais suivants :

\- Emprisonnement inférieur ou égal à un an **: 5 ans**

\- Emprisonnement inférieur ou égal à 10 ans : **10 ans**

\- Emprisonnement résultant de peines multiples n’excédant pas 5 ans : **10 ans**

Ces délais sont doublés si la personne est en état de récidive légale.

Le délai court à compter de l'exécution ou de la prescription de la peine.

_Peines d’emprisonnement ferme non réhabilitables :_

Les peines d’emprisonnement correctionnelles suivantes ne sont pas susceptibles de réhabilitation de plein droit :

\- Emprisonnement supérieur à 10 ans

\- Emprisonnement résultant de peines multiples supérieur à 5 ans

Lorsqu’elles n'ont pas été suivies d'une nouvelle condamnation à une peine criminelle ou correctionnelles, elles sont effacées dans un délai de **40 ans** (article 769 alinéa 3 CPP), sauf en cas de réhabilitation judiciaire, laquelle pouvant être sollicitée par sous certaines conditions dans un délai de 3 ans ou 6 en cas de récidive légale (art 786 et suivants CPP).

</td></tr>
<tr>
<td>

**_En matière criminelle :_**

Les peines criminelles ne peuvent pas être réhabilitées de plein droit.

Ainsi, lorsqu’elles n'ont pas été suivies d'une nouvelle condamnation à une peine criminelle ou correctionnelles, elles sont effacées dans un délai de **40 ans** (article 769 alinéa 3 CPP), sauf en cas de réhabilitation judiciaire, laquelle pouvant être sollicitée par sous certaines conditions dans un délai de 5 ans ou 10 ans en cas de récidive légale (art 786 et suivants CPP).

</td></tr></table>

**_5\. CONSULTATION ET MODALITÉS D’APPLICATION_**

La Commission nationale de l’informatique et des libertés a été consultée.

La réforme s’appliquera dès le lendemain de la publication de la loi, en principe avant le 1er mai 2018, date à laquelle le Conseil constitutionnel a reporté l’application de sa décision de censure.

Aucun texte réglementaire ne doit être pris en application de la nouvelle rédaction de l’article 230-8 du code de procédure pénale.

Ces dispositions seront applicables sur l’ensemble du territoire, y compris dans les collectivités d’outre-mer. L’article « compteur » du code de procédure pénal est mis à jour en conséquence.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/etude-impact-490/titre-v/article-22" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/titre-v/articles-24" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

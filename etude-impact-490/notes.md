<!-- TITLE: Notes -->

1 « La protection des données personnelles », étude n° 1700780, CSA research, septembre 2017.

2 COM(2012) 11 final (disponible en ligne : [_http://ec.europa.eu/justice/data-protection/document/review2012/com_2012_11_fr.pdf_](http://ec.europa.eu/justice/data-protection/document/review2012/com_2012_11_fr.pdf) )

3 Rapport d’information en conclusion des travaux d’une mission d’information sur les incidences des nouvelles normes européennes en matière de protection des données personnelles sur la législation française, présenté par Mme Anne-Yvonne Le Dain et M. Philippe Gosselin ? 22 février 2017.

4 Article 258 du Traité sur le fonctionnement de l’Union européenne : « _Si la Commission estime qu'un État membre a manqué à une des obligations qui lui incombent en vertu des traités, elle émet un avis motivé à ce sujet, après avoir mis cet État en mesure de présenter ses observations./Si l'État en cause ne se conforme pas à cet avis dans le délai déterminé par la Commission, celle-ci peut saisir la Cour de justice de l'Union européenne._ »

5 Article 288 du Traité sur le fonctionnement de l’Union européenne : « _(…) Le règlement a une portée générale. Il est obligatoire dans tous ses éléments et il est directement applicable dans tout État membre. (…)_ ».

6 Article 8 du règlement.

7 Article 11-3-c) : _« Elle délivre un label à des produits ou à des procédures tendant à la protection des personnes à l'égard du traitement des données à caractère personnel, après qu'elle les a reconnus conformes aux dispositions de la présente loi dans le cadre de l'instruction préalable à la délivrance du label par la commission ; la commission peut également déterminer, de sa propre initiative, les produits et procédures susceptibles de bénéficier d'un label . Le président peut, lorsque la complexité du produit ou de la procédure le justifie, recourir à toute personne indépendante qualifiée pour procéder à leur évaluation. Le coût de cette évaluation est pris en charge par l'entreprise qui demande le label ; elle retire le label lorsqu'elle constate, par tout moyen, que les conditions qui ont permis sa délivrance ne sont plus satisfaites ; »_

8 Avant dernier alinéa de l’article 11 de la loi n°78-17.

9 _Conseil d’Etat, Etude annuelle 2013, « Le droit souple »_ , La documentation française, p.5.

10 « Le droit souple vu de la CNIL : un droit relais nécessaire à la crédibilité de la régulation des données personnelles », dans _Conseil d’Etat, Etude annuelle 2013, « Le droit souple »_ , la documentation française, p.5

11 Ibid, pp. 241 et 242.

12 Section 14 “(1) In addition to the tasks listed in Regulation (EU) 2016/679, the Federal Commissioner shall have the following tasks: \ 1. to monitor and enforce the application of this Act and other data protection legislation, including legislation adopted to implement Directive (EU) 2016/680;\ 2. to promote public awareness and understanding of the risks, rules, safeguards and rights in relation to the processing of personal data, paying special attention to measures specifically for children; \ 3. to advise the German Bundestag, the Bundesrat, the Federal Government, and other institutions and bodies on legislative and administrative measures relating to the protection of natural persons’ rights and freedoms with regard to the processing of personal data; \ 4. to promote the awareness of controllers and processors of their obligations under this Act and other data protection legislation, including legislation adopted to implement Directive (EU) 2016/680; \ 5. upon request, to provide information to any data subject concerning the exercise of their rights under this Act and other data protection legislation, including legislation adopted to implement Directive (EU) 2016/680, and if appropriate, to cooperate with the supervisory authorities in other Member States to that end; \ 6. to handle complaints lodged by a data subject, or by a body, organization or association in accordance with Article 55 of Directive (EU) 2016/680, and investigate, to the extent appropriate, the subject matter of the complaint and inform the complainant of the progress and the outcome of the investigation within a reasonable period, in particular if further investigation or coordination with another supervisory authority is necessary; \ 7. to cooperate with, including by sharing information, and provide mutual assistance to other supervisory authorities, to ensure the consistency of application and enforcement of this Act and other data protection legislation, including legislation adopted to implement Directive (EU) 2016/680; \ 8. to conduct investigations on the application of this Act and other data protection legislation, including legislation adopted to implement Directive (EU) 2016/680, also on the basis of information received from another supervisory authority or other public authority; \ 9. to monitor relevant developments, insofar as they have an impact on the protection of personal data, in particular the development of information and communication technologies and commercial practices; \10. to provide advice on the processing operations referred to in Section 69; \ and 11. to contribute to the activities of the European Data Protection Board. \ Within the scope of Directive (EU) 2016/680, the Federal Commissioner shall also perform the task pursuant to Section 60. \ (…)”

13 https://www.lachambre.be/flwb/pdf/54/2648/54K2648007.pdf

14 http://www.chd.lu/wps/PA_RoleDesAffaires/FTSByteServingServletImpl?path=/export/exped/sexpdata/Mag/0004/195/9951.pdf

15 [_http://www.justice.ie/en/JELR/General_Scheme_of_Data_Protection_Bill_(May_2017).pdf/Files/Genera l_Scheme_of_Data_Protection_Bill_(May_2017).pdf_](http://www.justice.ie/en/JELR/General_Scheme_of_Data_Protection_Bill_\(May_2017\).pdf/Files/General_Scheme_of_Data_Protection_Bill_\(May_2017\).pdf) (p.12)

16 Article 58.3. « _Chaque autorité de contrôle dispose de tous les pouvoirs d'autorisation et de tous les pouvoirs consultatifs suivants : […] : f) délivrer des certifications et approuver des critères de certification conformément à l'article 42, paragraphe 5_ » ;

17 Article 43.1 : « […] _Les États membres veillent à ce que ces organismes de certification soient agréés par une des entités suivantes ou les deux : a) l’autorité de contrôle qui est compétente en vertu de l'article 55 ou 56; \ b) l’organisme national d'accréditation désigné conformément au règlement (CE) no_ _765/2008 du Parlement européen et du Conseil, conformément à la norme EN-ISO/IEC 17065/2012 et aux exigences supplémentaires établies par l'autorité de contrôle qui est compétente en vertu de l'article 55 ou 56_ ».

18 Le COFRAC, créé en 1994 sous le régime d’une association de droit privé à but non lucratif, a été désigné comme unique instance nationale d’accréditation par le n° 2008-1401 du 19 décembre 2008 relatif à l'accréditation et à l'évaluation de conformité pris en application de l'article 137 de la loi n° 2008-776 du 4 août 2008 de modernisation de l'économie, habilité à délivrer des certificats d'accréditation aux organismes d'évaluation de la conformité, que cette accréditation soit obligatoire ou non.

19 Article 57.1.c : « _conseille, conformément au droit de l'État membre, le parlement national, le gouvernement et d'autres institutions et organismes au sujet des mesures législatives et administratives relatives à la protection des droits et libertés des personnes physiques à l'égard du traitement;_ »

20 Même rédaction que ci-dessus.

21 Le considérant 98 du règlement précise à cet égard que : « _Il y a lieu d'encourager les associations ou autres organismes représentant des catégories de responsables du traitement ou de sous-traitants à élaborer des codes de conduite, dans les limites du présent règlement, de manière à en faciliter la bonne application, compte tenu des spécificités des traitements effectués dans certains secteurs et des besoins spécifiques des micro, petites et moyennes entreprises. Ces codes de conduite pourraient, en particulier, définir les obligations qui incombent aux responsables du traitement et aux sous-traitants, compte tenu du risque que le traitement peut engendrer pour les droits et libertés des personnes physiques_ ».

22 L’article 35.5 du règlement prévoit que « _L'autorité de contrôle peut aussi établir et publier une liste des types d'opérations de traitement pour lesquelles aucune analyse d'impact relative à la protection des données n'est requise. L'autorité de contrôle communique cette liste au comité_ »

23 Tel n’est pas le cas en revanche d’une recommandation qui ne se borne pas à commenter les règles dont la CNIL doit assurer le respect mais qui ajoute à l'ordonnancement juridique (CE, sect., 30 octobre 2001, Association française des sociétés financières et a., n° 204909).

24 [_http://www.itgovernance.co.uk/iso27001-global-report-2016.aspx_](http://www.itgovernance.co.uk/iso27001-global-report-2016.aspx)

25 Article 11 2° f : « _Elle peut, par décision particulière, charger un ou plusieurs de ses membres ou le secrétaire général, dans les conditions prévues à l'article 44, de procéder ou de faire procéder par les agents de ses services à des vérifications portant sur tous traitements et, le cas échéant, d'obtenir des copies de tous documents ou supports d'information utiles à ses missions_ ».

26 Articles L. 253-2 et L. 253-3 du code de la sécurité intérieure.

27 CE, 6 novembre 2009 Société Inter confort, n° 304300 et Pro déco, n° 304301.

28 Rapport d’activité de la Commission nationale de l’informatique et des libertés, 2016.

29 Décision n° 2004-499 DC du 29 juillet 2004, cons. 17 et 18.

30 CE, 6 novembre 2009 Société Inter confort n° 304300.

31 « _Seul un médecin peut requérir la communication de données médicales individuelles incluses dans un traitement nécessaire aux fins de la médecine préventive, de la recherche médicale, des diagnostics médicaux, de l'administration de soins ou de traitements, ou à la gestion de service de santé, et qui est mis en œuvre par un membre d'une profession de santé_ ».

32 Voir par exemple récemment l’art. L. 1333-17-1, issu de l’ordonnance n° 2017-45 du 19 janvier 2017, permettant aux inspecteurs de la radioprotection d’accéder _« ils accèdent, à leur demande et dans des conditions préservant la confidentialité des données à l'égard des tiers, aux informations […] qui leur sont strictement nécessaires, sans que puisse leur être opposé le secret médical ou le secret en matière industrielle ou commerciale._ »

33 Article 19 de la loi n° 78-17 : « _[…] Ceux des agents qui peuvent être appelés à participer à la mise en œuvre des missions de vérification mentionnées à l'article 44 doivent y être habilités par la commission ; cette habilitation ne dispense pas de l'application des dispositions définissant les procédures autorisant l'accès aux secrets protégés par la loi._ »

34 Article L. 621-10-1 du code monétaire et financier : « _Lorsque les personnes et entités mentionnées au II de l'article L. 621-9 fournissent leurs services sur internet, les enquêteurs et les contrôleurs peuvent, pour accéder aux informations et éléments disponibles sur ces services, faire usage d'une identité d'emprunt sans en être pénalement responsables. / Un décret en Conseil d'Etat précise les conditions dans lesquelles les enquêteurs et les contrôleurs procèdent dans ces cas à leurs constatation_ s ».

35 Décision n° 80-116 DC du 17 juillet 1980, considérant 4.

36 Décision n° 91-294 DC du 25 juillet 1991, considérants 38 et 39 ;

37 Cour de cassation, civile, Chambre commerciale, 20 janvier 2015, 13-16.745 13-16.764 13-16.765 13-16.955, Publié au bulletin

38 La procédure de guichet unique est précisée au considérant 127 du règlement : « _Chaque autorité de contrôle qui ne fait pas office d'autorité de contrôle chef de file devrait être compétente pour traiter les cas de portée locale lorsque le responsable du traitement ou le sous-traitant est établi dans plusieurs États membres mais que l'objet du traitement spécifique ne se rapporte qu'à un traitement effectué dans un seul État membre et ne porte que sur des personnes concernées de ce seul État membre, par exemple lorsqu'il s'agit de traiter des données à caractère personnel relatives à des employés dans le contexte des relations de travail propre à un État membre. Dans ces cas, l'autorité de contrôle devrait informer sans tarder l'autorité de contrôle chef de file de la question. Après avoir été informée, l'autorité de contrôle chef de file devrait décider si elle traitera le cas en vertu de la disposition relative à la coopération entre l'autorité de contrôle chef de file et les autres autorités de contrôle concernées (ci-après dénommé “mécanisme de guichet unique“), ou si l'autorité de contrôle qui l'a informée devrait traiter le cas au niveau local. Lorsqu'elle décide si elle traitera le cas, l'autorité de contrôle chef de file devrait considérer s'il existe un établissement du responsable du traitement ou du sous-traitant dans l'État membre dont relève l'autorité de contrôle qui l'a informée, afin d'assurer l'exécution effective d'une décision à l'égard du responsable du traitement ou du sous-traitant. Lorsque l'autorité de contrôle chef de file décide de traiter le cas, l'autorité de contrôle qui l'a informée devrait avoir la possibilité de soumettre un projet de décision, dont l'autorité de contrôle chef de file devrait tenir le plus grand compte lorsqu'elle élabore son projet de décision dans le cadre de ce mécanisme de guichet unique._ »

39 CE 27 juillet 2012, AIS2, n° 340026.

40 Rapport d’activité de la Commission nationale de l’informatique et des libertés, 2016.

41 Décisions n° 2012-280 QPC du 12 octobre 2012, Société Groupe Canal Plus et autre (Autorité de la concurrence : organisation et pouvoir de sanction), cons. 16 ; n° 2013-359 QPC du 13 décembre 2013, cons. 3.

42 CE, 26 mars 2012, Sté Pages jaunes, n° 353193

43 CE, 12 mars 2014, société Foncia Groupe, n° 354629.

44 Voir le considérant 20 du règlement : « _Bien que le présent règlement s'applique, entre autres, aux activités des juridictions et autres autorités judiciaires, le droit de l'Union ou le droit des États membres pourrait préciser les opérations et procédures de traitement en ce qui concerne le traitement des données à caractère personnel par les juridictions et autres autorités judiciaires. La compétence des autorités de contrôle ne devrait pas s'étendre au traitement de données à caractère personnel effectué par les juridictions dans l'exercice de leur fonction juridictionnelle, afin de préserver_ _l'indépendance du pouvoir judiciaire dans l'accomplissement de ses missions judiciaires, y compris lorsqu'il prend des décisions._ »

45 L’ensemble des pouvoirs prévus par la directive, au 2 de l’article 47 se retrouve dans la liste prévue par le règlement.

46 CE, 19 février 2008, société PROFIL France, n° 311974.

47 [_http://www.lachambre.be/FLWB/PDF/54/2648/54K2648007.pdf_](http://www.lachambre.be/FLWB/PDF/54/2648/54K2648007.pdf)

48 Avant-projet de loi portant création de la Commission nationale pour la protection des données et la mise en œuvre du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données, portant modification de la loi du 25 mars 2015 fixant le régime des traitements et les conditions et modalités d'avancement des fonctionnaires de l'Etat et abrogeant la loi modifiée du 2 août 2002 relative à la protection des personnes à l'égard du traitement des données à caractère personnel. (MEDIAS IOa/2017)

49 [_https://www.bgbl.de/xaver/bgbl/start.xav?start=%2F%2F*%5B%40attr_id%3D%27bgb l117s2097.pdf%27%5D#__bgbl__%2F%2F*%5B%40attr_id%3D%27bgbl117s2097.pdf%27%5D__1510752756678_](https://www.bgbl.de/xaver/bgbl/start.xav?start=%2F%2F*%5B@attr_id%3D'bgbl117s2097.pdf'%5D#__bgbl__%2F%2F*%5B@attr_id%3D'bgbl117s2097.pdf'%5D__1510752756678)

50 [_https://services.parliament.uk/bills/2017-19/dataprotection.html_](https://services.parliament.uk/bills/2017-19/dataprotection.html)

51 [_http://servicios.mpr.es/seacyp/search_def_asp.aspx?crypt=xh%8A%8Aw%98%85d%A2%B0%8DNs%90%8C%8An%87%A2%7F%8B%99tt%8 4sm%A3%91_](http://servicios.mpr.es/seacyp/search_def_asp.aspx?crypt=xh%8A%8Aw%98%85d%A2%B0%8DNs%90%8C%8An%87%A2%7F%8B%99tt%84sm%A3%91)

52 Le considérant 129 du règlement précise à cet égard que : « […] _Toute mesure devrait notamment être appropriée, nécessaire et proportionnée en vue de garantir le respect du présent règlement, compte tenu des circonstances de l'espèce, respecter le droit de chacun à être entendu avant que soit prise toute mesure individuelle susceptible de lui porter atteinte et éviter les coûts superflus ainsi que les désagréments excessifs pour les personnes concernées._ ».

53 CE, 27 juillet 2012, AIS2, n° 340026.

54 Voir par exemple CE, 5 septembre 2008, Société Directannonces, n° 319071, à propos d’une mise en demeure.

55 Rapport d’activité 2016 de la Commission nationale de l’informatique et des libertés.

56 Rapport d’activité 2015 de la Commission nationale de l’informatique et des libertés.

57 Décision n°2004-499 DC du 29 juillet 2004.

58 Décision n°2014-412 QPC du 19 septembre 2014.

59 Délibération n° 2016-186 du 30 juin 2016 portant autorisation unique de mise en œuvre de dispositifs ayant pour finalité le contrôle d'accès par authentification biométrique aux locaux, aux appareils et aux applications informatiques sur les lieux de travail et garantissant la maîtrise par la personne concernée sur son gabarit biométrique (AU-052).

60 Les termes de « prétendue race » qui avaient été critiqués devant le Conseil constitutionnel, ont été déclarés conformes à la Constitution (décision n° 2016-745 DC du 26 janvier 2017).

61 CE, 8 décembre 2000, Conseil supérieur et l'administration de biens et a., n° 217046 et 217826.

62 « This Act shall apply to public bodies. It shall apply to private bodies if: 1. the controller or processor processes personal data in Germany, 2. personal data are processed in the context of the activities of an establishment of the controller or processor in Germany, or if, 3. although the controller or processor has no establishment in a Member State of the European Union or another contracting state of the European Economic Area, it does fall within the scope of Regulation (EU) 2016/679 of the European Parliament and the Council of 27 April 2016 on the protection of natural persons with regard to the processing of personal data and on the free movement of such data, and repealing Directive 95/46/EC (General Data Protection Regulation) (OJ L 119 of 4 May 2016, p. 1; L 314 of 22 November 2016, p. 72).

If this Act does not apply in accordance with the second sentence, only Sections 8 to 21 and 39 to 44 shall apply to the controller or processor. »

63 CJUE, 16 décembre 2008, l’affaire C-73/07.

64 Rapport d’activité 2016 de la Commission nationale de l’informatique et des libertés.

65 Décision n° 98-405 DC du 29 décembre 1998

66 [_https://www.cnil.fr/fr/etude-dimpacts-sur-la-vie-privee-suivez-la-methode-de-la-cnil_](https://www.cnil.fr/fr/etude-dimpacts-sur-la-vie-privee-suivez-la-methode-de-la-cnil)

67 Voir Groupe de travail « Article 29 » sur la protection des données, « Guidelines on Data Protection Impact Assessment (DPIA) and determining whether processing is “likely to result in a high risk” for the purposes of Regulation» [_http://ec.europa.eu/newsroom/just/item-detail.cfm?item_id=50083_](http://ec.europa.eu/newsroom/just/item-detail.cfm?item_id=50083) ,

68 Conseil d’Etat, Etude annuelle 2014, _« Le numérique et les droits fondamentaux_ _»_ , la documentation française, p.177

69 Les traitements concernant des données génétiques mis en œuvre par l’Etat dans le cadre de fichiers de souveraineté (ex : Fichier national automatisé des empreintes génétiques - FNAEG) continueront à relever du régime d’autorisation prévu à l’article 26. Lorsque les données génétiques sont traitées en tant que données se rapportant à la santé des personnes, les dispositions du chapitre IX s’appliqueront.

70 Voir _Informatique et libertés, Le droit de la protection des données à caractère personnel en droit français et européen_ , Anne Debet, Nathalie Metallinos et Jean Massot, Lextenso, 2015.

71 Voir par exemple, à propos du fichier TES (titres électroniques sécurisés), créé par le décret n° 2016-1460 du 28 octobre 2016 autorisant la création d'un traitement de données à caractère personnel relatif aux passeports et aux cartes nationales d'identité.

72 Délibération portant refus d'autorisation de mise en œuvre par la société VOLKSWAGEN FINANCES d'un traitement automatisé de données à caractère personnel ayant pour finalité l'utilisation du numéro de sécurité sociale à des fins de détection d'incohérence dans les demandes de crédit

73 L’ancien article 18 de la loi n° 78-17 (avant 2004) disposait que : « _l'utilisation du répertoire national d'identification des personnes physiques en vue d'effectuer des traitements nominatifs est autorisée par décret en Conseil d'État pris après avis de la commission_ »

74 Voir pour un exemple pris sous l’empire de la loi avant 2004, Décret n° 91-1404 du 27 décembre 1991 autorisant l’utilisation du NIR par les employeurs pour les traitements de gestion de la paie et de la gestion du personnel,

75 Articles 22 I bis et V, 27 II 4° de la loi n°78-17.

76 Une circulaire pourra appeler une attention particulière sur la protection des données à caractère personnel et la nécessité de prévoir une analyse d’impact au sens du règlement. Voir en ce sens, la circulaire n° 5598/SG du Premier ministre du 23 août 2012 relative à la prise en compte dans la préparation des textes législatifs et réglementaires de leur impact en termes d’égalité entre les hommes et les femmes définit des orientations particulières en termes de prise en compte, dans les travaux d’évaluation préalable, de la dimension des droits des femmes et de l’égalité entre les hommes et les femmes. Egalement, la circulaire n° 5602/SG du Premier ministre du 4 septembre 2012 relative à la prise en compte du handicap dans les projets de loi précise les modalités d’évaluation préalable portant sur la nécessité d’introduire dans le projet de loi des dispositions adaptées à la situation des personnes handicapées.

77 Cour de cassation, Com. 25 juin 2013, n°12-17.037.

78 Art. 30.5 du règlement (UE) 2016/679.

79 Article 30.5 du règlement : « _5\. Les obligations visées aux paragraphes 1 et 2 ne s'appliquent pas à une entreprise ou à une organisation comptant moins de 250 employés, sauf si le traitement qu'elles effectuent est susceptible de comporter un risque pour les droits et des libertés des personnes concernées, s'il n'est pas occasionnel ou s'il porte notamment sur les catégories particulières de données visées à l'article 9, paragraphe 1, ou sur des données à caractère personnel relatives à des condamnations pénales et à des infractions visées à l'article 10_ ».

80 Art. 83 du règlement.

81 Les Echos, 7 décembre 2017.

 _82 « Les données à caractère personnel ne peuvent faire l'objet d'une opération de traitement de la part d'un sous-traitant, d'une personne agissant sous l'autorité du responsable du traitement ou de celle du sous-traitant, que sur instruction du responsable du traitement. / Toute personne traitant des données à caractère personnel pour le compte du responsable du traitement est considérée comme un sous-traitant au sens de la présente loi. / Le sous-traitant doit présenter des garanties suffisantes pour assurer la mise en œuvre des mesures de sécurité et de confidentialité mentionnées à l'article 34. Cette exigence ne décharge pas le responsable du traitement de son obligation de veiller au respect de ces mesures. / Le contrat liant le sous-traitant au responsable du traitement comporte l'indication des obligations incombant au sous-traitant en matière de protection de la sécurité et de la confidentialité des données et prévoit que le sous-traitant ne peut agir que sur instruction du responsable du traitement. »_

83 Article 4 – 8) du règlement (UE) 2016/679, article 3-9) de la directive (UE) 2016/680.

84 Décision n° 2005-514 DC du 28 avril 2005, cons. 14

85 Décision n°2004-499 DC du 29 juillet 2004.

86 _« Les personnes morales victimes d'infractions ou agissant pour le compte desdites victimes pour les stricts besoins de la prévention et de la lutte contre la fraude ainsi que de la réparation du préjudice subi, dans les conditions prévues par la loi. »_

87 « _11\. Considérant que le 3° de l'article 9 de la loi du 6 janvier 1978, dans la rédaction que lui donne l'article 2 de la loi déférée, permettrait à une personne morale de droit privé, mandatée par plusieurs autres personnes morales estimant avoir été victimes ou être susceptibles d'être victimes d'agissements passibles de sanctions pénales, de rassembler un grand nombre d'informations nominatives portant sur des infractions, condamnations et mesures de sûreté ; qu'en raison de l'ampleur que pourraient revêtir les traitements de données personnelles ainsi mis en œuvre et de la nature des informations traitées, le 3° du nouvel article 9 de la loi du 6 janvier 1978 pourrait affecter, par ses conséquences, le droit au respect de la vie privée et les garanties fondamentales accordées aux citoyens pour l'exercice des libertés publiques ; que la disposition critiquée doit dès lors comporter les garanties appropriées et spécifiques répondant aux exigences de l'article 34 de la Constitution ;_

 _12\. Considérant que, s'agissant de l'objet et des conditions du mandat en cause, la disposition critiquée n'apporte pas ces précisions ; qu'elle est ambiguë quant aux infractions auxquelles s'applique le terme de « fraude » ; qu'elle laisse indéterminée la question de savoir dans quelle mesure les données traitées pourraient être partagées ou cédées, ou encore si pourraient y figurer des personnes sur lesquelles pèse la simple crainte qu'elles soient capables de commettre une infraction ; qu'elle ne dit rien sur les limites susceptibles d'être assignées à la conservation des mentions relatives aux condamnations ; qu'au regard de l'article 34 de la Constitution, toutes ces précisions ne sauraient être apportées par les seules autorisations délivrées par la Commission nationale de l'informatique et des libertés ; qu'en l'espèce et eu égard à la matière concernée, le législateur ne pouvait pas non plus se contenter, ainsi que le prévoit la disposition critiquée éclairée par les débats parlementaires, de poser une règle de principe et d'en renvoyer intégralement les modalités d'application à des lois futures ; que, par suite, le 3° du nouvel article 9 de la loi du 6 janvier 1978 est entaché d'incompétence négative ;_ »

88 http://www.justice.ie/en/JELR/General_Scheme_of_Data_Protection_Bill_(May_2017).pdf/Files/General_Scheme_of_Data_Protection_Bill_(May_2017).pdf

89 Les traitements portent sur des infractions constatées par les commerçants sur les lieux de vente par exemple, l’autorisation AU-017), la lutte contre la contrefaçon (par exemple, la délibération CNIL n°2011-111), incivilités des clients à l’égard des salariés (par exemple, la délibération CNIL n°2013-036), la gestion des procédures de conciliation (par exemple, la délibération CNIL n°2012-168).

90 Actuellement, 166 associations d'aide aux victimes sont subventionnées par le programme 101 « Accès au droit et à la justice ».

91 Article 3 de la loi n° 2009-1436 du 24 novembre 2009 pénitentiaire : « _Le service public pénitentiaire est assuré par l'administration pénitentiaire sous l'autorité du garde des sceaux, ministre de la justice, avec le concours des autres services de l'Etat, des collectivités territoriales, des associations et d'autres personnes publiques ou privées. »_

92 D’après TechCrunch, depuis décembre 2014, « la Legaltech est en plein essor, avec des sociétés qui tentent d’innover sur le marché du droit à tous les niveaux et de toute part ». Rubin, Basha (6 December 2014). "Legal Tech Startups Have A Short History And A Bright Future". TechCrunch. Retrieved 1 May 2015.

93 Décision n°2008-566 DC du 9 juillet 2008

94 Décision n° 2017-655 QPC du 15 septembre 2017.

95 Article 2 : « Un service public d’archives a pour missions de collecter, de conserver, d’évaluer, d’organiser, de décrire, de communiquer, de mettre en valeur et de diffuser des archives publiques conformément au I du l’article L. 212-4 et aux articles L. 212-6, L. 212-6-1, L. 212-8, L. 212-11, L. 212-12, R. 212-5, R. 212-6 et R. 212-8 [du code du patrimoine] »

96 Décision n° 2003-467 DC du 13 mars 2003.

97 C’est–à-dire  une forme de « droit à l’explication » du traitement, sur le fondement de l’article 39 de la loi n° 78-17 et, principalement, sur celui du décret n° 2017-330 du 14 mars 2017 qui prévoit que, lorsque l'usager exerce son droit, l'administration lui communique, sous une forme intelligible :

1° Le degré et le mode de contribution du traitement algorithmique à la prise de décision ;

2° Les données traitées et leurs sources ;

3° Les paramètres de traitement et, le cas échéant, leur pondération, appliqués à la situation de l'intéressé ;

4° Les opérations effectuées par le traitement.

98 Article publié dans Le Monde, 2 mai 2017, « La transparence des algorithmes relève des droits civiques »

([ _http://www.lemonde.fr/campus/article/2017/05/02/o21-la-transparence-des-algorithmes-releve-des-droits-civiques_5121201_4401467.html#5ayGvqYycS1oLG0s.99_](http://www.lemonde.fr/campus/article/2017/05/02/o21-la-transparence-des-algorithmes-releve-des-droits-civiques_5121201_4401467.html#5ayGvqYycS1oLG0s.99) ).

99 Étude annuelle 2014 - _Le numérique et les droits fondamentaux_ , p. 233 et s.

100 Etude annuelle 2017 - _Puissance publique et plateformes numériques : accompagner l’«ubérisation »._

101 On notera que c’est la principale garantie offerte dans l’exemple britannique.

102 _Acteurs publics_ , 8 décembre 2017.

103 L’article 13 paragraphe 3, l’article 15 paragraphe 1 et l’article 16 paragraphe 4 de la directive (UE) 680/2016 prévoient des dispositions équivalentes.

104 _« a) la sécurité nationale;\ b) la défense nationale; \ c) la sécurité publique; \d) la prévention et la détection d'infractions pénales, ainsi que les enquêtes et les poursuites en la matière ou l'exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces; \ e) d'autres objectifs importants d'intérêt public général de l'Union ou d'un État membre, notamment un intérêt économique ou financier important de l'Union ou d'un État membre, y compris dans les domaines monétaire, budgétaire et fiscal, de la santé publique et de la sécurité sociale; \ f) la protection de l'indépendance de la justice et des procédures judiciaires; \ g) la prévention et la détection de manquements à la déontologie des professions réglementées, ainsi que les enquêtes et les poursuites en la matière; h) \ une mission de contrôle, d'inspection ou de réglementation liée, même occasionnellement, à l'exercice de l'autorité publique, dans les cas visés aux points a) à e) et g); i) \ la protection de la personne concernée ou des droits et libertés d'autrui; j) \ l'exécution des demandes de droit civil. »_

105 CEDH, Huvig et Kruslin c/ France, 24 avril 1990, 11801/85 points 27 à 29.

106 Violation de la sécurité entraînant, de manière accidentelle ou illicite, la destruction, la perte, l'altération, la divulgation non autorisée de données à caractère personnel transmises, conservées ou traitées d'une autre manière, ou l'accès non autorisé à de telles données.

107 CJUE, 6 octobre 2015, Maximillian Schrems, C-362/14, considérant 65.

108 Décision n° 2006-540 DC du 27 juillet 2006, cons. 20 ; Décision 2008-564 DC du 19 juin 2008, cons.45, Décision n° 2010-605 DC du 12 mai 2010, cons.15 et 18.

109 Act to Adapt Data Protection Law to Regulation (EU) 2016/679 and to Implement Directive (EU) 2016/680

110 “Recourse to the administrative courts shall be provided for disputes between natural or legal persons and a supervisory authority of the Federation or a Land concerning rights according to Article 78 (1) and (2) of Regulation (EU) 2016/679 and Section 61. (…)”.

111 “(1) If a supervisory authority believes that an adequacy decision of the European Commission or a decision on the recognition of standard protection clauses or on the general validity of approved codes of conduct, on the validity of which a decision of the supervisory authority depends, violates the law, the supervisory authority shall suspend its procedure and lodge an application for a court decision. (…) 6. In proceedings pursuant to subsection 1, Section 47 (5), first sentence and (6) of the Code of Administrative Court Procedure shall apply accordingly. If the Federal Administrative Court finds that the European Commission’s decision pursuant to subsection 1 is valid, it shall state this in its decision. Otherwise it shall refer the question as to the validity of the decision in accordance with Article 267 of the Treaty on the Functioning of the European Union to the European Court of Justice.”

112 Data Protection Bill [HL] 2017-19, Data Protection Bill (HL Bill 66).

113 “173. Representation of data subjects (1) In relation to the processing of personal data to which the GDPR applies— / (a) Article 80 of the GDPR (representation of data subjects) enables a data subject to authorise a body or other organisation which meets the conditions set out in that Article to exercise certain rights on the data subject’s behalf, and / (b) a data subject may also authorise such a body or organisation to exercise the data subject’s rights under Article 82 (right to compensation).”

114 CJCE, 20 mars 1997, Commission contre Allemagne, C 96/95, Rec. p. 1653.

115 Décision n° 2003-467 DC du 13 mars 2003, Loi pour la sécurité intérieure, cons. 21 à 27 et 28 à 35.

116 Décision n° 2012-652 DC du 22 mars 2012, Loi relative à la protection de l’identité, cons. 8 ; cf également décision n° 2016-745 DC du 26 janvier 2017, Loi relative à l’égalité et à la citoyenneté, paragr. 25

117 Examen de la conformité à l’article 8 des fichiers FIJAIS (CEDH, 17 décembre 2009, M.B. c. France, n° 22115/06), FAED (CEDH, 18 avril 2013, M.K. c. France, n° 19522/09), STIC (CEDH, 18 septembre 2014, Brunet c. France) et FNAEG (CEDH, 22 juin 2017)

118 CEDH, 22 juin 2017, Aycaguer c.France, n° 8806/12, §38

119 Dans son arrêt précité du 18 septembre 2014, la Cour a considéré que la durée de vingt ans de conservation des données inscrites au STIC était « _importante, compte tenu de l’absence de déclaration judiciaire de culpabilité et du classement sans suite de la procédure après le succès de la médiation pénale_ » (§ 39 et 40).

120 Décret n° 2016-1862 du 23 décembre 2016.

121 Décision n° 2004-499 DC du 29 juillet 2004, cons. 21 à 23

122 Décision n°2010-25 QPC du 16 septembre 2010, M. jean-Victor C., cons. 16

123 Décision n°2017-670 QPC du 27 octobre 2017, M. Mikhail P.

124 CEDH, 18 septembre 2014, § 35

125 La Cour a particulièrement examiné la conformité à l’article 8 des fichiers FIJAIS (CEDH, 17 décembre 2009, M.B. c. France, n° 22115/06), FAED (CEDH, 18 avril 2013, M.K. c. France, n° 19522/09, STIC (CEDH, 18 septembre 2014) et FNAEG (CEDH, 22 juin 2017).

126 CEDH, 22 juin 2017, Aycaguer c.France, n° 8806/12, §38

127 CEDH, 17 décembre 2009 précité, § 60

128 CEDH, Aycaguer c. France précité, §44 et 45

129 En effet, l’article 3, paragraphe 2 (champ d'application de cette directive) dispose : « La présente directive ne s'applique pas au traitement de données à caractère personnel: - mis en œuvre pour l'exercice d'activités qui ne relèvent pas du champ d'application du droit communautaire, telles que celles prévues aux titres V et VI du traité sur l'Union européenne, et, en tout état de cause, aux traitements ayant pour objet la sécurité publique, la défense, la sûreté de l'État (y compris le bien-être économique de l'État lorsque ces traitements sont liés à des questions de sûreté de l'État) et les activités de l'État relatives à des domaines du droit pénal,… »

Il en résulte que l’article 25 de cette directive qui prohibe les transferts de données vers des Etats tiers n’assurant pas un niveau de protection adéquat ne s’applique pas aux transferts de données en matière pénale, en particulier les données en matière pénale, policières ou judiciaires.

130 JO n°74 du 18 mars 2007 page 5782

131 L’article 91 du décret dispose : « _Les informations figurant au 7° du I de l’article 32 de la loi du 6 janvier 1978 susvisée que le responsable du traitement communique, dans les conditions prévues à l’article 90, à la personne auprès de laquelle des données à caractère personnel sont recueillies, sont les suivantes :_

 _1° Le ou les pays d’établissement du destinataire des données dans les cas où ce ou ces pays sont déterminés lors de la collecte des données ;_

 _2° La nature des données transférées ;_

 _3° La finalité du transfert envisagé ;_

 _4° La ou les catégories de destinataires des données ;_

 _5° Le niveau de protection offert par le ou les pays tiers :_

 _a) Si le ou les pays tiers figurent dans la liste prévue à l’article 108, il est fait mention de la décision de la Commission européenne autorisant ce transfert ;_

 _b) Si le ou les pays tiers ne satisfont pas aux conditions prévues à l’article 68 de la même loi, il est fait mention de l’exception prévue à l’article 69 de cette loi qui permet ce transfert ou de la décision de la Commission nationale de l’informatique et des libertés autorisant ce transfert._

132 L’article 108 du décret dispose : « _La commission met à la disposition du public la liste des décisions de la Commission européenne concernant le niveau de protection offert par les États n’appartenant pas à la Communauté européenne au regard de la vie privée, des libertés et droits fondamentaux et à l’égard d’un transfert ou d’une catégorie de transferts de données à caractère personnel. Elle actualise cette liste au fur et à mesure de la publication des décisions de la Commission européenne au Journal officiel de l’Union européenne._

 _Elle met également à la disposition du public les clauses contractuelles types approuvées par la Commission européenne._

133 Lors de l’examen du projet de loi de ratification de l'accord d'entraide judiciaire en matière pénale entre le Gouvernement de la République française et le Gouvernement de la République populaire de Chine, signé à Paris le 18 avril 2005, le Conseil d’État a examiné cet accord et après de longs débats a émis un avis favorable en adressant au Gouvernement une note en formulant des observations. Cette note en date du 26 octobre 2006 comprend notamment une interprétation des articles 26, 68 et 69 de la loi informatique et libertés mentionnant (cf. page 3 de la note en annexe I) précisant :« _la ratification, après autorisation parlementaire, de l'accord dont les articles XVII et XVIII autorisent l'échange d'informations figurant dans les casiers judiciaires et d'avis de condamnation ne saurait avoir pour effet de dispenser les autorités françaises, chargées de transférer des données contenues dans des traitements automatisés de données à caractère personnel gérés par les autorités judiciaires vers un État n'appartenant pas à la Communauté européenne, des obligations de vérification qui leur incombent en application des articles 26, 68 et 69 précités à l'occasion de chaque demande de transfert._

 _Cette vérification devra tenir compte, à la date du transfert des données, non seulement du niveau spécifique de protection garanti par le traitement appliqué aux données objet du transfert, mais aussi de l'ensemble des circonstances qui commandent l'application effective des règles de protection définies pour ce transfert._

 _Il est souligné que, le transfert de données à caractère personnel dont le traitement a pour objet la prévention, la recherche, la constatation ou la poursuite des infractions pénales ou l'exécution des condamnations pénales ou des mesures de sûreté relève non pas de la catégorie des transferts nécessaires à la sauvegarde de l'intérêt public au sens du 2° de l'article 69 précité, mais de celle des transferts de données dont le traitement est autorisé par décret en Conseil d'État dans les conditions de fond et de procédure prévues à l'avant-dernier alinéa de l'article 69 »_.

134 C’est notamment le cas de la Suisse, du Canada, de l'Argentine, des territoires de Jersey et Guernesey, de l'Isle de Man, de la Norvège, de l’Islande et d’Israël, et de quelques autres Etats.

135 L’article 45, paragraphe 2 de la directive mentionne : « Chaque État membre prévoit que chaque autorité de contrôle n'est pas compétente pour contrôler les opérations de traitement effectuées par les juridictions dans l'exercice de leur fonction juridictionnelle. Les États membres peuvent prévoir que leur autorité de contrôle n'est pas compétente pour contrôler les opérations de traitement effectuées par d'autres autorités judiciaires indépendantes lorsqu'elles agissent dans l'exercice de leur fonction juridictionnelle »

136 Ordonnance n° 96-267 du 28 mars 1996 relative à l'entrée en vigueur du nouveau code pénal dans les territoires d'outre-mer et dans la collectivité territoriale de Mayotte ainsi qu'à l'extension et à la modification de certaines dispositions législatives rendues nécessaires par cette entrée en vigueur ; ordonnance n° 2000-916 du 19 septembre 2000 portant adaptation de la valeur en euros de certains montants exprimés en francs dans les textes législatifs ; ordonnance n° 2011-1012 du 24 août 2011 relative aux communications électroniques ; ordonnance n° 2015-948 du 31 juillet 2015 relative à l'égal accès des femmes et des hommes au sein des autorités administratives indépendantes et des autorités publiques indépendantes ; ordonnance n° 2016-462 du 14 avril 2016 portant création de l'Agence nationale de santé publique.

137 Décision du 16 juillet 2009, n° 2009-584 DC, cons. 20 à 23.

138 Voir par exemple, décision du 17 mai 2013, n°2013-669 DC, cons. 79.

139 Décision du 23 juin 2003, n°2003-473 DC, cons. 8.

140 Décision n° 2004-500 DC du 29 juillet 2004, cons. 13 ; ° 2005-514 DC, 28 avril 2005, cons. 14

141 Décision n° 2007-561 DC du 17 janvier 2008, cons. 6.

142 CJCE, arrêt Costa contre Enel, 15 juillet 1964.

143 Décision n° 2007-561 DC du 17 janvier 2008, cons. 8.

144 L’article 21 du présent projet de loi procède à des mesures de coordination du fait notamment de la disparation de certaines formalités préalables. Les impacts de cette disposition sont uniquement de nature juridique.

145 Le Conseil constitutionnel n’a pas eu l’occasion de se prononcer sur la nouvelle rédaction de l’article 31 de la loi n°78-17 relative à l'informatique, aux fichiers et aux libertés, dans sa rédaction issue de la loi pour une République numérique.

146 Article L. 311-1 du code des relations entre le public et l’administration : « _Le droit à communication ne s'applique qu'à des documents achevés._ » ; article L. 311-6 de ce code : « _Ne sont communicables qu'à l'intéressé les documents administratifs : / 1° Dont la communication porterait atteinte à la protection de la vie privée, (…)_ ».

 _147 « Les autorisations accordées par un État membre ou une autorité de contrôle sur le fondement de l'article 26, paragraphe 2, de la directive 95/46/CE demeurent valables jusqu'à leur modification, leur remplacement ou leur abrogation, si nécessaire, par ladite autorité de contrôle. Les décisions adoptées par la Commission sur le fondement de l'article 26, paragraphe 4, de la directive 95/46/CE demeurent en vigueur jusqu'à leur modification, leur remplacement ou leur abrogation, si nécessaire, par une décision de la Commission adoptée conformément au paragraphe 2 du présent article. »_

148 Groupe de travail « Article 29 » sur la protection des données ,« Lignes directrices concernant l’analyse d’impact relative à la protection des données (AIPD) et la manière de déterminer si le traitement est «susceptible d’engendrer un risque élevé» aux fins du règlement (UE) 2016/679 » , version française, page 15 [_http://ec.europa.eu/newsroom/just/item-detail.cfm?item_id=50083_](http://ec.europa.eu/newsroom/just/item-detail.cfm?item_id=50083)

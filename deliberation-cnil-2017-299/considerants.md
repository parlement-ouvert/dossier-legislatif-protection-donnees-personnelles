<!-- TITLE: Considérants -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Délibération CNIL</span> <a href="/deliberation-cnil-2017-299/introduction" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

La Commission nationale de l'informatique et des libertés,

Saisie par la ministre de la justice, garde des sceaux, d’une demande d’avis concernant un projet de loi d’adaptation au droit de l’Union européenne de la [loi n°78-17 du 6 janvier 1978](/loi-78-17) ;

Vu la [convention n° 108 du Conseil de l’Europe](https://www.coe.int/en/web/conventions/full-list/-/conventions/rms/0900001680078b39) pour la protection des personnes à l’égard du traitement automatisé des données à caractère personnel ;

Vu le [Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016](/reglement-2016-679) relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la [Directive 95/46/CE](http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:31995L0046) ;

Vu la [Directive (UE) 2016/680 du Parlement européen et du Conseil du 27 avril 2016](/directive-2016-680) relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales, et à la libre circulation de ces données, et abrogeant la [décision-cadre 2008/977/JAI du Conseil](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex:32008F0977) ;

Vu la loi n° 78-17 du 6 janvier 1978 modifiée relative à l’informatique, aux fichiers et aux libertés, notamment son [article 11-4°-a)](/loi-78-17/chapitre-iii/article-11) ;

Vu le [décret n° 2005-1309 du 20 octobre 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006052581&dateTexte=20090319) modifié pris pour l’application de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés ;

Après avoir entendu Mme Isabelle FALQUE-PIERROTIN, commissaire, en son rapport, et Mme Nacima BELKACEM, commissaire du Gouvernement, en ses observations,

Émet l’avis suivant :

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Délibération CNIL</span> <a href="/deliberation-cnil-2017-299/introduction" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

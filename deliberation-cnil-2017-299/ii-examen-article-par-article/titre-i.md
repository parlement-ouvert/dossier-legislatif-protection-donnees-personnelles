<!-- TITLE: TITRE I<sup>er</sup> -->
<!-- SUBTITLE: DISPOSITIONS COMMUNES AU REGLEMENT ET A LA  DIRECTIVE 2016 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/deliberation-cnil-2017-299/ii-examen-article-par-article" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Délibération CNIL</span> <a href="/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Chapitre I<sup>er</sup> — DISPOSITIONS RELATIVES A LA Commission NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i)

  * [Article 1<sup>er</sup> (Missions de la Commission)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-1)
  * [Article 2 (Composition de la Commission) et 3 (Commissaire du Gouvernement)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-2-composition-de-la-commission-et-3-commissaire-du-gouvernement)
  * [Article 4 (Pouvoirs de contrôle de la Commission)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-4-pouvoirs-de-controle-de-la-commission)
  * [Article 5 (Coopération)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-5-cooperation)
  * [Article 6 (Mesures correctrices)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-6-mesures-correctrices)

* [Chapitre II — Dispositions relatives a certaines categories de donnees](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-ii)

  * [Article 7 (Données sensibles)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-ii/article-7-donnees-sensibles)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/deliberation-cnil-2017-299/ii-examen-article-par-article" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Délibération CNIL</span> <a href="/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

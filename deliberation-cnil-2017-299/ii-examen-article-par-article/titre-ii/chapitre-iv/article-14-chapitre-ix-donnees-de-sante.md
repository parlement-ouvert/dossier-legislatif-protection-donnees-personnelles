<!-- TITLE: Article 14 (Chapitre IX données de santé) -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv/article-13-archives-recherche-scientifique-et-statistique" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Délibération CNIL</span> <a href="/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv-bis" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

L’article 14 du projet de loi vise à insérer dans l’actuel chapitre IX de la loi l’ensemble des dispositions relatives aux traitements de données à caractère personnel dans le domaine de la santé ; il comporte à cette fin deux sections : l’une relative aux dispositions générales, l’autre relative aux traitements à des fins de recherche, d’étude ou d’évaluation dans le domaine de la santé.

A titre liminaire, la Commission estime utile qu’un chapitre de la loi soit dédié aux traitements de données de santé. Elle prend acte de l’économie générale retenue qui ne vise pas à bouleverser les principes en matière de traitement de données de santé qui, pour ceux issus de la loi de modernisation de notre système de santé de janvier 2016, rentrent seulement en application depuis quelques mois. Elle relève toutefois le maintien d’un processus d’autorisation exigeant, encadrant les traitements relatifs à la recherche dans le domaine de la santé, en comparaison de l’allègement général des formalités préalables portées par le Règlement et le projet de loi qui lui est soumis.

En tout état de cause, la Commission s’interroge très fortement sur la rédaction de ce chapitre qui opère une confusion entre les règles applicables, d’une part, aux traitements à des fins de recherche, d’étude et d’évaluation et, d’autre part, celles – nécessairement plus générales - applicables aux autres traitements comportant des données de santé. Il en résulte une réelle incertitude juridique, notamment sur les outils de simplification développés précédemment par la Commission en la matière.

\- Plus précisément, la section 1 relative aux dispositions générales fixe les règles s’appliquant à l’ensemble des traitements de données de santé.

Le projet de loi reprend le dispositif actuellement prévu par la loi Informatique et Libertés , c’est-à-dire la possibilité de traiter des données de santé en dehors des exceptions limitativement énumérés à l’article 8 de la loi, pour une seule finalité d’intérêt public et sous réserve d’une autorisation de la Commission.

Etrangement, le projet énonce ensuite toute une série de dispositions directement reprises de l’actuel chapitre IX : d’une part, des mesures destinées à alléger la procédure d’autorisation ( _méthodologies de référence_ , _jeux de données pouvant faire l’objet d’une mise à disposition_ ou encore _décisions uniques_ ), d’autre part, des règles de fond (interdiction de _communication_ des données, possibilité pour les professionnels de _transmettre_ les données à caractère personnel qu’ils détiennent, condition de présentation _des résultats_ , etc.).

Manifestement, ces dispositions n’ont pas vocation à s’appliquer aux traitements comportant des données de santé en dehors de la recherche tels que les vigilances (pharmacovigilance, matériovigilance, cosmétovigilance, etc.), les mécanismes de surveillance sanitaire ou encore les entrepôts de données.

La Commission propose donc de réintégrer l’ensemble des dispositions relatives aux traitements de recherche, d’étude ou d’évaluation dans la section 2, y compris le rôle de l’Institut national des données de santé et ce, tant au regard de ses missions, telles que prévues par le code de la santé publique, que de sa composition.

Ne devraient ainsi plus figurer dans la section 1 que le principe général d’autorisation par la Commission des traitements présentant une finalité d’intérêt public et la faculté, pour elle, d’établir des autorisations uniques sur le modèle de ce qu’elle a déjà pu adopter.

La Commission souligne que ce dispositif, équivalent à celui instauré depuis 2004, a permis un encadrement solide et efficace des traitements de données de santé et a vocation à être complété par les nouveaux pouvoirs dont le projet de loi envisage de doter la Commission (établissement et publication de lignes directrices, de recommandations, de référentiels et de règlements types).

\- Dans cette logique, la section 2 relative aux dispositions particulières aux traitements à des fins de recherche, d’étude ou d’évaluation dans le domaine de la santé devrait reprendre, pour l’essentiel, les dispositions actuellement en vigueur au sein du chapitre IX.

Sous cette réserve, les modifications envisagées par le projet de loi aux traitements à des fins de recherche, d’étude ou d’évaluation appellent les principales observations suivantes.

En premier lieu, le projet de loi prévoit que la demande d’autorisation est réputée acceptée lorsque la Commission ne s’est pas prononcée dans un délai de deux mois si l’avis du comité d’expertise compétent est expressément favorable. Ce renversement du principe en vigueur (en cas de silence, la demande est actuellement réputée rejetée) n’est pas sans incidence sur la capacité d’analyse de la Commission au vu du nombre de dossiers qui lui est soumis (plus de 700 demandes d’autorisation reçues en 2017) et, surtout, de leur sensibilité.

Ce principe appelle donc des réserves de la part de la Commission.

En deuxième lieu, le projet de loi maintient une information individuelle atténuée des personnes dont les données sont appelées à être traitées à des fins de recherche. La Commission relève que cette modalité d’information n’est pas conforme aux exigences posées par les articles 13 et 14 du Règlement.

En troisième lieu, le projet de loi contient des dispositions en contradiction avec celles du code de la santé publique.

D’une part, il maintient le principe du consentement des personnes concernées dans le cas où la recherche nécessite le recueil de prélèvements biologiques identifiants. Cette disposition est contraire à celles de l’article L. 1131-1-1 du code de la santé publique. La Commission estime indispensable une coordination entre ces deux dispositions dans le sens d’une protection renforcée des personnes.

D’autre part, les dispositions du projet de loi maintiennent des dérogations à l’information des titulaires de l’autorité parentale incompatibles avec les dispositions du code de la santé publique. En effet, ce dernier ne prévoit que des dérogations à l’obligation de recueillir le consentement de chacun des titulaires de l’autorité parentale et non à celle de les informer.

La Commission estime donc nécessaire d’harmoniser l’ensemble de ces dispositions.

Enfin, la Commission propose de supprimer le projet d’article 62 de la loi, dans la mesure où tout transfert de données à caractère personnel hors Union européenne doit être conforme aux conditions établies par le Règlement, y compris pour les données codées.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv/article-13-archives-recherche-scientifique-et-statistique" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Délibération CNIL</span> <a href="/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv-bis" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

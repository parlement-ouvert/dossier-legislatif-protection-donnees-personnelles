<!-- TITLE: TITRE II -->
<!-- SUBTITLE: MARGES DE MANOEUVRE -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-ii/article-7-donnees-sensibles" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Délibération CNIL</span> <a href="/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Chapitre I — Champ d’application](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-i)

  * [Article 8 (Champ d’application territorial)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-i/article-8-champ-dapplication-territorial)

* [Chapitre II — Dispositions relatives a la simplification des formalites prealables a la mise en œuvre des traitements](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-ii)

  * [Article 9 (Formalités et NIR)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-ii/article-9-formalites-et-nir)

* [Chapitre III — Obligations incombant aux responsables de traitements et sous-traitants](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iii)

  * [Articles 10 et 22 (Délégué à la protection des données)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iii/articles-10-et-22-delegue-a-la-protection-des-donnees)
  * [Articles 11 et 22 (Sous-traitant)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iii/articles-11-et-22-sous-traitant)

* [Chapitre IV — Dispositions relatives a certaines categories particulieres de traitement](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv)

  * [Article 12 (Données d’infraction)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv/article-12-donnees-dinfraction)
  * [Article 13 (Archives, recherche scientifique et statistique)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv/article-13-archives-recherche-scientifique-et-statistique)
  * [Article 14 (Chapitre IX données de santé)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv/article-14-chapitre-ix-donnees-de-sante)

* [Chapitre IV bis — Dispositions particulières relatives aux droits des personnes concernées](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv-bis)

  * [Article 15 (Mineurs)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv-bis/article-15-mineurs)
  * [Article 16 (Profilage)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv-bis/article-16-profilage)
  * [Article 17 (Limitation des droits des personnes pour certains traitements de l’Etat)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv-bis/article-17-limitation-des-droits-des-personnes-pour-certains-traitements-de-letat)

* [Chapitre V — Voies de recours](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-v)

  * [Article 18 (Action de groupe)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-v/article-18-action-de-groupe)
  * [Article 19 (Action en justice)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-v/article-19-action-en-justice)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-ii/article-7-donnees-sensibles" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Délibération CNIL</span> <a href="/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

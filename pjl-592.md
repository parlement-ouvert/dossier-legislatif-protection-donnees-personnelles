<!-- TITLE: N° 592 annexe 0 - Rapport sur le projet de loi, après engagement de la procédure accélérée, relatif à la protection des données personnelles (n°490). -->

* [Entête](/pjl-592/entete)

* [TITRE IER — DISPOSITIONS D’ADAPTATION COMMUNES AU RÈGLEMENT (UE) 2016/679 DU PARLEMENT EUROPÉEN ET DU CONSEIL DU 27 AVRIL 2016 ET À LA DIRECTIVE (UE) 2016/680
DU PARLEMENT EUROPÉEN ET DU CONSEIL DU 27 AVRIL 2016](/pjl-592/titre-i)

  * [Chapitre Ier — Dispositions relatives
à la Commission nationale de l’informatique et des libertés](/pjl-592/titre-i/chapitre-i)

    * [Article 1er](/pjl-592/titre-i/chapitre-i/article-1)
    * [Article 1er bis (nouveau)](/pjl-592/titre-i/chapitre-i/article-1-bis-nouveau)
    * [Article 2](/pjl-592/titre-i/chapitre-i/article-2)
    * [Article 2 bis (nouveau)](/pjl-592/titre-i/chapitre-i/article-2-bis-nouveau)
    * [Article 3](/pjl-592/titre-i/chapitre-i/article-3)
    * [Article 4](/pjl-592/titre-i/chapitre-i/article-4)
    * [Article 5](/pjl-592/titre-i/chapitre-i/article-5)
    * [Article 6](/pjl-592/titre-i/chapitre-i/article-6)

  * [Chapitre II — Dispositions relatives à certaines catégories de données](/pjl-592/titre-i/chapitre-ii)

    * [Article 7](/pjl-592/titre-i/chapitre-ii/article-7)

* [TITRE II — MARGES DE MANŒUVRE PERMISES PAR LE RÈGLEMENT (UE) 2016/679 DU PARLEMENT EUROPÉEN ET DU CONSEIL DU 27 AVRIL 2016 RELATIF À LA PROTECTION DES PERSONNES PHYSIQUES À L’ÉGARD DU TRAITEMENT DES DONNÉES À CARACTÈRE PERSONNEL ET À LA LIBRE CIRCULATION DE CES DONNÉES, ET ABROGEANT LA DIRECTIVE 95/46/CE](/pjl-592/titre-ii)

  * [Chapitre Ier — Champ d’application territorial des dispositions
complétant le règlement (UE) 2016/679](/pjl-592/titre-ii/chapitre-i)

    * [Article 8](/pjl-592/titre-ii/chapitre-i/article-8)

  * [Chapitre II — Dispositions relatives à la simplification
des formalités préalables à la mise en œuvre des traitements](/pjl-592/titre-ii/chapitre-ii)

    * [Article 9](/pjl-592/titre-ii/chapitre-ii/article-9)

  * [Chapitre III — Obligations incombant aux responsables de traitements et sous-traitants](/pjl-592/titre-ii/chapitre-iii)

    * [Article 10](/pjl-592/titre-ii/chapitre-iii/article-10)

  * [Chapitre IV — Dispositions relatives à certaines catégories particulières de traitement](/pjl-592/titre-ii/chapitre-iv)

    * [Article 11](/pjl-592/titre-ii/chapitre-iv/article-11)
    * [Article 12](/pjl-592/titre-ii/chapitre-iv/article-12)
    * [Article 13](/pjl-592/titre-ii/chapitre-iv/article-13)

  * [Chapitre V — Dispositions particulières relatives aux droits des personnes concernées](/pjl-592/titre-ii/chapitre-v)

    * [Article 14 A (nouveau)](/pjl-592/titre-ii/chapitre-v/article-14-a-nouveau)
    * [Article 14](/pjl-592/titre-ii/chapitre-v/article-14)
    * [Article 15](/pjl-592/titre-ii/chapitre-v/article-15)

  * [Chapitre VI — Voies de recours](/pjl-592/titre-ii/chapitre-vi)

    * [Article 16 A (nouveau)](/pjl-592/titre-ii/chapitre-vi/article-16-a-nouveau)
    * [Article 16](/pjl-592/titre-ii/chapitre-vi/article-16)
    * [Article 17](/pjl-592/titre-ii/chapitre-vi/article-17)

* [TITRE III — DISPOSITIONS PORTANT TRANSPOSITION DE LA DIRECTIVE (UE) 2016/680 DU PARLEMENT EUROPÉEN ET DU CONSEIL
DU 27 AVRIL 2016 RELATIVE À LA PROTECTION DES PERSONNES PHYSIQUES À L’ÉGARD DU TRAITEMENT DES DONNÉES À CARACTÈRE PERSONNEL PAR LES AUTORITÉS COMPÉTENTES À DES FINS DE PRÉVENTION ET DE DÉTECTION DES INFRACTIONS PÉNALES, D’ENQUÊTES ET DE POURSUITES EN LA MATIÈRE OU D’EXÉCUTION DE SANCTIONS PÉNALES, ET À LA LIBRE CIRCULATION
DE CES DONNÉES, ET ABROGEANT LA DÉCISION-CADRE 2008/977/JAI DU CONSEIL](/pjl-592/titre-iii)

  * [Article 18](/pjl-592/titre-iii/article-18)
  * [Article 19](/pjl-592/titre-iii/article-19)

* [TITRE IV — HABILITATION À AMÉLIORER L’INTELLIGIBILITÉ
DE LA LÉGISLATION APPLICABLE
À LA PROTECTION DES DONNÉES](/pjl-592/titre-iv)

  * [Article 20](/pjl-592/titre-iv/article-20)

* [TITRE V — DISPOSITIONS DIVERSES ET FINALES](/pjl-592/titre-v)

  * [Article 21](/pjl-592/titre-v/article-21)
  * [Article 22](/pjl-592/titre-v/article-22)
  * [Article 23](/pjl-592/titre-v/article-23)
  * [Article 23 bis (nouveau)](/pjl-592/titre-v/article-23-bis-nouveau)
  * [Article 24](/pjl-592/titre-v/article-24)

<!-- TITLE: Article 17 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-iii/article-16" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-iii/article-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2011-334
 du 29 mars 2011 - art. 3](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782608&dateTexte=20110330&categorieLien=id#LEGIARTI000023782608)

----

La formation restreinte <ins style="background-color: #F8EDA8; text-decoration: none;">prend les mesures et</ins> prononce les sanctions à l'encontre des responsables de traitements qui ne respectent pas les obligations découlant <ins style="background-color: #F8EDA8; text-decoration: none;">du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité et</ins> de la présente loi dans les conditions prévues au [chapitre VII](/loi-78-17-pjl-592/chapitre-vii).

<ins style="background-color: #F8EDA8; text-decoration: none;">Ses membres délibèrent hors de la présence des agents de la commission, à l’exception de ceux chargés de la tenue de la séance.</ins>

Les membres de la formation restreinte ne peuvent participer à l'exercice des attributions de la commission mentionnées aux c, e et f du 2° <ins style="background-color: #F8EDA8;">du I</ins> de l'[article 11](/loi-78-17-pjl-592/chapitre-iii/article-11) et à l'[article 44](/loi-78-17-pjl-592/chapitre-vi/article-44).

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-iii/article-16" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-iii/article-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 15 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-iii/article-14-abroge" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-iii/article-15-bis" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

Sous réserve des compétences du bureau et de la formation restreinte, la commission se réunit en formation plénière.

En cas de partage égal des voix, la voix du président est prépondérante.

La commission peut charger le président ou le vice-président délégué d'exercer celles de ses attributions mentionnées :

\- au troisième alinéa du I de l'[article 23](/loi-78-17-pjl-592/chapitre-iv/section-1/article-23) ;

\- aux e et f du 2° <ins style="background-color: #F8EDA8;">du I</ins> de l'[article 11](/loi-78-17-pjl-592/chapitre-iii/article-11) ;

\- au c du 2° <ins style="background-color: #F8EDA8;">du I</ins> de l'[article 11](/loi-78-17-pjl-592/chapitre-iii/article-11) ;

\- au d du 4° <ins style="background-color: #F8EDA8;">du I</ins> de l'[article 11](/loi-78-17-pjl-592/chapitre-iii/article-11) ;

\- aux articles [41](/loi-78-17-pjl-592/chapitre-v/section-2/article-41) et [42](/loi-78-17-pjl-592/chapitre-v/section-2/article-42) ;

\- à l'[article 54](/loi-78-17-pjl-592/chapitre-ix/article-54) ;

\- aux deux derniers alinéas de l'[article 69](/loi-78-17-pjl-592/chapitre-xii/article-69), à l'exception des traitements mentionnés aux I ou II de l'[article 26](/loi-78-17-pjl-592/chapitre-iv/section-2/article-26) ;

\- au premier alinéa de l'[article 70](/loi-78-17-pjl-592/chapitre-xii/article-70).

<ins style="background-color: #F8EDA8; text-decoration: none;">La commission peut également charger le secrétaire général d’informer les auteurs de réclamations, pétitions et plaintes relatives à la mise en œuvre des traitements de données à caractère personnel des suites données à celles-ci, en application du c du 2° de l’[article 11](/loi-78-17-pjl-592/chapitre-iii/article-11), sous réserve que ce dernier ne détienne pas, directement ou indirectement, des intérêts dans une entreprise du secteur des communications électroniques ou de l’informatique.</ins>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-iii/article-14-abroge" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-iii/article-15-bis" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

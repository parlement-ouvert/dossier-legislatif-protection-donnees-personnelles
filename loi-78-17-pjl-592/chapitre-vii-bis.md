<!-- TITLE: Chapitre VII bis  -->
<!-- SUBTITLE: De la coopération -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-vii/article-48" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-vii-bis/article-49" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

----

* [Article 49](/loi-78-17-pjl-592/chapitre-vii-bis/article-49)
* <ins style="background-color: #F8EDA8; text-decoration: none;">[Article 49-1](/loi-78-17-pjl-592/chapitre-vii-bis/article-49-1)
* <ins style="background-color: #F8EDA8; text-decoration: none;">[Article 49-2](/loi-78-17-pjl-592/chapitre-vii-bis/article-49-2)
* <ins style="background-color: #F8EDA8; text-decoration: none;">[Article 49-3](/loi-78-17-pjl-592/chapitre-vii-bis/article-49-3)
* <ins style="background-color: #F8EDA8; text-decoration: none;">[Article 49-4](/loi-78-17-pjl-592/chapitre-vii-bis/article-49-4)
* [Article 49-5](/loi-78-17-pjl-592/chapitre-vii-bis/article-49-5)

----

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-vii/article-48" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-vii-bis/article-49" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>
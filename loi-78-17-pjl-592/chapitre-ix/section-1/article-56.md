<!-- TITLE: Article 56 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-ix/section-1/article-55" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-ix/section-1/article-57" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Créé par [Loi n°2004-801 du 6 août 2004 - art. 9 JORF 7 août 2004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529402&dateTexte=20040807&categorieLien=id#LEGIARTI000006529402)

----

<del style="background-color: #FFAEAE;">Toute personne a le droit de s'opposer à ce que des données à caractère personnel la concernant fassent l'objet de la levée du secret professionnel rendue nécessaire par un traitement de la nature de ceux qui sont visés à l'article 53.

<del style="background-color: #FFAEAE;">Dans le cas où la recherche nécessite le recueil de prélèvements biologiques identifiants, le consentement éclairé et exprès des personnes concernées doit être obtenu préalablement à la mise en oeuvre du traitement de données.

<del style="background-color: #FFAEAE;">Les informations concernant les personnes décédées, y compris celles qui figurent sur les certificats des causes de décès, peuvent faire l'objet d'un traitement de données, sauf si l'intéressé a, de son vivant, exprimé son refus par écrit.

<ins style="background-color: #F8EDA8; text-decoration:none;">Nonobstant les règles relatives au secret professionnel, les membres des professions de santé peuvent transmettre au responsable de traitement de données autorisé en application de [l’article 54](/loi-78-17/chapitre-ix/article-54) les données à caractère personnel qu’ils détiennent.

<ins style="background-color: #F8EDA8; text-decoration:none;">Lorsque ces données permettent l’identification des personnes, leur transmission doit être effectuée dans des conditions de nature à garantir leur confidentialité. La Commission nationale de l’informatique et des libertés peut adopter des recommandations ou des référentiels sur les procédés techniques à mettre en œuvre.

<ins style="background-color: #F8EDA8; text-decoration:none;">Lorsque le résultat du traitement de données est rendu public, l’identification directe ou indirecte des personnes concernées doit être impossible.

<ins style="background-color: #F8EDA8; text-decoration:none;">Les personnes appelées à mettre en œuvre le traitement de données ainsi que celles qui ont accès aux données sur lesquelles il porte sont astreintes au secret professionnel sous les peines prévues à [l’article 226-13 du code pénal](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417945).

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-ix/section-1/article-55" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-ix/section-1/article-57" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 22 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-iv/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 34](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205110&dateTexte=20161008&categorieLien=id#LEGIARTI000033205110)

----

<del style="background-color: #FFAEAE;">I. - A l'exception de ceux qui relèvent des dispositions prévues aux articles 25, 26 et 27 ou qui sont visés au deuxième alinéa de l'article 36, les traitements automatisés de données à caractère personnel font l'objet d'une déclaration auprès de la Commission nationale de l'informatique et des libertés.

<del style="background-color: #FFAEAE;">I bis.-Par dérogation au 1° des I et II de l'article 27, font également l'objet d'une déclaration auprès de la Commission nationale de l'informatique et des libertés les traitements qui portent sur des données à caractère personnel parmi lesquelles figure le numéro d'inscription des personnes au répertoire national d'identification des personnes physiques ou qui requièrent une consultation de ce répertoire, lorsque ces traitements ont exclusivement des finalités de statistique publique, sont mis en œuvre par le service statistique public et ne comportent aucune des données mentionnées au I de l'article 8 ou à l'article 9, à la condition que le numéro d'inscription à ce répertoire ait préalablement fait l'objet d'une opération cryptographique lui substituant un code statistique non signifiant, ainsi que les traitements ayant comme finalité exclusive de réaliser cette opération cryptographique. L'utilisation du code statistique non signifiant n'est autorisée qu'au sein du service statistique public. L'opération cryptographique est renouvelée à une fréquence définie par décret en Conseil d'Etat pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés.

<del style="background-color: #FFAEAE;">II. - Toutefois, ne sont soumis à aucune des formalités préalables prévues au présent chapitre :

<del style="background-color: #FFAEAE;">1° Les traitements ayant pour seul objet la tenue d'un registre qui, en vertu de dispositions législatives ou réglementaires, est destiné exclusivement à l'information du public et est ouvert à la consultation de celui-ci ou de toute personne justifiant d'un intérêt légitime ;

<del style="background-color: #FFAEAE;">2° Les traitements mentionnés au 3° du II de l'article 8.

<del style="background-color: #FFAEAE;">III. - Les traitements pour lesquels le responsable a désigné un correspondant à la protection des données à caractère personnel chargé d'assurer, d'une manière indépendante, le respect des obligations prévues dans la présente loi sont dispensés des formalités prévues aux articles 23 et 24, sauf lorsqu'un transfert de données à caractère personnel à destination d'un Etat non membre de la Communauté européenne est envisagé.

<del style="background-color: #FFAEAE;">La désignation du correspondant est notifiée à la Commission nationale de l'informatique et des libertés. Elle est portée à la connaissance des instances représentatives du personnel.

<del style="background-color: #FFAEAE;">Le correspondant est une personne bénéficiant des qualifications requises pour exercer ses missions. Il tient une liste des traitements effectués immédiatement accessible à toute personne en faisant la demande et ne peut faire l'objet d'aucune sanction de la part de l'employeur du fait de l'accomplissement de ses missions. Il peut saisir la Commission nationale de l'informatique et des libertés des difficultés qu'il rencontre dans l'exercice de ses missions.

<del style="background-color: #FFAEAE;">En cas de non-respect des dispositions de la loi, le responsable du traitement est enjoint par la Commission nationale de l'informatique et des libertés de procéder aux formalités prévues aux articles 23 et 24. En cas de manquement constaté à ses devoirs, le correspondant est déchargé de ses fonctions sur demande, ou après consultation, de la Commission nationale de l'informatique et des libertés.

<del style="background-color: #FFAEAE;">IV. - Le responsable d'un traitement de données à caractère personnel qui n'est soumis à aucune des formalités prévues au présent chapitre communique à toute personne qui en fait la demande les informations relatives à ce traitement mentionnées aux 2° à 6° du I de l'article 31.

<del style="background-color: #FFAEAE;">V. - Les traitements de données de santé à caractère personnel mis en œuvre par les organismes ou les services chargés d'une mission de service public figurant sur une liste fixée par arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l'informatique et des libertés, afin de répondre, en cas de situation d'urgence, à une alerte sanitaire, au sens de l'[article L. 1413-1 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006686950&dateTexte=&categorieLien=cid), sont soumis au régime de la déclaration préalable prévu au présent article. Le responsable de traitement rend compte chaque année à la Commission nationale de l'informatique et des libertés des traitements ainsi mis en œuvre.

<del style="background-color: #FFAEAE;">Les conditions dans lesquelles ces traitements peuvent utiliser le numéro d'inscription au répertoire national d'identification des personnes physiques sont définies par décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés.

<ins style="background-color: #F8EDA8;">***Un décret en Conseil d’État, pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés**, détermine les catégories de responsables de traitement et les finalités de ces traitements au vu desquelles ces derniers peuvent être mis en œuvre lorsqu’ils portent sur des données comportant le numéro d’inscription des personnes au répertoire national d’identification des personnes physiques. La mise en œuvre des traitements intervient sans préjudice des obligations qui incombent aux responsables de traitement ou à leurs sous-traitants en application de la [section 3 du chapitre IV du règlement (UE) 2016/679](/reglement-2016-679/chapitre-iv/section-3) du Parlement européen et du Conseil du 27 avril 2016 précité.*

<ins style="background-color: #F8EDA8;">« N’entrent pas dans le champ d’application du premier alinéa du présent article ceux des traitements portant sur des données à caractère personnel parmi lesquelles figure le numéro d’inscription des personnes au répertoire national d’identification des personnes physiques ou qui requièrent une consultation de ce répertoire :

<ins style="background-color: #F8EDA8;">« 1° Qui ont exclusivement des finalités de statistique publique, sont mis en œuvre par le service statistique public et ne comportent aucune des données mentionnées au [I de l’article 8](/loi-78-17/chapitre-ii/section-2/article-8) ou à [l’article 9](/loi-78-17/chapitre-ii/section-2/article-9) ;

<ins style="background-color: #F8EDA8;">« 2° Qui ont exclusivement des finalités de recherche scientifique ou historique ;

<ins style="background-color: #F8EDA8;">« 3° Qui ont pour objet de mettre à la disposition des usagers de l’administration un ou plusieurs téléservices de l’administration électronique définis à [l’article 1er de l’ordonnance n° 2005-1516 du 8 décembre 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives, mis en œuvre par l’État ou une personne morale de droit public ou une personne morale de droit privé gérant un service public.

<ins style="background-color: #F8EDA8;">« Pour les traitements dont les finalités sont mentionnées aux 1° et 2° du présent article, le numéro d’inscription au répertoire national d’identification des personnes physiques fait préalablement l’objet d’une opération cryptographique lui substituant un code statistique non signifiant. Cette opération est renouvelée à une fréquence définie par décret en Conseil d’État, pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés. Les traitements ayant comme finalité exclusive de réaliser cette opération cryptographique ne sont pas soumis aux dispositions du premier alinéa.

<ins style="background-color: #F8EDA8;">« Pour les traitements dont les finalités sont mentionnées au 1°, l’utilisation du code statistique non signifiant n’est autorisée qu’au sein du service statistique public.

<ins style="background-color: #F8EDA8;">« Pour les traitements dont les finalités sont mentionnées au 2°, l’opération cryptographique et, le cas échéant, l’interconnexion de deux fichiers par l’utilisation du code spécifique non signifiant qui en est issu ne peuvent être assurées par la même personne ni par le responsable de traitement.

<ins style="background-color: #F8EDA8;">« À l’exception des traitements mentionnés au [deuxième alinéa de l’article 55](/loi-78-17-pjl-490/chapitre-ix/section-1/article-55), le présent article n’est pas applicable aux traitements de données à caractère personnel dans le domaine de la santé qui sont régis par le chapitre IX. »

<ins style="background-color: #F8EDA8;">II. – L’[article 27](/loi-78-17/chapitre-iv/section-2/article-27) de la loi n° 78-17 du 6 janvier 1978 précitée est ainsi rédigé :

<ins style="background-color: #F8EDA8;">« _Art. 27_. – Sont autorisés par décret en Conseil d’État, pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés, les traitements de données à caractère personnel mis en  œuvre pour le compte de l’État, agissant dans l’exercice de ses prérogatives de puissance publique, qui portent sur des données génétiques ou sur des données biométriques nécessaires à l’authentification ou au contrôle de l’identité des personnes. »

<ins style="background-color: #F8EDA8;">III. – Les articles [23](/loi-78-17/chapitre-iv/section-1/article-23) à [25](/loi-78-17/chapitre-iv/section-2/article-25) de la loi n° 78-17 du 6 janvier 1978 précitée sont abrogés.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 8](/loi-78-17-pjl-592/chapitre-ii/section-2/article-8)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-iv/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

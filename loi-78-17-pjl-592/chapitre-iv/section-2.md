<!-- TITLE: Section 2 -->
<!-- SUBTITLE: Autorisation. -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-iv/section-1/article-24" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-iv/section-2/article-25" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 25](/loi-78-17-pjl-592/chapitre-iv/section-2/article-25)
* [Article 26](/loi-78-17-pjl-592/chapitre-iv/section-2/article-26)
* [Article 27](/loi-78-17-pjl-592/chapitre-iv/section-2/article-27)
* [Article 28](/loi-78-17-pjl-592/chapitre-iv/section-2/article-28)
* [Article 29](/loi-78-17-pjl-592/chapitre-iv/section-2/article-29)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-iv/section-1/article-24" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-iv/section-2/article-25" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

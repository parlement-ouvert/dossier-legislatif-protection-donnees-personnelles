<!-- TITLE: Chapitre IX -->
<!-- SUBTITLE: Traitements de données à caractère personnel  à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé. -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-viii/article-52" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-ix/section-1/article-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* <ins style="background-color: #F8EDA8;">[Section 1 — Dispositions générales.](/loi-78-17-pjl-592/chapitre-ix/section-1)

  * <ins style="background-color: #F8EDA8;">[Article 53](/loi-78-17-pjl-592/chapitre-ix/section-1/article-53)
  * <ins style="background-color: #F8EDA8;">[Article 54](/loi-78-17-pjl-592/chapitre-ix/section-1/article-54)
  * <ins style="background-color: #F8EDA8;">[Article 55](/loi-78-17-pjl-592/chapitre-ix/section-1/article-55)
  * <ins style="background-color: #F8EDA8;">[Article 56](/loi-78-17-pjl-592/chapitre-ix/section-1/article-56)
  * <ins style="background-color: #F8EDA8;">[Article 57](/loi-78-17-pjl-592/chapitre-ix/section-1/article-57)
  * <ins style="background-color: #F8EDA8;">[Article 58](/loi-78-17-pjl-592/chapitre-ix/section-1/article-58)
  * <ins style="background-color: #F8EDA8;">[Article 59](/loi-78-17-pjl-592/chapitre-ix/section-1/article-59)
  * <ins style="background-color: #F8EDA8;">[Article 60](/loi-78-17-pjl-592/chapitre-ix/section-1/article-60)

* <ins style="background-color: #F8EDA8;">[Section 2 —  Dispositions particulières aux traitements à des fins de recherche, d’étude ou d’évaluation dans le domaine de la santé.](/loi-78-17-pjl-592/chapitre-ix/section-2)

  * <ins style="background-color: #F8EDA8;">[Article 61](/loi-78-17-pjl-592/chapitre-ix/section-2/article-61)
  * <ins style="background-color: #F8EDA8;">[Article 62](/loi-78-17-pjl-592/chapitre-ix/section-2/article-62)
  * <ins style="background-color: #F8EDA8;">[Article 62-1](/loi-78-17-pjl-592/chapitre-ix/section-2/article-62-1)
  * <ins style="background-color: #F8EDA8;">[Article 63](/loi-78-17-pjl-592/chapitre-ix/section-2/article-63)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 8](/loi-78-17-pjl-592/chapitre-ii/section-2/article-8)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-viii/article-52" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-ix/section-1/article-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

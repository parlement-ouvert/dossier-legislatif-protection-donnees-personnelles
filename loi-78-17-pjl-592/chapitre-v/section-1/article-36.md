<!-- TITLE: Article 36 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-v/section-1/article-35" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-v/section-1/article-37" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 62](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205080&dateTexte=20161008&categorieLien=id#LEGIARTI000033205080)

----

Les données à caractère personnel ne peuvent être conservées au-delà de la durée prévue au 5° de l'article 6 qu'en vue d'être traitées à des fins <del style="background-color: #FFAEAE;">historiques, statistiques ou scientifiques</del> <ins style="background-color: #F8EDA8; text-decoration: none;">archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique ou à des fins statistiques</ins> ; le choix des données ainsi conservées est opéré dans les conditions prévues à l'article [L. 212-3 du code du patrimoine](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074236&idArticle=LEGIARTI000006845568&dateTexte=&categorieLien=cid).

<del style="background-color: #FFAEAE;">Les traitements dont la finalité se limite à assurer la conservation à long terme de documents d'archives dans le cadre du livre II du même code sont dispensés des formalités préalables à la mise en oeuvre des traitements prévues au [chapitre IV](/loi-78-17/chapitre-iv-abroge) de la présente loi.</del>

<del style="background-color: #FFAEAE;">Il peut être procédé à un traitement ayant des finalités autres que celles mentionnées au premier alinéa :</del>

<del style="background-color: #FFAEAE;">-soit avec l'accord exprès de la personne concernée ou en vertu de ses directives, formulées dans les conditions définies à l'[article 40-1](/loi-78-17/chapitre-v/section-2/article-40-1) ;</del>

<del style="background-color: #FFAEAE;">-soit avec l'autorisation de la Commission nationale de l'informatique et des libertés ;</del>

<del style="background-color: #FFAEAE;">-soit dans les conditions prévues au [8° du II et au IV de l'article 8](/loi-78-17/chapitre-ii/section-2/article-8) s'agissant de données mentionnées au I de ce même article.</del>

<ins style="background-color: #F8EDA8; text-decoration: none;">Lorsque les traitements de données à caractère personnel sont mis en œuvre par les services publics d’archives à des fins archivistiques dans l’intérêt public conformément à l’[article L. 211-2 du code du patrimoine](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074236&idArticle=LEGIARTI000006845560), les droits prévus aux articles [15](/reglement-2016-679/chapitre-iii/section-2/article-15), [16](/reglement-2016-679/chapitre-iii/section-3/article-16) et [18 à 21](/reglement-2016-679/chapitre-iii) du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité ne s’appliquent pas dans la mesure où ces droits rendent impossible ou entravent sérieusement la réalisation des finalités spécifiques et où de telles dérogations sont nécessaires pour atteindre ces finalités. Les conditions et garanties appropriées prévues à l’[article 89](/reglement-2016-679/chapitre-ix/article-89) du même règlement sont déterminées par le code du patrimoine et les autres dispositions législatives et réglementaires applicables aux archives publiques. Elles sont également assurées par le respect des normes conformes à l’état de l’art en matière d’archivage électronique.</ins>

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 70-7](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-7)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-v/section-1/article-35" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-v/section-1/article-37" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 49 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-vii-bis" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-vii-bis/article-49-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

----

_Art. 49. –_ Dans les conditions prévues aux [articles 60 à 67 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vii) du Parlement européen et du Conseil du 27 avril 2016 précité, la Commission nationale de l’informatique et des libertés met en œuvre des procédures de coopération et d’assistance mutuelle avec les autorités de contrôle des autres États membres de l’Union européenne et réalise avec ces autorités des opérations conjointes.

La commission, le président, le bureau, la formation restreinte et les agents de la commission mettent en œuvre, chacun pour ce qui le concerne, les procédures mentionnées au premier alinéa du présent article.

----

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-vii-bis" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-vii-bis/article-49-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Chapitre XIII-->
<!-- SUBTITLE: Dispositions applicables aux traitements relevant de la directive (UE) 2016/680 du 27 avril 2016. -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-xii/article-70" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-xiii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* <ins style="background-color: #F8EDA8;">[Section 1 - Dispositions générales.](/loi-78-17-pjl-592/chapitre-xiii/section-1)

  * <ins style="background-color: #F8EDA8;">[Article 70-1](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-1)
  * <ins style="background-color: #F8EDA8;">[Article 70-2](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-2)
  * <ins style="background-color: #F8EDA8;">[Article 70-3](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-3)
  * <ins style="background-color: #F8EDA8;">[Article 70-4](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-4)
  * <ins style="background-color: #F8EDA8;">[Article 70-5](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-5)
  * <ins style="background-color: #F8EDA8;">[Article 70-6](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-6)
  * <ins style="background-color: #F8EDA8;">[Article 70-7](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-7)
  * <ins style="background-color: #F8EDA8;">[Article 70-8](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-8)
  * <ins style="background-color: #F8EDA8;">[Article 70-9](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-9)
  * <ins style="background-color: #F8EDA8;">[Article 70-10](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-10)

* <ins style="background-color: #F8EDA8;">[Section 2 — Obligations incombant aux autorités compétentes et aux responsables de traitements.](/loi-78-17-pjl-592/chapitre-xiii/section-2)

  * <ins style="background-color: #F8EDA8;">[Article 70-11](/loi-78-17-pjl-592/chapitre-xiii/section-2/article-70-11)
  * <ins style="background-color: #F8EDA8;">[Article 70-12](/loi-78-17-pjl-592/chapitre-xiii/section-2/article-70-12)
  * <ins style="background-color: #F8EDA8;">[Article 70-13](/loi-78-17-pjl-592/chapitre-xiii/section-2/article-70-13)
  * <ins style="background-color: #F8EDA8;">[Article 70-14](/loi-78-17-pjl-592/chapitre-xiii/section-2/article-70-14)
  * <ins style="background-color: #F8EDA8;">[Article 70-15](/loi-78-17-pjl-592/chapitre-xiii/section-2/article-70-15)
  * <ins style="background-color: #F8EDA8;">[Article 70-16](/loi-78-17-pjl-592/chapitre-xiii/section-2/article-70-16)
  * <ins style="background-color: #F8EDA8;">[Article 70-17](/loi-78-17-pjl-592/chapitre-xiii/section-2/article-70-17)

* <ins style="background-color: #F8EDA8;">[Section 3 — Droits de la personne concernée.](/loi-78-17-pjl-592/chapitre-xiii/section-3)

  * <ins style="background-color: #F8EDA8;">[Article 70-18](/loi-78-17-pjl-592/chapitre-xiii/section-3/article-70-18)
  * <ins style="background-color: #F8EDA8;">[Article 70-19](/loi-78-17-pjl-592/chapitre-xiii/section-3/article-70-19)
  * <ins style="background-color: #F8EDA8;">[Article 70-20](/loi-78-17-pjl-592/chapitre-xiii/section-3/article-70-20)
  * <ins style="background-color: #F8EDA8;">[Article 70-21](/loi-78-17-pjl-592/chapitre-xiii/section-3/article-70-21)
  * <ins style="background-color: #F8EDA8;">[Article 70-22](/loi-78-17-pjl-592/chapitre-xiii/section-3/article-70-22)
  * <ins style="background-color: #F8EDA8;">[Article 70-23](/loi-78-17-pjl-592/chapitre-xiii/section-3/article-70-23)
  * <ins style="background-color: #F8EDA8;">[Article 70-24](/loi-78-17-pjl-592/chapitre-xiii/section-3/article-70-24)

* <ins style="background-color: #F8EDA8;">[Section 4 — Transferts de données à caractère personnel vers des États n’appartenant pas à l’Union européenne ou vers des destinataires établis dans des États non membres de l’Union européenne.](/loi-78-17-pjl-592/chapitre-xiii/section-4)

  * <ins style="background-color: #F8EDA8;">[Article 70-25](/loi-78-17-pjl-592/chapitre-xiii/section-4/article-70-25)
  * <ins style="background-color: #F8EDA8;">[Article 70-26](/loi-78-17-pjl-592/chapitre-xiii/section-4/article-70-26)
  * <ins style="background-color: #F8EDA8;">[Article 70-27](/loi-78-17-pjl-592/chapitre-xiii/section-4/article-70-27)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 35](/loi-78-17-pjl-592/chapitre-v/section-1/article-35)
  * [Article 43 quater](/loi-78-17-pjl-592/section-2/article-43-quater)
  * [Article 46](/loi-78-17-pjl-592/chapitre-vii/article-46)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-xii/article-70" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-xiii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>
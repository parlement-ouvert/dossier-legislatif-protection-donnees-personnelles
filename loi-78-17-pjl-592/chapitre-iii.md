<!-- TITLE: Chapitre III -->
<!-- SUBTITLE: La Commission nationale de l'informatique et des libertés. -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-ii-abroge" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-iii/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 11](/loi-78-17-pjl-592/chapitre-iii/article-11)
* [Article 12 (abrogé)](/loi-78-17-pjl-592/chapitre-iii/article-12-abroge)
* [Article 13](/loi-78-17-pjl-592/chapitre-iii/article-13)
* [Article 14 (abrogé)](/loi-78-17-pjl-592/chapitre-iii/article-14-abroge)
* [Article 15](/loi-78-17-pjl-592/chapitre-iii/article-15)
* [Article 15 bis](/loi-78-17-pjl-592/chapitre-iii/article-15-bis)
* [Article 16](/loi-78-17-pjl-592/chapitre-iii/article-16)
* [Article 17](/loi-78-17-pjl-592/chapitre-iii/article-17)
* [Article 18](/loi-78-17-pjl-592/chapitre-iii/article-18)
* [Article 19](/loi-78-17-pjl-592/chapitre-iii/article-19)
* [Article 20](/loi-78-17-pjl-592/chapitre-iii/article-20)
* [Article 21](/loi-78-17-pjl-592/chapitre-iii/article-21)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-ii-abroge" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-iii/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

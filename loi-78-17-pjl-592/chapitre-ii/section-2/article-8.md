<!-- TITLE: Article 8 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-ii/section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-ii/section-2/article-9" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 37](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205052&dateTexte=20161008&categorieLien=id#LEGIARTI000033205052)

----

<del style="background-color: #FFAEAE;">I. - Il est interdit de collecter ou de traiter des données à caractère personnel qui font apparaître, directement ou indirectement, les origines raciales ou ethniques, les opinions politiques, philosophiques ou religieuses ou l'appartenance syndicale des personnes, ou qui sont relatives à la santé ou à la vie sexuelle de celles-ci.</del>

<ins style="background-color: #F8EDA8; text-decoration: none;">I. – Il est interdit de traiter des données à caractère personnel, qui révèlent la prétendue origine raciale ou l’origine ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l’appartenance syndicale d’une personne physique, ou de traiter des données génétiques, des données biométriques aux fins d’identifier une personne physique de manière unique, des données concernant la santé ou des données concernant vie sexuelle ou l’orientation sexuelle d’une personne physique.</ins>

II. - Dans la mesure où la finalité du traitement l'exige pour certaines catégories de données, ne sont pas soumis à l'interdiction prévue au I :

1° Les traitements pour lesquels la personne concernée a donné son consentement exprès, sauf dans le cas où la loi prévoit que l'interdiction visée au I ne peut être levée par le consentement de la personne concernée ;

2° Les traitements nécessaires à la sauvegarde de la vie humaine, mais auxquels la personne concernée ne peut donner son consentement par suite d'une incapacité juridique ou d'une impossibilité matérielle ;

3° Les traitements mis en oeuvre par une association ou tout autre organisme à but non lucratif et à caractère religieux, philosophique, politique ou syndical :

\- pour les seules données mentionnées au I correspondant à l'objet de ladite association ou dudit organisme ;

\- sous réserve qu'ils ne concernent que les membres de cette association ou de cet organisme et, le cas échéant, les personnes qui entretiennent avec celui-ci des contacts réguliers dans le cadre de son activité ;

\- et qu'ils ne portent que sur des données non communiquées à des tiers, à moins que les personnes concernées n'y consentent expressément ;

4° Les traitements portant sur des données à caractère personnel rendues publiques par la personne concernée ;

5° Les traitements nécessaires à la constatation, à l'exercice ou à la défense d'un droit en justice ;

6° Les traitements nécessaires aux fins de la médecine préventive, des diagnostics médicaux, de l'administration de soins ou de traitements, ou de la gestion de services de santé et mis en oeuvre par un membre d'une profession de santé, ou par une autre personne à laquelle s'impose en raison de ses fonctions l'obligation de secret professionnel prévue par [l'article 226-13](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417944&dateTexte=&categorieLien=cid) du code pénal ;

7° Les traitements statistiques réalisés par l'Institut national de la statistique et des études économiques ou l'un des services statistiques ministériels dans le respect de la [loi n° 51-711 du 7 juin 1951](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000888573&categorieLien=cid) sur l'obligation, la coordination et le secret en matière de statistiques, après avis du Conseil national de l'information statistique <del style="background-color: #FFAEAE;">et dans les conditions prévues à l'article 25 de la présente loi</del> ;

<del style="background-color: #FFAEAE;">8° Les traitements nécessaires à la recherche, aux études et évaluations dans le domaine de la santé selon les modalités prévues au chapitre IX.</del>

<ins style="background-color: #F8EDA8; text-decoration: none;">8° Les traitements comportant des données concernant la santé justifiés par l’intérêt public et conformes aux dispositions du [chapitre IX](/loi-78-17-pjl-592/chapitre-ix) de la présente loi.</ins>

<ins style="background-color: #F8EDA8; text-decoration: none;">9° Les traitements conformes aux règlements types mentionnés au [b du 2° du I de l’article 11](/loi-78-17-pjl-592/chapitre-iii/article-11) mis en œuvre par les employeurs ou les administrations qui portent sur des données biométriques nécessaires au contrôle de l’accès aux lieux de travail ainsi qu’aux appareils et aux applications utilisés dans le cadre des missions confiées aux salariés ou aux agents ;</ins>

<ins style="background-color: #F8EDA8; text-decoration: none;">10° (nouveau) Les traitements portant sur la réutilisation des informations publiques figurant dans les jugements et décisions mentionnés à l’[article L. 10 du code de justice administrative](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006449164&cidTexte=LEGITEXT000006070933) et à l’[article L. 111-13 du code de l’organisation judiciaire](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071164&idArticle=LEGIARTI000033206155), sous réserve que ces traitements n’aient ni pour objet ni pour effet de permettre la réidentification des personnes concernées.</ins>

<del style="background-color: #FFAEAE;">III. - Si les données à caractère personnel visées au I sont appelées à faire l'objet à bref délai d'un procédé d'anonymisation préalablement reconnu conforme aux dispositions de la présente loi par la Commission nationale de l'informatique et des libertés, celle-ci peut autoriser, compte tenu de leur finalité, certaines catégories de traitements selon les modalités prévues à l'[article 25](/loi-78-17-pjl-592/chapitre-iv/section-2/article-25). Les dispositions du [chapitre IX](/loi-78-17-pjl-592/chapitre-ix) ne sont pas applicables.</del>

<ins style="background-color: #F8EDA8; text-decoration: none;">III. – N’entrent pas dans le champ de l’interdiction prévue au I les données à caractère personnel mentionnées au même I qui sont appelées à faire l’objet, à bref délai, d’un procédé d’anonymisation préalablement reconnu conforme aux dispositions de la présente loi par la Commission nationale de l’informatique et des libertés.</ins>

<del style="background-color: #FFAEAE;">IV. - De même, ne sont pas soumis à l'interdiction prévue au I les traitements, automatisés ou non, justifiés par l'intérêt public et soit autorisés dans les conditions prévues au [I de l'article 25](/loi-78-17-pjl-592/chapitre-iv/section-2/article-25) ou au [II de l'article 26](/loi-78-17-pjl-592/chapitre-iv/section-2/article-26), soit déclarés dans les conditions prévues au [V de l'article 22](/loi-78-17-pjl-592/chapitre-iv/article-22).</del>

<ins style="background-color: #F8EDA8; text-decoration: none;">IV. – De même, ne sont pas soumis à l’interdiction prévue au I les traitements, automatisés ou non, justifiés par l’intérêt public et autorisés dans les conditions prévues au [II de l’article 26](/loi-78-17-pjl-592/chapitre-iv/section-2/article-26).</ins>

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 10](/loi-78-17-pjl-592/chapitre-ii/section-2/article-10)
  * [Article 70-3](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-3)
  * [Article 70-4](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-4)
  * [Article 70-9](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-9)
  * [Article 70-13](/loi-78-17-pjl-592/chapitre-xiii/section-2/article-70-13)
  * [Article 70-14](/loi-78-17-pjl-592/chapitre-xiii/section-2/article-70-14)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-ii/section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-ii/section-2/article-9" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

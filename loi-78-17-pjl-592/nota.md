<!-- TITLE: Nota -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/signataires" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <span> </span> </div>

Travaux préparatoires. Assemblée nationale :

Projet de loi (n° 2516 et propositions de loi (n° 1004 et 3092) ;

Rapport de M. Foyer, au nom de la commission des lois (n° 3125) ;

Discussion les 4 et 5 octobre 1977 ;

Adoption le 5 octobre 1977. Sénat :

Projet de loi, adopté par l'Assemblée nationale, n° 5 (1977-1978) ;

Rapport de M. Jacques Thyraud, au nom de la commission des lois, n° 72 (1977-1978) ;

Discussion et adoption le 17 novembre 1977. Assemblée nationale :

Projet de loi, modifié par le Sénat (n° 3226) ;

Rapport de M. Foyer, au nom de la commission des lois (n° 3352) ;

Discussion et adoption le 16 décembre 1977. Sénat :

Projet de loi, modifié par l'Assemblée nationale, n° 195 (1977-1978) ;

Rapport de M. Jacques Thyraud, au nom de la commission des lois, n° 199 (1977-1978) ;

Discussion et adoption le 19 décembre 1977. Assemblée nationale :

Rapport de M. Foyer, au nom de la commission mixte paritaire (n° 3432) ;

Discussion et adoption le 21 décembre 1977. Sénat :

Rapport de M. Thyraud, au nom de la commission mixte paritaire, n° 232 (1977-1978) ;

Discussion et rejet le 21 décembre 1977. Assemblée nationale :

Projet de loi, modifié par le Sénat (n° 3384) ;

Discussion et adoption le 21 décembre 1977. Sénat :

Projet de loi, adopté par l'Assemblée nationale, n° 240 (1977-1978) ;

Discussion et adoption le 21 décembre 1977.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/signataires" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <span> </span> </div>

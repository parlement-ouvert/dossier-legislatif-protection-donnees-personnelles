<!-- TITLE: Article 70-1 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-xiii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<ins style="background-color: #F8EDA8; text-decoration:none;">Les dispositions du présent chapitre s’appliquent, le cas échéant par dérogation aux autres dispositions de la présente loi, aux traitements des données à caractère personnel mis en œuvre, à des fins de prévention et de détection des infractions pénales, d’enquêtes et de poursuites en la matière ou d’exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces, par toute autorité publique compétente ou tout autre organisme ou entité à qui a été confié, à ces mêmes fins, l’exercice de l’autorité publique et des prérogatives de puissance publique, ci-après dénommés autorité compétente.

<ins style="background-color: #F8EDA8;  text-decoration:none;">Ces traitements ne sont licites que si et dans la mesure où ils sont nécessaires à l’exécution d’une mission effectuée, pour les finalités énoncées au premier alinéa, par une autorité compétente au sens du même premier alinéa, et où sont respectées les dispositions des articles [70-3](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-3) et [70-4](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-4). Le traitement doit notamment assurer la proportionnalité de la durée de conservation des données personnelles, compte tenu de l’objet du fichier et de la nature ou de la gravité des infractions concernées.

<ins style="background-color: #F8EDA8;  text-decoration:none;">Pour l’application du présent chapitre, lorsque les notions utilisées ne sont pas définies au chapitre Ier de la présente loi, les définitions de [l’article 4 du règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité sont applicables.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 70-3](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-3)
  * [Article 70-5](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-5)
  * [Article 70-6](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-6)
  * [Article 70-25](/loi-78-17-pjl-592/chapitre-xiii/section-4/article-70-25)
  * [Article 70-26](/loi-78-17-pjl-592/chapitre-xiii/section-4/article-70-26)
  * [Article 70-27](/loi-78-17-pjl-592/chapitre-xiii/section-4/article-70-27)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-xiii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>
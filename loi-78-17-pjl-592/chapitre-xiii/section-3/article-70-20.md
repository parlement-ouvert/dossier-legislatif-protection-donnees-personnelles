<!-- TITLE: Article 70-20 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-xiii/section-3/article-70-19" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-xiii/section-3/article-70-21" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<ins style="background-color: #F8EDA8; text-decoration:none;">I. – La personne concernée a le droit d’obtenir du responsable du traitement :

<ins style="background-color: #F8EDA8; text-decoration:none;">1° Que soit rectifiées dans les meilleurs délais des données à caractère personnel la concernant qui sont inexactes ;

<ins style="background-color: #F8EDA8; text-decoration:none;">2° Que soient complétées des données à caractère personnel la concernant incomplètes, y compris en fournissant à cet effet une déclaration complémentaire ;

<ins style="background-color: #F8EDA8; text-decoration:none;">3° Que soit effacées dans les meilleurs délais des données à caractère personnel la concernant lorsque le traitement est réalisé en violation des dispositions de la présente loi ou lorsque ces données doivent être effacées pour respecter une obligation légale à laquelle est soumis le responsable du traitement.

<ins style="background-color: #F8EDA8; text-decoration:none;">II. – Lorsque l’intéressé en fait la demande, le responsable du traitement doit justifier qu’il a procédé aux opérations exigées en vertu du I.

<ins style="background-color: #F8EDA8; text-decoration:none;">III. – Au lieu de procéder à l’effacement, le responsable du traitement limite le traitement lorsque :

<ins style="background-color: #F8EDA8; text-decoration:none;">1° Soit l’exactitude des données à caractère personnel est contestée par la personne concernée et il ne peut être déterminé si les données sont exactes ou non ;

<ins style="background-color: #F8EDA8; text-decoration:none;">2° Soit les données à caractère personnel doivent être conservées à des fins probatoires.

<ins style="background-color: #F8EDA8; text-decoration:none;">Lorsque le traitement est limité en vertu du 1°, le responsable du traitement informe la personne concernée avant de lever la limitation du traitement.

<ins style="background-color: #F8EDA8; text-decoration:none;">IV. – Le responsable du traitement informe la personne concernée de tout refus de rectifier ou d’effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs du refus.

<ins style="background-color: #F8EDA8; text-decoration:none;">V. – Le responsable du traitement communique la rectification des données à caractère personnel inexactes à l’autorité compétente dont elles proviennent.

<ins style="background-color: #F8EDA8; text-decoration:none;">VI. – Lorsque des données à caractère personnel ont été rectifiées ou effacées ou que le traitement a été limité au titre des I, II et III, le responsable du traitement le notifie aux destinataires afin que ceux-ci rectifient ou effacent les données ou limitent le traitement des données sous leur responsabilité.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 70-11](/loi-78-17-pjl-592/chapitre-xiii/section-2/article-70-11)
  * [Article 70-21](/loi-78-17-pjl-592/chapitre-xiii/section-3/article-70-21)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-xiii/section-3/article-70-19" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-xiii/section-3/article-70-21" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>
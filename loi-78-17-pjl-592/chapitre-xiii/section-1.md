<!-- TITLE: Section 1 -->
<!-- SUBTITLE: Dispositions générales. -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-xiii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* <ins style="background-color: #F8EDA8;">[Article 70-1](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-1)
* <ins style="background-color: #F8EDA8;">[Article 70-2](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-2)
* <ins style="background-color: #F8EDA8;">[Article 70-3](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-3)
* <ins style="background-color: #F8EDA8;">[Article 70-4](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-4)
* <ins style="background-color: #F8EDA8;">[Article 70-5](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-5)
* <ins style="background-color: #F8EDA8;">[Article 70-6](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-6)
* <ins style="background-color: #F8EDA8;">[Article 70-7](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-7)
* <ins style="background-color: #F8EDA8;">[Article 70-8](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-8)
* <ins style="background-color: #F8EDA8;">[Article 70-9](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-9)
* <ins style="background-color: #F8EDA8;">[Article 70-10](/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-10)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-592/chapitre-xiii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 592</span> <a href="/loi-78-17-pjl-592/chapitre-xiii/section-1/article-70-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>
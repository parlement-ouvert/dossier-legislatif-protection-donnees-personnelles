<!-- TITLE: Amendements en commission des lois de l'Assemblée (première lecture) -->

* [Index des amendements par auteur](/amendements-1-assemblee-commission/index-amendements-par-auteur)

* [TITRE Ier](/amendements-1-assemblee-commission/titre-ier)

  * [Amendement n° 88 — Adopté](/amendements-1-assemblee-commission/titre-ier/amendement-88)

* [Avant l'article PREMIER](/amendements-1-assemblee-commission/avant-l-article-premier)

  * [Amendement n° 5 — Rejeté](/amendements-1-assemblee-commission/avant-l-article-premier/amendement-5)

* [Article PREMIER](/amendements-1-assemblee-commission/article-premier)

  * [Amendement n° 13 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-13)
  * [Amendement n° 239 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-239)
  * [Amendement n° 240 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-240)
  * [Amendement n° 21 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-21)
  * [Amendement n° 89 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-89)
  * [Amendement n° 90 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-90)
  * [Amendement n° 56 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-56)
  * [Amendement n° 241 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-241)
  * [Amendement n° 242 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-242)
  * [Amendement n° 243 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-243)
  * [Amendement n° 23 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-23)
  * [Amendement n° 244 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-244)
  * [Amendement n° 22 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-22)
  * [Amendement n° 245 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-245)
  * [Amendement n° 91 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-91)
  * [Amendement n° 92 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-92)
  * [Amendement n° 246 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-246)
  * [Amendement n° 93 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-93)
  * [Amendement n° 94 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-94)
  * [Amendement n° 96 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-96)
  * [Amendement n° 264 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-264)
  * [Amendement n° 24 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-24)
  * [Amendement n° 57 — Retiré](/amendements-1-assemblee-commission/article-premier/amendement-57)
  * [Amendement n° 65 — Retiré](/amendements-1-assemblee-commission/article-premier/amendement-65)
  * [Amendement n° 73 — Retiré](/amendements-1-assemblee-commission/article-premier/amendement-73)

* [Après l'article PREMIER](/amendements-1-assemblee-commission/apres-l-article-premier)

  * [Amendement n° 25 — Rejeté](/amendements-1-assemblee-commission/apres-l-article-premier/amendement-25)
  * [Amendement n° 247 — Adopté](/amendements-1-assemblee-commission/apres-l-article-premier/amendement-247)

* [Article 2](/amendements-1-assemblee-commission/article-2)

  * [Amendement n° 248 — Adopté](/amendements-1-assemblee-commission/article-2/amendement-248)
  * [Amendement n° 18 — Adopté](/amendements-1-assemblee-commission/article-2/amendement-18)
  * [Amendement n° 60 — Adopté](/amendements-1-assemblee-commission/article-2/amendement-60)
  * [Amendement n° 66 — Tombé](/amendements-1-assemblee-commission/article-2/amendement-66)
  * [Amendement n° 71 — Tombé](/amendements-1-assemblee-commission/article-2/amendement-71)
  * [Amendement n° 75 — Tombé](/amendements-1-assemblee-commission/article-2/amendement-75)
  * [Amendement n° 26 — Tombé](/amendements-1-assemblee-commission/article-2/amendement-26)
  * [Amendement n° 27 — Tombé](/amendements-1-assemblee-commission/article-2/amendement-27)

* [Après l'article 2](/amendements-1-assemblee-commission/apres-l-article-2)

  * [Amendement n° 249 — Adopté](/amendements-1-assemblee-commission/apres-l-article-2/amendement-249)

* [Article 3](/amendements-1-assemblee-commission/article-3)

  * [Amendement n° 97 — Adopté](/amendements-1-assemblee-commission/article-3/amendement-97)

* [Article 4](/amendements-1-assemblee-commission/article-4)

  * [Amendement n° 250 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-250)
  * [Amendement n° 98 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-98)
  * [Amendement n° 64 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-64)
  * [Amendement n° 76 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-76)
  * [Amendement n° 28 — Retiré](/amendements-1-assemblee-commission/article-4/amendement-28)
  * [Amendement n° 77 — Rejeté](/amendements-1-assemblee-commission/article-4/amendement-77)
  * [Amendement n° 58 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-58)
  * [Amendement n° 63 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-63)
  * [Amendement n° 72 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-72)
  * [Amendement n° 78 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-78)
  * [Amendement n° 62 — Rejeté](/amendements-1-assemblee-commission/article-4/amendement-62)
  * [Amendement n° 79 — Rejeté](/amendements-1-assemblee-commission/article-4/amendement-79)
  * [Amendement n° 61 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-61)
  * [Amendement n° 80 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-80)
  * [Amendement n° 100 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-100)

* [Article 5](/amendements-1-assemblee-commission/article-5)

  * [Amendement n° 252 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-252)
  * [Amendement n° 101 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-101)
  * [Amendement n° 102 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-102)
  * [Amendement n° 103 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-103)
  * [Amendement n° 29 — Rejeté](/amendements-1-assemblee-commission/article-5/amendement-29)
  * [Amendement n° 104 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-104)
  * [Amendement n° 105 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-105)
  * [Amendement n° 106 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-106)
  * [Amendement n° 108 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-108)
  * [Amendement n° 109 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-109)
  * [Amendement n° 110 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-110)
  * [Amendement n° 112 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-112)
  * [Amendement n° 113 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-113)
  * [Amendement n° 114 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-114)
  * [Amendement n° 115 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-115)
  * [Amendement n° 31 — Rejeté](/amendements-1-assemblee-commission/article-5/amendement-31)
  * [Amendement n° 253 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-253)
  * [Amendement n° 117 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-117)
  * [Amendement n° 118 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-118)

* [Article 6](/amendements-1-assemblee-commission/article-6)

  * [Amendement n° 119 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-119)
  * [Amendement n° 32 — Non soutenu](/amendements-1-assemblee-commission/article-6/amendement-32)
  * [Amendement n° 120 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-120)
  * [Amendement n° 261 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-261)
  * [Amendement n° 121 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-121)
  * [Amendement n° 82 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-82)
  * [Amendement n° 263 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-263)
  * [Amendement n° 86 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-86)
  * [Amendement n° 43 — Non soutenu](/amendements-1-assemblee-commission/article-6/amendement-43)
  * [Amendement n° 8 — Non soutenu](/amendements-1-assemblee-commission/article-6/amendement-8)
  * [Amendement n° 15 — Non soutenu](/amendements-1-assemblee-commission/article-6/amendement-15)
  * [Amendement n° 122 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-122)
  * [Amendement n° 123 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-123)
  * [Amendement n° 124 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-124)
  * [Amendement n° 127 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-127)
  * [Amendement n° 128 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-128)
  * [Amendement n° 129 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-129)
  * [Amendement n° 254 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-254)
  * [Amendement n° 130 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-130)
  * [Amendement n° 131 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-131)
  * [Amendement n° 132 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-132)

* [Après l'article 6](/amendements-1-assemblee-commission/apres-l-article-6)

  * [Amendement n° 30 — Non soutenu](/amendements-1-assemblee-commission/apres-l-article-6/amendement-30)
  * [Amendement n° 33 — Non soutenu](/amendements-1-assemblee-commission/apres-l-article-6/amendement-33)

* [Article 7](/amendements-1-assemblee-commission/article-7)

  * [Amendement n° 133 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-133)
  * [Amendement n° 54 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-54)
  * [Amendement n° 17 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-17)
  * [Amendement n° 260 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-260)
  * [Amendement n° 255 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-255)
  * [Amendement n° 7 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-7)
  * [Amendement n° 48 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-48)
  * [Amendement n° 134 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-134)
  * [Amendement n° 16 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-16)
  * [Amendement n° 135 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-135)
  * [Amendement n° 50 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-50)
  * [Amendement n° 34 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-34)

* [Après l'article 7](/amendements-1-assemblee-commission/apres-l-article-7)

  * [Amendement n° 47 — Non soutenu](/amendements-1-assemblee-commission/apres-l-article-7/amendement-47)

* [Article 9](/amendements-1-assemblee-commission/article-9)

  * [Amendement n° 51 — Non soutenu](/amendements-1-assemblee-commission/article-9/amendement-51)
  * [Amendement n° 136 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-136)
  * [Amendement n° 137 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-137)
  * [Amendement n° 138 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-138)
  * [Amendement n° 139 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-139)
  * [Amendement n° 140 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-140)
  * [Amendement n° 226 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-226)
  * [Amendement n° 207 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-207)

* [Article 10](/amendements-1-assemblee-commission/article-10)

  * [Amendement n° 227 — Adopté](/amendements-1-assemblee-commission/article-10/amendement-227)

* [Article 11](/amendements-1-assemblee-commission/article-11)

  * [Amendement n° 3 — Non soutenu](/amendements-1-assemblee-commission/article-11/amendement-3)
  * [Amendement n° 52 — Non soutenu](/amendements-1-assemblee-commission/article-11/amendement-52)
  * [Amendement n° 35 — Non soutenu](/amendements-1-assemblee-commission/article-11/amendement-35)
  * [Amendement n° 228 — Adopté](/amendements-1-assemblee-commission/article-11/amendement-228)
  * [Amendement n° 87 — Rejeté](/amendements-1-assemblee-commission/article-11/amendement-87)
  * [Amendement n° 141 — Adopté](/amendements-1-assemblee-commission/article-11/amendement-141)

* [Article 12](/amendements-1-assemblee-commission/article-12)

  * [Amendement n° 36 — Non soutenu](/amendements-1-assemblee-commission/article-12/amendement-36)

* [Article 13](/amendements-1-assemblee-commission/article-13)

  * [Amendement n° 53 — Non soutenu](/amendements-1-assemblee-commission/article-13/amendement-53)
  * [Amendement n° 20 — Non soutenu](/amendements-1-assemblee-commission/article-13/amendement-20)
  * [Amendement n° 142 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-142)
  * [Amendement n° 143 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-143)
  * [Amendement n° 144 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-144)
  * [Amendement n° 208 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-208)
  * [Amendement n° 145 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-145)
  * [Amendement n° 146 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-146)
  * [Amendement n° 147 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-147)
  * [Amendement n° 258 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-258)
  * [Amendement n° 257 — Retiré](/amendements-1-assemblee-commission/article-13/amendement-257)
  * [Amendement n° 149 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-149)
  * [Amendement n° 265 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-265)
  * [Amendement n° 256 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-256)
  * [Amendement n° 150 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-150)

* [Après l'article 13](/amendements-1-assemblee-commission/apres-l-article-13)

  * [Amendement n° 12 — Retiré](/amendements-1-assemblee-commission/apres-l-article-13/amendement-12)
  * [Amendement n° 67 — Non soutenu](/amendements-1-assemblee-commission/apres-l-article-13/amendement-67)
  * [Amendement n° 9 — Non soutenu](/amendements-1-assemblee-commission/apres-l-article-13/amendement-9)
  * [Amendement n° 46 — Non soutenu](/amendements-1-assemblee-commission/apres-l-article-13/amendement-46)

* [Avant l'article 14](/amendements-1-assemblee-commission/avant-l-article-14)

  * [Amendement n° 234 — Adopté](/amendements-1-assemblee-commission/avant-l-article-14/amendement-234)
  * [Amendement n° 10 — Non soutenu](/amendements-1-assemblee-commission/avant-l-article-14/amendement-10)
  * [Amendement n° 55 — Non soutenu](/amendements-1-assemblee-commission/avant-l-article-14/amendement-55)
  * [Amendement n° 11 — Non soutenu](/amendements-1-assemblee-commission/avant-l-article-14/amendement-11)
  * [Amendement n° 14 — Non soutenu](/amendements-1-assemblee-commission/avant-l-article-14/amendement-14)
  * [Amendement n° 4 — Non soutenu](/amendements-1-assemblee-commission/avant-l-article-14/amendement-4)

* [Article 14](/amendements-1-assemblee-commission/article-14)

  * [Amendement n° 37 — Non soutenu](/amendements-1-assemblee-commission/article-14/amendement-37)
  * [Amendement n° 235 — Adopté](/amendements-1-assemblee-commission/article-14/amendement-235)
  * [Amendement n° 237 — Adopté](/amendements-1-assemblee-commission/article-14/amendement-237)

* [Après l'article 14](/amendements-1-assemblee-commission/apres-l-article-14)

  * [Amendement n° 38 — Non soutenu](/amendements-1-assemblee-commission/apres-l-article-14/amendement-38)
  * [Amendement n° 45 — Non soutenu](/amendements-1-assemblee-commission/apres-l-article-14/amendement-45)

* [Article 15](/amendements-1-assemblee-commission/article-15)

  * [Amendement n° 2 — Non soutenu](/amendements-1-assemblee-commission/article-15/amendement-2)
  * [Amendement n° 39 — Non soutenu](/amendements-1-assemblee-commission/article-15/amendement-39)
  * [Amendement n° 210 — Adopté](/amendements-1-assemblee-commission/article-15/amendement-210)
  * [Amendement n° 40 — Non soutenu](/amendements-1-assemblee-commission/article-15/amendement-40)

* [Avant l'article 16](/amendements-1-assemblee-commission/avant-l-article-16)

  * [Amendement n° 262 — Adopté](/amendements-1-assemblee-commission/avant-l-article-16/amendement-262)
  * [Amendement n° 70 — Retiré](/amendements-1-assemblee-commission/avant-l-article-16/amendement-70)

* [Article 16](/amendements-1-assemblee-commission/article-16)

  * [Amendement n° 211 — Adopté](/amendements-1-assemblee-commission/article-16/amendement-211)
  * [Amendement n° 218 — Adopté](/amendements-1-assemblee-commission/article-16/amendement-218)
  * [Amendement n° 68 — Non soutenu](/amendements-1-assemblee-commission/article-16/amendement-68)

* [Article 17](/amendements-1-assemblee-commission/article-17)

  * [Amendement n° 212 — Adopté](/amendements-1-assemblee-commission/article-17/amendement-212)
  * [Amendement n° 229 — Adopté](/amendements-1-assemblee-commission/article-17/amendement-229)
  * [Amendement n° 219 — Adopté](/amendements-1-assemblee-commission/article-17/amendement-219)

* [Après l'article 17](/amendements-1-assemblee-commission/apres-l-article-17)

  * [Amendement n° 69 — Retiré](/amendements-1-assemblee-commission/apres-l-article-17/amendement-69)

* [Article 18](/amendements-1-assemblee-commission/article-18)

  * [Amendement n° 151 — Adopté](/amendements-1-assemblee-commission/article-18/amendement-151)
  * [Amendement n° 152 — Adopté](/amendements-1-assemblee-commission/article-18/amendement-152)

* [Article 19](/amendements-1-assemblee-commission/article-19)

  * [Amendement n° 41 — Rejeté](/amendements-1-assemblee-commission/article-19/amendement-41)
  * [Amendement n° 153 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-153)
  * [Amendement n° 49 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-49)
  * [Amendement n° 6 — Non soutenu](/amendements-1-assemblee-commission/article-19/amendement-6)
  * [Amendement n° 154 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-154)
  * [Amendement n° 156 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-156)
  * [Amendement n° 157 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-157)
  * [Amendement n° 159 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-159)
  * [Amendement n° 160 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-160)
  * [Amendement n° 161 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-161)
  * [Amendement n° 162 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-162)
  * [Amendement n° 163 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-163)
  * [Amendement n° 164 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-164)
  * [Amendement n° 165 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-165)
  * [Amendement n° 166 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-166)
  * [Amendement n° 167 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-167)
  * [Amendement n° 168 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-168)
  * [Amendement n° 169 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-169)
  * [Amendement n° 170 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-170)
  * [Amendement n° 171 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-171)
  * [Amendement n° 172 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-172)
  * [Amendement n° 173 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-173)
  * [Amendement n° 174 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-174)
  * [Amendement n° 175 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-175)
  * [Amendement n° 222 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-222)
  * [Amendement n° 176 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-176)
  * [Amendement n° 177 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-177)
  * [Amendement n° 178 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-178)
  * [Amendement n° 179 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-179)
  * [Amendement n° 180 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-180)
  * [Amendement n° 181 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-181)
  * [Amendement n° 182 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-182)
  * [Amendement n° 183 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-183)
  * [Amendement n° 184 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-184)
  * [Amendement n° 185 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-185)
  * [Amendement n° 186 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-186)
  * [Amendement n° 223 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-223)
  * [Amendement n° 224 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-224)
  * [Amendement n° 187 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-187)
  * [Amendement n° 188 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-188)
  * [Amendement n° 189 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-189)
  * [Amendement n° 209 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-209)
  * [Amendement n° 190 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-190)
  * [Amendement n° 191 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-191)
  * [Amendement n° 19 — Non soutenu](/amendements-1-assemblee-commission/article-19/amendement-19)
  * [Amendement n° 221 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-221)
  * [Amendement n° 192 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-192)
  * [Amendement n° 193 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-193)
  * [Amendement n° 194 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-194)
  * [Amendement n° 195 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-195)
  * [Amendement n° 196 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-196)
  * [Amendement n° 198 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-198)
  * [Amendement n° 199 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-199)
  * [Amendement n° 200 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-200)
  * [Amendement n° 201 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-201)

* [Article 20](/amendements-1-assemblee-commission/article-20)

  * [Amendement n° 1 — Rejeté](/amendements-1-assemblee-commission/article-20/amendement-1)
  * [Amendement n° 42 — Rejeté](/amendements-1-assemblee-commission/article-20/amendement-42)
  * [Amendement n° 214 — Adopté](/amendements-1-assemblee-commission/article-20/amendement-214)

* [Article 21](/amendements-1-assemblee-commission/article-21)

  * [Amendement n° 230 — Adopté](/amendements-1-assemblee-commission/article-21/amendement-230)
  * [Amendement n° 233 — Adopté](/amendements-1-assemblee-commission/article-21/amendement-233)
  * [Amendement n° 232 — Adopté](/amendements-1-assemblee-commission/article-21/amendement-232)
  * [Amendement n° 215 — Adopté](/amendements-1-assemblee-commission/article-21/amendement-215)
  * [Amendement n° 231 — Adopté](/amendements-1-assemblee-commission/article-21/amendement-231)

* [Article 22](/amendements-1-assemblee-commission/article-22)

  * [Amendement n° 216 — Adopté](/amendements-1-assemblee-commission/article-22/amendement-216)

* [Article 23](/amendements-1-assemblee-commission/article-23)

  * [Amendement n° 202 — Adopté](/amendements-1-assemblee-commission/article-23/amendement-202)
  * [Amendement n° 203 — Adopté](/amendements-1-assemblee-commission/article-23/amendement-203)
  * [Amendement n° 205 — Adopté](/amendements-1-assemblee-commission/article-23/amendement-205)
  * [Amendement n° 225 — Adopté](/amendements-1-assemblee-commission/article-23/amendement-225)
  * [Amendement n° 206 — Adopté](/amendements-1-assemblee-commission/article-23/amendement-206)

* [Après l'article 23](/amendements-1-assemblee-commission/apres-l-article-23)

  * [Amendement n° 238 — Adopté](/amendements-1-assemblee-commission/apres-l-article-23/amendement-238)

* [Article 24](/amendements-1-assemblee-commission/article-24)

  * [Amendement n° 217 — Adopté](/amendements-1-assemblee-commission/article-24/amendement-217)

* [Après l'article 24](/amendements-1-assemblee-commission/apres-l-article-24)

  * [Amendement n° 44 — Rejeté](/amendements-1-assemblee-commission/apres-l-article-24/amendement-44)

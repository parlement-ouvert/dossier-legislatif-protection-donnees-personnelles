<!-- TITLE: Amendement n° 21 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-premier/amendement-240" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-premier/amendement-89" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

[ARTICLE PREMIER](/pjl-490/titre-i/chapitre-i/article-1)

# Titre

Après l’alinéa 4, insérer l’alinéa suivant :

« 2° _bis_ À la seconde phrase du premier alinéa, après le mot : « exerce », sont insérés les mots : « à titre principal », et après le mot : « suivantes », sont insérés les mots : « énumérées au 1°, aux _a_ , _c_ , _d_ , _e_ et _f_ du 2° et au 4°, et à titre subsidiaire les missions énumérées aux _a_ bis et _b_ du 2° et au 3° » ; ».

# Exposé sommaire

Les textes européens Directive / Règlement 2016 ont constitué des avancées positives en matière de droits et libertés et prévoient d’harmoniser le cadre de régulation des traitements de données personnelles dans l’Union européenne et la coopération entre les Etats membres (objectivement actuellement indispensable à l’ère du numérique mondialisé sur le marché intérieur européen).

Or, sans qu’un débat public ait permis aux citoyens de se saisir de cette thématique, le Gouvernement utilise cette loi de transposition pour profondément réformer la CNIL, notamment son mode de contrôle sur les traitements de données / fichiers, ce en généralisant un régime de simple contrôle / supervision qui prévalait jusqu’alors, et en restreignant les déclarations préalables et autorisations à des domaines très limités (l’exception devient la règle).

Nous estimons qu’en libéralisant ainsi la CNIL, le Gouvernement la transforme en instance de supervision / gendarme sans capacité réelle de contrôle, puisqu’il est patent que la CNIL ne dispose pas des moyens humains et financier pour mener à bien ses missions…

Concrètement, selon les derniers documents budgétaires, la CNIL dispose de 198 ETP (dont 187 contractuels https://www.performance-publique.budget.gouv.fr/sites/performance_publique/files/farandole/ressources/2017/pap/pdf/DBGPGMPGM308.pdf), et son budget qui n’est qu’à 17 millions d’euros n’a augmenté que de +6% ce alors même que le Gouvernement en envisage une réforme importante (Pour comparaison, la FTC américaine, qui est notamment elle aussi chargée de la protection de la vie privée des consommateurs, est dotée d’un budget de plus de 300 millions de dollarss (soit presque vingt fois plus que la CNIL).

Cette logique qui consiste, sur le modèle anglo-saxon, à confier un travail de garantie des droits et libertés fondamentaux en matière numérique à des acteurs (dont beaucoup d'entreprises à seule visée de profits) dont l’intérêt n’est pas nécessairement cette protection, nous apparaît problématique. En effet, bon nombres d’acteurs risquent de quantifier et budgéter leur violation du droit (en anticipant les risques et les sanctions pouvant être prises), ce qui mènerait à ce que le non respect des données personnelles ne soit pas un principe à respecter, mais un prix éventuel à payer…

Dans cette logique, cet amendement vise à droit constant proposé par le Gouvernement à préciser que la CNIL exerce à titre principal des missions d’autorisation et d’information, et qu’elle n’exerce qu’à titre subsidiaire ses missions de “droit mou” (c’est à dire la rédaction de règlements types, notamment).

Ceci est prévu par le droit européen. Selon les termes mêmes du 5 de l’article 36 du règlement européen RGPD : “5\. Nonobstant le paragraphe 1, le droit des États membres peut exiger que les responsables du traitement consultent l'autorité de contrôle et obtiennent son autorisation préalable en ce qui concerne le traitement effectué par un responsable du traitement dans le cadre d'une mission d'intérêt public exercée par celui-ci, y compris le traitement dans le cadre de la protection sociale et de la santé publique.”

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-premier/amendement-240" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-premier/amendement-89" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

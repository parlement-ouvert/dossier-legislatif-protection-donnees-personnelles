<!-- TITLE: Amendement n° 22 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-premier/amendement-244" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-premier/amendement-245" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE PREMIER](/pjl-490/titre-i/chapitre-i/article-1)

# Titre

À la seconde phrase de l’alinéa 9, supprimer les mots :

« sauf pour les traitements mis en œuvre pour le compte de l’État, agissant dans l’exercice de ses prérogatives de puissance publique, ».

# Exposé sommaire

Dans son avis du 30 novembre 2017 ([https://www.legifrance.gouv.fr/affichCnil.do?oldAction=rechExpCnil&amp;id=CNILTEXT000036195647&amp;fastReqId=180931766&amp;fastPos=1](https://www.legifrance.gouv.fr/affichCnil.do?oldAction=rechExpCnil&amp;id=CNILTEXT000036195647&amp;fastReqId=180931766&amp;fastPos=1)) la CNIL regrettait que si les règlements types qu’elle peut élaborer (données biométriques, génétiques et de santé ou pour les données d’infraction) pouvaient concerner les traitements mis en place pour le compte de l’Etat, agissant dans l’exercice même des prérogatives de puissance publique, il existait toutefois une exception majeure...

En effet, le projet de loi dispose que pour ces données particulièrement sensibles, la CNIL peut “prévoir des mesures techniques et organisationnelles supplémentaires (...) et des garanties complémentaires en matière de traitement de données d’infraction”. Or, les “traitements mis en oeuvre pour le compte de l’Etat, agissant dans l’exercice de ses prérogatives de puissance publique” sont explicitement exclus.

Nous rejoignons ici la CNIL sur ses interrogations. En quoi est ce que l’Etat “dans ses prérogatives de puissance publique” devrait s’exempter de ces lignes directrices prévues par la CNIL? Eu égard à leur sensibilité et à l’importance de la protection du droit à la vie privée, les droits et libertés des personnes physiques (car c’est bien ici ce dont il s’agit principalement, une personne morale n’ayant ni donnée biométrique, génétique ou de santé !) doivent primer sur cette, en l’espèce, pseudo justification d’un intérêt supérieur de l’Etat.

Concrètement le Gouvernement ne pourra plus créer impunément et sans cadrage par la CNIL des fichiers avec des données personnelles aussi sensibles, tels le :

\- le fichier national automatisé des empreintes génétiques (FNAEG) ;

\- le fichier “des honnêtes gens” (Décret du 28 octobre 2016 autorise la création d'un traitement intitulé "titres électroniques sécurisés" (TES)), qui concernait en 2017 plus de 29 millions de dossiers de demande de passeports biométriques et électroniques et devrait à terme s’étendre à plus de 60 millions ;

\- le Système National des Données de Santé (SNDS), fichier géant et centralisé qui regroupe les informations de santé (feuilles de soin, consultations, hospitalisations et achat de médicaments) de plus 65 millions de Français. (décret du 28 décembre 2016).

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-premier/amendement-244" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-premier/amendement-245" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

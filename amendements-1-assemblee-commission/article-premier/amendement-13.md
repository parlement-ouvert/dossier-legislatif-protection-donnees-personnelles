<!-- TITLE: Amendement n° 13 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-premier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-premier/amendement-239" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Marietta Karamanli et les membres du groupe Nouvelle Gauche**

[ARTICLE PREMIER](/pjl-490/titre-i/chapitre-i/article-1)

# Titre

Supprimer cet article.

# Exposé sommaire

Cet amendement vise à attirer l’attention de la représentation nationale sur les risques que présente le changement de paradigme dans le domaine de la protection des données personnelles promu par le présent projet de loi. Il est à douter que le passage d’une logique d’autorisation préalable à un contrôle a posteriori puisse être à même d'assurer de manière satisfaisante la fonction de protection des données assurée par la CNIL. Cette approche est d’autant plus questionable que les instruments de droit souple introduit dans le projet de loi (labellisation, certification, lignes directrices, recommandations ou référentiels) ne figurent pas expressément dans le règlement européen et constituent un ajout de la part du Gouvernement. Il n’était nul besoin de les introduire dans la loi. Si le groupe Nouvelle Gauche n’est pas opposé per se à l’utilisation de ces instruments, leur généralisation et le fait que d’exception ils deviennent la règle pose un problème de droit à travers lequel le principe de libre circulation des données semble l’avoir emporté sur celui de protection des données.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-premier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-premier/amendement-239" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

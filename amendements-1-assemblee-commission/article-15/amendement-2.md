<!-- TITLE: Amendement n° 2 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-15/amendement-39" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Marietta Karamanli et les membres du groupe Nouvelle Gauche**

[ARTICLE 15](/pjl-490/titre-ii/chapitre-v/article-15)

# Titre

Supprimer cet article.

# Exposé sommaire

L’article 15 permet de limiter certains droits (droit à l’information, d’accès, de rectification, à l’effacement, à la portabilité, d’opposition) « lorsqu’une telle limitation respecte l’essence des libertés et droits fondamentaux » et qu’elle constitue « une mesure nécessaire et proportionnée dans une société démocratique pour garantir certains objectifs (sécurité nationale, défense nationale, sécurité publique, prévention et la détection d’infractions pénales, protection de l’indépendance de la justice et des procédures judiciaires, objectifs importants d’intérêt public général de l’Union ou d’un État membre,…) ». Ces limitations pourront être prévues dans le cadre d’actes réglementaires ! On pourra, au demeurant, longtemps s’interroger sur la notion d’« essence des libertés et droits fondamentaux" mentionnée dans l'exposé des motifs.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-15/amendement-39" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

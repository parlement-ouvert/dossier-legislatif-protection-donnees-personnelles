<!-- TITLE: Article 6 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-5/amendement-118" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-6/amendement-119" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 119 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-119)
* [Amendement n° 32 — Non soutenu](/amendements-1-assemblee-commission/article-6/amendement-32)
* [Amendement n° 120 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-120)
* [Amendement n° 261 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-261)
* [Amendement n° 121 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-121)
* [Amendement n° 82 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-82)
* [Amendement n° 263 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-263)
* [Amendement n° 86 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-86)
* [Amendement n° 43 — Non soutenu](/amendements-1-assemblee-commission/article-6/amendement-43)
* [Amendement n° 8 — Non soutenu](/amendements-1-assemblee-commission/article-6/amendement-8)
* [Amendement n° 15 — Non soutenu](/amendements-1-assemblee-commission/article-6/amendement-15)
* [Amendement n° 122 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-122)
* [Amendement n° 123 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-123)
* [Amendement n° 124 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-124)
* [Amendement n° 127 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-127)
* [Amendement n° 128 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-128)
* [Amendement n° 129 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-129)
* [Amendement n° 254 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-254)
* [Amendement n° 130 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-130)
* [Amendement n° 131 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-131)
* [Amendement n° 132 — Adopté](/amendements-1-assemblee-commission/article-6/amendement-132)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-5/amendement-118" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-6/amendement-119" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 68 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-16/amendement-218" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Gosselin**

[ARTICLE 16](/pjl-490/titre-ii/chapitre-vi/article-16)

# Titre

Compléter l’alinéa 2 par la phrase suivante :

« Lorsqu’elle constate un manquement, la Commission peut ordonner au responsable de traitement de rembourser à l’association ou à l’organisation qui en fait la demande les frais engagés par celle-ci pour exercer les droits des personnes concernées ».

# Exposé sommaire

L'action de groupe instituée par le RGPD ne saurait revêtir un caractère effectif si elle ne reposait sur aucune source de financement dédiée. À moins que les personnes concernées ne puissent être indemnisées du préjudice qu'elles ont subi, il y a peu de chance qu'elles financent elles-mêmes une association pour les représenter.
Or, mener efficacement une action de groupe a un coût important, qui peut s'avérer dissuasif sans mesures d'accompagnement appropriées. Mettre directement ce coût à la charge des responsables de traitements sanctionnés permettra d'atteindre l'équilibre financier le plus pertinent.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-16/amendement-218" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

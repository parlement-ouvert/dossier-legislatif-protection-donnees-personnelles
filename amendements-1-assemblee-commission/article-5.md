<!-- TITLE: Article 5 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-4/amendement-100" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-5/amendement-252" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 252 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-252)
* [Amendement n° 101 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-101)
* [Amendement n° 102 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-102)
* [Amendement n° 103 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-103)
* [Amendement n° 29 — Rejeté](/amendements-1-assemblee-commission/article-5/amendement-29)
* [Amendement n° 104 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-104)
* [Amendement n° 105 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-105)
* [Amendement n° 106 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-106)
* [Amendement n° 108 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-108)
* [Amendement n° 109 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-109)
* [Amendement n° 110 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-110)
* [Amendement n° 112 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-112)
* [Amendement n° 113 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-113)
* [Amendement n° 114 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-114)
* [Amendement n° 115 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-115)
* [Amendement n° 31 — Rejeté](/amendements-1-assemblee-commission/article-5/amendement-31)
* [Amendement n° 253 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-253)
* [Amendement n° 117 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-117)
* [Amendement n° 118 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-118)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-4/amendement-100" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-5/amendement-252" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

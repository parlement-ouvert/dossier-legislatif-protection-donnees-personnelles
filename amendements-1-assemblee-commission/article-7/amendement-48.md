<!-- TITLE: Amendement n° 48 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-7/amendement-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-7/amendement-134" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE 7](/pjl-490/titre-i/chapitre-ii/article-7)

# Titre

À l’alinéa 7, après le mot :

« biométriques »,

insérer le mot :

« strictement ».

# Exposé sommaire

Si Charles Pasqua considérait que "La démocratie s'arrête là où commence la raison d'Etat." (émission télévisée le 26 février 1987), nous estimons que la balance doit plutôt pencher du côté de la protection des droits et libertés fondamentales.

A cet effet, nous estimons que la dérogation à l’interdiction d’utilisation de données biométriques par les employeurs pour contrôler l’accès au lieu de travail ne doit pas être autorisée pour l’utilisation des données “nécessaires”, mais bien pour des données “strictement nécessaires”.

Cet amendement a en outre été proposé officiellement par la Conférence nationale consultative des droits de l’homme.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-7/amendement-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-7/amendement-134" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 260 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-7/amendement-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-7/amendement-255" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Amendement du Gouvernement

[ARTICLE 7](/pjl-490/titre-i/chapitre-ii/article-7)

# Titre

Rédiger ainsi l’alinéa 6 :

« 4° Le II est complété par des 9° et 10° ainsi rédigés : ».

II. – En conséquence, après l’alinéa 7, insérer un alinéa ainsi rédigé :

« 10° Les traitements portant sur la réutilisation des informations publiques figurant dans les jugements et décisions mentionnés aux articles L. 10 du code de justice administrative et L. 111‑13 du code de l’organisation judiciaire, sous réserve que ces traitements n’aient ni pour objet ni pour effet de permettre la ré-identification des personnes concernées. »

# Exposé sommaire

Cet amendement a pour objet de compléter l’article 8 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés afin d’autoriser la réutilisation des données sensibles dans le cadre de la mise à disposition du public à titre gratuit (open data) des décisions de justice prévu par la loi du 7 octobre 2016 pour une République numérique, à la condition que cette réutilisation n’aient ni pour objet ni pour effet de permettre la ré-identification des personnes. Une modification similaire est d’ores et déjà prévue pour les données d’infractions par l’article 11 du projet de loi.

Cet amendement répond à la recommandation n° 17 du rapport de la mission d’étude et de préfiguration sur l’ouverture au public des décisions de justice présidée par le Professeur Cadiet  remis à la garde des sceaux le 10 janvier dernier. Il répond également à une préconisation figurant dans l’avis de la Commission nationale de l’informatique et des libertés sur le projet de loi.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-7/amendement-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-7/amendement-255" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 54 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-7/amendement-133" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-7/amendement-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Latombe**

[ARTICLE 7](/pjl-490/titre-i/chapitre-ii/article-7)

# Titre

Compléter l’alinéa 3 par la phrase suivante :

« Il est interdit de traiter des données à caractère personnel collectées dans le cadre de l’utilisation de services numériques au sein de l’éducation nationale. »

# Exposé sommaire

Cet amendement vise à protéger les données personnelles des élèves français. En effet, avec le l'utilisation croissante des outils numériques dans un cadre éducatif et le développement de l'offre commerciale dans ce domaine, il est nécessaire de s'assurer que les données pouvant être collectées à cette occasion soient suffisamment protégées.

Nous pensons notamment aux services proposés par les grandes entreprises telles que Google, Apple, Facebook, Amazon ou Microsoft, qui développent des outils pédagogiques, concurrençant le développement d'espaces numériques du travail (ENT) sécurisés - privilégiés par le Ministère de l'éducation jusqu'à présent.

Or, nous nous interrogeons sur l'utilisation et le traitement des données collectées par ces services souvent gratuits.

Bien qu'il existe des conditions générale d'utilisations (CGU) spécifiques à l'éducation, nous doutons que les règles soient aussi strictes que celles prévues dans les cahiers des charges des espaces numériques de travail (ENT).

Afin de pallier à cette incertitude, cet amendement propose de classer les données scolaires des élèves parmi les données sensibles.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-7/amendement-133" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-7/amendement-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

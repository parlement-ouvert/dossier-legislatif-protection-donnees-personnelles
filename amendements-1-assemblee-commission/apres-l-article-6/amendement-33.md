<!-- TITLE: Amendement n° 33 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-6/amendement-30" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 6](/pjl-490/titre-i/chapitre-i/article-6), insérer l'article suivant:

# Titre

Le chapitre VI de la loi n° 78‑17 du 6 janvier 1978 précitée est complété par un article 44 _bis_ ainsi r édigé :

« _Art. 44 _bis. – I. – À titre expérimental, pour une durée d’un an et dans les conditions définies par un décret en Conseil d’État, et afin d’évaluer et de contrôler les missions d’inspection et de contrôle effectuées par la Commission nationale informatiques et libertés, peut être constituée une commission non permanente composée de composée de 10 députés et 10 sénateurs désignés respectivement par le Président de l’Assemblée nationale et le Président du Sénat, issus de chaque groupe parlementaire, en particulier ceux d’opposition et minoritaires, de manière à assurer une représentation équilibrée des groupes politiques et d’experts, de 10 experts nommés par le Gouvernement, issu du monde universitaire économique ou associatif, et de 10 citoyens ou citoyennes tiré.e.s au sort sur la base du volontariat.

« II. – Le Gouvernement remet au Parlement, au plus tard deux an après promulgation de la présente loi, un rapport exhaustif présentant l’expérimentation, ainsi que ses résultats. »

# Exposé sommaire

Cet amendement vise à créer un système de **_contr ôle_** social et citoyen de l'effectivité des missions d’inspection et de contrôle effectuées par la Commission nationales informatiques et libertés.

Dans un processus d’interaction entre les institutions publiques et les acteurs non étatiques au titre desquelles les organisations sociales, les acteurs économiques, le groupe de la France insoumise propose la mise en place dans le cadre d’une expérimentation, d’une instance de dialogue multi-acteurs, afin de renforcer la responsabilisation de tous les acteurs de l’action publique.

Facteur important de reconnexion entre les populations et les institutions publiques et donc de la légitimation de ces dernières, il est important d’assurer un contrôle de l’action publique afin de renforcer l’efficacité et l’impact de l’action publique, notamment en contribuant à réduire le décalage, très souvent constaté, entre les objectifs affichés des politiques publiques et les résultats de leur mise en œuvre.

Afin de pleinement pouvoir mener ses missions à bien, cette commission disposera d’un pouvoir d’injonction pour donner des instructions générales aux services de la CNIL, et pourra être plus aux prises avec la réalité du terrain en pouvant auditionner directement tout agent

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-6/amendement-30" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 249 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 2](/pjl-490/titre-i/chapitre-i/article-2), insérer l'article suivant:

# Titre

L’article 15 de la loi n° 78‑17 du 6 janvier 1978 précitée est complété par un alinéa ainsi rédigé :

« La commission peut également charger le secrétaire général d’informer les auteurs de réclamations, pétitions et plaintes relatives à la mise en œuvre des traitements de données à caractère personnel des suites données à celles-ci, en application du _c_ du 2° de l’article 11, sous réserve que ce dernier ne détienne pas, directement ou indirectement, des intérêts dans une entreprise du secteur des communications électroniques ou de l’informatique. »

# Exposé sommaire

Cet amendement a pour objet de permettre à la commission de déléguer au secrétaire général l'exercice de sa mission consistant à informer les auteurs de plaintes ou de réclamations des suites données à celles-ci. Il doit permettre à la CNIL de mieux traiter les nombreuses réclamations individuelles reçues chaque année.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

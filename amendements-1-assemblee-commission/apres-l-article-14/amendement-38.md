<!-- TITLE: Amendement n° 38 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/apres-l-article-14/amendement-45" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 14](/pjl-490/titre-ii/chapitre-v/article-14), insérer l'article suivant:

# Titre

I. – Après l’article 11 de la loi n° 78‑17 du 6 janvier 1978 précitée, il est inséré un article 11 _bis_ ainsi r édigé :

« _Art. 11 _bis. – A titre expérimental, pour une durée maximale de deux ans, la Commission nationale de l’informatique et des libertés peut :

« 1° Informer, par l’organisation d’ateliers, de conférences et de discussions publiques, les acteurs et actrices du numérique, du secteur privé et du secteur public, sur les risques de production et de reproduction, par des algorithmes, de biais discriminatoires constitutionnellement interdits ;

« 2° Conseiller les acteurs et actrices du numérique sur les moyens d’éviter ou de corriger la production ou la reproduction de biais discriminatoires constitutionnellement interdits. »

II. – Un décret en Conseil d’État précise les conditions d’application du présent article. Cette expérimentation fait l’objet d’un bilan transmis au Parlement évaluant l’opportunité d’une généralisation de ce dispositif.

# Exposé sommaire

Les données classifiées comme sensibles ou personnelles le sont pour des raisons de confidentialité d’une part et de lutte contre les discriminations d’autre part, les deux impératifs ne s’excluant pas l’un l’autre.

Or nombre de données sensibles sont classifiées comme tels car elles sont habitées de significations sociales particulières et pouvant entraîner un traitement différencié défavorable. En cela le monde numérique est un miroir, une prolongation de notre réalité physique.

Les recherches récentes montrent que les algorithmes peuvent produire ou reproduire certaines associations discriminantes (http://msh-paris-saclay.fr/workshop-algorithme-discrimination/ par exemple).

Cet amendement vise à faire de la CNIL une force d’impulsion dans l’information et la formation afin de lutter contre ce phénomène pouvant avoir des conséquences graves pour les individus dans leurs recherches d’emploi, d’appartement, leur demande de crédit etc. alors même que les informations sont erronées.

Cela vient dans beaucoup de cas renforcer les difficultés pour des personnes déjà dans une situation structurelle d’une certaine vulnérabilité sociale.

Parce que la France insoumise est convaincue qu’Internet doit être un espace de liberté pour tous et toutes et non d’oppression pour certain.e.s,  elle souhaite renforcer et encourager par cet amendement l’information et le conseil dans ce domaine.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/apres-l-article-14/amendement-45" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

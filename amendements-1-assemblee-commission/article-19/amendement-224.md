<!-- TITLE: Amendement n° 224 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-19/amendement-223" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-19/amendement-187" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

[ARTICLE 19](/pjl-490/titre-iii/article-19)

# Titre

Substituer à l’alinéa 115 les deux alinéas suivants :

« _Art. 70‑23_. – I. – Les informations mentionnées aux articles 70‑18 à 70‑20 sont fournies par le responsable de traitement à la personne concernée par tout moyen approprié, y compris par voie électronique, et, de manière générale, sous la même forme que la demande.

« II. – Aucun paiement n’est exigé pour prendre les mesures et fournir ces mêmes informations, sauf en cas de demande manifestement infondée ou abusive. »

# Exposé sommaire

Le présent amendement vise à éviter toute contradiction entre les exigences prévues par l’article 12§1 de la directive et celles fixées par l’actuel article 43 _bis_ de la loi.

En effet, aux termes de l’article 12§1 de la directive, les informations sont fournies par le responsable du traitement à la personne concernée par tout moyen approprié, y compris par voie électronique, et de manière générale, sous la même forme que la demande.

Des précisions similaires figurent déjà dans l’actuel article 43 _bis_ de la loi qui pr évoit notamment que, hors le cas d’un traitement mis en œuvre pour le compte de l’État et intéressant la sécurité publique, le responsable du traitement, s’il a collecté par voie électronique des données à caractère personnel, permet à toute personne d’exercer ses droits par voie électronique lorsque cela est possible.

C'est pourquoi le présent amendement insère dans la loi ces précisions afin qu'elles s'appliquent aux traitements à des fins pénales.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-19/amendement-223" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-19/amendement-187" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

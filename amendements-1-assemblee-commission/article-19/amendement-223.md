<!-- TITLE: Amendement n° 223 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-19/amendement-186" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-19/amendement-224" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

[ARTICLE 19](/pjl-490/titre-iii/article-19)

# Titre

À l’alinéa 111, substituer à la seconde occurrence du mot :

« ou »,

les mots :

« . Hors le cas prévu au 1° du II, il l’informe également de la possibilité ».

# Exposé sommaire

Le nouvel article 70‑21 transpose les articles 13, 15 et 16 de la directive en prévoyant qu’en cas de restriction des droits d’information, d’accès, de rectification ou d’effacement de la personne concernée, celle-ci doit être avisée de la possibilité d’exercer ses droits par l’intermédiaire de la Commission nationale de l’informatique et des libertés ou de former un recours juridictionnel.

Cet article procède toutefois à une sur-transposition injustifiée, car lorsque la restriction porte sur le droit d’information, le responsable du traitement doit seulement, aux termes de l’article 13 de la directive, aviser la personne de la possibilité de saisir la CNIL, et non de celle d’exercer un recours.

Le présent amendement procède ainsi à une exacte transposition de la directive en précisant que cet avis n’a pas lieu d’être dans une telle hypothèse.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-19/amendement-186" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-19/amendement-224" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

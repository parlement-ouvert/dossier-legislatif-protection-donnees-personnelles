<!-- TITLE: Amendement n° 226 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-9/amendement-140" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-9/amendement-207" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

[ARTICLE 9](/pjl-490/titre-ii/chapitre-ii/article-9)

# Titre

Substituer aux alinéas 11 à 16 les deux alinéas suivants :

II. – L’article 27 de la loi n° 78‑17 du 6 janvier 1978 précitée est ainsi rédigé :

« _Art. 27_. – Sont autorisés par décret en Conseil d’État, pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés, les traitements de données à caractère personnel mis en œuvre pour le compte de l’État, agissant dans l’exercice de ses prérogatives de puissance publique, qui portent sur des données génétiques ou sur des données biométriques nécessaires à l’authentification ou au contrôle de l’identité des personnes. »

# Exposé sommaire

Amendement de clarification.

Pour une meilleure compréhension des modifications apportées au droit actuel par le projet de loi, le présente amendement rédige nouvellement l’article 27 de la loi de 1978 afin de faire apparaître clairement que le régime d’autorisation préalable par décret en Conseil d’État après avis de la CNIL prévu par cet article n’est conservé que pour les traitements mis en œuvre pour le compte de l’État et portant sur des données biométriques ou génétiques aux fins d’authentification ou de contrôle de l’identité des personnes.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-9/amendement-140" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-9/amendement-207" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

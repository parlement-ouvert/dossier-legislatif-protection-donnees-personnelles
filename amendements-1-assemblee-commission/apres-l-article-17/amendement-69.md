<!-- TITLE: Amendement n° 69 -->
<!-- SUBTITLE: Retiré -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Éric Bothorel**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 17](/pjl-490/titre-ii/chapitre-vi/article-17), insérer l'article suivant:

# Titre

Tout fabricant ou distributeur de terminal, fixe ou mobile, permettant l’accès à des services de communication au public en ligne doit configurer par défaut ce terminal d’une manière qui n’incite pas l’utilisateur final à recourir, à des fins de recherches d’informations, à un service qui collecte et conserve tout ou partie des requêtes associées à des données personnelles de l’utilisateur.

Lorsqu’elles intègrent des points d’entrée de recherches d’informations en ligne, les personnes visées au premier alinéa proposent, par défaut, le choix d’un service qui ne collecte et ne conserve pas de données personnelles associées aux recherches effectuées.

Toute clause contractuelle ayant pour effet de contraindre les personnes visées au premier alinéa de configurer par défaut des points d’entrée de recherches d’informations est nulle lorsqu’elle conduit l’utilisateur final à recourir à un service qui collecte et conserve des données personnelles liées aux recherches effectuées.

# Exposé sommaire

La quasi-totalité des smartphones commercialisés en France et en Europe sont équipés d’un système d’exploitation mobile iOS ou Android qui impose par défaut le même moteur de recherche à leurs utilisateurs, à travers différents points d’entrée (navigateur, barre de recherche, assistant vocal…). Ce moteur de recherche collecte, conserve et analyse l’intégralité des requêtes de chaque individu, par l’effet d’un contrat d’adhésion qui crée la fiction d’un consentement, en réalité obtenu faute de proposition alternative. Or le niveau d’intrusion dans la vie privée que permet un tel moteur de recherche utilisé quotidiennement par des dizaines de millions de Français est considérable, et il appartient donc au législateur d’apporter aux citoyens la protection de la loi.

Actuellement, par l’effet de clauses insérées dans les contrats de licences liés au système Android, les fabricants et distributeurs de terminaux mobiles qui souhaitent utiliser ce système d’exploitation et donner accès à son écosystème d’applications n’ont pas d’autres choix que d’installer par défaut ce moteur de recherche sur tous les points d’entrée possibles. Dans son article 25, le Règlement général à la protection des données (RGPD) impose au contraire d’adopter des principes de « protection par défaut » des données personnelles des individus.

Le présent amendement vise donc à rendre nulles de telles clauses contraires à l’esprit du RGPD, afin que les fabricants et distributeurs puissent offrir aux utilisateurs finaux des alternatives plus respectueuses de leur vie privée.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

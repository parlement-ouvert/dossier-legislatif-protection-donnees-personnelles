<!-- TITLE: Amendement n° 46 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-13/amendement-9" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/avant-l-article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 13](/pjl-490/titre-ii/chapitre-iv/article-13), insérer l'article suivant:

# Titre

Le chapitre X de la loi n° 78 17 du 6 janvier 1978 précitée est ainsi rétabli :

« Chapitre X

« Traitements de données à caractère personnel dans le domaine scolaire :

« _Art. 62_. – L’ensemble des données collectées par les services du ministère de l’Éducation nationale aux fins d’assurer la mission d’enseignement et d’encadrement des élèves, doivent être considérées comme des données d’intérêt public. À ce titre, conformément aux dispositions du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE, les traitements de ces données sont dispensés du consentement des personnes concernées.

« _Art. 63_. – I. – Les traitements relevant du présent chapitre ne peuvent être mis en œuvre qu’en considération de la finalité d’intérêt public qu’ils présentent.

« II. – Des référentiels et règlements types, au sens des _a_ bis et _b_ du 2° de l’article 11, s’appliquant aux traitements relevant du présent chapitre sont établis par la Commission nationale de l’informatique et des libertés en concertation avec le ministre chargé de l’éducation et des organismes publics et privés représentatifs des acteurs concernés.

« III. – Le ministre chargé de l’éducation s’assure que l’ensemble des traitements des données effectués par les services de son ministère sont conformes à ces référentiels.

« Ces référentiels, peuvent également porter sur la description et les garanties de procédure permettant la mise à disposition en vue de leur traitement de jeux de données scolaires présentant un faible risque d’impact sur la vie privée.

« IV. – La Commission nationale de l’informatique et des libertés peut, par décision unique, délivrer au ministre de l’éducation une autorisation pour des traitements répondant à une même finalité, portant sur des catégories de données identiques et ayant des catégories de destinataires identiques.

« V. – La Commission nationale de l’informatique et des libertés se prononce dans un délai de deux mois à compter de la réception de la demande. Toutefois, ce délai peut être renouvelé une fois sur décision motivée de son président ou lorsque l’Inspection générale est saisie en application du II du présent article.

« Lorsque la commission ne s’est pas prononcée dans ces délais, la demande d’autorisation est réputée acceptée. Cette disposition n’est toutefois pas applicable si l’autorisation fait l’objet d’un avis préalable en vertu des dispositions du présent chapitre et que l’avis ou les avis rendus ne sont pas expressément favorables.

« _Art. 64_. – S’agissant des mineurs, sont destinataires de l’information et exercent les droits de la personne concernée par le traitement les titulaires de l’exercice de l’autorité parentale. »

# Exposé sommaire

Cet amendement proposé par la Commission nationale consultative des droits de l’homme nous semble particulièrement à propos, et c’est pour cela que nous nous en faisons le relais.

En effet, en n’utilisant pas les marges de manoeuvres prévues le règlement RGPD, le Gouvernement a par là même préféré la l’ouverture “sauvage” de nombreuses données en facilitant la création de traitements (qui n’auront plus à être autorisés par la CNIL). La CNCD, par son intervention et sa proposition d’amendement rappelle au Gouvernement qu’il a donc laissé différents angles morts sur des données personnelles massives et réellement problématiques (dans le cas d’espèce, il est question de méga fichiers de l’éducation nationale - au vu du nombre d’élèves scolarisés - et de données de bon nombre de  personnes mineures).

Selon les termes mêmes du 5 de l’article 36 du règlement européen RGPD : “ _5\. Nonobstant le paragraphe 1, le droit des États membres peut exiger que les responsables du traitement consultent l'autorité de contrôle et obtiennent son autorisation préalable en ce qui concerne le traitement effectué par un responsable du traitement dans le cadre d'une mission d'intérêt public exercée par celui-ci, y compris le traitement dans le cadre de la protection sociale et de la santé publique_.”.

Cet amendement va de pair avec un autre amendement que nous proposons et qui prévoit que l’article 25 de la loi de 1978 soit modifié pour que soient précisées les missions d’intérêt public de cet article 36 en droit français (notamment dans les domaines sociaux, éducatifs, économiques, environnementaux, sanitaires, médico-sociaux, de l’énergie, des transports et culturels.).

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-13/amendement-9" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/avant-l-article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 12 -->
<!-- SUBTITLE: Retiré -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/apres-l-article-13/amendement-67" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Blandine Brocard**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 13](/pjl-490/titre-ii/chapitre-iv/article-13), insérer l'article suivant:

# Titre

L’article 7 de la loi n° 78‑17 du 6 janvier 1978 précitée est complété par cinq alinéas ainsi rédigés :

« Le consentement de la personne concernée doit être obtenu de manière loyale et résulter d’une action volontaire, explicite, libre, spécifique et informée. Il ne doit pas être exigé en contrepartie d’un bien ou d’un service à moins que le traitement faisant l’objet du consentement ne soit indispensable à la fourniture de ce bien ou service.

« Si la possibilité de refuser le consentement est dissociée de son acceptation, les deux propositions doivent être accolées et présentées sous la même forme et notamment dans les mêmes tailles de caractère, couleur, typographie ou intensité sonore.

« L’objet du consentement doit être spécifique et comporter le nom de la personne morale ou physique responsable du traitement ainsi que la finalité du traitement. Si plusieurs personnes morales ou physiques sont destinataires des données collectées, le consentement doit être donné de manière distincte pour chaque destinataire.

« Le consentement ne peut pas être présumé et, à ce titre, ne doit pas être présenté sous la forme d’une case précochée, d’un bouton radio ou d’une liste d’options, prédéfinis sur l’acceptation. Il ne peut être recueilli que par une action spécifique de la personne concernée sans aucune autre finalité. Cela implique notamment que cette action n’ait pas pour conséquence de faire disparaître l’option de refus, de diriger la navigation vers une nouvelle page, ou de valider l’envoi d’un formulaire.

« Le consentement ne peut être obtenu de manière indirecte, notamment par l’acceptation de conditions générales ou d’un règlement. »

# Exposé sommaire

Cet amendement vise à définir la manière selon laquelle un consentement au traitement des données personnelles peut être obtenu et à empêcher les contournements de l'actuelle législation par les responsables de traitement.

Introduite par l'article 22 de la Loi sur la confiance dans l'économie numérique, la définition du consentement doit être précisée car elle fait l'objet de nombreux abus et de contournements résultant en une extorsion déloyale du consentement, notamment en masquant la possibilité de le refuser, ou en diluant le consentement par différentes formulations ou artifices techniques.

Cette notion de liberté et de spécificité du consentement est réaffirmée par la norme simplifiée NS-048 de la CNIL dont l'article 6 précise certaines modalités de recueil de consentement. Ces normes d'obtention du consentement doivent être précisées car elle font l'objet de trop nombreux contournements.

Cet amendement vise enfin à intégrer la définition donnée par la CNIL et le G29 (groupement des CNIL européennes) du caractère libre du consentement qui doit être sans ambiguité, ne doit pas être dilué, et doit être donné librement sans que le refus de consentement n'occasionne l'impossibilité d'accéder à un service si celui-ci peut être réalisé sans ce consentement. Le G29 donne à ce sujet l'exemple d'une application de modifications de photographies obligeant l'activation de la géolocalisation et le traitement des données à des fins publicitaires pour pouvoir l'utiliser et conclut que dans ce cas le consentement n'est pas donné librement.

Sources citées :

LCEN : https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=JORFARTI000001093763&amp;cidTexte=JORFTEXT000000801164

Norme simplifiée NS-048 de la CNIL : https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033117877&amp;dateTexte=&amp;categorieLien=id

G29, définition du consentement : http://ec.europa.eu/newsroom/just/document.cfm?doc_id=48849

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/apres-l-article-13/amendement-67" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

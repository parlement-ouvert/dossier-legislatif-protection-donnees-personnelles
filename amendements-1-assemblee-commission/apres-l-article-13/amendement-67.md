<!-- TITLE: Amendement n° 67 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-13/amendement-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/apres-l-article-13/amendement-9" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Gosselin**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 13](/pjl-490/titre-ii/chapitre-iv/article-13), insérer l'article suivant:

# Titre

L’article 7 de la loi n° 78‑17 du 6 janvier 1978 précitée est complété par un alinéa ainsi rédigé :

« Pour être valide, le consentement de la personne concernée doit être donné de façon explicite, libre, spécifique et informée. Cela implique notamment que son consentement ne soit pas exigé en contrepartie d’un bien ou d’un service, à moins que le traitement faisant l’objet du consentement ne soit indispensable à la fourniture de ce bien ou service. »

# Exposé sommaire

Cet amendement vise à intégrer la définition déterminante donnée par la CNIL et le G29 du caractère libre du
consentement (voir les dernières propositions de lignes direcrices produites par le G29 à ce sujet :
http://ec.europa.eu/newsroom/just/document.cfm?doc_id=4884 9).

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-13/amendement-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/apres-l-article-13/amendement-9" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

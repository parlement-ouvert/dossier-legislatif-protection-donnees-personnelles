<!-- TITLE: Article 9 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-7/amendement-47" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-9/amendement-51" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 51 — Non soutenu](/amendements-1-assemblee-commission/article-9/amendement-51)
* [Amendement n° 136 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-136)
* [Amendement n° 137 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-137)
* [Amendement n° 138 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-138)
* [Amendement n° 139 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-139)
* [Amendement n° 140 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-140)
* [Amendement n° 226 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-226)
* [Amendement n° 207 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-207)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-7/amendement-47" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-9/amendement-51" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

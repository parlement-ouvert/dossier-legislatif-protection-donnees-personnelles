<!-- TITLE: Amendement n° 229 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-17/amendement-212" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-17/amendement-219" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

[ARTICLE 17](/pjl-490/titre-ii/chapitre-vi/article-17)

# Titre

À la première phrase de l’alinéa 2, supprimer les mots :

« ou la cessation ».

# Exposé sommaire

Cet amendement vise à supprimer la possibilité pour le Conseil d'Etat d'ordonner, sur demande de la CNIL, la "cessation" du transfert de données à caractère personnel vers un État tiers, pour se cantonner à la possibilité d'ordonner "la suspension" du transfert dans la mesure où la décision du Conseil d'Etat est nécessairement temporaire.

En effet, cette décision ne peut intervenir que dans l'attente de la décision de la CJUE, saisie à titre préjudiciel par le Conseil d’État sur demande de la CNIL, lorsque cette dernière estime fondée la demande d’une personne relative à la protection de ses droits et libertés à l’égard du traitement de données à caractère personnel la concernant qui ont été transférées depuis un État membre vers un pays tiers, quand cette personne fait valoir que le droit et les pratiques en vigueur dans celui‑ci n’assurent pas un niveau de protection adéquat, malgré une décision d'adéquation de la Commission européenne.

Cette précision est conforme à la décision de la CJUE dans l'affaire "Schrems" C‑362/14 dans laquelle la Cour ne vise que la "suspension" des flux de données.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-17/amendement-212" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-17/amendement-219" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 7 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-6/amendement-33" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-7/amendement-133" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 133 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-133)
* [Amendement n° 54 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-54)
* [Amendement n° 17 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-17)
* [Amendement n° 260 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-260)
* [Amendement n° 255 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-255)
* [Amendement n° 7 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-7)
* [Amendement n° 48 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-48)
* [Amendement n° 134 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-134)
* [Amendement n° 16 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-16)
* [Amendement n° 135 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-135)
* [Amendement n° 50 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-50)
* [Amendement n° 34 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-34)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-6/amendement-33" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-7/amendement-133" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 2 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-premier/amendement-247" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-2/amendement-248" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 248 — Adopté](/amendements-1-assemblee-commission/article-2/amendement-248)
* [Amendement n° 18 — Adopté](/amendements-1-assemblee-commission/article-2/amendement-18)
* [Amendement n° 60 — Adopté](/amendements-1-assemblee-commission/article-2/amendement-60)
* [Amendement n° 66 — Tombé](/amendements-1-assemblee-commission/article-2/amendement-66)
* [Amendement n° 71 — Tombé](/amendements-1-assemblee-commission/article-2/amendement-71)
* [Amendement n° 75 — Tombé](/amendements-1-assemblee-commission/article-2/amendement-75)
* [Amendement n° 26 — Tombé](/amendements-1-assemblee-commission/article-2/amendement-26)
* [Amendement n° 27 — Tombé](/amendements-1-assemblee-commission/article-2/amendement-27)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-premier/amendement-247" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-2/amendement-248" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

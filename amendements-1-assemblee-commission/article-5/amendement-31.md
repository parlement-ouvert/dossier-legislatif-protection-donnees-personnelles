<!-- TITLE: Amendement n° 31 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-5/amendement-115" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-5/amendement-253" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE 5](/pjl-490/titre-i/chapitre-i/article-5)

# Titre

Supprimer l’alinéa 19.

# Exposé sommaire

Le régime juridique de base prévu à l'alinéa 19 de l'article 5 par ce projet de loi est le suivant  : "III. – Lorsque le responsable d’un traitement ou le sous-traitant ne respecte pas les obligations découlant du règlement (UE) 2016/679 ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut également prononcer à son égard une mise en demeure, dans le délai qu’il fixe".

Eu égard à l'importance de privilégier les droits et libertés sur la "raison d'Etat" selon la formule de Charles Pasqua ("la démocratie s'arrête là où commence la raison d'Etat"), notre amendement propose la suppression de l’exception suivante qui nous apparaît excessivement injustifiée : “3° À l’exception des traitements qui intéressent la sûreté de l’État ou la défense et ceux mentionnées à l’article 27, de communiquer à la personne concernée une violation de données à caractère personnel".

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-5/amendement-115" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-5/amendement-253" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

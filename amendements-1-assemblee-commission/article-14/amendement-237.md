<!-- TITLE: Amendement n° 237 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-14/amendement-235" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/apres-l-article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

[ARTICLE 14](/pjl-490/titre-ii/chapitre-v/article-14)

# Titre

I. – À l’alinéa 7, après le mot :

« administratives »,

insérer le mot :

« individuelles ».

II. – En conséquence, compléter le même alinéa par les mots :

« afin de pouvoir expliquer, en détails et sous une forme intelligible, à la personne concernée, la manière dont le traitement a été mis en œuvre à son égard. »

# Exposé sommaire

Cet amendement vise à préciser ce que recouvre l’obligation pour le responsable de traitement de s’assurer de la maitrise du traitement algorithmique et de ses évolutions, sur le fondement duquel est prise une décision administrative individuelle, à savoir celle de pouvoir expliquer, en détails et sous une forme intelligible, à la personne concernée, la manière dont le traitement a été mis en œuvre et ses conséquences à son égard.

Cela empêche l’administration de prendre des décisions individuelles automatiques sur le fondement d’algorithmes qu’elle ne maîtrise plus et qu’elle ne sait pas expliquer, comme les algorithmes d’apprentissage profond.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-14/amendement-235" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/apres-l-article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

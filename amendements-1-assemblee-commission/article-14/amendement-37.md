<!-- TITLE: Amendement n° 37 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-14/amendement-235" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE 14](/pjl-490/titre-ii/chapitre-v/article-14)

# Titre

Supprimer cet article.

# Exposé sommaire

Par cette loi de transposition, et sans que cela n’ait été explicitement prévu par la directive européenne (directive (UE) 2016/680 relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel (…)) le Gouvernement a impulsé de lui-même une libéralisation du “fichage” en facilitant l’accès aux données (plus de champs de données ouvertes), la création d’un traitement étant facilitée (l’activité principale de la CNIL devenant la supervision plus que l’autorisation), et en ouvrant la possibilité à plus d’acteurs d’utiliser ces données.

Tel que le rappelle notre Livret numérique, nous nous opposons au fichage de masse tel que proposé par ce projet de loi (<https://avenirencommun.fr/le-livret-numerique/>).

Or, par cet article 14, le Gouvernement souhaite autoriser la mise en place de définir le profil d’une personne comme aide à la prise de décision administrative (auparavant définir un profil était interdit, ce projet de loi prévoit que désormais c’est seulement “prévoir” qui est interdit - mais la frontière juridique est bien mince…).

Concrètement, en permettant de définir un profil de demandeur (pour un titre de séjour pour un étranger, pour une demande d’emploi pour un chômeur,  pour un permis de construire pour un riverain de l’océan) est ce que ce projet de loi ne permettra pas par exemple d’établir un niveau de risque ou de probabilité d’acceptation d’une demande ?

Par exemple, selon son pays d’origine, son âge, son adresse, son cursus scolaire, ces algorithmes permis par cet article ne vont-il pas permettre de pré-sélectionner (dossiers pouvant être automatisés selon certains critères et d’autres nécessitant une intervention humaine - le travail d’un agent), et donc de discriminer (voire de renforcer des discriminations - par exemple la situation de l’emploi et la localisation géographique) et totalement nier l’analyse individualisée qui doit obligatoirement être faite des dossiers (le “défaut d’examen individuel et personnalisé” est considéré comme une erreur de droit par les juges).

Nous refusons les conséquences directes de cet article 14 : cette déshumanisation et quasi-automatisation du fonctionnement notamment de l’administration de l’Etat et des collectivités territoriales.

Quelle va être la prochaine étape ? la justice va-t-elle aussi faire du profilage ? (1er alinéa de l’article 10 de la loi de 1978, alors qu’ici c’est le 2e alinéa, relatif aux décisions administratives qui est modifié !)...

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-14/amendement-235" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

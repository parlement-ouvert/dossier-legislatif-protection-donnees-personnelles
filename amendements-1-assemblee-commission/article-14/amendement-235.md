<!-- TITLE: Amendement n° 235 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-14/amendement-37" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-14/amendement-237" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

[ARTICLE 14](/pjl-490/titre-ii/chapitre-v/article-14)

# Titre

I. – Supprimer l’alinéa 3.

II. – En conséquence, après le mot :

« exception »,

supprimer la fin de l’alinéa 5.

III. – En conséquence, après l’alinéa 5, insérer les trois alinéas suivants :

1° _bis_ Après le deuxième alinéa, sont insérés deux alinéas ainsi rédigés :

« 1° des cas mentionnés aux _a_ et _c_ du 2 de l’article 22 du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE et sous les réserves mentionnées au 3 du même article ;

« 2° des décisions administratives individuelles prises dans le respect de l’article L. 311‑3‑1 et du chapitre Ier du titre Ier du livre IV du code des relations entre le public et l’administration, à condition que le traitement ne porte pas sur des données mentionnées au I de l’article 8 de la présente loi. » ;

# Exposé sommaire

Cet amendement, d'apparence rédactionnelle, vise à clarifier le principe selon lequel aucune décision produisant des effets juridiques à l'égard d'une personne ne peut être prise sur le seul fondement d'un traitement automatisés de données sauf exceptions. Ces exceptions sont les suivantes :

1) les décisions résultant de la conclusion ou de l'exécution d'un contrat entre la personne concernée et un responsable du traitement et les décisions fondées sur le consentement explicite de la personne concernée (exceptions mentionnés au a et c du 2 de l'article 22 du règlement général sur les données personnelles - RGDP) dès lors que les garanties minimales suivantes sont assurées :

\- droit de la personne concernée d'obtenir une intervention humaine de la part du responsable du traitement;

\- droit d'exprimer son point de vue ;

\- droit de contester la décision.

2) les décisions administratives individuelles dès lors que les garanties minimales suivantes sont assurées :

\- le respect des obligations prévues par l'article L. 311-3-1 et par le chapitre Ier du titre Ier du livre IV du code des relations entre le public et l’administration (droit d'accès au code source, obligation générale d'information du public par l'administration des règles définissant les principaux traitements algorithmiques utilisés dans l'accomplissement de leurs missions lorsqu'ils fondent des décisions individuelles, obligation d'information à l'égard de la personne concernée qu'elle fait l'objet d'une décision individuelle fondée sur un seul traitement algorithmique et qu'elle peut demander les principales caractéristiques de sa mise en oeuvre par l'administration selon les modalités fixées par l'article R. 311-3-1-1) ;

\- l'interdiction que le traitement porte sur des données faisant apparaître, directement ou indirectement, les origines raciales ou ethniques, les opinions politiques, philosophiques ou religieuses ou l'appartenance syndicale des personnes, ou qui sont relatives à la santé ou à la vie sexuelle de la personne concernée (I de l'article 8 de la loi n° 78-17 du 6 janvier 1978).

\- le droit d'exercer un recours comme pour toute décision administrative individuelle.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-14/amendement-37" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-14/amendement-237" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

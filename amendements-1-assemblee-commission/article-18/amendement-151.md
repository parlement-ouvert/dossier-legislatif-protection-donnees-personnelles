<!-- TITLE: Amendement n° 151 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-18/amendement-152" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

[ARTICLE 18](/pjl-490/titre-iii/article-18)

# Titre

Rédiger ainsi l’alinéa 1 :

« I. – Le début du V de l’article 32 de la loi n° 78‑17 du 6 janvier 1978 précitée est ainsi rédigé : « Sans préjudice de l’application des dispositions du chapitre XIII, les dispositions du I ne s’appliquent pas aux données recueillies dans les conditions prévues au III et utilisées lors d’un traitement mis en œuvre pour le compte de l’État et intéressant la sûreté de l’État, la défense ou la sécurité publique, dans la ... _(le reste sans changement)_ » ; ».

# Exposé sommaire

Amendement rédactionnel.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-18/amendement-152" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 247 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-premier/amendement-25" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE PREMIER](/pjl-490/titre-i/chapitre-i/article-1), insérer l'article suivant:

# Titre

L’article 4 _bis_ de l ’ordonnance n° 58‑1100 du 17 novembre 1958 relative au fonctionnement des assemblées parlementaires est ainsi modifié :

1° Après le premier alinéa, il est inséré un alinéa ainsi rédigé :

« Le président d’une assemblée parlementaire peut également saisir la Commission nationale de l’informatique et des libertés d’une proposition de loi dans les mêmes conditions. » ;

2° Au deuxième alinéa, après les mots : « Conseil d’État », sont insérés les mots : « ou à la Commission nationale de l’informatique et des libertés » ;

3° À l’avant-dernier alinéa, après les mêmes mots, sont insérés les mots : « ou de la Commission nationale de l’informatique et des libertés ».

# Exposé sommaire

Cet amendement vise à inscrire la possibilité pour le président d'une assemblée parlementaire de saisir la CNIL dans l'ordonnance du 17 novembre 1958.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-premier/amendement-25" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

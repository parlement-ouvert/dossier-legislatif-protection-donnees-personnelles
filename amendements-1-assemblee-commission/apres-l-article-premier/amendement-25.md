<!-- TITLE: Amendement n° 25 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-premier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/apres-l-article-premier/amendement-247" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE PREMIER](/pjl-490/titre-i/chapitre-i/article-1), insérer l'article suivant:

# Titre

I. – Après l’article 11 de la loi n° 78‑17 du 6 janvier 1978 précitée, il est inséré un article 11 _bis_ ainsi r édigé :

« _ Art. 11 _bis. – À titre expérimental, la Commission nationale de l’informatique et des libertés exerce aussi la mission suivante :

« Elle assure l’information du public, en particulier des élèves de l’enseignement des premiers, deuxième degrés et de l’enseignement supérieur, quant aux enjeux du numérique sur leurs droits et libertés fondamentales et les moyens de faire pleinement valoir ceux-ci. »

II. – À titre expérimental, pour une durée maximale de trois ans, la mission de la Commission nationale de l’informatique et des libertés mentionnée au I peut être mise en œuvre dans les départements, universités, académies et rectorats volontaires qui en formulent la demande auprès de l’autorité administrative compétente. Un décret en Conseil d’État précise les conditions d’applications du présent article. Cette expérimentation fait l’objet d’un bilan transmis au Parlement évaluant l’opportunité d’une généralisation de ce dispositif.

# Exposé sommaire

“ _Nous pr ônons un numérique en commun, construit sur les principes fondateurs de notre pacte républicain (...) - la liberté (...) en donnant à chacun l'éducation et l'information nécessaires pour comprendre le monde numérique_” (Livret numérique : pour de nouveaux droits et libertés numériques <https://avenirencommun.fr/le-livret-numerique/>).

En effet, au delà de la fracture numérique qui a d’ores et déjà été dénoncée par le Défenseur des droits (<https://www.defenseurdesdroits.fr/sites/default/files/atoms/files/enquete-relations-usagers-servpublics-v6-29.03.17.pdf>), la connaissance par chaque personne physique, chaque citoyen et citoyenne de ses droits et libertés en matière d’informatique et de numérique est indispensable au XXIème siècle. Quelques exemples concrets :

- une éducation aux risques pouvant peser sur ces droits et libertés : par une bonne information sur les conséquences pratiques de la mise à disposition de données personnelles (pensées, photos) sur des sites internet ou des applications qui les conservent (Facebook, Linkedin, etc.), ou collectées en masse (par exemple lorsque l’on souscrit à une carte de réduction avec avantages d’un grand hypermarché) ;

\- une bonne information quant aux moyens de faire valoir ses droits (droit d'accès, droit de rectification, droit d'opposition, droit au déréférencement, etc) ; ce qui permettra à titre d’illustration aux personnes dont des traitements de données tels le TAJ (traitement des antécédents judiciaires du ministère de l’intérieur) ou le STIC (Système de traitement des infractions constatées) contiennent des données erronées ou ne devant pas/plus y figurer, et entravent leurs projets (par exemple accéder à un certain type de métier soumis à autorisation administrative, comme agent de sécurité, etc.), ...

La loi actuelle reste en effet trop vague. L’article 11 de la loi de 1978 dispose seulement que la CNIL “1° (...) informe toutes les personnes concernées et tous les responsables de traitements de leurs droits et obligations ;”. Il s’agit aussi de donner les moyens et le cadre légal à la CNIL de renforcer son programme dit “collectif EDUCNUM” (qui a été initié par la CNIL depuis seulement mai 2013 et qui regroupe aujourd’hui 60 acteurs de l’éducation, de la recherche et de l’économie numérique).

Par cet amendement, nous donnons la possibilité aux départements, universités, académies et rectorats volontaires qui en formulent la demande de pouvoir bénéficier de l’appui et des compétences de la CNIL pour informer le public et/ou les élèves des enjeux liés aux droits et libertés numériques, aux moyens de se prémunir d’atteintes qui pourraient leur être faits et de les faire valoir.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-premier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/apres-l-article-premier/amendement-247" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

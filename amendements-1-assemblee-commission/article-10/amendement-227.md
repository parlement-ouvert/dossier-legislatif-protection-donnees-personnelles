<!-- TITLE: Amendement n° 227 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

[ARTICLE 10](/pjl-490/titre-ii/chapitre-iii/article-10)

# Titre

Rédiger ainsi cet article :

L’article 35 de la loi n° 78‑17 du 6 janvier 1978 précitée est ainsi modifié :

1° Au début du premier alinéa, est ajoutée la mention : « I. – » ;

2° Sont ajoutés deux alinéas ainsi rédigés :

« Le présent I est applicable aux traitements ne relevant ni du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité, ni du chapitre XIII de la présente loi.

« II. – Dans le champ d’application du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité, le sous-traitant respecte les conditions prévues par ce règlement. »

# Exposé sommaire

Le présent amendement vise à clarifier le champ des obligations applicables aux sous-traitants :

– les dispositions actuelles de l'article 35 s'appliqueront aux sous-traitants de responsables de traitements ne relevant ni du règlement général sur la protection des données, ni de la directive relative aux traitements de données à des fins pénales, principalement les fichiers de souveraineté, les traitements de données de personnes décédées ou les traitements exclus par l'article 2 du RGPD (I) ;

– les obligations à la charge des sous-traitants de responsables de traitements entrant dans le champ du règlement seront celles prévues au chapitre IV (et au chapitre V ?) de ce règlement (II).

Les obligations des sous-traitants de responsables de traitements à des fins pénales, relevant de la directive, seront celles mentionnées au nouvel article 70-10.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

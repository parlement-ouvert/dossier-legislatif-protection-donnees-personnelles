<!-- TITLE: Amendement n° 47 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-9" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 7](/pjl-490/titre-i/chapitre-ii/article-7), insérer l'article suivant:

# Titre

Après le I de l’article 25 de la loi n° 78‑17 du 6 janvier 1978 précitée, il est inséré un I _bis_ ainsi r édigé :

« I _bis_. – En application du 5 de l’article 36 du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité, sont aussi mis en œuvre après autorisation de la Commission nationale de l’informatique et des libertés les traitements automatisés relevant des missions d’intérêt public, notamment dans les domaines sociaux, éducatifs, économiques, environnementaux, sanitaires, médico-sociaux, de l’énergie, des transports et culturels.

« La liste de ces missions est précisée par décret en Conseil d’État dans les trois mois à compter de la promulgation de la présente loi. »

# Exposé sommaire

Le règlement européen prévoit que les Etats peuvent définir des champs de “missions d’intérêt public” qui échappent à la fin de la nécessité d’une autorisation préalable. Or, le Gouvernement n’a pas du tout saisi cette possibilité pour mieux protéger les droits et libertés des citoyens en matière numérique.

Selon les termes mêmes du 5 de l’article 36 du règlement européen RGPD : “ _5\. Nonobstant le paragraphe 1, le droit des États membres peut exiger que les responsables du traitement consultent l'autorité de contrôle et obtiennent son autorisation préalable en ce qui concerne le traitement effectué par un responsable du traitement dans le cadre d'une mission d'intérêt public exercée par celui-ci, y compris le traitement dans le cadre de la protection sociale et de la santé publique_.”.

Nous proposons ainsi que le niveau normatif législatif précise les champs dits de “mission d’intérêt public”, et que le niveau normatif réglementaire fasse un recensement exhaustif. En effet, le Gouvernement peut fixer quels types de traitements de données dans ces domaines devront faire l’objet d’une autorisation, et qui s’appliquera aux responsables de traitements de données et à la CNIL.

En effet, il nous semble que de nombreux traitements de données massifs en termes de personnes concernées, d’ores et déjà existants ne sont pas concernés par le régime d’autorisation, ce alors même que leur caractère massif et du type de données traitées pourrait induire une telle autorisation administrative,  (traitements de données de l’Etat sur la paie des agents publics du type CHORUS, traitements de données de l’éducation nationale (notes, parents, adresses, etc), de Pôle emploi, des caisses d'assurance familiales).

C’est en ce sens que nous laissons au Gouvernement la possibilité de faire un recensement exhaustif des missions qui devraient faire l’objet d’une procédure d’autorisation par la CNIL eu égard au caractère d’intérêt public des missions exercées

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/apres-l-article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-9" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

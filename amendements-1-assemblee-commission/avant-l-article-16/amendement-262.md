<!-- TITLE: Amendement n° 262 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-16" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/avant-l-article-16/amendement-70" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 16](/pjl-490/titre-ii/chapitre-vi/article-16), insérer l'article suivant:

# Titre

L’article 43 _ter_ de la loi n ° 78-17 du 6 janvier 1978 précitée est ainsi modifié :

1° Le III est remplacé par un alinéa ainsi rédigé :

« Cette action peut être exercée en vue soit de la cessation du manquement mentionné au deuxième alinéa, soit de l’engagement de la responsabilité de la personne ayant causé le dommage afin d’obtenir la réparation des préjudices matériels et moraux subis, soit de ces deux fins. » ;

2° Le IV est complété par un alinéa ainsi rédigé :

« Lorsque l’action tend à la réparation des préjudices subis, elle s’exerce dans le cadre de la procédure individuelle de réparation définie au chapitre Ier du titre V de la loi n° 2016‑1547 du 18 novembre 2016 de modernisation de la justice du XXIe siècle. »

# Exposé sommaire

Cet amendement vise à introduire la possibilité d'exercer une action de groupe dans le domaine de la protection des données personnelles aux fin de réparation du dommage causé par un manquement aux dispositions de la loi n° 78-17 relative à l'informatique, aux fichiers et aux libertés, par un responsable de traitement de données à caractère personnel ou un sous-traitant.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-16" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/avant-l-article-16/amendement-70" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 70 -->
<!-- SUBTITLE: Retiré -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-16/amendement-262" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-16" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Éric Bothorel**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 16](/pjl-490/titre-ii/chapitre-vi/article-16), insérer l'article suivant:

# Titre

Après le mot : « tend », la fin du III de l’article 43 _ter_ de la loi n° 78-17 du 6 janvier 1978 précitée est ainsi rédigée : « à la cessation de ce manquement et à obtenir réparation du préjudice subi. »

# Exposé sommaire

L’article 80 du Règlement Général sur la Protection des Données n°2016/679 prévoit la possibilité pour les législateurs nationaux d’accorder aux personnes physiques le pouvoir de former une action de groupe afin d’obtenir réparation.

Or, l’article 43 ter limite la finalité de l’action de groupe à la seule cessation d’un manquement aux dispositions de la loi n°78-18. Cette limitation reviendrait à vider ce mécanisme juridique de son sens et de sa définition, mentionnée à l’article L. 623-1 du Code de la consommation, qui est le pouvoir « d’agir devant une juridiction civile afin d'obtenir la réparation des préjudices individuels subis par des consommateurs placés dans une situation similaire ou identique et ayant pour cause commune un manquement d'un ou des mêmes professionnels à leurs obligations légales ou contractuelles ».

Surtout, plusieurs rapports et organismes dénoncent depuis plusieurs années l’impuissance juridique des consommateurs français face aux acteurs de l’internet, souvent établis à l’étranger.

Dans sa délibération n° 2017-299 du 30 novembre 2017 la CNIL regrette explicitement l’absence de ce droit à réparation dans le projet de loi : « Elle regrette enfin que le projet de loi ne prévoit pas de disposition s’agissant de la possibilité de mandater ces mêmes organismes aux fins d’obtenir réparation du préjudice subi ».

Cette délibération préconise d’élargir les objectifs de l’action de groupe déjà existante, en donnant la possibilité aux citoyens de demander réparation de leur préjudice, comme le propose également le présent amendement.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-16/amendement-262" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-16" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 5 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-premier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-premier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Marietta Karamanli et les membres du groupe Nouvelle Gauche**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE PREMIER](/pjl-490/titre-i/chapitre-i/article-1), insérer l'article suivant:

# Titre

L’article 7 de la loi n° 78‑17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés est complété par un alinéa ainsi rédigé :

« Des référentiels et règlements types, au sens des _a _ bis et _b_ du 2° de l’article 11, relatifs à l’accessibilité et à la clarté des termes de la demande de consentement, propres à garantir un consentement libre et éclairé, sont établis par la Commission nationale de l’informatique et des libertés en concertation avec des organismes publics et privés en charge de la protection des personnes vulnérables. »

# Exposé sommaire

Afin de préciser les exigences du règlement en matière de clarté et d’accessibilité des termes de la demande de consentement, il est nécessaire de prendre en considération les capacités spécifiques des personnes en situation de vulnérabilité. De ce point de vue, un référentiel particulier destiné à orienter l’ergonomie des formulaires de demande de consentement doit être envisagé.

Cet amendement s'inspire des préconisations de la CNCDH sur le présent projet de loi.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-premier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-premier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

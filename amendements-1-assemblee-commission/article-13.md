<!-- TITLE: Article 13 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-12/amendement-36" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-13/amendement-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 53 — Non soutenu](/amendements-1-assemblee-commission/article-13/amendement-53)
* [Amendement n° 20 — Non soutenu](/amendements-1-assemblee-commission/article-13/amendement-20)
* [Amendement n° 142 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-142)
* [Amendement n° 143 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-143)
* [Amendement n° 144 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-144)
* [Amendement n° 208 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-208)
* [Amendement n° 145 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-145)
* [Amendement n° 146 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-146)
* [Amendement n° 147 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-147)
* [Amendement n° 258 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-258)
* [Amendement n° 257 — Retiré](/amendements-1-assemblee-commission/article-13/amendement-257)
* [Amendement n° 149 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-149)
* [Amendement n° 265 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-265)
* [Amendement n° 256 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-256)
* [Amendement n° 150 — Adopté](/amendements-1-assemblee-commission/article-13/amendement-150)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-12/amendement-36" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-13/amendement-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 261 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-6/amendement-120" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-6/amendement-121" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Amendement du Gouvernement

[ARTICLE 6](/pjl-490/titre-i/chapitre-i/article-6)

# Titre

I – À l’alinéa 8, substituer aux mots :

« ou la défense »,

les mots :

« , la défense ou de ceux relevant du chapitre XIII de la présente loi lorsqu’ils sont mis en œuvre pour le compte de l’État ».

II. – En conséquence, à l’alinéa 19, supprimer les mots :

« et ceux mentionnées à l’article 27 ».

III. – En conséquence, après le mot :

« qui »,

rédiger ainsi la fin de l’alinéa 27 :

« intéressent la sûreté de l’État, la défense ou de ceux relevant du chapitre XIII lorsqu’ils sont mis en œuvre pour le compte de l’État ; ».

IV. – En conséquence, après le mot :

« qui »,

rédiger ainsi la fin de l’alinéa 28 :

« intéressent la sûreté de l’État, la défense ou de ceux relevant du même chapitre XIII lorsqu’ils sont mis en œuvre pour le compte de l’État ; ».

V. – En conséquence, après le mot :

« qui »,

rédiger ainsi la fin de la première phrase de l’alinéa 34 :

« intéressent la sûreté de l’État, la défense ou de ceux relevant du chapitre XIII lorsqu’ils sont mis en œuvre pour le compte de l’État. »

VI. – En conséquence, à l’alinéa 37, substituer aux mots :

« régis par le chapitre XIII, lorsqu’une autorité de contrôle compétente en vertu du règlement (UE) 2016/679 »,

les mots :

« relevant du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité, lorsqu’une autorité de contrôle compétente en application de ce règlement ».

# Exposé sommaire

Cet amendement précise les dispositions de l’article 6 réécrivant les articles 45 et 46 de la loi de 1978 relatifs aux sanctions et décisions que pourra prendre la Commission nationale de l’informatique et des libertés en cas de non-respect par le responsable d’un traitement de ses obligations, ces dispositions comportant en effet des incohérences s’agissant des catégories de traitements (relevant du règlement, de la directive ou du seul droit national) pouvant faire l’objet de ces décisions, au regard de ce qu’exigent ou permettent les instruments européens. Certaines de ces incohérences ont d’ailleurs été relevées par la CNIL dans son avis sur le projet de loi.

Il convient ainsi, dans le 3° du III de l’article 45 et dans le 1° du I de l’article 46 de ne pas interdire à la CNIL de communiquer à la personne une violation de donnée ou d’interrompre provisoirement un traitement parce qu’il s’agit de traitements relevant de l’article 27 modifié. En effet ces traitements relèveront du règlement, qui impose que de telles décisions puissent être prises par l’autorité de contrôle.

De même, il convient, dans le III du nouvel article 46, de ne pas limiter aux traitements relevant la directive la possibilité pour la CNIL de demander au comité européen de la protection des données un avis ou une décision urgente en application de l’article 66 du règlement, ces décisions devant concerner les traitements relevant du règlement.

De même encore, il convient, dans les 1° et 2° du I de l’article 46, de ne pas interdire à la CNIL d’interrompre provisoirement un traitement ou de limiter le traitement de certaines données, dès lors qu’il s’agit d’un traitement mentionné aux I et II de l’article 26, car cela exclut les traitements de sécurité publique qui relèvent du règlement, et pour lesquels ces décisions doivent pouvoir être prises. Seuls doivent être exclus les traitements qui intéressent la sûreté de l’Etat ou la défense, ou les traitements de nature pénale relevant de la directive et mis en œuvre pour le compte de l’Etat.

Ces derniers traitements, doivent être également exclus de la possibilité pour la CNIL, prévue par le 3° du II de l’article 45, d’ordonner la limitation ou l’interdiction du traitement.

En effet, la directive n’impose pas de donner de telles prérogatives à la CNIL, qui seraient d’ailleurs en contradiction avec le choix fait par le présent projet de loi maintenir pour ces traitements un acte réglementaire de création pris après avis de CNIL.

Il convient enfin de prévoir expressément qu’en application du 8° du I de l’article 46, pour les traitements qui intéressent la sûreté de l’Etat ou la défense ou pour ceux de nature pénale relevant de la directive et mis en œuvre pour le compte de l’Etat, la formation restreinte de la CNIL puisse informer le Premier ministre afin qu’il fasse cesser la violation constatée.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-6/amendement-120" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-6/amendement-121" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

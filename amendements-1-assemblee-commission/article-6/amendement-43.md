<!-- TITLE: Amendement n° 43 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-6/amendement-86" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-6/amendement-8" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE 6](/pjl-490/titre-i/chapitre-i/article-6)

# Titre

Compléter l'alinéa 12 par la phrase suivante :

« Pour les hypothèses prévues aux articles 4 à 6 de l’article 83 du règlement (UE) n° 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité, en cas de récidive, les plafonds d’amendes administratives peuvent être multipliés par deux. »

# Exposé sommaire

Le règlement européen RGPD prévoit des sanctions pouvant s’élever de 10 000 / 20 000 euros ou, dans le cas d’une entreprise, jusqu’à 2 % / 4% du chiffre d’affaires annuel mondial total de l’exercice précédent. Ceci permettra de pouvoir exercer un effet réellement dissuasif sur les grandes entreprises (notamment les GAFA) qui pourraient, à défaut d’amendes aux montants dissuasifs, considérer que l’illégalité n’est qu’un risque financier qui vaut la peine d’être couru.

Le législateur européen n’ayant pas épuisé sa compétence sur le cas précis d’une récidive en l’état du règlement RGPD, le législateur national peut tout à fait décider de préciser et compléter les dispositions européennes.

En transposant par ailleurs le raisonnement de cet arrêt du droit administratif qu’était la jurisprudence CE, 18 Avril 1902, Commune De Néris Le-Bains, nous pouvons estimer qu’eu égard aux circonstances propres à la France (une attention particulière menée contre certaines entreprises attirées par les perspectives de lucre et récidivistes <http://www.zdnet.fr/actualites/geolocalisation-la-cnil-inflige-100-000-euros-d-amende-a-google-39759236.htm>, et plus récemment <http://www.lemonde.fr/pixels/article/2017/05/16/donnees-personnelles-facebook-condamne-par-la-cnil-a-150-000-euros-d-amende_5128370_4408996.html>), il est pleinement justifié de recourir à un complément de sanctions administratives dans le sens de la rigueur.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-6/amendement-86" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-6/amendement-8" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 8 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-6/amendement-43" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-6/amendement-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Marietta Karamanli et les membres du groupe Nouvelle Gauche**

[ARTICLE 6](/pjl-490/titre-i/chapitre-i/article-6)

# Titre

Après l’alinéa 12 insérer l’alinéa suivant :

« « Dans le cas où le responsable de traitement est une personne physique, le montant de la sanction administrative ne peut excéder 45 000 euros d’amende. »

# Exposé sommaire

Conformément aux termes du RGPD, hérités de la directive 95/46, une personne physique traitant les données d’un tiers dans un cadre qui n’est pas « strictement personnel et domestique » (art. 2), doit être considérée comme un responsable de traitement. L’arrêt Lindqvist de la Cour de justice de l’Union européenne, en 2003, a d’ailleurs rappelé cet élément de définition. En d’autres termes, et pour prendre un cas d’espèce, il est envisageable qu’une personne physique ayant diffusé des données à caractère personnel d’un tiers, sans son consentement, sur un réseau social, fasse l’objet de poursuites devant la CNIL et soit soumise aux sanctions administratives prévues par la loi. Pour mémoire, ces sanctions s’élèvent à dix millions d’euro. Appliquées à une personne physique, elles paraissent disproportionnées.

Il n’est pas souhaitable, cependant, d’exclure les personnes physiques du champ de ces sanctions. En effet, compte tenu de l’arrêt n°780 du 16 mars 2016 de la Chambre criminelle de la Cour de cassation, d’une part, et du caractère limité des modifications du code pénal apportées par l’article 67 de la loi République numérique, d’autre part, les sanctions prévues par l’actuel projet de loi constituent un levier utile dans la lutte contre les cyber-persécutions.

Il s’agit néanmoins de ramener les sanctions à des montants maximums réalistes du point de vue des capacités financières des personnes physiques.

Cet amendement s'inspire des préconisations de la CNCDH sur le présent projet de loi.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-6/amendement-43" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-6/amendement-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

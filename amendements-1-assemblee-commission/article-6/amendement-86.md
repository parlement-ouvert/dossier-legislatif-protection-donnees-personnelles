<!-- TITLE: Amendement n° 86 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-6/amendement-263" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-6/amendement-43" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Éric Bothorel**

[ARTICLE 6](/pjl-490/titre-i/chapitre-i/article-6)

# Titre

I. – Au début de l’alinéa 11, substituer aux mots :

« Le retrait »,

les mots :

« La suspension partielle ».

II. – En conséquence, après le mot :

« approbation »,

rédiger ainsi la fin du même alinéa :

« des règles d’entreprise contraignantes ; ».

# Exposé sommaire

Le retrait de la décision d’approbation d’une règle d’entreprise contraignante (ou « BCR ») envisagée au 6° du II de l’article 6 est un acte administratif lourd de conséquences pour les entreprises visées ; il entraine pour elles une forte insécurité juridique sur un temps très long (délai requis pour l’approbation de nouveaux BCR) alors même que les BCR ont été élaborés en concertation étroite avec la CNIL qui en prône le développement.

Ce type d’accord est, en outre, le fruit d’une coopération entre autorité puisque, conformément à l’article 47 du RGPD, l’autorité de contrôle compétente (en l’espèce, la CNIL) approuve les règles d’entreprise contraignantes conformément au mécanisme de contrôle de la cohérence prévu à l’article 63 du RGPD. Remettre en cause unilatéralement cet accord reviendrait à nier ce mécanisme de contrôle européen et à rompre l’égalité entre autorités de contrôle dans le traitement de cet outil.

Il semble donc opportun de mieux proportionner la sanction envisagée en prévoyant une suspension partielle permettant aux entreprises un dialogue constructif avec la CNIL pour remédier rapidement à leurs méconnaissances des textes.

Par ailleurs, il est proposé de reprendre le pluriel pour le terme « règles d’entreprise contraignantes » utilisé dans l’article 47 du RGPD.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-6/amendement-263" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-6/amendement-43" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

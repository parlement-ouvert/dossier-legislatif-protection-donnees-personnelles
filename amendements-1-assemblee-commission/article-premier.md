<!-- TITLE: Article PREMIER -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-premier/amendement-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-premier/amendement-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 13 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-13)
* [Amendement n° 239 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-239)
* [Amendement n° 240 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-240)
* [Amendement n° 21 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-21)
* [Amendement n° 89 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-89)
* [Amendement n° 90 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-90)
* [Amendement n° 56 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-56)
* [Amendement n° 241 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-241)
* [Amendement n° 242 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-242)
* [Amendement n° 243 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-243)
* [Amendement n° 23 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-23)
* [Amendement n° 244 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-244)
* [Amendement n° 22 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-22)
* [Amendement n° 245 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-245)
* [Amendement n° 91 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-91)
* [Amendement n° 92 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-92)
* [Amendement n° 246 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-246)
* [Amendement n° 93 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-93)
* [Amendement n° 94 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-94)
* [Amendement n° 96 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-96)
* [Amendement n° 264 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-264)
* [Amendement n° 24 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-24)
* [Amendement n° 57 — Retiré](/amendements-1-assemblee-commission/article-premier/amendement-57)
* [Amendement n° 65 — Retiré](/amendements-1-assemblee-commission/article-premier/amendement-65)
* [Amendement n° 73 — Retiré](/amendements-1-assemblee-commission/article-premier/amendement-73)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-premier/amendement-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-premier/amendement-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

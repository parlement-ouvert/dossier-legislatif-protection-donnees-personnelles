<!-- TITLE: Article 4 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-3/amendement-97" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-4/amendement-250" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 250 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-250)
* [Amendement n° 98 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-98)
* [Amendement n° 64 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-64)
* [Amendement n° 76 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-76)
* [Amendement n° 28 — Retiré](/amendements-1-assemblee-commission/article-4/amendement-28)
* [Amendement n° 77 — Rejeté](/amendements-1-assemblee-commission/article-4/amendement-77)
* [Amendement n° 58 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-58)
* [Amendement n° 63 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-63)
* [Amendement n° 72 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-72)
* [Amendement n° 78 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-78)
* [Amendement n° 62 — Rejeté](/amendements-1-assemblee-commission/article-4/amendement-62)
* [Amendement n° 79 — Rejeté](/amendements-1-assemblee-commission/article-4/amendement-79)
* [Amendement n° 61 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-61)
* [Amendement n° 80 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-80)
* [Amendement n° 100 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-100)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-3/amendement-97" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-4/amendement-250" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

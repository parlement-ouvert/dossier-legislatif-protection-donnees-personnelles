<!-- TITLE: Amendement n° 225 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-23/amendement-205" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-23/amendement-206" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

[ARTICLE 23](/pjl-490/titre-v/article-23)

# Titre

Après l’alinéa 4, insérer l’alinéa suivant :

« I _bis_. – À la dernière phrase du deuxième alinéa de l’article 230‑9 du même code, les mots : « d’un » sont remplacés par les mots : « de deux ». »

# Exposé sommaire

L’article 23 du projet de loi modifie l’article 230‑8 du code de procédure pénale relatif au traitement des antécédents judiciaires (TAJ) pour tenir compte de la décision du Conseil constitutionnel du 27 octobre 2017\. Il précise ainsi les suites que le procureur de la République doit donner aux demandes d’effacement ou de rectification de données, en prévoyant un délai de réponse de deux mois.

Par coordination, le présent amendement prévoit un même délai de deux mois lorsque ces demandes sont adressées au magistrat-référent du traitement en application de l’article 230‑9 du même code.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-23/amendement-205" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-23/amendement-206" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

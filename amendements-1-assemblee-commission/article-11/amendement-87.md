<!-- TITLE: Amendement n° 87 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-11/amendement-228" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-11/amendement-141" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Constance Le Grip**

[ARTICLE 11](/pjl-490/titre-ii/chapitre-iv/article-11)

# Titre

À l’alinéa 6, après le mot :

« permettre »,

insérer les mots :

« d’assurer leur conformité à toutes les obligations légales et ».

# Exposé sommaire

L’amendement proposé vise à répondre aux différentes obligations légales auxquelles les entreprises sont soumises au-delà de la problématique liée à la protection des données personnelles.

En effet, certains dispositifs ne peuvent être respectés sans le traitement de données personnelles même si les traitements ne sont pas expressément prévus et organisés par ces textes nationaux ou internationaux.

Dès lors que le traitement de telles données est nécessaire pour respecter ces dispositifs, l’entreprise doit pouvoir les traiter avec toutes les garanties prévues par le règlement (UE) 2016/679 et le principe de responsabilisation (voir en ce sens les dispositions de la loi Sapin II).

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-11/amendement-228" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-11/amendement-141" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

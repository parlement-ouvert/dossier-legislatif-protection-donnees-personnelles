<!-- TITLE: Amendement n° 228 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-11/amendement-35" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-11/amendement-87" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

[ARTICLE 11](/pjl-490/titre-ii/chapitre-iv/article-11)

# Titre

À l’alinéa 4, après le mot :

« avis »,

insérer les mots :

« motivé et publié ».

# Exposé sommaire

Cet amendement, de cohérence, précise que l’avis de la CNIL sur le projet de décret fixant la liste des personnes collaborant au service public de la justice habilitées à procéder au traitement de données pénales devra être motivé et publié.

Cette précision figure à de nombreux endroits de la loi de 1978 dès lors que la CNIL est appelée à émettre un avis sur un décret ou un arrêté (articles 22 et 25 sur le décret relatif aux opérations cryptographiques applicables à certains traitements du NIR, article 40‑1 sur le décret relatif au registre unique des directives d’une personne après son décès, article 69 sur le décret relatif aux exceptions à l’interdiction de transfert de données personnelles vers un État non membre de l’UE qui n’assure pas un niveau de protection suffisant de la vie privée et des libertés et droits fondamentaux...). En principe, la publicité des avis de la CNIL est garanti par le a) du 4° de l’article 11 de la loi de 1978 qui dispose que « _ lorsqu’une loi prévoit qu’un décret ou un arrêté est pris après avis de la commission, cet avis est publié avec le décret ou l’arrêté_ ».

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-11/amendement-35" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-11/amendement-87" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

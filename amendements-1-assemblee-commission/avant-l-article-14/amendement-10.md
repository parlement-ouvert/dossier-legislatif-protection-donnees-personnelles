<!-- TITLE: Amendement n° 10 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-234" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-55" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Gosselin**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 14](/pjl-490/titre-ii/chapitre-v/article-14), insérer l'article suivant:

# Titre

La section 1 du chapitre II de la loi n° 78‑17 du 6 janvier 1978 précitée est complétée par un article 7 _bis_ ainsi r édigé :

« _Art. 7 _bis. – Le traitement de données à caractère personnel de mineurs d’au moins quinze ans est licite sous réserve de respecter les dispositions du présent article 7 et de la présente loi. Le traitement de données à caractère personnel de mineurs de moins de quinze ans n’est licite que sous réserve d’avoir obtenu le consentement du titulaire de la responsabilité parentale à l’égard du mineur. »

# Exposé sommaire

Selon le Règlement européen (RGPD), tout traitement de données personnelles concernant des mineurs de moins de 16 ans doit être soumis au consentement préalable du titulaire de l’autorité parentale mais il prévoit que les états membres peuvent descendre jusqu'à l'âge de 13 ans (l'âge de consentement en Espagne et en République Tchèque est de 13 ans pour l'accès à certains services de l'information).

Toutefois, le projet de loi actuel n'a pas souhaité revenir dessus. Aux termes des auditions, beaucoup d'acteurs ont rappelé l'"hypocrisie" de cet âge invoquant que les mineurs de moins de 16 ans s'inscriront quand même sur les réseaux sociaux bafouant ces dispositions.

Si l'âge de 13 ans semble trop jeune pour ne pas bénéficier du consentement de l'autorité parentale, l'âge de 15 ans, calqué sur l'âge de la majorité sexuelle, semble un bon compromis. Tel est l'objet de cet amendement.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-234" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-55" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 4 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Marietta Karamanli et les membres du groupe Nouvelle Gauche**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 14](/pjl-490/titre-ii/chapitre-v/article-14), insérer l'article suivant:

# Titre

Le premier alinéa de l’article L. 311‑3‑1 du code des relations entre le public et l’administration est ainsi modifié :

1° Après le mot : « sont », il est inséré le mot : « systématiquement » ;

2° À la fin, les mots : « à l’intéressé s’il en fait la demande » sont remplacés par les mots : « aux intéressés ».

# Exposé sommaire

Cet amendement vise à renforcer les obligations d'information du public par l'administration lorsque celle-ci a décidé de recourir à un algorithme.

Alors que l'article L. 311-3-1 du code des relations entre le public et l'administration, dans sa rédaction actuelle, prévoit que les informations concernant le fonctionnement de l'algorithme sont communiquées par l'administration à l'intéressé "s'il en fait la demande", le groupe Nouvelle Gauche propose que cette information soit systématique.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 11 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-55" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Gosselin**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 14](/pjl-490/titre-ii/chapitre-v/article-14), insérer l'article suivant:

# Titre

Le III de l’article 32 de la loi n° 78‑17 du 6 janvier 1978 précitée est complété par un alinéa ainsi rédigé :

« Lorsque les données sont collectées auprès de mineurs de moins de seize ans, le responsable de traitement transmet au mineur les informations mentionnées au I de l’article 32 dans un langage clair et facilement accessible. »

# Exposé sommaire

Un amendement propose d'abaisser l'âge de consentement de l'autorité parentale à 15 ans. En contrepartie de cet assouplissement, il faut renforcer les modalités de contrôle des responsables afin qu'ils puissent informer au mieux les mineurs de moins de seize ans pour qu'ils soient en mesure de savoir comment leurs données personnelles sont traitées, ainsi que la manière dont elles le sont. Tel est l'objet de cet amendement.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-55" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 234 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 14](/pjl-490/titre-ii/chapitre-v/article-14), insérer l'article suivant:

# Titre

La section 1 du chapitre II de la loi n° 78‑17 du 6 janvier 1978 précitée est complétée par un article 7 _bi_ s ainsi rédigé :

« _Art. 7 _bis. – En application du 1 de l’article 8 du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité, un mineur peut consentir seul à un traitement de données à caractère personnel en ce qui concerne l’offre directe de services de la société de l’information à compter de l’âge de quinze ans.

« Un mineur âgé de moins de quinze ans peut être autorisé par le ou les titulaires de l’autorité parentale à l’égard de ce mineur à consentir seul à un traitement mentionné au premier alinéa du présent article.

« Le responsable de traitement rédige en des termes clairs et simples, aisément compréhensibles par le mineur, toute information et communication relatives au traitement qui le concerne. »

# Exposé sommaire

Le règlement européen sur la protection des données fixe à 16 ans l’âge à partir duquel un mineur peut consentir seul au traitement de ses données. En dessous de cet âge, le consentement de ses parents sera nécessaire. Il autorise toutefois les États à abaisser ce seuil à 13, 14 ou 15 ans, marge de manœuvre qui a été largement utilisé par nos voisins.

Cette situation, protectrice des intérêts de l’enfant dans l’univers numérique, traite toutefois de manière homogène et indiscriminée un enfant pré-adolescent et un adolescent. Or, un enfant de 11 ans ne peut pas être traité de la même manière qu’un jeune de 15 ans. Il convient également de prendre en compte les seuils établis dans d’autres domaines, comme en matière d’opposition à l’accès des parents aux données de santé (15 ans) ou dans le domaine encore plus sensible de la majorité sexuelle (fixée, en droit français, à 15 ans).

C’est pourquoi le présent amendement propose un nouvel équilibre dans la protection des enfants face au traitement de leurs données personnelles.

En premier lieu, il abaisse de 16 à 15 ans l’âge à partir duquel un mineur pourra consentir seul au traitement des données qui le concerne.

En deuxième lieu, et afin d’accompagner les enfants dans leur apprentissage de l’univers numérique et des réseaux sociaux, il soumet le traitement de données relatives à un mineur de moins de 15 ans au double consentement des parents et du mineur.

En dernier lieu, il impose aux responsables de traitements une obligation d’information du mineur dans des termes adaptés à son âge. En effet, les enfants doivent être accompagnés dans leur apprentissage d’internet et de ses usages, en particulier au moment de leur inscription sur les réseaux sociaux.

Par ailleurs, cette proposition n’est pas exclusive des initiatives de « droit souple » qui pourront être prises afin d’encourager les acteurs du numérique au respect de règles protectrices des intérêts de l’enfant sur internet (blocage de certaines fonctionnalités, absence de référencement des profils de mineurs sur les moteurs de recherche, facilitation des mécanismes de signalement...). Ces initiatives pourraient parfaitement s’inscrire dans la mission de labellisation de produits et de procédures reconnue à la CNIL au c) du 3° de l’article 11 de la loi de 1978.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 55 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Latombe**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 14](/pjl-490/titre-ii/chapitre-v/article-14), insérer l'article suivant:

# Titre

La section 1 du chapitre II de la loi n° 78‑17 du 6 janvier 1978 précitée est complétée par un article 7 _bis_ ainsi r édigé :

« _Art. 7_ bis _._ – Le traitement de données à caractère personnel d’un mineur de moins de quinze ans n’est autorisé qu’avec le consentement exprès du titulaire de l’autorité parentale dudit mineur. »

# Exposé sommaire

Il est proposé d'abaisser de 16 à 15 ans l'âge en deça duquel il est nécessaire d'obtenir le consentement des parents, conformément à la marge de manœuvre autorisée à l'article 8 du règlement 2016/679

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/avant-l-article-14/amendement-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

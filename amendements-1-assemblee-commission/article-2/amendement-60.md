<!-- TITLE: Amendement n° 60 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-2/amendement-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-2/amendement-66" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Gosselin**

[ARTICLE 2](/pjl-490/titre-i/chapitre-i/article-2)

# Titre

Rédiger ainsi cet article :

Le I de l’article 13 de la loi n° 78‑17 du 6 janvier 1978 précitée est ainsi modifié :

1° Au 6°, le mot : « ou » est remplacé par le mot : « et » ;

2° Au 7°, après le mot : « numérique », sont insérés les mots : « et des questions touchant aux libertés individuelles ».

# Exposé sommaire

Le présent amendement vise à étendre le champ des qualifications des trois personnalités nommées
par décret ainsi que des deux personnalités désignées respectivement par le Président de
l’Assemblée nationale et par le Président du Sénat qui siègent à la Commission nationale de
l’informatique et des libertés (CNIL).
Il est en effet primordial que les membres de la CNIL possèdent une double expertise dans les
questions touchant au numérique et aux libertés individuelles de façon à améliorer la qualité des
décisions rendues par ladite commission.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-2/amendement-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-2/amendement-66" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 42 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-20/amendement-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-20/amendement-214" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE 20](/pjl-490/titre-iv/article-20)

# Titre

Supprimer cet article.

# Exposé sommaire

Première “fake news“. En effet, contrairement à ce qui avait été indiqué à la presse en novembre (<https://www.nextinpact.com/brief/rgpd---le-projet-de-loi---relatif-a-la-protection-des-donnees-personnelles---se-devoile-1822.htm>), une habilitation à légiférer par voie d’ordonnance est belle et bien intégrée au projet de loin, dans cet article.

Qu’en tirer comme conclusions ? Soit le Gouvernement ment - ou tout du moins est inconstant, versatile, revient sur ses annonces et engagements -, soit il est incompétent. Dans tous les cas, la réponse est en droit de nous inquiéter en tant que représentation nationale.

Cet article est le symbole d’une précipitation d’amateurs et d’un mépris pour un domaine aussi fondamental que les droits et libertés numériques. C’est le Conseil d’Etat qui l’a rappelé dans son avis (<http://www.assemblee-nationale.fr/15/pdf/projets/pl0490-ace.pdf>) : les règles qui sont transposées ici constituent une nouveauté juridique et sont d’une ampleur de fait inédite / ce projet de loi a été élaboré à la va-vite et le Gouvernement souhaite le faire voter dans la précipitation. Tout comme lui, la CNIL a dénoncé l’empressement du Gouvernement et dit qu’elle n’a pas pu se prononcer sérieusement (<https://www.cnil.fr/sites/default/files/atoms/files/projet_davis_cnil.pdf>).

Il s’agit de la plus grande révision de loi de 1978 relative à l'informatique depuis 13 ans (2004), aux fichiers et aux libertés, et elle est faite sans grande préparation - aucune concertation majeure en amont. Le Gouvernement a déposé ce projet de loi le 13 décembre 2017 en procédure accélérée, alors que les règles européennes entreront automatiquement en vigueur les 6 et 25 mai 2018. De fait, le Gouvernement souhaite faire adopter un texte en très peu de temps,… qu’il ajustera après… par ordonnance. Selon la même abjecte méthode qu'il a utilisée pour le code du travail à la fin 2017.

Cette impréparation et ce mépris pour le débat parlementaire ne sont pas à la hauteur de l’enjeu des droits et libertés à l'ère numérique.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-commission/article-20/amendement-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à la commission de lois de l'Assemblée</span> <a href="/amendements-1-assemblee-commission/article-20/amendement-214" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

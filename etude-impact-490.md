<!-- TITLE: Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/introduction-generale" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Entête](/etude-impact-490/entete)

* [INTRODUCTION GENERALE](/etude-impact-490/introduction-generale)

* [TABLEAU RÉCAPITULATIF DES TEXTES D’APPLICATION DU PROJET DE LOI](/etude-impact-490/tableau-recapitulatif-des-textes-dapplication-du-projet-de-loi)

* [TABLEAU SYNOPTIQUE DES CONSULTATIONS OBLIGATOIRES](/etude-impact-490/tableau-synoptique-des-consultations-obligatoires)

* [TITRE IER — DISPOSITIONS COMMUNES AU REGLEMENT (UE) 2016/679 DU PARLEMENT EUROPEEN ET DU CONSEIL DU 27 AVRIL 2016 ET A LA DIRECTIVE (UE) 2016/680 DU PARLEMENT EUROPEEN ET DU CONSEIL DU 27 AVRIL 2016](/etude-impact-490/titre-ier)

  * [CHAPITRE IER — DISPOSITIONS RELATIVES A LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier)

    * [ARTICLE 1ER — MISSIONS DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier/article-1er)
    * [ARTICLES 2 et 3 — COMMISSAIRE DU GOUVERNEMENT ET MEMBRES DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier/articles-2-et-3)
    * [ARTICLE 4 — POUVOIRS DE CONTRÔLE DE LA CNIL](/etude-impact-490/titre-ier/chapitre-ier/article-4)
    * [ARTICLE 5 — PROCÉDURE DE COOPÉRATION DE LA CNIL AVEC LES AUTRES AUTORITÉS DE CONTRÔLE](/etude-impact-490/titre-ier/chapitre-ier/article-5)
    * [ARTICLE 6 — MESURES CORRECTRICES ET SANCTIONS](/etude-impact-490/titre-ier/chapitre-ier/article-6)

  * [CHAPITRE II — DISPOSITIONS RELATIVES A CERTAINES CATEGORIES DE DONNEES](/etude-impact-490/titre-ier/chapitre-ii)

    * [ARTICLE 7 — DONNÉES SENSIBLES](/etude-impact-490/titre-ier/chapitre-ii/article-7)

* [TITRE II — MARGES DE MANOEUVRE PERMISES PAR LE REGLEMENT (UE) 2016/679 DU PARLEMENT EUROPEEN ET DU CONSEIL DU 27 AVRIL 2016 RELATIF A LA PROTECTION DES PERSONNES PHYSIQUES A L'EGARD DU TRAITEMENT DES DONNEES A CARACTERE PERSONNEL ET A LA LIBRE CIRCULATION DE CES DONNEES, ET ABROGEANT LA DIRECTIVE 95/46/CE](/etude-impact-490/titre-ii)

  * [CHAPITRE IER — CHAMP D’APPLICATION TERRITORIAL DES DISPOSITIONS COMPLETANT LE REGLEMENT (UE) 2016/679](/etude-impact-490/titre-ii/chapitre-ier)

    * [ARTICLE 8 — CRITERE D’APPLICATION DU DROIT](/etude-impact-490/titre-ii/chapitre-ier/article-8)

  * [CHAPITRE II — DISPOSITIONS RELATIVES A LA SIMPLIFICATION DES FORMALITES PREALABLES A LA MISE EN OEUVRE DES TRAITEMENTS](/etude-impact-490/titre-ii/chapitre-ii)

    * [ARTICLE 9 — ALLEGEMENT DES FORMALITES PREALABLES](/etude-impact-490/titre-ii/chapitre-ii/article-9)

  * [CHAPITRE III — OBLIGATIONS INCOMBANT AUX RESPONSABLES DE TRAITEMENTS ET SOUS-TRAITANTS](/etude-impact-490/titre-ii/chapitre-iii)

    * [ARTICLE 10 — SOUS-TRAITANT](/etude-impact-490/titre-ii/chapitre-iii/article-10)

  * [CHAPITRE IV — DISPOSITIONS RELATIVES A CERTAINES CATEGORIES PARTICULIERES DE TRAITEMENT](/etude-impact-490/titre-ii/chapitre-iv)

    * [ARTICLE 11 — DONNEES D’INFRACTION](/etude-impact-490/titre-ii/chapitre-iv/article-11)
    * [ARTICLE 12 — TRAITEMENTS ARCHIVISTIQUES](/etude-impact-490/titre-ii/chapitre-iv/article-12)
    * [ARTICLE 13 — TRAITEMENTS DE DONNEES DE SANTE](/etude-impact-490/titre-ii/chapitre-iv/article-13)
    * [ARTICLE 14 — DECISION ADMINISTRATIVE AUTOMATISEE](/etude-impact-490/titre-ii/chapitre-iv/article-14)
    * [ARTICLE 15 — LIMITATION DES DROITS](/etude-impact-490/titre-ii/chapitre-iv/article-15)

  * [CHAPITRE V — VOIES DE RECOURS](/etude-impact-490/titre-ii/chapitre-v)

    * [ARTICLES 16 ET 17 — MODALITES D’EXERCICE DES VOIES DE RECOURS](/etude-impact-490/titre-ii/chapitre-v/articles-16-et-17)

* [TITRE III — DISPOSITIONS PORTANT TRANSPOSITION DE LA DIRECTIVE (UE) 2016/680 DU PARLEMENT EUROPEEN ET DU CONSEIL DU 27 AVRIL 2016 RELATIVE A LA PROTECTION DES PERSONNES PHYSIQUES A L'EGARD DU TRAITEMENT DES DONNEES A CARACTERE PERSONNEL PAR LES AUTORITES COMPETENTES A DES FINS DE PREVENTION ET DE DETECTION DES INFRACTIONS PENALES, D'ENQUETES ET DE POURSUITES EN LA MATIERE OU D'EXECUTION DE SANCTIONS PENALES, ET A LA LIBRE CIRCULATION DE CES DONNEES](/etude-impact-490/titre-iii)

  * [ARTICLES 18 ET 19 — PRESENTATION GENERALE ET DÉFINITIONS](/etude-impact-490/titre-iii/articles-18-et-19)
  * [ARTICLE 19 SECTION 1 — DISPOSITIONS GENERALES](/etude-impact-490/titre-iii/article-19-section-1)
  * [ARTICLE 19 SECTION 2 — OBLIGATIONS INCOMBANT AUX AUTORITES COMPETENTES ET AUX RESPONSABLES DE TRAITEMENT](/etude-impact-490/titre-iii/article-19-section-2)
  * [ARTICLE 18 ET ARTICLE 19 SECTION 3 — DROITS DE LA PERSONNE CONCERNÉE](/etude-impact-490/titre-iii/article-18-et-article-19-section-3)
  * [ARTICLE 19 SECTION 4 — TRANSFERTS INTERNATIONAUX](/etude-impact-490/titre-iii/article-19-section-4)

* [TITRE IV — HABILITATION A AMELIORER L’INTELLIGIBILITE DE LA LEGISLATION APPLICABLE A LA PROTECTION DES DONNEES](/etude-impact-490/titre-iv)

  * [ARTICLE 20](/etude-impact-490/titre-iv/article-20)

* [TITRE V — DISPOSITIONS DIVERSES ET FINALES144](/etude-impact-490/titre-v)

  * [ARTICLE 22 — MISE A DISPOSITION DE LA LISTE DES TRAITEMENTS AYANT FAIT L’OBJET DE FORMALITES PREALABLES](/etude-impact-490/titre-v/article-22)
  * [ARTICLE 23 — MODIFICATION DE L’ARTICLE 230-8 DU CODE DE PROCEDURE PENALE](/etude-impact-490/titre-v/article-23)
  * [ARTICLES 24](/etude-impact-490/titre-v/articles-24)

* [ANNEXE — TABLEAU DE CONCORDANCE ENTRE LES DISPOSITIONS DE LA DIRECTIVE ET CELLES DE LA LOI DE 1978](/etude-impact-490/annexe)

* [ANNEXE 2 — TABLEAU COMPARATIF - PROJET DE LOI RELATIF A LA PROTECTION DES DONNEES PERSONNELLES](/etude-impact-490/annexe-2)

* [Notes](/etude-impact-490/notes)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles</span> <a href="/etude-impact-490/introduction-generale" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

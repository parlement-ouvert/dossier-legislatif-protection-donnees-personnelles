<!-- TITLE: Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés</span> <a href="/avis-conseil-etat-393836/presentation-generale" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Conseil d'État

Séance du jeudi 7 décembre 2017

N° 393836

* [Présentation générale](/avis-conseil-etat-393836/presentation-generale)

  * [1.](/avis-conseil-etat-393836/presentation-generale/1)
  * [2.](/avis-conseil-etat-393836/presentation-generale/2)
  * [3.](/avis-conseil-etat-393836/presentation-generale/3)

* [Etude d’impact et consultations](/avis-conseil-etat-393836/etude-dimpact-et-consultations)

  * [4.](/avis-conseil-etat-393836/etude-dimpact-et-consultations/4)
  * [5.](/avis-conseil-etat-393836/etude-dimpact-et-consultations/5)

* [Choix légistiques et propositions pour une meilleure lisibilité du droit](/avis-conseil-etat-393836/choix-legistiques-et-propositions-pour-une-meilleure-lisibilite-du-droit)

  * [6.](/avis-conseil-etat-393836/choix-legistiques-et-propositions-pour-une-meilleure-lisibilite-du-droit/6)
  * [7.](/avis-conseil-etat-393836/choix-legistiques-et-propositions-pour-une-meilleure-lisibilite-du-droit/7)
  * [8.](/avis-conseil-etat-393836/choix-legistiques-et-propositions-pour-une-meilleure-lisibilite-du-droit/8)
  * [9.](/avis-conseil-etat-393836/choix-legistiques-et-propositions-pour-une-meilleure-lisibilite-du-droit/9)
  * [10.](/avis-conseil-etat-393836/choix-legistiques-et-propositions-pour-une-meilleure-lisibilite-du-droit/10)

* [Missions de la CNIL](/avis-conseil-etat-393836/missions-de-la-cnil)

  * [11.](/avis-conseil-etat-393836/missions-de-la-cnil/11)
  * [12.](/avis-conseil-etat-393836/missions-de-la-cnil/12)

* [Commissaire du Gouvernement et formation restreinte](/avis-conseil-etat-393836/commissaire-du-gouvernement-et-formation-restreinte)

  * [13.](/avis-conseil-etat-393836/commissaire-du-gouvernement-et-formation-restreinte/13)

* [Contrôles](/avis-conseil-etat-393836/controles)

  * [14.](/avis-conseil-etat-393836/controles/14)
  * [15.](/avis-conseil-etat-393836/controles/15)
  * [16.](/avis-conseil-etat-393836/controles/16)
  * [17.](/avis-conseil-etat-393836/controles/17)
  * [18.](/avis-conseil-etat-393836/controles/18)

* [Modalités de coopération entre autorités de contrôle européennes](/avis-conseil-etat-393836/modalites-de-cooperation-entre-autorites-de-controle-europeennes)

  * [19.](/avis-conseil-etat-393836/modalites-de-cooperation-entre-autorites-de-controle-europeennes/19)
  * [20.](/avis-conseil-etat-393836/modalites-de-cooperation-entre-autorites-de-controle-europeennes/20)

* [Formalités préalables](/avis-conseil-etat-393836/formalites-prealables)

  * [21.](/avis-conseil-etat-393836/formalites-prealables/21)
  * [22.](/avis-conseil-etat-393836/formalites-prealables/22)
  * [23.](/avis-conseil-etat-393836/formalites-prealables/23)
  * [24.](/avis-conseil-etat-393836/formalites-prealables/24)

* [Délégué à la protection des données](/avis-conseil-etat-393836/delegue-a-la-protection-des-donnees)

  * [25.](/avis-conseil-etat-393836/delegue-a-la-protection-des-donnees/25)
  * [26.](/avis-conseil-etat-393836/delegue-a-la-protection-des-donnees/26)

* [Données d’infraction](/avis-conseil-etat-393836/donnees-dinfraction)

  * [27.](/avis-conseil-etat-393836/donnees-dinfraction/27)
  * [28.](/avis-conseil-etat-393836/donnees-dinfraction/28)

* [Données de santé](/avis-conseil-etat-393836/donnees-de-sante)

  * [29.](/avis-conseil-etat-393836/donnees-de-sante/29)
  * [30.](/avis-conseil-etat-393836/donnees-de-sante/30)

* [Consentement des mineurs pour les autres traitements de données](/avis-conseil-etat-393836/consentement-des-mineurs-pour-les-autres-traitements-de-donnees)

  * [31.](/avis-conseil-etat-393836/consentement-des-mineurs-pour-les-autres-traitements-de-donnees/31)
  * [32.](/avis-conseil-etat-393836/consentement-des-mineurs-pour-les-autres-traitements-de-donnees/32)
  * [33.](/avis-conseil-etat-393836/consentement-des-mineurs-pour-les-autres-traitements-de-donnees/33)

* [Algorithmes](/avis-conseil-etat-393836/algorithmes)

  * [34.](/avis-conseil-etat-393836/algorithmes/34)
  * [35.](/avis-conseil-etat-393836/algorithmes/35)

* [Traitements mis en œuvre par l’Etat](/avis-conseil-etat-393836/traitements-mis-en-oeuvre-par-letat)

  * [36.](/avis-conseil-etat-393836/traitements-mis-en-oeuvre-par-letat/36)

* [Transfert de données personnelles vers un pays tiers](/avis-conseil-etat-393836/transfert-de-donnees-personnelles-vers-un-pays-tiers)

  * [37.](/avis-conseil-etat-393836/transfert-de-donnees-personnelles-vers-un-pays-tiers/37)
  * [38.](/avis-conseil-etat-393836/transfert-de-donnees-personnelles-vers-un-pays-tiers/38)
  * [39.](/avis-conseil-etat-393836/transfert-de-donnees-personnelles-vers-un-pays-tiers/39)

* [En ce qui concerne le titre III relatif à la transposition de la directive (UE) 2016/680](/avis-conseil-etat-393836/en-ce-qui-concerne-le-titre-iii-relatif-a-la-transposition-de-la-directive-ue-2016-680)

  * [40.](/avis-conseil-etat-393836/en-ce-qui-concerne-le-titre-iii-relatif-a-la-transposition-de-la-directive-ue-2016-680/40)

* [Données sensibles](/avis-conseil-etat-393836/donnees-sensibles)

  * [41.](/avis-conseil-etat-393836/donnees-sensibles/41)

* [Décision individuelle automatisée](/avis-conseil-etat-393836/decision-individuelle-automatisee)

  * [42.](/avis-conseil-etat-393836/decision-individuelle-automatisee/42)

* [Formalités préalables à la création des traitements de données relevant du champ de la directive](/avis-conseil-etat-393836/formalites-prealables-a-la-creation-des-traitements-de-donnees-relevant-du-champ-de-la-directive)

  * [43.](/avis-conseil-etat-393836/formalites-prealables-a-la-creation-des-traitements-de-donnees-relevant-du-champ-de-la-directive/43)

* [Traitements de données relevant à la fois du champ de la directive et de celui du règlement ou du droit interne](/avis-conseil-etat-393836/traitements-de-donnees-relevant-a-la-fois-du-champ-de-la-directive-et-de-celui-du-reglement-ou-du-droit-interne)

  * [44.](/avis-conseil-etat-393836/traitements-de-donnees-relevant-a-la-fois-du-champ-de-la-directive-et-de-celui-du-reglement-ou-du-droit-interne/44)
  * [45.](/avis-conseil-etat-393836/traitements-de-donnees-relevant-a-la-fois-du-champ-de-la-directive-et-de-celui-du-reglement-ou-du-droit-interne/45)
  * [46.](/avis-conseil-etat-393836/traitements-de-donnees-relevant-a-la-fois-du-champ-de-la-directive-et-de-celui-du-reglement-ou-du-droit-interne/46)
  * [47.](/avis-conseil-etat-393836/traitements-de-donnees-relevant-a-la-fois-du-champ-de-la-directive-et-de-celui-du-reglement-ou-du-droit-interne/47)
  * [48.](/avis-conseil-etat-393836/traitements-de-donnees-relevant-a-la-fois-du-champ-de-la-directive-et-de-celui-du-reglement-ou-du-droit-interne/48)

* [Traitement des antécédents judiciaires](/avis-conseil-etat-393836/traitement-des-antecedents-judiciaires)

  * [49.](/avis-conseil-etat-393836/traitement-des-antecedents-judiciaires/49)

* [Application outre-mer](/avis-conseil-etat-393836/application-outre-mer)

  * [50.](/avis-conseil-etat-393836/application-outre-mer/50)

* [Entrée en vigueur](/avis-conseil-etat-393836/entree-en-vigueur)

  * [51.](/avis-conseil-etat-393836/entree-en-vigueur/51)

_Cet avis a été délibéré par l’assemblée générale du Conseil d’Etat dans sa séance du jeudi 7 décembre 2017._

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 11 — DONNEES D’INFRACTION](/etude-impact-490/titre-ii/chapitre-iv/article-11)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés</span> <a href="/avis-conseil-etat-393836/presentation-generale" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Comment contribuer -->
<!-- SUBTITLE: Les multiples manières de contribuer à l'amélioration de ce site -->

Ce dossier législatif est contributif. Il est possible d'y contribuer de différentes manières.
# Commentaires sur le projet de loi

Vous pouvez commenter les différents articles du projet de loi en écrivant dans le forum se trouvant en bas de chaque article.

Ainsi vous pouvez proposer des améliorations, répondre à des questions d'autres utilisateurs, etc.

# Liens manquants ou défectueux

Si vous rencontrez un lien manquant ou défectueux dans ce dossier législatif, vous pouvez effectuer vous même la correction.

Vous devez d'abord obtenir un compte avec les droits de modification sur ce site. Pour cela écrivez-nous à equipe@forteza.fr.

Ensuite, il ne reste plus qu'à vous identifier sur ce site, aller sur la page concernée, cliquer sur le bouton d'édition en haut à droite et effectuer la modification en utilisant la syntaxe Markdown.

Si vous savez utiliser Git, vous pouvez directement modifier les documents dans leur [dépôt Framagit](https://framagit.org/parlement-ouvert/dossier-legislatif-protection-donnees-personnelles).

Enfin, si vous êtes un développeur, vous pouvez aussi [améliorer le script de génération automatique de liens](https://framagit.org/parlement-ouvert/lielaloi). Pour plus d'information, référez-vous à l'[architecture technique du projet](/documentation-technique/fonctionnement).

# Erreurs dans les documents

Les documents du dossier législatif ont été convertis automatiquement en syntaxe Markdown à partir de pages HTML ou de fichiers PDF. Le résultat n'est donc pas toujours optimal.

Si vous trouvez un défaut, vous pouvez nous le signaler en écrivant à equipe@forteza.fr.

Mieux, vous pouvez effectuer vous même la correction en demandant à ce qu'on vous crée un compte.

Si vous êtes un développeur, vous pouvez aussi améliorer [les scripts de conversion](https://framagit.org/parlement-ouvert/prendlaloi) ou [les scripts de nettoyage](https://framagit.org/parlement-ouvert/lielaloi).

# Documents manquants

Si un document important manque ou si vous souhaitez apporter votre propre contribution, vous pouvez le déposer sur le [forum Parlement-ouvert.fr](https://forum.parlement-ouvert.fr/c/donnees-personnelles/dossier-legislatif).

Le mieux est de l'écrire en syntaxe Markdown afin d'en faciliter la lecture, mais vous pouvez le déposer sous n'importe quel format ouvert.

----

<div id='discourse-comments'></div>
<script type="text/javascript">
  DiscourseEmbed = {
		discourseUrl: 'https://forum.parlement-ouvert.fr/',
    discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/documentation-technique/contribution'
	};
  (function() {
    var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
    d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
  })();
</script>


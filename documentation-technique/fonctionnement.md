<!-- TITLE: Fonctionnement -->
<!-- SUBTITLE: Fonctionnement technique du site -->

# Présentation du fonctionnement technique du site

## Téléchargement

Les différents documents constituant le dossier législatif sont d'abord téléchargés.

## Conversion

Ensuite des [scripts](https://framagit.org/parlement-ouvert/prendlaloi)  déterminent la structure des documents, notamment pour en extraire les chapitres et les articles, puis convertissent ces différents éléments de structure en autant de fichiers au format Markdown[^1].

[^1]: L'utilisation de ces scripts de conversion en Markdown est documentée dans une [page dédiée](/documentation-technique/conversion-markdown).

## Stockage

Ces fichiers Markdown sont stockés dans un [dépôt Git public](https://framagit.org/parlement-ouvert/dossier-legislatif-protection-donnees-personnelles).

## Ajout des liens

Un autre [script](https://framagit.org/parlement-ouvert/lielaloi) parcourt ces fichiers Markdown, tente de détecter des références à d'autres articles et de remplacer ces références par des liens. Ces modifications sont ensuite proposées sous forme de « _merge requests_ » au dépôt Git des fichiers Markdown.

## Affichage et édition

Un [site web](/), basé sur le logiciel libre [Wiki.js](https://wiki.js.org/) affiche les fichiers Markdown, sous la forme d'un dossier législatif. Ce wiki permet d'éditer les fichiers Markdown et permet d'améliorer manuellement les documents.

Ainsi, des [bénévoles](https://framagit.org/parlement-ouvert/dossier-legislatif-protection-donnees-personnelles/graphs/master), en particuliers des membres du [bureau ouvert ](https://forteza.fr/index.php/2017/10/26/bureau-ouvert/), ont ajouté manuellement de (très très) nombreux liens manquants entre les documents.

Le wiki se synchronise automatiquement avec le [dépôt Git public](https://framagit.org/parlement-ouvert/dossier-legislatif-protection-donnees-personnelles), ce qui permet aussi de modifier les pages Markdown directement depuis Git ou GitLab.

## Commentaires

Enfin, un [forum](https://forum.parlement-ouvert.fr/), basé sur le logiciel libre [Discourse](https://www.discourse.org/), permet aux visiteurs de commenter certaines pages du Wiki (en particulier les différents articles du projet de loi).

----

<div id='discourse-comments'></div>
<script type="text/javascript">
  DiscourseEmbed = {
		discourseUrl: 'https://forum.parlement-ouvert.fr/',
    discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/documentation-technique/fonctionnement'
	};
  (function() {
    var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
    d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
  })();
</script>


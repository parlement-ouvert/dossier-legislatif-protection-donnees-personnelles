<!-- TITLE: Conversion Markdown -->
<!-- SUBTITLE: Conversion des documents du dossier législatif au format Markdown -->

# Récupération des documents originaux

```bash
mkdir dossier-legislatif-protection-donnees-personnelles-sources
cd dossier-legislatif-protection-donnees-personnelles-sources/
```

## Récupération de l'étude d'impact et des différentes version du projet de loi sur la protection des données personnelles

```bash
curl -o etude_impact_pjl_490_du_12_decembre_2017.html "http://www.assemblee-nationale.fr/15/projets/pl0490-ei.asp"
curl -o pjl_490_du_13_decembre_2017.html "http://www2.assemblee-nationale.fr/documents/notice/15/projets/pl0490/(index)/projets-loi"
curl -o pjl_592_du_25_janvier_2018.html "http://www.assemblee-nationale.fr/15/ta-commission/r0592-a0.asp"
```

## Récupération des textes européens

```bash
curl -o reglement_ue_2016-679_du_27_avril_2016.html "http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679"
curl -o directive_ue_2016-680_du_27_avril_2016.html "http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016L0680"
```

## Récupération des lois françaises

```bash
curl -o loi_78-17_du_6_janvier_1978.html "https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460"
curl -o loi_2016-1321_du_7_octobre_2016.html "https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746"
```

## Récupération de l'avis du Conseil d'État (en PDF) et conversion en HTML

_Note_ : Installer le paquet Debian `poppler-utils`, contenant le convertisseur `pdftohtml`.

```bash
curl -o avis_conseil_etat_393836_du_7_decembre_2017.pdf "http://www.conseil-etat.fr/content/download/121205/1225385/version/2/file/393836_AVIS.pdf"
pdftohtml -noframes avis_conseil_etat_393836_du_7_decembre_2017.pdf
```

## Récupération de la délibération de la CNIL sur le site Légifrance

```bash
curl -o deliberation_cnil_2017-299_du_30_novembre_2017.html "https://www.legifrance.gouv.fr/affichCnil.do?oldAction=rechExpCnil&id=CNILTEXT000036195647"
```

## Récupération des rapports d'information

```bash
curl -o rapport_information_4544_du_22_fevrier_2017.html "http://www.assemblee-nationale.fr/14/rap-info/i4544.asp"
curl -o html/rapport_information_577_du_18_janvier_2018.html "http://www2.assemblee-nationale.fr/documents/notice/15/europe/rap-info/i0577/(index)/depots"
```

# Conversion des documents en HTML au format Markdown

## Récupération de la dernière version du dossier législatif au format Markdown

```bash
cd ../
git clone https://framagit.org/parlement-ouvert/dossier-legislatif-protection-donnees-personnelles.git
```

## Installation des scripts de conversion des document HTML en Markdown

```bash
git clone https://framagit.org/parlement-ouvert/prendlaloi.git
cd prendlaloi
pip install -r requirements.txt
ln -s ../dossier-legislatif-protection-donnees-personnelles-sources html
ln -s ../dossier-legislatif-protection-donnees-personnelles markdown
```

## Conversion en Markdown de l'étude d'impact et des différentes version du projet de loi

```bash
./conversion_etude_impact_assemblee.py --json --number 490 --repair html/etude_impact_pjl_490_du_12_decembre_2017.html markdown/
./conversion_pjl_assemblee.py --discourse --json --repair html/pjl_490_du_13_decembre_2017.html markdown/
./conversion_pjl_assemblee.py --encoding iso-8859-1 --json --repair html/pjl_592_du_25_janvier_2018.html markdown/
```

## Conversion en Markdown des textes européens

```bash
./conversion_texte_journal_officiel_europe.py --discourse --json --repair html/reglement_ue_2016-679_du_27_avril_2016.html markdown/
./conversion_texte_journal_officiel_europe.py --json --repair html/directive_ue_2016-680_du_27_avril_2016.html markdown/
```

## Conversion en Markdown des lois françaises

```bash
./conversion_loi.py  --json --repair html/loi_78-17_du_6_janvier_1978.html markdown/
./conversion_loi.py  --json --repair html/loi_2016-1321_du_7_octobre_2016.html markdown/
```

## Conversion en Markdown de l'avis du Conseil d'État

```bash
./conversion_avis_conseil_etat.py --date "Séance du jeudi 7 décembre 2017" --json --number "393836" --repair --title "Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés" html/avis_conseil_etat_393836_du_7_decembre_2017.html markdown/
```

## Conversion en Markdown de la délibération de la CNIL

```bash
./conversion_deliberation_cnil.py --json --repair html/deliberation_cnil_2017-299_du_30_novembre_2017.html markdown/
```

## Conversion en Markdown des rapports d'information

```bash
./conversion_rapport_information_assemblee.py --encoding iso-8859-1 --json --number 4544 --repair html/rapport_information_4544_du_22_fevrier_2017.html html/rapport_information_4544_du_22_fevrier_2017_sommaire.txt markdown/
./conversion_rapport_information_assemblee.py --json --number 577 --repair html/rapport_information_577_du_18_janvier_2018.html html/rapport_information_577_du_18_janvier_2018_sommaire.txt markdown/
```

# Nettoyage et enrichissement des fichiers Markdown

## Installation des scripts de nettoyage et d'amélioration des fichiers Markdown

```bash
git clone https://framagit.org/parlement-ouvert/lielaloi.git
cd lielaloi
pip install -r requirements.txt
ln -s ../dossier-legislatif-protection-donnees-personnelles markdown
```

## Nettoyage des fichiers Markdown

```bash
./lavelaloi.py markdown/
```

## Ajout de la pagination aux fichiers Markdown

```bash
./paginelaloi.py "Projet de loi, version du Gouvernement" markdown/pjl-490
./paginelaloi.py "Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée" markdown/pjl-592
./paginelaloi.py "Règlement européen" markdown/reglement-2016-679
./paginelaloi.py "Directive européenne" markdown/directive-2016-680
./paginelaloi.py "Loi informatique et libertés" markdown/loi-78-17
./paginelaloi.py "Loi informatique et libertés selon le PJL 490" markdown/loi-78-17-pjl-490
./paginelaloi.py "Loi informatique et libertés selon le PJL 592" markdown/loi-78-17-pjl-592
./paginelaloi.py "Loi république numérique" markdown/loi-2016-1321
./paginelaloi.py "Avis du Conseil d'État" markdown/avis-conseil-etat-393836
./paginelaloi.py "Délibération CNIL" markdown/deliberation-cnil-2017-299
./paginelaloi.py "Rapport d'information de la précédente législature" markdown/rapport-information-4544
./paginelaloi.py "Rapport d'information de la commission des affaires européennes" markdown/rapport-information-577
```

## Ajout des références aux articles

```bash
./bilielaloi.py markdown/
```

# Récupération, conversion et nettoyage des amendements

## Amendements en première lecture en Commission des lois de l'Assemblée

```bash
cd ../prendlaloi/
./prendlesamendements.py -v html/
rm -Rf markdown/amendements-1-assemblee-commission*
./conversion_amendements.py html markdown

cd ../lielaloi/
./lavelaloi.py markdown/
./paginelaloi.py "Amendements en 1ère lecture à la commission de lois de l'Assemblée" markdown/amendements-1-assemblee-commission
./lielesamendements.py markdown/pjl-490 markdown/amendements-1-assemblee-commission
./bilielaloi.py markdown/
```

## Amendements en première lecture à l'hémicycle de l'Assemblée

**Attention** : _Les scripts `prendlesamendements.py` et `conversion_amendements.py` contiennent encore des paramètres en dur qu'il faut modifier pour chaque étape du projet de loi._

```bash
cd ../prendlaloi/
./prendlesamendements.py -v html/
rm -Rf markdown/amendements-1-assemblee-hemicycle*
./conversion_amendements.py html markdown

cd ../lielaloi/
./lavelaloi.py markdown/
./paginelaloi.py "Amendements en 1ère lecture à l'hémicycle de l'Assemblée" markdown/amendements-1-assemblee-hemicycle
./lielesamendements.py markdown/pjl-592 markdown/amendements-1-assemblee-hemicycle
./bilielaloi.py markdown/
```

# Synchronisation avec le forum

Le forum Discourse reprend automatiquement le contenu des articles, qu'il sert à commenter. Mais ce contenu est mal présenté.

Un script dédié permet d'améliorer le contenu des articles dans le forum. Il sert aussi à mettre à jour ces articles quand ils sont modifés côté wiki.

Pour utiliser ce script, commencer par recopier le fichier `config-sample.ini` en `config.ini`,  puis éditer ce fichier pour y changer les champs `forum_username` et `forum_api_key`.

Ensuite exécuter le script de synchronisation du forum :

```bash
./commentelaloi.py config.ini
```

<!-- TITLE: N° 577 - Rapport d'information de Mme Christine Hennion déposé par la commission des affaires européennes portant observations sur le projet de loi relatif à la protection des données personnelles (n° 490) - XVe législature - Assemblée nationale -->

* [Entête](/rapport-information-577/entete)

* [INTRODUCTION](/rapport-information-577/introduction)

* [I — LE RGPD (RÈGLEMENT GÉNÉRAL DE PROTECTION DES DONNÉES) S’INSCRIT DANS LA CONSTRUCTION DU PARADIGME EUROPÉEN DE PROTECTION DES LIBERTÉS FONDAMENTALES DANS LE DOMAINE NUMÉRIQUE](/rapport-information-577/i)

  * [A — LA MODERNISATION DU CADRE JURIDIQUE APPLICABLE À LA PROTECTION DES DONNÉES PERSONNELLES](/rapport-information-577/i/a)

    * [1 — Une adaptation aux nouvelles technologies…](/rapport-information-577/i/a/1)

    * [2 — …dans le contexte du marché unique du numérique](/rapport-information-577/i/a/2)

    * [3 — La difficulté des négociations, qui tient notamment à l’ampleur des enjeux, a abouti à un texte hybride](/rapport-information-577/i/a/3)

      * [a — La souveraineté numérique : l’application extraterritoriale du règlement](/rapport-information-577/i/a/3/a)

      * [b — Un règlement autorisant des marges de manœuvre nationales](/rapport-information-577/i/a/3/b)

      * [c — Un travail suivi pour assurer la plus grande harmonisation possible](/rapport-information-577/i/a/3/c)

  * [B — DE NOUVEAUX DROITS POUR UNE RESPONSABILITÉ ACCRUE DES RESPONSABLES DE TRAITEMENT](/rapport-information-577/i/b)

    * [1 — Assurer le respect de la vie privée et la maîtrise des données](/rapport-information-577/i/b/1)

    * [2 — Garantir techniquement un traitement adapté et proportionné des données](/rapport-information-577/i/b/2)

    * [3 — Une inversion de la logique de contrôle et de responsabilité](/rapport-information-577/i/b/3)

    * [4 — Une adaptation aux risques](/rapport-information-577/i/b/4)

* [II — LE PROJET DE LOI RELATIF AUX DONNÉES PERSONNELLES : UNE MODERNISATION BIENVENUE DU CADRE JURIDIQUE NATIONAL](/rapport-information-577/ii)

  * [A — LE CHOIX COHÉRENT D’UNE HARMONISATION EUROPÉENNE MAXIMALE](/rapport-information-577/ii/a)

    * [1 — Le RGPD contient de larges marges de manœuvre nationales](/rapport-information-577/ii/a/1)

    * [2 — Un choix légistique cohérent mais peu clair](/rapport-information-577/ii/a/2)

    * [3 — La pertinence du règlement quant aux libertés fondamentales : l’exemple de l’accès des mineurs aux plateformes](/rapport-information-577/ii/a/3)

  * [B — LA DÉFINITION D’UN ESPACE EUROPÉEN DE PROTECTION DES DONNÉES PERSONNELLES](/rapport-information-577/ii/b)

    * [1 — La coopération entre les agences de protection des données personnelles](/rapport-information-577/ii/b/1)

    * [2 — Le champ territorial d’application : une difficile coordination entre les États membres](/rapport-information-577/ii/b/2)

    * [3 — L’extension du régime de responsabilité aux sous-traitants](/rapport-information-577/ii/b/3)

  * [C — LES NOUVELLES MISSIONS ET ACTIONS DES AUTORITÉS DE CONTRÔLE NATIONALES](/rapport-information-577/ii/c)

    * [1 — La certification et les instruments de droit souple](/rapport-information-577/ii/c/1)

    * [2 — Le traitement des données de l’État : une exception dommageable](/rapport-information-577/ii/c/2)

    * [3 — Le contrôle en ligne : une collaboration qui reste à construire](/rapport-information-577/ii/c/3)

    * [4 — Le référentiel de sanctions](/rapport-information-577/ii/c/4)

    * [5 — Les limites du contrôle : l’opposabilité du secret professionnel](/rapport-information-577/ii/c/5)

  * [D — UNE PROTECTION DES CITOYENS À CONSOLIDER](/rapport-information-577/ii/d)

    * [1 — La limitation des droits dans le domaine numérique](/rapport-information-577/ii/d/1)

    * [2 — L’action de groupe : introduire des modalités de réparation](/rapport-information-577/ii/d/2)

    * [3 — Les actions de médiation](/rapport-information-577/ii/d/3)

* [TRAVAUX DE LA COMMISSION](/rapport-information-577/travaux-de-la-commission)

* [ANNEXE N° 1 — PRINCIPALES PROPOSITIONS](/rapport-information-577/annexe-1)

* [ANNEXE N° 2 — LISTE DES PERSONNES AUDITIONNÉES](/rapport-information-577/annexe-2)

* [Notes](/rapport-information-577/notes)

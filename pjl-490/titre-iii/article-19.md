<!-- TITLE: Article 19 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-iii/article-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Le [chapitre XIII](/loi-78-17/chapitre-xiii) de la même loi devient le chapitre XIV et, après [l’article 70](/loi-78-17/chapitre-xii/article-70), il est inséré les dispositions suivantes :

 _« Chapitre XIII_

 _ **« Dispositions applicables aux traitements relevant de la [directive (UE) 2016/680 du 27 avril 2016](/directive-2016-680)**_

 _« Section 1_

 _ **« Dispositions générales**_

« _Art. 70-1. –_ Les dispositions du présent chapitre s’appliquent, le cas échéant par dérogation aux autres dispositions de la présente loi, aux traitements des données à caractère personnel mis en œuvre :

« 1° À des fins de prévention et de détection des infractions pénales, d’enquêtes et de poursuites en la matière ou d’exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces ;

« 2° Par toute autorité publique compétente pour l’une des finalités énoncées au 1°, ou tout autre organisme ou entité à qui a été confié, à ces mêmes fins, l’exercice de l’autorité publique et des prérogatives de puissance publique, ci-après dénommée autorité compétente.

« Ces traitements ne sont licites que si et dans la mesure où ils sont nécessaires à l’exécution d’une mission effectuée, pour les finalités énoncées au 1°, par une autorité compétente au sens du 2°, et où sont respectées les dispositions des articles 70-3 et 70-4.

« Pour l’application du présent chapitre, lorsque les notions utilisées ne sont pas définies au chapitre premier de la présente loi, les définitions de [l’article 4 du règlement (UE) 2016/679](/reglement-2016-679) sont applicables.

« _Art. 70-2. –_ Le traitement de données mentionnées au [I de l’article 8](/loi-78-17/chapitre-ii/section-2/article-8) est possible uniquement en cas de nécessité absolue, sous réserve de garanties appropriées pour les droits et libertés de la personne concernée, et, soit s’il est prévu par un acte législatif ou règlementaire, soit s’il vise à protéger les intérêts vitaux d’une personne physique, soit s’il porte sur des données manifestement rendues publiques par la personne concernée.

« _Art. 70-3. –_ Si le traitement est mis en œuvre pour le compte de l’État pour au moins l’une des finalités prévues au 1° de l’article 70-1, il doit être prévu par un acte règlementaire pris conformément au [I de l’article 26](/loi-78-17/chapitre-iv/section-2/article-26) et aux articles [28](/loi-78-17/chapitre-iv/section-2/article-28) à [31](/loi-78-17/chapitre-iv/section-3/article-31).

« Si le traitement porte sur des données mentionnées au [I de l’article 8](/loi-78-17/chapitre-ii/section-2/article-8), il est prévu par un acte règlementaire pris conformément au [II de l’article 26](/loi-78-17/chapitre-iv/section-2/article-26).

« _Art. 70-4. –_ Si le traitement est susceptible d’engendrer un risque élevé pour les droits et les libertés des personnes physiques, notamment parce qu’il porte sur des données mentionnées au [I de l’article 8](/loi-78-17/chapitre-ii/section-2/article-8), le responsable du traitement effectue une analyse d’impact relative à la protection des données à caractère personnel.

« Si le traitement est mis en œuvre pour le compte de l’État, cette analyse d’impact est adressée à la Commission nationale de l’informatique et des libertés avec la demande d’avis prévue par [l’article 30](/loi-78-17/chapitre-iv/section-3/article-30).

« Dans les autres cas, le responsable du traitement ou le sous-traitant consulte la Commission nationale de l’informatique et des libertés préalablement au traitement des données à caractère personnel :

« 1° Soit lorsque l’analyse d’impact relative à la protection des données indique que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque ;

« 2° Soit lorsque le type de traitement, en particulier en raison de l’utilisation de nouveaux mécanismes, technologies ou procédures, présente des risques élevés pour les libertés et les droits des personnes concernées.

« _Art. 70-5. –_ Les données à caractère personnel collectées par les autorités compétentes pour les finalités énoncées au 1° de l’article 70-1, ne peuvent être traitées pour d’autres finalités, à moins qu’un tel traitement ne soit autorisé par des dispositions législatives ou réglementaires, ou par le droit de l’Union européenne. Lorsque des données à caractère personnel sont traitées à de telles autres fins, le [règlement (UE) 2016/679](/reglement-2016-679) s’applique, à moins que le traitement ne soit effectué dans le cadre d’une activité ne relevant pas du champ d’application du droit de l’Union européenne.

« Lorsque les autorités compétentes sont chargées d’exécuter des missions autres que celles exécutées pour les finalités énoncées au 1° de l’article 70-1, le [règlement (UE) 2016/679](/reglement-2016-679) s’applique au traitement effectué à de telles fins, y compris à des fins archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques, à moins que le traitement ne soit effectué dans le cadre d’une activité ne relevant pas du champ d’application du droit de l’Union européenne.

« Si le traitement est soumis à des conditions spécifiques, l’autorité compétente qui transmet les données informe le destinataire de ces données à caractère personnel de ces conditions et de l’obligation de les respecter.

« L’autorité compétente qui transmet les données n’applique pas aux destinataires dans les autres États membres ou aux services, organes et organismes établis en vertu des [chapitres 4 et 5 du titre V du traité sur le fonctionnement de l’Union européenne](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex:12012E/TXT) des conditions en vertu du paragraphe 3 différentes de celles applicables aux transferts de données similaires à l’intérieur de l’État membre dont relève l’autorité compétente qui transmet les données.

« _Art. 70-6. –_ Les traitements effectués pour l’une des finalités énoncées au 1° de l’article 70-1 autre que celles pour lesquelles les données ont été collectées sont autorisés sous réserve du respect des principes prévus au chapitre Ier de la présente loi et au présent chapitre.

« Ces traitements peuvent comprendre l’archivage dans l’intérêt public, à des fins scientifiques, statistiques ou historiques, aux fins énoncées à l’article 70-1.

« _Art. 70-7. –_ Les traitements à des fins archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques sont mis en œuvre dans les conditions de l’[article 36](/loi-78-17/chapitre-v/section-1/article-36) de la présente loi.

« _Art. 70-8. –_ Les données à caractère personnel fondées sur des faits sont dans la mesure du possible distinguées de celles fondées sur des appréciations personnelles.

« _Art. 70-9. –_ Aucune décision de justice impliquant une appréciation sur le comportement d’une personne ne peut avoir pour fondement un traitement automatisé de données à caractère personnel destiné à évaluer certains aspects de sa personnalité.

« Aucune autre décision produisant des effets juridiques à l’égard d’une personne ne peut être prise sur le seul fondement d’un traitement automatisé de données destiné à prévoir ou à évaluer certains aspects personnels relatifs à la personne concernée.

« Tout profilage qui entraîne une discrimination à l’égard des personnes physiques sur la base des catégories particulières de données à caractère personnel visées à l’[article 8](/loi-78-17/chapitre-ii/section-2/article-8) est interdit.

« _Art. 70-10. –_ Les données à caractère personnel ne peuvent faire l’objet d’une opération de traitement de la part d’un sous-traitant que dans les conditions prévues aux [paragraphes 1, 2, 9 et 10 de l’article 28](/reglement-2016-679/chapitre-iv/section-1/article-28) et à l’[article 29](/reglement-2016-679/chapitre-iv/section-1/article-29) du règlement (UE) 2016/679 et au présent article.

« Les sous-traitants doivent présenter des garanties suffisantes quant à la mise en œuvre de mesures techniques et organisationnelles appropriées de manière que le traitement réponde aux exigences du présent chapitre et garantisse la protection des droits de la personne concernée.

« Le traitement par un sous-traitant est régi par un contrat ou un autre acte juridique, qui lie le sous-traitant à l’égard du responsable du traitement, définit l’objet et la durée du traitement, la nature et la finalité du traitement, le type de données à caractère personnel et les catégories de personnes concernées, et les obligations et les droits du responsable du traitement, et qui prévoit que le sous-traitant n’agit que sur instruction du responsable de traitement. Le contenu de ce contrat ou acte juridique est précisé par décret en Conseil d’État pris après avis de la Commission nationale de l’informatique et des libertés.

 _« Section 2_

 _ **« Obligations incombant aux autorités compétentes et aux responsables de traitements**_

« _Art. 70-11. –_ Les autorités compétentes prennent toutes les mesures raisonnables pour garantir que les données à caractère personnel qui sont inexactes, incomplètes ou ne sont plus à jour soient effacées ou rectifiées sans tarder ou ne soient pas transmises ou mises à disposition. À cette fin, chaque autorité compétente vérifie, dans la mesure du possible, la qualité des données à caractère personnel avant leur transmission ou mise à disposition.

« Dans la mesure du possible, lors de toute transmission de données à caractère personnel, sont ajoutées des informations nécessaires permettant à l’autorité compétente destinataire de juger de l’exactitude, de l’exhaustivité, et de la fiabilité des données à caractère personnel, et de leur niveau de mise à jour.

« S’il s’avère que des données à caractère personnel inexactes ont été transmises ou que des données à caractère personnel ont été transmises de manière illicite, le destinataire en est informé sans retard. Dans ce cas, les données à caractère personnel sont rectifiées ou effacées ou leur traitement est limité conformément à l’article 70-20.

« _Art. 70-12. –_ Le responsable du traitement établit dans la mesure du possible et le cas échéant une distinction claire entre les données à caractère personnel de différentes catégories de personnes concernées, telles que :

« 1° Les personnes à l’égard desquelles il existe des motifs sérieux de croire qu’elles ont commis ou sont sur le point de commettre une infraction pénale ;

« 2° Les personnes reconnues coupables d’une infraction pénale ;

« 3° Les victimes d’une infraction pénale ou les personnes à l’égard desquelles certains faits portent à croire qu’elles pourraient être victimes d’une infraction pénale ;

« 4° Les tiers à une infraction pénale, tels que les personnes pouvant être appelées à témoigner lors d’enquêtes en rapport avec des infractions pénales ou des procédures pénales ultérieures, des personnes pouvant fournir des informations sur des infractions pénales, ou des contacts ou des associés de l’une des personnes visées aux 1° et 2°.

« _Art. 70-13._ – I. – Afin de démontrer que le traitement est effectué conformément au présent chapitre, le responsable du traitement et le sous-traitant mettent en œuvre les mesures prévues aux [paragraphes 1 et 2 de l’article 24](/reglement-2016-679/chapitre-iv/section-1/article-24) et aux [paragraphes 1 et 2 de l’article 25](/reglement-2016-679/chapitre-iv/section-1/article-25) du règlement (UE) 2016/679 et celles appropriées afin de garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l’[article 8](/loi-78-17/chapitre-ii/section-2/article-8).

« II. – En ce qui concerne le traitement automatisé, le responsable du traitement ou le sous-traitant met en œuvre, à la suite d’une évaluation des risques, des mesures destinées à :

« 1° Empêcher toute personne non autorisée d’accéder aux installations utilisées pour le traitement (contrôle de l’accès aux installations) ;

« 2° Empêcher que des supports de données puissent être lus, copiés, modifiés ou supprimés de façon non autorisée (contrôle des supports de données) ;

« 3° Empêcher l’introduction non autorisée de données à caractère personnel dans le fichier, ainsi que l’inspection, la modification ou l’effacement non autorisé de données à caractère personnel enregistrées (contrôle de la conservation) ;

« 4° Empêcher que les systèmes de traitement automatisé puissent être utilisés par des personnes non autorisées à l’aide d’installations de transmission de données (contrôle des utilisateurs) ;

« 5° Garantir que les personnes autorisées à utiliser un système de traitement automatisé ne puissent accéder qu’aux données à caractère personnel sur lesquelles porte leur autorisation (contrôle de l’accès aux données) ;

« 6° Garantir qu’il puisse être vérifié et constaté à quelles instances des données à caractère personnel ont été ou peuvent être transmises ou mises à disposition par des installations de transmission de données (contrôle de la transmission) ;

« 7° Garantir qu’il puisse être vérifié et constaté a posteriori quelles données à caractère personnel ont été introduites dans les systèmes de traitement automatisé, et à quel moment et par quelle personne elles y ont été introduites (contrôle de l’introduction) ;

« 8° Empêcher que, lors de la transmission de données à caractère personnel ainsi que lors du transport de supports de données, les données puissent être lues, copiées, modifiées ou supprimées de façon non autorisée (contrôle du transport) ;

« 9° Garantir que les systèmes installés puissent être rétablis en cas d’interruption (restauration) ;

« 10° Garantir que les fonctions du système opèrent, que les erreurs de fonctionnement soient signalées (fiabilité) et que les données à caractère personnel conservées ne puissent pas être corrompues par un dysfonctionnement du système (intégrité).

« _Art. 70-14. –_ Le responsable du traitement et le sous-traitant tiennent un registre des activités de traitement dans les conditions prévues aux [paragraphes 1 à 4 de l’article 30 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-iv/section-1/article-30). Ce registre contient aussi la description générale des mesures visant à garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l’[article 8](/loi-78-17/chapitre-ii/section-2/article-8), l’indication de la base juridique de l’opération de traitement, y compris les transferts, à laquelle les données à caractère personnel sont destinées et, le cas échéant, le recours au profilage.

 _« Art. 70-15. **–**_ Le responsable du traitement ou son sous-traitant établit pour chaque traitement automatisé un journal des opérations de collecte, de modification, de consultation, de communication, y compris les transferts, l’interconnexion et l’effacement, portant sur de telles données.

« Les journaux des opérations de consultation et de communication permettent d’en établir le motif, la date et l’heure. Ils permettent également, dans la mesure du possible, d’identifier les personnes qui consultent ou communiquent les données et leurs destinataires.

« Ce journal est uniquement utilisé à des fins de vérification de la licéité du traitement, d’autocontrôle, de garantie de l’intégrité et de la sécurité des données et à des fins de procédures pénales.

« Ce journal est mis à la disposition de la Commission nationale de l’informatique et des libertés à sa demande.

« _Art. 70-16. –_ Les articles [31](/reglement-2016-679/chapitre-iv/section-1/article-31), [33](/reglement-2016-679/chapitre-iv/section-2/article-33) et [34](/reglement-2016-679/chapitre-iv/section-2/article-34) du règlement (UE) 2016/679 sont applicables aux traitements des données à caractère personnel relevant du présent chapitre.

« Si la violation de données à caractère personnel porte sur des données à caractère personnel qui ont été transmises par le responsable du traitement d’un autre État membre ou à celui-ci, le responsable du traitement notifie également la violation au responsable du traitement de l’autre État membre dans les meilleurs délais.

« La communication d’une violation de données à caractère personnel à la personne concernée peut être retardée, limitée ou ne pas être délivrée, dès lors et aussi longtemps qu’une mesure de cette nature constitue une mesure nécessaire et proportionnée dans une société démocratique, en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne physique concernée, lorsque sa mise en œuvre est de nature à mettre en danger la sécurité publique, la sécurité nationale ou les droits ou libertés d’autrui ou à faire obstacle au bon déroulement des enquêtes et procédures destinées à prévenir, détecter ou poursuivre des infractions pénales ou à exécuter des sanctions pénales.

« _Art. 70-17. –_ I. – Sauf pour les juridictions agissant dans l’exercice de leur fonction juridictionnelle, le responsable du traitement désigne un délégué à la protection des données.

« Un seul délégué à la protection des données peut être désigné pour plusieurs autorités compétentes, compte tenu de leur structure organisationnelle et de leur taille.

« Les dispositions des [paragraphes 5 et 7 de l’article 37](/reglement-2016-679/chapitre-iv/section-4/article-37), des [paragraphes 1 et 2 de l’article 38](/reglement-2016-679/chapitre-iv/section-4/article-38) et du [paragraphe 1 de l’article 39](/reglement-2016-679/chapitre-iv/section-4/article-39) du règlement (UE) 2016/679, en ce qu’elles concernent le responsable du traitement, sont applicables aux traitements des données à caractère personnel relevant du présent chapitre.

 _« Section 3_

 _ **« Droits de la personne concernée**_

« _Art. 70-18. –_ I. – Le responsable du traitement met à la disposition de la personne concernée les informations suivantes :

« 1° L’identité et les coordonnées du responsable du traitement, et le cas échéant celles de son représentant ;

« 2° Le cas échéant, les coordonnées du délégué à la protection des données ;

« 3° Les finalités poursuivies par le traitement auquel les données sont destinées ;

« 4° Le droit d’introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés et les coordonnées de la commission ;

« 5° L’existence du droit de demander au responsable du traitement l’accès aux données à caractère personnel, leur rectification ou leur effacement, et la limitation du traitement des données à caractère personnel relatives à une personne concernée.

« II. – En plus des informations visées au I, le responsable du traitement fournit à la personne concernée, dans des cas particuliers, les informations additionnelles suivantes afin de lui permettre d’exercer ses droits :

« 1° La base juridique du traitement ;

« 2° La durée de conservation des données à caractère personnel ou, lorsque ce n’est pas possible, les critères utilisés pour déterminer cette durée ;

« 3° Le cas échéant, les catégories de destinataires des données à caractère personnel, y compris dans les États non membres de l’Union européenne ou au sein d’organisations internationales ;

« 4° Au besoin, des informations complémentaires, en particulier lorsque les données à caractère personnel sont collectées à l’insu de la personne concernée.

« _Art. 70-19. –_ La personne concernée a le droit d’obtenir du responsable du traitement la confirmation que des données à caractère personnel la concernant sont ou ne sont pas traitées et, lorsqu’elles le sont, l’accès auxdites données ainsi que les informations suivantes :

« 1° Les finalités du traitement ainsi que sa base juridique ;

« 2° Les catégories de données à caractère personnel concernées ;

« 3° Les destinataires ou catégories de destinataires auxquels les données à caractère personnel ont été communiquées, en particulier les destinataires qui sont établis dans des États non membres de l’Union européenne ou les organisations internationales ;

« 4° Lorsque cela est possible, la durée de conservation des données à caractère personnel envisagée ou, lorsque ce n’est pas possible, les critères utilisés pour déterminer cette durée ;

« 5° L’existence du droit de demander au responsable du traitement la rectification ou l’effacement des données à caractère personnel, ou la limitation du traitement de ces données ;

« 6° Le droit d’introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés et les coordonnées de la commission ;

« 7° La communication des données à caractère personnel en cours de traitement, ainsi que toute information disponible quant à leur source.

« _Art. 70-20. –_ I. – La personne concernée a le droit d’obtenir du responsable du traitement :

« 1° Que soit rectifiées dans les meilleurs délais des données à caractère personnel la concernant qui sont inexactes ;

« 2° Que soient complétées des données à caractère personnel la concernant incomplètes, y compris en fournissant à cet effet une déclaration complémentaire ;

« 3° Que soit effacées dans les meilleurs délais des données à caractère personnel la concernant lorsque le traitement est réalisé en violation des dispositions de la présente loi ou lorsque ces données doivent être effacées pour respecter une obligation légale à laquelle est soumis le responsable du traitement.

« II. – Lorsque l’intéressé en fait la demande, le responsable du traitement doit justifier qu’il a procédé aux opérations exigées en vertu du I.

« III. – Au lieu de procéder à l’effacement, le responsable du traitement limite le traitement lorsque :

« 1° Soit l’exactitude des données à caractère personnel est contestée par la personne concernée et il ne peut être déterminé si les données sont exactes ou non ;

« 2° Soit les données à caractère personnel doivent être conservées à des fins probatoires.

« Lorsque le traitement est limité en vertu du 1°, le responsable du traitement informe la personne concernée avant de lever la limitation du traitement.

« IV. – Le responsable du traitement informe la personne concernée de tout refus de rectifier ou d’effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs du refus.

« V. – Le responsable du traitement communique la rectification des données à caractère personnel inexactes à l’autorité compétente dont elles proviennent.

« VI. – Lorsque des données à caractère personnel ont été rectifiées ou effacées ou que le traitement a été limité au titre des I, II et III, le responsable du traitement le notifie aux destinataires afin que ceux-ci rectifient ou effacent les données ou limitent le traitement des données sous leur responsabilité.

« _Art. 70-21. –_ I. – Les droits de la personne physique concernée peuvent faire l’objet de restrictions selon les modalités prévues au II du présent article dès lors et aussi longtemps qu’une telle restriction constitue une mesure nécessaire et proportionnée dans une société démocratique en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne pour :

« 1° Éviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires :

« 2° Éviter de nuire à la prévention ou à la détection d’infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l’exécution de sanctions pénales ;

« 3° Protéger la sécurité publique ;

« 4° Protéger la sécurité nationale ;

« 5° Protéger les droits et libertés d’autrui.

« Ces restrictions sont prévues par l’acte instaurant le traitement.

« II. – Lorsque les conditions prévues au I sont remplies, le responsable du traitement peut :

« 1° Retarder ou limiter la fourniture à la personne concernée des informations mentionnées au II de l’article 70-18, ou ne pas fournir ces informations ;

« 2° Limiter, entièrement ou partiellement, le droit d’accès de la personne concernée prévu par l’article 70-19 ;

« 3° Ne pas informer la personne de son refus de rectifier ou d’effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs de cette décision conformément au IV de l’article 70-20.

« III. – Dans les cas visés au 2° du II, le responsable du traitement informe la personne concernée, dans les meilleurs délais, de tout refus ou de toute limitation d’accès, ainsi que des motifs du refus ou de la limitation. Ces informations peuvent ne pas être fournies lorsque leur communication risque de compromettre l’un des objectifs énoncés au I. Le responsable du traitement consigne les motifs de fait ou de droit sur lesquels se fonde la décision, et met ces informations à la disposition de la Commission nationale de l’informatique et des libertés.

« IV. – En cas de restriction des droits de la personne concernée intervenue en application du II ou du III, le responsable du traitement informe la personne concernée de la possibilité d’exercer ses droits par l’intermédiaire de la Commission nationale de l’informatique et des libertés ou de former un recours juridictionnel.

« _Art. 70-22. –_ En cas de restriction des droits de la personne concernée intervenue en application du II ou du III de l’article 70-21, la personne concernée peut saisir la Commission nationale de l’informatique et des libertés.

« Les dispositions des deuxième et troisième alinéas de l’[article 41](/loi-78-17/chapitre-v/section-2/article-41) sont alors applicables.

« Lorsque la commission informe la personne concernée qu’il a été procédé aux vérifications nécessaires, elle l’informe également de son droit de former un recours juridictionnel.

« _Art. 70-23. –_ Aucun paiement n’est exigé pour prendre les mesures et fournir les informations visées aux articles 70-18 à 70-20, sauf en cas de demande manifestement infondée ou abusive.

« Dans ce cas, le responsable du traitement peut également refuser de donner suite à la demande.

« En cas de contestation, la charge de la preuve du caractère manifestement infondé ou abusif des demandes incombe au responsable du traitement auprès duquel elles sont adressées.

« _Art 70-24. –_ Les dispositions de la présente sous-section ne s’appliquent pas lorsque les données à caractère personnel figurent soit dans une décision judiciaire, soit dans un dossier judiciaire faisant l’objet d’un traitement lors d’une procédure pénale. Dans ces cas, l’accès à ces données ne peut se faire que dans les conditions prévues par le code de procédure pénale.

 _« Section 4_

 _ **« Transferts de données à caractère personnel vers des États n’appartenant pas à l’Union européenne ou vers des destinataires établis dans des États non membres de l’Union européenne**_

« _Art. 70-25. –_ Le responsable d’un traitement de données à caractère personnel ne peut transférer des données ou autoriser le transfert de données déjà transmises vers un État n’appartenant pas à l’Union européenne que lorsque les conditions suivantes sont respectées :

« 1° Le transfert de ces données est nécessaire à l’une des finalités énoncées au 1° de l’article 70-1 ;

« 2° Les données à caractère personnel sont transférées à un responsable dans cet État tiers ou à une organisation internationale qui est une autorité compétente chargée dans cet État des fins relevant en France __ du 1° de l’article 70-1 ;

« 3° Si les données à caractère personnel proviennent d’un autre État, l’État qui a transmis ces données a préalablement autorisé ce transfert conformément à son droit national.

« Toutefois, si l’autorisation préalable ne peut pas être obtenue en temps utile, ces données à caractère personnel peuvent être retransmises sans l’autorisation préalable de l’État qui a transmis ces données lorsque cette retransmission est nécessaire à la prévention d’une menace grave et immédiate pour la sécurité publique d’un autre État ou pour la sauvegarde des intérêts essentiels de la France. L’autorité d’où provenaient ces données personnelles est informée sans retard.

« 4° L’une au moins des trois conditions suivantes est remplie :

« _a)_ La commission a adopté une décision d’adéquation en application de l’[article 36 de la directive (UE) 2016/680](/directive-2016-680/chapitre-v/article-36) du Parlement et du Conseil du 27 avril 2016 ;

« _b)_ À défaut d’une telle décision d’adéquation, des garanties appropriées en ce qui concerne la protection des données à caractère personnel sont fournies dans un instrument juridiquement contraignant ; ces garanties appropriées peuvent soit résulter des garanties relatives à la protection des données mentionnées dans les conventions mises en  œuvre avec cet État tiers, soit résulter de dispositions juridiquement contraignantes exigées à l’occasion de l’échange de données ;

« _c)_ À défaut d’une telle décision d’adéquation et de garanties appropriées telles que prévues au _b_ , le responsable du traitement a évalué toutes les circonstances du transfert et estime qu’il existe des garanties appropriées au regard de la protection des données à caractère personnel ;

« Lorsque le responsable d’un traitement de données à caractère personnel transfère des données à caractère personnel sur le seul fondement de l’existence de garanties appropriées au regard de la protection des données à caractère personnel, autre qu’une juridiction effectuant une activité de traitement dans le cadre de ses activités juridictionnelles, il avise la Commission nationale de l’informatique et des libertés des catégories de transferts relevant de ce fondement.

« Dans ce cas, le responsable du traitement des données doit garder trace de la date et l’heure du transfert, des informations sur l’autorité compétente destinataire, et de la justification du transfert et des données à caractère personnel transférées. Cette documentation est mise à la disposition de l’autorité de contrôle, sur sa demande.

« Lorsque la commission a abrogé, modifié ou suspendu une décision d’adéquation adoptée en application de l’[article 36](/directive-2016-680/chapitre-v/article-36) de la directive précitée, le responsable d’un traitement de données à caractère personnel peut néanmoins transférer des données personnelles ou autoriser le transfert de données déjà transmises vers un État n’appartenant pas à l’Union européenne si des garanties appropriées en ce qui concerne la protection des données à caractère personnel sont fournies dans un instrument juridiquement contraignant ou s’il estime après avoir évalué toutes les circonstances du transfert qu’il existe des garanties appropriées au regard de la protection des données à caractère personnel.

« _Art. 70-26. –_ Par dérogation aux dispositions de l’article précédent, le responsable d’un traitement de données à caractère personnel ne peut, en l’absence de décision d’adéquation ou de garanties appropriées, transférer ces données ou autoriser le transfert de données déjà transmises vers un État n’appartenant pas à l’Union européenne que lorsque le transfert est nécessaire :

« 1° À la sauvegarde des intérêts vitaux de la personne concernée ou d’une autre personne ;

« 2° À la sauvegarde des intérêts légitimes de la personne concernée lorsque le droit français le prévoit ;

« 3° Pour prévenir une menace grave et immédiate pour la sécurité publique d’un État membre de l’Union européenne ou d’un pays tiers ;

« 4° Dans des cas particuliers, à l’une des finalités énoncées au 1° de l’article 70-1 ;

« 5° Dans un cas particulier, à la constatation, à l’exercice ou à la défense de droits en justice en rapport avec les mêmes fins.

« Dans les cas visés aux 4° et 5°, le responsable du traitement de données à caractère personnel ne transfère pas ces données s’il estime que les libertés et droits fondamentaux de la personne concernée l’emportent sur l’intérêt public dans le cadre du transfert envisagé.

« Lorsqu’un transfert est effectué aux fins de la sauvegarde des intérêts légitimes de la personne concernée, le responsable du traitement garde trace de la date et l’heure du transfert, des informations sur l’autorité compétente destinataire, et de la justification du transfert et les données à caractère personnel transférées. Il met ces informations à la disposition de la Commission nationale de l’informatique et des libertés, à sa demande.

« _Art. 70-27. –_ Toute autorité publique compétente mentionnée au 2° de l’article 70-1 __ peut, dans certains cas particuliers, transférer des données à caractère personnel directement à des destinataires établis dans un État n’appartenant pas à l’Union européenne, lorsque les autres dispositions de la présente loi applicables aux traitements relevant de l’article 70-1 sont respectées et que les conditions ci-après sont remplies :

« 1° Le transfert est nécessaire à l’exécution de la mission de l’autorité compétente qui transfère ces données pour l’une des finalités énoncées à l’article 70-1 ;

« 2° L’autorité compétente qui transfère ces données établit qu’il n’existe pas de libertés ni de droits fondamentaux de la personne concernée qui prévalent sur l’intérêt public nécessitant le transfert dans le cas considéré ;

« 3° L’autorité compétente qui transfère ces données estime que le transfert à l’autorité compétente de l’autre État est inefficace ou inapproprié, notamment parce que le transfert ne peut pas être effectué en temps opportun ;

« 4° L’autorité compétente de l’autre État est informée dans les meilleurs délais, à moins que cela ne soit inefficace ou inapproprié ;

« 5° L’autorité compétente qui transfère ces données informe le destinataire de la finalité ou des finalités déterminées pour lesquelles les données à caractère personnel transmises doivent exclusivement faire l’objet d’un traitement par ce destinataire, à condition qu’un tel traitement soit nécessaire ;

« L’autorité compétente qui transfère des données informe la Commission nationale de l’informatique et des libertés des transferts relevant du présent article.

« L’autorité compétente garde trace de la date et l’heure de ce transfert, des informations sur le destinataire, et de la justification du transfert et les données à caractère personnel transférées. »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 41 — Rejeté](/amendements-1-assemblee-commission/article-19/amendement-41)
  * [Amendement n° 153 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-153)
  * [Amendement n° 49 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-49)
  * [Amendement n° 6 — Non soutenu](/amendements-1-assemblee-commission/article-19/amendement-6)
  * [Amendement n° 154 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-154)
  * [Amendement n° 156 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-156)
  * [Amendement n° 157 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-157)
  * [Amendement n° 159 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-159)
  * [Amendement n° 160 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-160)
  * [Amendement n° 161 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-161)
  * [Amendement n° 162 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-162)
  * [Amendement n° 163 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-163)
  * [Amendement n° 164 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-164)
  * [Amendement n° 165 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-165)
  * [Amendement n° 166 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-166)
  * [Amendement n° 167 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-167)
  * [Amendement n° 168 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-168)
  * [Amendement n° 169 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-169)
  * [Amendement n° 170 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-170)
  * [Amendement n° 171 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-171)
  * [Amendement n° 172 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-172)
  * [Amendement n° 173 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-173)
  * [Amendement n° 174 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-174)
  * [Amendement n° 175 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-175)
  * [Amendement n° 222 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-222)
  * [Amendement n° 176 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-176)
  * [Amendement n° 177 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-177)
  * [Amendement n° 178 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-178)
  * [Amendement n° 179 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-179)
  * [Amendement n° 180 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-180)
  * [Amendement n° 181 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-181)
  * [Amendement n° 182 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-182)
  * [Amendement n° 183 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-183)
  * [Amendement n° 184 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-184)
  * [Amendement n° 185 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-185)
  * [Amendement n° 186 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-186)
  * [Amendement n° 223 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-223)
  * [Amendement n° 224 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-224)
  * [Amendement n° 187 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-187)
  * [Amendement n° 188 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-188)
  * [Amendement n° 189 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-189)
  * [Amendement n° 209 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-209)
  * [Amendement n° 190 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-190)
  * [Amendement n° 191 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-191)
  * [Amendement n° 19 — Non soutenu](/amendements-1-assemblee-commission/article-19/amendement-19)
  * [Amendement n° 221 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-221)
  * [Amendement n° 192 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-192)
  * [Amendement n° 193 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-193)
  * [Amendement n° 194 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-194)
  * [Amendement n° 195 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-195)
  * [Amendement n° 196 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-196)
  * [Amendement n° 198 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-198)
  * [Amendement n° 199 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-199)
  * [Amendement n° 200 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-200)
  * [Amendement n° 201 — Adopté](/amendements-1-assemblee-commission/article-19/amendement-201)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 10 — SOUS-TRAITANT](/etude-impact-490/titre-ii/chapitre-iii/article-10)
  * [ARTICLES 18 ET 19 — PRESENTATION GENERALE ET DÉFINITIONS](/etude-impact-490/titre-iii/articles-18-et-19)
  * [ARTICLE 19 SECTION 1 — DISPOSITIONS GENERALES](/etude-impact-490/titre-iii/article-19-section-1)
  * [ARTICLE 19 SECTION 2 — OBLIGATIONS INCOMBANT AUX AUTORITES COMPETENTES ET AUX RESPONSABLES DE TRAITEMENT](/etude-impact-490/titre-iii/article-19-section-2)
  * [ARTICLE 18 ET ARTICLE 19 SECTION 3 — DROITS DE LA PERSONNE CONCERNÉE](/etude-impact-490/titre-iii/article-18-et-article-19-section-3)
  * [ARTICLE 19 SECTION 4 — TRANSFERTS INTERNATIONAUX](/etude-impact-490/titre-iii/article-19-section-4)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)
  * [Article 24](/pjl-490/titre-v/article-24)
* N° 592 annexe 0 - Rapport sur le projet de loi, après engagement de la procédure accélérée, relatif à la protection des données personnelles (n°490).
  * [Article 24](/pjl-592/titre-v/article-24)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-iii/article-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-iii/article-19' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

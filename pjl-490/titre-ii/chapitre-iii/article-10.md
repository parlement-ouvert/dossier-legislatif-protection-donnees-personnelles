<!-- TITLE: Article 10 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-ii/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

[L’article 35](/loi-78-17/chapitre-v/section-1/article-35) de la même loi est complété par l’alinéa suivant : « Toutefois, dans le champ d’application du [règlement (UE) 2016/679](/reglement-2016-679), le sous-traitant respecte les conditions prévues au [chapitre IV de ce règlement](/reglement-2016-679/chapitre-iv). »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 227 — Adopté](/amendements-1-assemblee-commission/article-10/amendement-227)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLES 18 ET 19 — PRESENTATION GENERALE ET DÉFINITIONS](/etude-impact-490/titre-iii/articles-18-et-19)
  * [ARTICLE 22 — MISE A DISPOSITION DE LA LISTE DES TRAITEMENTS AYANT FAIT L’OBJET DE FORMALITES PREALABLES](/etude-impact-490/titre-v/article-22)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-ii/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-ii/chapitre-iii/article-10' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

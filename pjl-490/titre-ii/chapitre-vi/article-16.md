<!-- TITLE: Article 16 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-ii/chapitre-vi" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii/chapitre-vi/article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Après [l’article 43 _ter_ ](/loi-78-17/chapitre-v/section-2/article-43-ter) de la même loi, il est inséré un article 43 _quater_ ainsi rédigé :

« _Art. 43_ quater. – La personne concernée peut mandater une association ou une organisation mentionnée au IV de l’article 43 _ter_ aux fins d’exercer en son nom les droits visés aux articles [77](/reglement-2016-679/chapitre-viii/article-77) à [79](/reglement-2016-679/chapitre-viii/article-79) du [règlement (UE) 2016/679](/reglement-2016-679). Elle peut également les mandater pour agir devant la Commission nationale de l’informatique et des libertés, contre celle-ci devant un juge ou contre le responsable du traitement ou le sous-traitant devant une juridiction lorsqu’est en cause un traitement relevant du [chapitre XIII](/loi-78-17/chapitre-xiii). »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 262 — Adopté](/amendements-1-assemblee-commission/avant-l-article-16/amendement-262)
  * [Amendement n° 70 — Retiré](/amendements-1-assemblee-commission/avant-l-article-16/amendement-70)
  * [Amendement n° 211 — Adopté](/amendements-1-assemblee-commission/article-16/amendement-211)
  * [Amendement n° 218 — Adopté](/amendements-1-assemblee-commission/article-16/amendement-218)
  * [Amendement n° 68 — Non soutenu](/amendements-1-assemblee-commission/article-16/amendement-68)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLES 18 ET 19 — PRESENTATION GENERALE ET DÉFINITIONS](/etude-impact-490/titre-iii/articles-18-et-19)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-ii/chapitre-vi" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii/chapitre-vi/article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-ii/chapitre-vi/article-16' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

<!-- TITLE: Article 14 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-ii/chapitre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii/chapitre-v/article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

[L’article 10](/loi-78-17/chapitre-ii/section-2/article-10) de la même loi est ainsi modifié :

1° Au deuxième alinéa :

 _a)_ Les mots : « Outre les cas mentionnés aux _a_ et _c_ sous le 2 de [l’article 22 du règlement 2016/679](/reglement-2016-679/chapitre-iii/section-4/article-22) » sont introduits au début de la première phrase ;

_b)_ Les mots : « définir le profil de l’intéressé » sont remplacés par le mot : « prévoir » ;

 _c)_ Les mots : « de sa personnalité » sont remplacés par les mots : « personnels relatifs à la personne concernée, à l’exception des décisions administratives individuelles prises dans le respect de [l’article L. 311-3-1 et du chapitre Ier du titre Ier du livre IV du code des relations du public et de l’administration](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033205535&cidTexte=LEGITEXT000031366350&dateTexte=20180113), à condition que le traitement ne porte pas sur des données mentionnées au [I de l’article 8](/loi-78-17/chapitre-ii/section-2/article-8), » ;

2° Le troisième alinéa est remplacé par les dispositions suivantes :

« Pour les décisions administratives mentionnées à l’alinéa précédent, le responsable du traitement s’assure de la maîtrise du traitement algorithmique et de ses évolutions ».

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 234 — Adopté](/amendements-1-assemblee-commission/avant-l-article-14/amendement-234)
  * [Amendement n° 10 — Non soutenu](/amendements-1-assemblee-commission/avant-l-article-14/amendement-10)
  * [Amendement n° 55 — Non soutenu](/amendements-1-assemblee-commission/avant-l-article-14/amendement-55)
  * [Amendement n° 11 — Non soutenu](/amendements-1-assemblee-commission/avant-l-article-14/amendement-11)
  * [Amendement n° 14 — Non soutenu](/amendements-1-assemblee-commission/avant-l-article-14/amendement-14)
  * [Amendement n° 4 — Non soutenu](/amendements-1-assemblee-commission/avant-l-article-14/amendement-4)
  * [Amendement n° 37 — Non soutenu](/amendements-1-assemblee-commission/article-14/amendement-37)
  * [Amendement n° 235 — Adopté](/amendements-1-assemblee-commission/article-14/amendement-235)
  * [Amendement n° 237 — Adopté](/amendements-1-assemblee-commission/article-14/amendement-237)
  * [Amendement n° 38 — Non soutenu](/amendements-1-assemblee-commission/apres-l-article-14/amendement-38)
  * [Amendement n° 45 — Non soutenu](/amendements-1-assemblee-commission/apres-l-article-14/amendement-45)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-ii/chapitre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii/chapitre-v/article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-ii/chapitre-v/article-14' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

<!-- TITLE: Article 9 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-ii/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

I. – [L’article 22](/loi-78-17/chapitre-iv/article-22) de la même loi est remplacé par les dispositions suivantes :

« [_Art. 22.](/loi-78-17/chapitre-iv/article-22) –_ Un __ décret en Conseil d’État, pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés __ détermine les catégories de responsables de traitement et les finalités de ces traitements au vu desquelles ces derniers peuvent être mis en œuvre lorsqu’ils portent sur des données comportant le numéro d’inscription des personnes au répertoire national d’identification des personnes physiques. La mise en œuvre des traitements intervient sans préjudice des obligations qui incombent aux responsables de traitement ou aux sous-traitants en vertu de la [section 3 du chapitre IV du règlement (UE) 2016/679](/reglement-2016-679/chapitre-iv/section-3).

« Ne sont pas soumis aux dispositions du premier alinéa ceux des traitements portant sur des données à caractère personnel parmi lesquelles figure le numéro d’inscription des personnes au répertoire national d’identification des personnes physiques ou qui requièrent une consultation de ce répertoire :

« 1° Qui ont exclusivement des finalités de statistique publique, mis en œuvre par le service statistique public et ne comportent aucune des données mentionnées au [I de l’article 8](/loi-78-17/chapitre-ii/section-2/article-8) ou à [l’article 9](/loi-78-17/chapitre-ii/section-2/article-9) ;

« 2° Qui ont exclusivement des finalités de recherche scientifique ou historique ;

« 3° Qui mettent à la disposition des usagers de l’administration un ou plusieurs téléservices de l’administration électronique définis à [l’article 1er de l’ordonnance n° 2005-1516 du 8 décembre 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives, mis en œuvre par l’État ou une personne morale de droit public ou une personne morale de droit privé gérant un service public.

« Pour les traitements dont les finalités sont mentionnées aux 1° et 2°, le numéro d’inscription au répertoire national d’identification des personnes physiques fait l’objet préalablement d’une opération cryptographique lui substituant un code statistique non signifiant. Cette opération est renouvelée à une fréquence définie par décret en Conseil d’État pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés. Les traitements ayant comme finalité exclusive de réaliser cette opération cryptographique ne sont pas soumis aux dispositions du premier alinéa.

« Pour les traitements dont les finalités sont mentionnées au 1°, l’utilisation du code statistique non signifiant n’est autorisée qu’au sein du service statistique public.

« Pour les traitements dont les finalités sont mentionnées au 2°, l’opération cryptographique et, le cas échéant, l’interconnexion de deux fichiers par l’utilisation du code spécifique non signifiant qui en est issu, ne peuvent être assurés par la même personne ni par le responsable de traitement.

« À l’exception des traitements mentionnés au [second alinéa de l’article 55](/loi-78-17/chapitre-ix/article-55), le présent article n’est pas applicable aux traitements de données à caractère personnel dans le domaine de la santé qui sont régis par les [dispositions du chapitre IX](/loi-78-17/chapitre-ix). »

II. – L’[article 27](/loi-78-17/chapitre-iv/section-2/article-27) de la même loi est ainsi modifié :

1° Au 2° du I :

 _a)_ La référence : « 2° » est supprimée ;

 _b)_ Après le mot : « État », sont insérés les mots : « , agissant dans l’exercice de ses prérogatives de puissance publique, » ;

 _c)_ Après les mots : « qui portent », sont insérés les mots : « sur des données génétiques ou » ;

2° Le 1° du I ainsi que les II, III et IV sont abrogés.

III. – Les articles [24](/loi-78-17/chapitre-iv/section-1/article-24) et [25](/loi-78-17/chapitre-iv/section-2/article-25) de la même loi sont abrogés.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 51 — Non soutenu](/amendements-1-assemblee-commission/article-9/amendement-51)
  * [Amendement n° 136 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-136)
  * [Amendement n° 137 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-137)
  * [Amendement n° 138 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-138)
  * [Amendement n° 139 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-139)
  * [Amendement n° 140 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-140)
  * [Amendement n° 226 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-226)
  * [Amendement n° 207 — Adopté](/amendements-1-assemblee-commission/article-9/amendement-207)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 9 — ALLEGEMENT DES FORMALITES PREALABLES](/etude-impact-490/titre-ii/chapitre-ii/article-9)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-ii/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-ii/chapitre-ii/article-9' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

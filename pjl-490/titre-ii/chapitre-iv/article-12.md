<!-- TITLE: Article 12 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-ii/chapitre-iv/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii/chapitre-iv/article-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

[L’article 36](/loi-78-17/chapitre-v/section-1/article-36) de la même loi est ainsi modifié :

1° Au premier alinéa, les mots : « historiques, statistiques ou scientifiques » sont remplacés par les mots : « archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques » ;

2° Les deuxième et cinquième alinéas sont abrogés ;

3° L’article est complété par l’alinéa suivant :

« Lorsque les traitements de données à caractère personnel sont mis en œuvre par les services publics d’archives à des fins archivistiques dans l’intérêt public conformément à [l’article L. 211-2 du code du patrimoine](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006845560&cidTexte=LEGITEXT000006074236&dateTexte=20180112), les droits visés aux articles [15](/reglement-2016-679/chapitre-iii/section-2/article-15), [16](reglement-2016-679/chapitre-iii/section-2/article-16), [18](reglement-2016-679/chapitre-iii/section-2/article-18), [19](reglement-2016-679/chapitre-iii/section-2/article-19), [20](reglement-2016-679/chapitre-iii/section-2/article-20) et [21](reglement-2016-679/chapitre-iii/section-2/article-21) du [règlement (UE) 2016/679](reglement-2016-679/) ne s’appliquent pas dans la mesure où ces droits rendent impossible ou entravent sérieusement la réalisation des finalités spécifiques et où de telles dérogations sont nécessaires pour atteindre ces finalités. Les conditions et garanties appropriées prévues à [l’article 89 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-ix/article-89) sont déterminées par le code du patrimoine et les autres dispositions législatives et réglementaires applicables aux archives publiques. Elles sont également assurées par le respect des normes conformes à l’état de l’art en matière d’archivage électronique. »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 36 — Non soutenu](/amendements-1-assemblee-commission/article-12/amendement-36)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-ii/chapitre-iv/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii/chapitre-iv/article-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-ii/chapitre-iv/article-12' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

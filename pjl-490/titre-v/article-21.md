<!-- TITLE: Article 21 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-v/article-22" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

La [loi n° 78-17 du 6 janvier 1978 relative à l’informatique et aux libertés](/loi-78-17)  est ainsi modifiée :

1° À [l’article 15](/loi-78-17/chapitre-iii/article-15), le quatrième alinéa est supprimé ;

2° À [l’article 16](/loi-78-17/chapitre-iii/article-16), le troisième alinéa est supprimé ;

3° À [l’article 29](/loi-78-17/chapitre-iv/section-2/article-29), le mot : « 25, » est supprimé ;

4° Au [I de l’article 30](/loi-78-17/chapitre-iv/section-3/article-30), le mot : « déclarations, » et les références à l’article 25 sont supprimées ;

5° Au [I de l’article 31](/loi-78-17/chapitre-iv/section-3/article-31), les mots : « 23 à » sont remplacés par les mots : « 26 et » et les mots : « ou la date de la déclaration de ce traitement » sont supprimés ;

6° Au dernier alinéa de [l’article 39](/loi-78-17/chapitre-v/section-2/article-39), les mots : « ou dans la déclaration » sont supprimés ;

7° À [l’article 67](/loi-78-17/chapitre-xi/article-67), sont supprimés :

 _a)_ Au premier alinéa, les mots : « 22, les 1° et 3° du I de [l’article 25](/loi-78-17/chapitre-iv/section-2/article-25), les articles » ;

 _b)_ Le quatrième alinéa ;

 _c)_ Au cinquième alinéa, les mots : « En cas de manquement constaté à ses devoirs, le correspondant est déchargé de ses fonctions sur demande, ou après consultation, de la Commission nationale de l’informatique et des libertés » ;

8° À [l’article 70](/loi-78-17/chapitre-xii/article-70), les premier et troisième alinéas sont supprimés et au deuxième alinéa, les mots : « saisie d’une déclaration déposée en application des articles [23](/loi-78-17/chapitre-iv/section-1/article-23) ou [24](/loi-78-17/chapitre-iv/section-1/article-24) et faisant apparaître que des données à caractère personnel seront transférées vers cet État, la Commission nationale de l’informatique et des libertés délivre le récépissé et » sont remplacés par les mots : « consultée en application de [l’article 36 du règlement (UE) 2016/679](/loi-78-17/chapitre-v/section-1/article-36) et en cas de transfert de données à caractère personnel vers cet État, la Commission » ;

9° La deuxième phrase de [l’article 71](/loi-78-17/chapitre-xiii/article-71) est supprimée.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 230 — Adopté](/amendements-1-assemblee-commission/article-21/amendement-230)
  * [Amendement n° 233 — Adopté](/amendements-1-assemblee-commission/article-21/amendement-233)
  * [Amendement n° 232 — Adopté](/amendements-1-assemblee-commission/article-21/amendement-232)
  * [Amendement n° 215 — Adopté](/amendements-1-assemblee-commission/article-21/amendement-215)
  * [Amendement n° 231 — Adopté](/amendements-1-assemblee-commission/article-21/amendement-231)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 18 ET ARTICLE 19 SECTION 3 — DROITS DE LA PERSONNE CONCERNÉE](/etude-impact-490/titre-iii/article-18-et-article-19-section-3)
  * [ARTICLE 20](/etude-impact-490/titre-iv/article-20)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-v/article-22" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-v/article-21' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

<!-- TITLE: Article 7 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-i/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

[L’article 8](/loi-78-17/chapitre-ii/section-2/article-8) de la même loi est ainsi modifié :

1° Le I est ainsi rédigé :

« _I._ – Il est interdit de traiter des données à caractère personnel, qui révèlent la prétendue origine raciale ou l’origine ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l’appartenance syndicale ou de traiter des données génétiques, des données biométriques aux fins d’identifier une personne physique de manière unique, des données concernant la santé ou des données concernant vie sexuelle ou l’orientation sexuelle d’une personne physique. » ;

2° Au 7° du II, les mots : « et dans les conditions prévues à l’article 25 de la présente loi » sont supprimés ;

3° Le 8° du II est remplacé par les dispositions suivantes : « _8_ ° Les traitements comportant des données concernant la santé justifiés par l’intérêt public et conformes aux dispositions du [chapitre IX](/loi-78-17/chapitre-ix). » ;

4° Après le 8° du II, il est inséré un 9° ainsi rédigé :

« _9_ ° Les traitements mis en œuvre par les employeurs ou les administrations qui portent sur des données biométriques nécessaires aux contrôles de l’accès aux lieux de travail ainsi qu’aux appareils et aux applications utilisés dans le cadre des missions confiées aux salariés ou aux agents. » ;

5° Au III, la première phrase est remplacée par la phrase suivante :

« Ne sont également pas soumises à l’interdiction prévue au I les données à caractère personnel mentionnées au I qui sont appelées à faire l’objet, à bref délai, d’un procédé d’anonymisation préalablement reconnu conforme aux dispositions de la présente loi par la Commission nationale de l’informatique et des libertés. »

Et la seconde phrase est supprimée ;

6° Le IV est remplacé par les dispositions suivantes :

« _IV_. – De même, ne sont pas soumis à l’interdiction prévue au I les traitements, automatisés ou non, justifiés par l’intérêt public et autorisés dans les conditions prévues au II de l’[article 26](/loi-78-17/chapitre-iv/section-2/article-26). »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 133 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-133)
  * [Amendement n° 54 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-54)
  * [Amendement n° 17 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-17)
  * [Amendement n° 260 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-260)
  * [Amendement n° 255 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-255)
  * [Amendement n° 7 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-7)
  * [Amendement n° 48 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-48)
  * [Amendement n° 134 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-134)
  * [Amendement n° 16 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-16)
  * [Amendement n° 135 — Adopté](/amendements-1-assemblee-commission/article-7/amendement-135)
  * [Amendement n° 50 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-50)
  * [Amendement n° 34 — Non soutenu](/amendements-1-assemblee-commission/article-7/amendement-34)
  * [Amendement n° 47 — Non soutenu](/amendements-1-assemblee-commission/apres-l-article-7/amendement-47)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-i/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-i/chapitre-ii/article-7' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

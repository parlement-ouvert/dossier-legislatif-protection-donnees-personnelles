<!-- TITLE: Article 1er -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-i/chapitre-i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-i/chapitre-i/article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

[L’article 11 de la loi n° 78-17 du 6 janvier 1978](/loi-78-17/chapitre-iii/article-11) relative à l’informatique, aux fichiers et aux libertés est ainsi modifié :

1° Au début du premier alinéa, est insérée la référence : « I. - » ;

2° Après la première phrase du premier alinéa est insérée la phrase suivante :

« Elle est l’autorité de contrôle nationale au sens et pour l’application du  [règlement (UE) 2016/679](/reglement-2016-679) » ;

3° Au _a_ du 2° les mots : « autorise les traitements mentionnés à l’article 25, » et les mots : » et reçoit les déclarations relatives aux autres traitements » sont supprimés ;

4° Après le _a_ du 2°, il est inséré un _a_ bis ainsi rédigé :

« _a_ bis __ Elle établit et publie des lignes directrices, recommandations ou référentiels destinés à faciliter la mise en conformité des traitements de données à caractère personnel avec les textes relatifs à la protection des données à caractère personnel et à procéder à l’évaluation préalable des risques par les responsables de traitement et leurs sous-traitants. Elle encourage l’élaboration de codes de conduite définissant les obligations qui incombent aux responsables du traitement et aux sous-traitants, compte tenu du risque inhérent aux traitements de données à caractère personnel pour les droits et libertés des personnes physiques ; elle homologue et publie les méthodologies de référence mentionnées au IV de l’article 54, destinées à favoriser la conformité des traitement de données de santé à caractère personnel » ;

5° Le _b_ du 2° est remplacé par les dispositions suivantes :

« _b)_ Elle établit et publie des règlements types en vue d’assurer la sécurité des systèmes de traitement de données à caractère personnel et de régir les traitements de données de santé relevant du [chapitre IX](/loi-78-17/chapitre-ix). À ce titre, sauf pour les traitements mis en œuvre pour le compte de l’État, agissant dans l’exercice de ses prérogatives de puissance publique, elle peut prescrire des mesures techniques et organisationnelles supplémentaires pour le traitement des données biométriques, génétiques et de santé conformément à [l’article 9.4 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-ii/article-9) et des garanties complémentaires en matière de traitement de données d’infraction conformément à [l’article 10](/reglement-2016-679/chapitre-ii/article-10) du même règlement. » ;

6° Après le _f_ du 2°, il est inséré un _f_ bis ainsi rédigé :

« _f_ bis _)_ Elle peut décider de certifier des personnes, des produits, des systèmes de données ou des procédures aux fins de reconnaître qu’ils se conforment au [règlement (UE) 2016/679](/reglement-2016-679) et la présente loi. Elle agrée, aux mêmes fins, des organismes certificateurs, sur la base, le cas échéant, de leur accréditation par l’instance nationale d’accréditation, mentionnée à [l’article 43(1) _b_ du règlement](/reglement-2016-679/chapitre-iv/section-5/article-43), dans des conditions précisées par décret en Conseil d’État pris après avis de la Commission nationale de l’informatique et des libertés. La commission élabore ou approuve les critères des référentiels de certification et d’agrément. Elle peut établir des exigences supplémentaires aux normes d’accréditation. » ;

7° Au _g_ du 2°, après le mot : « certification » sont insérés les mots : « , par des tiers agréés ou accrédités selon les modalités mentionnées au _f_ bis, » ;

8° Au _h_ du 2°, les mots : « d’accès concernant les traitements mentionnés aux [articles 41](/reglement-2016-679/chapitre-iv/section-5/article-41) et [42](/reglement-2016-679/chapitre-iv/section-5/article-42) » sont remplacés par les mots : « d’exercice des droits prévues aux articles 41, 42 et 70-22 » ;

9° Après le _h_ du 2°, il est inséré un _i_ ainsi rédigé :

« _i)_ Elle peut établir une liste des traitements susceptibles de créer un risque élevé devant faire l’objet d’une consultation préalable conformément à [l’article 70-4](/reglement-2016-679/chapitre-vii/section-3/article-70) » ;

10° Au _a_ du 4°, après la première phrase, il est inséré une phrase ainsi rédigée :

« Elle peut également être consultée par le président de l’Assemblée nationale ou par le président du Sénat sur toute proposition de loi relative à la protection des données à caractère personnel ou au traitement de telles données. » ;

11° Après le _f_ du 4°, est inséré un alinéa ainsi rédigé :

« 5° Elle peut présenter des observations devant toute juridiction à l’occasion d’un litige relatif à l’application du règlement (UE) 2016/679 et de la présente loi » ;

12° Au début du vingt-sixième alinéa, est insérée la référence : « II. - ».

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 88 — Adopté](/amendements-1-assemblee-commission/titre-ier/amendement-88)
  * [Amendement n° 5 — Rejeté](/amendements-1-assemblee-commission/avant-l-article-premier/amendement-5)
  * [Amendement n° 13 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-13)
  * [Amendement n° 239 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-239)
  * [Amendement n° 240 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-240)
  * [Amendement n° 21 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-21)
  * [Amendement n° 89 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-89)
  * [Amendement n° 90 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-90)
  * [Amendement n° 56 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-56)
  * [Amendement n° 241 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-241)
  * [Amendement n° 242 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-242)
  * [Amendement n° 243 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-243)
  * [Amendement n° 23 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-23)
  * [Amendement n° 244 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-244)
  * [Amendement n° 22 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-22)
  * [Amendement n° 245 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-245)
  * [Amendement n° 91 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-91)
  * [Amendement n° 92 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-92)
  * [Amendement n° 246 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-246)
  * [Amendement n° 93 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-93)
  * [Amendement n° 94 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-94)
  * [Amendement n° 96 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-96)
  * [Amendement n° 264 — Adopté](/amendements-1-assemblee-commission/article-premier/amendement-264)
  * [Amendement n° 24 — Rejeté](/amendements-1-assemblee-commission/article-premier/amendement-24)
  * [Amendement n° 57 — Retiré](/amendements-1-assemblee-commission/article-premier/amendement-57)
  * [Amendement n° 65 — Retiré](/amendements-1-assemblee-commission/article-premier/amendement-65)
  * [Amendement n° 73 — Retiré](/amendements-1-assemblee-commission/article-premier/amendement-73)
  * [Amendement n° 25 — Rejeté](/amendements-1-assemblee-commission/apres-l-article-premier/amendement-25)
  * [Amendement n° 247 — Adopté](/amendements-1-assemblee-commission/apres-l-article-premier/amendement-247)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 1ER — MISSIONS DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier/article-1er)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-i/chapitre-i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-i/chapitre-i/article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-i/chapitre-i/article-1' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

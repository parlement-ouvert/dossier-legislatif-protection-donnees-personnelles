<!-- TITLE: Article 3 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-i/chapitre-i/article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-i/chapitre-i/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

I. – Au premier alinéa de [l’article 17](/loi-78-17/chapitre-iii/article-17) de la même loi, après les mots : « la formation restreinte », sont ajoutés les mots : « prend les mesures et » et après les mots : « obligations découlant » sont ajoutés les mots : « du règlement (UE) 2016/679 et ».

II. – Après le premier alinéa de [l’article 17](/loi-78-17/chapitre-iii/article-17) de la même loi, il est inséré un alinéa ainsi rédigé :

« Les membres délibèrent hors de la présence des agents de la commission, à l’exception de ceux chargés de la tenue de la séance ».

III. – Le deuxième alinéa de [l’article 18](/loi-78-17/chapitre-iii/article-18) de la même loi est remplacé par les dispositions suivantes :

« Le commissaire du Gouvernement assiste à toutes les délibérations de la commission réunie en formation plénière, ainsi qu’à celles des réunions de son bureau qui ont pour objet l’exercice des attributions déléguées en vertu de [l’article 16](/loi-78-17/chapitre-iii/article-16). Il peut assister aux séances de la formation restreinte, sans être présent au délibéré. Il est rendu destinataire de l’ensemble des avis et décisions de la commission et de la formation restreinte ».

IV. – Le troisième alinéa de [l’article 18](/loi-78-17/chapitre-iii/article-18) de la même loi est remplacé par les dispositions suivantes :

« Sauf en matière de mesures ou de sanctions relevant du [chapitre VII](/loi-78-17/chapitre-vii), il peut provoquer une seconde délibération de la commission, qui doit intervenir dans les dix jours de la délibération initiale ».

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 97 — Adopté](/amendements-1-assemblee-commission/article-3/amendement-97)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLES 2 et 3 — COMMISSAIRE DU GOUVERNEMENT ET MEMBRES DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier/articles-2-et-3)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-i/chapitre-i/article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-i/chapitre-i/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-i/chapitre-i/article-3' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

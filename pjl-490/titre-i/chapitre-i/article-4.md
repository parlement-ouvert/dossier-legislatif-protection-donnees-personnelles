<!-- TITLE: Article 4 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-i/chapitre-i/article-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-i/chapitre-i/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

[L’article 44](/loi-78-17/chapitre-vi/article-44) de la même loi est ainsi modifié :

1° Au I, les mots : « et qui sont à usage professionnel » sont supprimés ;

2° À la première phrase du II, les mots : « de locaux professionnels privés » sont remplacés par les mots : « de ces lieux, locaux, enceintes, installations ou établissements » et à la dernière phrase du même II, après le mot : « visite » est ajouté le membre de phrase suivant :

« dont la finalité est l’exercice effectif des missions prévues au III » ;

3° Au III, les trois premiers alinéas sont remplacés par deux alinéas ainsi rédigés :

« Pour l’exercice des missions confiées à la Commission nationale de l’informatique et des libertés par le [règlement (UE) 2016/679](/reglement-2016-679) et par la présente loi, les membres et agents mentionnés au premier alinéa du I peuvent demander communication de tous documents, quel qu’en soit le support, et en prendre copie. Ils peuvent recueillir, notamment sur place ou sur convocation, tout renseignement et toute justification utiles. Ils peuvent accéder, dans des conditions préservant la confidentialité à l’égard des tiers, aux programmes informatiques et aux données, ainsi qu’en demander la transcription par tout traitement approprié dans des documents directement utilisables pour les besoins du contrôle. Le secret ne peut leur être opposé sauf concernant les informations couvertes par le secret professionnel applicable aux relations entre un avocat et son client, par le secret des sources des traitements journalistiques ou, sous réserve des dispositions de l’alinéa suivant, par le secret médical.

« Le secret médical est opposable s’agissant des informations qui figurent dans un traitement nécessaire aux fins de la médecine préventive, de la recherche médicale, des diagnostics médicaux, de l’administration de soins ou de traitements, ou de la gestion de service de santé. Toutefois la communication des données médicales individuelles incluses dans cette catégorie de traitement peut être faite sous l’autorité et en présence d’un médecin. » ;

4° Après le quatrième alinéa du III, il est inséré un alinéa ainsi rédigé :

« Pour le contrôle de services de communication au public en ligne, les membres et agents mentionnés au premier alinéa du I peuvent réaliser toute opération nécessaire à leur mission sous une identité d’emprunt. L’utilisation d’une identité d’emprunt est sans incidence sur la régularité des constatations effectuées conformément à l’alinéa précédent. Un décret en Conseil d’État pris après avis de la Commission nationale de l’informatique et des libertés précise les conditions dans lesquelles ils procèdent dans ces cas à leurs constatations. » ;

5° Il est ajouté un alinéa ainsi rédigé :

« _V_. – Dans l’exercice de son pouvoir de contrôle portant sur les traitements relevant du [règlement (UE) 2016/679](/reglement-2016-679) et de la présente loi, la Commission nationale de l’informatique et des libertés n’est pas compétente pour contrôler les opérations de traitement effectuées, dans l’exercice de leur fonction juridictionnelle, par les juridictions. »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 250 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-250)
  * [Amendement n° 98 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-98)
  * [Amendement n° 64 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-64)
  * [Amendement n° 76 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-76)
  * [Amendement n° 28 — Retiré](/amendements-1-assemblee-commission/article-4/amendement-28)
  * [Amendement n° 77 — Rejeté](/amendements-1-assemblee-commission/article-4/amendement-77)
  * [Amendement n° 58 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-58)
  * [Amendement n° 63 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-63)
  * [Amendement n° 72 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-72)
  * [Amendement n° 78 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-78)
  * [Amendement n° 62 — Rejeté](/amendements-1-assemblee-commission/article-4/amendement-62)
  * [Amendement n° 79 — Rejeté](/amendements-1-assemblee-commission/article-4/amendement-79)
  * [Amendement n° 61 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-61)
  * [Amendement n° 80 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-80)
  * [Amendement n° 100 — Adopté](/amendements-1-assemblee-commission/article-4/amendement-100)
* Délibération n°2017-299 du 30 novembre 2017 — Commission Nationale de l'Informatique et des Libertés
  * [Article 4 (Pouvoirs de contrôle de la Commission)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-4-pouvoirs-de-controle-de-la-commission)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 8 — CRITERE D’APPLICATION DU DROIT](/etude-impact-490/titre-ii/chapitre-ier/article-8)
  * [ARTICLES 18 ET 19 — PRESENTATION GENERALE ET DÉFINITIONS](/etude-impact-490/titre-iii/articles-18-et-19)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-i/chapitre-i/article-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-i/chapitre-i/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-i/chapitre-i/article-4' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

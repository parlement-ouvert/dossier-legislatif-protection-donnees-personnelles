<!-- TITLE: Article 5 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-i/chapitre-i/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-i/chapitre-i/article-6" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

I. – [L’article 49](/loi-78-17/chapitre-vii/article-49) de la même loi est remplacé par les dispositions suivantes :

« [_Art. 49.](/loi-78-17/chapitre-vii/article-49) –_ Dans les conditions prévues aux articles [60](/reglement-2016-679/chapitre-vii/section-1/article-60) à [67](/reglement-2016-679/chapitre-vii/section-2/article-67), du [règlement (UE) 2016/679](/reglement-2016-679), la Commission nationale de l’informatique et des libertés met en œuvre des procédures de coopération et d’assistance mutuelle avec les autorités de contrôle des autres États membres de l’Union européenne, et réalise avec elles des opérations conjointes.

« La commission, le président, le bureau, la formation restreinte et les agents de la commission mettent en œuvre, chacun pour ce qui les concerne, les procédures visées à l’alinéa précédent. »

II. – Après [l’article 49](/loi-78-17/chapitre-vii/article-49), sont insérés les articles 49-1, 49-2, 49-3 et 49-4 ainsi rédigés :

« _Art. 49-1. –_ I. – La Commission nationale de l’informatique et des libertés coopère avec les autorités de contrôle des autres États membres de l’Union européenne en application de [l’article 62 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vii/section-1/article-62), dans les conditions prévues au présent article. Cette coopération n’est pas applicable aux traitements qui ne relèvent pas du champ d’application du droit de l’Union européenne.

« II. – Qu’elle agisse en tant qu’autorité de contrôle chef de file ou en tant qu’autorité concernée au sens des articles [4](/reglement-2016-679/chapitre-i/article-4) et [56](/reglement-2016-679/chapitre-vi/section-2/article-56) du [règlement (UE) 2016/679](/reglement-2016-679), la Commission nationale de l’informatique et des libertés est compétente pour traiter une réclamation ou une éventuelle violation des dispositions du même règlement affectant par ailleurs d’autres États membres. Le président de la commission invite les autres autorités de contrôle concernées à participer aux opérations de contrôle conjointes qu’il décide de conduire.

« III. – Lorsqu’une opération de contrôle conjointe se déroule sur le territoire français, des membres ou agents habilités de la commission, agissant en tant qu’autorité de contrôle d’accueil, sont présents aux côtés des membres et agents des autres autorités de contrôle participant, le cas échéant, à l’opération. À la demande de l’autorité de contrôle de l’État membre, le président de la commission peut habiliter, par décision particulière, ceux des membres ou agents de l’autorité de contrôle concernée qui présentent des garanties comparables à celles requises des agents de la commission, en application des dispositions de [l’article 19](/loi-78-17/chapitre-iii/article-19), à exercer, sous son autorité, tout ou partie des pouvoirs de vérification et d’enquête dont disposent les membres et les agents de la commission.

« IV. – Lorsque la commission est invitée à contribuer à une opération de contrôle conjointe décidée par une autre autorité compétente, le président de la commission se prononce sur le principe et les conditions de la participation, désigne les membres et agents habilités, et en informe l’autorité requérante dans les conditions prévues à [l’article 62 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vii/section-1/article-62).

« _Art. 49-2._ – I. – Les traitements mentionnés à [l’article 70-1](/loi-78-17/chapitre-xii/article-70) font l’objet d’une coopération entre la Commission nationale de l’informatique et des libertés et les autorités de contrôle des autres États membres de l’Union européenne dans les conditions prévues au présent article.

« II. – La commission communique aux autorités de contrôle des autres États membres les informations utiles et leur prête assistance en mettant notamment en œuvre, à leur demande, des mesures de contrôle telles que les mesures de consultation, d’inspections et d’enquête.

« La commission répond à une demande d’assistance mutuelle formulée par une autre autorité de contrôle dans les meilleurs délais et au plus tard un mois après réception de la demande contenant toutes les informations nécessaires, notamment sa finalité et ses motifs. Elle ne peut refuser de satisfaire à cette demande que si elle n’est pas compétente pour traiter l’objet de la demande ou les mesures qu’elle est invitée à exécuter, ou si une disposition du droit de l’Union européenne ou du droit français y fait obstacle.

« La Commission informe l’autorité requérante des résultats obtenus ou, selon le cas, de l’avancement du dossier ou des mesures prises pour donner suite à la demande.

« La commission peut, pour l’exercice de ses missions, solliciter l’assistance d’une autorité de contrôle d’un autre État membre de l’Union européenne.

« La commission donne les motifs de tout refus de satisfaire une demande lorsqu’elle estime ne pas être compétente ou lorsqu’elle considère que satisfaire à la demande constituerait une violation du droit de l’Union européenne, ou de la législation française.

« _Art. 49-3. –_ Lorsque la commission agit en tant qu’autorité de contrôle chef de file s’agissant d’un traitement transfrontalier au sein de l’Union européenne, elle communique le rapport du membre rapporteur, ainsi que l’ensemble des informations utiles de la procédure ayant permis d’établir le rapport, aux autres autorités de contrôle concernées sans tarder et avant l’éventuelle audition du responsable du traitement ou du sous-traitant. Les autorités concernées sont mises en mesure d’assister à l’audition par la formation restreinte du responsable de traitement ou du sous-traitant par tout moyen de retransmission approprié, ou de prendre connaissance d’un procès-verbal dressé à la suite de l’audition.

« Après en avoir délibéré, la formation restreinte soumet son projet de décision aux autres autorités concernées conformément à la procédure définie à [l’article 60 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vii/section-1/article-60). À ce titre, elle se prononce sur la prise en compte des objections pertinentes et motivées émises par les autorités concernées et saisit, si elle décide d’écarter l’une des objections, le comité européen de la protection des données conformément à [l’article 65 du règlement](/reglement-2016-679/chapitre-vii/section-2/article-65).

« Les conditions d’application du présent article sont définies par un décret en Conseil d’État, après avis de la Commission nationale de l’informatique et des libertés.

« _Art. 49-4. –_ Lorsque la commission agit en tant qu’autorité concernée, au sens du [règlement (UE) 2016/679](/reglement-2016-679), le président de la commission est saisi des projets de mesures correctrices soumis à la commission par une autre autorité chef de file.

« Lorsque ces mesures sont d’objet équivalent à celles définies aux [I et III de l’article 45](/loi-78-17/chapitre-vii/article-45), le président décide, le cas échéant, d’émettre une objection pertinente et motivée selon les modalités prévues à [l’article 60 de ce règlement](/reglement-2016-679/chapitre-vii/section-1/article-60).

« Lorsque ces mesures sont d’objet équivalent à celles définies au [II de l’article 45](/loi-78-17/chapitre-vii/article-45) et à [l’article 46](/loi-78-17/chapitre-vii/article-46), le président saisit la formation restreinte. Le président de la formation restreinte ou le membre de la formation restreinte qu’il désigne peut, le cas échéant, émettre une objection pertinente et motivée selon les mêmes modalités. »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 252 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-252)
  * [Amendement n° 101 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-101)
  * [Amendement n° 102 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-102)
  * [Amendement n° 103 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-103)
  * [Amendement n° 29 — Rejeté](/amendements-1-assemblee-commission/article-5/amendement-29)
  * [Amendement n° 104 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-104)
  * [Amendement n° 105 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-105)
  * [Amendement n° 106 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-106)
  * [Amendement n° 108 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-108)
  * [Amendement n° 109 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-109)
  * [Amendement n° 110 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-110)
  * [Amendement n° 112 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-112)
  * [Amendement n° 113 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-113)
  * [Amendement n° 114 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-114)
  * [Amendement n° 115 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-115)
  * [Amendement n° 31 — Rejeté](/amendements-1-assemblee-commission/article-5/amendement-31)
  * [Amendement n° 253 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-253)
  * [Amendement n° 117 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-117)
  * [Amendement n° 118 — Adopté](/amendements-1-assemblee-commission/article-5/amendement-118)
* Délibération n°2017-299 du 30 novembre 2017 — Commission Nationale de l'Informatique et des Libertés
  * [Article 5 (Coopération)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-5-cooperation)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 5 — PROCÉDURE DE COOPÉRATION DE LA CNIL AVEC LES AUTRES AUTORITÉS DE CONTRÔLE](/etude-impact-490/titre-ier/chapitre-ier/article-5)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-i/chapitre-i/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-i/chapitre-i/article-6" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-i/chapitre-i/article-5' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

<!-- TITLE: TITRE II -->
<!-- SUBTITLE: MARGES DE MANŒUVRE PERMISES PAR LE RÈGLEMENT (UE) 2016/679 DU PARLEMENT EUROPÉEN ET DU CONSEIL DU 27 AVRIL 2016 RELATIF À LA PROTECTION DES PERSONNES PHYSIQUES À L’ÉGARD DU TRAITEMENT DES DONNÉES À CARACTÈRE PERSONNEL ET À LA LIBRE CIRCULATION DE CES DONNÉES, ET ABROGEANT LA DIRECTIVE 95/46/CE -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-i/chapitre-ii/article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii/chapitre-i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Chapitre I<sup>er</sup> — Champ d’application territorial des dispositions complétant le règlement (UE) 2016/679](/pjl-490/titre-ii/chapitre-i)

  * [Article 8](/pjl-490/titre-ii/chapitre-i/article-8)

* [Chapitre II — <span>Dispositions relatives à la simplification des formalités préalables à la mise en </span><span><!--&#339;-->œ</span><span>uvre des traitements</span>](/pjl-490/titre-ii/chapitre-ii)

  * [Article 9](/pjl-490/titre-ii/chapitre-ii/article-9)

* [Chapitre III — Obligations incombant aux responsables de traitements et sous-traitants](/pjl-490/titre-ii/chapitre-iii)

  * [Article 10](/pjl-490/titre-ii/chapitre-iii/article-10)

* [Chapitre IV — Dispositions relatives à certaines catégories particulières de traitement](/pjl-490/titre-ii/chapitre-iv)

  * [Article 11](/pjl-490/titre-ii/chapitre-iv/article-11)
  * [Article 12](/pjl-490/titre-ii/chapitre-iv/article-12)
  * [Article 13](/pjl-490/titre-ii/chapitre-iv/article-13)

* [Chapitre V — Dispositions particulières relatives aux droits des personnes concernées](/pjl-490/titre-ii/chapitre-v)

  * [Article 14](/pjl-490/titre-ii/chapitre-v/article-14)
  * [Article 15](/pjl-490/titre-ii/chapitre-v/article-15)

* [Chapitre VI — Voies de recours](/pjl-490/titre-ii/chapitre-vi)

  * [Article 16](/pjl-490/titre-ii/chapitre-vi/article-16)
  * [Article 17](/pjl-490/titre-ii/chapitre-vi/article-17)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-i/chapitre-ii/article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-ii/chapitre-i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

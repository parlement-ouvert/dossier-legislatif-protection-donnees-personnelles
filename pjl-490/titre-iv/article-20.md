<!-- TITLE: Article 20 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

I. – Dans les conditions prévues à [l’article 38 de la Constitution](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000571356&idArticle=LEGIARTI000006527512&dateTexte=&categorieLien=cid), le Gouvernement est autorisé à prendre par voie d’ordonnance les mesures relevant du domaine de la loi nécessaires :

1° À la réécriture de l’ensemble de la [loi n° 78-17 du 6 janvier 1978](/loi-78-17) relative à l’informatique, aux fichiers et aux libertés afin d’apporter les corrections formelles et les adaptations nécessaires à la simplification et à la cohérence ainsi qu’à la simplicité de la mise en œuvre par les personnes concernées des dispositions qui mettent le droit national en conformité avec le [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 et transposent la [directive (UE) 2016/680](/directive-2016-680) du Parlement européen et du Conseil du 27 avril 2016, telles que résultant de la présente loi ;

2° Pour mettre en cohérence avec ces changements l’ensemble de la législation applicable à la protection des données à caractère personnel, apporter les modifications qui seraient rendues nécessaires pour assurer le respect de la hiérarchie des normes et la cohérence rédactionnelle des textes, harmoniser l’état du droit, remédier aux éventuelles erreurs et omissions résultant de la présente loi, et abroger les dispositions devenues sans objet ;

3° À l’adaptation et aux extensions à l’outre-mer des dispositions prévues aux 1° et 2°, ainsi qu’à l’application en Nouvelle-Calédonie, à Wallis-et-Futuna en Polynésie française, à Saint-Barthélemy, à Saint-Pierre-et-Miquelon et dans les Terres australes et antarctique françaises.

II. – Cette ordonnance est prise, après avis de la Commission nationale de l’informatique et des libertés, dans un délai de six mois à compter de la promulgation de la présente loi.

III. – Un projet de loi de ratification est déposé devant le Parlement dans un délai de six mois à compter de la publication de l’ordonnance.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 1 — Rejeté](/amendements-1-assemblee-commission/article-20/amendement-1)
  * [Amendement n° 42 — Rejeté](/amendements-1-assemblee-commission/article-20/amendement-42)
  * [Amendement n° 214 — Adopté](/amendements-1-assemblee-commission/article-20/amendement-214)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 20](/etude-impact-490/titre-iv/article-20)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-490/titre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version du Gouvernement</span> <a href="/pjl-490/titre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/pjl-490/titre-iv/article-20' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

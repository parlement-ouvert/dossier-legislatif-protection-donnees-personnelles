<!-- TITLE: Notes -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/signataires" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <span> </span> </div>

(1) [JO C 391 du 18.12.2012, p. 127](./../../../../legal-content/FR/AUTO/?uri=OJ:C:2012:391:TOC).

(2) Position du Parlement européen du 12 mars 2014 (non encore parue au Journal officiel) et position du Conseil en première lecture du 8 avril 2016 (non encore parue au Journal officiel). Position du Parlement européen du 14 avril 2016.

(3) Directive 95/46/CE du Parlement européen et du Conseil du 24 octobre 1995 relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données ([JO L 281 du 23.11.1995, p. 31](./../../../../legal-content/FR/AUTO/?uri=OJ:L:1995:281:TOC)).

(4) Décision-cadre 2008/977/JAI du Conseil du 27 novembre 2008 relative à la protection des données à caractère personnel traitées dans le cadre de la coopération policière et judiciaire en matière pénale ([JO L 350 du 30.12.2008, p. 60](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2008:350:TOC)).

(5) Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données et abrogeant la directive 95/46/CE (règlement général sur la protection des données) (voir page 1 du présent Journal officiel).

(6) Règlement (CE) no 45/2001 du Parlement européen et du Conseil du 18 décembre 2000 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel par les institutions et organes communautaires et à la libre circulation de ces données ([JO L 8 du 12.1.2001, p. 1](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2001:008:TOC)).

(7) Directive 2011/24/UE du Parlement européen et du Conseil du 9 mars 2011 relative à l'application des droits des patients en matière de soins de santé transfrontaliers ([JO L 88 du 4.4.2011, p. 45](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2011:088:TOC)).

(8) Position commune 2005/69/JAI du Conseil du 24 janvier 2005 relative à l'échange de certaines données avec Interpol ([JO L 27 du 29.1.2005, p. 61](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2005:027:TOC)).

(9) Décision 2007/533/JAI du Conseil du 12 juin 2007 sur l'établissement, le fonctionnement et l'utilisation du système d'information Schengen de deuxième génération (SIS II) ([JO L 205 du 7.8.2007, p. 63](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2007:205:TOC)).

(10) Directive 77/249/CEE du Conseil du 22 mars 1977 tendant à faciliter l'exercice effectif de la libre prestation de services par les avocats ([JO L 78 du 26.3.1977, p. 17](./../../../../legal-content/FR/AUTO/?uri=OJ:L:1977:078:TOC)).

(11) Règlement (UE) no 182/2011 du Parlement européen et du Conseil du 16 février 2011 établissant les règles et principes généraux relatifs aux modalités de contrôle par les États membres de l'exercice des compétences d'exécution par la Commission ([JO L 55 du 28.2.2011, p. 13](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2011:055:TOC)).

(12) Décision 2008/615/JAI du Conseil du 23 juin 2008 relative à l'approfondissement de la coopération transfrontalière, notamment en vue de lutter contre le terrorisme et la criminalité transfrontalière ([JO L 210 du 6.8.2008, p. 1](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2008:210:TOC)).

(13) Acte du Conseil du 29 mai 2000 établissant, conformément à l'article 34 du traité sur l'Union européenne, la convention relative à l'entraide judiciaire en matière pénale entre les États membres de l'Union européenne ([JO C 197 du 12.7.2000, p. 1](./../../../../legal-content/FR/AUTO/?uri=OJ:C:2000:197:TOC)).

(14) Directive 2011/93/UE du Parlement européen et du Conseil du 13 décembre 2011 relative à la lutte contre les abus sexuels et l'exploitation sexuelle des enfants, ainsi que la pédopornographie et remplaçant la décision-cadre 2004/68/JAI du Conseil ([JO L 335 du 17.12.2011, p. 1](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2011:335:TOC)).

(15) [JO L 176 du 10.7.1999, p. 36](./../../../../legal-content/FR/AUTO/?uri=OJ:L:1999:176:TOC).

(16) [JO L 53 du 27.2.2008, p. 52](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2008:053:TOC).

(17) [JO L 160 du 18.6.2011, p. 21](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2011:160:TOC).

(18) [JO C 192 du 30.6.2012, p. 7](./../../../../legal-content/FR/AUTO/?uri=OJ:C:2012:192:TOC).

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/signataires" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <span> </span> </div>

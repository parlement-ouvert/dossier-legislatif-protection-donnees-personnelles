<!-- TITLE: Article 50 -->
<!-- SUBTITLE: Assistance mutuelle -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-vii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-vii/article-51" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Chaque État membre prévoit que leurs autorités de contrôle se communiquent les informations utiles et se prêtent mutuellement assistance en vue de mettre en œuvre et d'appliquer la présente directive de façon cohérente, et met en place des mesures pour coopérer efficacement. L'assistance mutuelle concerne notamment les demandes d'information et les mesures de contrôle, telles que les demandes de consultation, les inspections et les enquêtes.

2\. Chaque État membre prévoit que chaque autorité de contrôle prend toutes les mesures appropriées requises pour répondre à la demande d'une autre autorité de contrôle dans les meilleurs délais et au plus tard un mois après réception de la demande. De telles mesures peuvent comprendre notamment la transmission d'informations utiles sur la conduite d'une enquête.

3\. Les demandes d'assistance contiennent toutes les informations nécessaires, notamment la finalité et les motifs de la demande. Les informations échangées ne sont utilisées qu'aux fins pour lesquelles elles ont été demandées.

4\. Une autorité de contrôle saisie d'une demande ne peut refuser d'y satisfaire, sauf si:

<table>
<tr>
<td>

a)

</td>
<td>

elle n'est pas compétente pour traiter l'objet de la demande ou les mesures qu'elle est invitée à exécuter; ou

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

satisfaire à la demande constituerait une violation de la présente directive ou du droit de l'Union ou du droit de l'État membre auquel l'autorité de contrôle qui a reçu la demande est soumise.

</td> </tr> </table>

5\. L'autorité de contrôle requise informe l'autorité de contrôle requérante des résultats obtenus ou, selon le cas, de l'avancement du dossier ou des mesures prises pour donner suite à la demande. L'autorité de contrôle requise donne les motifs de tout refus de satisfaire à une demande en application du paragraphe 4.

6\. Les autorités de contrôle requises communiquent, en règle générale, par voie électronique et au moyen d'un formulaire type, les informations demandées par d'autres autorités de contrôle.

7\. Les autorités de contrôle requises ne perçoivent pas de frais pour une mesure qu'elles prennent à la suite d'une demande d'assistance mutuelle. Les autorités de contrôle peuvent convenir de règles concernant l'octroi de dédommagements entre elles pour des dépenses spécifiques résultant de la fourniture d'une assistance mutuelle dans des circonstances exceptionnelles.

8\. La Commission peut, par voie d'actes d'exécution, préciser la forme et les procédures de l'assistance mutuelle visée au présent article, ainsi que les modalités de l'échange d'informations par voie électronique entre les autorités de contrôle et entre les autorités de contrôle et le comité. Ces actes d'exécution sont adoptés en conformité avec la procédure d'examen visée à l'article 58, paragraphe 2.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-vii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-vii/article-51" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

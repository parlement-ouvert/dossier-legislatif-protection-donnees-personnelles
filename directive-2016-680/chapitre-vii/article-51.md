<!-- TITLE: Article 51 -->
<!-- SUBTITLE: Missions du comité -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-vii/article-50" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-viii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Le comité institué par le règlement (UE) 2016/679 exerce les missions ci-après en ce qui concerne les activités de traitement relevant du champ d'application de la présente directive:

<table>
<tr>
<td>

a)

</td>
<td>

conseiller la Commission sur toute question relative à la protection des données à caractère personnel dans l'Union, notamment sur tout projet de modification de la présente directive;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

examiner, de sa propre initiative, à la demande de l'un de ses membres ou à la demande de la Commission, toute question portant sur l'application de la présente directive, et publier des lignes directrices, des recommandations et des bonnes pratiques afin de favoriser l'application cohérente de la présente directive;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

élaborer, à l'intention des autorités de contrôle, des lignes directrices concernant l'application des mesures visées à l'article 47, paragraphes 1 et 3;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

publier des lignes directrices, des recommandations et des bonnes pratiques conformément au point b) du présent alinéa, en vue d'établir les violations de données à caractère personnel et de déterminer les meilleurs délais visés à l'article 30, paragraphes 1 et 2, et de préciser les circonstances particulières dans lesquelles un responsable du traitement ou un sous-traitant est tenu de notifier la violation des données à caractère personnel;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

publier des lignes directrices, des recommandations et des bonnes pratiques conformément au point b) du présent alinéa concernant les circonstances dans lesquelles une violation de données à caractère personnel est susceptible d'engendrer un risque élevé pour les droits et libertés des personnes physiques, comme le prévoit l'article 31, paragraphe 1;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

faire le bilan de l'application pratique des lignes directrices, des recommandations et des bonnes pratiques visées aux points b) et c);

</td> </tr> </table>

<table>
<tr>
<td>

g)

</td>
<td>

rendre à la Commission un avis en ce qui concerne l'évaluation du caractère adéquat du niveau de protection assuré par un pays tiers, un territoire ou un ou plusieurs secteurs déterminés dans un pays tiers, ou une organisation internationale, y compris concernant l'évaluation visant à déterminer si ce pays tiers, ce territoire, ce secteur déterminé ou cette organisation internationale n'assure plus un niveau adéquat de protection;

</td> </tr> </table>

<table>
<tr>
<td>

h)

</td>
<td>

promouvoir la coopération et l'échange bilatéral et multilatéral effectif d'informations et de bonnes pratiques entre les autorités de contrôle;

</td> </tr> </table>

<table>
<tr>
<td>

i)

</td>
<td>

promouvoir l'élaboration de programmes de formation conjoints et faciliter les échanges de personnel entre autorités de contrôle, ainsi que, le cas échéant, avec les autorités de contrôle de pays tiers ou avec des organisations internationales;

</td> </tr> </table>

<table>
<tr>
<td>

j)

</td>
<td>

promouvoir l'échange, avec des autorités de contrôle de la protection des données de tous pays, de connaissances et de documentation sur le droit et les pratiques en matière de protection des données.

</td> </tr> </table>

En ce qui concerne le point g) du premier alinéa, la Commission fournit au comité tous les documents nécessaires, y compris la correspondance avec le gouvernement du pays tiers, le territoire ou le secteur déterminé dans ce pays tiers, ou avec l'organisation internationale.

2\. Lorsque la Commission demande conseil au comité, elle peut mentionner un délai, selon l'urgence de la question.

3\. Le comité transmet ses avis, lignes directrices, recommandations et bonnes pratiques à la Commission et au comité visé à l'article 58, paragraphe 1, et les publie.

4\. La Commission informe le comité des suites qu'elle a réservées aux avis, lignes directrices, recommandations et bonnes pratiques publiés par le comité.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-vii/article-50" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-viii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

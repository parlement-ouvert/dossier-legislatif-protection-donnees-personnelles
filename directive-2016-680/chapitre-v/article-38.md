<!-- TITLE: Article 38 -->
<!-- SUBTITLE: Dérogations pour des situations particulières -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-v/article-37" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-v/article-39" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. En l'absence de décision d'adéquation en vertu de l'article 36 ou de garanties appropriées en vertu de l'article 37, les États membres prévoient qu'un transfert ou une catégorie de transferts de données à caractère personnel vers un pays tiers ou à une organisation internationale ne peut avoir lieu qu'à condition que le transfert soit nécessaire:

<table>
<tr>
<td>

a)

</td>
<td>

à la sauvegarde des intérêts vitaux de la personne concernée ou d'une autre personne;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

à la sauvegarde des intérêts légitimes de la personne concernée lorsque le droit de l'État membre transférant les données à caractère personnel le prévoit;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

pour prévenir une menace grave et immédiate pour la sécurité publique d'un État membre ou d'un pays tiers;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

dans des cas particuliers, aux fins énoncées à l'article 1er, paragraphe 1; ou

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

dans un cas particulier, à la constatation, à l'exercice ou à la défense de droits en justice en rapport avec les fins énoncées à l'article 1er, paragraphe 1.

</td> </tr> </table>

2\. Les données à caractère personnel ne sont pas transférées si l'autorité compétente qui transfère les données estime que les libertés et droits fondamentaux de la personne concernée l'emportent sur l'intérêt public dans le cadre du transfert visé au paragraphe 1, points d) et e).

3\. Lorsqu'un transfert est effectué sur la base du paragraphe 1, point b), ce transfert est documenté et la documentation est mise à la disposition de l'autorité de contrôle, sur demande, et indique la date et l'heure du transfert, donne des informations sur l'autorité compétente destinataire, indique la justification du transfert et les données à caractère personnel transférées.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-v/article-37" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-v/article-39" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

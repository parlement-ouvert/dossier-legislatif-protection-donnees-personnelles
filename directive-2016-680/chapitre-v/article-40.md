<!-- TITLE: Article 40 -->
<!-- SUBTITLE: Coopération internationale dans le domaine de la protection des données à caractère personnel -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-v/article-39" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-vi" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

La Commission et les États membres prennent, à l'égard des pays tiers et des organisations internationales, les mesures appropriées pour:

<table>
<tr>
<td>

a)

</td>
<td>

élaborer des mécanismes de coopération internationaux destinés à faciliter l'application effective de la législation relative à la protection des données à caractère personnel;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

se prêter mutuellement assistance sur le plan international dans l'application de la législation relative à la protection des données à caractère personnel, notamment par la notification, la transmission des réclamations, l'entraide pour les enquêtes et l'échange d'informations, sous réserve de garanties appropriées pour la protection des données à caractère personnel et pour d'autres libertés et droits fondamentaux;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

associer les parties prenantes intéressées aux discussions et activités visant à développer la coopération internationale dans le domaine de l'application de la législation relative à la protection des données à caractère personnel;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

favoriser l'échange et la documentation de la législation et des pratiques en matière de protection des données à caractère personnel, y compris en ce qui concerne les conflits de compétence avec des pays tiers.

</td> </tr> </table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-v/article-39" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-vi" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

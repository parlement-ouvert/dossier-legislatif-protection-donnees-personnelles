<!-- TITLE: Article 47 -->
<!-- SUBTITLE: Pouvoirs -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-vi/section-2/article-46" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-vi/section-2/article-48" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Chaque État membre prévoit, par la loi, que chaque autorité de contrôle dispose de pouvoirs d'enquête effectifs. Ces pouvoirs comprennent au moins celui d'obtenir du responsable du traitement ou du sous-traitant l'accès à toutes les données à caractère personnel qui sont traitées et à toutes les informations nécessaires à l'exercice de ses missions.

2\. Chaque État membre prévoit, par la loi, que chaque autorité de contrôle dispose de pouvoirs effectifs en matière d'adoption de mesures correctrices, tels que, par exemple:

<table>
<tr>
<td>

a)

</td>
<td>

avertir un responsable du traitement ou un sous-traitant du fait que les opérations de traitement envisagées sont susceptibles de violer les dispositions adoptées en vertu de la présente directive;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

ordonner au responsable du traitement ou au sous-traitant de mettre les opérations de traitement en conformité avec les dispositions adoptées en vertu de la présente directive, le cas échéant, de manière spécifique et dans un délai déterminé, en particulier en ordonnant la rectification ou l'effacement de données à caractère personnel ou la limitation du traitement en application de l'article 16;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

limiter temporairement ou définitivement, y compris interdire, un traitement.

</td> </tr> </table>

3\. Chaque État membre prévoit, par la loi, que chaque autorité de contrôle dispose de pouvoirs consultatifs effectifs pour conseiller le responsable du traitement conformément à la procédure de consultation préalable visée à l'article 28 et d'émettre, de sa propre initiative ou sur demande, des avis à l'attention de son parlement national et de son gouvernement ou, conformément à son droit national, d'autres institutions et organismes ainsi que du public, sur toute question relative à la protection des données à caractère personnel.

4\. L'exercice des pouvoirs conférés à l'autorité de contrôle en application du présent article est subordonné à des garanties appropriées, y compris le droit à un recours juridictionnel effectif et à une procédure régulière, prévues par le droit de l'Union et le droit de l'État membre conformément à la Charte.

5\. Chaque État membre prévoit, par la loi, que chaque autorité de contrôle a le pouvoir de porter les violations des dispositions adoptées en vertu de la présente directive à la connaissance des autorités judiciaires et, le cas échéant, d'ester en justice d'une manière ou d'une autre, en vue de faire respecter les dispositions adoptées en vertu de la présente directive.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 6 — MESURES CORRECTRICES ET SANCTIONS](/etude-impact-490/titre-ier/chapitre-ier/article-6)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-vi/section-2/article-46" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-vi/section-2/article-48" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

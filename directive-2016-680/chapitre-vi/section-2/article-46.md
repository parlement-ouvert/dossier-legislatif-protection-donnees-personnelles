<!-- TITLE: Article 46 -->
<!-- SUBTITLE: Missions -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-vi/section-2/article-45" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-vi/section-2/article-47" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Chaque État membre prévoit que, sur son territoire, chaque autorité de contrôle:

<table>
<tr>
<td>

a)

</td>
<td>

contrôle l'application des dispositions adoptées en application de la présente directive et de ses mesures d'exécution et veille au respect de celles-ci;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

favorise la sensibilisation du public et sa compréhension des risques, des règles, des garanties et des droits relatifs au traitement;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

conseille, conformément au droit de l'État membre, le parlement national, le gouvernement et d'autres institutions et organismes au sujet des mesures législatives et administratives relatives à la protection des droits et libertés des personnes physiques à l'égard du traitement;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

encourage la sensibilisation des responsables du traitement et des sous-traitants aux obligations qui leur incombent en vertu de la présente directive;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

fournit, sur demande, à toute personne concernée, des informations sur l'exercice de ses droits découlant de la présente directive et, le cas échéant, coopère à cette fin avec les autorités de contrôle d'autres États membres;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

traite les réclamations introduites par une personne concernée ou par un organisme, une organisation ou une association conformément à l'article 55, enquête sur l'objet de la réclamation, dans la mesure nécessaire, et informe l'auteur de la réclamation de l'état d'avancement et de l'issue de l'enquête dans un délai raisonnable, notamment si un complément d'enquête ou une coordination avec une autre autorité de contrôle est nécessaire;

</td> </tr> </table>

<table>
<tr>
<td>

g)

</td>
<td>

vérifie la licéité du traitement en vertu de l'article 17, et informe la personne concernée dans un délai raisonnable de l'issue de la vérification, conformément au paragraphe 3 dudit article, ou des motifs ayant empêché sa réalisation;

</td> </tr> </table>

<table>
<tr>
<td>

h)

</td>
<td>

coopère avec d'autres autorités de contrôle, y compris en partageant des informations, et leur fournit une assistance mutuelle dans ce cadre en vue d'assurer une application cohérente de la présente directive et des mesures prises pour en assurer le respect;

</td> </tr> </table>

<table>
<tr>
<td>

i)

</td>
<td>

effectue des enquêtes sur l'application de la présente directive, y compris sur la base d'informations reçues d'une autre autorité de contrôle ou d'une autre autorité publique;

</td> </tr> </table>

<table>
<tr>
<td>

j)

</td>
<td>

suit les évolutions pertinentes, dans la mesure où elles ont une incidence sur la protection des données à caractère personnel, notamment dans le domaine des technologies de l'information et de la communication;

</td> </tr> </table>

<table>
<tr>
<td>

k)

</td>
<td>

fournit des conseils sur les opérations de traitement visées à l'article 28; et

</td> </tr> </table>

<table>
<tr>
<td>

l)

</td>
<td>

contribue aux activités du comité.

</td> </tr> </table>

2\. Chaque autorité de contrôle facilite l'introduction des réclamations visées au paragraphe 1, point f), par des mesures telles que la fourniture d'un formulaire de réclamation qui peut être rempli également par voie électronique, sans que d'autres moyens de communication ne soient exclus.

3\. L'accomplissement des missions de chaque autorité de contrôle est gratuit pour la personne concernée et pour le délégué à la protection des données.

4\. Lorsqu'une demande est manifestement infondée ou excessive, en raison, notamment, de son caractère répétitif, l'autorité de contrôle peut exiger le paiement de frais raisonnables basés sur ses coûts administratifs ou refuser de donner suite à la demande. Il incombe à l'autorité de contrôle de démontrer le caractère manifestement infondé ou excessif de la demande.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 1ER — MISSIONS DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier/article-1er)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-vi/section-2/article-45" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-vi/section-2/article-47" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

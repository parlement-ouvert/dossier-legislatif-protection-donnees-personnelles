<!-- TITLE: Article 10 -->
<!-- SUBTITLE: Traitement portant sur des catégories particulières de données à caractère personnel -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-ii/article-9" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-ii/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Le traitement des données à caractère personnel qui révèlent l'origine raciale ou ethnique, les opinions politiques, les convictions religieuses ou philosophiques, ou l'appartenance syndicale, et le traitement des données génétiques, des données biométriques aux fins d'identifier une personne physique de manière unique, des données concernant la santé ou des données concernant la vie sexuelle ou l'orientation sexuelle d'une personne physique est autorisé uniquement en cas de nécessité absolue, sous réserve de garanties appropriées pour les droits et libertés de la personne concernée, et uniquement:

<table>
<tr>
<td>

a)

</td>
<td>

lorsqu'ils sont autorisés par le droit de l'Union ou le droit d'un État membre;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

pour protéger les intérêts vitaux de la personne concernée ou d'une autre personne physique; ou

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

lorsque le traitement porte sur des données manifestement rendues publiques par la personne concernée.

</td> </tr> </table>

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 7 — DONNÉES SENSIBLES](/etude-impact-490/titre-ier/chapitre-ii/article-7)
  * [ARTICLE 19 SECTION 1 — DISPOSITIONS GENERALES](/etude-impact-490/titre-iii/article-19-section-1)
  * [ARTICLE 18 ET ARTICLE 19 SECTION 3 — DROITS DE LA PERSONNE CONCERNÉE](/etude-impact-490/titre-iii/article-18-et-article-19-section-3)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-ii/article-9" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-ii/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

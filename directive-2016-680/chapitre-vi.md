<!-- TITLE: CHAPITRE VI -->
<!-- SUBTITLE: Autorités de contrôle indépendantes -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-v/article-40" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-vi/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Section 1 — Statut d'indépendance](/directive-2016-680/chapitre-vi/section-1)

  * [Article 41 — Autorité de contrôle](/directive-2016-680/chapitre-vi/section-1/article-41)
  * [Article 42 — Indépendance](/directive-2016-680/chapitre-vi/section-1/article-42)
  * [Article 43 — Conditions générales applicables aux membres de l'autorité de contrôle](/directive-2016-680/chapitre-vi/section-1/article-43)
  * [Article 44 — Règles relatives à l'établissement de l'autorité de contrôle](/directive-2016-680/chapitre-vi/section-1/article-44)

* [Section 2 — Compétence, missions et pouvoirs](/directive-2016-680/chapitre-vi/section-2)

  * [Article 45 — Compétence](/directive-2016-680/chapitre-vi/section-2/article-45)
  * [Article 46 — Missions](/directive-2016-680/chapitre-vi/section-2/article-46)
  * [Article 47 — Pouvoirs](/directive-2016-680/chapitre-vi/section-2/article-47)
  * [Article 48 — Signalement des violations](/directive-2016-680/chapitre-vi/section-2/article-48)
  * [Article 49 — Rapports d'activité](/directive-2016-680/chapitre-vi/section-2/article-49)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-v/article-40" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-vi/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

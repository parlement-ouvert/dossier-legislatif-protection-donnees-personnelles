<!-- TITLE: CHAPITRE IV -->
<!-- SUBTITLE: Responsable du traitement et sous-traitant -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iii/article-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iv/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Section 1 — Obligations générales](/directive-2016-680/chapitre-iv/section-1)

  * [Article 19 — Obligations incombant au responsable du traitement](/directive-2016-680/chapitre-iv/section-1/article-19)
  * [Article 20 — Protection des données dès la conception et protection des données par défaut](/directive-2016-680/chapitre-iv/section-1/article-20)
  * [Article 21 — Responsables conjoints du traitement](/directive-2016-680/chapitre-iv/section-1/article-21)
  * [Article 22 — Sous-traitant](/directive-2016-680/chapitre-iv/section-1/article-22)
  * [Article 23 — Traitement effectué sous l'autorité du responsable du traitement ou du sous-traitant](/directive-2016-680/chapitre-iv/section-1/article-23)
  * [Article 24 — Registre des activités de traitement](/directive-2016-680/chapitre-iv/section-1/article-24)
  * [Article 25 — Journalisation](/directive-2016-680/chapitre-iv/section-1/article-25)
  * [Article 26 — Coopération avec l'autorité de contrôle](/directive-2016-680/chapitre-iv/section-1/article-26)
  * [Article 27 — Analyse d'impact relative à la protection des données](/directive-2016-680/chapitre-iv/section-1/article-27)
  * [Article 28 — Consultation préalable de l'autorité de contrôle](/directive-2016-680/chapitre-iv/section-1/article-28)

* [Section 2 — Sécurité des données](/directive-2016-680/chapitre-iv/section-2)

  * [Article 29 — Sécurité du traitement](/directive-2016-680/chapitre-iv/section-2/article-29)
  * [Article 30 — Notification à l'autorité de contrôle d'une violation de données à caractère personnel](/directive-2016-680/chapitre-iv/section-2/article-30)
  * [Article 31 — Communication à la personne concernée d'une violation de données à caractère personnel](/directive-2016-680/chapitre-iv/section-2/article-31)

* [Section 3 — Délégué à la protection des données](/directive-2016-680/chapitre-iv/section-3)

  * [Article 32 — Désignation du délégué à la protection des données](/directive-2016-680/chapitre-iv/section-3/article-32)
  * [Article 33 — Fonction du délégué à la protection des données](/directive-2016-680/chapitre-iv/section-3/article-33)
  * [Article 34 — Missions du délégué à la protection des données](/directive-2016-680/chapitre-iv/section-3/article-34)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLES 18 ET 19 — PRESENTATION GENERALE ET DÉFINITIONS](/etude-impact-490/titre-iii/articles-18-et-19)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iii/article-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iv/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 33 -->
<!-- SUBTITLE: Fonction du délégué à la protection des données -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iv/section-3/article-32" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iv/section-3/article-34" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Les États membres prévoient que le responsable du traitement veille à ce que le délégué à la protection des données soit associé, d'une manière appropriée et en temps utile, à toutes les questions relatives à la protection des données à caractère personnel.

2\. Le responsable du traitement aide le délégué à la protection des données à exercer les missions visées à l'article 34 en fournissant les ressources nécessaires pour exercer ces missions ainsi que l'accès aux données à caractère personnel et aux traitements, et lui permettant d'entretenir ses connaissances spécialisées.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 19 SECTION 2 — OBLIGATIONS INCOMBANT AUX AUTORITES COMPETENTES ET AUX RESPONSABLES DE TRAITEMENT](/etude-impact-490/titre-iii/article-19-section-2)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iv/section-3/article-32" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iv/section-3/article-34" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

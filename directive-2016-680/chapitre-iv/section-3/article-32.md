<!-- TITLE: Article 32 -->
<!-- SUBTITLE: Désignation du délégué à la protection des données -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iv/section-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iv/section-3/article-33" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Les États membres prévoient que le responsable du traitement désigne un délégué à la protection des données. Les États membres peuvent dispenser les tribunaux et d'autres autorités judiciaires indépendantes de cette obligation lorsqu'elles agissent dans l'exercice de leur fonction juridictionnelle.

2\. Le délégué à la protection des données est désigné sur la base de ses qualités professionnelles et, en particulier, de ses connaissances spécialisées du droit et des pratiques en matière de protection des données, et de sa capacité à exercer les missions visées à l'article 34.

3\. Un seul délégué à la protection des données peut être désigné pour plusieurs autorités compétentes, compte tenu de leur structure organisationnelle et de leur taille.

4\. Les États membres prévoient que le responsable du traitement publie les coordonnées du délégué à la protection des données et les communique à l'autorité de contrôle.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [25.](/avis-conseil-etat-393836/delegue-a-la-protection-des-donnees/25)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 19 SECTION 2 — OBLIGATIONS INCOMBANT AUX AUTORITES COMPETENTES ET AUX RESPONSABLES DE TRAITEMENT](/etude-impact-490/titre-iii/article-19-section-2)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iv/section-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iv/section-3/article-33" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 29 -->
<!-- SUBTITLE: Sécurité du traitement -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iv/section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iv/section-2/article-30" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Les États membres prévoient que, compte tenu de l'état des connaissances, des coûts de la mise en œuvre et de la nature, de la portée, du contexte et des finalités du traitement ainsi que des risques, dont le degré de probabilité et de gravité varie, pour les droits et libertés des personnes physiques, le responsable du traitement et le sous-traitant mettent en œuvre les mesures techniques et organisationnelles appropriées afin de garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l'article 10.

2\. En ce qui concerne le traitement automatisé, chaque État membre prévoit que le responsable du traitement ou le sous-traitant met en œuvre, à la suite d'une évaluation des risques, des mesures destinées à:

<table>
<tr>
<td>

a)

</td>
<td>

empêcher toute personne non autorisée d'accéder aux installations utilisées pour le traitement (contrôle de l'accès aux installations);

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

empêcher que des supports de données puissent être lus, copiés, modifiés ou supprimés de façon non autorisée (contrôle des supports de données);

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

empêcher l'introduction non autorisée de données à caractère personnel dans le fichier, ainsi que l'inspection, la modification ou l'effacement non autorisé de données à caractère personnel enregistrées (contrôle de la conservation);

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

empêcher que les systèmes de traitement automatisé puissent être utilisés par des personnes non autorisées à l'aide d'installations de transmission de données (contrôle des utilisateurs);

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

garantir que les personnes autorisées à utiliser un système de traitement automatisé ne puissent accéder qu'aux données à caractère personnel sur lesquelles porte leur autorisation (contrôle de l'accès aux données);

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

garantir qu'il puisse être vérifié et constaté à quelles instances des données à caractère personnel ont été ou peuvent être transmises ou mises à disposition par des installations de transmission de données (contrôle de la transmission);

</td> </tr> </table>

<table>
<tr>
<td>

g)

</td>
<td>

garantir qu'il puisse être vérifié et constaté a posteriori quelles données à caractère personnel ont été introduites dans les systèmes de traitement automatisé, et à quel moment et par quelle personne elles y ont été introduites (contrôle de l'introduction);

</td> </tr> </table>

<table>
<tr>
<td>

h)

</td>
<td>

empêcher que, lors de la transmission de données à caractère personnel ainsi que lors du transport de supports de données, les données puissent être lues, copiées, modifiées ou supprimées de façon non autorisée (contrôle du transport);

</td> </tr> </table>

<table>
<tr>
<td>

i)

</td>
<td>

garantir que les systèmes installés puissent être rétablis en cas d'interruption (restauration);

</td> </tr> </table>

<table>
<tr>
<td>

j)

</td>
<td>

garantir que les fonctions du système opèrent, que les erreurs de fonctionnement soient signalées (fiabilité) et que les données à caractère personnel conservées ne puissent pas être corrompues par un dysfonctionnement du système (intégrité).

</td> </tr> </table>

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 19 SECTION 2 — OBLIGATIONS INCOMBANT AUX AUTORITES COMPETENTES ET AUX RESPONSABLES DE TRAITEMENT](/etude-impact-490/titre-iii/article-19-section-2)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iv/section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iv/section-2/article-30" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 24 -->
<!-- SUBTITLE: Registre des activités de traitement -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iv/section-1/article-23" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iv/section-1/article-25" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Les États membres prévoient que les responsables du traitement tiennent un registre de toutes les catégories d'activités de traitement effectuées sous leur responsabilité. Ce registre comporte toutes les informations suivantes:

<table>
<tr>
<td>

a)

</td>
<td>

le nom et les coordonnées du responsable du traitement et, le cas échéant, du responsable conjoint du traitement et du délégué à la protection des données;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

les finalités du traitement;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

les catégories de destinataires auxquels les données à caractère personnel ont été ou seront communiquées, y compris les destinataires dans des pays tiers ou des organisations internationales;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

une description des catégories de personnes concernées et des catégories de données à caractère personnel;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

le cas échéant, le recours au profilage;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

le cas échéant, les catégories de transferts de données à caractère personnel vers un pays tiers ou à une organisation internationale;

</td> </tr> </table>

<table>
<tr>
<td>

g)

</td>
<td>

une indication de la base juridique de l'opération de traitement, y compris les transferts, à laquelle les données à caractère personnel sont destinées;

</td> </tr> </table>

<table>
<tr>
<td>

h)

</td>
<td>

dans la mesure du possible, les délais prévus pour l'effacement des différentes catégories de données à caractère personnel;

</td> </tr> </table>

<table>
<tr>
<td>

i)

</td>
<td>

dans la mesure du possible, une description générale des mesures de sécurité techniques et organisationnelles visées à l'article 29, paragraphe 1.

</td> </tr> </table>

2\. Les États membres prévoient que chaque sous-traitant tient un registre de toutes les catégories d'activités de traitement effectuées pour le compte du responsable du traitement, comprenant:

<table>
<tr>
<td>

a)

</td>
<td>

le nom et les coordonnées du ou des sous-traitants, de chaque responsable du traitement pour le compte duquel le sous-traitant agit et, le cas échéant, du délégué à la protection des données;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

les catégories de traitements effectués pour le compte de chaque responsable du traitement;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

le cas échéant, les transferts de données à caractère personnel vers un pays tiers ou à une organisation internationale, lorsqu'il en est expressément chargé par le responsable du traitement, y compris l'identification de ce pays tiers ou de cette organisation internationale;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

dans la mesure du possible, une description générale des mesures de sécurité techniques et organisationnelles visées à l'article 29, paragraphe 1.

</td> </tr> </table>

3\. Les registres visés aux paragraphes 1 et 2 se présentent sous une forme écrite, y compris la forme électronique.

Le responsable du traitement et le sous-traitant mettent ces registres à la disposition de l'autorité de contrôle, sur demande.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 19 SECTION 2 — OBLIGATIONS INCOMBANT AUX AUTORITES COMPETENTES ET AUX RESPONSABLES DE TRAITEMENT](/etude-impact-490/titre-iii/article-19-section-2)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iv/section-1/article-23" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iv/section-1/article-25" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

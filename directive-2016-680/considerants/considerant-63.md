<!-- TITLE: Considérant 63 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/considerants/considerant-62" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/considerants/considerant-64" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Le responsable du traitement devrait désigner une personne qui l'aiderait à vérifier le respect, au niveau interne, des dispositions adoptées en vertu de la présente directive, sauf lorsqu'un État membre décide que des tribunaux et d'autres autorités judiciaires indépendantes en sont dispensés dans l'exercice de leur fonction juridictionnelle. Cette personne pourrait être un membre du personnel du responsable du traitement ayant reçu une formation spéciale dans le domaine du droit et des pratiques en matière de protection des données afin d'acquérir des connaissances spécialisées dans ce domaine. Le niveau de connaissances spécialisées requis devrait être déterminé notamment en fonction du traitement des données effectué et de la protection exigée pour les données à caractère personnel traitées par le responsable du traitement. Cette personne pourrait exercer cette fonction à temps plein ou à temps partiel. Un délégué à la protection des données peut être désigné conjointement par plusieurs responsables du traitement, compte tenu de leur structure organisationnelle et de leur taille, par exemple en cas de partage des ressources au sein d'unités centrales. Cette personne peut également être désignée pour occuper différents postes au sein de la structure des responsables du traitement concernés. Elle devrait aider le responsable du traitement et les employés traitant des données à caractère personnel en les informant et en les conseillant sur le respect des obligations leur incombant en matière de protection des données. Ces délégués à la protection des données devraient être en mesure d'exercer leurs fonctions et missions en toute indépendance conformément au droit de l'État membre.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 19 SECTION 2 — OBLIGATIONS INCOMBANT AUX AUTORITES COMPETENTES ET AUX RESPONSABLES DE TRAITEMENT](/etude-impact-490/titre-iii/article-19-section-2)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/considerants/considerant-62" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/considerants/considerant-64" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

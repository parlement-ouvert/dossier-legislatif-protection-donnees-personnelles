<!-- TITLE: Considérant 96 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/considerants/considerant-95" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/considerants/considerant-97" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Les États membres devraient disposer d'un délai maximal de deux ans à compter de la date d'entrée en vigueur de la présente directive pour sa transposition. Les traitements déjà en cours à cette date devraient être mis en conformité avec la présente directive dans un délai de deux ans après son entrée en vigueur. Toutefois, lorsque ces traitements ont lieu en conformité avec le droit de l'Union applicable avant la date d'entrée en vigueur de la présente directive, les exigences prévues par celle-ci concernant la consultation préalable de l'autorité de contrôle ne devraient pas s'appliquer aux opérations de traitement déjà en cours à ladite date, étant donné que ces exigences, de par leur nature même, doivent être satisfaites avant le traitement. Lorsque les États membres recourent au délai de mise en œuvre plus long, venant à expiration sept ans après la date d'entrée en vigueur de la présente directive, pour se conformer aux obligations en matière de journalisation pour les systèmes de traitement automatisé mis en place avant cette date, le responsable du traitement ou le sous-traitant devrait s'être doté des moyens effectifs de démontrer la licéité du traitement des données, de pratiquer l'autocontrôle et de garantir l'intégrité et la sécurité des données, tels que des journaux ou d'autres formes de registres.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLES 24](/etude-impact-490/titre-v/articles-24)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/considerants/considerant-95" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/considerants/considerant-97" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

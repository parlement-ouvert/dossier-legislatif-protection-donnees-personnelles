<!-- TITLE: Article 16 -->
<!-- SUBTITLE: Droit de rectification ou d'effacement des données à caractère personnel et limitation du traitement -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iii/article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iii/article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Les États membres prévoient le droit pour la personne concernée d'obtenir du responsable du traitement, dans les meilleurs délais, la rectification des données à caractère personnel la concernant qui sont inexactes. Compte tenu des finalités du traitement, les États membres prévoient que la personne concernée a le droit d'obtenir que les données à caractère personnel incomplètes soient complétées, y compris en fournissant à cet effet une déclaration complémentaire.

2\. Les États membres exigent que le responsable du traitement efface dans les meilleurs délais les données à caractère personnel et accordent à la personne concernée le droit d'obtenir du responsable du traitement l'effacement dans les meilleurs délais de données à caractère personnel la concernant lorsque le traitement constitue une violation des dispositions adoptées en vertu de l'article 4, 8 ou 10 ou lorsque les données à caractère personnel doivent être effacées pour respecter une obligation légale à laquelle est soumis le responsable du traitement.

3\. Au lieu de procéder à l'effacement, le responsable du traitement limite le traitement lorsque:

<table>
<tr>
<td>

a)

</td>
<td>

l'exactitude des données à caractère personnel est contestée par la personne concernée et qu'il ne peut être déterminé si les données sont exactes ou non; ou

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

les données à caractère personnel doivent être conservées à des fins probatoires.

</td> </tr> </table>

Lorsque le traitement est limité en vertu du premier alinéa, point a), le responsable du traitement informe la personne concernée avant de lever la limitation du traitement.

4\. Les États membres prévoient que le responsable du traitement informe la personne concernée par écrit de tout refus de rectifier ou d'effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs du refus. Les États membres peuvent adopter des mesures législatives limitant, en tout ou partie, l'obligation de fournir ces informations, dès lors qu'une telle limitation constitue une mesure nécessaire et proportionnée dans une société démocratique en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne physique concernée pour:

<table>
<tr>
<td>

a)

</td>
<td>

éviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

éviter de nuire à la prévention ou à la détection d'infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l'exécution de sanctions pénales;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

protéger la sécurité publique;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

protéger la sécurité nationale;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

protéger les droits et libertés d'autrui.

</td> </tr> </table>

Les États membres prévoient que le responsable du traitement informe la personne concernée des possibilités d'introduire une réclamation auprès d'une autorité de contrôle ou de former un recours juridictionnel.

5\. Les États membres prévoient que le responsable du traitement communique la rectification des données à caractère personnel inexactes à l'autorité compétente dont proviennent les données à caractère personnel inexactes.

6\. Les États membres prévoient que, lorsque des données à caractère personnel ont été rectifiées ou effacées ou que le traitement a été limité au titre des paragraphes 1, 2 et 3, le responsable du traitement adresse une notification aux destinataires et que ceux-ci rectifient ou effacent les données à caractère personnel ou limitent le traitement des données à caractère personnel sous leur responsabilité.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 18 ET ARTICLE 19 SECTION 3 — DROITS DE LA PERSONNE CONCERNÉE](/etude-impact-490/titre-iii/article-18-et-article-19-section-3)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iii/article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iii/article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

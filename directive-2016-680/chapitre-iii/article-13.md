<!-- TITLE: Article 13 -->
<!-- SUBTITLE: Informations à mettre à la disposition de la personne concernée ou à lui fournir -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iii/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iii/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Les États membres prévoient que le responsable du traitement met à la disposition de la personne concernée au moins les informations suivantes:

<table>
<tr>
<td>

a)

</td>
<td>

l'identité et les coordonnées du responsable du traitement;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

le cas échéant, les coordonnées du délégué à la protection des données;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

les finalités du traitement auquel sont destinées les données à caractère personnel;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

le droit d'introduire une réclamation auprès d'une autorité de contrôle et les coordonnées de ladite autorité;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

l'existence du droit de demander au responsable du traitement l'accès aux données à caractère personnel, leur rectification ou leur effacement, et la limitation du traitement des données à caractère personnel relatives à une personne concernée.

</td> </tr> </table>

2\. En plus des informations visées au paragraphe 1, les États membres prévoient, par la loi, que le responsable du traitement fournit à la personne concernée, dans des cas particuliers, les informations additionnelles suivantes afin de lui permettre d'exercer ses droits:

<table>
<tr>
<td>

a)

</td>
<td>

la base juridique du traitement,

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

la durée de conservation des données à caractère personnel ou, lorsque ce n'est pas possible, les critères utilisés pour déterminer cette durée;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

le cas échéant, les catégories de destinataires des données à caractère personnel, y compris dans les pays tiers ou au sein d'organisations internationales;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

au besoin, des informations complémentaires, en particulier lorsque les données à caractère personnel sont collectées à l'insu de la personne concernée.

</td> </tr> </table>

3\. Les États membres peuvent adopter des mesures législatives visant à retarder ou limiter la fourniture des informations à la personne concernée en application du paragraphe 2, ou à ne pas fournir ces informations, dès lors et aussi longtemps qu'une mesure de cette nature constitue une mesure nécessaire et proportionnée dans une société démocratique, en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne physique concernée pour:

<table>
<tr>
<td>

a)

</td>
<td>

éviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

éviter de nuire à la prévention ou à la détection d'infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l'exécution de sanctions pénales;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

protéger la sécurité publique;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

protéger la sécurité nationale;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

protéger les droits et libertés d'autrui.

</td> </tr> </table>

4\. Les États membres peuvent adopter des mesures législatives afin de déterminer des catégories de traitements susceptibles de relever, dans leur intégralité ou en partie, d'un quelconque des points énumérés au paragraphe 3.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 18 ET ARTICLE 19 SECTION 3 — DROITS DE LA PERSONNE CONCERNÉE](/etude-impact-490/titre-iii/article-18-et-article-19-section-3)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/directive-2016-680/chapitre-iii/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Directive européenne</span> <a href="/directive-2016-680/chapitre-iii/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 11 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-iii/article-12-abroge" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2017-55 du 20 janvier 2017 - art. 40](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033897475&idArticle=LEGIARTI000033898735&dateTexte=20170121&categorieLien=id#LEGIARTI000033898735)

----

<ins style="background-color: #F8EDA8;">I - </ins>La Commission nationale de l'informatique et des libertés est une autorité administrative indépendante. <ins style="background-color: #F8EDA8;">Elle est l’autorité de contrôle nationale au sens et pour l’application du [règlement (UE) 2016/679](/reglement-2016-679).</ins> Elle exerce les missions suivantes :

1° Elle informe toutes les personnes concernées et tous les responsables de traitements de leurs droits et obligations ;

2° Elle veille à ce que les traitements de données à caractère personnel soient mis en oeuvre conformément aux dispositions de la présente loi.

A ce titre :

a) <del style="background-color: #FFAEAE;">Elle autorise les traitements mentionnés à l'article 25,</del> Elle donne un avis sur les traitements mentionnés aux articles [26](/loi-78-17-pjl-490/chapitre-iv/section-2/article-26) et [27](/loi-78-17-pjl-490/chapitre-iv/section-2/article-27) <del style="background-color: #FFAEAE;">et reçoit les déclarations relatives aux autres traitements</del> ;

<ins style="background-color: #F8EDA8;"> a bis) Elle établit et publie des lignes directrices, recommandations ou référentiels destinés à faciliter la mise en conformité des traitements de données à caractère personnel avec les textes relatifs à la protection des données à caractère personnel et à procéder à l’évaluation préalable des risques par les responsables de traitement et leurs sous-traitants. Elle encourage l’élaboration de codes de conduite définissant les obligations qui incombent aux responsables du traitement et aux sous-traitants, compte tenu du risque inhérent aux traitements de données à caractère personnel pour les droits et libertés des personnes physiques ; elle homologue et publie les méthodologies de référence mentionnées au IV de l’[article 54](/loi-78-17-pjl-490/chapitre-ix/section-1/article-54), destinées à favoriser la conformité des traitement de données de santé à caractère personnel ;</ins>

b) <del style="background-color: #FFAEAE;">b) Elle établit et publie les normes mentionnées au I de l'article 24 et édicte, le cas échéant, des règlements types en vue d'assurer la sécurité des systèmes ;</del> <ins style="background-color: #F8EDA8;">Elle établit et publie des règlements types en vue d’assurer la sécurité des systèmes de traitement de données à caractère personnel et de régir les traitements de données de santé relevant du [chapitre IX](/loi-78-17-pjl-490/chapitre-ix). À ce titre, sauf pour les traitements mis en œuvre pour le compte de l’État, agissant dans l’exercice de ses prérogatives de puissance publique, elle peut prescrire des mesures techniques et organisationnelles supplémentaires pour le traitement des données biométriques, génétiques et de santé conformément à l’[article 9.4 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-ii/article-9) et des garanties complémentaires en matière de traitement de données d’infraction conformément à l’[article 10 du même règlement](/reglement-2016-679/chapitre-ii/article-10). ;</ins>

c) Elle reçoit les réclamations, pétitions et plaintes relatives à la mise en oeuvre des traitements de données à caractère personnel et informe leurs auteurs des suites données à celles-ci ;

d) Elle répond aux demandes d'avis des pouvoirs publics et, le cas échéant, des juridictions, et conseille les personnes et organismes qui mettent en oeuvre ou envisagent de mettre en oeuvre des traitements automatisés de données à caractère personnel ;

e) Elle informe sans délai le procureur de la République, conformément à l'[article 40 du code de procédure pénale](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000006574931&dateTexte=&categorieLien=cid), des infractions dont elle a connaissance, et peut présenter des observations dans les procédures pénales, dans les conditions prévues à l'[article 52](/loi-78-17-pjl-490/chapitre-viii/article-52) ;

f) Elle peut, par décision particulière, charger un ou plusieurs de ses membres ou le secrétaire général, dans les conditions prévues à l'article 44, de procéder ou de faire procéder par les agents de ses services à des vérifications portant sur tous traitements et, le cas échéant, d'obtenir des copies de tous documents ou supports d'information utiles à ses missions ;

<ins style="background-color: #F8EDA8;">f bis) Elle peut décider de certifier des personnes, des produits, des systèmes de données ou des procédures aux fins de reconnaître qu’ils se conforment au règlement (UE) 2016/679 et la présente loi. Elle agrée, aux mêmes fins, des organismes certificateurs, sur la base, le cas échéant, de leur accréditation par l’instance nationale d’accréditation, mentionnée à l’article 43(1) b du règlement, dans des conditions précisées par décret en Conseil d’État pris après avis de la Commission nationale de l’informatique et des libertés. La commission élabore ou approuve les critères des référentiels de certification et d’agrément. Elle peut établir des exigences supplémentaires aux normes d’accréditation. ;</ins>

g) Elle peut certifier ou homologuer et publier des référentiels ou des méthodologies générales aux fins de certification<ins style="background-color: #F8EDA8;">, par des tiers agréés ou accrédités selon les modalités mentionnées au f bis,</ins> de la conformité à la présente loi de processus d'anonymisation des données à caractère personnel, notamment en vue de la réutilisation d'informations publiques mises en ligne dans les conditions prévues au titre II du livre III du code des relations entre le public et l'administration.

Il en est tenu compte, le cas échéant, pour la mise en œuvre des sanctions prévues au [chapitre VII](/loi-78-17-pjl-490/chapitre-vii) de la présente loi.

h) Elle répond aux demandes <del style="background-color: #FFAEAE;">d'accès concernant les traitements mentionnés aux articles 41 et 42</del> <ins style="background-color: #F8EDA8;"> d’exercice des droits prévues aux articles [41](/loi-78-17-pjl-490/chapitre-v/section-2/article-41), [42](/loi-78-17-pjl-490/chapitre-v/section-2/article-42) et [70-22](/loi-78-17-pjl-490/chapitre-xiii/section-3/article-70-22)</ins> ;

<ins style="background-color: #F8EDA8;">i) Elle peut établir une liste des traitements susceptibles de créer un risque élevé devant faire l’objet d’une consultation préalable conformément à l’[article 70-4](/loi-78-17-pjl-490/chapitre-xiii/section-1/article-70-4) » ;</ins>

3° A la demande d'organisations professionnelles ou d'institutions regroupant principalement des responsables de traitements :

a) Elle donne un avis sur la conformité aux dispositions de la présente loi des projets de règles professionnelles et des produits et procédures tendant à la protection des personnes à l'égard du traitement de données à caractère personnel, ou à l'anonymisation de ces données, qui lui sont soumis ;

b) Elle porte une appréciation sur les garanties offertes par des règles professionnelles qu'elle a précédemment reconnues conformes aux dispositions de la présente loi, au regard du respect des droits fondamentaux des personnes ;

c) Elle délivre un label à des produits ou à des procédures tendant à la protection des personnes à l'égard du traitement des données à caractère personnel, après qu'elle les a reconnus conformes aux dispositions de la présente loi dans le cadre de l'instruction préalable à la délivrance du label par la commission ; la commission peut également déterminer, de sa propre initiative, les produits et procédures susceptibles de bénéficier d'un label . Le président peut, lorsque la complexité du produit ou de la procédure le justifie, recourir à toute personne indépendante qualifiée pour procéder à leur évaluation. Le coût de cette évaluation est pris en charge par l'entreprise qui demande le label ; elle retire le label lorsqu'elle constate, par tout moyen, que les conditions qui ont permis sa délivrance ne sont plus satisfaites ;

4° Elle se tient informée de l'évolution des technologies de l'information et rend publique le cas échéant son appréciation des conséquences qui en résultent pour l'exercice des droits et libertés mentionnés à l'[article 1er](/loi-78-17-pjl-490/chapitre-ier/article-1) ;

A ce titre :

a) Elle est consultée sur tout projet de loi ou de décret ou toute disposition de projet de loi ou de décret relatifs à la protection des données à caractère personnel ou au traitement de telles données. <ins style="background-color: #F8EDA8;"> Elle peut également être consultée par le président de l’Assemblée nationale ou par le président du Sénat sur toute proposition de loi relative à la protection des données à caractère personnel ou au traitement de telles données.</ins> Outre les cas prévus aux articles [26](/loi-78-17-pjl-490/chapitre-iv/section-2/article-26) et [27](/loi-78-17-pjl-490/chapitre-iv/section-2/article-27), lorsqu'une loi prévoit qu'un décret ou un arrêté est pris après avis de la commission, cet avis est publié avec le décret ou l'arrêté ;

b) Elle propose au Gouvernement les mesures législatives ou réglementaires d'adaptation de la protection des libertés à l'évolution des procédés et techniques informatiques ;

c) A la demande d'autres autorités administratives indépendantes, elle peut apporter son concours en matière de protection des données ;

d) Elle peut être associée, à la demande du Premier ministre, à la préparation et à la définition de la position française dans les négociations internationales dans le domaine de la protection des données à caractère personnel. Elle peut participer, à la demande du Premier ministre, à la représentation française dans les organisations internationales et communautaires compétentes en ce domaine ;

e) Elle conduit une réflexion sur les problèmes éthiques et les questions de société soulevés par l'évolution des technologies numériques ;

f) Elle promeut, dans le cadre de ses missions, l'utilisation des technologies protectrices de la vie privée, notamment les technologies de chiffrement des données.

<ins style="background-color: #F8EDA8;">5° Elle peut présenter des observations devant toute juridiction à l’occasion d’un litige relatif à l’application du règlement (UE) 2016/679 et de la présente loi ;</ins>

<ins style="background-color: #F8EDA8;"> II. -</ins> Pour l'accomplissement de ses missions, la commission peut procéder par voie de recommandation et prendre des décisions individuelles ou réglementaires dans les cas prévus par la présente loi.

La commission peut saisir pour avis l'Autorité de régulation des communications électroniques et des postes de toute question relevant de la compétence de celle-ci.

La commission présente chaque année au Président de la République et au Premier ministre un rapport public rendant compte de l'exécution de sa mission.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [Article 17](/loi-78-17-pjl-490/chapitre-iii/article-17)
  * [Article 54](/loi-78-17-pjl-490/chapitre-ix/section-1/article-54)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-iii/article-12-abroge" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

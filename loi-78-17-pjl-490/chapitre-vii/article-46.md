<!-- TITLE: Article 46 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-vii/article-45" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-vii/article-47" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 64](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205210&dateTexte=20161008&categorieLien=id#LEGIARTI000033205210)

----

<del style="background-color: #FFAEAE;">Les sanctions prévues au I et au 1° du II de l'article 45 sont prononcées sur la base d'un rapport établi par l'un des membres de la Commission nationale de l'informatique et des libertés, désigné par le président de celle-ci parmi les membres n'appartenant pas à la formation restreinte. Ce rapport est notifié au responsable du traitement, qui peut déposer des observations et se faire représenter ou assister. Le rapporteur peut présenter des observations orales à la formation restreinte mais ne prend pas part à ses délibérations. La formation restreinte peut entendre toute personne dont l'audition lui paraît susceptible de contribuer utilement à son information, y compris, à la demande du secrétaire général, les agents des services.</del>

<del style="background-color: #FFAEAE;">La formation restreinte peut rendre publiques les sanctions qu'elle prononce. Elle peut ordonner que les personnes sanctionnées informent individuellement de cette sanction, à leur frais, chacune des personnes concernées. Elle peut également ordonner leur insertion dans des publications, journaux et supports qu'elle désigne aux frais des personnes sanctionnées. Le président de la commission peut demander au bureau de rendre publique la mise en demeure prévue au deuxième alinéa du I de l'article 45. Lorsque le président de la commission prononce la clôture de la procédure dans les conditions définies au troisième alinéa du même I, la clôture fait l'objet de la même mesure de publicité que celle, le cas échéant, de la mise en demeure.</del>

<del style="background-color: #FFAEAE;">Les décisions prises par la formation restreinte au titre de l'article 45 sont motivées et notifiées au responsable du traitement. Les décisions prononçant une sanction peuvent faire l'objet d'un recours de pleine juridiction devant le Conseil d'Etat.</del>

<ins style="background-color: #F8EDA8;">I. – Lorsque le non-respect des dispositions du règlement (UE) 2016/679 ou de la présente loi entraîne une violation des droits et libertés mentionnés à l’article 1er et que le président de la commission considère qu’il est urgent d’intervenir, il saisit la formation restreinte qui peut, dans le cadre d’une procédure d’urgence contradictoire définie par décret en Conseil d’État, adopter l’une des mesures suivantes :</ins>

<ins style="background-color: #F8EDA8;">1° L’interruption provisoire de la mise en œuvre du traitement, y compris d’un transfert de données hors de l’Union européenne, pour une durée maximale de trois mois, si le traitement n’est pas au nombre de ceux qui sont mentionnés aux I et II de l’article 26 et ceux mentionnées à l’article 27 ;</ins>

<ins style="background-color: #F8EDA8;">2° La limitation du traitement de certaines des données à caractère personnel traitées, pour une durée maximale de trois mois, si le traitement n’est pas au nombre de ceux qui sont mentionnés aux I et II de l’article 26 ;</ins>

<ins style="background-color: #F8EDA8;">3° La suspension provisoire de la certification délivrée au responsable du traitement ou au sous-traitant ;</ins>

<ins style="background-color: #F8EDA8;">4° La suspension provisoire de l’agrément délivré à un organisme de certification ou un organisme chargé du respect d’un code de conduite ;</ins>

<ins style="background-color: #F8EDA8;">5° La suspension provisoire de l’autorisation délivrée sur le fondement du III de l’article 54 du chapitre IX de la présente loi.</ins>

<ins style="background-color: #F8EDA8;">6° L’injonction de mettre en conformité le traitement avec les obligations résultant du règlement (UE) 2016/679 ou de la présente loi, qui peut être assortie, sauf dans des cas où le traitement est mis en œuvre par l’État, d’une astreinte dont le montant ne peut excéder 100 000 € par jour ;</ins>

<ins style="background-color: #F8EDA8;">7° Un rappel à l’ordre ;</ins>

<ins style="background-color: #F8EDA8;">8° L’information du Premier ministre pour qu’il prenne, le cas échéant, les mesures permettant de faire cesser la violation constatée, si le traitement en cause est au nombre de ceux qui sont mentionnés aux mêmes I et II de l’article 26. Le Premier ministre fait alors connaître à la formation restreinte les suites qu’il a données à cette information au plus tard quinze jours après l’avoir reçue.</ins>

<ins style="background-color: #F8EDA8;">II. – Dans les circonstances exceptionnelles prévues au 1 de l’article 66 du règlement (UE) 2016/679, lorsque la formation restreinte adopte les mesures provisoires prévues aux 1° à 4° du I du présent article, elle informe sans délai de la teneur des mesures prises et de leurs motifs les autres autorités de contrôle concernées, le Comité européen de la protection des données et la Commission européenne.</ins>

<ins style="background-color: #F8EDA8;">Lorsque la formation restreinte a pris de telles mesures et qu’elle estime que des mesures définitives doivent être prises, elle met en œuvre les dispositions du 2 de l’article 66 du règlement.</ins>

<ins style="background-color: #F8EDA8;">III. – Pour les traitements régis par le chapitre XIII, lorsqu’une autorité de contrôle compétente en vertu du règlement (UE) 2016/679 n’a pas pris de mesure appropriée dans une situation où il est urgent d’intervenir afin de protéger les droits et libertés des personnes concernées, la formation restreinte, saisie par le président de la commission, peut demander au comité européen de la protection des données un avis d’urgence ou une décision contraignante d’urgence dans les conditions et selon les modalités prévues aux 3 et 4 de l’article 66 de ce règlement.</ins>

<ins style="background-color: #F8EDA8;">IV. – En cas d’atteinte grave et immédiate aux droits et libertés mentionnés à l’article 1er, le président de la commission peut en outre demander, par la voie du référé, à la juridiction compétente d’ordonner, le cas échéant sous astreinte, toute mesure nécessaire à la sauvegarde de ces droits et libertés.</ins>

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [Article 49-4](/loi-78-17-pjl-490/chapitre-vii/article-49-4)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-vii/article-45" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-vii/article-47" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

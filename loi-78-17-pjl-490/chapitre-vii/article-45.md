<!-- TITLE: Article 45 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-vii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-vii/article-46" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2016-1321 du 7 octobre 2016 - art. 64](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205210&dateTexte=20161008&categorieLien=id#LEGIARTI000033205210)

----

<del style="background-color: #FFAEAE;">I. - Lorsque le responsable d'un traitement ne respecte pas les obligations découlant de la présente loi, le président de la Commission nationale de l'informatique et des libertés peut le mettre en demeure de faire cesser le manquement constaté dans un délai qu'il fixe. En cas d'extrême urgence, ce délai peut être ramené à vingt-quatre heures.</del>

<del style="background-color: #FFAEAE;">Si le responsable du traitement se conforme à la mise en demeure qui lui est adressée, le président de la commission prononce la clôture de la procédure.</del>

<del style="background-color: #FFAEAE;">Dans le cas contraire, la formation restreinte de la commission peut prononcer, après une procédure contradictoire, les sanctions suivantes :</del>

<del style="background-color: #FFAEAE;">1° Un avertissement ;</del>

<del style="background-color: #FFAEAE;">2° Une sanction pécuniaire, dans les conditions prévues à l'article 47, à l'exception des cas où le traitement est mis en œuvre par l'Etat ;</del>

<del style="background-color: #FFAEAE;">3° Une injonction de cesser le traitement, lorsque celui-ci relève de l'article 22, ou un retrait de l'autorisation accordée en application de l'article 25.</del>

<del style="background-color: #FFAEAE;">Lorsque le manquement constaté ne peut faire l'objet d'une mise en conformité dans le cadre d'une mise en demeure, la formation restreinte peut prononcer, sans mise en demeure préalable et après une procédure contradictoire, les sanctions prévues au présent I.</del>

<del style="background-color: #FFAEAE;">II. - Lorsque la mise en œuvre d'un traitement ou l'exploitation des données traitées entraîne une violation des droits et libertés mentionnés à l'article 1er, la formation restreinte, saisie par le président de la commission, peut, dans le cadre d'une procédure d'urgence définie par décret en Conseil d'Etat, après une procédure contradictoire :</del>

<del style="background-color: #FFAEAE;">1° Décider l'interruption de la mise en œuvre du traitement, pour une durée maximale de trois mois, si le traitement n'est pas au nombre de ceux qui sont mentionnés aux I et II de l'article 26 ou de ceux mentionnés à l'article 27 mis en œuvre par l'Etat ;</del>

<del style="background-color: #FFAEAE;">2° Prononcer un avertissement visé au 1° du I ;</del>

<del style="background-color: #FFAEAE;">3° Décider le verrouillage de certaines des données à caractère personnel traitées, pour une durée maximale de trois mois, si le traitement n'est pas au nombre de ceux qui sont mentionnés aux I et II de l'article 26 ;</del>

<del style="background-color: #FFAEAE;">4° Informer le Premier ministre pour qu'il prenne, le cas échéant, les mesures permettant de faire cesser la violation constatée, si le traitement en cause est au nombre de ceux qui sont mentionnés aux mêmes I et II de l'article 26 ; le Premier ministre fait alors connaître à la formation restreinte les suites qu'il a données à cette information au plus tard quinze jours après l'avoir reçue.</del>

<del style="background-color: #FFAEAE;">III. - En cas d'atteinte grave et immédiate aux droits et libertés mentionnés à l'article 1er, le président de la commission peut demander, par la voie du référé, à la juridiction compétente d'ordonner, le cas échéant sous astreinte, toute mesure nécessaire à la sauvegarde de ces droits et libertés.</del>

<ins style="background-color: #F8EDA8;"> I. – Le président de la Commission nationale de l’informatique et des libertés peut avertir un responsable du traitement ou un sous-traitant du fait que les opérations de traitement envisagées sont susceptibles de violer les dispositions du règlement (UE) 2016/679 ou de la présente loi.</ins>

<ins style="background-color: #F8EDA8;">II. – Lorsque le responsable du traitement ou le sous-traitant ne respecte pas les obligations résultant du règlement (UE) 2016/679 ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut saisir la formation restreinte de la commission en vue du prononcé, après procédure contradictoire, de l’une ou de plusieurs des mesures suivantes :</ins>

<ins style="background-color: #F8EDA8;">1° Un rappel à l’ordre ;</ins>

<ins style="background-color: #F8EDA8;">2° Une injonction de mettre en conformité le traitement avec les obligations résultant de la présente loi ou du règlement (UE) 2016/679 ou de satisfaire aux demandes présentées par la personne concernée en vue d’exercer ses droits, qui peut être assortie, sauf dans des cas où le traitement est mis en œuvre par l’État, d’une astreinte dont le montant ne peut excéder 100 000 € par jour ;</ins>

<ins style="background-color: #F8EDA8;">3° À l’exception des traitements qui intéressent la sûreté de l’État ou la défense, la limitation temporaire ou définitive du traitement, son interdiction ou le retrait d’une autorisation accordée en application du règlement (UE) 2016/679 ou de la présente loi ;</ins>

<ins style="background-color: #F8EDA8;">4° Le retrait d’une certification ou l’injonction, à l’organisme concerné, de refuser ou de retirer la certification accordée ;</ins>

<ins style="background-color: #F8EDA8;">5° La suspension des flux de données adressées à un destinataire situé dans un pays tiers ou à une organisation internationale ;</ins>

<ins style="background-color: #F8EDA8;">6° Le retrait de la décision d’approbation d’une règle d’entreprise contraignante ;</ins>

<ins style="background-color: #F8EDA8;">7° À l’exception des cas où le traitement est mis en œuvre par l’État, une amende administrative ne pouvant excéder 10 millions d’euros ou, s’agissant d’une entreprise, 2 % du chiffre d’affaires annuel mondial total de l’exercice précédent, le montant le plus élevé étant retenu. Dans les hypothèses mentionnées aux paragraphes 5 et 6 de l’article 83 du règlement (UE) 2016/679, ces plafonds sont portés respectivement à 20 millions d’euros et 4 % du chiffre d’affaires. La formation restreinte prend en compte, dans la détermination du montant de l’amende, les critères précisés à l’article 83 du règlement (UE) 2016/679.</ins>

<ins style="background-color: #F8EDA8;">Lorsque la formation restreinte a prononcé une sanction pécuniaire devenue définitive avant que le juge pénal ait statué définitivement sur les mêmes faits ou des faits connexes, celui-ci peut ordonner que l’amende administrative s’impute sur l’amende pénale qu’il prononce.</ins>

<ins style="background-color: #F8EDA8;">Les sanctions pécuniaires sont recouvrées comme les créances de l’État étrangères à l’impôt et au domaine.</ins>

<ins style="background-color: #F8EDA8;">Le projet de mesure est le cas échéant soumis aux autres autorités concernées selon les modalités définies à l’article 60 du règlement (UE) 2016/679.</ins>

<ins style="background-color: #F8EDA8;">III. – Lorsque le responsable d’un traitement ou le sous-traitant ne respecte pas les obligations découlant du règlement (UE) 2016/679 ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut également prononcer à son égard une mise en demeure, dans le délai qu’il fixe :

<ins style="background-color: #F8EDA8;">1° De satisfaire aux demandes présentées par la personne concernée en vue d’exercer ses droits ;</ins>

<ins style="background-color: #F8EDA8;">2° De mettre les opérations de traitement en conformité avec les dispositions applicables ;</ins>

<ins style="background-color: #F8EDA8;">3° À l’exception des traitements qui intéressent la sûreté de l’État ou la défense et ceux mentionnées à l’article 27, de communiquer à la personne concernée une violation de données à caractère personnel ;</ins>

<ins style="background-color: #F8EDA8;">4° De rectifier ou d’effacer des données à caractère personnel, ou de limiter le traitement.</ins>

<ins style="background-color: #F8EDA8;">Dans le cas prévu au 4°, le président peut, dans les mêmes conditions, mettre en demeure le responsable de traitement ou le sous-traitant de notifier aux destinataires des données les mesures qu’il a prises.</ins>

<ins style="background-color: #F8EDA8;">Le délai de mise en conformité peut être fixé à vingt-quatre heures en cas d’extrême urgence.</ins>

<ins style="background-color: #F8EDA8;">Le président prononce, le cas échéant, la clôture de la procédure de mise en demeure.</ins>

<ins style="background-color: #F8EDA8;">Le président peut demander au bureau de rendre publique la mise en demeure. Dans ce cas, la décision de clôture de la procédure de mise en demeure fait l’objet de la même publicité.</ins>

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [Article 49-4](/loi-78-17-pjl-490/chapitre-vii/article-49-4)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-vii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-vii/article-46" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

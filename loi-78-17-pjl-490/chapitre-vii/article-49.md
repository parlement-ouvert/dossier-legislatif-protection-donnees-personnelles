<!-- TITLE: Article 49 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-vii/article-48" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-vii/article-49-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2011-334
 du 29 mars 2011 - art. 8](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782616&dateTexte=20110330&categorieLien=id#LEGIARTI000023782616)

----

<del style="background-color: #FFAEAE;">La commission peut, à la demande d'une autorité exerçant des compétences analogues aux siennes dans un autre Etat membre de l'Union européenne, procéder à des vérifications dans les mêmes conditions que celles prévues à l'article 44, sauf s'il s'agit d'un traitement mentionné aux I ou II de l'article 26.</del>

<del style="background-color: #FFAEAE;">Le président de la commission ou la formation restreinte peuvent, à la demande d'une autorité exerçant des compétences analogues aux leurs dans un autre Etat membre de l'Union européenne, prendre les décisions mentionnées aux articles 45 à 47 et dans les conditions prévues par ces mêmes articles, sauf s'il s'agit d'un traitement mentionné aux I ou II de l'article 26.</del>

<del style="background-color: #FFAEAE;">La commission est habilitée à communiquer les informations qu'elle recueille ou qu'elle détient, à leur demande, aux autorités exerçant des compétences analogues aux siennes dans d'autres Etats membres de la Communauté européenne.</del>

<ins style="background-color: #F8EDA8;">Dans les conditions prévues aux articles [60](/reglement-2016-679/chapitre-vii/section-1/article-60) à [67](/reglement-2016-679/chapitre-vii/section-2/article-67), du règlement (UE) 2016/679, la Commission nationale de l’informatique et des libertés met en œuvre des procédures de coopération et d’assistance mutuelle avec les autorités de contrôle des autres États membres de l’Union européenne, et réalise avec elles des opérations conjointes.</ins>

<ins style="background-color: #F8EDA8;">La commission, le président, le bureau, la formation restreinte et les agents de la commission mettent en œuvre, chacun pour ce qui les concerne, les procédures visées à l’alinéa précédent.</ins>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-vii/article-48" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-vii/article-49-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Chapitre V -->
<!-- SUBTITLE: Obligations incombant aux responsables de traitements et droits des personnes -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-iv-abroge/article-33-1-abroge" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-v/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Section 1 — Obligations incombant aux responsables de traitements.](/loi-78-17-pjl-490/chapitre-v/section-1)

  * [Article 32](/loi-78-17-pjl-490/chapitre-v/section-1/article-32)
  * [Article 33](/loi-78-17-pjl-490/chapitre-v/section-1/article-33)
  * [Article 34](/loi-78-17-pjl-490/chapitre-v/section-1/article-34)
  * [Article 34 bis](/loi-78-17-pjl-490/chapitre-v/section-1/article-34-bis)
  * [Article 35](/loi-78-17-pjl-490/chapitre-v/section-1/article-35)
  * [Article 36](/loi-78-17-pjl-490/chapitre-v/section-1/article-36)
  * [Article 37](/loi-78-17-pjl-490/chapitre-v/section-1/article-37)

* [Section 2 — Droits des personnes à l'égard des traitements de données à caractère personnel.](/loi-78-17-pjl-490/chapitre-v/section-2)

  * [Article 38](/loi-78-17-pjl-490/chapitre-v/section-2/article-38)
  * [Article 39](/loi-78-17-pjl-490/chapitre-v/section-2/article-39)
  * [Article 40](/loi-78-17-pjl-490/chapitre-v/section-2/article-40)
  * [Article 40-1](/loi-78-17-pjl-490/chapitre-v/section-2/article-40-1)
  * [Article 41](/loi-78-17-pjl-490/chapitre-v/section-2/article-41)
  * [Article 42](/loi-78-17-pjl-490/chapitre-v/section-2/article-42)
  * [Article 43](/loi-78-17-pjl-490/chapitre-v/section-2/article-43)
  * [Article 43 bis](/loi-78-17-pjl-490/chapitre-v/section-2/article-43-bis)
  * [Article 43 ter](/loi-78-17-pjl-490/chapitre-v/section-2/article-43-ter)
  * [Article 43 quater](/loi-78-17-pjl-490/chapitre-v/section-2/article-43-quater)
  * [Article 43 quinquies](/loi-78-17-pjl-490/chapitre-v/section-2/article-43-quinquies)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-iv-abroge/article-33-1-abroge" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-v/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

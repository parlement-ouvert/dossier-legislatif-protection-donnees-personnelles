<!-- TITLE: Article 54 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-ix/section-1/article-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-ix/section-1/article-55" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

<del style="background-color: #FFAEAE;">I.-Les traitements de données à caractère personnel ayant une finalité d'intérêt public de recherche, d'étude ou d'évaluation dans le domaine de la santé sont autorisés par la Commission nationale de l'informatique et des libertés, dans le respect des principes définis par la présente loi et en fonction de l'intérêt public que la recherche, l'étude ou l'évaluation présente.</del>

<del style="background-color: #FFAEAE;">II.-La Commission nationale de l'informatique et des libertés prend sa décision après avis :</del>

<del style="background-color: #FFAEAE;">1° Du comité compétent de protection des personnes mentionné à l' article L. 1123-6 du code de la santé publique , pour les demandes d'autorisation relatives aux recherches impliquant la personne humaine mentionnées à l'article L. 1121-1 du même code ;</del>

<del style="background-color: #FFAEAE;">2° Du comité d'expertise pour les recherches, les études et les évaluations dans le domaine de la santé, pour les demandes d'autorisation relatives à des études ou à des évaluations ainsi qu'à des recherches n'impliquant pas la personne humaine, au sens du 1° du présent II.</del>

<del style="background-color: #FFAEAE;">Le comité d'expertise est composé de personnes choisies en raison de leur compétence, dans une pluralité de disciplines. Un décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, précise la composition du comité et définit ses règles de fonctionnement. Il peut prévoir l'existence de plusieurs sections au sein du comité, compétentes en fonction de la nature ou de la finalité du traitement. Le comité d'expertise est soumis à l' article L. 1451-1 du code de la santé publique.</del>

<del style="background-color: #FFAEAE;">Le comité d'expertise émet, dans un délai d'un mois à compter de sa saisine, un avis sur la méthodologie retenue, sur la nécessité du recours à des données à caractère personnel, sur la pertinence de celles-ci par rapport à la finalité du traitement et, s'il y a lieu, sur la qualité scientifique du projet. Le cas échéant, le comité recommande aux demandeurs des modifications de leur projet afin de le mettre en conformité avec les obligations prévues par la présente loi. A défaut d'avis du comité dans le délai d'un mois, l'avis est réputé favorable. En cas d'urgence, ce délai peut être ramené à quinze jours.</del>

<del style="background-color: #FFAEAE;">Dans des conditions définies par décret en Conseil d'Etat, l'Institut national des données de santé, prévu à l' article L. 1462-1 du code de la santé publique , peut être saisi par la Commission nationale de l'informatique et des libertés ou le ministre chargé de la santé sur le caractère d'intérêt public que présente la recherche, l'étude ou l'évaluation justifiant la demande de traitement ; il peut également évoquer le cas de sa propre initiative. Dans tous les cas, il rend un avis dans un délai d'un mois à compter de sa saisine.</del>

<del style="background-color: #FFAEAE;">Les dossiers présentés dans le cadre du présent chapitre, à l'exclusion des recherches mentionnées aux 1° et 2° de l'article L. 1121-1 du code de la santé publique et de celles mentionnées au 3° du même article L. 1121-1 portant sur des produits mentionnés à l'article L. 5311-1 du même code, sont déposés auprès d'un secrétariat unique, qui assure leur orientation vers les instances compétentes.</del>

<del style="background-color: #FFAEAE;">III.-Pour chaque demande, la Commission nationale de l'informatique et des libertés vérifie les garanties présentées par le demandeur pour l'application des présentes dispositions et la conformité de sa demande à ses missions ou à son objet social. Si le demandeur n'apporte pas d'éléments suffisants pour attester la nécessité de disposer de certaines informations parmi l'ensemble des données à caractère personnel dont le traitement est envisagé, la commission peut interdire la communication de ces informations par l'organisme qui les détient et n'autoriser le traitement que pour ces données réduites.</del>

<del style="background-color: #FFAEAE;">La commission statue sur la durée de conservation des données nécessaires au traitement et apprécie les dispositions prises pour assurer leur sécurité et la garantie des secrets protégés par la loi.</del>

<del style="background-color: #FFAEAE;">IV.-Pour les catégories les plus usuelles de traitements automatisés de données de santé à caractère personnel à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé, la Commission nationale de l'informatique et des libertés peut homologuer et publier des méthodologies de référence destinées à simplifier la procédure d'examen. Celles-ci sont établies en concertation avec le comité d'expertise et des organismes publics et privés représentatifs des acteurs concernés.</del>

<del style="background-color: #FFAEAE;">V.-Des jeux de données agrégées ou des échantillons, issus des traitements des données de santé à caractère personnel pour des finalités et dans des conditions reconnues conformes à la présente loi par la Commission nationale de l'informatique et des libertés, peuvent faire l'objet d'une mise à disposition, dans des conditions préalablement homologuées par la commission, sans que l'autorisation prévue au I du présent article soit requise.</del>

<del style="background-color: #FFAEAE;">VI.-La commission peut, par décision unique, délivrer à un même demandeur une autorisation pour des traitements répondant à une même finalité, portant sur des catégories de données identiques et ayant des catégories de destinataires identiques.</del>

<ins style="background-color: #F8EDA8;">I. – Les traitements relevant du présent chapitre ne peuvent être mis en œuvre qu’en considération de la finalité d’intérêt public qu’ils présentent.

<ins style="background-color: #F8EDA8;">II. – Des référentiels et règlements types, au sens des [_a bis_ et _b_ du 2° de l’article 11](/loi-78-17-pjl-490/chapitre-iii/article-11), s’appliquant aux traitements relevant du présent chapitre sont établis par la Commission nationale de l’informatique et des libertés en concertation avec l’Institut national des données de santé mentionné à l’[article L. 1462-1 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031923960) et des organismes publics et privés représentatifs des acteurs concernés.</ins>

<ins style="background-color: #F8EDA8;">Les traitements conformes à ces référentiels et règlements types peuvent être mis en œuvre à la condition que leurs responsables adressent préalablement à la Commission nationale de l’informatique une déclaration attestant de cette conformité.</ins>

<ins style="background-color: #F8EDA8;">Ces référentiels, peuvent également porter sur la description et les garanties de procédure permettant la mise à disposition en vue de leur traitement de jeux de données de santé présentant un faible risque d’impact sur la vie privée.</ins>

<ins style="background-color: #F8EDA8;">III. – Les traitements mentionnés au premier alinéa du I qui ne sont pas conformes à un référentiel ou à un règlement type mentionné au II ne peuvent être mis en œuvre qu’après autorisation par la Commission nationale de l’informatique et des libertés.</ins>

<ins style="background-color: #F8EDA8;">L’Institut national des données de santé mentionné à l’article [L. 1462-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031923960) du code de la santé publique peut se saisir ou être saisi, dans des conditions définies par décret en Conseil d’État, par la Commission nationale de l’informatique et des libertés ou le ministre chargé de la santé sur le caractère d’intérêt public que présente le traitement.</ins>

<ins style="background-color: #F8EDA8;">IV. – La commission peut, par décision unique, délivrer à un même demandeur une autorisation pour des traitements répondant à une même finalité, portant sur des catégories de données identiques et ayant des catégories de destinataires identiques.</ins>

<ins style="background-color: #F8EDA8;">V. – La Commission nationale de l’informatique et des libertés se prononce dans un délai de deux mois à compter de la réception de la demande. Toutefois, ce délai peut être renouvelé une fois sur décision motivée de son président ou lorsque l’Institut national des données de santé est saisi en application du II du présent article.</ins>

<ins style="background-color: #F8EDA8;">Lorsque la commission ne s’est pas prononcée dans ces délais, la demande d’autorisation est réputée acceptée. Cette disposition n’est toutefois pas applicable si l’autorisation fait l’objet d’un avis préalable en vertu des dispositions du présent chapitre et que l’avis ou les avis rendus ne sont pas expressément favorables.</ins>

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [Article 11](/loi-78-17-pjl-490/chapitre-iii/article-11)
* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 11](/loi-78-17-pjl-592/chapitre-iii/article-11)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-ix/section-1/article-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-ix/section-1/article-55" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

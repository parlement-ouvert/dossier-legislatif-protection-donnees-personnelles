<!-- TITLE: Article 53 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-ix/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-ix/section-1/article-54" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

<del style="background-color: #FFAEAE;">Les traitements automatisés de données à caractère personnel ayant pour finalité la recherche ou les études dans le domaine de la santé ainsi que l'évaluation ou l'analyse des pratiques ou des activités de soins ou de prévention sont soumis à la présente loi, à l'exception des articles 23 et 24, du I de l'article 25 et des articles 26,32 et 38.</del>

<del style="background-color: #FFAEAE;">Toutefois, le présent chapitre n'est pas applicable :</del>

<del style="background-color: #FFAEAE;">1° Aux traitements de données à caractère personnel ayant pour fin le suivi thérapeutique ou médical individuel des patients ;</del>

<del style="background-color: #FFAEAE;">2° Aux traitements permettant d'effectuer des études à partir des données recueillies en application du 1° lorsque ces études sont réalisées par les personnels assurant ce suivi et destinées à leur usage exclusif ;</del>

<del style="background-color: #FFAEAE;">3° Aux traitements effectués à des fins de remboursement ou de contrôle par les organismes chargés de la gestion d'un régime de base d'assurance maladie ;</del>

<del style="background-color: #FFAEAE;">4° Aux traitements effectués au sein des établissements de santé par les médecins responsables de l'information médicale, dans les conditions prévues au deuxième alinéa de l'article L. 6113-7 du code de la santé publique ;</del>

<del style="background-color: #FFAEAE;">5° Aux traitements effectués par les agences régionales de santé, par l'Etat et par la personne publique désignée par lui en application du premier alinéa de l'article L. 6113-8 du même code, dans le cadre défini au même article ;</del>

<del style="background-color: #FFAEAE;">6° Aux traitements mis en œuvre par les organismes ou les services chargés d'une mission de service public figurant sur une liste fixée par arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l'informatique et des libertés, afin de répondre à une alerte sanitaire, dans les conditions prévues au V de l'article 22.</del>

<ins style="background-color: #F8EDA8;">Outre les dispositions du [règlement (UE) 2016/679](/reglement-2016-679), les traitements contenant des données concernant la santé des personnes sont soumis aux dispositions du présent chapitre, à l’exception des catégories de traitements suivantes :</ins>

<ins style="background-color: #F8EDA8;">1° Les traitements relevant des 1° à 6° du II de l’[article 8](/loi-78-17-pjl-490/chapitre-ii/section-2/article-8) ;</ins>

<ins style="background-color: #F8EDA8;">2° Les traitements permettant d’effectuer des études à partir des données recueillies en application du [6° du II de l’article 8](/loi-78-17-pjl-490/chapitre-ii/section-2/article-8) lorsque ces études sont réalisées par les personnels assurant ce suivi et destinées à leur usage exclusif ;</ins>

<ins style="background-color: #F8EDA8;">3° Les traitements effectués à des fins de remboursement ou de contrôle par les organismes chargés de la gestion d’un régime de base d’assurance maladie ;</ins>

<ins style="background-color: #F8EDA8;">4° Les traitements effectués au sein des établissements de santé par les médecins responsables de l’information médicale, dans les conditions prévues au deuxième alinéa de l’[article L. 6113-7 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006690710) ;</ins>

<ins style="background-color: #F8EDA8;">5° Les traitements effectués par les agences régionales de santé, par l’État et par la personne publique désignée par lui en application du premier alinéa de l’[article L. 6113-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021940593&cidTexte=LEGITEXT000006072665&dateTexte=20180112) du même code, dans le cadre défini au même article.</ins>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-ix/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-ix/section-1/article-54" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

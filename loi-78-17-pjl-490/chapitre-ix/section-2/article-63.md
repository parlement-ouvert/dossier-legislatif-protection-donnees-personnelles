<!-- TITLE: Article 63 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-ix/section-2/article-62" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-x-abroge" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

<del style="background-color: #FFAEAE;">(abrogé)</del>

<ins style="background-color: #F8EDA8;">L’autorisation du traitement est accordée par la Commission nationale de l’informatique et des libertés dans les conditions définies à [l’article 54](/loi-78-17-pjl-490/chapitre-ix/article-54) et après avis :</ins>

<ins style="background-color: #F8EDA8;">1° Du comité compétent de protection des personnes mentionné à [l’article L. 1123-6 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685877), pour les demandes d’autorisation relatives aux recherches impliquant la personne humaine mentionnées à [l’article L. 1121-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032722870&cidTexte=LEGITEXT000006072665) du même code ;</ins>

<ins style="background-color: #F8EDA8;">2° Du comité d’expertise pour les recherches, les études et les évaluations dans le domaine de la santé, pour les demandes d’autorisation relatives à des études ou à des évaluations ainsi qu’à des recherches n’impliquant pas la personne humaine, au sens du 1° du présent article. Un décret en Conseil d’État, pris après avis de la Commission nationale de l’informatique et des libertés, fixe la composition de ce comité et définit ses règles de fonctionnement. Le comité d’expertise est soumis à [l’article L. 1451-1 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000022019483).</ins>

<ins style="background-color: #F8EDA8;">Les dossiers présentés dans le cadre de la présente section, à l’exclusion des recherches impliquant la personne humaine, sont déposés auprès d’un secrétariat unique assuré par l’Institut national des données de santé, qui assure leur orientation vers les instances compétentes.</ins>

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 592 annexe 0 - Rapport sur le projet de loi, après engagement de la procédure accélérée, relatif à la protection des données personnelles (n°490).
  * [Article 13](/pjl-592/titre-ii/chapitre-iv/article-13)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-ix/section-2/article-62" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-x-abroge" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

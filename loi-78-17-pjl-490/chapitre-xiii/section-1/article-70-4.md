<!-- TITLE: Article 70-4 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-1/article-70-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-1/article-70-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<ins style="background-color: #F8EDA8;">Si le traitement est susceptible d’engendrer un risque élevé pour les droits et les libertés des personnes physiques, notamment parce qu’il porte sur des données mentionnées au [I de l’article 8](/loi-78-17-pjl-490/chapitre-ii/section-2/article-8), le responsable du traitement effectue une analyse d’impact relative à la protection des données à caractère personnel.

<ins style="background-color: #F8EDA8;">Si le traitement est mis en œuvre pour le compte de l’État, cette analyse d’impact est adressée à la Commission nationale de l’informatique et des libertés avec la demande d’avis prévue par [l’article 30](/loi-78-17-pjl-490/chapitre-iv/section-3/article-30).

<ins style="background-color: #F8EDA8;">Dans les autres cas, le responsable du traitement ou le sous-traitant consulte la Commission nationale de l’informatique et des libertés préalablement au traitement des données à caractère personnel :

<ins style="background-color: #F8EDA8;">1° Soit lorsque l’analyse d’impact relative à la protection des données indique que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque ;

<ins style="background-color: #F8EDA8;">2° Soit lorsque le type de traitement, en particulier en raison de l’utilisation de nouveaux mécanismes, technologies ou procédures, présente des risques élevés pour les libertés et les droits des personnes concernées.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [Article 11](/loi-78-17-pjl-490/chapitre-iii/article-11)
* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 11](/loi-78-17-pjl-592/chapitre-iii/article-11)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-1/article-70-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-1/article-70-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

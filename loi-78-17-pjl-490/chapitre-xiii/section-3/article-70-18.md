<!-- TITLE: Article 70-18 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-3/article-70-19" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<ins style="background-color: #F8EDA8;">I. – Le responsable du traitement met à la disposition de la personne concernée les informations suivantes :

<ins style="background-color: #F8EDA8;">1° L’identité et les coordonnées du responsable du traitement, et le cas échéant celles de son représentant ;

<ins style="background-color: #F8EDA8;">2° Le cas échéant, les coordonnées du délégué à la protection des données ;

<ins style="background-color: #F8EDA8;">3° Les finalités poursuivies par le traitement auquel les données sont destinées ;

<ins style="background-color: #F8EDA8;">4° Le droit d’introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés et les coordonnées de la commission ;

<ins style="background-color: #F8EDA8;">5° L’existence du droit de demander au responsable du traitement l’accès aux données à caractère personnel, leur rectification ou leur effacement, et la limitation du traitement des données à caractère personnel relatives à une personne concernée.

<ins style="background-color: #F8EDA8;">II. – En plus des informations visées au I, le responsable du traitement fournit à la personne concernée, dans des cas particuliers, les informations additionnelles suivantes afin de lui permettre d’exercer ses droits :

<ins style="background-color: #F8EDA8;">1° La base juridique du traitement ;

<ins style="background-color: #F8EDA8;">2° La durée de conservation des données à caractère personnel ou, lorsque ce n’est pas possible, les critères utilisés pour déterminer cette durée ;

<ins style="background-color: #F8EDA8;">3° Le cas échéant, les catégories de destinataires des données à caractère personnel, y compris dans les États non membres de l’Union européenne ou au sein d’organisations internationales ;

<ins style="background-color: #F8EDA8;">4° Au besoin, des informations complémentaires, en particulier lorsque les données à caractère personnel sont collectées à l’insu de la personne concernée.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-3/article-70-19" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

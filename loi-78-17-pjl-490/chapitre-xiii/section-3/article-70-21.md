<!-- TITLE: Article 70-21 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-3/article-70-20" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-3/article-70-22" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<ins style="background-color: #F8EDA8;">I. – Les droits de la personne physique concernée peuvent faire l’objet de restrictions selon les modalités prévues au II du présent article dès lors et aussi longtemps qu’une telle restriction constitue une mesure nécessaire et proportionnée dans une société démocratique en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne pour :

<ins style="background-color: #F8EDA8;">1° Éviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires :

<ins style="background-color: #F8EDA8;">2° Éviter de nuire à la prévention ou à la détection d’infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l’exécution de sanctions pénales ;

<ins style="background-color: #F8EDA8;">3° Protéger la sécurité publique ;

<ins style="background-color: #F8EDA8;">4° Protéger la sécurité nationale ;

<ins style="background-color: #F8EDA8;">5° Protéger les droits et libertés d’autrui.

<ins style="background-color: #F8EDA8;">Ces restrictions sont prévues par l’acte instaurant le traitement.

<ins style="background-color: #F8EDA8;">II. – Lorsque les conditions prévues au I sont remplies, le responsable du traitement peut :

<ins style="background-color: #F8EDA8;">1° Retarder ou limiter la fourniture à la personne concernée des informations mentionnées au II de l’article 70-18, ou ne pas fournir ces informations ;

<ins style="background-color: #F8EDA8;">2° Limiter, entièrement ou partiellement, le droit d’accès de la personne concernée prévu par l’article 70-19 ;

<ins style="background-color: #F8EDA8;">3° Ne pas informer la personne de son refus de rectifier ou d’effacer des données à caractère personnel ou de limiter le traitement, ainsi que des motifs de cette décision conformément au IV de l’article 70-20.

<ins style="background-color: #F8EDA8;">III. – Dans les cas visés au 2° du II, le responsable du traitement informe la personne concernée, dans les meilleurs délais, de tout refus ou de toute limitation d’accès, ainsi que des motifs du refus ou de la limitation. Ces informations peuvent ne pas être fournies lorsque leur communication risque de compromettre l’un des objectifs énoncés au I. Le responsable du traitement consigne les motifs de fait ou de droit sur lesquels se fonde la décision, et met ces informations à la disposition de la Commission nationale de l’informatique et des libertés.

<ins style="background-color: #F8EDA8;">IV. – En cas de restriction des droits de la personne concernée intervenue en application du II ou du III, le responsable du traitement informe la personne concernée de la possibilité d’exercer ses droits par l’intermédiaire de la Commission nationale de l’informatique et des libertés ou de former un recours juridictionnel.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-3/article-70-20" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-3/article-70-22" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 70-19 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-3/article-70-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-3/article-70-20" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<ins style="background-color: #F8EDA8;">La personne concernée a le droit d’obtenir du responsable du traitement la confirmation que des données à caractère personnel la concernant sont ou ne sont pas traitées et, lorsqu’elles le sont, l’accès auxdites données ainsi que les informations suivantes :

<ins style="background-color: #F8EDA8;">1° Les finalités du traitement ainsi que sa base juridique ;

<ins style="background-color: #F8EDA8;">2° Les catégories de données à caractère personnel concernées ;

<ins style="background-color: #F8EDA8;">3° Les destinataires ou catégories de destinataires auxquels les données à caractère personnel ont été communiquées, en particulier les destinataires qui sont établis dans des États non membres de l’Union européenne ou les organisations internationales ;

<ins style="background-color: #F8EDA8;">4° Lorsque cela est possible, la durée de conservation des données à caractère personnel envisagée ou, lorsque ce n’est pas possible, les critères utilisés pour déterminer cette durée ;

<ins style="background-color: #F8EDA8;">5° L’existence du droit de demander au responsable du traitement la rectification ou l’effacement des données à caractère personnel, ou la limitation du traitement de ces données ;

<ins style="background-color: #F8EDA8;">6° Le droit d’introduire une réclamation auprès de la Commission nationale de l’informatique et des libertés et les coordonnées de la commission ;

<ins style="background-color: #F8EDA8;">7° La communication des données à caractère personnel en cours de traitement, ainsi que toute information disponible quant à leur source.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-3/article-70-18" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-3/article-70-20" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

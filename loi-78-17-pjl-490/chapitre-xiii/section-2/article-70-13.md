<!-- TITLE: Article 70-13 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-2/article-70-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-2/article-70-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<ins style="background-color: #F8EDA8;">I. – Afin de démontrer que le traitement est effectué conformément au présent chapitre, le responsable du traitement et le sous-traitant mettent en œuvre les mesures prévues aux [paragraphes 1 et 2 de l’article 24](/reglement-2016-679/chapitre-iv/section-1/article-24) et aux [paragraphes 1 et 2 de l’article 25](/reglement-2016-679/chapitre-iv/section-1/article-25) du règlement (UE) 2016/679 et celles appropriées afin de garantir un niveau de sécurité adapté au risque, notamment en ce qui concerne le traitement portant sur des catégories particulières de données à caractère personnel visées à l’[article 8](/loi-78-17-pjl-490/chapitre-ii/section-2/article-8).

<ins style="background-color: #F8EDA8;">II. – En ce qui concerne le traitement automatisé, le responsable du traitement ou le sous-traitant met en œuvre, à la suite d’une évaluation des risques, des mesures destinées à :

<ins style="background-color: #F8EDA8;">1° Empêcher toute personne non autorisée d’accéder aux installations utilisées pour le traitement (contrôle de l’accès aux installations) ;

<ins style="background-color: #F8EDA8;">2° Empêcher que des supports de données puissent être lus, copiés, modifiés ou supprimés de façon non autorisée (contrôle des supports de données) ;

<ins style="background-color: #F8EDA8;">3° Empêcher l’introduction non autorisée de données à caractère personnel dans le fichier, ainsi que l’inspection, la modification ou l’effacement non autorisé de données à caractère personnel enregistrées (contrôle de la conservation) ;

<ins style="background-color: #F8EDA8;">4° Empêcher que les systèmes de traitement automatisé puissent être utilisés par des personnes non autorisées à l’aide d’installations de transmission de données (contrôle des utilisateurs) ;

<ins style="background-color: #F8EDA8;">5° Garantir que les personnes autorisées à utiliser un système de traitement automatisé ne puissent accéder qu’aux données à caractère personnel sur lesquelles porte leur autorisation (contrôle de l’accès aux données) ;

<ins style="background-color: #F8EDA8;">6° Garantir qu’il puisse être vérifié et constaté à quelles instances des données à caractère personnel ont été ou peuvent être transmises ou mises à disposition par des installations de transmission de données (contrôle de la transmission) ;

<ins style="background-color: #F8EDA8;">7° Garantir qu’il puisse être vérifié et constaté a posteriori quelles données à caractère personnel ont été introduites dans les systèmes de traitement automatisé, et à quel moment et par quelle personne elles y ont été introduites (contrôle de l’introduction) ;

<ins style="background-color: #F8EDA8;">8° Empêcher que, lors de la transmission de données à caractère personnel ainsi que lors du transport de supports de données, les données puissent être lues, copiées, modifiées ou supprimées de façon non autorisée (contrôle du transport) ;

<ins style="background-color: #F8EDA8;">9° Garantir que les systèmes installés puissent être rétablis en cas d’interruption (restauration) ;

<ins style="background-color: #F8EDA8;">10° Garantir que les fonctions du système opèrent, que les erreurs de fonctionnement soient signalées (fiabilité) et que les données à caractère personnel conservées ne puissent pas être corrompues par un dysfonctionnement du système (intégrité).

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-2/article-70-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés selon le PJL 490</span> <a href="/loi-78-17-pjl-490/chapitre-xiii/section-2/article-70-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

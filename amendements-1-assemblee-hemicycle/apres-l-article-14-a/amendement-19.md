<!-- TITLE: Amendement n° 19 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-14-a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Marietta Karamanli et les membres du groupe Nouvelle Gauche**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 14](/pjl-592/titre-ii/chapitre-v/article-14) A, insérer l'article suivant:

# Titre

La seconde phrase du premier alinéa de l’article L. 311‑3‑1 du code des relations entre le public et l’administration est ainsi modifiée :

1° Après le mot : « sont », il est inséré le mot : « systématiquement » ;

2° À la fin, les mots : « à l’intéressé s’il en fait la demande » sont remplacés par les mots : « aux intéressés ».

# Exposé sommaire

Cet amendement vise à renforcer les obligations d’information du public par l’administration lorsque celle-ci a décidé de recourir à un algorithme.

Alors que l’article L. 311‑3‑1 du code des relations entre le public et l’administration, dans sa rédaction actuelle, prévoit que les informations concernant le fonctionnement de l’algorithme sont communiquées par l’administration à l’intéressé « s’il en fait la demande », le groupe Nouvelle Gauche propose que cette information soit systématique.

En effet, bien qu’à l’initiative de la rapporteure la commission des lois ait renforcé les obligations d’information de l’administration en prévoyant que l’explication du fonctionnement de l’algorithme doit être intelligible, ces éléments d’information ne sont communiquées qu’aux personnes qui en formulent la demande.

L’ensemble de ce dispositif repose sur une fiction impossible : celle d’administrés qui disposent du temps de formuler de telles demandes d’explication à l’administration ; alors que la plupart des administrés subissent ces décisions avec résignation.

Cet amendement a pour but de créer une information systématique des administrés concernant le fonctionnement des algorithmes lorsqu’une décision les concernant a été prise sur leur base.

Seule cette information systématique rendra effective la possibilité de comprendre comment a été prise la décision et in fine de la de contester.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-14-a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 42 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-2/amendement-158" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-2/amendement-43" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

[ARTICLE 2](/pjl-592/titre-i/chapitre-i/article-2)

# Titre

Compléter cet article par les trois alinéas suivants :

« 3° Après le douzième alinéa, sont insérés deux alinéas ainsi rédigés :

« Tous les membres sont désignés eu égard à leurs compétences réelles, notamment en matière numérique et leur connaissance des droits et libertés fondamentales. Un décret en Conseil d’État précise les critères précis d’évaluation de ces compétences avec un barème allant de 1 à 10\. L’évaluation précise de chaque candidat par son autorité de nomination est rendue publique.

« Les candidats font l’objet d’une audition devant un jury composé à parité de députés du Sénat et de l’Assemblée nationale, d’experts issus de la société civile, de membres d’organisations non-gouvernementales spécialisées en matière numérique et en matière de protection des libertés, de citoyens tirés au sort sur la base du volontariat. Le jury émet à la suite des auditions un avis consultatif rendu public. »

# Exposé sommaire

Les missions de la CNIL ne vont cesser de s’accroître quantitativement et de se spécialiser qualitativement. Il s’agit d’une autorité administrative indépendante devant faire face aux enjeux majeurs du XXIe de renouvellement de logiciel de protection des libertés fondamentales auquel nous invite prestement l’ère numérique.

Le Conseil d’État l’a montré dans son étude sur “ le numérique et les droits fondamentaux” [http ://www.ladocumentationfrancaise.fr/var/storage/rapports-publics/144000541.pdf](http://www.ladocumentationfrancaise.fr/var/storage/rapports-publics/144000541.pdf)

L’inventivité que ce changement de paradigme va requérir exige un haut degré d’expertise et de technicité des personnes membres de la CNIL. Il faut donc que tous les membres de la CNIL, sans exception, y soit parce qu’ils et elles sont compétent.e.s. Tout ceci en évitant les conflits d’intérêts (ô combien nombreux, qui peuvent survenir dans la nominations des membres d’autorités de régulation du type AAI...).

Afin d’assurer le respect de cette exigence de compétence, la publication des critères d’évaluation et des évaluations elles-mêmes est nécessaire, cela assure une certaine transparence de la procédure de sélection. Cela répond également à une exigence plus générale de transparence dans les procédures de nomination aux fonctions administratives de responsabilité.

L’existence et la composition du jury répond aux mêmes exigences de transparences et de compétences dans la sélection et permet de croiser lors du processus nomination plusieurs perspectives, institutionnelles, professionnelles, citoyennes, militantes…

Cet amendement va dans le sens d’un renforcement d’une institution au rôle de plus en plus central et participe à sa crédibilisation.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-2/amendement-158" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-2/amendement-43" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

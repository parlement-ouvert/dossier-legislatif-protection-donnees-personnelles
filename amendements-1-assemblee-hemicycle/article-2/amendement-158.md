<!-- TITLE: Amendement n° 158 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-2/amendement-42" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Gosselin**

[ARTICLE 2](/pjl-592/titre-i/chapitre-i/article-2)

# Titre

I. – Supprimer l’alinéa 2.

II. – En conséquence, à l’alinéa 3, substituer au mot :

« et »

le mot :

« ou ».

# Exposé sommaire

Le présent amendement vise à revenir à l’écriture initiale du projet de loi et concerne le champs des qualifications des trois personnalités nommées par décret ainsi que des deux personnalités désignées respectivement par le Président de l’Assemblée nationale et par le Président du Sénat qui siègent à la Commission nationale de l’informatique et des libertés (CNIL).

Il est en effet primordial que les membres de la CNIL possèdent une expertise large dans les questions touchant au numérique et aux libertés individuelles mais sans lier nécessairement entre-elles ces qualifications ce qui, en réalité, réduit considérablement le champ des profils possibles.

Compte tenu des nouvelles missions de la CNIL, la présence d’un(e) philosophe, d’un(e) historien(ne), etc... pourrait être, en effet, intéressant. La rédaction adoptée en commission qui semblait bien venue risque, de fait, de ne plus permettre cette possibilité.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-2/amendement-42" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

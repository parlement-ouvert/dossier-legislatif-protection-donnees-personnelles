<!-- TITLE: Amendement n° 67 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-11/amendement-126" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

[ARTICLE 11](/pjl-592/titre-ii/chapitre-iv/article-11)

# Titre

Supprimer cet article.

# Exposé sommaire

Cet article prévoit l’ouverture du traitement de données relatives aux condamnations pénales, aux infractions ou aux mesures de sûreté connexes à des personnes morales de droit privé. Si nous entendons l’argument selon lequel ces données seront utiles aux associations d’aide aux victimes et de réinsertion, nous estimons que le dispositif ne contient pas de garanties suffisantes du caractère restreint et contrôlé de l’usage de ces données très sensibles.

Mais surtout, il banalise l’idée selon laquelle des missions de service sont assurées par le secteur privé. Cela ne nous semble pas être un modèle gage de qualité ni de cohérence républicaine. La solidité de nos institutions passe par leur maintien dans le secteur public, avec toutes les garanties que lui seul offre en terme de réserve, d’encadrement et d’égalité face au public. C’est pourquoi nous demandons la suppression de cet article.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-11/amendement-126" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

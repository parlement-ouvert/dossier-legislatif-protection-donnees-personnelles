<!-- TITLE: Amendement n° 51 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-11/amendement-126" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE 11](/pjl-592/titre-ii/chapitre-iv/article-11)

# Titre

À la fin de l’alinéa 2, supprimer les mots :

« sous le contrôle de l’autorité publique ou ».

# Exposé sommaire

Si nous ne sommes pas opposés à que les citoyens, les personnes physiques et morales concernées / requérantes, et certains organismes privés spécifiques (“personnes morales de droit privé collaborant au service public de la justice, et appartenant à des catégories dont la liste est fixée par décret en Conseil d’État pris après avis de la Commission nationale de l’informatique et des libertés, dans la mesure strictement nécessaire à leur mission”) puissent mettre en place des traitements de données à caractère personnel relatives aux infractions, condamnations et mesures de sûreté, nous estimons toutefois que la mention “sous le contrôle de l’autorité publique” pose une réelle difficulté.

En effet, par cette loi de transposition, le Gouvernement impulse une libéralisation du “fichage” en facilitant l’accès aux données (plus de champs de données ouvertes), la création d’un traitement étant plus aisée (l’activité principale de la CNIL devenant la supervision plus que l’autorisation), et en ouvrant la possibilité à plus d’acteurs d’utiliser ces données. Tel que le rappelle notre Livret numérique, nous nous opposons au fichage de masse tel que proposé par ce projet de loi (https ://avenirencommun.fr/le-livret-numerique/).

Or, la rédaction actuelle de la loi de 1978, notamment son article 9, ne prévoyaient pas une mention sibylline telle que “sous le contrôle de l’autorité publique”.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-11/amendement-126" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

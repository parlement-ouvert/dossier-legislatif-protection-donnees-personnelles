<!-- TITLE: Amendement n° 2 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-premier/amendement-41" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-premier/amendement-142" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Patrick Hetzel**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE PREMIER](/pjl-592/titre-i/chapitre-i/article-1), insérer l'article suivant:

# Titre

La loi n° 2012‑410 du 27 mars 2012 relative à la protection de l’identité est ainsi modifiée :

1° L’article 3 est ainsi rédigé :

« _Art. 3_. – Est soumis au consentement explicite et préalable du titulaire, tel que défini au dernier alinéa de l’article 2 de la directive 95/46/CE du Parlement européen et du Conseil du 24 octobre 1995 relative à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données, l’accès ou l’utilisation, à des fins autres que l’authentification de l’état civil, de tout ou partie des éléments biométriques mentionnés au 5° de l’article 2 issus de la carte nationale d’identité ou du passeport électronique.

« Le recueil du consentement explicite et préalable s’impose à toute personne souhaitant collecter, accéder ou utiliser un identifiant biométrique relatif à un ressortissant français, sauf disposition législative contraire, et sans préjudice du respect des dispositions énoncées au 8° du I de l’article 25 de la loi n° 78‑17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.

« Les dispositions du présent article sont d’ordre public international. »

2° Après le mot : « pris », la fin de l’article 8 est ainsi rédigée : « dans les conditions prévues par la loi n° 78‑17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés fixe les modalités d’application de la présente loi et précise les modalités de collecte et de conservation des données et des éléments d’identification biométriques relatifs au titulaire d’une carte nationale d’identité ou d’un passeport électronique. »

# Exposé sommaire

Cet amendement avait d’abord été déposé lors de l’examen du projet de loi pour un État au service d’une société de confiance

Le ministre Darmanin a indiqué qu’il avait sa place dans le projet de loi données personnelles.

Il est devenu indispensable de pouvoir attester de l’authenticité de son identité dans le cadre des technologies existantes. À cette fin, le recours à la biométrie fait l’objet d’un encadrement strict. Cette nécessité est conjointe aux pouvoirs publics et aux entreprises privées.

Pour autant, cet encadrement est plus exigeant pour l’État qu’il ne l’est pour une société commerciale. Cette situation est préjudiciable à l’État qui ne peut pleinement assumer ses obligations d’ordre public. Elle est également préjudiciable aux individus dont les données biométriques sont désormais à la libre disposition d’entreprises privées.

Ce déséquilibre de fait résulte de la décision n° 2012‑652 DC du Conseil constitutionnel du 22 mars 2012 qui, tout en reconnaissant la possibilité pour l’État d’instaurer une carte d’identité et un passeport électroniques, a limité pour celui-ci la collecte d’informations biométriques aux seules fins d’authentification de l’état civil et lui a interdit de permettre leur utilisation à d’autres fins, telles que la création d’un fichier à des fins judicaires ou la sécurisation des transactions commerciales.

Il s’agissait pour le Conseil constitutionnel de garantir les libertés fondamentales de nos concitoyens qui auraient pu être menacées par la création de tels dispositifs s’imposant à eux.

Paradoxalement, si nos concitoyens sont protégés par le juge constitutionnel des dérives potentielles de l’État, ils ne le sont pas des mêmes risques lorsqu’il s’agit d’une entreprise commerciale.

En effet, ces sociétés commerciales sont en mesure de collecter des informations biométriques dont l’utilisation n’est en réalité soumise qu’au contrat liant ces sociétés à leurs clients. Ces sociétés et leurs contrats échappent d’ailleurs le plus souvent aux prescriptions constitutionnelles ou réglementaires françaises, pour motif d’extraterritorialité.

Ce déséquilibre n’a pas lieu d’être.

D’une part, l’État n’a pas à se priver des instruments d’authentification de l’identité disponibles pour les entreprises commerciales, sauf à le rendre dépendant à terme de ces dernières pour accomplir ses missions régaliennes. D’autre part, les individus doivent être mis en mesure de consentir, en dehors des motifs d’ordre public, que leurs données biométriques soient traitées par l’État ou par des entreprises commerciales.

C’est pourquoi cet amendement rétablit la création d’une carte nationale d’identité électronique et d’un passeport électronique utilisant des procédés d’identification biométriques. Le dispositif tient compte pour cela des prescriptions du Conseil constitutionnel énoncées lors de l’adoption de la loi n° 2012‑410 du 27 mars 2012 relative à la protection de l’identité.

Pour protéger les citoyens de l’utilisation abusive des données biométriques, il est ainsi proposé de subordonner à l’accord explicite, libre, informé et spécifique de son titulaire, l’emploi, à des fins autres que la lecture d’identité, des données contenues dans le titre électronique.

Il offre par ailleurs des garanties en termes de libertés fondamentales et d’application territoriale de la loi en soumettant les entreprises commerciales recourant à la biométrie au consentement explicite, libre, informé et spécifique de la personne et à la soumission d’une autorisation préalable de la CNIL dans des conditions conformes aux dispositions actuelles de la loi n° 78‑17 « Informatiques &amp; Libertés ».

Il établit enfin le caractère d’ordre public international de ces dispositions afin d’offrir aux citoyens français la protection de leurs données biométriques dans le cadre des conditions contractuelles soumises à une loi étrangère ou dans le cadre de la justice internationale.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-premier/amendement-41" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-premier/amendement-142" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 142 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-premier/amendement-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-1-bis" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Caroline Janvier**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE PREMIER](/pjl-592/titre-i/chapitre-i/article-1), insérer l'article suivant:

# Titre

I. – Le Gouvernement remet au Parlement, dans un délai de vingt-quatre mois à compter de la promulgation de la présente loi, un rapport relatif à sa mise en application.

II. – Ce rapport évalue en particulier l’impact, pour les très petites entreprises, les petites et moyennes entreprises et les entreprises de taille intermédiaire, des nouvelles prérogatives de la Commission nationale de l’informatique et des libertés et de leur mise à niveau au regard des nouvelles dispositions que contient la présente loi.

# Exposé sommaire

Cet amendement vise à accompagner la mise en application des dispositions de la présente loi, en particulier auprès des petites et moyennes entreprises.

La digitalisation est, pour notre pays et pour nos entreprises, devenue une urgence absolue. Selon des récentes études, l’état actuelle de la transformation numérique de nos entreprises constitue une réelle faiblesse pour notre économie. Une étude réalisée en avril-mai 2017 par OpinionWay conclut que seules deux entreprises françaises de type PME-ETI sur dix déclarent être bien engagées dans cette transformation.

Le rôle d’accompagnateur de la CNIL, qui est à raison facilité par ce texte, va prendre dans ce cadre une certaine importance. En effet, au-delà de l’aspect technique de la digitalisation, c’est un ensemble de processus de certification qu’il va falloir penser, ainsi qu’un travail culturel auprès des entrepreneurs qu’il faudra opérer.

Ce rapport d’information représentera un outil utile, qui permettra au législateur d’entamer un premier travail d’évaluation de ces dispositions, face aux spécificités du tissu économique français, et de pouvoir prendre, au besoin, toute disposition complémentaire.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-premier/amendement-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-1-bis" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

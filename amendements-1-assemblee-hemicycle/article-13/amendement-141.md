<!-- TITLE: Amendement n° 141 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-30" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-95" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Caroline Janvier**

[ARTICLE 13](/pjl-592/titre-ii/chapitre-iv/article-13)

# Titre

Après le mot :

« transmises »,

rédiger ainsi la fin de l’alinéa 30 :

« doivent au préalable donner leur consentement, tel que défini par le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité. »

# Exposé sommaire

Cet amendement vise non pas à mettre un frein à l’utilisation et à la diffusion des données aux fins de recherche ou d’innovation, mais concernant les données de santé, de renforcer la notion de consentement telle qu’elle ressort du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016.

Le règlement évoque en effet que : « Le consentement devrait être donné par un acte positif clair par lequel la personne concernée manifeste de façon libre, spécifique, éclairée et univoque son accord au traitement des données à caractère personnel la concernant, par exemple au moyen d’une déclaration écrite, y compris par voie électronique, ou d’une déclaration orale. Cela pourrait se faire notamment en cochant une case lors de la consultation d’un site internet, en optant pour certains paramètres techniques pour des services de la société de l’information ou au moyen d’une autre déclaration ou d’un autre comportement indiquant clairement dans ce contexte que la personne concernée accepte le traitement proposé de ses données à caractère personnel. Il ne saurait dès lors y avoir de consentement en cas de silence, de cases cochées par défaut ou d’inactivité. Le consentement donné devrait valoir pour toutes les activités de traitement ayant la ou les mêmes finalités. Lorsque le traitement a plusieurs finalités, le consentement devrait être donné pour l’ensemble d’entre elles. Si le consentement de la personne concernée est donné à la suite d’une demande introduite par voie électronique, cette demande doit être claire et concise et ne doit pas inutilement perturber l’utilisation du service pour lequel il est accordé ».

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-30" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-95" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 29 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-175" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-36" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Emmanuelle Ménard**

[ARTICLE 13](/pjl-592/titre-ii/chapitre-iv/article-13)

# Titre

Après le mot :

« santé »,

rédiger ainsi la fin de l’alinéa 24 :

« ne peuvent transmettre au responsable de traitement de données autorisé, sans le consentement des personnes concernées, des données à caractère personnel qu’ils détiennent, qu’en cas de menace grave et avérée à l’ordre public. Dans le cas contraire, ces données ne pourront être transmises qu’avec l’autorisation des personnes concernées. »

# Exposé sommaire

L’alinéa 24 de l’article 13 du présent projet de loi se réfère à l’article 54 de la loi n° 78‑17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés dispose « Les traitements de données à caractère personnel ayant une finalité d’intérêt public de recherche, d’étude ou d’évaluation dans le domaine de la santé sont autorisés par la Commission nationale de l’informatique et des libertés, dans le respect des principes définis par la présente loi et en fonction de l’intérêt public que la recherche, l’étude ou l’évaluation présente. »

Se référer à cet article pour transmettre des informations qui, normalement, sont protégées par le secret professionnel est dangereux et peut porter gravement atteinte à la vie privée du patient.

En effet, l’expression « Les traitements de données à caractère personnel ayant une finalité d’intérêt public de recherche, d’étude ou d’évaluation dans le domaine de la santé » est tendancieuse car nul ne sait à quelle réalité elle se réfère.

Doit-on par exemple considérer que les données médicales référençant les bébés atteints de la trisomie 21 et de leur maman (recensement organisé par un décret publié le 5 mai 2016) font partie ces données qu’il faudrait transmettre ? Pour quel motif ?

Parce qu’une personne est la seule propriétaire de ses données personnelles, il convient, surtout lorsqu’il s’agit de données médicales, de lui demander son autorisation avant de pouvoir les utiliser.

L’objectif ici n’est pas d’entraver la recherche, mais d’associer librement les personnes concernées à cette recherche.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-175" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-36" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

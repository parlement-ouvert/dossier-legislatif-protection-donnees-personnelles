<!-- TITLE: Amendement n° 128 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-68" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-148" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Amendement du Gouvernement

[ARTICLE 13](/pjl-592/titre-ii/chapitre-iv/article-13)

# Titre

Rédiger ainsi l’alinéa 9 :

« 3° Les traitements mis en œuvre aux fins d’assurer le service des prestations ou le contrôle par les organismes chargés de la gestion d’un régime de base d’assurance maladie, ainsi que la prise en charge des prestations par les organismes d’assurance maladie complémentaire. »

# Exposé sommaire

Pour l’exécution de leurs engagements contractuels envers leurs assurés, et le service des prestations prévues, les organismes d’assurance maladie complémentaire utilisent des données issues de la facturation des soins notamment par les différents professionnels de santé, ou des données qui leur sont transmises par les régimes d’assurance maladie obligatoire à l’issue de leurs opérations de remboursement.

Ces données sont nécessaires à la liquidation des prestations garanties par les contrats d’assurance maladie complémentaire, pour lesquels les assurés ont été informés de leurs droits en matière de protection des données personnelles.

Ces traitements de données entrent dans le champ de l’article 9 du Règlement Général sur la Protection des Données (RGPD) permettant le traitement de données de santé lorsque le traitement est nécessaire aux fins d’exécution des obligations en matière de protection sociale.

Pour les traitements de l’assurance maladie obligatoire, l’article 53 de la loi du 6 janvier 1978 prévoit qu’ils sont exclus de l’application du chapitre IX. Aussi, compte tenu de la similitude des traitements de l’assurance maladie complémentaire mis en œuvre pour la prise en charge des prestations, le présent amendement les aligne sur les règles applicables à ceux de l’assurance maladie obligatoire.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-68" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-148" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 148 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-128" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-125" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Gosselin**

[ARTICLE 13](/pjl-592/titre-ii/chapitre-iv/article-13)

# Titre

Après l’alinéa 11, insérer l'alinéa suivant :

« Les responsables de traitements relevant du 1° du II de l’article 8, valablement mis en œuvre antérieurement à la date d’entrée en application du règlement UE n°2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité ne sont pas tenus, pour ces traitements, de recueillir de nouveau le consentement des personnes concernées dans les conditions définies à l’article 7 du même règlement, ni de fournir les informations mentionnées à l’article 12 du même règlement, dans les conditions prévues au même article 12 pour poursuivre la mise en œuvre de leur traitement. »

# Exposé sommaire

Le RGPD stipule qu’un traitement fondé sur un consentement donné en vertu de la directive 95/46/CE (considérant 171) n’a pas besoin d’être recueilli de nouveau s’il a été donné de manière conforme aux conditions du RGPD.

Mais, concernant les entreprises dans le domaine de la santé, les consentements recueillis l’ont été sur le fondement d’informations délivrées selon les termes de la précédente directive (dont le champ a été étendu par le RGPD). Ainsi il est nécessaire de savoir si ces dernières auront besoin de recueillir à nouveau le consentement de chaque personne.

Cet amendement permet d’ôter tout doute en admettant que les consentements déjà donnés ne devront pas être recueillis de nouveau.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-128" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-125" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

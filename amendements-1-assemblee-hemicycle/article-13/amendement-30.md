<!-- TITLE: Amendement n° 30 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-36" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-141" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Emmanuelle Ménard**

[ARTICLE 13](/pjl-592/titre-ii/chapitre-iv/article-13)

# Titre

Rédiger ainsi l’alinéa 28 :

« _Art. 57_. – Toute personne, dont les données à caractère personnel et relevant du secret professionnel et qui sont susceptibles d’intéresser la recherche scientifique et l’intérêt général, doit donner son accord explicite pour que ces données soient utilisées à ces fins. »

# Exposé sommaire

La rédaction initiale de l’article 57 est tendancieuse car on ne sait si la personne, dont les données à caractère personnel couvertes normalement par le secret professionnel, a le droit de s’opposer a priori (avant que le préjudice soit commis) ou a posteriori (après que le préjudice soit commis) au transfert de ces données.

Parce que les données personnelles sont la propriété des personnes, il convient de s’assurer qu’elles ont consenties en amont à la transmission de ces informations.

C’est pourquoi il semble davantage pertinent de demander explicitement à toute personne si ces informations peuvent être utilisées à des fins de recherches scientifiques ou au nom de l’intérêt général, au lieu de consacrer un droit d’opposabilité imprécis.

Tel est le sens de cet amendement.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-36" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-141" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

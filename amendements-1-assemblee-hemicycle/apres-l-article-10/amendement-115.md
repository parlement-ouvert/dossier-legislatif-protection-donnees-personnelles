<!-- TITLE: Amendement n° 115 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-10/amendement-114" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-10/amendement-116" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Stéphane Peu**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 10](/pjl-592/titre-ii/chapitre-iii/article-10), insérer l'article suivant:

# Titre

L’article L. 121-4 du code de la consommation est complété par un 23° ainsi rédigé :

« 23° De collecter des données personnelles lors d’une connexion sur les réseaux de communications électroniques en vue d’augmenter artificiellement les prix d’un service ou d’une prestation en ligne à l’occasion d’une connexion ultérieure. »

# Exposé sommaire

Le présent amendement entend supprimer l’ « IP tracking », pratique utilisée par certains sites Internet d’e-commerce, notamment de vente de billets de train ou d’avion, augmentant les tarifs entre plusieurs recherches et qui pousse in fine l’internaute à réaliser l’achat.

Depuis 2013, la Commission européenne considère l’adresse IP comme une « donnée personnelle ».

La CNIL et la DGCCRF ont mené une opération conjointe de contrôle des sites de sociétés françaises d’e-commerce sur les pratiques de modulation des prix de vente, notamment en matière de transport. Cette opération a mis en lumière qu’ « aucune des techniques observées ne prend en compte l’adresse IP des internautes comme élément déterminant ou ne vise à moduler le prix des produits ou services proposés aux consommateurs ».

Cette technique n’est certes pas « déterminante » mais elle peut être utilisée. Le présent amendement entend donc l’interdire.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-10/amendement-114" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-10/amendement-116" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

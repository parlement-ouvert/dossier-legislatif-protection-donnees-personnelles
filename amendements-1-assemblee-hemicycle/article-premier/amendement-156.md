<!-- TITLE: Amendement n° 156 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-40" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Rémy Rebeyrotte et les membres du groupe La République en Marche**

[ARTICLE PREMIER](/pjl-592/titre-i/chapitre-i/article-1)

# Titre

Après l’alinéa 17, insérer les deux alinéas suivants :

_h)_ Il est ajouté un _j_ ainsi r édigé :

« _j)_ Elle m ène des actions de sensibilisation auprès des médiateurs de la consommation et des médiateurs publics, au sens de l’article L. 611‑1 du code de la consommation, en vue de la bonne application des dispositions de la présente loi. »

# Exposé sommaire

Cet amendement vise à faciliter les actions de médiation qui peuvent opposer les consommateurs ou utilisateurs aux responsables de traitement ou sous-traitants. Les médiateurs publics ou sectoriels, tels qu’établis aux articles L 611 et L 612 du code de la consommation, sont compétents dans les litiges opposant consommateurs et professionnels dans des secteurs tels que l’énergie pour le Médiateur de l’énergie, et de nombreux champs sectoriels de la consommation.

Il convient que ces médiateurs puissent être formés et sensibilisés aux enjeux propres à la protection des données personnelles et aux nouveaux droits et devoirs inscrits dans le règlement (UE) 2016/679 du 27 avril 2016 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-40" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

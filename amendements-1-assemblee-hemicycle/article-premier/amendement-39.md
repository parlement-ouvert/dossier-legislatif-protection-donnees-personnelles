<!-- TITLE: Amendement n° 39 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-38" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

[ARTICLE PREMIER](/pjl-592/titre-i/chapitre-i/article-1)

# Titre

I. – À la première phrase de l’alinéa 11, après le mot :

« types »,

insérer le mot :

« notamment ».

II. – En conséquence, à la même phrase, après le mot :

« assurer »,

insérer les mots :

« la protection des données, à savoir par exemple le respect des droits notamment en matière d’ergonomie et d’accessibilité, leur finalité, la minimisation des données, ».

# Exposé sommaire

Dans son avis du 30 novembre 2017 ([https://www.legifrance.gouv.fr/affichCnil.do?oldAction=rechExpCnil&amp;id=CNILTEXT000036195647&amp;fastReqId=180931766&amp;fastPos=1](https://www.legifrance.gouv.fr/affichCnil.do?oldAction=rechExpCnil&amp;id=CNILTEXT000036195647&amp;fastReqId=180931766&amp;fastPos=1)) la CNIL regrettait que également que l’objet des règlements-types prévus à l’article 1 du projet de loi, soit limité à la seule dimension de la sécurité des systèmes. En effet, d’autres dimensions de la protection des données (finalité, minimisation des données, respect des droits, etc.) devraient pouvoir faire l’objet d’un encadrement par des règles de fond, et non seulement par des règles de sécurité, (ce alors même que le Règlement RGPD permet une telle marge de manoeuvre, dans son article 9.4).

En précisant que les règlements types peuvent ne pas uniquement concerner la seule sécurité des systèmes de traitement de données, mais être relatifs à d’autres éléments fondamentaux, tels le respect des droits et libertés, la CNIL peut ainsi jouer un rôle plus important pour cadrer la création de nombreux traitements de données.

En voulant restreindre la CNIL à la seule possibilité d’édicter des règlements types pour la sécurité de systèmes d’information, le Gouvernement nie tout le rôle de protection des droits et l’importance des enjeux en termes de régulation pour les traitements de données personnelles. Cet amendement permet de réparer cet impair.

Ainsi, la CNIL pourra désormais par exemple édicter des règlements types sur le respect au droit de la vie privée (comme par exemple prévoir différents types d’accès pour que tous les utilisateurs du traitement n’aient accès à des données non nécessaires) sur la minimisation (pour que le responsable de traitement ne collecte pas plus de données qu’il n’en a besoin), sur leur mise à jour (pour éviter par exemple que des données erronées ou devant être effacées soient toujours présentes -  fichier d'antécédents du ministère de l'intérieur (TAJ) ou STIC (Système de traitement des infractions constatées), etc…

Nous estimons en outre qu’il existe actuellement un réel manque de cadrage pour ce qui concerne l’ergonomie garantissant la clarté et l'accessibilité (que ce soit pour certains publics - affectés par des handicaps physiques ou mentaux, personnes âgées personnes connaissant des baisses d’acuité visuelles -,  tant que de supports (tablette, ordinateur, téléphone, etc…)

NB : Dans sa rédaction et dans son esprit, cet amendement ne contrevient en aucun cas à l’article 40 de la Constitution et ne crée aucune charge supplémentaire en ce qu’il n’entend pas créer une compétence nouvelle ou une nouvelle mission pour la CNIL (au sens de la page 84 du Rapport Carrez de 2017, http://www.assemblee-nationale.fr/14/pdf/rap-info/i4546.pdf), étant donné que cette compétence pour la CNIL de recourir de pouvoir édicter des règlements types est déjà prévue par le projet de loi du Gouvernement. Par cet amendement, nous ne faisons que préciser le champ de ces règlements types.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-38" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

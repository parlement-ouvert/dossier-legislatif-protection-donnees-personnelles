<!-- TITLE: Amendement n° 179 -->
<!-- SUBTITLE: Tombé -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-102" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Christine Hennion**

[ARTICLE PREMIER](/pjl-592/titre-i/chapitre-i/article-1)

# Titre

Compléter l'alinéa 6 par les mots :

« , en tenant compte, le cas échéant, du caractère expérimental du traitement. »

# Exposé sommaire

Cet amendement vise à atténuer les contraintes au développement économiques des petites et moyennes entreprises innovantes, ainsi que des start-up. Alors que celles-ci n’ont souvent pas les moyens de procéder à une analyse des risques et de l’impact que leur activité aura sur les données personnelles qu’elles vont traiter, étant donné le caractère expérimental du traitement, la CNIL doit prendre en compte ces difficultés, notamment dans l’évaluation des analyses d’impact effectuées par ces responsables de traitement.

Un tel dispositif pourrait être rapproché du « bac à sable » réglementaire, tel que le permet l’Autorité de régulation des communications électroniques et des postes (ARCEP). En effet, l’ARCEP peut, en vertu des articles L 42‑1 et L 44 du code des postes et des communications électroniques, attribuer des autorisations d’utilisation de fréquences ou des ressources de numérotation et des codes à des fins expérimentales, en vertu desquelles le titulaire n’est pas soumis à tout ou partie des droits et obligations attachés à l’attribution de ces ressources ou à l’exercice de l’activité d’opérateur de communications électroniques.

Ce système pourrait s’appliquer aux obligations en matière de protection des données personnelles, dès lors que le dispositif est strictement encadré et que la dérogation ne puisse s’entendre que de manière temporaire.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-102" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

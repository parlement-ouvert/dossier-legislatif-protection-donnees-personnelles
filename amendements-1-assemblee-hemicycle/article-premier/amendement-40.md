<!-- TITLE: Amendement n° 40 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-156" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-104" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE PREMIER](/pjl-592/titre-i/chapitre-i/article-1)

# Titre

À l’alinéa 18, après la première occurrence du mot :

« Sénat »,

insérer les mots :

« , par tout député ou sénateur, ainsi que toute association agréée dans des conditions fixées par décret en Conseil d’État ».

# Exposé sommaire

Si la CNIL est actuellement une instance uniquement saisie automatiquement sur les projets de loi (textes présentés par le pouvoir exécutif - article 11 de la loi de 1978 modifiée), nous sommes d’accord avec ce projet de loi qui prévoit une saisine de la CNIL sur les propositions de loi. En effet, à de trop nombreuses occasions, le Gouvernement peut décider de court-circuiter la CNIL en faisant passer des textes devant normalement être soumis à son avis en les transformant en “proposition de loi” déposée par un parlementaire docile et obédient de sa “Majorité” (sur le même modèles des “études d’impact” qui ne sont pas nécessaires pour les propositions de loi …).

Pour éviter que des textes législatifs qui affectent les droits et libertés numériques soient adoptés dans la hâte et sans réelle expertise technique, il nous est apparu indispensable de créer ce “garde fou” parlementaire et citoyen qui permet à tout parlementaire ou association agréée de pouvoir, dans les mêmes conditions que le président de l’Assemblée nationale et du Sénat, saisir la CNIL pour avis sur une proposition de loi.

NB : Dans sa rédaction et dans son esprit, cet amendement ne contrevient en aucun cas à l’article 40 de la Constitution et ne crée aucune charge supplémentaire en ce qu’il n’entend pas créer une compétence nouvelle ou une nouvelle mission pour la CNIL (au sens de la page 84 du Rapport Carrez de 2017, <http://www.assemblee-nationale.fr/14/pdf/rap-info/i4546.pdf>), étant donné que cette possibilité de saisir la CNIL pour avis sur une proposition de loi est déjà prévue par le projet de loi du Gouvernement. Par cet amendement, nous ne faisons que préciser les personnes pouvant saisir la CNIL pour avis sur une proposition de loi.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-156" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-104" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

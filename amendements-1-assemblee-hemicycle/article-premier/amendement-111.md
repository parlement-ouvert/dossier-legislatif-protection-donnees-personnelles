<!-- TITLE: Amendement n° 111 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-38" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-181" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Stéphane Peu**

[ARTICLE PREMIER](/pjl-592/titre-i/chapitre-i/article-1)

# Titre

À la seconde phrase de l’alinéa 11, supprimer les mots :

« sauf pour les traitements mis en œuvre pour le compte de l’État, agissant dans l’exercice de ses prérogatives de puissance publique, ».

# Exposé sommaire

L’alinéa 11 prévoit que la CNIL établit et publie des règlements types en vue d’assurer, d’une part, la sécurité des systèmes de traitement de données à caractère personnel et, d’autre part, de régir les traitements de données de santé.

À ce titre, elle peut prescrire des mesures techniques et organisationnelles supplémentaires pour le traitement des données biométriques, génétiques et de santé conformément à l’article 9 du règlement européen, sauf pour les traitements mis en œuvre pour le compte de l’État agissant dans l’exercice de ses prérogatives de puissance publique.

Le présent amendement vise à supprimer cette exception au regard de la nécessité d’encadrer strictement le traitement des données sensibles.

La CNIL s’est exprimée en ce sens dans son avis du 30 novembre 2017.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-38" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-181" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

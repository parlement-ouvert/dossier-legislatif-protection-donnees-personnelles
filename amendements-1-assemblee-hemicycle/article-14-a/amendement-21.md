<!-- TITLE: Amendement n° 21 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-69" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Christine Hennion**

[ARTICLE 14](/pjl-592/titre-ii/chapitre-v/article-14) A

# Titre

I. – À l’alinéa 2, substituer au mot :

« quinze »

le mot :

« treize ».

II. – En conséquence, procéder à la même substitution à l’alinéa 3.

# Exposé sommaire

Le règlement européen sur la protection des données fixe à 16 ans l’âge à partir duquel un mineur peut consentir seul au traitement de ses données. En dessous de cet âge, le consentement de ses parents sera nécessaire. Il autorise toutefois les États à abaisser ce seuil à 13, 14 ou 15 ans, marge de manœuvre qui a été largement utilisé par nos voisins.

Cette situation ne prend pas en compte la réalité de l’accès au numérique des enfants.

3 enfants de 13 ans sur 4 sont équipés de téléphone portable et surfent sur internet tous les jours. 2 sur 3 ont un compte Facebook. L’accès aux numériques de cette catégorie d’âge est une réalité que l’on ne peut ignorer.

L’idée générale sous-jacente derrière cette prise de position est qu’elle assure un équilibre raisonnable entre le droit à la protection des données et le droit à la liberté d’information et d’expression.

L’âge de 13 ans se justifie par de nombreux travaux préparatoires :

La Convention sur les droits de l’enfant de 1989 est longuement citée et notamment son article 13 : « l’enfant a droit à la liberté d’expression ».

Les travaux préparatoires de la Commission, le premier avis du Parlement européen et l’avis du Contrôleur européen de la protection des données s’alignaient tous sur l’âge de 13 ans.

Des questions persistent sur l’authenticité de l’autorisation des parents et il est certain que les enfants pourraient aisément la contourner.

Ensuite, les autorités de contrôle, tout comme les prestataires techniques et les responsables de traitement, ne disposent pas de moyens, dans l’état actuel des avancées technologiques, pour vérifier que des mineurs de moins de seize ans n’usurpent pas le consentement au traitement de leurs données personnelles, auquel seules peuvent normalement consentir les personnes titulaires de l’autorité parentale, sauf à demander une photocopie de la carte d’identité ou de tout autre document susceptible de prouver l’âge de l’internaute, pratique impossible à mettre en œuvre.

De plus et du fait de la divergence des choix entre les différentes États membres, les responsables de traitement et les sous-traitants devront en effet, pour cette matière comme pour d’autres, rester attentif aux différentes normes nationales et endurer le coût d’information engendré par cette situation.

Nos petites et moyennes entreprises seront les plus impactées.

Enfin, une limite d’âge à treize ans obligerait ainsi les pouvoirs publics à mettre en œuvre de nombreuses actions de formation et d’éducation à la protection des données dès le plus jeune âge. Il est très envisageable qu’une amélioration globale des compétences en résulte.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-69" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

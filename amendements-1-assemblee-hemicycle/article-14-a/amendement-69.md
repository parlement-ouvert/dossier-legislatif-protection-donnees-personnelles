<!-- TITLE: Amendement n° 69 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-35" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-21" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE 14](/pjl-592/titre-ii/chapitre-v/article-14) A

# Titre

I. – À la fin de l’alinéa 2, substituer au mot :

« quinze »

le mot :

« treize ».

II. – En conséquence, à l’alinéa 3, procéder à la même substitution.

III – En conséquence, substituer à l’alinéa 4 les deux alinéas suivants :

« Le responsable de traitement ne peut obtenir le consentement du mineur de treize ans ou plus que par l’intermédiaire d’une information et d’une communication relatives à ce traitement qui soient présentée sur un support et en des termes, clairs et simples adaptés à l’âge minimum de ce mineur, qui puissent objectivement garantir que ce consentement n’a pas été obtenu indûment.

« Le fait de ne pas avoir présenté à un mineur de treize ans ou plus une information et une communication relatives au traitement, et le fait d’avoir indûment obtenu le consentement d’un mineur de treize ans ou plus , dans les conditions prévues à l’alinéa précédent, peuvent être sanctionnés par une amende administrative ne pouvant excéder 10 millions d’euros ou, s’agissant d’une entreprise, 2 % du chiffre d’affaires annuel mondial total de l’exercice précédent ».

# Exposé sommaire

Par cet amendement, nous souhaitons, dans la lignée des recommandations de la Commission nationale consultative des droits de l'homme, arrêter l'hypocrisie consistant à nier que la majorité numérique s'établit de fait - de par les comportements observés par études - au moins à 13 ans (seuil minimal prévu par la directive ici transposée).

En effet, porter une majorité numérique à 15 ou 16 ans sans que celle-ci n'ait à réellement s'appliquer dans les faits relève ne nous apparaît pas souhaitable. Au contraire, nous estimons qu'il faut reconnaître la majorité numérique à 13 ans tout en la complétant par les protections adéquates :

\- Tout d'abord, et c'est le sens de notre amendement n°XXX, nous estimons que la CNIL doit se voir consacrer une mission d'information du public, en particulier des élèves de l’enseignement des premiers, deuxième degrés et de l’enseignement supérieur, quant aux enjeux du numérique sur leurs droits et libertés fondamentales et les moyens de faire pleinement valoir ceux-ci. Ceci permettra au moins de s'assurer qu'il y a une éducation aux risques associés aux traitements de données personnelles et à un consentement numérique trop vite accordé ;

\- Ensuite, nous estimons qu'en imposant une obligation ferme au responsable du traitement d'informer dûment le mineur concerné par une explication adaptée(en prenant en compte son âge - de 13 à 18 ans), et d'utiliser à cet effet le support adéquat (texte, photo, audio, vidéo, etc...), et en doublant celles-ci de sanctions importantes, des garde fous suffisant sont posés.

C'est uniquement avec un tel arsenal de prévention et de sanction que nous pourrons garantir que c'est bien le consentement éclairé des mineurs de 13 qui est obtenu s'il décide de mettre à disposition d'un responsable de traitement ses données personnelles.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-35" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-21" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

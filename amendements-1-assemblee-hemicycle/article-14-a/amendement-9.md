<!-- TITLE: Amendement n° 9 -->
<!-- SUBTITLE: Tombé -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-182" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Pierre Vatin**

[ARTICLE 14](/pjl-592/titre-ii/chapitre-v/article-14) A

# Titre

I. – À l’alinéa 3, supprimer les mots :

« âgé de moins de quinze ans ».

II. En conséquence, après le même alinéa, insérer l’alinéa suivant :

« Les modalités du recueil de l’autorisation mentionnée à l'alinéa précédent sont fixées par décret en Conseil d’État » .

# Exposé sommaire

Cet amendement vise à établir l’âge de consentement de mineur à un traitement de données à caractère personnel en ce qui concerne l’offre directe de services de la société d’information à dix-huit ans. Ceci est nécessaire afin d’harmoniser la législation existante et d’éviter de multiples réglementations qui prévoient des majorités à des âges très variables selon les sujets et qui peuvent être sources de confusions, voire de conflits.

De plus apprentissage de la recherche n’est pas accessible à l’ensemble de la jeunesse, on ne peut que le constater chaque jour, et cette restriction de majorité à dix-huit ans vise notamment à protéger les mineurs les plus vulnérables. L’âge seul n’est pas un gage de maturité.

Cet amendement vise également à spécifier que les modalités du recueil de l’autorisation du consentement sont fixées par décret en Conseil d’État.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-182" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 16 A -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-15/amendement-71" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-82" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 82 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-82)
* [Amendement n° 164 — Adopté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-164)
* [Amendement n° 87 — Adopté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-87)
* [Amendement n° 165 — Adopté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-165)
* [Amendement n° 70 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-70)
* [Amendement n° 73 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-73)
* [Amendement n° 109 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-109)
* [Amendement n° 150 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-150)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-15/amendement-71" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-82" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

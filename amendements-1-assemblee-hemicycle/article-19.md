<!-- TITLE: Article 19 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-17/amendement-173" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-19/amendement-57" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 57 — Rejeté](/amendements-1-assemblee-hemicycle/article-19/amendement-57)
* [Amendement n° 13 — Adopté](/amendements-1-assemblee-hemicycle/article-19/amendement-13)
* [Amendement n° 14 — Adopté](/amendements-1-assemblee-hemicycle/article-19/amendement-14)
* [Amendement n° 138 — Non soutenu](/amendements-1-assemblee-hemicycle/article-19/amendement-138)
* [Amendement n° 15 — Adopté](/amendements-1-assemblee-hemicycle/article-19/amendement-15)
* [Amendement n° 140 — Non soutenu](/amendements-1-assemblee-hemicycle/article-19/amendement-140)
* [Amendement n° 143 — Non soutenu](/amendements-1-assemblee-hemicycle/article-19/amendement-143)
* [Amendement n° 145 — Non soutenu](/amendements-1-assemblee-hemicycle/article-19/amendement-145)
* [Amendement n° 16 — Adopté](/amendements-1-assemblee-hemicycle/article-19/amendement-16)
* [Amendement n° 17 — Adopté](/amendements-1-assemblee-hemicycle/article-19/amendement-17)
* [Amendement n° 18 — Adopté](/amendements-1-assemblee-hemicycle/article-19/amendement-18)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-17/amendement-173" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-19/amendement-57" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

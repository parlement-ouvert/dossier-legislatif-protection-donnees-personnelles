<!-- TITLE: Amendement n° 82 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-16-a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-164" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Jean-Louis Masson**

[ARTICLE 16](/pjl-592/titre-ii/chapitre-vi/article-16) A

# Titre

Supprimer cet article.

# Exposé sommaire

Il est proposé de circonscrire l’action de groupe en matière de données personnelle à la seule cessation d’un manquement, tel que prévu dans la loi n° 2016-1547 du 18 novembre 2016 de modernisation de la justice du XXIe siècle.

En effet, l’article 43 ter de la loi Informatique et Libertés prévoit la possibilité, pour les associations ad hoc, les associations de consommateurs agréées et les organisations syndicales de salariés ou de fonctionnaires, d’exercer une action de groupe, permettant la cessation d’un manquement.

Or, lors de l’examen en commission des Lois, le champ de l’action de groupe relative aux données personnelles a été étendu, afin de permettre la réparation du dommage causé.

Toutefois, comme le spécifie l’étude d’impact, l’introduction de l’action de groupe dans le droit national en matière de données personnelles est encore très récente et ne permet pas à ce stade un recul et une évaluation suffisants de sa mise en application. De plus, un tel dispositif constituerait une surenchère législative par rapport aux normes européennes, non seulement dans le temps mais surtout sur le fond, les actions de groupe n’étant pas rendues obligatoires par le règlement européen.

Par ailleurs, l’extension du champ de l’action de groupe à la réparation des préjudices ferait peser un risque financier important et prématuré pour les entreprises françaises, qui réalisent actuellement d’importants efforts de mise en conformité avec les obligations inhérentes au RGPD.

De plus, une telle disposition serait contraire aux engagements pris par le Gouvernement d'aller au-delà de ces obligations communautaires, surtout que Mme Věra Jourova, Commissaire européenne en charge de la Justice et des consommateurs, a annoncé l'adoption en mars prochain d'une proposition de directive sur les recours collectifs.

Enfin, à l’heure où la France cherche à attirer de nombreuses entreprises et investisseurs étrangers, l’extension prématurée d’un tel dispositif pourrait venir en limiter l’attractivité. Ainsi, sont créées des conditions de concurrence défavorables aux entreprises nationales vis-à-vis de leurs concurrentes européennes, qui sont en contradiction avec les préconisations du Conseil de la simplification.

Dès lors, pour toutes ces raisons, il convient de circonscrire à ce stade l’action de groupe à la seule cessation du manquement, comme actuellement prévu par la loi Informatique et Libertés, et donc de ne pas l’étendre à la réparation du préjudice.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-16-a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-164" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

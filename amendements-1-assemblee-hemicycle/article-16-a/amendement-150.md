<!-- TITLE: Amendement n° 150 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-109" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Gosselin**

[ARTICLE 16](/pjl-592/titre-ii/chapitre-vi/article-16) A

# Titre

Compléter cet article par les six alinéas suivants :

« 3° Il est complété par un V ainsi rédigé :

« V. – Au moins deux personnes mentionnées au II peuvent agir directement en justice sans l’intervention des associations, ou à la place des associations mentionnées au IV, dans l’un des cas suivants :

« 1° Il n’existe pas d’association compétente ou ayant un intérêt à agir ;

« 2° L’association reste inactive et n’agit pas en justice même quinze jours après mise en demeure par les usagers susvisés ;

« 3° L’association est dans l’impossibilité d’agir ou de continuer son action en justice ;

« 4° L’association est dans une situation de conflit d’intérêts ou de risque de ce conflit. »

# Exposé sommaire

L’article 43 _ter_ de la loi du 6 janvier 1978 dite  « informatique et libertés » prévoit la possibilité de mener une action de groupe quand plusieurs personnes physiques sont victimes d’un dommage ayant pour cause commune un manquement de même nature aux dispositions de la présente loi par un responsable de traitement de données à caractère personnel ou un sous-traitant.

Mais cet article ne prévoit pas les différents cas où il n’existe pas d’association agréée ou qu’elle n’agisse pas ou est dans l’incapacité de le faire.

Cet amendement y remédie et permet à une personne qui n’est pas représentée de pouvoir l’être par un avocat dans 4 situations.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-109" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

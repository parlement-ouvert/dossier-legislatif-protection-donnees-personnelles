<!-- TITLE: Amendement n° 109 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-73" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-150" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Stéphane Peu**

[ARTICLE 16](/pjl-592/titre-ii/chapitre-vi/article-16) A

# Titre

Compléter cet article par les six alinéas suivants :

« 3° Il est complété par un V ainsi rédigé :

« V. – Au moins deux personnes mentionnées au II peuvent agir directement en justice sans l’intervention des associations, ou à la place des associations mentionnées au IV, dans l’un des cas suivants :

« 1° Il n’existe pas d’association compétente ou ayant un intérêt à agir ;

« 2° L’association reste inactive et n’agit pas en justice même quinze jours après mise en demeure par les usagers susvisés ;

« 3° L’association est dans l’impossibilité d’agir ou de continuer son action en justice ;

« 4° L’association est dans une situation de conflit d’intérêts ou de risque de ce conflit. »

# Exposé sommaire

Cet amendement tend à modifier l’article 43 ter de la loi°78‑17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés qui prévoit la possibilité pour plusieurs personnes physiques victimes d’un manquement aux dispositions de cette loi, d’engager une action de groupe.

Cet article 43 ter de la loi du 6 janvier 1978 ne prévoit pas les cas où il n’existe pas d’association agrée et ceux où celle-ci est dans l’incapacité d’agir en justice. Cet amendement remédie à cette situation en couvrant les hypothèses où l’usager n’est pas représenté.

Dans les quatre cas cités dans l’amendement, un avocat doit pouvoir se substituer à cette carence afin d’assurer au justiciable la meilleure représentation possible.

L’avocat offre en effet aux justiciables des garanties qu’il est seul à apporter :

\- Les compétences professionnelles d’un expert : l’avocat suit une formation initiale approfondie complétée par une formation continue lui permettant une mise à jour de ses connaissances en matière de droit

\- La sécurité juridique : la signature de l’avocat assure à son acte une sécurité renforcée

\- Le respect de la déontologie : l’avocat exerce dans le cadre de principes éthiques et d’indépendance très stricts dont le respect est garanti par les Ordres

\- L’assurance responsabilité civile professionnelle : l’avocat souscrit une assurance obligatoire qui permet d’indemniser ses clients en cas de manquement de sa part.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-73" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-150" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

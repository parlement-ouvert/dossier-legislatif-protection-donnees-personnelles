<!-- TITLE: Amendement n° 73 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-70" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-109" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Latombe et les membres du groupe du Mouvement Démocrate et apparentés**

[ARTICLE 16](/pjl-592/titre-ii/chapitre-vi/article-16) A

# Titre

Compléter cet article par les six alinéas suivants :

« 3° Il est complété par un V ainsi rédigé :

« V. – Au moins deux personnes mentionnées au II peuvent agir directement en justice sans l’intervention des associations, ou à la place des associations mentionnées au IV, dans l’un des cas suivants :

« 1° Il n’existe pas d’association compétente ou ayant un intérêt à agir ;

« 2° L’association reste inactive et n’agit pas en justice même quinze jours après mise en demeure par les usagers susvisés ;

« 3° L’association est dans l’impossibilité d’agir ou de continuer son action en justice ;

« 4° L’association est dans une situation de conflit d’intérêts ou de risque de ce conflit. »

# Exposé sommaire

Cet amendement tend à modifier l’article 43 ter de la loi n°78‑17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés qui prévoit la possibilité pour plusieurs personnes physiques victimes d’un manquement aux dispositions de cette loi, d’engager une action de groupe.

Dans sa rédaction actuelle, la loi ne prévoit rien pour les cas où il n’existe pas d’association compétente, ni pour ceux où l’association est dans l’incapacité d’agir en justice. Cet amendement remédie à cette situation en couvrant les hypothèses où l’usager ne parvient pas à faire usage de son droit par le biais d’une association.

Ainsi, le présent amendement propose de créer la possibilité de recourir à un avocat, professionnel dont la compétence est reconnue et offrant des garanties utiles au justiciable, pour introduire une action de groupe en cas de manquement aux dispositions de la présente loi.

Cette possibilité ne serait ouverte qu’en dernier recours, lorsque les associations normalement compétentes ne sont pas en mesure de répondre aux demandes des citoyens s’estimant lésés.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-70" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16-a/amendement-109" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

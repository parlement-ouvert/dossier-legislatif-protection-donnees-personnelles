<!-- TITLE: Amendement n° 83 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Jean-Louis Masson**

[ARTICLE 17](/pjl-592/titre-ii/chapitre-vi/article-17)

# Titre

Supprimer cet article.

# Exposé sommaire

Cet article donne compétence à la CNIL pour saisir le Conseil d’Etat afin d’ordonner la suspension ou la cessation d’un transfert de données personnelles, le cas échéant sous astreinte, et de pouvoir assortir ses conclusions d’une question préjudicielle à la Cour de Justice de l’Union européenne (CJUE) en vue d’apprécier la validité de la décision d’adéquation de la Commission européenne sur laquelle est fondé le transfert de données en cause.

Or, cette possibilité de saisine va au-delà des pouvoirs conférés aux autorités de contrôle par l’article 58-2(j) du RGPD, ce qui pourrait avoir des conséquences catastrophiques pour les entreprises concernées par un telle interdiction, en particulier si la CJUE reconnaît finalement la décision d’adéquation comme étant valide.

Par ailleurs, il est important de rappeler que les transferts de données sont très fréquents, d’autant que la notion de transfert est définie très largement et ne couvre pas uniquement les cas où les données sont physiquement situées dans un serveur situé hors UE, mais également lorsque les données sont simplement accessibles ou téléchargeables sur un écran depuis un pays hors de l’UE.

Dès lors, cette possibilité de saisine, si elle venait à être retenue, créerait un aléa judiciaire important qui risquerait de mettre en péril certaines activités qui nécessitent pourtant des transferts de données hors UE.

Pour toutes ces raisons, il convient de supprimer cet article.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

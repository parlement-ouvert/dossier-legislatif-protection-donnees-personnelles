<!-- TITLE: Amendement n° 84 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-22" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-23" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Jean-Louis Masson**

APRÈS L'[ARTICLE 22](/pjl-592/titre-v/article-22), insérer la division et l'intitulé suivants:

# Titre

I. – Le chapitre VIII de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés est abrogé.

II. – La section 5 du chapitre VI du titre II du livre II du code pénal est abrogée.

# Exposé sommaire

Le dispositif pénal, en ce qu’il prévoit des sanctions pénales pour défaut d’accomplissement des formalités préalables notamment, semble être maintenu en l’état alors même que les formalités préalables de déclaration et d’autorisation sont supprimées.

Par ailleurs, les sanctions administratives ayant été considérablement augmentées, elles sont suffisamment dissuasives, d’autant plus que le cumul de sanctions administratives et de sanctions pénales produirait un régime de double sanction non conforme à la jurisprudence de la CEDH.

Dès lors, la révision de la loi de 1978 devrait prendre en compte les récentes évolutions du Conseil Constitutionnel relative aux pouvoirs de l’AMF afin que la règle du « non bis in idem » s’applique également dans le cadre des sanctions prononcées par la CNIL.

Par conséquent, le dispositif pénal prévu au chapitre VIII de la loi n° 78‑17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés doit donc être supprimé.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-22" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-23" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

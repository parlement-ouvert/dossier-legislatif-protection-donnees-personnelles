<!-- TITLE: Article 13 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-12/amendement-52" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-68" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 68 — Rejeté](/amendements-1-assemblee-hemicycle/article-13/amendement-68)
* [Amendement n° 128 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-128)
* [Amendement n° 148 — Rejeté](/amendements-1-assemblee-hemicycle/article-13/amendement-148)
* [Amendement n° 125 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-125)
* [Amendement n° 176 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-176)
* [Amendement n° 94 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-94)
* [Amendement n° 175 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-175)
* [Amendement n° 29 — Non soutenu](/amendements-1-assemblee-hemicycle/article-13/amendement-29)
* [Amendement n° 36 — Non soutenu](/amendements-1-assemblee-hemicycle/article-13/amendement-36)
* [Amendement n° 30 — Non soutenu](/amendements-1-assemblee-hemicycle/article-13/amendement-30)
* [Amendement n° 141 — Rejeté](/amendements-1-assemblee-hemicycle/article-13/amendement-141)
* [Amendement n° 95 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-95)
* [Amendement n° 174 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-174)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-12/amendement-52" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13/amendement-68" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 107 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Stéphane Peu**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 10](/pjl-592/titre-ii/chapitre-iii/article-10), insérer l'article suivant:

# Titre

Le premier alinéa de l’article 34 de la loi n° 78‑17 du 6 janvier 1978 précitée est complété par une phrase ainsi rédigée : « Cela implique notamment, chaque fois que cela est possible, que les données soient chiffrées de sorte à n’être accessibles qu’au moyen d’une clef mise à la seule disposition des personnes autorisées à y accéder. »

# Exposé sommaire

Selon les préconisations de la Quadrature du Net, cet amendement prévoit une obligation pour les responsables de traitements et sous -traitants de chiffrer les données de bout en bout, chaque fois que cela est possible. En effet, le chiffrement de bout en bout où seules les personnes autorisées à accéder aux données (par exemple l’expéditeur et le/les destinataire/s d’un message) ont la clef limite considérablement les risques d’intrusion.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

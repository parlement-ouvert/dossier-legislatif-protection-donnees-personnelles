<!-- TITLE: Amendement n° 85 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-6" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-6/amendement-46" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Jean-Louis Masson**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 6](/pjl-592/titre-i/chapitre-i/article-6), insérer l'article suivant:

# Titre

Le deuxième alinéa de l’article 2 de la loi n° 78‑17 du 6 janvier 1978 précitée est ainsi modifié :

1° À la première phrase, les mots : « relative à une personne physique identifiée ou qui peut être identifiée » sont remplacés par les mots : « se rapportant à une personne physique identifiée ou identifiable » ;

2° Après le mot : « moyens », la fin de la seconde phrase est ainsi rédigée : « raisonnablement susceptibles d’être utilisés ou mis en œuvre en vue de permettre son identification ».

# Exposé sommaire

Afin de ne pas créer de disparités juridiques entre la France et le reste de l’Union européenne et d’écarter tout risque de divergence d’interprétation, la définition de donnée à caractère personnel doit être la même dans l’ensemble du marché unique.

Ainsi, un alignement de la définition de donnée à caractère personnel sur celle du RGPD apparaît nécessaire, en particulier en précisant les conditions dans lesquelles une donnée doit être considérée comme indirectement identifiable.

Aussi, à ce titre, il est proposé d’intégrer la notion d’utilisation de « moyens raisonnables » présente dans le considérant 26 du RGPD, ainsi que dans les travaux du G29 (cf. avis du G29 4/2007 du 20 juin 2007 sur le concept de données à caractère personnel et n° 05/2014, 10 avril 2014, sur les techniques d’anonymisation, WP 216).

En effet, il convient de concilier la protection de la vie privée avec le développement des outils d’analyse et d’exploitation des informations essentiels à l’innovation et au développement des entreprises à l’ère numérique.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-6" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-6/amendement-46" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

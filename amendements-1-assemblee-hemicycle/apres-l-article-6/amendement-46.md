<!-- TITLE: Amendement n° 46 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-6/amendement-85" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 6](/pjl-592/titre-i/chapitre-i/article-6), insérer l'article suivant:

# Titre

La section 2 du chapitre V de la loi n° 78‑17 du 6 janvier 1978 précitée est complétée par un article 43 _quater_ ainsi r édigé :

« _Art. 43 _quater. – I. – À titre expérimental, le juge des libertés et de la détention peut prononcer une injonction avec astreinte sur les demandes des personnes physiques relatives à la protection et au traitement de leurs données à caractère personnel, notamment les droits d’accès, d’opposition, de rectification, à l’effacement ou « droit à l’oubli » et à la portabilité des données. Ces dispositions sont mises en œuvre dans les conditions précisées au II.

« II. – Pour une durée maximale de trois ans, le recours défini au I peut être mis en œuvre dans le ressort des tribunaux de grande instance qui en formulent la demande auprès de l’autorité administrative compétente. Un décret en Conseil d’État précise les conditions d’application du présent article.

« III. – Le Gouvernement remet au Parlement, au plus tard deux ans après promulgation de la présente loi, un rapport exhaustif présentant l’expérimentation, ainsi que ses résultats. »

# Exposé sommaire

Cet amendement vise à permettre l’effectivité de la protection des personnes physiques à l’égard du traitement des données à caractère personnel en instaurant le recours à une autorité judiciaire, gardienne des libertés individuelles au titre de l’article 66 de la Constitution.

Le groupe de la France insoumise considère que dans le cadre d’une expérimentation le recours à une autorité judiciaire, à savoir le juge des libertés et de la détention, doit permettre la garantie des droits des personnes notamment les droits d’accès, de rectification, d’opposition, à la limitation du traitement, ou encore le droit à l’effacement ou « droit à l’oubli » et le droit à la portabilité des données.

Le cadre souple d’une expérimentation permettra au Gouvernement de pouvoir calibrer le dispositif et de développer une approche scientifique d’évaluation afin de permettre une réelle appréciation qualitative et quantitative de celle-ci.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-6/amendement-85" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 134 -->
<!-- SUBTITLE: Retiré -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-90" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 13](/pjl-592/titre-ii/chapitre-iv/article-13), insérer l'article suivant:

# Titre

Après l’article L. 121‑4‑1 du code de l’éducation, il est inséré un article ainsi rédigé :

« _Art. L. 121‑4‑2_. – Les établissements d’enseignement scolaire mettent à la disposition du public, dans un format ouvert et aisément réutilisable, la liste des traitements automatisés de données à caractère personnel effectués sous leur responsabilité . »

# Exposé sommaire

Cet amendement propose une plus grande transparence des traitements de données scolaires. Si les données scolaires ne sont pas juridiquement des « données sensibles », leur traitement doit nécessairement faire l’objet d’une attention particulière de la part de la société civile, dans la mesure où elles sont relatives à un très grande nombre de personnes mineures. Si les traitements automatisés de données à caractère personnel collectées pour les besoins des établissements d’enseignement scolaire sont parfaitement légitimes et utiles, il convient toutefois de prévoir que chaque établissement scolaire en publie la liste, qui pourrait notamment comprendre la dénomination et la finalité du traitement, l’identité et l’adresse du responsable du traitement et les catégories de données à caractère personnel faisant l’objet du traitement.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-90" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

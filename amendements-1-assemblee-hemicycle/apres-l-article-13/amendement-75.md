<!-- TITLE: Amendement n° 75 -->
<!-- SUBTITLE: Retiré -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-118" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Latombe et les membres du groupe du Mouvement Démocrate et apparentés**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 13](/pjl-592/titre-ii/chapitre-iv/article-13), insérer l'article suivant:

# Titre

Le chapitre X de la loi n° 78‑17 du 6 janvier 1978 précitée est ainsi rétabli :

« Chapitre X

« Traitements de données à caractère personnel dans le domaine scolaire

« _Art. 62_. – L’ensemble des données collectées par les services du ministère de l’Éducation nationale aux fins d’assurer la mission d’enseignement et d’encadrement des élèves doivent être considérées comme des données d’intérêt public. À ce titre, conformément aux dispositions du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE, les traitements de ces données sont dispensés du consentement des personnes concernées.

« _Art. 63_. – I. – Les traitements relevant du présent chapitre ne peuvent être mis en œuvre qu’en considération de la finalité d’intérêt public qu’ils présentent.

« II. – Des référentiels et règlements types, au sens des _a_ bis et _b_ du 2° de l’article 11, s’appliquant aux traitements relevant du présent chapitre sont établis par la Commission nationale de l’informatique et des libertés en concertation avec le ministre chargé de l’éducation et des organismes publics et privés représentatifs des acteurs concernés.

« III. – Le ministre chargé de l’éducation s’assure que l’ensemble des traitements des données effectués par les services de son ministère sont conformes à ces référentiels. Ces référentiels, peuvent également porter sur la description et les garanties de procédure permettant la mise à disposition en vue de leur traitement de jeux de données scolaires présentant un faible risque d’impact sur la vie privée.

« IV. – La Commission nationale de l’informatique et des libertés peut, par décision unique, délivrer au ministre de l’éducation une autorisation pour des traitements répondant à une même finalité, portant sur des catégories de données identiques et ayant des catégories de destinataires identiques.

« V. – La Commission nationale de l’informatique et des libertés se prononce dans un délai de deux mois à compter de la réception de la demande. Toutefois, ce délai peut être renouvelé une fois sur décision motivée de son président ou lorsque l’Inspection générale est saisie en application du II du présent article.

« Lorsque la commission ne s’est pas prononcée dans ces délais, la demande d’autorisation est réputée acceptée. Cette disposition n’est toutefois pas applicable si l’autorisation fait l’objet d’un avis préalable en vertu des dispositions du présent chapitre et que l’avis ou les avis rendus ne sont pas expressément favorables.

« _Art. 64._ – S’agissant des mineurs de moins de 15 ans, sont destinataires de l’information et exercent les droits de la personne concernée par le traitement les titulaires de l’exercice de l’autorité parentale. »

# Exposé sommaire

Cet amendement vise à créer une nouvelle catégorie spécifique pour les traitements de données à caractère personnel dans le domaine scolaire, en utilisant les marges de manœuvre prévue par le Règlement (UE) 2016/679 relatif à la protection des données.

En effet, avec le l’utilisation croissante des outils numériques dans un cadre éducatif et le développement de l’offre commerciale dans ce domaine, il est nécessaire de s’assurer que les données pouvant être collectées à cette occasion soient suffisamment protégées.

Nous pensons notamment aux services proposés par les grandes entreprises telles que Google, Apple, Facebook, Amazon ou Microsoft, qui développent des outils pédagogiques, concurrençant le développement d’espaces numériques du travail (ENT) sécurisés - privilégiés par le Ministère de l’éducation jusqu’à présent.

Or, nous nous interrogeons sur l’utilisation et le traitement des données collectées par ces services souvent gratuits.

Bien qu’il existe des conditions générale d’utilisations (CGU) spécifiques à l’éducation, nous doutons que les règles soient aussi strictes que celles prévues dans les cahiers des charges des espaces numériques de travail (ENT).

Le Règlement prévoit un régime protecteur, entourant le consentement des mineurs de moins de 16 ans – 15 ans pour la France -. Dès lors, il n’est pas normal que les données à caractère personnel dans le domaine scolaire, portant précisément sur des mineurs, ne bénéficient pas elles aussi d’un régime spécifique et protecteur.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-118" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 144 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-90" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-167" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Latombe et les membres du groupe du Mouvement Démocrate et apparentés**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 13](/pjl-592/titre-ii/chapitre-iv/article-13), insérer l'article suivant:

# Titre

La seconde phrase de l’article L. 312‑9 du code de l’éducation est complétée par les mots : « , ainsi qu’aux règles applicables aux traitements des données à caractère personnel. »

# Exposé sommaire

L’effort de pédagogie doit être poursuivi au sein de l’Éducation nationale pour sensibiliser à la fois le corps enseignant et les élèves aux problématiques liées à la protection des données personnelles. En effet, si les données personnelles récoltées dans le cadre scolaire ne sont pas des données sensibles au sens de la réglementation européenne elles n’en sont pas moins des données devant être traitées d’une manière particulièrement rigoureuse. D’une manière plus large, la formation à l’utilisation des ressources et outils numériques doit éveiller les élèves à la nécessité de protéger l’ensemble de leurs données personnelles.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-90" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-167" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

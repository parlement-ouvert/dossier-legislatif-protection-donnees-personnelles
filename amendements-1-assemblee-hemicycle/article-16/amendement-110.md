<!-- TITLE: Amendement n° 110 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-16/amendement-88" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16/amendement-151" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Stéphane Peu**

[ARTICLE 16](/pjl-592/titre-ii/chapitre-vi/article-16)

# Titre

Compléter l’alinéa 2 par la phrase suivante :

« Lorsqu’elle constate un manquement, la Commission nationale de l’informatique et des libertés peut ordonner au responsable de traitement ou à son sous-traitant de rembourser à l’association ou à l’organisation qui en fait la demande les frais engagés par celle-ci pour exercer les droits des personnes concernées ».

# Exposé sommaire

Le présent amendement vise à rendre effectif le recours aux actions de groupe en autorisant la CNIL à ordonner au responsable de traitement ou à son sous-traitant de rembourser à l’association ou à l’organisation qui en fait la demande les frais engagés par celle-ci pour exercer les droits des personnes concernées. Il s’agit de permettre aux associations et organismes mandatés de mener efficacement un action de groupe dont l’avance des frais peut s’avérer dissuasif sans mesures d’accompagnement appropriées.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-16/amendement-88" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16/amendement-151" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

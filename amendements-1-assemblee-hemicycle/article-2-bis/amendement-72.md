<!-- TITLE: Amendement n° 72 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-2-bis/amendement-100" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE 2](/pjl-592/titre-i/chapitre-i/article-2) BIS

# Titre

Compléter cet article par l’alinéa suivant :

« Les délibérations de la commission réunie en formation plénière sont diffusées en direct sur le site internet de la commission et sont accessibles après diffusion, librement et gratuitement, sur internet. »

# Exposé sommaire

Si l’article 3 modifie les articles 17 et 18 de la loi du 6 janvier 1978 pour notamment prévoir des cas où les  membres de la CNIL délibèrent hors de la présence des agents de la commission et du commissaire du Gouvernement auprès de la CNIL, nous estimons que la CNIL doit faire aussi preuve de transparence vis à vis du grand public pour certaines de ses délibérations (dans un but à la fois de contrôle citoyen sur le bon fonctionnement d’une instance aussi importante pour les droits et libertés que la CNIL, mais surtout pour que les citoyens puissent être pleinement conscients et informés de son activité).

Pour ce faire, il nous est apparu indispensable de proposer une expérimentation : que certaines délibérations de la CNIL, qu’elles soient en formation collégiale ou en formation restreinte puissent être télédiffusées et accessibles au public.

En effet, le secret du délibéré (Article 304 du code de procédure pénale et et article L.8 du code de justice administrative, notamment), ne s’applique en aucun cas à la CNIL puisque celle-ci n’est en tout état de cause pas une instance judiciaire, mais bien une “autorité administrative indépendante”. Les délibérations et la prise d’une juste sanction dans certains cas peut ainsi avoir une vertu pédagogique et s’assurer que la CNIL n’ait pas la main qui tremble lorsqu’il s’agit de condamner une entreprise avec un pouvoir de marché et de nuisance tels que les GAFA (Google, Amazon, Facebook, Apple), qui font peu de cas des droits et libertés numériques.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-2-bis/amendement-100" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

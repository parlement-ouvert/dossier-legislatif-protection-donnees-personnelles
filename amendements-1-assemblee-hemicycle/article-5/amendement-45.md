<!-- TITLE: Amendement n° 45 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-5/amendement-136" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-5/amendement-137" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE 5](/pjl-592/titre-i/chapitre-i/article-5)

# Titre

À la seconde phrase de l’alinéa 9, substituer aux mots :

« le président de la commission »,

les mots :

« la commission en formation plénière ».

# Exposé sommaire

Nous estimons que pour une décision aussi fondamentale que d’autoriser des agents publics d’autres États membres de l’Union européenne de pouvoir participer à des enquêtes conjointes en France, ce n’est pas le président de la CNIL seul qui doit décider, mais bien la Commission en formation plénière.

Ceci permet une meilleure transparence et une réelle possibilité d’échange et de délibération sur un tel pouvoir d’autorisation / habilitation d’agents dont par nature, la loyauté va à d’autres agences de contrôle de protection des données et d’autres États.

Cet amendement s’inscrit pleinement dans les marges de transposition permises par la directive qui ne précise pas spécifiquement les modalités d’habilitation d’agents hors-CNIL, à savoir des agents d’autres autorités de contrôle européennes chargées de veiller à la protection des données, de pouvoir participer à des équipes de contrôle conjointes en France.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-5/amendement-136" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-5/amendement-137" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

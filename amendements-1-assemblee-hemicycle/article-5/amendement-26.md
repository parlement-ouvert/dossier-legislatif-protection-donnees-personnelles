<!-- TITLE: Amendement n° 26 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-5/amendement-136" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Emmanuelle Ménard**

[ARTICLE 5](/pjl-592/titre-i/chapitre-i/article-5)

# Titre

Après la première phrase de l’alinéa 7, insérer la phrase suivante :

« Cette coopération ne peut donner lieu à aucune ingérence de pays membres de l’Union européenne. »

# Exposé sommaire

L’application du règlement sur la protection des données personnelles (RGPD) entre en vigueur en mai 2018 et met en place un Comité européen de la protection des données (CEPD). Celui-ci pourra organiser des enquêtes sur le sol français et ses habitants. Une fois de plus, c’est une mesure qui va à l’encontre de la souveraineté de française.

Si on peut envisager une collaboration entre la CNIL et ses homologues européens, il n’est pas possible que ce soit au dépend des Français. La CNIL, qui existe depuis quarante ans, qui est une référence en France comme dans le monde, est suffisamment autonome et efficace dans la protection des données personnelles qu’elle gère. Elle ne saurait souffrir aucune ingérence dans son fonctionnement.

Il n’est donc pas raisonnable que la France soit amenée à communiquer les données dont elle a la protection, au risque d’en perdre le contrôle précieux.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-5/amendement-136" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

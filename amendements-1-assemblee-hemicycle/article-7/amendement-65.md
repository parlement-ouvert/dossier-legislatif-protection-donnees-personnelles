<!-- TITLE: Amendement n° 65 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-7/amendement-50" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

[ARTICLE 7](/pjl-592/titre-i/chapitre-ii/article-7)

# Titre

Compléter cet article par les deux alinéas suivants :

« 5° Il est ajouté un V ainsi rédigé :

« V. – Il est interdit de pouvoir obtenir, à des fins lucratives, des données sensibles en croisant des données non sensibles. »

# Exposé sommaire

Par cet amendement, nous proposons d’interdire la production de données sensibles grâce à des données non sensibles (croisements de données à caractère commercial et de vente, données bancaires, cookies etc.). Ceci est particulièrement malheureusement aisé dans le domaine de la santé ...

Un exemple : les applications liées à la sphère de l “e-santé”ont pour particularité d’offrir aux utilisateurs des outils de mesures de leurs performances physiques. L’une des applications les plus répandues est le “compteur de pas” disponible dans nombre de smartphones, dont le pionnier fut l’entreprise Nike. “Nike+” est ainsi un système qui se décline sur baladeur, montre connecté et smartphone pour obtenir un suivi rigoureux des parcours de l’utilisateur et leur partage sur les réseaux sociaux.

Ces applications ont pour qualité d’inciter à la pratique sportive. Mais elles ont pour défaut l’exportation de données à des fins commerciales. Cela contribue à du profilage mais aussi à la monopolisation de données de santé publique par des entreprises privées.

Pour promouvoir un usage lié à la recherche et à la santé, cet amendement propose l’interdiction formelle - à des fins lucratives seulement - de pouvoir contourner la législation en croisant des données non sensibles pour obtenir des données sensibles..

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-7/amendement-50" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 50 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-7/amendement-101" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-7/amendement-65" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE 7](/pjl-592/titre-i/chapitre-ii/article-7)

# Titre

Compléter cet article par les deux alinéas suivants :

« 5° Il est ajouté un V ainsi rédigé :

« V. – Il est interdit d’effectuer un profilage privé à des fins lucratives. »

# Exposé sommaire

Nul ne peut nier l’importance qu’ont pris les données personnelles dans le commerce et particulièrement sur internet, faisant émerger une véritable guerre commerciale des données personnelles. Et ce serait faire de l’angélisme que de croire que la série Black Mirror n’est qu’une fiction, preuve en est d’ailleurs dans le documentaire édifiant sinon terrifiant « Nothing to Hide » de (2016) de Marc Meillassoux et Mihaela Gladovic.

Longtemps les législations françaises et européennes ont progressé autour de la recherche d’un « l’équilibre » entre droits fondamentaux et liberté du commerce, et de la notion de profilage.

Aux terme du considérant numéro 38 de la directive n° 2016/680 du parlement européen et du Conseil en date du 27 avril 2016 relative à la protection des personnes physiques à l’égard du traitement des données à caractère personnel par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d’enquêtes et de poursuites en la matière ou d’exécution de sanctions pénales, et à la libre circulation de ces données, le profilage est définit ainsi : « la personne concernée devrait avoir le droit de ne pas faire l’objet d’une décision impliquant l’évaluation de certains aspects personnels la concernant, qui résulterait exclusivement d’un traitement automatisé et qui produirait des effets juridiques la concernant ou qui l’affecterait de manière sensible, par exemple le rejet automatique d’une demande de crédit en ligne ou des pratiques de recrutement en ligne sans aucune intervention humaine. Ce type de traitement inclut notamment le « profilage », à savoir toute forme de traitement automatisé de données à caractère personnel visant à évaluer certains aspects personnels liés à une personne physique, notamment par l’analyse et la prédiction d’éléments concernant le rendement au travail, la situation économique, la santé, les préférences personnelles ou les intérêts, la fiabilité ou le comportement, ou la localisation et les déplacements, dès lors qu’il produit des effets juridiques la concernant ou qu’il l’affecte de manière sensible […] ».

L’alinéa 2 de l’article 7 bis de la loi informatique et libertés du 6 janvier 1978 dispose qu’« aucune autre décision produisant des effets juridiques à l’égard d’une personne ne peut être prise sur le seul fondement d’un traitement automatisé de données destiné à définir le profil de l’intéressé ou à évaluer certains aspects de sa personnalité ».

Pour le groupe de la France insoumise, il est important de ne pas transiger sur nos libertés individuelles et de ne pas tomber dans le faux débat de la rhétorique de la recherche d’un équilibre, qui en vérité cherche à porter sans cesse atteinte à la protection des données individuelles dans une logique disproportionnée et purement mercantile.

C’est pourquoi il est urgent d’interdire le profilage privé à des fins lucratives, pour protéger la partie la plus faible dans cette situation.

En effet, notre constat est double :

\- D’une part, les pratiques commerciales de récolte des données ne respectent pas le consentement des personnes (absence d’information, traitement automatisé, …) ;

\- D’autre part, l’usage des données individuelles constituent une création de richesse, qui se fait à l’insu des utilisateurs, par l’alimentation de bases de données.

Notre responsabilité en tant que législateur est de créer un cadre réellement protecteur de nos droits et libertés qui incitera les acteurs français et européens du numérique à investir dans des modèles plus vertueux.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-7/amendement-101" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-7/amendement-65" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

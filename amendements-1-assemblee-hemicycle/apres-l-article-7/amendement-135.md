<!-- TITLE: Amendement n° 135 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-7/amendement-63" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Marine Le Pen**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 7](/pjl-592/titre-i/chapitre-ii/article-7), insérer l'article suivant:

# Titre

Après l’article 5 de la loi n° 78‑17 du 6 janvier 1978 précitée, il est inséré un article 5‑1 ainsi rédigé :

« _Art. 5‑1_. – Le stockage des données à caractère personnel concernant les citoyens français doit être localisé exclusivement sur le territoire national ».

# Exposé sommaire

Les données personnelles sont le nouvel or dans notre société. Il convient donc de les protéger. Leur stockage, lorsqu’elles concernent les citoyens français doit se faire en France pour les protéger. En outre cette obligation sera source de développement économique surtout que les serveurs peuvent être aisément localisés sur l’ensemble du territoire et donc dans des zones à développer économiquement.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-7/amendement-63" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

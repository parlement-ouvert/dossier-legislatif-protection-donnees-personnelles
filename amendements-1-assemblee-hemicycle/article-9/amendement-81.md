<!-- TITLE: Amendement n° 81 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-9/amendement-66" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-9/amendement-147" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Jean-Louis Masson**

[ARTICLE 9](/pjl-592/titre-ii/chapitre-ii/article-9)

# Titre

Compléter cet article par l’alinéa suivant :

« IV. – Sauf changement survenu, les traitements autorisés antérieurs au 25 mai 2018 et encore en cours bénéficient d’une présomption de conformité aux dispositions du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité. »

# Exposé sommaire

Il est important pour les acteurs, qu’aucune formalité ou analyse supplémentaire ne soit prévue pour les traitements en cours.

Comme le préconise le G29 dans ses lignes directrices relatives aux analyses d’impact, il convient d’assurer une continuité pour les traitements antérieurs qui ont obtenu une autorisation, sauf si un changement est survenu depuis.

Ainsi, ces traitements doivent être présumés en conformité, notamment pour les autorisations les plus récemment délivrées.

Dès lors, une disposition spécifique est nécessaire pour garantir le maintien des traitements en cours, lorsqu’ils ont été déclarés ou a fortiori autorisés avant l’entrée en application du RGPD, dans les mêmes conditions et/ou pour une durée limitée à 3 ans par exemple.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-9/amendement-66" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-9/amendement-147" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

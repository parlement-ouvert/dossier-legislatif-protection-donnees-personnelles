<!-- TITLE: Amendement n° 66 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-9" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-9/amendement-81" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Ugo Bernalicis**

[ARTICLE 9](/pjl-592/titre-ii/chapitre-ii/article-9)

# Titre

Supprimer cet article.

# Exposé sommaire

Cet article prétend libéraliser la procédure de déclaration d’utilisation de données comportant le numéro de sécurité tout en protégeant un certains nombres de cas comme celui des données de santé. Si nous demandons sa suppression, c’est parce que nous estimons que les données de santé sont menacés par la libéralisation qui est mise en place par ailleurs. En effet, il est possible de créer des données de santé à partir de données apparemment inoffensives.

Par exemple, le croisement entre des données de comptage de pas (application sur téléphone par exemple) et l’achat de nourriture diététique ou de certains types d’aliments (pharmacie, hypermarché, commerce de proximité) contribue à donner des renseignements sur la santé d’une personne.

Or, spécifiquement, le NIR (numéro de sécurité sociale), s’il est utilisé dans de nombreux domaines (lors d’une embauche, à la pharmacie, etc) permettra nécessairement de croiser des données pouvant permettre de recouper des éléments pouvant donner des indications sur la santé (puisqu’il s’agit bien du numéro de sécurité sociale…), qui elles sont toujours considérées comme des données d’une sensibilité particulière.

En définitive, ce que nous reprochons à cet article c’est de sous-estimer l'habileté de ceux qui le souhaiteront à profiter de la libéralisation de l’accès aux données non-sensibles pour se procurer des données sensibles. C’est pourquoi nous en demandons sa suppression.

Par cet amendement, nous ne faisons ici qu’utiliser les marges de manoeuvre prévues par le droit européen, à travers la notion de “mission d’intérêt public”.Selon les termes mêmes du 5 de l’article 36 du règlement européen RGPD : “5\. Nonobstant le paragraphe 1, le droit des États membres peut exiger que les responsables du traitement consultent l'autorité de contrôle et obtiennent son autorisation préalable en ce qui concerne le traitement effectué par un responsable du traitement dans le cadre d'une mission d'intérêt public exercée par celui-ci, **y compris le traitement dans le cadre de la protection sociale et de la sant é publique**.”.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-9" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-9/amendement-81" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

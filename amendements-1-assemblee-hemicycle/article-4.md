<!-- TITLE: Article 4 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-3/amendement-157" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-4/amendement-153" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 153 — Retiré](/amendements-1-assemblee-hemicycle/article-4/amendement-153)
* [Amendement n° 76 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-76)
* [Amendement n° 23 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-23)
* [Amendement n° 24 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-24)
* [Amendement n° 162 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-162)
* [Amendement n° 122 — Non soutenu](/amendements-1-assemblee-hemicycle/article-4/amendement-122)
* [Amendement n° 77 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-77)
* [Amendement n° 78 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-78)
* [Amendement n° 161 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-161)
* [Amendement n° 124 — Retiré](/amendements-1-assemblee-hemicycle/article-4/amendement-124)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-3/amendement-157" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-4/amendement-153" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

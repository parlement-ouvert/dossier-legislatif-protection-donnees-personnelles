<!-- TITLE: Amendement n° 57 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-19" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-19/amendement-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

[ARTICLE 19](/pjl-592/titre-iii/article-19)

# Titre

Supprimer cet article.

# Exposé sommaire

Par cette loi de transposition, et sans que cela n’ait été explicitement prévu par la directive européenne (directive (UE) 2016/680 relative à la protection des personnes physiques à l’égard du traitement des données à caractère personnel (…)) le Gouvernement a impulsé de lui-même une libéralisation du “fichage” en facilitant l’accès aux données (plus de champs de données ouvertes), la création d’un traitement étant facilitée (l’activité principale de la CNIL devenant la supervision plus que l’autorisation), et en ouvrant la possibilité à plus d’acteurs d’utiliser ces données.

Tel que le rappelle notre Livret numérique, nous nous opposons au fichage de masse tel que proposé par ce projet de loi ([https ://avenirencommun.fr/le-livret-numerique/](https://avenirencommun.fr/le-livret-numerique/)).

Cet article, parmi l’un des plus problématiques du projet de loi permet la généralisation de “fichiers” en matière pénale (« à des fins de prévention et de détection des infractions pénales, d’enquêtes et de poursuites en la matière ou d’exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces ») car ils n’ont plus nécessairement à être autorisés par arrêté ministériel (article 26 de la loi de 1978 actuelle).

Tout comme la CNIL, nous souhaitons plutôt promouvoir une logique de renforcement du droit commun plutôt que de facilitation et de démantèlement…

Dans une même perspective, tel que le rappelle la CNIL dans son avis du 30 novembre 2017 (https ://www.legifrance.gouv.fr/affichCnil.do ?oldAction=rechExpCnil&amp;id=CNILTEXT000036195647&amp;fastReqId=180931766&amp;fastPos=1) : « le projet de loi ne prévoit aucune disposition concernant le droit d’opposition des personnes concernées, qui doit pouvoir, y compris en ces matières, trouver à s’appliquer dans des circonstances particulières, comme par exemple dans le cadre du traitement de données relatives à des personnes victimes dans le Traitement des Antécédents Judiciaires (TAJ)”.

Nous ne faisons ici qu’utiliser les marges de manœuvre prévues par le droit européen, à travers la notion de “mission d’intérêt public”. Selon les termes mêmes du 5 de l’article 36 du règlement européen RGPD : “5\. Nonobstant le paragraphe 1, le droit des États membres peut exiger que les responsables du traitement consultent l’autorité de contrôle et obtiennent son autorisation préalable en ce qui concerne le traitement effectué par un responsable du traitement dans le cadre d’une mission d’intérêt public exercée par celui-ci, y compris le traitement dans le cadre de la protection sociale et de la santé publique.”.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-19" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-19/amendement-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 127 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-14/amendement-119" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza**

[ARTICLE 14](/pjl-592/titre-ii/chapitre-v/article-14)

# Titre

À l’alinéa 7, substituer aux mots :

« et sous les réserves mentionnées au 3 du même article 22 ; »

les mots :

« , sous les réserves mentionnées au 3 du même article 22 et à condition que les règles définissant le traitement ainsi que les principales caractéristiques de sa mise en œuvre soient communiquées, à l’exception des secrets protégés par la loi, par le responsable de traitement à l’intéressé s’il en fait la demande. »

# Exposé sommaire

S’agissant des traitements algorithmiques mis en oeuvre par l’administration, la loi sur la République numérique a prévu à l’article L. 311‑3‑1 du code des relations entre le public et l’administration que « Sous réserve de l’application du 2° de l’article L. 311‑5, une décision individuelle prise sur le fondement d’un traitement algorithmique comporte une mention explicite en informant l’intéressé. Les règles définissant ce traitement ainsi que les principales caractéristiques de sa mise en œuvre sont communiquées par l’administration à l’intéressé s’il en fait la demande. »

L’article R.311‑3‑1‑2, pris en application de l’article L. 311‑3‑1, dispose ainsi que : ’L’administration communique à la personne faisant l’objet d’une décision individuelle prise sur le fondement d’un traitement algorithmique, à la demande de celle-ci, sous une forme intelligible et sous réserve de ne pas porter atteinte à des secrets protégés par la loi, les informations suivantes :

1° Le degré et le mode de contribution du traitement algorithmique à la prise de décision ;

2° Les données traitées et leurs sources ;

3° Les paramètres de traitement et, le cas échéant, leur pondération, appliqués à la situation de l’intéressé ;

4° Les opérations effectuées par le traitement. »

Le présent amendement propose d’appliquer ces garanties aux cas mentionnés par les a) et c) de l’article 22 du RGPD.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-14/amendement-119" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

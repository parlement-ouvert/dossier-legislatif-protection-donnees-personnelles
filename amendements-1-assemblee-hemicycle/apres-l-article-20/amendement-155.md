<!-- TITLE: Amendement n° 155 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-20" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-21" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Rémy Rebeyrotte et les membres du groupe La République en Marche**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 20](/pjl-592/titre-iv/article-20), insérer l'article suivant:

# Titre

I. – Le code de la consommation est ainsi modifié :

1° La sous-section 4 de la section 3 du chapitre IV du titre II du livre II du code de la consommation, dans sa rédaction résultant de l’article 48 de la loi n° 2016‑1321 du 7 octobre 2016 pour une République numérique, est abrogée ;

2° Au premier alinéa de l’article L. 242‑20, dans sa rédaction résultant du même l’article 48, la référence : « L. 224‑42‑3 » est supprimée.

II. – Le II de l’article 48 de la loi n° 2016‑1321 du 7 octobre 2016 pour une République numérique est abrogé.

# Exposé sommaire

L’article 48 de la loi n° 2016‑1321 du 7 octobre 2016 pour une République numérique a introduit dans le Code de la consommation des dispositions permettant au consommateur de récupérer les fichiers qu’il a mis en ligne, les données résultant de l’utilisation de son compte d’utilisateur et consultables en ligne par celui-ci, ainsi que d’autres données associées à son compte utilisateur sous certaines conditions.

Or, l’articulation entre cette disposition et le droit à la portabilité des données prévu par l’article 20 du RGPD soulève des problématiques puisque les données qui doivent être transmises au consommateur recoupent celles qui doivent être communiquées à la personne concernée au titre du droit à la portabilité des données personnelles prévu par le RGPD.

Ceci a été confirmé par les lignes directrices relatives au droit à la portabilité des données qui ont été adoptées par le Groupe de travail « article 29 » sur la protection des données (organe consultatif européen sur la protection des données).

En outre, ce droit européen à la portabilité prévoit des modalités de portabilité plus aisées pour les consommateurs que celles prévues par le droit national.

En conséquence, les dispositions introduites par l’article 48 de la loi pour une République numérique, et notamment les articles L. L224‑42‑1 à 4 du code de la consommation, doivent être supprimés afin d’assurer une application cohérente du droit à la portabilité des données des personnes.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-20" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-21" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

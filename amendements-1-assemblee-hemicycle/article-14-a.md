<!-- TITLE: Article 14 A -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-184" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-35" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 35 — Rejeté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-35)
* [Amendement n° 69 — Rejeté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-69)
* [Amendement n° 21 — Rejeté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-21)
* [Amendement n° 10 — Rejeté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-10)
* [Amendement n° 103 — Adopté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-103)
* [Amendement n° 182 — Adopté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-182)
* [Amendement n° 9 — Tombé](/amendements-1-assemblee-hemicycle/article-14-a/amendement-9)
* [Amendement n° 12 — Adopté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-12)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-184" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-14-a/amendement-35" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

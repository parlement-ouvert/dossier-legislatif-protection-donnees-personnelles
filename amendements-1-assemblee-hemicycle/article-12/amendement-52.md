<!-- TITLE: Amendement n° 52 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

[ARTICLE 12](/pjl-592/titre-ii/chapitre-iv/article-12)

# Titre

Supprimer cet article.

# Exposé sommaire

Par cette loi de transposition, et sans que cela n’ait été explicitement prévu par la directive européenne (directive (UE) 2016/680 relative à la protection des personnes physiques à l’égard du traitement des données à caractère personnel (…)) le Gouvernement a impulsé de lui-même une libéralisation du “fichage” en facilitant l’accès aux données (plus de champs de données ouvertes), la création d’un traitement étant facilitée (l’activité principale de la CNIL devenant la supervision plus que l’autorisation), et en ouvrant la possibilité à plus d’acteurs d’utiliser ces données.

Tel que le rappelle notre Livret numérique, nous nous opposons au fichage de masse tel que proposé par ce projet de loi ([https ://avenirencommun.fr/le-livret-numerique/](https://avenirencommun.fr/le-livret-numerique/)).

Dans ce cadre, nous proposons la suppression de cer article 12 qui permet une durée de conservation plus longue de nombreuses données. En effet, il remplace les termes « historiques, statistiques ou scientifiques » par les mots « archivistiques dans l’intérêt public, à des fins de recherche scientifique ou historique, ou à des fins statistiques », qui sont volontairement beaucoup moins précis, ce afin que le plus de données possibles puissent rentrer dans cette catégorie.

Ceci a fait d’augmenter le nombre de dérogations possibles au 5° de l’article 6 de la loi de 1978 qui précise que : « sont conservées sous une forme permettant l’identification des personnes concernées pendant une durée qui n’excède pas la durée nécessaire aux finalités pour lesquelles elles sont collectées et traitées. »

Concrètement, en élargissant ainsi les types de données concernées, le Gouvernement veut étendre la durée de conservation de nombreuses données, ce pour des motifs particulièrement peu précis.

Par exemple, avec un tel amendement, on peut imaginer que “à des fins de recherche scientifique” on puisse permettre de conserver au-delà de leur durée initialement prévue des données personnelles sensibles (données de santé, etc…) alors que cela n’aurait pas été possible avant. (“scientifiques” se référant à des données étant déjà scientifiques et non pouvant être utilisées pour).

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-13" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

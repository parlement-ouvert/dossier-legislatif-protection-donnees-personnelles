<!-- TITLE: Amendement n° 56 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-15/amendement-132" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

[ARTICLE 15](/pjl-592/titre-ii/chapitre-v/article-15)

# Titre

Compléter cet article par l’alinéa suivant :

« IV. – Toute dérogation prévue au III doit faire l’objet d’une validation par le juge des libertés et de la détention, dans un délai de 72 heures, qui s’attache à vérifier le respect de la procédure et des libertés individuelles. »

# Exposé sommaire

Le Groupe de la France insoumise est attaché au respect de l’équilibre des pouvoirs et considère qu’une autorité administrative indépendante ne doit pas exclure un contrôle par une autorité judiciaire et ce afin de renforcer le caractère légitime des dérogations que cette autorité peut prononcer.

Afin de garantir le respect des libertés individuelles, il apparaît donc important de permettre à une autorité judiciaire, gardienne des libertés individuelles au titre de l'article 66 de la Constitution, de venir équilibrer le dispositif mis en place par ces dérogations.

Cet amendement vise donc à établir un tel contrôle a priori effectué par le juge des libertés et de la détention afin d’assurer un équilibre entre les exigences de l'action administrative et celles de la protection des droits fondamentaux.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-15/amendement-132" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

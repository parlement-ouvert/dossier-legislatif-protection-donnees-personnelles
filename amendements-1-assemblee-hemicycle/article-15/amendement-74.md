<!-- TITLE: Amendement n° 74 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-15/amendement-149" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Philippe Latombe et les membres du groupe du Mouvement Démocrate et apparentés**

[ARTICLE 15](/pjl-592/titre-ii/chapitre-v/article-15)

# Titre

Avant l’alinéa 1, insérer les cinq alinéas suivants :

« Le II de l’article 40 de la loi n° 78‑17 du 6 janvier 1978 précitée est ainsi modifié :

« 1° Après le mot : « peut », la fin du deuxième alinéa est ainsi rédigée : « recourir à un médiateur en vue de la résolution amiable du litige qui l’oppose au responsable du traitement, en application de la section 1 du chapitre Ier du titre II de la loi n° 95‑125 du 8 février 1995 relative à l’organisation des juridictions et à la procédure civile, pénale et administrative. » ;

« 2° Après le deuxième alinéa, il est inséré un alinéa ainsi rédigé :

« En cas d’échec de la médiation, la personne concernée peut saisir la Commission nationale de l’informatique et des libertés, qui se prononce sur cette demande dans un délai de trois semaines à compter de la date de réception de la réclamation. » ;

3° Au troisième alinéa, le mot : « deux » est remplacé par le mot : « trois ».

# Exposé sommaire

Cet amendement tend à ouvrir la possibilité pour la personne concernée de recourir à une médiation, lorsque le responsable du traitement n’a pas fait droit à sa demande d’effacement de ses données à caractère personnel ou lorsque le responsable du traitement n’a pas répondu dans un délai d'un mois à compter de la demande.

Cette médiation se déroulera suivant les dispositions de la section 1 du chapitre Ier du titre II de la loi n° 95-125 du 8 février 1995 relative à l'organisation des juridictions et à la procédure civile, pénale et administrative.

En cas d’échec de cette médiation, la personne concernée a la possibilité de saisir la CNIL.

Le recours à une médiation permettra de soulager les services de la CNIL, laquelle sera amenée à se prononcer en cas de réelle difficulté.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-15/amendement-149" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

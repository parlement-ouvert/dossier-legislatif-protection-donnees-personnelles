<!-- TITLE: Amendement n° 159 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-10/amendement-177" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Constance Le Grip**

[ARTICLE 10](/pjl-592/titre-ii/chapitre-iii/article-10)

# Titre

Compléter cet article par l’alinéa suivant :

« « Les responsables de traitements et les sous-traitants peuvent recourir au médiateur des entreprises dans des conditions fixées par décret. » »

# Exposé sommaire

Dans la vie des entreprises, l’équilibre des relations contractuelles entre les responsables de traitement et les sous-traitants est capital pour assurer la bonne mise en œuvre du RGPD. Les obligations des deux catégories d’acteurs sont à ce titre définies précisément dans le règlement.

Toutefois, en cas de différend entre un responsable de traitement et un sous-traitant, il convient de privilégier la recherche d’un règlement amiable entre les parties. Les procédures litigieuses peuvent en effet fragiliser des petites structures économiques.

A ce titre, le médiateur des entreprises pourrait être mobilisé, dans des conditions définies par décret.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-10/amendement-177" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

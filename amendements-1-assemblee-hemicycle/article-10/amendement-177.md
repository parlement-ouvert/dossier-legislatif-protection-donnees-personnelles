<!-- TITLE: Amendement n° 177 -->
<!-- SUBTITLE: Retiré -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-10/amendement-159" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Christine Hennion**

[ARTICLE 10](/pjl-592/titre-ii/chapitre-iii/article-10)

# Titre

Compléter cet article par l’alinéa suivant :

« III. – Les différends entre responsables de traitement et sous-traitants peuvent être soumis à un médiateur. L’autorité responsable de ce dispositif soumet un rapport d’activité tous les ans dont les résultats sont transmis au Parlement. »

# Exposé sommaire

Cet amendement vise à favoriser le recours par les professionnels à des actions de médiation, en amont des litiges qui pourraient opposer responsables de traitement et sous-traitants.

Ces actions pourraient être menées par le Médiateur des entreprises, en lien avec la CNIL ou des médiateurs sectoriels tels que le Médiateur des communications électroniques. Le Médiateur des entreprises dispose d’une certaine expérience dans ce domaine, étant donné que les litiges opposant potentiellement responsables de traitement et sous-traitants entrent dans le cadre des litiges contractuels, dont le Médiateur des entreprises est amené à connaître depuis sa création en 2010.

Ce système permettra aux acteurs économiques, et notamment aux petites et moyennes entreprises, de renouer des liens contractuels essentiels, alors que 75 % des médiations mises en œuvre par le Médiateur des entreprises ont abouti, jusqu’à présent, à un protocole d’accord signé ou un accord verbal. Le Médiateur des entreprises a également l’avantage de proposer des actions rapides et gratuites.

Ce dispositif s’inscrit enfin dans l’économie globale du Règlement général de protection des données, qui vise à responsabiliser les entreprises traitant des données personnelles et à faire en sorte qu’elles soient actrices de la protection des données. Ces actions rejoignent enfin le cadre européen en faveur des règlements alternatifs des différends professionnels, tel qu’il est promu par l’Union européenne dans la directive 2013/11/UE du Parlement européen et du Conseil du 21 mai 2013 relative au règlement extrajudiciaire des litiges de consommation.

La présentation régulière du résultat des actions de médiation, soumises à des impératifs de confidentialité et de loyauté, et leur communication au Parlement représentent également des atouts précieux pour apprécier la mise en œuvre de la loi n° 78‑17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, telle qu’amendée par le présent projet de loi.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-10" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-10/amendement-159" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 129 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-20/amendement-120" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-20/amendement-146" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Sébastien Huyghe**

[ARTICLE 20](/pjl-592/titre-iv/article-20)

# Titre

Supprimer cet article.

# Exposé sommaire

Il est tout à fait regrettable que le Gouvernement ait choisi de réécrire l’ensemble de la loi n° 78‑17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, dite loi « Informatique et libertés », par voie d’ordonnances.

C’est d’autant plus déplorable que cette habilitation prive nos concitoyens d’un moment de pédagogie et de l’occasion de choisir la société du numérique qu’ils souhaitent. L’enjeu de la protection des données personnelles mérite un grand débat démocratique au sein du Parlement. Il est fondamental que les élus de la Nation s’emparent de ce sujet plutôt que de laisser le soin à de hauts fonctionnaires de réécrire ce texte.

Le présent amendement a donc pour objectif de revenir sur l’habilitation en permettant ainsi un véritable débat sur le sujet au sein des deux assemblées.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-20/amendement-120" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-20/amendement-146" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

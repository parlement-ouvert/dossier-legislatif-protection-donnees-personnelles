<!-- TITLE: Article PREMIER -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/index-amendements-par-auteur" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-37" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 37 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-37)
* [Amendement n° 102 — Adopté](/amendements-1-assemblee-hemicycle/article-premier/amendement-102)
* [Amendement n° 179 — Tombé](/amendements-1-assemblee-hemicycle/article-premier/amendement-179)
* [Amendement n° 7 — Retiré](/amendements-1-assemblee-hemicycle/article-premier/amendement-7)
* [Amendement n° 39 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-39)
* [Amendement n° 38 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-38)
* [Amendement n° 111 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-111)
* [Amendement n° 181 — Retiré](/amendements-1-assemblee-hemicycle/article-premier/amendement-181)
* [Amendement n° 3 — Non soutenu](/amendements-1-assemblee-hemicycle/article-premier/amendement-3)
* [Amendement n° 156 — Adopté](/amendements-1-assemblee-hemicycle/article-premier/amendement-156)
* [Amendement n° 40 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-40)
* [Amendement n° 104 — Adopté](/amendements-1-assemblee-hemicycle/article-premier/amendement-104)
* [Amendement n° 105 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-105)
* [Amendement n° 152 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-152)
* [Amendement n° 163 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-163)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/index-amendements-par-auteur" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-premier/amendement-37" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

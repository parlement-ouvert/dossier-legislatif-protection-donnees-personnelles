<!-- TITLE: Amendement n° 173 -->
<!-- SUBTITLE: Retiré -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-17/amendement-169" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-19" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Éric Bothorel**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 17](/pjl-592/titre-ii/chapitre-vi/article-17), insérer l'article suivant:

# Titre

Lors de l’élaboration, de la conception, de la sélection et de l’utilisation d’applications, de services et de produits qui reposent sur le traitement de données à caractère personnel ou traitent de telles données pour remplir leurs fonctions, les fabricants ou distributeurs de terminaux, fixes ou mobiles, permettant l’accès à des services de communication au public en ligne sont responsables conjoints du traitement.

Afin de s’assurer qu’ils sont en mesure de s’acquitter des obligations qui leur incombent en matière de protection des données, les responsables conjoints mentionnés à l’alinéa précédent ont obligation de mettre en œuvre les mesures techniques et organisationnelles appropriées pour garantir que, par défaut, seules les données qui sont nécessaires au regard de chaque finalité spécifique du traitement sont traitées. Ces mesures peuvent consister à configurer par défaut un terminal d’une manière à garantir que seules les données qui sont nécessaires au regard de chaque finalité spécifique du traitement sont traitées.

Lorsque le traitement repose sur le consentement, les responsables conjoints précités sont soumis à l’obligation de s’assurer du caractère libre du consentement et notamment que, par défaut, le choix d’un service qui ne collecte et ne conserve pas de données personnelles associées aux recherches effectuées est proposé explicitement.

En application des articles 25.2 et 26.1 du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité, toute clause contractuelle ayant pour effet de contraindre les responsables conjoints précités à une obligation contraire est nulle lorsqu’elle conduit l’utilisateur final à recourir à un service qui collecte et conserve des données personnelles non nécessaires au regard de la finalité de recherche d’information. Tel est notamment le cas de la conservation de données personnelles liées aux recherches effectuées lorsque le traitement est effectué à des fins de recherches d’informations.

# Exposé sommaire

Cet amendement définit les obligations respectives des responsables conjoints de traitement que sont les fabricants ou distributeurs de terminal, fixe ou mobile permettant l’accès à des services de communication au public en ligne.

L’article 26.1 du règlement (UE) 2016/679 permet au droit national de définir les « obligations respectives » auquel les responsables conjoints du traitement sont soumis. L’amendement précise ainsi que les fabricants ou distributeurs s’assurent que seules les données nécessaires au regard de chaque finalité spécifique du traitement sont traitées et que le caractère libre du consentement est respecté.

En effet, la quasi-totalité des smartphones commercialisés en France et en Europe sont équipés d’un système d’exploitation mobile iOS ou Android qui impose par défaut le même moteur de recherche à leurs utilisateurs, à travers différents points d’entrée (navigateur, barre de recherche, assistant vocal…). Ce moteur de recherche collecte, conserve et analyse l’intégralité des requêtes de chaque individu, par l’effet d’un contrat d’adhésion qui crée la fiction d’un consentement, en réalité obtenu faute de proposition alternative. Or le niveau d’intrusion dans la vie privée que permet un tel moteur de recherche utilisé quotidiennement par des dizaines de millions de Français est considérable, et il appartient donc au législateur d’apporter aux citoyens la protection de la loi.

Imposer l’utilisation d’un seul service collectant des données à caractère personnel pour d’autres fins que celle de la fourniture de service en question ne permettrait pas d’obtenir un consentement libre de la personne concernée.

Or, actuellement, par l’effet de clauses insérées dans les contrats de licences liés au système Android, les fabricants et distributeurs de terminaux mobiles qui souhaitent utiliser ce système d’exploitation et donner accès à son écosystème d’applications n’ont pas d’autres choix que d’installer par défaut ce moteur de recherche sur tous les points d’entrée possibles. Dans son article 25, le Règlement général à la protection des données (RGPD) impose au contraire d’adopter des principes de « protection par défaut » des données personnelles des individus.

Il apparaît donc important que chaque responsable du traitement conjoint ait pour obligation de s’assurer du respect de la protection des données par défaut (article 25.2 du RGPD) et du caractère libre du consentement.

Le présent amendement vise donc à rendre nulles de telles clauses contraires à l’esprit du RGPD, afin que les fabricants et distributeurs puissent offrir aux utilisateurs finaux des alternatives plus respectueuses de leur vie privée.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-17/amendement-169" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-19" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

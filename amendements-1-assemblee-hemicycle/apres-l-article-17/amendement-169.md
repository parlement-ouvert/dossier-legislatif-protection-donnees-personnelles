<!-- TITLE: Amendement n° 169 -->
<!-- SUBTITLE: Retiré -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-17/amendement-173" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Émilie Cariou**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 17](/pjl-592/titre-ii/chapitre-vi/article-17), insérer l'article suivant:

# Titre

I. – La section II du chapitre III du titre II de la première partie du Livre de procédures fiscales est complétée par un article L. 135 ZJ ainsi rédigé :

« _Art. L. 135 ZJ._ – Afin d’assurer l’exécution de sa mission, la Commission nationale de l’informatique et des libertés peut obtenir des informations et documents conformément au règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité et à la loi n° 78‑17 du 6 janvier 1978 précitée, en particulier au III de l’article 44, dans sa rédaction issue de la présente loi.

II. – Le rapport public mentionné au dernier alinéa de l’article 11 de la loi n° 78‑17 du 6 janvier 1978 précitée présente les éléments quantitatifs et qualitatifs relatifs aux échanges d’informations entre la Commission nationale de l’informatique et des libertés et l’administration fiscale ainsi que les suites qu’y sont données.

# Exposé sommaire

Il s'agit ici de renforcer le droit pour la Commission nationale de l’informatique et des libertés (CNIL) d'obtenir des informations de l'administration fiscale.

Formellement, le droit de communication de la Commission nationale de l’informatique et des libertés (CNIL) est défini assez largement dans la loi de 1978. Néanmoins, bien imposer à l’administration fiscale un travail commun avec la commission via un texte spécifique directement dans la législation fiscale permettrait de clarifier le sujet et d’obtenir un suivi approfondi.

La capacité à identifier les flux financiers relatifs aux données peut notamment passer par la compétence technique de l’Administration fiscale, et pousser ainsi l’inscription de la collaboration entre Commission nationale de l’informatique et des libertés (CNIL) et Bercy peut selon nous se révéler à l’avenir productif, en particulier pour des grands responsables de traitement de données agissant à l’échelon multinational, pour lesquels les moyens de Bercy bien employés pourraient s’avérer particulièrement utiles.

Dans leur rapport sur l’évolution de la fiscalité du numérique, Nicolas Colin et Pierre Collin rappellent qu’une régulation efficace du numérique passe par une action proportionnée et résolue (P. Collin et N. Colin, Mission d’expertise sur la fiscalité de l’économie numérique, 2013 p. 130). Le présent amendement appelle à bien mettre en réseau acteurs de la régulation fiscale et CNIL, pour l’exercice de la souveraineté numérique française et européenne. La progression prochaine en matière de données et de leur exploitation par l’administration fiscale, annoncée par Monsieur le Premier Ministre ce 31 janvier, rend d’autant plus utile ce dispositif appelé par notre amendement.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-17/amendement-173" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

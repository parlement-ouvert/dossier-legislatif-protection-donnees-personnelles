<!-- TITLE: Amendement n° 78 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-4/amendement-77" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-4/amendement-161" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Jean-Louis Masson**

[ARTICLE 4](/pjl-592/titre-i/chapitre-i/article-4)

# Titre

Après l’alinéa 10, insérer les deux alinéas suivants :

« La procédure d’enquête et d’instruction de la Commission nationale de l’informatique et des libertés est secrète.

« Toute personne qui concourt à cette procédure est tenue au secret professionnel dans les conditions et sous les peines des articles 226‑13 et 226‑14 du code pénal. »

# Exposé sommaire

La rédaction actuelle de la loi Informatique et Libertés permet à la CNIL de communiquer sur les contrôles qu’elle effectue.

Or, dans le cadre de ces procédures de contrôle et d’instruction, il est important que le nom des entreprises faisant l’objet d’une telle procédure ne soit pas cité avant qu’une quelconque sanction devenue définitive soit prononcée à leur encontre.

En effet, la publicité d’un contrôle à l’encontre d’une entreprise laisse présumer la non-conformité de ses traitements de données personnelles dans l’esprit du client, alors que la confiance est un élément fondamental dans les relations entre individus et entreprises.

Dès lors, il est donc nécessaire que la confidentialité de ces procédures soit assurée au niveau de l’enquête, tel le secret de l’instruction dans les enquêtes pénales, afin de ne pas porter atteinte prématurément et inutilement à l’image des entreprises contrôlées.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-4/amendement-77" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-4/amendement-161" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

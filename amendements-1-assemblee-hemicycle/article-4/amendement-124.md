<!-- TITLE: Amendement n° 124 -->
<!-- SUBTITLE: Retiré -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-4/amendement-161" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza**

[ARTICLE 4](/pjl-592/titre-i/chapitre-i/article-4)

# Titre

Après l’alinéa 10, insérer les quatre alinéas suivants :

« 4° _bis_ Le IV est ainsi modifié :

« _a)_ Après le mot : « traitement », la fin est ainsi rédigée : « est soumis aux modalités de contrôles prévues à l’alinéa suivant. » ;

« _b)_ Il est ajouté un alinéa ainsi rédigé :

« La conformité de ces traitements est contrôlée, en coopération avec la Commission nationale de contrôle des techniques de renseignement, par un ou plusieurs membres de la Commission nationale de l’informatique et des libertés désignés par le président parmi les membres appartenant ou ayant appartenu au Conseil d’État, à la Cour de cassation ou à la Cour des comptes. Le contrôle est effectué dans des conditions permettant d’en assurer la confidentialité. Les conclusions du contrôle sont remises au seul ministre compétent. Les conditions de mise en œuvre de cette procédure sont précisées par décret en Conseil d’État, pris après avis de la Commission nationale de l’informatique et des libertés. » ; ».

# Exposé sommaire

En l’état du droit, les pouvoirs de contrôle général des fichiers reconnus à la CNIL ne s’appliquent pas à certains traitements intéressant la sûreté de l’État (IV de l’article 44 de la loi de 1978). Huit fichiers sont aujourd’hui concernés : le fichier de la direction générale de la sécurité intérieure (DGSI), le fichier de la direction générale de la sécurité extérieure (DGSE), le fichier SIREX de la direction du renseignement et de la sécurité de la défense (DRSD), le fichier de la direction du renseignement militaire (DRM), le fichier BCR-DNRED de la direction nationale du renseignement et des enquêtes douanières (DNRED), le fichier de personnes étrangères mis en œuvre par la DRM, le fichier dénommé Gestion du terrorisme et des extrémismes violents (GESTEREXT) mis en œuvre par la préfecture de police et le fichier dénommé “ BIOPEX ” mis en œuvre par la DRM.

Il n’existe donc, à l’heure actuelle, aucun contrôle en aval de ces fichiers, permettant de garantir qu’ils sont mis en œuvre dans le respect de la protection des données personnelles et des textes applicables en la matière, auxquels ils sont pourtant soumis. Il est impossible, par exemple, de savoir s’ils respectent effectivement les prescriptions fixées par les textes réglementaires qui les créent (finalités, catégories de données, durée de conservation). Le seul contrôle dont ils font l’objet est celui réalisé par la Commission nationale de contrôle des techniques de renseignement (CNCTR) lorsqu’elle émet un avis préalablement à la mise en œuvre d’une technique de renseignement susceptible d’alimenter ces fichiers.

Compte tenu des quantités de données qui abondent les fichiers de renseignement, il est indispensable de renforcer le contrôle de leur mise en œuvre. Tel est l’objet du présent amendement qui vise à aménager les conditions dans lesquelles ces fichiers peuvent être contrôlés, de manière globale et a posteriori, en confiant cette compétence à la CNIL, selon des modalités adaptées.

Ces contrôles viseraient à s’assurer du respect de la protection des données personnelles et, en aucun cas, à contrôler l’activité des services spécialisés. En pratique, ils ne viseraient pas à apprécier la pertinence et la réalité de telle ou telle information contenue dans le fichier, mais les conditions de mise en œuvre globale du fichier au regard de la loi « Informatique et libertés » : catégories de données collectées, durée de conservation, destinataires de ces données, mesures de sécurité apportées au traitement ou encore éventuels interconnexions et transferts de données entre fichiers...

Ces contrôles seraient réalisés en coopération avec la CNCTR et ne pourraient être effectués que par des membres de la CNIL chargés du « droit d’accès indirect », qui ont une connaissance des services et des fichiers concernés. Ces magistrats ou anciens magistrats du Conseil d’État, de la Cour de cassation ou de la Cour des comptes seraient, le cas échéant, accompagnés des seuls agents du service des contrôles de la CNIL habilités « secret défense ». Leur déroulement serait conforme aux dispositions de l’instruction générale interministérielle n° 1 300 sur la protection du secret de la défense nationale, qui traite notamment de la collecte, de la conservation et de l’échange des données classifiées. Les conclusions de ces contrôles seraient exclusivement communiquées au ministre responsable du traitement ayant fait l’objet du contrôle selon des modalités sécurisées.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-4/amendement-161" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

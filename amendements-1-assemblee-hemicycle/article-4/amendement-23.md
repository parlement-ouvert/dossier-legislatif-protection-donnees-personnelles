<!-- TITLE: Amendement n° 23 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-4/amendement-76" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-4/amendement-24" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Emmanuelle Ménard**

[ARTICLE 4](/pjl-592/titre-i/chapitre-i/article-4)

# Titre

Supprimer la seconde phrase de l’alinéa 8.

# Exposé sommaire

Le secret médical est consacré par l’article 4 du Code de déontologie médicale selon lequel « Le secret professionnel, institué dans l’intérêt des patients, s’impose à tout médecin dans les conditions établies par la loi. Le secret couvre tout ce qui est venu à la connaissance du médecin dans l’exercice de sa profession, c’est à dire non seulement ce qui lui a été confié mais aussi ce qu’il a vu, entendu ou compris ».

Bien que dans certaines circonstances, la loi permet de lever l’obligation pour le médecin de garder secrètes les informations relatives à un patient, il ne faut pas oublier que l’article 226‑13 du code pénal sanctionne clairement la révélation d’une « information à caractère secret ». En effet, par principe, toute divulgation relative au secret médical constitue donc un délit puni d’un an d’emprisonnement et 15 000 euros d’amende.

C’est dire si le secret médical revêt une importance particulière dans le lien de confiance qui doit exister entre le patient et son médecin.

Or, le présent alinéa instaure une nouvelle exception à ce droit. C’est une atteinte grave à l’obligation de discrétion et de respect de la personne.

Atteinte d’autant plus grave que le présent alinéa n’encadre pas précisément les cas où des informations médicales pourront être révélées.

C’est pourquoi il convient de supprimer cet alinéa.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-4/amendement-76" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-4/amendement-24" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 7 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-6/amendement-46" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-7/amendement-8" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Amendement n° 8 — Non soutenu](/amendements-1-assemblee-hemicycle/article-7/amendement-8)
* [Amendement n° 139 — Non soutenu](/amendements-1-assemblee-hemicycle/article-7/amendement-139)
* [Amendement n° 106 — Rejeté](/amendements-1-assemblee-hemicycle/article-7/amendement-106)
* [Amendement n° 64 — Adopté](/amendements-1-assemblee-hemicycle/article-7/amendement-64)
* [Amendement n° 112 — Adopté](/amendements-1-assemblee-hemicycle/article-7/amendement-112)
* [Amendement n° 101 — Adopté](/amendements-1-assemblee-hemicycle/article-7/amendement-101)
* [Amendement n° 50 — Rejeté](/amendements-1-assemblee-hemicycle/article-7/amendement-50)
* [Amendement n° 65 — Rejeté](/amendements-1-assemblee-hemicycle/article-7/amendement-65)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-6/amendement-46" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-7/amendement-8" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 79 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-6/amendement-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-6/amendement-47" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Jean-Louis Masson**

[ARTICLE 6](/pjl-592/titre-i/chapitre-i/article-6)

# Titre

I. – Après le mot :

« droits »

supprimer l’alinéa 7.

II – En conséquence, après le mot :

« loi »

supprimer l’alinéa 32.

# Exposé sommaire

L’article 6 du présent projet de loi prévoit les sanctions prises par la CNIL en cas de méconnaissance par le responsable de traitement ou le sous-traitant de ses obligations découlant du règlement ou de la loi.

Il prévoit notamment une injonction qui peut être assortie d’une astreinte pouvant atteindre 100 000 € par jour. Or, si l’injonction est une mesure prévue par le règlement (UE) 2016/679, l’astreinte n’est pas prévue.

Par ailleurs, le seuil des sanctions pécuniaires pouvant être prononcées par la CNIL a été considérablement augmenté, puisqu’il peut atteindre 20 millions d’euros ou 4 % du chiffre d’affaires annuel mondial d’une entreprise.

Dès lors, l’ajout d’une sanction pécuniaire supplémentaire sous forme d’astreinte parait excessive et constituerait une spécificité française pouvant nuire à l’attractivité de la France et dissuader les entreprises de s’y installer.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-6/amendement-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-6/amendement-47" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

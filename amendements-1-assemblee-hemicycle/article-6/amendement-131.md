<!-- TITLE: Amendement n° 131 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-6" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-6/amendement-48" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Jean-Félix Acquaviva**

[ARTICLE 6](/pjl-592/titre-i/chapitre-i/article-6)

# Titre

Après l’alinéa 4, insérer l’alinéa suivant :

« Les sanctions prononcées par la Commission nationale informatique et libertés sont strictement proportionnées à la gravité de l’infraction dans le respect des principes généraux, en particulier de l’individualisation de la sanction, de la prise en compte de toutes les circonstances, du principe de proportionnalité et de l’égalité de traitement. Une grille d’évaluation de la gravité des infractions est fixée par décret en Conseil d’État »

# Exposé sommaire

Par ce projet de loi, la CNIL pourra prononcer des sanctions. Tel que l’article est rédigé, le pouvoir de sanction ne semble pas assez encadré.

C’est pourquoi, cet amendement entend réaffirmer le respect des principes généraux pour protéger les libertés fondamentales ainsi que la publication d’une grille d’évaluation de la gravité des infractions.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-6" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-6/amendement-48" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

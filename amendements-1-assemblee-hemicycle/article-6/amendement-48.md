<!-- TITLE: Amendement n° 48 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-6/amendement-131" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-6/amendement-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Danièle Obono**

[ARTICLE 6](/pjl-592/titre-i/chapitre-i/article-6)

# Titre

Après l’alinéa 4, insérer l’alinéa suivant :

« Les sanctions prises par la Commission nationale informatique et libertés sont strictement modulées en fonction de la gravité de l’infraction en tenant compte principalement de la gravité de l’entrave directe ou indirecte aux droits et libertés constitutionnellement garantis, de la sensibilité des données au sens de la présente loi et de l’usage qui en est fait, de l’effectivité d’une sanction financière eu égard au chiffre d’affaires de l’entreprise concernée. Une grille d’astreintes est établie en fonction de ces critères dans des conditions fixées par décret en Conseil d’État. »

# Exposé sommaire

La compétence d’établir des sanctions pécuniaires octroyée à la CNIL prend de nouvelles dimensions à la suite de la nouvelle répartition des compétences organisée par ce projet de loi. Délaissant en grande partie sa compétence d’autorisation a priori pour se concentrer sur des fonctions de sanction a posteriori , La CNIL voit son rôle d’organe sanctionnant devenir l’une de ses missions premières. Or l’octroi d’un pouvoir de sanction doit être accompagné d’un strict encadrement des motifs légaux de la sanction, afin qu’une protection optimale des libertés fondamentales de tous et toutes soit assurée. Ainsi cet amendement dessine les conditions du bien fondé légal de la sanction.

Cela permettrait non seulement à la CNIL mais également aux organes juridictionnels de mêmes qu’aux citoyen.ne.s d’évaluer au plus juste le sens et donc le fondement de la sanction.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/article-6/amendement-131" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-6/amendement-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

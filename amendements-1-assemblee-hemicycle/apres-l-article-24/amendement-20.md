<!-- TITLE: Amendement n° 20 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-24" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <span> </span> </div>

**Amendement de M. Bruno Bonnell**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 24](/pjl-592/titre-v/article-24), insérer l'article suivant:

# Titre

Après l’alinéa 1 de l’article L. 341‑1 du code de la propriété intellectuelle, il est inséré un alinéa ainsi rédigé :

« Le citoyen, entendu comme la personne humaine qui consent à faire exploiter ses données, jouit des droits moraux sur les données personnelles qu’il génère individuellement ou par l’intermédiaire des outils numériques qu’il utilise. »

# Exposé sommaire

Toutes les mesures actuelles sont orientées vers la protection des données. Il y a un manque d’information du public sur le stockage et le traitement de leurs données personnelles, qu’elles soient statiques (nom, prénom, adresse…) ou générées par l’utilisateur (posts, likes. historiques de navigation, d’achat, de géolocalisation…).

Il existe le droit des brevets, les droits d’auteurs, mais « la propriété intellectuelle » n’est pas complète car il n’existe rien sur les données personnelles, qui sont de fait la quasi-propriété des gestionnaires, souvent les GAFAMI/BATX, données qu’ils utilisent à des fins commerciales.

Il convient dans un premier temps de rendre à tout individu ses droits moraux sur les données qu’il génère, il aura ainsi la possibilité de les léguer à ses héritiers, d’en autoriser l’usage par des tiers. C’est aussi un moyen pour que les individus se rendent compte des données qu’ils génèrent et de les gérer dans leur intérêt.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-24" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <span> </span> </div>

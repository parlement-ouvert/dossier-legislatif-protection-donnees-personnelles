<!-- TITLE: Amendement n° 71 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16-a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Loïc Prud'homme**

ARTICLE ADDITIONNEL

APRÈS L'[ARTICLE 15](/pjl-592/titre-ii/chapitre-v/article-15), insérer l'article suivant:

# Titre

Après le deuxième alinéa de l’article L. 341‑4 du code de l’énergie est inséré un alinéa ainsi rédigé :

« Il ne peut être procédé à une installation de compteurs dits « intelligents », tels les compteurs nommés « Linky », « Gazpar » et équivalents, sans le consentement exprès et écrit des personnes dont le compteur permet de collecter et de transmettre des informations relatives à sa consommation. Toute installation réalisée sans ce consentement est constitutive d’un délit d’atteinte à la vie privée tel que prévu à l’article 226‑4 du code pénal. »

# Exposé sommaire

Par cet amendement, nous souhaitons mettre fin à une atteinte majeure aux droits et libertés fondamentales numériques en l’espèce le droit au consentement En effet, si l’objectif affirmé par la directive 2006/32/CE du 5 avril 2006 relative à l'efficacité énergétique semble un objectif d’intérêt suffisant pour justifier de tempérer de tels droits et libertés numérique, il apparaît toutefois que la méthode de collecte centralisée utilisée (qui relève clairement d’une volonté de monétiser à terme du big data) n’apparaît absolument pas la meilleure des méthodes pour atteindre le but d’efficacité énergétique, en respectant le droit au consentement. Dans ce cadre, nous estimons que la transposition du règlement RGPD ([http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679&amp;from=FR](http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679&amp;from=FR)) implique de rendre obligatoire l’obtention du consentement des personnes chez qui l’on suite implanter des compteurs Linky et Gaspard.

Cet amendement a par ailleurs un lien direct (voire a minima indirect avec le texte), cf articles 44 et 45 de la Constitution.

Le règlement RGPD de 2016 précise à son article 4 - définitions \- : « consentement» de la personne concernée, toute manifestation de volonté, libre, spécifique, éclairée et univoque par laquelle la personne concernée accepte, par une déclaration ou par un acte positif clair, que des données à caractère personnel la concernant fassent l'objet d'un traitement », et encore à l’article 6 – licéité du traitement – « 1. Le traitement n'est licite que si, et dans la mesure où, au moins une des conditions suivantes est remplie : a) la personne concernée a consenti au traitement de ses données à caractère personnel pour une ou plusieurs finalités spécifiques ; (…) e) le traitement est nécessaire à l'exécution d'une mission d'intérêt public ou relevant de l'exercice de l'autorité publique dont est investi le responsable du traitement; f) le traitement est nécessaire aux fins des intérêts légitimes poursuivis par le responsable du traitement ou par un tiers, à moins que ne prévalent les intérêts ou les libertés et droits fondamentaux de la personne concernée qui exigent une protection des données à caractère personnel, notamment lorsque la personne concernée est un enfant. ».

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/apres-l-article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-16-a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 86 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-91" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Erwan Balanant**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 14](/pjl-592/titre-ii/chapitre-v/article-14) A, insérer l'article suivant:

# Titre

L’article 7 de la loi n° 78‑17 du 6 janvier 1978 précitée est complété par six alinéas ainsi rédigés :

« Le consentement de la personne concernée doit être obtenu de manière loyale et résulter d’une action volontaire, explicite, libre, spécifique. Il ne doit pas être exigé en contrepartie d’un bien ou d’un service à moins que le traitement faisant l’objet du consentement ne soit indispensable à la fourniture de ce bien ou service. »

« Pour être valide le consentement de la personne doit résulter d’une action informée, en toute connaissance de la finalité réservée à ses données, notamment dans le cas où les données seraient susceptibles d’être cédées, vendues ou encore transférées. »

« Si la possibilité de refuser le consentement est dissociée de son acceptation, les deux propositions doivent être accolées et présentées sous la même forme et notamment dans les mêmes tailles de caractère, couleur, typographie ou intensité sonore. »

« L’objet du consentement doit être spécifique et comporter le nom de la personne morale ou physique responsable du traitement ainsi que la finalité du traitement. Si plusieurs personnes morales ou physiques sont destinataires des données collectées, le consentement doit être donné de manière distincte pour chaque destinataire. »

« Le consentement ne peut pas être présumé et, à ce titre, ne doit pas être présenté sous la forme d’une case précochée, d’un bouton radio ou d’une liste d’options, prédéfinis sur l’acceptation. Il ne peut être recueilli que par une action spécifique de la personne concernée sans aucune autre finalité. Cela implique notamment que cette action n’ait pas pour conséquence de faire disparaître l’option de refus, de diriger la navigation vers une nouvelle page, ou de valider l’envoi d’un formulaire. »

« Le consentement ne peut être obtenu de manière indirecte, notamment par l’acceptation de conditions générales ou d’un règlement. De ce fait, une information faisant mention de la finalité réservée aux données personnelles, rédigée de façon claire, visible, dans des termes aisément compréhensibles devra apparaître lorsque le consentement de la personne sera demandé. »

# Exposé sommaire

Cet amendement vise à intégrer la définition du consentement donnée par la CNIL et le G29, il définit les qualités essentielles que doivent revêtir le consentement au traitement des données personnelles, qui sont la loyauté, le caractère volontaire, libre, spécifique et informé. Le consentement doit être délivré de manière libre et spécifique, cela est réaffirmé par la CNIL dans l’article 6 de la norme simplifiée NS-48.

Cet amendement vise également à renforcer l’information donnée aux consommateurs, en reprenant la norme simplifiée (voir _supra_ ) de la CNIL. Pour cela, l’accent doit notamment être mis sur l’utilisation que le site Internet fera des données personnelles, plus particulièrement lorsque le site a pour ambition de vendre les données récoltées. Il s’agit de mettre en place une information claire, compréhensible et visible pour l’utilisateur, grâce à laquelle il aura connaissance de la finalité du traitement. Par conséquent, ce message informatif devra être communiqué autrement que par des conditions générales ou un règlement.

Nous estimons que même si l’utilisateur d’un service ne peut pas se prévaloir d’un droit de propriété sur les données personnelles transférées _via_ Internet, le Conseil d ’État étant à l’origine de ce raisonnement, l’utilisateur doit être pleinement informé de ce à quoi il consent concernant la finalité du traitement de ses données.

Sources citées

G29, définition du consentement :

https ://www.cnil.fr/sites/default/files/atoms/files/wp259_enpdf_consent.pdf

Norme simplifiée NS-048 de la CNIL :

https ://www.legifrance.gouv.fr/affichTexte.do ?cidTexte=JORFTEXT000033117877&amp;dateTexte=&amp;categorieLien=id

Étude annuelle du Conseil d’État, « Le numérique et les droits fondamentaux », 2014 :

http ://www.ladocumentationfrancaise.fr/var/storage/rapports-publics/144000541.pdf

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-91" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

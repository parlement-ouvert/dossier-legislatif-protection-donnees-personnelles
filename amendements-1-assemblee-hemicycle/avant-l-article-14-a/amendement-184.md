<!-- TITLE: Amendement n° 184 -->
<!-- SUBTITLE: Adopté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-108" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-14-a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Paula Forteza, rapporteure au nom de la commission des lois**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 14](/pjl-592/titre-ii/chapitre-v/article-14) A, insérer l'article suivant:

# Titre

Au premier alinéa de l’article 7 de la loi n° 78‑17 du 6 janvier 1978 précitée, après le mot :

« concernée »,

sont insérés les mots :

« , dans les conditions mentionnées au 11 de l’article 4 et à l’article 7 du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données, »

# Exposé sommaire

L'article 7 de la loi du 6 janvier 1978 prévoit que, sous réserve de certaines exceptions, un traitement de données à caractère personnel doit avoir reçu le consentement de la personne concernée.

De manière à préciser le sens de cette obligation, le présent amendement propose de renvoyer explicitement aux dispositions du règlement européen précisant les conditions dans lesquelles le consentement doit être recueilli.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-108" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/article-14-a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

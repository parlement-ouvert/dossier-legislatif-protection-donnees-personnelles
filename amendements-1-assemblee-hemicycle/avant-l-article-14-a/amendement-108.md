<!-- TITLE: Amendement n° 108 -->
<!-- SUBTITLE: Non soutenu -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-183" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-184" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de M. Stéphane Peu**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 14](/pjl-592/titre-ii/chapitre-v/article-14) A, insérer l'article suivant:

# Titre

L’article 7 de la loi n° 78‑17 du 6 janvier 1978 précitée est complété par un alinéa ainsi rédigé :

« Pour être valide, le consentement de la personne concernée doit résulter d’une action volontaire, explicite, libre, spécifique et informée. Cela implique notamment que son consentement ne soit pas exigé en contrepartie d’un bien ou d’un service, à moins que le traitement faisant l’objet du consentement ne soit indispensable à la fourniture de ce bien ou service. »

# Exposé sommaire

Cet amendement préconisé par la Quadrature du Net vise à intégrer la définition déterminante donnée par la CNIL et le G29 du caractère libre du consentement.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-183" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-184" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

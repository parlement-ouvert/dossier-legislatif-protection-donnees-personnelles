<!-- TITLE: Amendement n° 91 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-86" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-92" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Blandine Brocard**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 14](/pjl-592/titre-ii/chapitre-v/article-14) A, insérer l'article suivant:

# Titre

L’article 7 de la loi n° 78‑17 du 6 janvier 1978 précitée est complété par deux alinéas ainsi rédigés :

« Le consentement ne peut pas être présumé et, à ce titre, ne doit pas être présenté sous une forme prédéfinie sur l’acceptation.

« Le consentement doit être recueilli par une action spécifique de la personne concernée sans que cette action n’ait aucune autre finalité. Cela implique notamment que cette action n’ait pas pour conséquence de faire disparaître l’option de refus, de diriger la navigation vers une nouvelle page, ou de valider l’envoi d’un formulaire.»

# Exposé sommaire

Cet amendement vise à définir la manière selon laquelle un consentement au traitement des données personnelles peut être obtenu et à empêcher la dilution ou le masquage du consentement dans des formulations trompeuses.

Le premier alinéa interdit par exemple l’utilisation de cases d’acceptation précochées dans un formulaire.

Le second alinéa interdit l’utilisation d’artifices pour extorquer le consentement, par exemple, comme on le voit souvent, un gros bouton « Je m’inscris » suivi d’une phrase à peine lisible indiquant « En m’inscrivant, j’accepte que mes données personnelles soient utilisées pour... des finalités qui n’ont rien à voir avec l’objet de l’inscription ».

Il est fréquent également que l’action de consentement ait pour effet de faire disparaître l’option de refus avant même qu’elle ait pu être lue.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-86" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-92" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Amendement n° 92 -->
<!-- SUBTITLE: Rejeté -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-91" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-183" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

**Amendement de Mme Blandine Brocard**

ARTICLE ADDITIONNEL

AVANT L'[ARTICLE 14](/pjl-592/titre-ii/chapitre-v/article-14) A, insérer l'article suivant:

# Titre

L’article 7 de la loi n° 78‑17 du 6 janvier 1978 précitée est complété par deux alinéas ainsi rédigés :

« Le consentement de la personne concernée doit être obtenu de manière loyale et résulter d’une action volontaire, explicite, libre, spécifique et informée.

« La Commission nationale de l’informatique et de libertés adopte une norme précisant la manière dont le consentement peut être obtenu pour être conforme aux alinéas précédents. Cette norme est révisée annuellement pour tenir compte des évolutions techniques et des nouvelles pratiques observées dans le cadre de l’obtention du consentement. »

# Exposé sommaire

La CNIL établit un cadre règlementaire définissant précisément les méthodes qui peuvent être utilisées, ou qui ne peuvent pas l’être, pour obtenir le consentement.

Par l’observation des nouvelles plaintes transmises, par sa mission de contrôle, la CNIL pourra enrichir et réévaluer ce cadre règlementaire afin de tenir compte des nouvelles technologies ou des pratiques jugées non conformes.

L’établissement d’un cahier des charges précis sur le recueil du consentement permettra :

\- aux personnes concernées d’avoir confiance dans la manière dont leur consentement est recueilli sans avoir à se méfier constamment des « petites lignes trompeuses ».

\- aux responsables de traitement de connaître précisément les pratiques autorisées et interdites lors de la mise en forme de leurs formulaires de recueil de consentement.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-91" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Amendements en 1ère lecture à l'hémicycle de l'Assemblée</span> <a href="/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-183" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

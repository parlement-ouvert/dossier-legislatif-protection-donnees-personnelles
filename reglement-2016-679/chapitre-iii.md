<!-- TITLE: CHAPITRE III -->
<!-- SUBTITLE: Droits de la personne concernée -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-ii/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-iii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Section 1 — Transparence et modalités](/reglement-2016-679/chapitre-iii/section-1)

  * [Article 12 — Transparence des informations et des communications et modalités de l'exercice des droits de la personne concernée](/reglement-2016-679/chapitre-iii/section-1/article-12)

* [Section 2 — Information et accès aux données à caractère personnel](/reglement-2016-679/chapitre-iii/section-2)

  * [Article 13 — Informations à fournir lorsque des données à caractère personnel sont collectées auprès de la personne concernée](/reglement-2016-679/chapitre-iii/section-2/article-13)
  * [Article 14 — Informations à fournir lorsque les données à caractère personnel n'ont pas été collectées auprès de la personne concernée](/reglement-2016-679/chapitre-iii/section-2/article-14)
  * [Article 15 — Droit d'accès de la personne concernée](/reglement-2016-679/chapitre-iii/section-2/article-15)

* [Section 3 — Rectification et effacement](/reglement-2016-679/chapitre-iii/section-3)

  * [Article 16 — Droit de rectification](/reglement-2016-679/chapitre-iii/section-3/article-16)
  * [Article 17 — Droit à l'effacement («droit à l'oubli»)](/reglement-2016-679/chapitre-iii/section-3/article-17)
  * [Article 18 — Droit à la limitation du traitement](/reglement-2016-679/chapitre-iii/section-3/article-18)
  * [Article 19 — Obligation de notification en ce qui concerne la rectification ou l'effacement de données à caractère personnel ou la limitation du traitement](/reglement-2016-679/chapitre-iii/section-3/article-19)
  * [Article 20 — Droit à la portabilité des données](/reglement-2016-679/chapitre-iii/section-3/article-20)

* [Section 4 — Droit d'opposition et prise de décision individuelle automatisée](/reglement-2016-679/chapitre-iii/section-4)

  * [Article 21 — Droit d'opposition](/reglement-2016-679/chapitre-iii/section-4/article-21)
  * [Article 22 — Décision individuelle automatisée, y compris le profilage](/reglement-2016-679/chapitre-iii/section-4/article-22)

* [Section 5 — Limitations](/reglement-2016-679/chapitre-iii/section-5)

  * [Article 23 — Limitations](/reglement-2016-679/chapitre-iii/section-5/article-23)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [28.](/avis-conseil-etat-393836/donnees-dinfraction/28)
* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 36](/loi-78-17-pjl-592/chapitre-v/section-1/article-36)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-ii/article-11" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-iii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

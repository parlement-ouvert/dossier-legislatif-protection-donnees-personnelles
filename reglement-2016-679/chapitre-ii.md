<!-- TITLE: CHAPITRE II -->
<!-- SUBTITLE: Principes -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-i/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-ii/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 5 — Principes relatifs au traitement des données à caractère personnel](/reglement-2016-679/chapitre-ii/article-5)
* [Article 6 — Licéité du traitement](/reglement-2016-679/chapitre-ii/article-6)
* [Article 7 — Conditions applicables au consentement](/reglement-2016-679/chapitre-ii/article-7)
* [Article 8 — Conditions applicables au consentement des enfants en ce qui concerne les services de la société de l'information](/reglement-2016-679/chapitre-ii/article-8)
* [Article 9 — Traitement portant sur des catégories particulières de données à caractère personnel](/reglement-2016-679/chapitre-ii/article-9)
* [Article 10 — Traitement des données à caractère personnel relatives aux condamnations pénales et aux infractions](/reglement-2016-679/chapitre-ii/article-10)
* [Article 11 — Traitement ne nécessitant pas l'identification](/reglement-2016-679/chapitre-ii/article-11)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-i/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-ii/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

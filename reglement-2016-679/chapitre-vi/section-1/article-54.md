<!-- TITLE: Article 54 -->
<!-- SUBTITLE: Règles relatives à l'établissement de l'autorité de contrôle -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vi/section-1/article-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vi/section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Chaque État membre prévoit, par la loi, tous les éléments suivants:

<table>
<tr>
<td>

a)

</td>
<td>

la création de chaque autorité de contrôle;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

les qualifications et les conditions d'éligibilité requises pour être nommé membre de chaque autorité de contrôle;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

les règles et les procédures pour la nomination du ou des membres de chaque autorité de contrôle;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

la durée du mandat du ou des membres de chaque autorité de contrôle, qui ne peut être inférieure à quatre ans, sauf pour le premier mandat après le 24 mai 2016, dont une partie peut être d'une durée plus courte lorsque cela est nécessaire pour protéger l'indépendance de l'autorité de contrôle au moyen d'une procédure de nominations échelonnées;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

le caractère renouvelable ou non du mandat du ou des membres de chaque autorité de contrôle et, si c'est le cas, le nombre de mandats;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

les conditions régissant les obligations du ou des membres et des agents de chaque autorité de contrôle, les interdictions d'activités, d'emplois et d'avantages incompatibles avec celles-ci, y compris après la fin de leur mandat, et les règles régissant la cessation de l'emploi.

</td> </tr> </table>

2\. Le ou les membres et les agents de chaque autorité de contrôle sont soumis, conformément au droit de l'Union ou au droit des États membres, au secret professionnel concernant toute information confidentielle dont ils ont eu connaissance dans l'exercice de leurs missions ou de leurs pouvoirs, y compris après la fin de leur mandat. Pendant la durée de leur mandat, ce secret professionnel s'applique en particulier au signalement par des personnes physiques de violations du présent règlement.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vi/section-1/article-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vi/section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-vi/section-1/article-54' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

<!-- TITLE: Section 2 -->
<!-- SUBTITLE: Compétence, missions et pouvoirs -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vi/section-1/article-54" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vi/section-2/article-55" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 55 — Compétence](/reglement-2016-679/chapitre-vi/section-2/article-55)
* [Article 56 — Compétence de l'autorité de contrôle chef de file](/reglement-2016-679/chapitre-vi/section-2/article-56)
* [Article 57 — Missions](/reglement-2016-679/chapitre-vi/section-2/article-57)
* [Article 58 — Pouvoirs](/reglement-2016-679/chapitre-vi/section-2/article-58)
* [Article 59 — Rapports d'activité](/reglement-2016-679/chapitre-vi/section-2/article-59)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vi/section-1/article-54" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vi/section-2/article-55" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

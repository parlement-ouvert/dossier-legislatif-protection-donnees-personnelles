<!-- TITLE: Article 58 -->
<!-- SUBTITLE: Pouvoirs -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vi/section-2/article-57" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vi/section-2/article-59" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Chaque autorité de contrôle dispose de tous les pouvoirs d'enquête suivants:

<table>
<tr>
<td>

a)

</td>
<td>

ordonner au responsable du traitement et au sous-traitant, et, le cas échéant, au représentant du responsable du traitement ou du sous-traitant, de lui communiquer toute information dont elle a besoin pour l'accomplissement de ses missions;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

mener des enquêtes sous la forme d'audits sur la protection des données;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

procéder à un examen des certifications délivrées en application de l'article 42, paragraphe 7;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

notifier au responsable du traitement ou au sous-traitant une violation alléguée du présent règlement;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

obtenir du responsable du traitement et du sous-traitant l'accès à toutes les données à caractère personnel et à toutes les informations nécessaires à l'accomplissement de ses missions;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

obtenir l'accès à tous les locaux du responsable du traitement et du sous-traitant, notamment à toute installation et à tout moyen de traitement, conformément au droit de l'Union ou au droit procédural des États membres.

</td> </tr> </table>

2\. Chaque autorité de contrôle dispose du pouvoir d'adopter toutes les mesures correctrices suivantes:

<table>
<tr>
<td>

a)

</td>
<td>

avertir un responsable du traitement ou un sous-traitant du fait que les opérations de traitement envisagées sont susceptibles de violer les dispositions du présent règlement;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

rappeler à l'ordre un responsable du traitement ou un sous-traitant lorsque les opérations de traitement ont entraîné une violation des dispositions du présent règlement;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

ordonner au responsable du traitement ou au sous-traitant de satisfaire aux demandes présentées par la personne concernée en vue d'exercer ses droits en application du présent règlement;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

ordonner au responsable du traitement ou au sous-traitant de mettre les opérations de traitement en conformité avec les dispositions du présent règlement, le cas échéant, de manière spécifique et dans un délai déterminé;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

ordonner au responsable du traitement de communiquer à la personne concernée une violation de données à caractère personnel;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

imposer une limitation temporaire ou définitive, y compris une interdiction, du traitement;

</td> </tr> </table>

<table>
<tr>
<td>

g)

</td>
<td>

ordonner la rectification ou l'effacement de données à caractère personnel ou la limitation du traitement en application des articles 16, 17 et 18 et la notification de ces mesures aux destinataires auxquels les données à caractère personnel ont été divulguées en application de l'article 17, paragraphe 2, et de l'article 19;

</td> </tr> </table>

<table>
<tr>
<td>

h)

</td>
<td>

retirer une certification ou ordonner à l'organisme de certification de retirer une certification délivrée en application des articles 42 et 43, ou ordonner à l'organisme de certification de ne pas délivrer de certification si les exigences applicables à la certification ne sont pas ou plus satisfaites;

</td> </tr> </table>

<table>
<tr>
<td>

i)

</td>
<td>

imposer une amende administrative en application de l'article 83, en complément ou à la place des mesures visées au présent paragraphe, en fonction des caractéristiques propres à chaque cas;

</td> </tr> </table>

<table>
<tr>
<td>

j)

</td>
<td>

ordonner la suspension des flux de données adressés à un destinataire situé dans un pays tiers ou à une organisation internationale.

</td> </tr> </table>

3\. Chaque autorité de contrôle dispose de tous les pouvoirs d'autorisation et de tous les pouvoirs consultatifs suivants:

<table>
<tr>
<td>

a)

</td>
<td>

conseiller le responsable du traitement conformément à la procédure de consultation préalable visée à l'article 36;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

émettre, de sa propre initiative ou sur demande, des avis à l'attention du parlement national, du gouvernement de l'État membre ou, conformément au droit de l'État membre, d'autres institutions et organismes ainsi que du public, sur toute question relative à la protection des données à caractère personnel;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

autoriser le traitement visé à l'article 36, paragraphe 5, si le droit de l'État membre exige une telle autorisation préalable;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

rendre un avis sur les projets de codes de conduite et les approuver en application de l'article 40, paragraphe 5;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

agréer des organismes de certification en application de l'article 43;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

délivrer des certifications et approuver des critères de certification conformément à l'article 42, paragraphe 5;

</td> </tr> </table>

<table>
<tr>
<td>

g)

</td>
<td>

adopter les clauses types de protection des données visées à l'article 28, paragraphe 8, et à l'article 46, paragraphe 2, point d);

</td> </tr> </table>

<table>
<tr>
<td>

h)

</td>
<td>

autoriser les clauses contractuelles visées à l'article 46, paragraphe 3, point a);

</td> </tr> </table>

<table>
<tr>
<td>

i)

</td>
<td>

autoriser les arrangements administratifs visés à l'article 46, paragraphe 3, point b);

</td> </tr> </table>

<table>
<tr>
<td>

j)

</td>
<td>

approuver les règles d'entreprise contraignantes en application de l'article 47.

</td> </tr> </table>

4\. L'exercice des pouvoirs conférés à l'autorité de contrôle en application du présent article est subordonné à des garanties appropriées, y compris le droit à un recours juridictionnel effectif et à une procédure régulière, prévues par le droit de l'Union et le droit des États membres conformément à la Charte.

5\. Chaque État membre prévoit, par la loi, que son autorité de contrôle a le pouvoir de porter toute violation du présent règlement à l'attention des autorités judiciaires et, le cas échéant, d'ester en justice d'une manière ou d'une autre, en vue de faire appliquer les dispositions du présent règlement.

6\. Chaque État membre peut prévoir, par la loi, que son autorité de contrôle dispose de pouvoirs additionnels à ceux visés aux paragraphes 1, 2 et 3. L'exercice de ces pouvoirs n'entrave pas le bon fonctionnement du chapitre VII.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Délibération n°2017-299 du 30 novembre 2017 — Commission Nationale de l'Informatique et des Libertés
  * [Article 6 (Mesures correctrices)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-6-mesures-correctrices)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 1ER — MISSIONS DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier/article-1er)
  * [ARTICLE 4 — POUVOIRS DE CONTRÔLE DE LA CNIL](/etude-impact-490/titre-ier/chapitre-ier/article-4)
  * [ARTICLE 6 — MESURES CORRECTRICES ET SANCTIONS](/etude-impact-490/titre-ier/chapitre-ier/article-6)
  * [ARTICLES 16 ET 17 — MODALITES D’EXERCICE DES VOIES DE RECOURS](/etude-impact-490/titre-ii/chapitre-v/articles-16-et-17)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vi/section-2/article-57" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vi/section-2/article-59" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-vi/section-2/article-58' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

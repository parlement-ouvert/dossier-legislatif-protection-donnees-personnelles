<!-- TITLE: Article 57 -->
<!-- SUBTITLE: Missions -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vi/section-2/article-56" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vi/section-2/article-58" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Sans préjudice des autres missions prévues au titre du présent règlement, chaque autorité de contrôle, sur son territoire:

<table>
<tr>
<td>

a)

</td>
<td>

contrôle l'application du présent règlement et veille au respect de celui-ci;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

favorise la sensibilisation du public et sa compréhension des risques, des règles, des garanties et des droits relatifs au traitement. Les activités destinées spécifiquement aux enfants font l'objet d'une attention particulière;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

conseille, conformément au droit de l'État membre, le parlement national, le gouvernement et d'autres institutions et organismes au sujet des mesures législatives et administratives relatives à la protection des droits et libertés des personnes physiques à l'égard du traitement;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

encourage la sensibilisation des responsables du traitement et des sous-traitants en ce qui concerne les obligations qui leur incombent en vertu du présent règlement;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

fournit, sur demande, à toute personne concernée des informations sur l'exercice des droits que lui confère le présent règlement et, si nécessaire, coopère, à cette fin, avec les autorités de contrôle d'autres États membres;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

traite les réclamations introduites par une personne concernée ou par un organisme, une organisation ou une association, conformément à l'article 80, examine l'objet de la réclamation, dans la mesure nécessaire, et informe l'auteur de la réclamation de l'état d'avancement et de l'issue de l'enquête dans un délai raisonnable, notamment si un complément d'enquête ou une coordination avec une autre autorité de contrôle est nécessaire;

</td> </tr> </table>

<table>
<tr>
<td>

g)

</td>
<td>

coopère avec d'autres autorités de contrôle, y compris en partageant des informations, et fournit une assistance mutuelle dans ce cadre en vue d'assurer une application cohérente du présent règlement et des mesures prises pour en assurer le respect;

</td> </tr> </table>

<table>
<tr>
<td>

h)

</td>
<td>

effectue des enquêtes sur l'application du présent règlement, y compris sur la base d'informations reçues d'une autre autorité de contrôle ou d'une autre autorité publique;

</td> </tr> </table>

<table>
<tr>
<td>

i)

</td>
<td>

suit les évolutions pertinentes, dans la mesure où elles ont une incidence sur la protection des données à caractère personnel, notamment dans le domaine des technologies de l'information et de la communication et des pratiques commerciales;

</td> </tr> </table>

<table>
<tr>
<td>

j)

</td>
<td>

adopte les clauses contractuelles types visées à l'article 28, paragraphe 8, et à l'article 46, paragraphe 2, point d);

</td> </tr> </table>

<table>
<tr>
<td>

k)

</td>
<td>

établit et tient à jour une liste en lien avec l'obligation d'effectuer une analyse d'impact relative à la protection des données en application de l'article 35, paragraphe 4;

</td> </tr> </table>

<table>
<tr>
<td>

l)

</td>
<td>

fournit des conseils sur les opérations de traitement visées à l'article 36, paragraphe 2;

</td> </tr> </table>

<table>
<tr>
<td>

m)

</td>
<td>

encourage l'élaboration de codes de conduite en application de l'article 40, paragraphe 1, rend un avis et approuve les codes de conduite qui fournissent des garanties suffisantes, en application de l'article 40, paragraphe 5;

</td> </tr> </table>

<table>
<tr>
<td>

n)

</td>
<td>

encourage la mise en place de mécanismes de certification ainsi que de labels et de marques en matière de protection des données en application de l'article 42, paragraphe 1, et approuve les critères de certification en application de l'article 42, paragraphe 5;

</td> </tr> </table>

<table>
<tr>
<td>

o)

</td>
<td>

procède, le cas échéant, à l'examen périodique des certifications délivrées conformément à l'article 42, paragraphe 7;

</td> </tr> </table>

<table>
<tr>
<td>

p)

</td>
<td>

rédige et publie les critères d'agrément d'un organisme chargé du suivi des codes de conduite en application de l'article 41 et d'un organisme de certification en application de l'article 43;

</td> </tr> </table>

<table>
<tr>
<td>

q)

</td>
<td>

procède à l'agrément d'un organisme chargé du suivi des codes de conduite en application de l'article 41 et d'un organisme de certification en application de l'article 43;

</td> </tr> </table>

<table>
<tr>
<td>

r)

</td>
<td>

autorise les clauses contractuelles et les dispositions visées à l'article 46, paragraphe 3;

</td> </tr> </table>

<table>
<tr>
<td>

s)

</td>
<td>

approuve les règles d'entreprise contraignantes en application de l'article 47;

</td> </tr> </table>

<table>
<tr>
<td>

t)

</td>
<td>

contribue aux activités du comité;

</td> </tr> </table>

<table>
<tr>
<td>

u)

</td>
<td>

tient des registres internes des violations au présent règlement et des mesures prises conformément à l'article 58, paragraphe 2; et

</td> </tr> </table>

<table>
<tr>
<td>

v)

</td>
<td>

s'acquitte de toute autre mission relative à la protection des données à caractère personnel.

</td> </tr> </table>

2\. Chaque autorité de contrôle facilite l'introduction des réclamations visées au paragraphe 1, point f), par des mesures telles que la fourniture d'un formulaire de réclamation qui peut aussi être rempli par voie électronique, sans que d'autres moyens de communication ne soient exclus.

3\. L'accomplissement des missions de chaque autorité de contrôle est gratuit pour la personne concernée et, le cas échéant, pour le délégué à la protection des données.

4\. Lorsque les demandes sont manifestement infondées ou excessives, en raison, notamment, de leur caractère répétitif, l'autorité de contrôle peut exiger le paiement de frais raisonnables basés sur les coûts administratifs ou refuser de donner suite à la demande. Il incombe à l'autorité de contrôle de démontrer le caractère manifestement infondé ou excessif de la demande.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 1ER — MISSIONS DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier/article-1er)
  * [ARTICLE 4 — POUVOIRS DE CONTRÔLE DE LA CNIL](/etude-impact-490/titre-ier/chapitre-ier/article-4)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vi/section-2/article-56" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vi/section-2/article-58" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-vi/section-2/article-57' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

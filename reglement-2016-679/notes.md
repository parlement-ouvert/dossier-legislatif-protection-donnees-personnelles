<!-- TITLE: Notes -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/signataires" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <span> </span> </div>

(1) [JO C 229 du 31.7.2012, p. 90](./../../../../legal-content/FR/AUTO/?uri=OJ:C:2012:229:TOC).

(2) [JO C 391 du 18.12.2012, p. 127](./../../../../legal-content/FR/AUTO/?uri=OJ:C:2012:391:TOC).

(3) Position du Parlement européen du 12 mars 2014 (non encore parue au Journal officiel) et position du Conseil en première lecture du 8 avril 2016 (non encore parue au Journal officiel). Position du Parlement européen du 14 avril 2016.

(4) Directive 95/46/CE du Parlement européen et du Conseil, du 24 octobre 1995, relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données ([JO L 281 du 23.11.1995, p. 31](./../../../../legal-content/FR/AUTO/?uri=OJ:L:1995:281:TOC)).

(5) Recommandation de la Commission du 6 mai 2003 concernant la définition des micro, petites et moyennes entreprises [C(2003) 1422] ([JO L 124 du 20.5.2003, p. 36](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2003:124:TOC)).

(6) Règlement (CE) no 45/2001 du Parlement européen et du Conseil du 18 décembre 2000 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel par les institutions et organes communautaires et à la libre circulation de ces données ([JO L 8 du 12.1.2001, p. 1](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2001:008:TOC)).

(7) Directive (UE) 2016/680 du Parlement européen et du Conseil du 27 avril 2016 relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales, et à la libre circulation de ces données et abrogeant la décision-cadre 2008/977/JAI du Conseil (voir page 89 du présent Journal officiel).

(8) Directive 2000/31/CE du Parlement européen et du Conseil du 8 juin 2000 relative à certains aspects juridiques des services de la société de l'information, et notamment du commerce électronique, dans le marché intérieur («directive sur le commerce électronique») ([JO L 178 du 17.7.2000, p. 1](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2000:178:TOC)).

(9) Directive 2011/24/UE du Parlement européen et du Conseil du 9 mars 2011 relative à l'application des droits des patients en matière de soins de santé transfrontaliers ([JO L 88 du 4.4.2011, p. 45](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2011:088:TOC)).

(10) Directive 93/13/CEE du Conseil, du 5 avril 1993, concernant les clauses abusives dans les contrats conclus avec les consommateurs ([JO L 95 du 21.4.1993, p. 29](./../../../../legal-content/FR/AUTO/?uri=OJ:L:1993:095:TOC)).

(11) Règlement (CE) no 1338/2008 du Parlement européen et du Conseil du 16 décembre 2008 relatif aux statistiques communautaires de la santé publique et de la santé et de la sécurité au travail ([JO L 354 du 31.12.2008, p. 70](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2008:354:TOC)).

(12) Règlement (UE) no 182/2011 du Parlement européen et du Conseil du 16 février 2011 établissant les règles et principes généraux relatifs aux modalités de contrôle par les États membres de l'exercice des compétences d'exécution par la Commission ([JO L 55 du 28.2.2011, p. 13](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2011:055:TOC)).

(13) Règlement (UE) no 1215/2012 du Parlement européen et du Conseil du 12 décembre 2012 concernant la compétence judiciaire, la reconnaissance et l'exécution des décisions en matière civile et commerciale ([JO L 351 du 20.12.2012, p. 1](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2012:351:TOC)).

(14) Directive 2003/98/CE du Parlement européen et du Conseil du 17 novembre 2003 concernant la réutilisation des informations du secteur public ([JO L 345 du 31.12.2003, p. 90](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2003:345:TOC)).

(15) Règlement (UE) no 536/2014 du Parlement européen et du Conseil du 16 avril 2014 relatif aux essais cliniques de médicaments à usage humain et abrogeant la directive 2001/20/CE ([JO L 158 du 27.5.2014, p. 1](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2014:158:TOC)).

(16) Règlement (CE) no 223/2009 du Parlement européen et du Conseil du 11 mars 2009 relatif aux statistiques européennes et abrogeant le règlement (CE, Euratom) no 1101/2008 du Parlement européen et du Conseil relatif à la transmission à l'Office statistique des Communautés européennes d'informations statistiques couvertes par le secret, le règlement (CE) no 322/97 du Conseil relatif à la statistique communautaire et la décision 89/382/CEE, Euratom du Conseil instituant un comité du programme statistique des Communautés européennes ([JO L 87 du 31.3.2009, p. 164](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2009:087:TOC)).

(17) [JO C 192 du 30.6.2012, p. 7](./../../../../legal-content/FR/AUTO/?uri=OJ:C:2012:192:TOC).

(18) Directive 2002/58/CE du Parlement européen et du Conseil du 12 juillet 2002 concernant le traitement des données à caractère personnel et la protection de la vie privée dans le secteur des communications électroniques (directive vie privée et communications électroniques) ([JO L 201 du 31.7.2002, p. 37](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2002:201:TOC)).

(19) Directive (UE) 2015/1535 du Parlement européen et du Conseil du 9 septembre 2015 prévoyant une procédure d'information dans le domaine des réglementations techniques et des règles relatives aux services de la société de l'information ([JO L 241 du 17.9.2015, p. 1](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2015:241:TOC)).

(20) Règlement (CE) no 765/2008 du Parlement européen et du Conseil du 9 juillet 2008 fixant les prescriptions relatives à l'accréditation et à la surveillance du marché pour la commercialisation des produits et abrogeant le règlement (CEE) no 339/93 du Conseil ([JO L 218 du 13.8.2008, p. 30](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2008:218:TOC)).

(21) Règlement (CE) no 1049/2001 du Parlement européen et du Conseil du 30 mai 2001 relatif à l'accès du public aux documents du Parlement européen, du Conseil et de la Commission ([JO L 145 du 31.5.2001, p. 43](./../../../../legal-content/FR/AUTO/?uri=OJ:L:2001:145:TOC)).

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/signataires" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <span> </span> </div>

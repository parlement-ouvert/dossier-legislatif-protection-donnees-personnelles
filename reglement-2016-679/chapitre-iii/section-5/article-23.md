<!-- TITLE: Article 23 -->
<!-- SUBTITLE: Limitations -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-iii/section-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Le droit de l'Union ou le droit de l'État membre auquel le responsable du traitement ou le sous-traitant est soumis peuvent, par la voie de mesures législatives, limiter la portée des obligations et des droits prévus aux articles 12 à 22 et à l'article 34, ainsi qu'à l'article 5 dans la mesure où les dispositions du droit en question correspondent aux droits et obligations prévus aux articles 12 à 22, lorsqu'une telle limitation respecte l'essence des libertés et droits fondamentaux et qu'elle constitue une mesure nécessaire et proportionnée dans une société démocratique pour garantir:

<table>
<tr>
<td>

a)

</td>
<td>

la sécurité nationale;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

la défense nationale;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

la sécurité publique;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

la prévention et la détection d'infractions pénales, ainsi que les enquêtes et les poursuites en la matière ou l'exécution de sanctions pénales, y compris la protection contre les menaces pour la sécurité publique et la prévention de telles menaces;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

d'autres objectifs importants d'intérêt public général de l'Union ou d'un État membre, notamment un intérêt économique ou financier important de l'Union ou d'un État membre, y compris dans les domaines monétaire, budgétaire et fiscal, de la santé publique et de la sécurité sociale;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

la protection de l'indépendance de la justice et des procédures judiciaires;

</td> </tr> </table>

<table>
<tr>
<td>

g)

</td>
<td>

la prévention et la détection de manquements à la déontologie des professions réglementées, ainsi que les enquêtes et les poursuites en la matière;

</td> </tr> </table>

<table>
<tr>
<td>

h)

</td>
<td>

une mission de contrôle, d'inspection ou de réglementation liée, même occasionnellement, à l'exercice de l'autorité publique, dans les cas visés aux points a) à e) et g);

</td> </tr> </table>

<table>
<tr>
<td>

i)

</td>
<td>

la protection de la personne concernée ou des droits et libertés d'autrui;

</td> </tr> </table>

<table>
<tr>
<td>

j)

</td>
<td>

l'exécution des demandes de droit civil.

</td> </tr> </table>

2\. En particulier, toute mesure législative visée au paragraphe 1 contient des dispositions spécifiques relatives, au moins, le cas échéant:

<table>
<tr>
<td>

a)

</td>
<td>

aux finalités du traitement ou des catégories de traitement;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

aux catégories de données à caractère personnel;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

à l'étendue des limitations introduites;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

aux garanties destinées à prévenir les abus ou l'accès ou le transfert illicites;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

à la détermination du responsable du traitement ou des catégories de responsables du traitement;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

aux durées de conservation et aux garanties applicables, en tenant compte de la nature, de la portée et des finalités du traitement ou des catégories de traitement;

</td> </tr> </table>

<table>
<tr>
<td>

g)

</td>
<td>

aux risques pour les droits et libertés des personnes concernées; et

</td> </tr> </table>

<table>
<tr>
<td>

h)

</td>
<td>

au droit des personnes concernées d'être informées de la limitation, à moins que cela risque de nuire à la finalité de la limitation.

</td> </tr> </table>

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [29.](/avis-conseil-etat-393836/donnees-de-sante/29)
  * [36.](/avis-conseil-etat-393836/traitements-mis-en-oeuvre-par-letat/36)
  * [47.](/avis-conseil-etat-393836/traitements-de-donnees-relevant-a-la-fois-du-champ-de-la-directive-et-de-celui-du-reglement-ou-du-droit-interne/47)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 15 — LIMITATION DES DROITS](/etude-impact-490/titre-ii/chapitre-iv/article-15)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-iii/section-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-iii/section-5/article-23' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

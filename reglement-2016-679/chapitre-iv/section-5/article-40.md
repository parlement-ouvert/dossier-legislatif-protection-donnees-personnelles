<!-- TITLE: Article 40 -->
<!-- SUBTITLE: Codes de conduite -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-iv/section-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-iv/section-5/article-41" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Les États membres, les autorités de contrôle, le comité et la Commission encouragent l'élaboration de codes de conduite destinés à contribuer à la bonne application du présent règlement, compte tenu de la spécificité des différents secteurs de traitement et des besoins spécifiques des micro, petites et moyennes entreprises.

2\. Les associations et autres organismes représentant des catégories de responsables du traitement ou de sous-traitants peuvent élaborer des codes de conduite, les modifier ou les proroger, aux fins de préciser les modalités d'application du présent règlement, telles que:

<table>
<tr>
<td>

a)

</td>
<td>

le traitement loyal et transparent;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

les intérêts légitimes poursuivis par les responsables du traitement dans des contextes spécifiques;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

la collecte des données à caractère personnel;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

la pseudonymisation des données à caractère personnel;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

les informations communiquées au public et aux personnes concernées;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

l'exercice des droits des personnes concernées;

</td> </tr> </table>

<table>
<tr>
<td>

g)

</td>
<td>

les informations communiquées aux enfants et la protection dont bénéficient les enfants et la manière d'obtenir le consentement des titulaires de la responsabilité parentale à l'égard de l'enfant;

</td> </tr> </table>

<table>
<tr>
<td>

h)

</td>
<td>

les mesures et les procédures visées aux articles 24 et 25 et les mesures visant à assurer la sécurité du traitement visées à l'article 32;

</td> </tr> </table>

<table>
<tr>
<td>

i)

</td>
<td>

la notification aux autorités de contrôle des violations de données à caractère personnel et la communication de ces violations aux personnes concernées;

</td> </tr> </table>

<table>
<tr>
<td>

j)

</td>
<td>

le transfert de données à caractère personnel vers des pays tiers ou à des organisations internationales; ou

</td> </tr> </table>

<table>
<tr>
<td>

k)

</td>
<td>

les procédures extrajudiciaires et autres procédures de règlement des litiges permettant de résoudre les litiges entre les responsables du traitement et les personnes concernées en ce qui concerne le traitement, sans préjudice des droits des personnes concernées au titre des articles 77 et 79.

</td> </tr> </table>

3\. Outre leur application par les responsables du traitement ou les sous-traitants soumis au présent règlement, les codes de conduite qui sont approuvés en vertu du paragraphe 5 du présent article et qui sont d'application générale en vertu du paragraphe 9 du présent article peuvent aussi être appliqués par des responsables du traitement ou des sous-traitants qui ne sont pas soumis au présent règlement en vertu de l'article 3, afin de fournir des garanties appropriées dans le cadre des transferts de données à caractère personnel vers un pays tiers ou à une organisation internationale dans les conditions visées à l'article 46, paragraphe 2, point e). Ces responsables du traitement ou sous-traitants prennent l'engagement contraignant et doté de force obligatoire au moyen d'instruments contractuels ou d'autres instruments juridiquement contraignants, d'appliquer ces garanties appropriées, y compris en ce qui concerne les droits des personnes concernées.

4\. Le code de conduite visé au paragraphe 2 du présent article comprend les mécanismes permettant à l'organisme visé à l'article 41, paragraphe 1, de procéder au contrôle obligatoire du respect de ses dispositions par les responsables du traitement ou les sous-traitants qui s'engagent à l'appliquer, sans préjudice des missions et des pouvoirs de l'autorité de contrôle qui est compétente en vertu de l'article 55 ou 56.

5\. Les associations et autres organismes visés au paragraphe 2 du présent article qui ont l'intention d'élaborer un code de conduite ou de modifier ou proroger un code de conduite existant soumettent le projet de code, la modifications ou la prorogation à l'autorité de contrôle qui est compétente en vertu de l'article 55. L'autorité de contrôle rend un avis sur la question de savoir si le projet de code, la modification ou la prorogation respecte le présent règlement et approuve ce projet de code, cette modification ou cette prorogation si elle estime qu'il offre des garanties appropriées suffisantes.

6\. Lorsque le projet de code, la modification ou la prorogation est approuvé conformément au paragraphe 5, et lorsque le code de conduite concerné ne porte pas sur des activités de traitement menées dans plusieurs États membres, l'autorité de contrôle enregistre et publie le code de conduite.

7\. Lorsque le projet de code de conduite concerne des activités de traitement menées dans plusieurs États membres, l'autorité de contrôle qui est compétente en vertu de l'article 55 soumet le projet de code, la modification ou la prorogation, avant approbation, selon la procédure visée à l'article 63, au comité, qui rend un avis sur la question de savoir si le projet de code, la modification ou la prorogation respecte le présent règlement ou, dans la situation visée au paragraphe 3 du présent article, s'il offre des garanties appropriées.

8\. Lorsque l'avis visé au paragraphe 7 confirme que le projet de code, la modification ou la prorogation respecte le présent règlement ou, dans la situation visée au paragraphe 3, offre des garanties appropriées, le comité soumet son avis à la Commission.

9\. La Commission peut décider, par voie d'actes d'exécution, que le code de conduite, la modification ou la prorogation approuvés qui lui ont été soumis en vertu du paragraphe 8 du présent article sont d'application générale au sein de l'Union. Ces actes d'exécution sont adoptés en conformité avec la procédure d'examen visée à l'article 93, paragraphe 2.

10\. La Commission veille à garantir une publicité appropriée aux codes approuvés dont elle a décidé qu'ils sont d'application générale conformément au paragraphe 9.

11\. Le comité consigne dans un registre tous les codes de conduite, les modifications et les prorogations approuvés et les met à la disposition du public par tout moyen approprié.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 1ER — MISSIONS DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier/article-1er)
* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [Article 40](/loi-78-17-pjl-490/chapitre-v/section-2/article-40)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 15](/pjl-490/titre-ii/chapitre-v/article-15)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-iv/section-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-iv/section-5/article-41" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-iv/section-5/article-40' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

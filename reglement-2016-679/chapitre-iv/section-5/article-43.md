<!-- TITLE: Article 43 -->
<!-- SUBTITLE: Organismes de certification -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-iv/section-5/article-42" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Sans préjudice des missions et des pouvoirs de l'autorité de contrôle compétente au titre des articles 57 et 58, les organismes de certification disposant d'un niveau d'expertise approprié en matière de protection des données délivrent et renouvellent les certifications, après en avoir informé l'autorité de contrôle pour qu'elle puisse exercer au besoin les pouvoirs qui lui sont dévolus en vertu de l'article 58, paragraphe 2, point h). Les États membres veillent à ce que ces organismes de certification soient agréés par une des entités suivantes ou les deux:

<table>
<tr>
<td>

a)

</td>
<td>

l'autorité de contrôle qui est compétente en vertu de l'article 55 ou 56;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

l'organisme national d'accréditation désigné conformément au règlement (CE) no 765/2008 du Parlement européen et du Conseil (20), conformément à la norme EN-ISO/IEC 17065/2012 et aux exigences supplémentaires établies par l'autorité de contrôle qui est compétente en vertu de l'article 55 ou 56.

</td> </tr> </table>

2\. Les organismes de certification visés au paragraphe 1 ne sont agréés conformément audit paragraphe que lorsqu'ils ont:

<table>
<tr>
<td>

a)

</td>
<td>

démontré, à la satisfaction de l'autorité de contrôle compétente, leur indépendance et leur expertise au regard de l'objet de la certification;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

pris l'engagement de respecter les critères visés à l'article 42, paragraphe 5, et approuvés par l'autorité de contrôle qui est compétente en vertu de l'article 55 ou 56 ou par le comité, en vertu de l'article 63;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

mis en place des procédures en vue de la délivrance, de l'examen périodique et du retrait d'une certification, de labels et de marques en matière de protection des données;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

établi des procédures et des structures pour traiter les réclamations relatives aux violations de la certification ou à la manière dont la certification a été ou est appliquée par un responsable du traitement ou un sous-traitant, et pour rendre ces procédures et structures transparentes à l'égard des personnes concernées et du public; et

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

démontré, à la satisfaction de l'autorité de contrôle compétente, que leurs tâches et leurs missions n'entraînent pas de conflit d'intérêts.

</td> </tr> </table>

3\. L'agrément des organismes de certification visés aux paragraphes 1 et 2 du présent article se fait sur la base de critères approuvés par l'autorité de contrôle qui est compétente en vertu de l'article 55 ou 56 ou, par le comité en vertu de l'article 63. En cas d'agrément en application du paragraphe 1, point b), du présent article, ces exigences complètent celles prévues dans le règlement (CE) no 765/2008 et les règles techniques qui décrivent les méthodes et procédures des organismes de certification.

4\. Les organismes de certification visés au paragraphe 1 sont chargés de procéder à l'évaluation appropriée conduisant à la délivrance de la certification ou au retrait de cette certification, sans préjudice de la responsabilité du responsable du traitement ou du sous-traitant en ce qui concerne le respect du présent règlement. L'agrément est délivré pour une durée maximale de cinq ans et peut être renouvelé dans les mêmes conditions tant que l'organisme de certification satisfait aux exigences énoncées au présent article.

5\. Les organismes de certification visés au paragraphe 1 communiquent aux autorités de contrôle compétentes les raisons de la délivrance ou du retrait de la certification demandée.

6\. Les exigences visées au paragraphe 3 du présent article et les critères visés à l'article 42, paragraphe 5, sont publiés par les autorités de contrôle sous une forme aisément accessible. Les autorités de contrôle transmettent aussi ces exigences et ces critères au comité. Le comité consigne dans un registre tous les mécanismes de certification et les labels en matière de protection des données et les met à la disposition du public par tout moyen approprié.

7\. Sans préjudice du chapitre VIII, l'autorité de contrôle compétente ou l'organisme national d'accréditation révoque l'agrément d'un organisme de certification en application du paragraphe 1 du présent article si les conditions d'agrément ne sont pas ou ne sont plus réunies ou si les mesures prises par l'organisme de certification constituent une violation du présent règlement.

8\. La Commission est habilitée à adopter des actes délégués en conformité avec l'article 92, aux fins de préciser les exigences à prendre en considération en ce qui concerne les mécanismes de certification en matière de protection des données visés à l'article 42, paragraphe 1.

9\. La Commission peut adopter des actes d'exécution visant à fixer des normes techniques pour les mécanismes de certification, les labels et les marques en matière de protection des données, ainsi que les mécanismes aux fins de la promotion et de la reconnaissance de ces mécanismes de certification, labels et marques. Ces actes d'exécution sont adoptés en conformité avec la procédure d'examen visée à l'article 93, paragraphe 2.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 1ER — MISSIONS DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier/article-1er)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 1er](/pjl-490/titre-i/chapitre-i/article-1)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-iv/section-5/article-42" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-iv/section-5/article-43' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

<!-- TITLE: Section 1 -->
<!-- SUBTITLE: Obligations générales -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-iv/section-1/article-24" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 24 — Responsabilité du responsable du traitement](/reglement-2016-679/chapitre-iv/section-1/article-24)
* [Article 25 — Protection des données dès la conception et protection des données par défaut](/reglement-2016-679/chapitre-iv/section-1/article-25)
* [Article 26 — Responsables conjoints du traitement](/reglement-2016-679/chapitre-iv/section-1/article-26)
* [Article 27 — Représentants des responsables du traitement ou des sous-traitants qui ne sont pas établis dans l'Union](/reglement-2016-679/chapitre-iv/section-1/article-27)
* [Article 28 — Sous-traitant](/reglement-2016-679/chapitre-iv/section-1/article-28)
* [Article 29 — Traitement effectué sous l'autorité du responsable du traitement ou du sous-traitant](/reglement-2016-679/chapitre-iv/section-1/article-29)
* [Article 30 — Registre des activités de traitement](/reglement-2016-679/chapitre-iv/section-1/article-30)
* [Article 31 — Coopération avec l'autorité de contrôle](/reglement-2016-679/chapitre-iv/section-1/article-31)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-iv/section-1/article-24" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 68 -->
<!-- SUBTITLE: Comité européen de la protection des données -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-3/article-69" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Le comité européen de la protection des données (ci-après dénommé «comité») est institué en tant qu'organe de l'Union et possède la personnalité juridique.

2\. Le comité est représenté par son président.

3\. Le comité se compose du chef d'une autorité de contrôle de chaque État membre et du Contrôleur européen de la protection des données, ou de leurs représentants respectifs.

4\. Lorsque, dans un État membre, plusieurs autorités de contrôle sont chargées de surveiller l'application des dispositions du présent règlement, un représentant commun est désigné conformément au droit de cet État membre.

5\. La Commission a le droit de participer aux activités et réunions du comité sans droit de vote. La Commission désigne un représentant. Le président du comité informe la Commission des activités du comité.

6\. Dans les cas visés à l'article 65, le Contrôleur européen de la protection des données ne dispose de droits de vote qu'à l'égard des décisions concernant des principes et règles applicables aux institutions, organes et organismes de l'Union qui correspondent, en substance, à ceux énoncés dans le présent règlement.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-3/article-69" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-vii/section-3/article-68' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

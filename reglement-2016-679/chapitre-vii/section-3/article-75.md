<!-- TITLE: Article 75 -->
<!-- SUBTITLE: Secrétariat -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-3/article-74" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-3/article-76" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Le comité dispose d'un secrétariat, qui est assuré par le Contrôleur européen de la protection des données.

2\. Le secrétariat accomplit ses tâches sous l'autorité exclusive du président du comité.

3\. Le personnel du Contrôleur européen de la protection des données qui participe à l'exercice des missions que le présent règlement confie au comité est soumis à une structure hiérarchique distincte de celle du personnel qui participe à l'exercice des missions confiées au Contrôleur européen de la protection des données.

4\. Le cas échéant, le comité et le Contrôleur européen de la protection des données établissent et publient un protocole d'accord mettant en œuvre le présent article, fixant les modalités de leur coopération et s'appliquant au personnel du Contrôleur européen de la protection des données qui participe à l'exercice des missions que le présent règlement confie au comité.

5\. Le secrétariat fournit un soutien analytique, administratif et logistique au comité.

6\. Le secrétariat est notamment chargé de:

<table>
<tr>
<td>

a)

</td>
<td>

la gestion courante du comité;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

la communication entre les membres du comité, son président et la Commission;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

la communication avec d'autres institutions et le public;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

l'utilisation des voies électroniques pour la communication interne et externe;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

la traduction des informations utiles;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

la préparation et le suivi des réunions du comité;

</td> </tr> </table>

<table>
<tr>
<td>

g)

</td>
<td>

la préparation, la rédaction et la publication d'avis, de décisions relatives au règlement des litiges entre autorités de contrôle et d'autres textes adoptés par le comité.

</td> </tr> </table>

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-3/article-74" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-3/article-76" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-vii/section-3/article-75' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

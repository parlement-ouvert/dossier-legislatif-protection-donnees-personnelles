<!-- TITLE: Article 70 -->
<!-- SUBTITLE: Missions du comité -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-3/article-69" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-3/article-71" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Le comité veille à l'application cohérente du présent règlement. À cet effet, le comité, de sa propre initiative ou, le cas échéant, à la demande de la Commission, a notamment pour missions:

<table>
<tr>
<td>

a)

</td>
<td>

de surveiller et garantir la bonne application du présent règlement dans les cas prévus aux articles 64 et 65, sans préjudice des missions des autorités de contrôle nationales;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

de conseiller la Commission sur toute question relative à la protection des données à caractère personnel dans l'Union, y compris sur tout projet de modification du présent règlement;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

de conseiller la Commission, en ce qui concerne les règles d'entreprise contraignantes, sur la forme de l'échange d'informations entre les responsables du traitement, les sous-traitants et les autorités de contrôle, ainsi que les procédures qui s'y rapportent;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

de publier des lignes directrices, des recommandations et des bonnes pratiques sur les procédures de suppression des liens vers des données à caractère personnel, des copies ou des reproductions de celles-ci existant dans les services de communication accessibles au public, ainsi que le prévoit l'article 17, paragraphe 2;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

d'examiner, de sa propre initiative, à la demande de l'un de ses membres ou à la demande de la Commission, toute question portant sur l'application du présent règlement, et de publier des lignes directrices, des recommandations et des bonnes pratiques afin de favoriser l'application cohérente du présent règlement;

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

de publier des lignes directrices, des recommandations et des bonnes pratiques conformément au point e) du présent paragraphe, en vue de préciser davantage les critères et conditions applicables aux décisions fondées sur le profilage en vertu de l'article 22, paragraphe 2;

</td> </tr> </table>

<table>
<tr>
<td>

g)

</td>
<td>

de publier des lignes directrices, des recommandations et des bonnes pratiques conformément au point e) du présent paragraphe, en vue d'établir les violations de données à caractère personnel, de déterminer les meilleurs délais visés à l'article 33, paragraphes 1 et 2, et de préciser les circonstances particulières dans lesquelles un responsable du traitement ou un sous-traitant est tenu de notifier la violation de données à caractère personnel;

</td> </tr> </table>

<table>
<tr>
<td>

h)

</td>
<td>

de publier des lignes directrices, des recommandations et des bonnes pratiques conformément au point e) du présent paragraphe concernant les circonstances dans lesquelles une violation de données à caractère personnel est susceptible d'engendrer un risque élevé pour les droits et libertés des personnes physiques comme le prévoit l'article 34, paragraphe 1;

</td> </tr> </table>

<table>
<tr>
<td>

i)

</td>
<td>

de publier des lignes directrices, des recommandations et des bonnes pratiques conformément au point e) du présent paragraphe, aux fins de préciser davantage les critères et exigences applicables aux transferts de données à caractère personnel fondés sur des règles d'entreprise contraignantes appliquées par les responsables du traitement et sur des règles d'entreprise contraignantes appliquées par les sous-traitants et concernant les autres exigences nécessaires pour assurer la protection des données à caractère personnel des personnes concernées visées à l'article 47;

</td> </tr> </table>

<table>
<tr>
<td>

j)

</td>
<td>

de publier des lignes directrices, des recommandations et des bonnes pratiques conformément au point e) du présent paragraphe, en vue de préciser davantage les critères et exigences applicables aux transferts de données à caractère personnel sur la base de l'article 49, paragraphe 1;

</td> </tr> </table>

<table>
<tr>
<td>

k)

</td>
<td>

d'élaborer, à l'intention des autorités de contrôle, des lignes directrices concernant l'application des mesures visées à l'article 58, paragraphes 1, 2 et 3, ainsi que la fixation des amendes administratives en vertu de l'article 83;

</td> </tr> </table>

<table>
<tr>
<td>

l)

</td>
<td>

de faire le bilan de l'application pratique des lignes directrices, recommandations et des bonnes pratiques visées aux points e) et f);

</td> </tr> </table>

<table>
<tr>
<td>

m)

</td>
<td>

de publier des lignes directrices, des recommandations et des bonnes pratiques conformément au point e) du présent paragraphe, en vue d'établir des procédures communes pour le signalement par des personnes physiques de violations du présent règlement en vertu de l'article 54, paragraphe 2;

</td> </tr> </table>

<table>
<tr>
<td>

n)

</td>
<td>

d'encourager l'élaboration de codes de conduite et la mise en place de mécanismes de certification et de labels et de marques en matière de protection des données en vertu des articles 40 et 42;

</td> </tr> </table>

<table>
<tr>
<td>

o)

</td>
<td>

de procéder à l'agrément des organismes de certification et à l'examen périodique de cet agrément en vertu de l'article 43 et de tenir un registre public des organismes agréés en vertu de l'article 43, paragraphe 6, ainsi que des responsables du traitement ou des sous-traitants agréés établis dans des pays tiers en vertu de l'article 42, paragraphe 7;

</td> </tr> </table>

<table>
<tr>
<td>

p)

</td>
<td>

de définir les exigences visées à l'article 43, paragraphe 3, aux fins de l'agrément des organismes de certification prévu à l'article 42;

</td> </tr> </table>

<table>
<tr>
<td>

q)

</td>
<td>

de rendre à la Commission un avis sur les exigences en matière de certification visées à l'article 43, paragraphe 8;

</td> </tr> </table>

<table>
<tr>
<td>

r)

</td>
<td>

de rendre à la Commission un avis sur les icônes visées à l'article 12, paragraphe 7;

</td> </tr> </table>

<table>
<tr>
<td>

s)

</td>
<td>

de rendre à la Commission un avis en ce qui concerne l'évaluation du caractère adéquat du niveau de protection assuré par un pays tiers ou une organisation internationale, y compris concernant l'évaluation visant à déterminer si un pays tiers, un territoire ou un ou plusieurs secteurs déterminés dans ce pays tiers, ou une organisation internationale n'assurent plus un niveau adéquat de protection. À cette fin, la Commission fournit au comité tous les documents nécessaires, y compris la correspondance avec le gouvernement du pays tiers, en ce qui concerne ledit pays tiers, territoire ou secteur déterminé ou avec l'organisation internationale;

</td> </tr> </table>

<table>
<tr>
<td>

t)

</td>
<td>

d'émettre des avis sur les projets de décisions des autorités de contrôle conformément au mécanisme de contrôle de la cohérence visé à l'article 64, paragraphe 1, sur les questions soumises en vertu de l'article 64, paragraphe 2, et d'émettre des décisions contraignantes en vertu de l'article 65, y compris dans les cas visés à l'article 66;

</td> </tr> </table>

<table>
<tr>
<td>

u)

</td>
<td>

de promouvoir la coopération et l'échange bilatéral et multilatéral effectif d'informations et de bonnes pratiques entre les autorités de contrôle;

</td> </tr> </table>

<table>
<tr>
<td>

v)

</td>
<td>

de promouvoir l'élaboration de programmes de formation conjoints et de faciliter les échanges de personnel entre autorités de contrôle, ainsi que, le cas échéant, avec les autorités de contrôle de pays tiers ou d'organisations internationales;

</td> </tr> </table>

<table>
<tr>
<td>

w)

</td>
<td>

de promouvoir l'échange, avec des autorités de contrôle de la protection des données de tous pays, de connaissances et de documentation sur la législation et les pratiques en matière de protection des données;

</td> </tr> </table>

<table>
<tr>
<td>

x)

</td>
<td>

d'émettre des avis sur les codes de conduite élaborés au niveau de l'Union en application de l'article 40, paragraphe 9; et

</td> </tr> </table>

<table>
<tr>
<td>

y)

</td>
<td>

de tenir un registre électronique, accessible au public, des décisions prises par les autorités de contrôle et les juridictions sur les questions traitées dans le cadre du mécanisme de contrôle de la cohérence.

</td> </tr> </table>

2\. Lorsque la Commission demande conseil au comité, elle peut mentionner un délai, selon l'urgence de la question.

3\. Le comité transmet ses avis, lignes directrices, recommandations et bonnes pratiques à la Commission et au comité visé à l'article 93, et les publie.

4\. Le comité consulte, le cas échéant, les parties intéressées et leur permet de formuler des observations dans un délai raisonnable. Il met les résultats de la procédure de consultation à la disposition du public, sans préjudice de l'article 76.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 1er](/pjl-490/titre-i/chapitre-i/article-1)
* N° 592 annexe 0 - Rapport sur le projet de loi, après engagement de la procédure accélérée, relatif à la protection des données personnelles (n°490).
  * [Article 1er](/pjl-592/titre-i/chapitre-i/article-1)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-3/article-69" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-3/article-71" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-vii/section-3/article-70' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

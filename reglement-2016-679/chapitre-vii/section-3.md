<!-- TITLE: Section 3 -->
<!-- SUBTITLE: Comité européen de la protection des données -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-2/article-67" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-3/article-68" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 68 — Comité européen de la protection des données](/reglement-2016-679/chapitre-vii/section-3/article-68)
* [Article 69 — Indépendance](/reglement-2016-679/chapitre-vii/section-3/article-69)
* [Article 70 — Missions du comité](/reglement-2016-679/chapitre-vii/section-3/article-70)
* [Article 71 — Rapports](/reglement-2016-679/chapitre-vii/section-3/article-71)
* [Article 72 — Procédure](/reglement-2016-679/chapitre-vii/section-3/article-72)
* [Article 73 — Président](/reglement-2016-679/chapitre-vii/section-3/article-73)
* [Article 74 — Missions du président](/reglement-2016-679/chapitre-vii/section-3/article-74)
* [Article 75 — Secrétariat](/reglement-2016-679/chapitre-vii/section-3/article-75)
* [Article 76 — Confidentialité](/reglement-2016-679/chapitre-vii/section-3/article-76)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-2/article-67" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-3/article-68" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

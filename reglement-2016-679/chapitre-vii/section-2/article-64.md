<!-- TITLE: Article 64 -->
<!-- SUBTITLE: Avis du comité -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-2/article-63" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-2/article-65" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Le comité émet un avis chaque fois qu'une autorité de contrôle compétente envisage d'adopter l'une des mesures ci-après. À cet effet, l'autorité de contrôle compétente communique le projet de décision au comité, lorsque ce projet:

<table>
<tr>
<td>

a)

</td>
<td>

vise à adopter une liste d'opérations de traitement pour lesquelles une analyse d'impact relative à la protection des données doit être effectuée en application de l'article 35, paragraphe 4;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

concerne la question de savoir, en application de l'article 40, paragraphe 7, si un projet de code de conduite ou une modification ou une prorogation d'un code de conduite respecte le présent règlement;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

vise à approuver les critères d'agrément d'un organisme en application de l'article 41, paragraphe 3, ou d'un organisme de certification en application de l'article 43, paragraphe 3;

</td> </tr> </table>

<table>
<tr>
<td>

d)

</td>
<td>

vise à fixer des clauses types de protection des données visées à l'article 46, paragraphe 2, point d), et à l'article 28, paragraphe 8;

</td> </tr> </table>

<table>
<tr>
<td>

e)

</td>
<td>

vise à autoriser les clauses contractuelles visées à l'article 46, paragraphe 3, point a); ou

</td> </tr> </table>

<table>
<tr>
<td>

f)

</td>
<td>

vise à approuver des règles d'entreprise contraignantes au sens de l'article 47.

</td> </tr> </table>

2\. Toute autorité de contrôle, le président du comité ou la Commission peuvent demander que toute question d'application générale ou produisant des effets dans plusieurs États membres soit examinée par le comité en vue d'obtenir un avis, en particulier lorsqu'une autorité de contrôle compétente ne respecte pas les obligations relatives à l'assistance mutuelle conformément à l'article 61 ou les obligations relatives aux opérations conjointes conformément à l'article 62.

3\. Dans les cas visés aux paragraphes 1 et 2, le comité émet un avis sur la question qui lui est soumise, à condition qu'il n'ait pas déjà émis un avis sur la même question. Cet avis est adopté dans un délai de huit semaines à la majorité simple des membres du comité. Ce délai peut être prolongé de six semaines en fonction de la complexité de la question. En ce qui concerne le projet de décision visé au paragraphe 1 transmis aux membres du comité conformément au paragraphe 5, un membre qui n'a pas formulé d'objection dans un délai raisonnable fixé par le président est réputé approuver le projet de décision.

4\. Les autorités de contrôle et la Commission communiquent, dans les meilleurs délais, au comité, par voie électronique et au moyen d'un formulaire type, toutes les informations utiles, y compris, selon le cas, un résumé des faits, le projet de décision, les motifs rendant nécessaire l'adoption de cette mesure et les points de vue des autres autorités de contrôle concernées.

5\. Le président du comité transmet dans les meilleurs délais par voie électronique:

<table>
<tr>
<td>

a)

</td>
<td>

toutes les informations utiles qui lui ont été communiquées aux membres du comité et à la Commission, au moyen d'un formulaire type. Le secrétariat du comité fournit, si nécessaire, les traductions des informations utiles; et

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

l'avis à l'autorité de contrôle visée, selon le cas, aux paragraphes 1 et 2, et à la Commission, et le publie.

</td> </tr> </table>

6\. L'autorité de contrôle compétente n'adopte pas son projet de décision visé au paragraphe 1 lorsque le délai visé au paragraphe 3 court.

7\. L'autorité de contrôle visée au paragraphe 1 tient le plus grand compte de l'avis du comité et fait savoir au président du comité par voie électronique au moyen d'un formulaire type, dans un délai de deux semaines suivant la réception de l'avis, si elle maintiendra ou si elle modifiera son projet de décision et, le cas échéant, son projet de décision modifié.

8\. Lorsque l'autorité de contrôle concernée informe le président du comité dans le délai visé au paragraphe 7 du présent article qu'elle n'a pas l'intention de suivre, en tout ou en partie, l'avis du comité, en fournissant les motifs pertinents, l'article 65, paragraphe 1, s'applique.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-2/article-63" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-2/article-65" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-vii/section-2/article-64' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

<!-- TITLE: Article 65 -->
<!-- SUBTITLE: Règlement des litiges par le comité -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-2/article-64" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-2/article-66" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. En vue d'assurer l'application correcte et cohérente du présent règlement dans les cas d'espèce, le comité adopte une décision contraignante dans les cas suivants:

<table>
<tr>
<td>

a)

</td>
<td>

lorsque, dans le cas visé à l'article 60, paragraphe 4, une autorité de contrôle concernée a formulé une objection pertinente et motivée à l'égard d'un projet de décision de l'autorité de contrôle chef de file ou que l'autorité de contrôle chef de file a rejeté cette objection au motif qu'elle n'est pas pertinente ou motivée. La décision contraignante concerne toutes les questions qui font l'objet de l'objection pertinente et motivée, notamment celle de savoir s'il y a violation du présent règlement;

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

lorsqu'il existe des points de vue divergents quant à l'autorité de contrôle concernée qui est compétente pour l'établissement principal;

</td> </tr> </table>

<table>
<tr>
<td>

c)

</td>
<td>

lorsqu'une autorité de contrôle compétente ne demande pas l'avis du comité dans les cas visés à l'article 64, paragraphe 1, ou qu'elle ne suit pas l'avis du comité émis en vertu de l'article 64. Dans ce cas, toute autorité de contrôle concernée ou la Commission peut saisir le comité de la question.

</td> </tr> </table>

2\. La décision visée au paragraphe 1 est adoptée à la majorité des deux tiers des membres du comité dans un délai d'un mois à compter de la transmission de la question. Ce délai peut être prolongé d'un mois en fonction de la complexité de la question. La décision visée au paragraphe 1, est motivée et est adressée à l'autorité de contrôle chef de file et à toutes les autorités de contrôle concernées et est contraignante à leur égard.

3\. Lorsque le comité n'a pas été en mesure d'adopter une décision dans les délais visés au paragraphe 2, il adopte sa décision, à la majorité simple de ses membres, dans un délai de deux semaines suivant l'expiration du deuxième mois visé au paragraphe 2. En cas d'égalité des voix au sein du comité, la voix de son président est prépondérante.

4\. Les autorités de contrôle concernées n'adoptent pas de décision sur la question soumise au comité en vertu du paragraphe 1 lorsque les délais visés aux paragraphes 2 et 3 courent.

5\. Le président du comité notifie, dans les meilleurs délais, la décision visée au paragraphe 1 aux autorités de contrôle concernées. Il en informe la Commission. La décision est publiée sur le site internet du comité sans tarder après que l'autorité de contrôle a notifié la décision finale visée au paragraphe 6.

6\. L'autorité de contrôle chef de file ou, selon le cas, l'autorité de contrôle auprès de laquelle la réclamation a été introduite adopte sa décision finale sur la base de la décision visée au paragraphe 1 du présent article, dans les meilleurs délais et au plus tard un mois après que le comité a notifié sa décision. L'autorité de contrôle chef de file ou, selon le cas, l'autorité de contrôle auprès de laquelle la réclamation a été introduite informe le comité de la date à laquelle sa décision finale est notifiée, respectivement, au responsable du traitement ou au sous-traitant et à la personne concernée. La décision finale des autorités de contrôle concernées est adoptée aux conditions de l'article 60, paragraphes 7, 8 et 9. La décision finale fait référence à la décision visée au paragraphe 1 du présent article et précise que celle-ci sera publiée sur le site internet du comité conformément au paragraphe 5 du présent article. La décision visée au paragraphe 1 du présent article est jointe à la décision finale.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 5](/pjl-490/titre-i/chapitre-i/article-5)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-2/article-64" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-2/article-66" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-vii/section-2/article-65' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

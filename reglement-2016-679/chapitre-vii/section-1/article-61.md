<!-- TITLE: Article 61 -->
<!-- SUBTITLE: Assistance mutuelle -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-1/article-60" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-1/article-62" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Les autorités de contrôle se communiquent les informations utiles et se prêtent mutuellement assistance en vue de mettre en œuvre et d'appliquer le présent règlement de façon cohérente, et mettent en place des mesures pour coopérer efficacement. L'assistance mutuelle concerne notamment les demandes d'informations et les mesures de contrôle, telles que les demandes d'autorisation et de consultation préalables, les inspections et les enquêtes.

2\. Chaque autorité de contrôle prend toutes les mesures appropriées requises pour répondre à une demande d'une autre autorité de contrôle dans les meilleurs délais et au plus tard un mois après réception de la demande. De telles mesures peuvent comprendre, notamment, la transmission d'informations utiles sur la conduite d'une enquête.

3\. Les demandes d'assistances contiennent toutes les informations nécessaires, notamment la finalité et les motifs de la demande. Les informations échangées ne sont utilisées qu'aux fins pour lesquelles elles ont été demandées.

4\. Une autorité de contrôle requise ne peut refuser de satisfaire à une demande d'assistance, sauf si:

<table>
<tr>
<td>

a)

</td>
<td>

elle n'est pas compétente pour traiter l'objet de la demande ou pour prendre les mesures qu'elle est requise d'exécuter; ou

</td> </tr> </table>

<table>
<tr>
<td>

b)

</td>
<td>

satisfaire à la demande constituerait une violation du présent règlement ou du droit de l'Union ou du droit de l'État membre auquel l'autorité de contrôle qui a reçu la demande est soumise.

</td> </tr> </table>

5\. L'autorité de contrôle requise informe l'autorité de contrôle requérante des résultats obtenus ou, selon le cas, de l'avancement des mesures prises pour donner suite à la demande. L'autorité de contrôle requise explique les raisons de tout refus de satisfaire à une demande en application du paragraphe 4.

6\. En règle générale, les autorités de contrôle requises communiquent par voie électronique et au moyen d'un formulaire type, les informations demandées par d'autres autorités de contrôle.

7\. Les autorités de contrôle requises ne perçoivent pas de frais pour toute action qu'elles prennent à la suite d'une demande d'assistance mutuelle. Les autorités de contrôle peuvent convenir de règles concernant l'octroi de dédommagements entre elles pour des dépenses spécifiques résultant de la fourniture d'une assistance mutuelle dans des circonstances exceptionnelles.

8\. Lorsqu'une autorité de contrôle ne fournit pas les informations visées au paragraphe 5 du présent article dans un délai d'un mois à compter de la réception de la demande formulée par une autre autorité de contrôle, l'autorité de contrôle requérante peut adopter une mesure provisoire sur le territoire de l'État membre dont elle relève conformément à l'article 55, paragraphe 1. Dans ce cas, les circonstances permettant de considérer qu'il est urgent d'intervenir conformément à l'article 66, paragraphe 1, sont réputées réunies et nécessitent une décision contraignante d'urgence du comité en application de l'article 66, paragraphe 2.

9\. La Commission peut, par voie d'actes d'exécution, préciser la forme et les procédures de l'assistance mutuelle visée au présent article, ainsi que les modalités de l'échange d'informations par voie électronique entre les autorités de contrôle et entre les autorités de contrôle et le comité, notamment en ce qui concerne le formulaire type visé au paragraphe 6 du présent article. Ces actes d'exécution sont adoptés en conformité avec la procédure d'examen visée à l'article 93, paragraphe 2.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Délibération n°2017-299 du 30 novembre 2017 — Commission Nationale de l'Informatique et des Libertés
  * [Article 5 (Coopération)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-5-cooperation)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-1/article-60" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-1/article-62" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-vii/section-1/article-61' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

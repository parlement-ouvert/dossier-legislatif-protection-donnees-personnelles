<!-- TITLE: Article 60 -->
<!-- SUBTITLE: Coopération entre l'autorité de contrôle chef de file et les autres autorités de contrôle concernées -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-1/article-61" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. L'autorité de contrôle chef de file coopère avec les autres autorités de contrôle concernées conformément au présent article en s'efforçant de parvenir à un consensus. L'autorité de contrôle chef de file et les autorités de contrôle concernées échangent toute information utile.

2\. L'autorité de contrôle chef de file peut demander à tout moment aux autres autorités de contrôle concernées de se prêter mutuellement assistance en application de l'article 61 et peut mener des opérations conjointes en application de l'article 62, en particulier pour effectuer des enquêtes ou contrôler l'application d'une mesure concernant un responsable du traitement ou un sous-traitant établi dans un autre État membre.

3\. L'autorité de contrôle chef de file communique, sans tarder, les informations utiles sur la question aux autres autorités de contrôle concernées. Elle soumet sans tarder un projet de décision aux autres autorités de contrôle concernées en vue d'obtenir leur avis et tient dûment compte de leur point de vue.

4\. Lorsqu'une des autres autorités de contrôle concernées formule, dans un délai de quatre semaines après avoir été consultée conformément au paragraphe 3 du présent article, une objection pertinente et motivée à l'égard du projet de décision, l'autorité de contrôle chef de file, si elle ne suit pas l'objection pertinente et motivée ou si elle est d'avis que cette objection n'est pas pertinente ou motivée, soumet la question au mécanisme de contrôle de la cohérence visé à l'article 63.

5\. Lorsque l'autorité de contrôle chef de file entend suivre l'objection pertinente et motivée formulée, elle soumet aux autres autorités de contrôle concernées un projet de décision révisé en vue d'obtenir leur avis. Ce projet de décision révisé est soumis à la procédure visée au paragraphe 4 dans un délai de deux semaines.

6\. Lorsqu'aucune des autres autorités de contrôle concernées n'a formulé d'objection à l'égard du projet de décision soumis par l'autorité de contrôle chef de file dans le délai visé aux paragraphes 4 et 5, l'autorité de contrôle chef de file et les autorités de contrôle concernées sont réputées approuver ce projet de décision et sont liées par lui.

7\. L'autorité de contrôle chef de file adopte la décision, la notifie à l'établissement principal ou à l'établissement unique du responsable du traitement ou du sous-traitant, selon le cas, et informe les autres autorités de contrôle concernées et le comité de la décision en question, y compris en communiquant un résumé des faits et motifs pertinents. L'autorité de contrôle auprès de laquelle une réclamation a été introduite informe de la décision l'auteur de la réclamation.

8\. Par dérogation au paragraphe 7, lorsqu'une réclamation est refusée ou rejetée, l'autorité de contrôle auprès de laquelle la réclamation a été introduite adopte la décision, la notifie à l'auteur de la réclamation et en informe le responsable du traitement.

9\. Lorsque l'autorité de contrôle chef de file et les autorités de contrôle concernées sont d'accord pour refuser ou rejeter certaines parties d'une réclamation et donner suite à d'autres parties de cette réclamation, une décision distincte est adoptée pour chacune des parties. L'autorité de contrôle chef de file adopte la décision pour la partie relative aux actions concernant le responsable du traitement, la notifie à l'établissement principal ou à l'établissement unique du responsable du traitement ou du sous-traitant sur le territoire de l'État membre dont elle relève et en informe l'auteur de la réclamation, tandis que l'autorité de contrôle de l'auteur de la réclamation adopte la décision pour la partie concernant le refus ou le rejet de cette réclamation, la notifie à cette personne et en informe le responsable du traitement ou le sous-traitant.

10\. Après avoir été informé de la décision de l'autorité de contrôle chef de file en application des paragraphes 7 et 9, le responsable du traitement ou le sous-traitant prend les mesures nécessaires pour assurer le respect de cette décision en ce qui concerne les activités de traitement menées dans le cadre de tous ses établissements dans l'Union. Le responsable du traitement ou le sous-traitant notifie les mesures prises pour assurer le respect de la décision à l'autorité de contrôle chef de file, qui informe les autres autorités de contrôle concernées.

11\. Lorsque, dans des circonstances exceptionnelles, une autorité de contrôle concernée a des raisons de considérer qu'il est urgent d'intervenir pour protéger les intérêts des personnes concernées, la procédure d'urgence visée à l'article 66 s'applique.

12\. L'autorité de contrôle chef de file et les autres autorités de contrôle concernées se communiquent par voie électronique et au moyen d'un formulaire type, les informations requises en vertu du présent article.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Délibération n°2017-299 du 30 novembre 2017 — Commission Nationale de l'Informatique et des Libertés
  * [Article 5 (Coopération)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-5-cooperation)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 5 — PROCÉDURE DE COOPÉRATION DE LA CNIL AVEC LES AUTRES AUTORITÉS DE CONTRÔLE](/etude-impact-490/titre-ier/chapitre-ier/article-5)
* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [Article 49](/loi-78-17-pjl-490/chapitre-vii/article-49)
  * [Article 49-4](/loi-78-17-pjl-490/chapitre-vii/article-49-4)
* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 45](/loi-78-17-pjl-592/chapitre-vii/article-45)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 5](/pjl-490/titre-i/chapitre-i/article-5)
  * [Article 6](/pjl-490/titre-i/chapitre-i/article-6)
* N° 592 annexe 0 - Rapport sur le projet de loi, après engagement de la procédure accélérée, relatif à la protection des données personnelles (n°490).
  * [Article 5](/pjl-592/titre-i/chapitre-i/article-5)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-1/article-61" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-vii/section-1/article-60' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

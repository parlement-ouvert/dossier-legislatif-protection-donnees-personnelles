<!-- TITLE: Article 62 -->
<!-- SUBTITLE: Opérations conjointes des autorités de contrôle -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-1/article-61" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Les autorités de contrôle mènent, le cas échéant, des opérations conjointes, y compris en effectuant des enquêtes conjointes et en prenant des mesures répressives conjointes, auxquelles participent des membres ou des agents des autorités de contrôle d'autres États membres.

2\. Lorsque le responsable du traitement ou le sous-traitant est établi dans plusieurs États membres ou si un nombre important de personnes concernées dans plusieurs États membres sont susceptibles d'être sensiblement affectées par des opérations de traitement, une autorité de contrôle de chacun de ces États membres a le droit de participer aux opérations conjointes. L'autorité de contrôle qui est compétente en vertu de l'article 56, paragraphe 1 ou 4, invite l'autorité de contrôle de chacun de ces États membres à prendre part aux opérations conjointes concernées et donne suite sans tarder à toute demande d'une autorité de contrôle souhaitant y participer.

3\. Une autorité de contrôle peut, conformément au droit d'un État membre, et avec l'autorisation de l'autorité de contrôle d'origine, conférer des pouvoirs, notamment des pouvoirs d'enquête, aux membres ou aux agents de l'autorité de contrôle d'origine participant à des opérations conjointes ou accepter, pour autant que le droit de l'État membre dont relève l'autorité de contrôle d'accueil le permette, que les membres ou les agents de l'autorité de contrôle d'origine exercent leurs pouvoirs d'enquête conformément au droit de l'État membre dont relève l'autorité de contrôle d'origine. Ces pouvoirs d'enquête ne peuvent être exercés que sous l'autorité et en présence de membres ou d'agents de l'autorité de contrôle d'accueil. Les membres ou agents de l'autorité de contrôle d'origine sont soumis au droit de l'État membre de l'autorité de contrôle d'accueil.

4\. Lorsque, conformément au paragraphe 1, les agents de l'autorité de contrôle d'origine opèrent dans un autre État membre, l'État membre dont relève l'autorité de contrôle d'accueil assume la responsabilité de leurs actions, y compris la responsabilité des dommages qu'ils causent au cours des opérations dont ils sont chargés, conformément au droit de l'État membre sur le territoire duquel ils opèrent.

5\. L'État membre sur le territoire duquel les dommages ont été causés répare ces dommages selon les conditions applicables aux dommages causés par ses propres agents. L'État membre dont relève l'autorité de contrôle d'origine dont les agents ont causé des dommages à des personnes sur le territoire d'un autre État membre rembourse intégralement à cet autre État membre les sommes qu'il a versées aux ayants droit.

6\. Sans préjudice de l'exercice de ses droits à l'égard des tiers et sous réserve du paragraphe 5, chaque État membre s'abstient, dans le cas prévu au paragraphe 1, de demander à un autre État membre le remboursement lié aux dommages visés au paragraphe 4.

7\. Lorsqu'une opération conjointe est envisagée et qu'une autorité de contrôle ne se conforme pas, dans un délai d'un mois, à l'obligation fixée au paragraphe 2, deuxième phrase, du présent article, les autres autorités de contrôle peuvent adopter une mesure provisoire sur le territoire de l'État membre dont celle-ci relève conformément à l'article 55. Dans ce cas, les circonstances permettant de considérer qu'il est urgent d'intervenir conformément à l'article 66, paragraphe 1, sont présumées être réunies et nécessitent un avis ou une décision contraignante d'urgence du comité en application de l'article 66, paragraphe 2.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [19.](/avis-conseil-etat-393836/modalites-de-cooperation-entre-autorites-de-controle-europeennes/19)
* Délibération n°2017-299 du 30 novembre 2017 — Commission Nationale de l'Informatique et des Libertés
  * [Article 5 (Coopération)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-5-cooperation)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 5 — PROCÉDURE DE COOPÉRATION DE LA CNIL AVEC LES AUTRES AUTORITÉS DE CONTRÔLE](/etude-impact-490/titre-ier/chapitre-ier/article-5)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 5](/pjl-490/titre-i/chapitre-i/article-5)
* N° 592 annexe 0 - Rapport sur le projet de loi, après engagement de la procédure accélérée, relatif à la protection des données personnelles (n°490).
  * [Article 5](/pjl-592/titre-i/chapitre-i/article-5)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-1/article-61" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-vii/section-1/article-62' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

<!-- TITLE: CHAPITRE VII -->
<!-- SUBTITLE: Coopération et cohérence -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vi/section-2/article-59" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Section 1 — Coopération](/reglement-2016-679/chapitre-vii/section-1)

  * [Article 60 — Coopération entre l'autorité de contrôle chef de file et les autres autorités de contrôle concernées](/reglement-2016-679/chapitre-vii/section-1/article-60)
  * [Article 61 — Assistance mutuelle](/reglement-2016-679/chapitre-vii/section-1/article-61)
  * [Article 62 — Opérations conjointes des autorités de contrôle](/reglement-2016-679/chapitre-vii/section-1/article-62)

* [Section 2 — Cohérence](/reglement-2016-679/chapitre-vii/section-2)

  * [Article 63 — Mécanisme de contrôle de la cohérence](/reglement-2016-679/chapitre-vii/section-2/article-63)
  * [Article 64 — Avis du comité](/reglement-2016-679/chapitre-vii/section-2/article-64)
  * [Article 65 — Règlement des litiges par le comité](/reglement-2016-679/chapitre-vii/section-2/article-65)
  * [Article 66 — Procédure d'urgence](/reglement-2016-679/chapitre-vii/section-2/article-66)
  * [Article 67 — Échange d'informations](/reglement-2016-679/chapitre-vii/section-2/article-67)

* [Section 3 — Comité européen de la protection des données](/reglement-2016-679/chapitre-vii/section-3)

  * [Article 68 — Comité européen de la protection des données](/reglement-2016-679/chapitre-vii/section-3/article-68)
  * [Article 69 — Indépendance](/reglement-2016-679/chapitre-vii/section-3/article-69)
  * [Article 70 — Missions du comité](/reglement-2016-679/chapitre-vii/section-3/article-70)
  * [Article 71 — Rapports](/reglement-2016-679/chapitre-vii/section-3/article-71)
  * [Article 72 — Procédure](/reglement-2016-679/chapitre-vii/section-3/article-72)
  * [Article 73 — Président](/reglement-2016-679/chapitre-vii/section-3/article-73)
  * [Article 74 — Missions du président](/reglement-2016-679/chapitre-vii/section-3/article-74)
  * [Article 75 — Secrétariat](/reglement-2016-679/chapitre-vii/section-3/article-75)
  * [Article 76 — Confidentialité](/reglement-2016-679/chapitre-vii/section-3/article-76)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 5 — PROCÉDURE DE COOPÉRATION DE LA CNIL AVEC LES AUTRES AUTORITÉS DE CONTRÔLE](/etude-impact-490/titre-ier/chapitre-ier/article-5)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vi/section-2/article-59" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-vii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

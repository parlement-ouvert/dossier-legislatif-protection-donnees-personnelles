<!-- TITLE: CHAPITRE XI -->
<!-- SUBTITLE: Dispositions finales -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-x/article-93" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-xi/article-94" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 94 — Abrogation de la directive 95/46/CE](/reglement-2016-679/chapitre-xi/article-94)
* [Article 95 — Relation avec la directive 2002/58/CE](/reglement-2016-679/chapitre-xi/article-95)
* [Article 96 — Relation avec les accords conclus antérieurement](/reglement-2016-679/chapitre-xi/article-96)
* [Article 97 — Rapports de la Commission](/reglement-2016-679/chapitre-xi/article-97)
* [Article 98 — Réexamen d'autres actes juridiques de l'Union relatifs à la protection des données](/reglement-2016-679/chapitre-xi/article-98)
* [Article 99 — Entrée en vigueur et application](/reglement-2016-679/chapitre-xi/article-99)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-x/article-93" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-xi/article-94" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

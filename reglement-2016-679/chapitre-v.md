<!-- TITLE: CHAPITRE V -->
<!-- SUBTITLE: Transferts de données à caractère personnel vers des pays tiers ou à des organisations internationales -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-iv/section-5/article-43" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-v/article-44" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 44 — Principe général applicable aux transferts](/reglement-2016-679/chapitre-v/article-44)
* [Article 45 — Transferts fondés sur une décision d'adéquation](/reglement-2016-679/chapitre-v/article-45)
* [Article 46 — Transferts moyennant des garanties appropriées](/reglement-2016-679/chapitre-v/article-46)
* [Article 47 — Règles d'entreprise contraignantes](/reglement-2016-679/chapitre-v/article-47)
* [Article 48 — Transferts ou divulgations non autorisés par le droit de l'Union](/reglement-2016-679/chapitre-v/article-48)
* [Article 49 — Dérogations pour des situations particulières](/reglement-2016-679/chapitre-v/article-49)
* [Article 50 — Coopération internationale dans le domaine de la protection des données à caractère personnel](/reglement-2016-679/chapitre-v/article-50)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-iv/section-5/article-43" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-v/article-44" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

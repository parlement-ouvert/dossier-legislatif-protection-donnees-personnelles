<!-- TITLE: CHAPITRE IV -->
<!-- SUBTITLE: Responsable du traitement et sous-traitant -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-iii/section-5/article-23" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-iv/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Section 1 — Obligations générales](/reglement-2016-679/chapitre-iv/section-1)

  * [Article 24 — Responsabilité du responsable du traitement](/reglement-2016-679/chapitre-iv/section-1/article-24)
  * [Article 25 — Protection des données dès la conception et protection des données par défaut](/reglement-2016-679/chapitre-iv/section-1/article-25)
  * [Article 26 — Responsables conjoints du traitement](/reglement-2016-679/chapitre-iv/section-1/article-26)
  * [Article 27 — Représentants des responsables du traitement ou des sous-traitants qui ne sont pas établis dans l'Union](/reglement-2016-679/chapitre-iv/section-1/article-27)
  * [Article 28 — Sous-traitant](/reglement-2016-679/chapitre-iv/section-1/article-28)
  * [Article 29 — Traitement effectué sous l'autorité du responsable du traitement ou du sous-traitant](/reglement-2016-679/chapitre-iv/section-1/article-29)
  * [Article 30 — Registre des activités de traitement](/reglement-2016-679/chapitre-iv/section-1/article-30)
  * [Article 31 — Coopération avec l'autorité de contrôle](/reglement-2016-679/chapitre-iv/section-1/article-31)

* [Section 2 — Sécurité des données à.caractère personnel](/reglement-2016-679/chapitre-iv/section-2)

  * [Article 32 — Sécurité du traitement](/reglement-2016-679/chapitre-iv/section-2/article-32)
  * [Article 33 — Notification à l'autorité de contrôle d'une violation de données à caractère personnel](/reglement-2016-679/chapitre-iv/section-2/article-33)
  * [Article 34 — Communication à la personne concernée d'une violation de données à caractère personnel](/reglement-2016-679/chapitre-iv/section-2/article-34)

* [Section 3 — Analyse d'impact relative à la protection des donnés et consultation préalable](/reglement-2016-679/chapitre-iv/section-3)

  * [Article 35 — Analyse d'impact relative à la protection des données](/reglement-2016-679/chapitre-iv/section-3/article-35)
  * [Article 36 — Consultation préalable](/reglement-2016-679/chapitre-iv/section-3/article-36)

* [Section 4 — Délégué à la protection des données](/reglement-2016-679/chapitre-iv/section-4)

  * [Article 37 — Désignation du délégué à la protection des données](/reglement-2016-679/chapitre-iv/section-4/article-37)
  * [Article 38 — Fonction du délégué à la protection des données](/reglement-2016-679/chapitre-iv/section-4/article-38)
  * [Article 39 — Missions du délégué à la protection des données](/reglement-2016-679/chapitre-iv/section-4/article-39)

* [Section 5 — Codes de conduite et certification](/reglement-2016-679/chapitre-iv/section-5)

  * [Article 40 — Codes de conduite](/reglement-2016-679/chapitre-iv/section-5/article-40)
  * [Article 41 — Suivi des codes de conduite approuvés](/reglement-2016-679/chapitre-iv/section-5/article-41)
  * [Article 42 — Certification](/reglement-2016-679/chapitre-iv/section-5/article-42)
  * [Article 43 — Organismes de certification](/reglement-2016-679/chapitre-iv/section-5/article-43)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [26.](/avis-conseil-etat-393836/delegue-a-la-protection-des-donnees/26)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 10 — SOUS-TRAITANT](/etude-impact-490/titre-ii/chapitre-iii/article-10)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 10](/pjl-490/titre-ii/chapitre-iii/article-10)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-iii/section-5/article-23" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-iv/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

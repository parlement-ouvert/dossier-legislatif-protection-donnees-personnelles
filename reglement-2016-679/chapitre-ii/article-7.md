<!-- TITLE: Article 7 -->
<!-- SUBTITLE: Conditions applicables au consentement -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-ii/article-6" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-ii/article-8" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Dans les cas où le traitement repose sur le consentement, le responsable du traitement est en mesure de démontrer que la personne concernée a donné son consentement au traitement de données à caractère personnel la concernant.

2\. Si le consentement de la personne concernée est donné dans le cadre d'une déclaration écrite qui concerne également d'autres questions, la demande de consentement est présentée sous une forme qui la distingue clairement de ces autres questions, sous une forme compréhensible et aisément accessible, et formulée en des termes clairs et simples. Aucune partie de cette déclaration qui constitue une violation du présent règlement n'est contraignante.

3\. La personne concernée a le droit de retirer son consentement à tout moment. Le retrait du consentement ne compromet pas la licéité du traitement fondé sur le consentement effectué avant ce retrait. La personne concernée en est informée avant de donner son consentement. Il est aussi simple de retirer que de donner son consentement.

4\. Au moment de déterminer si le consentement est donné librement, il y a lieu de tenir le plus grand compte de la question de savoir, entre autres, si l'exécution d'un contrat, y compris la fourniture d'un service, est subordonnée au consentement au traitement de données à caractère personnel qui n'est pas nécessaire à l'exécution dudit contrat.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [31.](/avis-conseil-etat-393836/consentement-des-mineurs-pour-les-autres-traitements-de-donnees/31)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-ii/article-6" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-ii/article-8" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<div id='discourse-comments'></div> <script type="text/javascript"> DiscourseEmbed = { discourseUrl: 'https://forum.parlement-ouvert.fr/', discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr/reglement-2016-679/chapitre-ii/article-7' }; (function() { var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true; d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js'; (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d); })(); </script>

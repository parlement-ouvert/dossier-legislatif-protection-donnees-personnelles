<!-- TITLE: CHAPITRE VIII -->
<!-- SUBTITLE: Voies de recours, responsabilité et sanctions -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-3/article-76" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-viii/article-77" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 77 — Droit d'introduire une réclamation auprès d'une autorité de contrôle](/reglement-2016-679/chapitre-viii/article-77)
* [Article 78 — Droit à un recours juridictionnel effectif contre une autorité de contrôle](/reglement-2016-679/chapitre-viii/article-78)
* [Article 79 — Droit à un recours juridictionnel effectif contre un responsable du traitement ou un sous-traitant](/reglement-2016-679/chapitre-viii/article-79)
* [Article 80 — Représentation des personnes concernées](/reglement-2016-679/chapitre-viii/article-80)
* [Article 81 — Suspension d'une action](/reglement-2016-679/chapitre-viii/article-81)
* [Article 82 — Droit à réparation et responsabilité](/reglement-2016-679/chapitre-viii/article-82)
* [Article 83 — Conditions générales pour imposer des amendes administratives](/reglement-2016-679/chapitre-viii/article-83)
* [Article 84 — Sanctions](/reglement-2016-679/chapitre-viii/article-84)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLES 16 ET 17 — MODALITES D’EXERCICE DES VOIES DE RECOURS](/etude-impact-490/titre-ii/chapitre-v/articles-16-et-17)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/reglement-2016-679/chapitre-vii/section-3/article-76" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Règlement européen</span> <a href="/reglement-2016-679/chapitre-viii/article-77" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: B -->
<!-- SUBTITLE: DE NOUVEAUX DROITS POUR UNE RESPONSABILITÉ ACCRUE DES RESPONSABLES DE TRAITEMENT -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/i/a/3/c" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/i/b/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

S’il est difficile de résumer en quelques traits un texte de près de cent articles, destinés à mettre en place un schéma juridique clair et solide pour la protection des données personnelles à l’échelle de l’Union, votre rapporteure souhaite rappeler les droits des personnes qui y sont rappelés ou consacrés, ainsi que l’inversion de la logique dans les dispositifs de contrôle.

* [1 — Assurer le respect de la vie privée et la maîtrise des données](/rapport-information-577/i/b/1)

* [2 — Garantir techniquement un traitement adapté et proportionné des données](/rapport-information-577/i/b/2)

* [3 — Une inversion de la logique de contrôle et de responsabilité](/rapport-information-577/i/b/3)

* [4 — Une adaptation aux risques](/rapport-information-577/i/b/4)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/i/a/3/c" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/i/b/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: A -->
<!-- SUBTITLE: LA MODERNISATION DU CADRE JURIDIQUE APPLICABLE À LA PROTECTION DES DONNÉES PERSONNELLES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/i/a/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [1 — Une adaptation aux nouvelles technologies…](/rapport-information-577/i/a/1)

* [2 — …dans le contexte du marché unique du numérique](/rapport-information-577/i/a/2)

* [3 — La difficulté des négociations, qui tient notamment à l’ampleur des enjeux, a abouti à un texte hybride](/rapport-information-577/i/a/3)

  * [a — La souveraineté numérique : l’application extraterritoriale du règlement](/rapport-information-577/i/a/3/a)

  * [b — Un règlement autorisant des marges de manœuvre nationales](/rapport-information-577/i/a/3/b)

  * [c — Un travail suivi pour assurer la plus grande harmonisation possible](/rapport-information-577/i/a/3/c)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/i/a/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

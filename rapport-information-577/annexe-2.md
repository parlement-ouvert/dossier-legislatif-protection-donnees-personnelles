<!-- TITLE: ANNEXE N° 2 -->
<!-- SUBTITLE: LISTE DES PERSONNES AUDITIONNÉES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/annexe-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/notes" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

 **Ministère de la Justice**

– M. Eric Thiers, conseiller au cabinet en charge des questions constitutionnelles

– M. Anthony Duplan, chef du bureau du droit constitutionnel et du droit public général

– M. Corentin Hellendorff, rédacteur au bureau du droit constitutionnel et du droit public général

 **Commission européenne**

– M. Emmanuel Crabit, directeur chargé des droits fondamentaux et de l’État de droit

– M. Olivier Micol, directeur chargé de la protection des données

– Mme Isabelle Chatelier

 **Qwant**

– M. Léonard Cox, vice-président des affaires publiques et RSE

– M. Guillaume Champeau, directeur éthique et relations publiques

– M. Léonidas Kalogeropoulos, conseil

 **CIGREF**

– M. Henri d’Agrain, délégué général

– Mme Flora Fischer, chargée de mission

 **UFC-Que Choisir**

– Mme Justine Massera, juriste

– Mme Alice Jubeau, chargée de missions relations institutionnelles, responsable du lobbying européen

 **Entretiens menés par la rapporteure :**

– M. Jean Lessi, secrétaire général de la CNIL

– Mme Tiphaine Havel, Conseillère pour les questions institutionnelles et parlementaires de la CNIL

– M. Olivier Iteanu, avocat à la Cour

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/annexe-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/notes" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

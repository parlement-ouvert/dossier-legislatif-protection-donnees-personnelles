<!-- TITLE: Notes -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/annexe-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <span> </span> </div>

 _1 () Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE (règlement général sur la protection des données)_

2 _() 1. Toute personne a droit à la protection des données à caractère personnel la concernant._

 _2\. Ces données doivent être traitées loyalement, à des fins déterminées et sur la base du consentement de la personne concernée ou en vertu d'un autre fondement légitime prévu par la loi. Toute personne a le droit d'accéder aux données collectées la concernant et d'en obtenir la rectification._

 _3\. Le respect de ces règles est soumis au contrôle d'une autorité indépendante._

3 _() 1. Toute personne a droit à la protection des données à caractère personnel la concernant._

 _2\. Le Parlement européen et le Conseil, statuant conformément à la procédure législative ordinaire, fixent les règles relatives à la protection des personnes physiques à l'égard du traitement des données à caractère personnel par les institutions, organes et organismes de l'Union, ainsi que par les États membres dans l'exercice d'activités qui relèvent du champ d'application du droit de l'Union, et à la libre circulation de ces données. Le respect de ces règles est soumis au contrôle d'autorités indépendantes._

 _Les règles adoptées sur la base du présent article sont sans préjudice des règles spécifiques prévues à l'article 39 du traité sur l'Union européenne._

4 _() Directive 95/46/CE du Parlement européen et du Conseil, du 24 octobre 1995, relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données_

5 _() Loi n° 2004-801 du 6 août 2004 relative à la protection des personnes physiques à l'égard des traitements de données à caractère personnel et modifiant la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés._

6 _() Considérant 3 de la directive 95/46/CE : considérant que l'établissement et le fonctionnement du marché intérieur dans lequel, conformément à l'article 7 A du traité, la libre circulation des marchandises, des personnes, des services et des capitaux est assurée, nécessitent non seulement que des données à caractère personnel puissent circuler librement d'un État membre à l'autre, mais également que les droits fondamentaux des personnes soient sauvegardés_

7 _() Directive 2002/58/CE du 12 juillet 2002 « vie privée et communications électroniques »._

8 _() Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE (règlement général sur la protection des données)._

9 _() Communication de la Commission européenne « Une approche globale de la protection des données à caractère personnel dans l’Union européenne », COM (2010) 609 final du 4 novembre 2010._

10 _() 1. Toute personne a droit à la protection des données à caractère personnel la concernant._

 _2\. Ces données doivent être traitées loyalement, à des fins déterminées et sur la base du consentement de la personne concernée ou en vertu d'un autre fondement légitime prévu par la loi. Toute personne a le droit d'accéder aux données collectées la concernant et d'en obtenir la rectification._

 _3\. Le respect de ces règles est soumis au contrôle d'une autorité indépendante._

11 _() 1. Toute personne a droit à la protection des données à caractère personnel la concernant._

 _2\. Le Parlement européen et le Conseil, statuant conformément à la procédure législative ordinaire, fixent les règles relatives à la protection des personnes physiques à l'égard du traitement des données à caractère personnel par les institutions, organes et organismes de l'Union, ainsi que par les États membres dans l'exercice d'activités qui relèvent du champ d'application du droit de l'Union, et à la libre circulation de ces données. Le respect de ces règles est soumis au contrôle d'autorités indépendantes._

 _Les règles adoptées sur la base du présent article sont sans préjudice des règles spécifiques prévues à l'article 39 du traité sur l'Union européenne._

12 _() Communication de la Commission au Parlement européen, au Conseil, au Comité économique et social européen et au Comité des régions, «Stratégie pour un marché unique numérique en Europe » COM(2015) 192 final._

13 _()_ Barreau, Catherine. « Le marché unique numérique et la régulation des données personnelles », _Annales des Mines - Réalités industrielles_ , vol. août 2016, n° 3, 2016, pp. 37-41.

14 _() Google, Amazon, Facebook, Apple, Microsoft._

15 _() Baidu, Alibaba, Tencent, Xiaomi._

16 _() Article 3 du RGPD._

17 _() Article 68 du RGPD._

18 _() CJUE, GC, 13 mai 2014, Google Spain SL et Google Inc._

19 _() Un certain nombre de critères encadrent toutefois cette demande. Cela ne peut être le cas que lorsque les données à caractère personnel ne sont plus nécessaires au regard des finalités pour lesquelles elles ont été collectées ou traitées d'une autre manière ou que la personne concernée retire son consentement au traitement des données._

20 _() Article 20 du RGPD._

21 _() Article 25 du RGPD._

22 _() Ibid._

23 _() Article 37 du RGPD, ce délégué doit être désigné en tout état de cause si le traitement est effectué par une autorité publique ou un organisme public, à l’exception des activités juridictionnelles, lorsque les activités du responsable privé du traitement ou de son sous-traitant consistent en des opérations de traitement qui exigent un suivi régulier et systématique des personnes concernées, ou lorsque ces mêmes activités consistent à traiter des données relatives à des condamnations pénales ou susceptibles de révéler des caractéristiques ethniques, politiques, religieuses ou syndicales, voire génétiques, des personnes._

24 _() Guidelines on Consent under Regulation 2016/679adopted on 28 November 2017. 17/EN WP259._

25 _() Le texte en cours de discussion réformera l’actuelle Isikuandmete kaitse seadus._

26 _() Selon une étude Ipsos du 29 avril 2015 sur les jeunes, Internet et les réseaux sociaux, les adolescents (13-19 ans) passaient en moyenne 13h30 par semaine sur Internet. À titre de comparaison, les enfants de 7 à 12 ans y passaient 5 h 30 en moyenne._

27 _() Article 9 du projet de loi._

28 _() Ces garanties peuvent avantageusement prendre la forme de certifications._

29 _() Considérant 81._

30 _() Article 42 du RGPD._

31 _() Article 42 (4) du RGPD._

32 _() Article 11 de la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés._

33 _() « La vie des entreprises accueille et utilise aussi le droit souple. Celui-ci y est souvent privilégié, pour des motifs à la fois économiques, juridiques et idéologiques, sans que ce terme ne revête de connotation péjorative : **le standard** , c’est-à-dire une référence commune dont on peut s’écarter en cas de besoin, apparaît préférable à la règle générale et contraignante. »_

34 _() Article 11, alinéa 3c) de la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés._

35 _() Proposition de règlement relatif à l'ENISA, Agence de l'Union européenne pour la cybersécurité, et abrogeant le règlement (UE) n° 526/2013, et relatif à la certification des technologies de l'information et des communications en matière de cybersécurité (règlement sur la cybersécurité) - COM(2017) 477 final : Article 43 : « Un système européen de certification de cybersécurité atteste que les produits et services TIC qui ont été certifiés conformément à ce système satisfont à des exigences spécifiées concernant leur capacité à résister, à un niveau d’assurance donné, à des actions visant à compromettre la disponibilité, l’authenticité, l’intégrité ou la confidentialité des données stockées, transmises ou traitées ou des fonctions ou services associés qui sont offerts ou accessibles par ces produits, processus, services et systèmes. »_

36 _() Le CIGREF est une association loi 1901, créée en 1970, qui regroupe près de 145 grandes entreprises et organismes français dans tous les secteurs d’activité._

37 _() Article 32 du RGPD._

38 _() Article 6 de la loi n° 78-17 du 6 janvier 1978._

39 _() Article 32 de la loi précitée._

40 _() Article 34 de la loi précitée._

41 _() Délibération n° 2013-378 du 5 décembre 2013 portant adoption d’une recommandation relative aux Cookies et aux autres traceurs visés par l'article 32-II de la loi du 6 janvier 1978._

42 _() Proposition de règlement du Parlement européen et du Conseil concernant le respect de la vie privée et la protection des données à caractère personnel dans les communications électroniques et abrogeant la directive 2002/58/CE (règlement «vie privée et communications électroniques») – COM (2017) 10._

43 _() Ce type de dispositif a été mis en place par l’ARCEP, dans le cadre de l’article L.40-1 du code des communications électroniques et des postes permet aux start-up qui le souhaitent, et plus largement à toute entreprise s'apprêtant à tester de nouveaux services, de bénéficier d'un allègement des obligations notamment liées aux attributions de fréquences et de ressources en numérotation._

44 _() Pour ce qui est du secret médical attaché aux traitements nécessaires aux fins de la médecine préventive, de la recherche médicale, des diagnostics médicaux, de l'administration de soins ou de traitements, ou de la gestion de service de santé, il ne sera plus exigé que ce soit seul un médecin qui puisse requérir la communication des données mais que cette communication ne peut être faite que sous l’autorité et en présence de ce dernier._

45 _() « Une profession soumise au secret professionnel et pour qui il existe des règles légales particulières, l’inspecteur général et les inspecteurs ne peuvent accéder, sans la présence d’un représentant de l’ordre professionnel, à leurs locaux que moyennant un accord écrit de la personne contrôlée ou bien moyennant l’accord du juge d’instruction. Il s’agit de conditions cumulatives (telles que chez les médecins, avocats ou journalistes). »_

46 _() Considérant 73 du RGPD._

47 _() Les mesures doivent intégrer des dispositions spécifiques relatives notamment :_

 _\- aux finalités du traitement ou des catégories de traitement ;_

 _\- aux catégories de données à caractère personnel ;_

 _\- à l’étendue des limitations introduites ;_

 _\- aux garanties destinées à prévenir les abus ou l’accès ou le transfert illicites ;_

 _\- à la détermination du responsable du traitement ou des catégories de responsables du traitement ;_

 _\- aux durées de conservation et aux garanties applicables, en tenant compte de la nature, de la portée et des finalités du traitement ou des catégories de traitement ;_

 _\- aux risques pour les droits et libertés des personnes concernées ;_

 _\- au droit des personnes concernées d’être informées de la limitation, à moins que cela ne risque de nuire à la finalité de la limitation._

48 _() Article 39._

49 _() Loi n° 2016-41 du 26 janvier 2016 de modernisation de notre système de santé et décret n°2016-1249 du 26 septembre 2016 relatif à l’action de groupe en matière de santé._

50 _() Titre V de la loi n° 2016-1547 du 18 novembre 2016 de modernisation de la justice du XXI e siècle._

51 _() Article 43_ ter _de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés._

52 _() Loi n° 2016-1547 du 18 novembre 2016 de modernisation de la justice du XXI e siècle._

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/annexe-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <span> </span> </div>

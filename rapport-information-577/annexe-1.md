<!-- TITLE: ANNEXE N° 1 -->
<!-- SUBTITLE: PRINCIPALES PROPOSITIONS -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/travaux-de-la-commission" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/annexe-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

1\. Porter à treize ans l’âge de consentement au traitement des données personnelles.

2\. Encourager la création d’un système de certification et de labellisation européen pour les produits, les entreprises et les organismes qui respectent les dispositions du règlement.

3\. Mettre en place un système de médiation dans une phase précontentieuse entre professionnels.

4\. Prendre en compte, dans l’exercice de définition des études d’impact, les caractéristiques des entreprises innovantes et la difficulté de mesurer les risques dans le cadre d’activités en phase d’expérimentation.

5\. Instaurer la possibilité de mettre en œuvre une action de groupe en responsabilité en cas de violation des dispositions du règlement et de la loi.

6\. Encourager le Gouvernement, habilité par ordonnance par l’article 20 du projet de loi, à améliorer l’intelligibilité et la cohérence du texte pour assurer sa bonne application par les acteurs économiques.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/travaux-de-la-commission" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/annexe-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

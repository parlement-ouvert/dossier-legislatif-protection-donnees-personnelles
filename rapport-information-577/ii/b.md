<!-- TITLE: B -->
<!-- SUBTITLE: LA DÉFINITION D’UN ESPACE EUROPÉEN DE PROTECTION DES DONNÉES PERSONNELLES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/ii/a/3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/ii/b/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

La bonne application des droits de la personne, inscrits au chapitre III du règlement, dépend en grande partie de la bonne coopération des agences nationales en charge de la protection des données personnelles, qui disposent de l’expertise et des moyens pour ce faire.

* [1 — La coopération entre les agences de protection des données personnelles](/rapport-information-577/ii/b/1)

* [2 — Le champ territorial d’application : une difficile coordination entre les États membres](/rapport-information-577/ii/b/2)

* [3 — L’extension du régime de responsabilité aux sous-traitants](/rapport-information-577/ii/b/3)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/ii/a/3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/ii/b/1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Entête -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/introduction" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

![](http://www.assemblee-nationale.fr/15/europe/rap-info/i0577-1.gif)
N° 577
______
ASSEMBLÉE NATIONALE
CONSTITUTION DU 4 OCTOBRE 1958
QUINZIÈME LÉGISLATURE
Enregistré à la Présidence de l'Assemblée nationale le 18 janvier 2018.
 **RAPPORT D’INFORMATION**
DÉPOSÉ
PAR LA COMMISSION DES AFFAIRES EUROPÉENNES (1)
portant observations sur le projet de loi
relatif à la **protection des données personnelles** (n° 490)
ET PRÉSENTÉ
PAR Mme Christine HENNION
Députée
——

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/introduction" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

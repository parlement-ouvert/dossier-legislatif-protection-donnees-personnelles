<!-- TITLE: II -->
<!-- SUBTITLE: LE PROJET DE LOI RELATIF AUX DONNÉES PERSONNELLES : UNE MODERNISATION BIENVENUE DU CADRE JURIDIQUE NATIONAL -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/i/b/4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/ii/a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [A — LE CHOIX COHÉRENT D’UNE HARMONISATION EUROPÉENNE MAXIMALE](/rapport-information-577/ii/a)

  * [1 — Le RGPD contient de larges marges de manœuvre nationales](/rapport-information-577/ii/a/1)

  * [2 — Un choix légistique cohérent mais peu clair](/rapport-information-577/ii/a/2)

  * [3 — La pertinence du règlement quant aux libertés fondamentales : l’exemple de l’accès des mineurs aux plateformes](/rapport-information-577/ii/a/3)

* [B — LA DÉFINITION D’UN ESPACE EUROPÉEN DE PROTECTION DES DONNÉES PERSONNELLES](/rapport-information-577/ii/b)

  * [1 — La coopération entre les agences de protection des données personnelles](/rapport-information-577/ii/b/1)

  * [2 — Le champ territorial d’application : une difficile coordination entre les États membres](/rapport-information-577/ii/b/2)

  * [3 — L’extension du régime de responsabilité aux sous-traitants](/rapport-information-577/ii/b/3)

* [C — LES NOUVELLES MISSIONS ET ACTIONS DES AUTORITÉS DE CONTRÔLE NATIONALES](/rapport-information-577/ii/c)

  * [1 — La certification et les instruments de droit souple](/rapport-information-577/ii/c/1)

  * [2 — Le traitement des données de l’État : une exception dommageable](/rapport-information-577/ii/c/2)

  * [3 — Le contrôle en ligne : une collaboration qui reste à construire](/rapport-information-577/ii/c/3)

  * [4 — Le référentiel de sanctions](/rapport-information-577/ii/c/4)

  * [5 — Les limites du contrôle : l’opposabilité du secret professionnel](/rapport-information-577/ii/c/5)

* [D — UNE PROTECTION DES CITOYENS À CONSOLIDER](/rapport-information-577/ii/d)

  * [1 — La limitation des droits dans le domaine numérique](/rapport-information-577/ii/d/1)

  * [2 — L’action de groupe : introduire des modalités de réparation](/rapport-information-577/ii/d/2)

  * [3 — Les actions de médiation](/rapport-information-577/ii/d/3)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/rapport-information-577/i/b/4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Rapport d'information de la commission des affaires européennes</span> <a href="/rapport-information-577/ii/a" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Délibération n°2017-299 du 30 novembre 2017 -->
<!-- SUBTITLE: Commission Nationale de l'Informatique et des Libertés -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Délibération n° 2017-299 du 30 novembre 2017 portant avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n°78-17 du janvier 1978 (demande d’avis n°17023753)</span> <a href="/deliberation-cnil-2017-299/i-observations-dordre-general" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

_Délibération n° 2017-299 du 30 novembre 2017 portant avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n°78-17 du janvier 1978
(demande d’avis n°17023753)_

* [Considérants](/deliberation-cnil-2017-299/considerants)
* [Introduction](/deliberation-cnil-2017-299/introduction)

* [I. Observations d’ordre général](/deliberation-cnil-2017-299/i-observations-dordre-general)

* [II. Examen article par article](/deliberation-cnil-2017-299/ii-examen-article-par-article)

  * [TITRE I<sup>er</sup> — DISPOSITIONS COMMUNES AU REGLEMENT ET A LA  DIRECTIVE 2016](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i)

    * [Chapitre I<sup>er</sup> — DISPOSITIONS RELATIVES A LA Commission NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i)

      * [Article 1<sup>er</sup> (Missions de la Commission)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-1)
      * [Article 2 (Composition de la Commission) et 3 (Commissaire du Gouvernement)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-2-composition-de-la-commission-et-3-commissaire-du-gouvernement)
      * [Article 4 (Pouvoirs de contrôle de la Commission)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-4-pouvoirs-de-controle-de-la-commission)
      * [Article 5 (Coopération)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-5-cooperation)
      * [Article 6 (Mesures correctrices)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-i/article-6-mesures-correctrices)

    * [Chapitre II — Dispositions relatives a certaines categories de donnees](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-ii)

      * [Article 7 (Données sensibles)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-i/chapitre-ii/article-7-donnees-sensibles)

  * [TITRE II — MARGES DE MANOEUVRE](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii)

    * [Chapitre I — Champ d’application](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-i)

      * [Article 8 (Champ d’application territorial)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-i/article-8-champ-dapplication-territorial)

    * [Chapitre II — Dispositions relatives a la simplification des formalites prealables a la mise en œuvre des traitements](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-ii)

      * [Article 9 (Formalités et NIR)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-ii/article-9-formalites-et-nir)

    * [Chapitre III — Obligations incombant aux responsables de traitements et sous-traitants](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iii)

      * [Articles 10 et 22 (Délégué à la protection des données)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iii/articles-10-et-22-delegue-a-la-protection-des-donnees)
      * [Articles 11 et 22 (Sous-traitant)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iii/articles-11-et-22-sous-traitant)

    * [Chapitre IV — Dispositions relatives a certaines categories particulieres de traitement](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv)

      * [Article 12 (Données d’infraction)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv/article-12-donnees-dinfraction)
      * [Article 13 (Archives, recherche scientifique et statistique)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv/article-13-archives-recherche-scientifique-et-statistique)
      * [Article 14 (Chapitre IX données de santé)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv/article-14-chapitre-ix-donnees-de-sante)

    * [Chapitre IV bis — Dispositions particulières relatives aux droits des personnes concernées](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv-bis)

      * [Article 15 (Mineurs)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv-bis/article-15-mineurs)
      * [Article 16 (Profilage)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv-bis/article-16-profilage)
      * [Article 17 (Limitation des droits des personnes pour certains traitements de l’Etat)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-iv-bis/article-17-limitation-des-droits-des-personnes-pour-certains-traitements-de-letat)

    * [Chapitre V — Voies de recours](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-v)

      * [Article 18 (Action de groupe)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-v/article-18-action-de-groupe)
      * [Article 19 (Action en justice)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-ii/chapitre-v/article-19-action-en-justice)

  * [TITRE III — DISPOSITIONS PORTANT TRANSPOSITION DE LA DIRECTIVE (articles 20, 21, 22 et 28)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-iii)

    * [Observations générales sur la transposition de la Directive](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-iii/observations-generales-sur-la-transposition-de-la-directive)
    * [Observations particulières sur la transposition de la Directive](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-iii/observations-particulieres-sur-la-transposition-de-la-directive)

  * [TITRE IV — HABILITATION A AMELIORER L’INTELLIGIBILITE DE LA LEGISLATION APPLICABLE A LA PROTECTION DES DONNEES](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-iv)

    * [Article 23 (Habilitation)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-iv/article-23-habilitation)

  * [TITRE V — DISPOSITIONS FINALES](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-v)

    * [Articles 24 et 25 (Liste dite de l’article 31 )](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-v/articles-24-et-25-liste-dite-de-larticle-31)
    * [Article 26 (Traitement des antécédents judiciaires)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-v/article-26-traitement-des-antecedents-judiciaires)
    * [Article 27 (Application aux PTOM)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-v/article-27-application-aux-ptom)
    * [Article 28 (Entrée en vigueur)](/deliberation-cnil-2017-299/ii-examen-article-par-article/titre-v/article-28-entree-en-vigueur)

* [Signataires](/deliberation-cnil-2017-299/signataires)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 10 — SOUS-TRAITANT](/etude-impact-490/titre-ii/chapitre-iii/article-10)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Délibération n° 2017-299 du 30 novembre 2017 portant avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n°78-17 du janvier 1978 (demande d’avis n°17023753)</span> <a href="/deliberation-cnil-2017-299/i-observations-dordre-general" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

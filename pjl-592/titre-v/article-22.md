<!-- TITLE: Article 22 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-v/article-21" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-v/article-23" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Pour les traitements ayant fait l’objet de formalités antérieurement au 25 mai 2018, la liste mentionnée à [l’article 31 de la loi n° 78-17](/loi-78-17/chapitre-iv/section-3/article-31) du 6 janvier 1978 précitée, arrêtée à cette date, est mise à la disposition du public, dans un format ouvert et aisément réutilisable pour une durée de dix ans.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 84 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-22/amendement-84)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-v/article-21" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-v/article-23" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

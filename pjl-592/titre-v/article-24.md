<!-- TITLE: Article 24 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-v/article-23-bis-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <span> </span> </div>

Les titres Ier à III et les articles [21](/loi-78-17/chapitre-iii/article-21) et [22](/loi-78-17/chapitre-iv/article-22) de la présente loi entrent en vigueur le 25 mai 2018.

Toutefois, les dispositions de l’[article 70-15](/pjl-490/titre-iii/article-19) de la loi n° 78-17 du 6 janvier 1978 précitée entrent en vigueur à une date fixée par décret, et au plus tard :

1° Le 6 mai 2023 lorsqu’une telle obligation exigerait des efforts disproportionnés ;

2° Le 6 mai 2026 lorsque, à défaut d’un tel report, il en résulterait de graves difficultés pour le fonctionnement du système de traitement automatisé.

La liste des traitements concernés par ces reports et les dates auxquelles, pour ces traitements, l’entrée en vigueur de cette obligation est reportée sont déterminées par voie réglementaire.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 20 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-24/amendement-20)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-v/article-23-bis-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <span> </span> </div>

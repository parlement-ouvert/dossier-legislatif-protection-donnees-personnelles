<!-- TITLE: Article 23 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-v/article-22" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-v/article-23-bis-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

I. – [L’article 230-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000032655577) du code de procédure pénale est ainsi modifié :

1° Le premier alinéa est ainsi rédigé :

« Le traitement des données à caractère personnel est opéré sous le contrôle du procureur de la République territorialement compétent qui, d’office ou à la demande de la personne concernée, ordonne qu’elles soient effacées, complétées ou rectifiées, notamment en cas de requalification judiciaire, ou qu’elles fassent l’objet d’une mention. La rectification pour requalification judiciaire est de droit. Le procureur de la République se prononce dans un délai de deux mois sur les suites qu’il convient de donner aux demandes qui lui sont adressées. La personne concernée peut former cette demande sans délai à la suite d’une décision devenue définitive de relaxe, d’acquittement, de condamnation avec dispense de peine ou dispense de mention au casier judiciaire, de non-lieu ou de classement sans suite. Dans les autres cas, la personne ne peut former sa demande, à peine d’irrecevabilité, que lorsque ne figure plus aucune mention dans le bulletin n° 2 de son casier judiciaire. En cas de décision de relaxe ou d’acquittement devenue définitive, les données personnelles concernant les personnes mises en cause sont effacées, sauf si le procureur de la République en prescrit le maintien, auquel cas elles font l’objet d’une mention. Lorsque le procureur de la République prescrit le maintien des données personnelles relatives à une personne ayant bénéficié d’une décision de relaxe ou d’acquittement devenue définitive, il en avise la personne concernée. En cas de décision de non-lieu ou de classement sans suite, les données personnelles concernant les personnes mises en cause font l’objet d’une mention, sauf si le procureur de la République ordonne l’effacement des données personnelles. Lorsque les données personnelles relatives à la personne concernée font l’objet d’une mention, elles ne peuvent faire l’objet d’une consultation dans le cadre des enquêtes administratives prévues aux articles [L. 114-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000025504941), L. 234-1 à L. 234-3 du code de la sécurité intérieure et à l’[article 17-1 de la loi n° 95-73 du 21 janvier 1995](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000369046&dateTexte=19950124) d’orientation et de programmation relative à la sécurité. Les décisions du procureur de la République prévues au présent alinéa ordonnant le maintien ou l’effacement des données personnelles ou ordonnant qu’elles fassent l’objet d’une mention sont prises pour des raisons liées à la finalité du fichier au regard de la nature ou des circonstances de commission de l’infraction ou de la personnalité de l’intéressé. » ;

2° Au troisième alinéa, les mots : « en matière d’effacement ou de rectification des données personnelles » sont supprimés.

I _bis (nouveau)_. – À la dernière phrase du deuxième alinéa de l’article 230-9 du code de procédure pénale, les mots : « d’un » sont remplacés par les mots : « de deux ».

II. – Le premier alinéa de [l’article 804](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000006578405&dateTexte=&categorieLien=cid) du code de procédure pénale est ainsi rédigé :

« Le présent code est applicable, dans sa rédaction résultant de loi n° du relative à la protection des données personnelles, en Nouvelle-Calédonie, en Polynésie française et dans les îles Wallis et Futuna, sous réserve des adaptations prévues au présent titre et aux seules exceptions : ».

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 60 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-23/amendement-60)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-v/article-22" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-v/article-23-bis-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

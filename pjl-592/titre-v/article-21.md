<!-- TITLE: Article 21 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-v/article-22" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

I. – La loi n° 78-17 du 6 janvier 1978 précitée est ainsi modifiée :

1° A _(nouveau)_ Au second alinéa du II de l’article 13, après la référence : « 3° », est insérée la référence : « du I » ;

1° L’article 15 est ainsi modifié :

 _a)_ Le quatrième alinéa est supprimé ;

 _b)_ _(nouveau)_ Aux cinquième et sixième alinéas, après la référence : « 2° », est insérée la référence : « du I » ;

 _c)_ _(nouveau)_ Au septième alinéa, après la référence : « 4° », est insérée la référence : « du I » ;

2° Le troisième alinéa de [l’article 16](/loi-78-17/chapitre-iii/article-16) est supprimé ;

2° _bis (nouveau)_ Au second alinéa de l’article 17, après la référence : « 2° », est insérée la référence : « du I » ;

2° _ter_ _(nouveau)_ Au second alinéa de l’article 21, après la référence : « 2° », est insérée la référence : « du I » ;

3° Au premier article de [l’article 29](/loi-78-17/chapitre-iv/section-2/article-29), la référence : « 25, » est supprimée ;

4° Le [I de l’article 30](/loi-78-17/chapitre-iv/section-3/article-30) est ainsi modifié :

 _a)_ Au premier alinéa, le mot : « déclarations, » est supprimé ;

 _b)_ Aux 2° et 6°, la référence : « 25, » est supprimée ;

5° Le [I de l’article 31](/loi-78-17/chapitre-iv/section-3/article-31) est ainsi modifié :

 _a)_ Au premier alinéa, la référence : « [23](/loi-78-17/chapitre-iv/section-1/article-23) à » est remplacée par la référence : « 26 et » ;

 _b)_ À la fin du 1°, les mots : « ou la date de la déclaration de ce traitement » sont supprimés ;

6° À la seconde phrase du second alinéa du II de [l’article 39](/loi-78-17/chapitre-v/section-2/article-39), les mots : « ou dans la déclaration » sont supprimés ;

6° _bis (nouveau)_ À l’article 42, la référence : « 25, » est supprimée ;

7° L’article 67 est ainsi modifié :

 _a)_ Au premier alinéa, les références : « 22, les 1° et 3° du I de [l’article 25](/loi-78-17/chapitre-iv/section-2/article-25), les articles » sont supprimées ;

 _b)_ Le quatrième alinéa est supprimé ;

 _c)_ La seconde phrase de l’avant-dernier alinéa est supprimée ;

8° L’article 70 est ainsi modifié :

 _a)_ Le premier alinéa est supprimé ;

 _b)_ À la deuxième phrase du second alinéa, les mots : « saisie d’une déclaration déposée en application des articles [23](/loi-78-17/chapitre-iv/section-1/article-23) ou [24](/loi-78-17/chapitre-iv/section-1/article-24) et faisant apparaître que des données à caractère personnel seront transférées vers cet État, la Commission nationale de l’informatique et des libertés délivre le récépissé et » sont remplacés par les mots : « consultée en application de [l’article 36 du règlement (UE) 2016/679](/loi-78-17/chapitre-v/section-1/article-36) du Parlement et du Conseil du 27 avril 2016 précité et en cas de transfert de données à caractère personnel vers cet État, la Commission nationale de l’informatique et des libertés » ;

9° La seconde phrase de [l’article 71](/loi-78-17/chapitre-xiii/article-71) est supprimée.

II _(nouveau)_. – L’article 226-16-1-A du code pénal est abrogé.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 32 — Adopté](/amendements-1-assemblee-hemicycle/article-21/amendement-32)
  * [Amendement n° 89 — Adopté](/amendements-1-assemblee-hemicycle/article-21/amendement-89)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-v/article-22" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 7 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

[L’article 8](/loi-78-17/chapitre-ii/section-2/article-8) de la loi n° 78-17 du 6 janvier 1978 précitée est ainsi modifié :

1° Le I est ainsi rédigé :

« I. – Il est interdit de traiter des données à caractère personnel, qui révèlent la prétendue origine raciale ou l’origine ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l’appartenance syndicale d’une personne physique, ou de traiter des données génétiques, des données biométriques aux fins d’identifier une personne physique de manière unique, des données concernant la santé ou des données concernant vie sexuelle ou l’orientation sexuelle d’une personne physique. » ;

2° Le II est ainsi modifié :

 _a)_ À la fin du 7°, les mots : « et dans les conditions prévues à l’article 25 de la présente loi » sont supprimés ;

 _b)_ Le 8° est ainsi rédigé :

« 8° Les traitements comportant des données concernant la santé justifiés par l’intérêt public et conformes aux dispositions du [chapitre IX](/loi-78-17/chapitre-ix) de la présente loi. » ;

 _c)_ Sont ajoutés des 9° et 10° ainsi rédigés : :

« 9° Les traitements conformes aux règlements types mentionnés au _b_ du 2° du I de l’article 11 mis en  œuvre par les employeurs ou les administrations qui portent sur des données biométriques nécessaires au contrôle de l’accès aux lieux de travail ainsi qu’aux appareils et aux applications utilisés dans le cadre des missions confiées aux salariés ou aux agents ;

« 10° _(nouveau)_ Les traitements portant sur la réutilisation des informations publiques figurant dans les jugements et décisions mentionnés à l’article L. 10 du code de justice administrative et à l’article L. 111-13 du code de l’organisation judiciaire, sous réserve que ces traitements n’aient ni pour objet ni pour effet de permettre la réidentification des personnes concernées. » ;

3° Le III est ainsi rédigé :

« III. – N’entrent pas dans le champ de l’interdiction prévue au I les données à caractère personnel mentionnées au même I qui sont appelées à faire l’objet, à bref délai, d’un procédé d’anonymisation préalablement reconnu conforme aux dispositions de la présente loi par la Commission nationale de l’informatique et des libertés. » ;

4° Le IV est ainsi rédigé :

« IV. – De même, ne sont pas soumis à l’interdiction prévue au I les traitements, automatisés ou non, justifiés par l’intérêt public et autorisés dans les conditions prévues au II de l’[article 26](/loi-78-17/chapitre-iv/section-2/article-26). »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 8 — Non soutenu](/amendements-1-assemblee-hemicycle/article-7/amendement-8)
  * [Amendement n° 139 — Non soutenu](/amendements-1-assemblee-hemicycle/article-7/amendement-139)
  * [Amendement n° 106 — Rejeté](/amendements-1-assemblee-hemicycle/article-7/amendement-106)
  * [Amendement n° 64 — Adopté](/amendements-1-assemblee-hemicycle/article-7/amendement-64)
  * [Amendement n° 112 — Adopté](/amendements-1-assemblee-hemicycle/article-7/amendement-112)
  * [Amendement n° 101 — Adopté](/amendements-1-assemblee-hemicycle/article-7/amendement-101)
  * [Amendement n° 50 — Rejeté](/amendements-1-assemblee-hemicycle/article-7/amendement-50)
  * [Amendement n° 65 — Rejeté](/amendements-1-assemblee-hemicycle/article-7/amendement-65)
  * [Amendement n° 135 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-7/amendement-135)
  * [Amendement n° 63 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-7/amendement-63)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

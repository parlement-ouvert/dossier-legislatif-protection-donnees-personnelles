<!-- TITLE: Article 1er -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i/article-1-bis-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

[L’article 11 de la loi n° 78-17 du 6 janvier 1978](/loi-78-17/chapitre-iii/article-11) relative à l’informatique, aux fichiers et aux libertés est ainsi modifié :

1° Au début du premier alinéa, est ajoutée la mention : « I. – » ;

2° Après la première phrase du même premier alinéa, est insérée une phrase ainsi rédigée : « Elle est l’autorité de contrôle nationale au sens et pour l’application du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité » ;

2° _bis_ _(nouveau)_ Le 1° est complété par les mots : « et peut, à cette fin, apporter une information personnalisée aux petites et moyennes entreprises » ;

3° Le 2° est ainsi modifié :

 _aa) (nouveau)_ Après le mot : « conformément », la fin du premier alinéa est ainsi rédigée : « aux textes relatifs à la protection des données personnelles. » ;

 _a)_ Au _a_ , les mots : « autorise les traitements mentionnés à l’article 25, » et les mots : « et reçoit les déclarations relatives aux autres traitements » sont supprimés ;

 _b)_ Après le même _a_ , il est inséré un _a_ bis ainsi rédigé :

« _a_ bis _)_ Elle établit et publie des lignes directrices, recommandations ou référentiels destinés à faciliter la mise en conformité des traitements de données à caractère personnel avec les textes relatifs à la protection des données à caractère personnel et à procéder à l’évaluation préalable des risques par les responsables de traitement et leurs sous-traitants. Elle encourage l’élaboration de codes de conduite définissant les obligations qui incombent aux responsables de traitement et à leurs sous-traitants, compte tenu du risque inhérent aux traitements de données à caractère personnel pour les droits et libertés des personnes physiques, notamment des mineurs, et des besoins spécifiques des micro-entreprises, petites entreprises et moyennes entreprises ; elle homologue et publie les méthodologies de référence destinées à favoriser la conformité des traitements de données de santé à caractère personnel ; »

 _c)_ Le _b_ est ainsi rédigé :

« _b)_ En concertation avec les organismes publics et privés représentatifs des acteurs concernés, elle établit et publie des règlements types en vue d’assurer la sécurité des systèmes de traitement de données à caractère personnel et de régir les traitements de données biométriques, génétiques et de santé. À ce titre, sauf pour les traitements mis en œuvre pour le compte de l’État, agissant dans l’exercice de ses prérogatives de puissance publique, elle peut prescrire des mesures, notamment techniques et organisationnelles, supplémentaires pour le traitement des données biométriques, génétiques et de santé en application du 4 de l’article 9 du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité et des garanties complémentaires en matière de traitement de données à caractère personnel relatives aux condamnations pénales et aux infractions conformément à [l’article 10](/reglement-2016-679/chapitre-ii/article-10) du même règlement ; »

 _d)_ Après le _f_ , il est inséré un _f_ bis ainsi rédigé :

« _f_ bis _)_ Elle peut décider de certifier des personnes, des produits, des systèmes de données ou des procédures aux fins de reconnaître qu’ils se conforment au [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité et à la présente loi. Elle prend en considération, à cette fin, les besoins spécifiques des micro-entreprises, petites entreprises et moyennes entreprises. Elle agrée, aux mêmes fins, des organismes certificateurs, sur la base, le cas échéant, de leur accréditation par l’organisme national d’accréditation, mentionné au _b_ du 1 de l’article 43 du même règlement, dans des conditions précisées par décret en Conseil d’État pris après avis de la Commission nationale de l’informatique et des libertés. La commission élabore ou approuve les critères des référentiels de certification et d’agrément. Elle peut établir des exigences supplémentaires en matière de normes d’accréditation ; »

 _e)_ Au _g_ , après le mot : « certification », sont insérés les mots : « , par des tiers agréés ou accrédités selon les modalités mentionnées au _f_ bis du présent 2°, » ;

 _f)_ À la fin du _h_ , les mots : « d’accès concernant les traitements mentionnés aux [articles 41](/loi-78-17/chapitre-v/section-2/article-41) et [42](/loi-78-17/chapitre-v/section-2/article-42) » sont remplacés par les mots : « ou saisines prévues aux [articles 41](/loi-78-17-pjl-490/chapitre-v/section-2/article-41), [42](/loi-78-17-pjl-490/chapitre-v/section-2/article-42) et [70-22](/loi-78-17-pjl-490/chapitre-xiii/section-3/article-70-22) » ;

 _g)_ Il est ajouté un _i_ ainsi rédigé :

« _i)_ Elle peut établir une liste des traitements susceptibles de créer un risque élevé devant faire l’objet d’une consultation préalable conformément à [l’article 70-4](/reglement-2016-679/chapitre-vii/section-3/article-70) ; »

4° Après la première phrase du _a_ du 4°, est insérée une phrase ainsi rédigée : « Elle peut également être consultée par le Président de l’Assemblée nationale, par le Président du Sénat ou par les commissions compétentes de l’Assemblée nationale et du Sénat sur toute proposition de loi relative à la protection des données à caractère personnel ou au traitement de telles données. » ;

5° Après le __ même 4°, il est inséré un 5° ainsi rédigé :

« 5° Elle peut présenter des observations devant toute juridiction à l’occasion d’un litige relatif à l’application du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité et de la présente loi. » ;

6° Au début du vingt-sixième alinéa, est ajoutée la mention : « II. – » ;

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 37 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-37)
  * [Amendement n° 102 — Adopté](/amendements-1-assemblee-hemicycle/article-premier/amendement-102)
  * [Amendement n° 179 — Tombé](/amendements-1-assemblee-hemicycle/article-premier/amendement-179)
  * [Amendement n° 7 — Retiré](/amendements-1-assemblee-hemicycle/article-premier/amendement-7)
  * [Amendement n° 39 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-39)
  * [Amendement n° 38 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-38)
  * [Amendement n° 111 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-111)
  * [Amendement n° 181 — Retiré](/amendements-1-assemblee-hemicycle/article-premier/amendement-181)
  * [Amendement n° 3 — Non soutenu](/amendements-1-assemblee-hemicycle/article-premier/amendement-3)
  * [Amendement n° 156 — Adopté](/amendements-1-assemblee-hemicycle/article-premier/amendement-156)
  * [Amendement n° 40 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-40)
  * [Amendement n° 104 — Adopté](/amendements-1-assemblee-hemicycle/article-premier/amendement-104)
  * [Amendement n° 105 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-105)
  * [Amendement n° 152 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-152)
  * [Amendement n° 163 — Rejeté](/amendements-1-assemblee-hemicycle/article-premier/amendement-163)
  * [Amendement n° 41 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-premier/amendement-41)
  * [Amendement n° 2 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-premier/amendement-2)
  * [Amendement n° 142 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-premier/amendement-142)
  * [Amendement n° 4 — Non soutenu](/amendements-1-assemblee-hemicycle/article-1-bis/amendement-4)
  * [Amendement n° 5 — Non soutenu](/amendements-1-assemblee-hemicycle/article-1-bis/amendement-5)
  * [Amendement n° 6 — Non soutenu](/amendements-1-assemblee-hemicycle/article-1-bis/amendement-6)
  * [Amendement n° 99 — Adopté](/amendements-1-assemblee-hemicycle/article-1-bis/amendement-99)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i/article-1-bis-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

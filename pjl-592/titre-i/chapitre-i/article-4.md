<!-- TITLE: Article 4 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i/article-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

[L’article 44](/loi-78-17/chapitre-vi/article-44) de la loi n° 78-17 du 6 janvier 1978 précitée est ainsi modifié :

1° Au premier aliéna du I, les mots : « et qui sont à usage professionnel » sont supprimés ;

2° Le II est ainsi modifié :

 _a)_ À la première phrase du premier alinéa, les mots : « de locaux professionnels privés » sont remplacés par les mots : « de ces lieux, locaux, enceintes, installations ou établissements » ;

 _b)_ La dernière phrase du dernier alinéa est complétée par les mots : « dont la finalité est l’exercice effectif des missions prévues au III » ;

3° Les trois premiers alinéas du III sont remplacés par deux alinéas ainsi rédigés :

« III. – Pour l’exercice des missions relevant de la Commission nationale de l’informatique et des libertés en application du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité et de la présente loi, les membres et agents mentionnés au premier alinéa du I du présent article peuvent demander communication de tous documents nécessaires à l’accomplissement de leur mission, quel qu’en soit le support, et en prendre copie. Ils peuvent recueillir, notamment sur place ou sur convocation, tout renseignement et toute justification utiles et nécessaires à l’accomplissement de leur mission. Ils peuvent accéder, dans des conditions préservant la confidentialité à l’égard des tiers, aux programmes informatiques et aux données ainsi qu’en demander la transcription par tout traitement approprié dans des documents directement utilisables pour les besoins du contrôle. Le secret ne peut leur être opposé sauf concernant les informations couvertes par le secret professionnel applicable aux relations entre un avocat et son client, par le secret des sources des traitements journalistiques ou, sous réserve du deuxième alinéa du présent III, par le secret médical.

« Le secret médical est opposable s’agissant des informations qui figurent dans un traitement nécessaire aux fins de la médecine préventive, de la recherche médicale, des diagnostics médicaux, de l’administration de soins ou de traitements, ou de la gestion de service de santé. Toutefois la communication des données médicales individuelles incluses dans cette catégorie de traitement ne peut se faire que sous l’autorité et en présence d’un médecin. » ;

4° Avant le dernier alinéa du même III, il est inséré un alinéa ainsi rédigé :

« Pour le contrôle de services de communication au public en ligne, les membres et agents mentionnés au premier alinéa du I peuvent réaliser toute opération en ligne nécessaire à leur mission sous une identité d’emprunt. L’utilisation d’une identité d’emprunt est sans incidence sur la régularité des constatations effectuées conformément au troisième alinéa du présent III. Un décret en Conseil d’État, pris après avis de la Commission nationale de l’informatique et des libertés, précise les conditions dans lesquelles ces membres et agents procèdent dans ces cas à leurs constatations. » ;

5° Il est ajouté un V ainsi rédigé :

« V. – Dans l’exercice de son pouvoir de contrôle portant sur les traitements relevant du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité et de la présente loi, la Commission nationale de l’informatique et des libertés n’est pas compétente pour contrôler les opérations de traitement effectuées, dans l’exercice de leur fonction juridictionnelle, par les juridictions. »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 153 — Retiré](/amendements-1-assemblee-hemicycle/article-4/amendement-153)
  * [Amendement n° 76 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-76)
  * [Amendement n° 23 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-23)
  * [Amendement n° 24 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-24)
  * [Amendement n° 162 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-162)
  * [Amendement n° 122 — Non soutenu](/amendements-1-assemblee-hemicycle/article-4/amendement-122)
  * [Amendement n° 77 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-77)
  * [Amendement n° 78 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-78)
  * [Amendement n° 161 — Rejeté](/amendements-1-assemblee-hemicycle/article-4/amendement-161)
  * [Amendement n° 124 — Retiré](/amendements-1-assemblee-hemicycle/article-4/amendement-124)
  * [Amendement n° 49 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-4/amendement-49)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i/article-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

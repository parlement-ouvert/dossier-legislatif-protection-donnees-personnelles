<!-- TITLE: Article 2 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i/article-1-bis-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i/article-2-bis-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Le I de l’[article 13 de la loi n° 78-17 du 6 janvier 1978](/loi-78-17/chapitre-iii/article-13) précitée est ainsi modifié :

1° _(nouveau)_ Au 6°, le mot : « ou » est remplacé par le mot : « et » ;

2° Au 7°, après le mot : « numérique », sont insérés les mots : « et des questions touchant aux libertés individuelles ».

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 158 — Adopté](/amendements-1-assemblee-hemicycle/article-2/amendement-158)
  * [Amendement n° 42 — Rejeté](/amendements-1-assemblee-hemicycle/article-2/amendement-42)
  * [Amendement n° 43 — Rejeté](/amendements-1-assemblee-hemicycle/article-2/amendement-43)
  * [Amendement n° 54 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-2/amendement-54)
  * [Amendement n° 123 — Adopté](/amendements-1-assemblee-hemicycle/article-2-bis/amendement-123)
  * [Amendement n° 98 — Adopté](/amendements-1-assemblee-hemicycle/article-2-bis/amendement-98)
  * [Amendement n° 100 — Adopté](/amendements-1-assemblee-hemicycle/article-2-bis/amendement-100)
  * [Amendement n° 72 — Non soutenu](/amendements-1-assemblee-hemicycle/article-2-bis/amendement-72)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i/article-1-bis-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i/article-2-bis-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 5 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i/article-6" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

La loi n° 78-17 du 6 janvier 1978 précitée est ainsi modifiée :

1° A _(nouveau)_ Après l’article 48, il est inséré un chapitre VII _bis_ , intitulé : « De la coopération », et comprenant les articles 49 à 49-5 tels qu’ils résultent des 1° à 3° du présent article ;

1° [L’article 49](/loi-78-17/chapitre-vii/article-49) est ainsi rédigé :

« [_Art. 49.](/loi-78-17/chapitre-vii/article-49) –_ Dans les conditions prévues aux articles [60](/reglement-2016-679/chapitre-vii/section-1/article-60) à [67](/reglement-2016-679/chapitre-vii/section-2/article-67) du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité, la Commission nationale de l’informatique et des libertés met en œuvre des procédures de coopération et d’assistance mutuelle avec les autorités de contrôle des autres États membres de l’Union européenne et réalise avec ces autorités des opérations conjointes.

« La commission, le président, le bureau, la formation restreinte et les agents de la commission mettent en œuvre, chacun pour ce qui le concerne, les procédures mentionnées au premier alinéa du présent article. » ;

2° Après le même article 49, sont insérés des articles 49-1 à 49-4 ainsi rédigés :

« _Art. 49-1. –_ I. – Pour l’application de [l’article 62 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vii/section-1/article-62) du Parlement européen et du Conseil du 27 avril 2016 précité, la Commission nationale de l’informatique et des libertés coopère avec les autorités de contrôle des autres États membres de l’Union européenne, dans les conditions prévues au présent article. Cette coopération n’est pas applicable aux traitements qui ne relèvent pas du champ d’application du droit de l’Union européenne.

« II. – Qu’elle agisse en tant qu’autorité de contrôle chef de file ou en tant qu’autorité concernée au sens des articles [4](/reglement-2016-679/chapitre-i/article-4) et [56](/reglement-2016-679/chapitre-vi/section-2/article-56) du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité, la Commission nationale de l’informatique et des libertés est compétente pour traiter une réclamation ou une éventuelle violation des dispositions du même règlement affectant par ailleurs d’autres États membres. Le président de la commission invite les autres autorités de contrôle concernées à participer aux opérations de contrôle conjointes qu’il décide de conduire.

« III. – Lorsqu’une opération de contrôle conjointe se déroule sur le territoire français, des membres ou agents habilités de la commission, agissant en tant qu’autorité de contrôle d’accueil, sont présents aux côtés des membres et agents des autres autorités de contrôle participant, le cas échéant, à l’opération. À la demande de l’autorité de contrôle d’un État membre, le président de la commission peut habiliter, par décision particulière, ceux des membres ou agents de l’autorité de contrôle concernée qui présentent des garanties comparables à celles requises des agents de la commission, en application de [l’article 19](/loi-78-17/chapitre-iii/article-19) de la présente loi, à exercer, sous son autorité, tout ou partie des pouvoirs de vérification et d’enquête dont disposent les membres et les agents de la commission.

« IV. – Lorsque la commission est invitée à contribuer à une opération de contrôle conjointe décidée par l’autorité de contrôle d’un autre État membre, le président de la commission se prononce sur le principe et les conditions de la participation, désigne les membres et agents habilités et en informe l’autorité requérante dans les conditions prévues à [l’article 62 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vii/section-1/article-62) du Parlement européen et du Conseil du 27 avril 2016 précité.

« _Art. 49-2._ – I. – Les traitements mentionnés à [l’article 70-1](/loi-78-17-pjl-490/chapitre-xiii/section-1/article-70-1) font l’objet d’une coopération entre la Commission nationale de l’informatique et des libertés et les autorités de contrôle des autres États membres de l’Union européenne dans les conditions prévues au présent article.

« II. – La commission communique aux autorités de contrôle des autres États membres les informations utiles et leur prête assistance en mettant notamment en œuvre, à leur demande, des mesures de contrôle telles que des mesures de consultation, d’inspection et d’enquête.

« La commission répond à une demande d’assistance mutuelle formulée par une autre autorité de contrôle dans les meilleurs délais et au plus tard un mois après réception de la demande contenant toutes les informations nécessaires, notamment sa finalité et ses motifs. Elle ne peut refuser de satisfaire à cette demande que si elle n’est pas compétente pour traiter l’objet de la demande ou les mesures qu’elle est invitée à exécuter, ou si une disposition du droit de l’Union européenne ou du droit français y fait obstacle.

« La commission informe l’autorité de contrôle requérante des résultats obtenus ou, selon le cas, de l’avancement du dossier ou des mesures prises pour donner suite à la demande.

« La commission peut, pour l’exercice de ses missions, solliciter l’assistance d’une autorité de contrôle d’un autre État membre de l’Union européenne.

« La commission donne les motifs de tout refus de satisfaire à une demande lorsqu’elle estime ne pas être compétente ou lorsqu’elle considère que satisfaire à la demande constituerait une violation du droit de l’Union européenne ou du droit français.

« _Art. 49-3. –_ Lorsque la commission agit en tant qu’autorité de contrôle chef de file s’agissant d’un traitement transfrontalier au sein de l’Union européenne, elle communique sans tarder aux autres autorités de contrôle concernées le rapport du rapporteur mentionné au premier alinéa de l’article 47 ainsi que l’ensemble des informations utiles de la procédure ayant permis d’établir le rapport, avant l’éventuelle audition du responsable de traitement ou de son sous-traitant. Les autorités concernées sont mises en mesure d’assister, par tout moyen de retransmission approprié, à l’audition par la formation restreinte du responsable de traitement ou de son sous-traitant, ou de prendre connaissance d’un procès-verbal dressé à la suite de l’audition.

« Après en avoir délibéré, la formation restreinte soumet son projet de décision aux autres autorités de contrôle concernées conformément à la procédure définie à [l’article 60 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vii/section-1/article-60) du Parlement européen et du Conseil du 27 avril 2016 précité. À ce titre, elle se prononce sur la prise en compte des objections pertinentes et motivées émises par ces autorités et saisit, si elle décide d’écarter l’une des objections, le comité européen de la protection des données conformément à l’article 65 du même règlement.

« Les conditions d’application du présent article sont définies par décret en Conseil d’État, après avis de la Commission nationale de l’informatique et des libertés.

« _Art. 49-4. –_ Lorsque la commission agit en tant qu’autorité de contrôle concernée, au sens du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité, le président de la commission est saisi des projets de mesures correctrices soumis à la commission par une autorité de contrôle chef de file.

« Lorsque ces mesures sont d’objet équivalent à celles définies aux [I et III de l’article 45](/loi-78-17/chapitre-vii/article-45) de la présente loi, le président décide, le cas échéant, d’émettre une objection pertinente et motivée selon les modalités prévues à [l’article 60 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vii/section-1/article-60) du Parlement européen et du Conseil du 27 avril 2016 précité.

« Lorsque ces mesures sont d’objet équivalent à celles définies au [II de l’article 45](/loi-78-17/chapitre-vii/article-45) de la présente loi, le président saisit la formation restreinte. Le président de la formation restreinte ou le membre de la formation restreinte que celui-ci désigne peut, le cas échéant, émettre une objection pertinente et motivée selon les mêmes modalités. » ;

3° _(nouveau)_ [L’article 49](/loi-78-17/chapitre-vii/article-49) _bis_ devient l’article 49-5.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 26 — Rejeté](/amendements-1-assemblee-hemicycle/article-5/amendement-26)
  * [Amendement n° 136 — Non soutenu](/amendements-1-assemblee-hemicycle/article-5/amendement-136)
  * [Amendement n° 45 — Rejeté](/amendements-1-assemblee-hemicycle/article-5/amendement-45)
  * [Amendement n° 137 — Non soutenu](/amendements-1-assemblee-hemicycle/article-5/amendement-137)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i/article-6" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

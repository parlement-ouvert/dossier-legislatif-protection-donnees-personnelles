<!-- TITLE: Article 1er bis (nouveau) -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i/article-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i/article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

L’[article 4 _bis_ de l’ordonnance n° 58-1100 du 17 novembre 1958 relative au fonctionnement des assemblées parlementaires](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000028288243&cidTexte=LEGITEXT000006069203&dateTexte=20180129) est ainsi modifié :

1° Après le premier alinéa, il est inséré un alinéa ainsi rédigé :

« Le président d’une assemblée parlementaire peut également saisir la Commission nationale de l’informatique et des libertés d’une proposition de loi dans les mêmes conditions. » ;

2° Au deuxième alinéa, après les mots : « Conseil d’État », sont insérés les mots : « ou à la Commission nationale de l’informatique et des libertés » ;

3° Au troisième alinéa, après les mots : « Conseil d’État », sont insérés les mots : « ou de la Commission nationale de l’informatique et des libertés ».

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i/article-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i/article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

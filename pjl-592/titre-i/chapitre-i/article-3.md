<!-- TITLE: Article 3 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i/article-2-bis-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

La loi n° 78-17 du 6 janvier 1978 précitée est ainsi modifiée :

1° Au premier alinéa de [l’article 17](/loi-78-17/chapitre-iii/article-17), après le mot : « restreinte », sont insérés les mots : « prend les mesures et » et, après le mot : « découlant », sont insérés les mots : « du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité et » ;

2° Après le même premier alinéa, il est inséré un alinéa ainsi rédigé :

« Ses membres délibèrent hors de la présence des agents de la commission, à l’exception de ceux chargés de la tenue de la séance. » ;

3° Les deuxième et dernier alinéas de [l’article 18](/loi-78-17/chapitre-iii/article-18) sont ainsi rédigés :

« Le commissaire du Gouvernement assiste à toutes les délibérations de la commission réunie en formation plénière, ainsi qu’à celles des réunions de son bureau qui ont pour objet l’exercice des attributions déléguées en application de [l’article 16](/loi-78-17/chapitre-iii/article-16). Il peut assister aux séances de la formation restreinte, sans être présent au délibéré. Il est rendu destinataire de l’ensemble des avis et décisions de la commission et de la formation restreinte.

« Sauf en matière de mesures ou de sanctions relevant du [chapitre VII](/loi-78-17/chapitre-vii), il peut provoquer une seconde délibération de la commission, qui doit intervenir dans les dix jours suivant la délibération initiale ».

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 157 — Adopté](/amendements-1-assemblee-hemicycle/article-3/amendement-157)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i/article-2-bis-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i/article-4" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 6 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

La loi n° 78-17 du 6 janvier 1978 précitée est ainsi modifiée :

1° L’intitulé du [chapitre VII](/loi-78-17/chapitre-vii) est ainsi rédigé : « Mesures et sanctions prises par la formation restreinte de la Commission nationale de l’informatique et des libertés » ;

2° [L’article 45](/loi-78-17/chapitre-vii/article-45) est ainsi rédigé :

« [_Art. 45.](/loi-78-17/chapitre-vii/article-45) –_ I. – Le président de la Commission nationale de l’informatique et des libertés peut avertir un responsable de traitement ou son sous-traitant du fait que les opérations de traitement envisagées sont susceptibles de violer les dispositions du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité ou de la présente loi.

« II. – Lorsque le responsable de traitement ou son sous-traitant ne respecte pas les obligations résultant du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut saisir la formation restreinte de la commission en vue du prononcé, après procédure contradictoire, de l’une ou de plusieurs des mesures suivantes :

« 1° Un rappel à l’ordre ;

« 2° Une injonction de mettre en conformité le traitement avec les obligations résultant de la présente loi ou du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité ou de satisfaire aux demandes présentées par la personne concernée en vue d’exercer ses droits, qui peut être assortie, sauf dans des cas où le traitement est mis en œuvre par l’État, d’une astreinte dont le montant ne peut excéder 100 000 € par jour ;

« 3° À l’exception des traitements qui intéressent la sûreté de l’État ou la défense ou de ceux relevant du [chapitre XIII](/loi-78-17/chapitre-xiii) de la présente loi lorsqu’ils sont mis en œuvre pour le compte de l’État, la limitation temporaire ou définitive du traitement, son interdiction ou le retrait d’une autorisation accordée en application du même règlement ou de la présente loi ;

« 4° Le retrait d’une certification ou l’injonction, à l’organisme certificateur concerné, de refuser une certification ou de retirer la certification accordée ;

« 5° La suspension des flux de données adressées à un destinataire situé dans un pays tiers ou à une organisation internationale ;

« 6° La suspension partielle ou totale de la décision d’approbation des règles d’entreprise contraignantes ;

« 7° À l’exception des cas où le traitement est mis en œuvre par l’État, une amende administrative ne pouvant excéder 10 millions d’euros ou, s’agissant d’une entreprise, 2 % du chiffre d’affaires annuel mondial total de l’exercice précédent, le montant le plus élevé étant retenu. Dans les hypothèses mentionnées aux 5 et 6 de [l’article 83 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-viii/article-83) du Parlement européen et du Conseil du 27 avril 2016 précité, ces plafonds sont portés, respectivement, à 20 millions d’euros et 4 % du chiffre d’affaires. La formation restreinte prend en compte, dans la détermination du montant de l’amende, les critères précisés au même article 83.

« Lorsque la formation restreinte a prononcé une sanction pécuniaire devenue définitive avant que le juge pénal ait statué définitivement sur les mêmes faits ou des faits connexes, celui-ci peut ordonner que l’amende administrative s’impute sur l’amende pénale qu’il prononce.

« Les sanctions pécuniaires sont recouvrées comme les créances de l’État étrangères à l’impôt et au domaine.

« Le projet de mesure est le cas échéant soumis aux autres autorités de contrôle concernées selon les modalités définies à l’article 60 du même règlement.

« III. – Lorsque le responsable de traitement ou son sous-traitant ne respecte pas les obligations découlant du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut également prononcer à son égard une mise en demeure, dans le délai qu’il fixe :

« 1° De satisfaire aux demandes présentées par la personne concernée en vue d’exercer ses droits ;

« 2° De mettre les opérations de traitement en conformité avec les dispositions applicables ;

« 3° À l’exception des traitements qui intéressent la sûreté de l’État ou la défense, de communiquer à la personne concernée une violation de données à caractère personnel ;

« 4° De rectifier ou d’effacer des données à caractère personnel, ou de limiter le traitement de ces données.

« Dans le cas prévu au 4°, le président peut, dans les mêmes conditions, mettre en demeure le responsable de traitement ou son sous-traitant de notifier aux destinataires des données les mesures qu’il a prises.

« Le délai de mise en conformité peut être fixé à vingt-quatre heures en cas d’extrême urgence.

« Le président prononce, le cas échéant, la clôture de la procédure de mise en demeure.

« Le président peut demander au bureau de rendre publique la mise en demeure. Dans ce cas, la décision de clôture de la procédure de mise en demeure fait l’objet de la même publicité. » ;

3° [L’article 46](/loi-78-17/chapitre-vii/article-46) est ainsi rédigé :

« [_Art. 46.](/loi-78-17/chapitre-vii/article-46) –_ I. – Lorsque le non-respect des dispositions du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité ou de la présente loi entraîne une violation des droits et libertés mentionnés à l’article 1er et que le président de la commission considère qu’il est urgent d’intervenir, il saisit la formation restreinte qui peut, dans le cadre d’une procédure d’urgence contradictoire définie par décret en Conseil d’État, adopter l’une des mesures suivantes :

« 1° L’interruption provisoire de la mise en œuvre du traitement, y compris d’un transfert de données hors de l’Union européenne, pour une durée maximale de trois mois, si le traitement n’est pas au nombre de ceux qui intéressent la sûreté de l’État ou la défense ou de ceux relevant du [chapitre XIII](/loi-78-17/chapitre-xiii) lorsqu’ils sont mis en œuvre pour le compte de l’État ;

« 2° La limitation du traitement de certaines des données à caractère personnel traitées, pour une durée maximale de trois mois, si le traitement n’est pas au nombre de ceux qui intéressent la sûreté de l’État ou la défense ou de ceux relevant du même [chapitre XIII](/loi-78-17/chapitre-xiii) lorsqu’ils sont mis en œuvre pour le compte de l’État ;

« 3° La suspension provisoire de la certification délivrée au responsable de traitement ou à son sous-traitant ;

« 4° La suspension provisoire de l’agrément délivré à un organisme de certification ou un organisme chargé du respect d’un code de conduite ;

« 5° La suspension provisoire de l’autorisation délivrée sur le fondement du III de l’article 54 de la présente loi.

« 6° L’injonction de mettre en conformité le traitement avec les obligations résultant du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité ou de la présente loi, qui peut être assortie, sauf dans le cas où le traitement est mis en œuvre par l’État, d’une astreinte dont le montant ne peut excéder 100 000 € par jour ;

« 7° Un rappel à l’ordre ;

« 8° L’information du Premier ministre pour qu’il prenne, le cas échéant, les mesures permettant de faire cesser la violation constatée, si le traitement en cause est au nombre de ceux qui intéressent la sûreté de l’État ou la défense ou de ceux relevant du [chapitre XIII](/loi-78-17/chapitre-xiii) de la présente loi lorsqu’ils sont mis en œuvre pour le compte de l’État. Le Premier ministre fait alors connaître à la formation restreinte les suites qu’il a données à cette information au plus tard quinze jours après l’avoir reçue.

« II. – En cas de circonstances exceptionnelles prévues au [1 de l’article 66 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-vii/section-2/article-66) du Parlement européen et du Conseil du 27 avril 2016 précité, lorsque la formation restreinte adopte les mesures provisoires prévues aux 1° à 4° du I du présent article, elle informe sans délai de la teneur des mesures prises et de leurs motifs les autres autorités de contrôle concernées, le comité européen de la protection des données et la Commission européenne.

« Lorsque la formation restreinte a pris de telles mesures et qu’elle estime que des mesures définitives doivent être prises, elle met en œuvre les dispositions du [2 de l’article 66 du règlement](/reglement-2016-679/chapitre-vii/section-2/article-66) (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité.

« III. – Pour les traitements relevant du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité, lorsqu’une autorité de contrôle compétente en application du même règlement n’a pas pris de mesure appropriée dans une situation où il est urgent d’intervenir afin de protéger les droits et libertés des personnes concernées, la formation restreinte, saisie par le président de la commission, peut demander au comité européen de la protection des données un avis d’urgence ou une décision contraignante d’urgence dans les conditions et selon les modalités prévues aux 3 et 4 de l’article 66 dudit règlement.

« IV. – En cas d’atteinte grave et immédiate aux droits et libertés mentionnés à l’article 1er de la présente loi, le président de la commission peut en outre demander, par la voie du référé, à la juridiction compétente d’ordonner, le cas échéant sous astreinte, toute mesure nécessaire à la sauvegarde de ces droits et libertés. » ;

4° [L’article 47](/loi-78-17/chapitre-vii/article-47) est ainsi rédigé :

« [_Art. 47.](/loi-78-17/chapitre-vii/article-47) –_ Les mesures prévues au [II de l’article 45](/loi-78-17/chapitre-vii/article-45) et aux 1° à 7° du [I de l’article 46](/loi-78-17/chapitre-vii/article-46) sont prononcées sur la base d’un rapport établi par l’un des membres de la Commission nationale de l’informatique et des libertés, désigné par le président de celle-ci parmi les membres n’appartenant pas à la formation restreinte. Ce rapport est notifié au responsable de traitement ou à son sous-traitant, qui peut déposer des observations et se faire représenter ou assister. Le rapporteur peut présenter des observations orales à la formation restreinte mais ne prend pas part à ses délibérations. La formation restreinte peut entendre toute personne dont l’audition lui paraît susceptible de contribuer utilement à son information, y compris, à la demande du secrétaire général de la commission, les agents des services de celle-ci.

« La formation restreinte peut rendre publiques les mesures qu’elle prend. Elle peut également ordonner leur insertion dans des publications, journaux et supports qu’elle désigne aux frais des personnes sanctionnées.

« Sans préjudice des obligations d’information qui incombent au responsable de traitement ou à son sous-traitant en application de [l’article 34 du règlement (UE) 2016/679](/reglement-2016-679/chapitre-iv/section-2/article-34) du Parlement européen et du Conseil du 27 avril 2016 précité, la formation restreinte peut ordonner que ce responsable ou ce sous-traitant informe individuellement, à ses frais, chacune des personnes concernées de la violation relevée des dispositions de la présente loi ou du règlement précité ainsi que, le cas échéant, de la mesure prononcée. » ;

5° [L’article 48](/loi-78-17/chapitre-vii/article-48) est ainsi rédigé :

« [_Art. 48.](/loi-78-17/chapitre-vii/article-48) –_ Lorsqu’un organisme de certification ou un organisme chargé du respect d’un code de conduite a manqué à ses obligations ou n’a pas respecté les dispositions du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité ou de la présente loi, le président de la Commission nationale de l’informatique et des libertés peut, le cas échéant après mise en demeure, saisir la formation restreinte de la commission qui peut prononcer, dans les mêmes conditions que celles prévues aux articles [45](/loi-78-17/chapitre-vii/article-45) à [47](/loi-78-17/chapitre-vii/article-47), le retrait de l’agrément qui lui a été délivré. »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 131 — Non soutenu](/amendements-1-assemblee-hemicycle/article-6/amendement-131)
  * [Amendement n° 48 — Rejeté](/amendements-1-assemblee-hemicycle/article-6/amendement-48)
  * [Amendement n° 11 — Rejeté](/amendements-1-assemblee-hemicycle/article-6/amendement-11)
  * [Amendement n° 79 — Rejeté](/amendements-1-assemblee-hemicycle/article-6/amendement-79)
  * [Amendement n° 47 — Rejeté](/amendements-1-assemblee-hemicycle/article-6/amendement-47)
  * [Amendement n° 59 — Rejeté](/amendements-1-assemblee-hemicycle/article-6/amendement-59)
  * [Amendement n° 80 — Rejeté](/amendements-1-assemblee-hemicycle/article-6/amendement-80)
  * [Amendement n° 85 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-6/amendement-85)
  * [Amendement n° 46 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-6/amendement-46)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-i/chapitre-i/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

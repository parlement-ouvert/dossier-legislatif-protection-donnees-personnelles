<!-- TITLE: Article 20 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

I. – Dans les conditions prévues à [l’article 38 de la Constitution](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000571356&idArticle=LEGIARTI000006527512&dateTexte=&categorieLien=cid), le Gouvernement est autorisé à prendre par voie d’ordonnance les mesures relevant du domaine de la loi nécessaires :

1° À la réécriture de l’ensemble de la [loi n° 78-17 du 6 janvier 1978](/loi-78-17) relative à l’informatique, aux fichiers et aux libertés afin d’apporter les corrections formelles et les adaptations nécessaires à la simplification et à la cohérence ainsi qu’à la simplicité de la mise en œuvre par les personnes concernées des dispositions qui mettent le droit national en conformité avec le [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE et transposent la [directive (UE) 2016/680](/directive-2016-680) du Parlement européen et du Conseil du 27 avril 2016 relative à la protection des personnes physiques à l’égard du traitement des données à caractère personnel par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d’enquêtes et de poursuites en la matière ou d’exécution de sanctions pénales, et à la libre circulation de ces données, et abrogeant la décision cadre 2008/977/JAI du Conseil dans l’attente de l’appréciation par la Cour de justice de l’Union européenne de la validité de cette décision d’adéquation, telles que résultant de la présente loi ;

2° Pour mettre en cohérence avec ces changements l’ensemble de la législation applicable à la protection des données à caractère personnel, apporter les modifications qui seraient rendues nécessaires pour assurer le respect de la hiérarchie des normes et la cohérence rédactionnelle des textes, harmoniser l’état du droit, remédier aux éventuelles erreurs et omissions résultant de la présente loi et abroger les dispositions devenues sans objet ;

3° À l’adaptation et aux extensions aux collectivités régies par l’article 73 de la Constitution des dispositions prévues aux 1° et 2° du présent I, ainsi qu’à l’application de la présente loi et des mesures mentionnées aux mêmes 1° et 2° en Nouvelle-Calédonie, à Wallis-et-Futuna en Polynésie française et dans les Terres australes et antarctique françaises.

II. – Cette ordonnance est prise, après avis de la Commission nationale de l’informatique et des libertés, dans un délai de six mois à compter de la promulgation de la présente loi.

III. – Un projet de loi de ratification est déposé devant le Parlement dans un délai de six mois à compter de la publication de l’ordonnance.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 31 — Rejeté](/amendements-1-assemblee-hemicycle/article-20/amendement-31)
  * [Amendement n° 58 — Rejeté](/amendements-1-assemblee-hemicycle/article-20/amendement-58)
  * [Amendement n° 120 — Rejeté](/amendements-1-assemblee-hemicycle/article-20/amendement-120)
  * [Amendement n° 129 — Rejeté](/amendements-1-assemblee-hemicycle/article-20/amendement-129)
  * [Amendement n° 146 — Non soutenu](/amendements-1-assemblee-hemicycle/article-20/amendement-146)
  * [Amendement n° 155 — Adopté](/amendements-1-assemblee-hemicycle/apres-l-article-20/amendement-155)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: TITRE IER -->
<!-- SUBTITLE: DISPOSITIONS D’ADAPTATION COMMUNES AU RÈGLEMENT (UE) 2016/679 DU PARLEMENT EUROPÉEN ET DU CONSEIL DU 27 AVRIL 2016 ET À LA DIRECTIVE (UE) 2016/680
DU PARLEMENT EUROPÉEN ET DU CONSEIL DU 27 AVRIL 2016 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/entete" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Chapitre Ier — Dispositions relatives
à la Commission nationale de l’informatique et des libertés](/pjl-592/titre-i/chapitre-i)

  * [Article 1er](/pjl-592/titre-i/chapitre-i/article-1)
  * [Article 1er bis (nouveau)](/pjl-592/titre-i/chapitre-i/article-1-bis-nouveau)
  * [Article 2](/pjl-592/titre-i/chapitre-i/article-2)
  * [Article 2 bis (nouveau)](/pjl-592/titre-i/chapitre-i/article-2-bis-nouveau)
  * [Article 3](/pjl-592/titre-i/chapitre-i/article-3)
  * [Article 4](/pjl-592/titre-i/chapitre-i/article-4)
  * [Article 5](/pjl-592/titre-i/chapitre-i/article-5)
  * [Article 6](/pjl-592/titre-i/chapitre-i/article-6)

* [Chapitre II — Dispositions relatives à certaines catégories de données](/pjl-592/titre-i/chapitre-ii)

  * [Article 7](/pjl-592/titre-i/chapitre-ii/article-7)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/entete" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-i/chapitre-i" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

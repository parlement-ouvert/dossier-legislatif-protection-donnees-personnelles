<!-- TITLE: Article 14 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-v/article-14-a-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-v/article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

[L’article 10](/loi-78-17/chapitre-ii/section-2/article-10) de la loi n° 78-17 du 6 janvier 1978 précitée est ainsi modifié :

1° Le deuxième alinéa est ainsi modifié :

 _a)_ _(Supprimé)_

 _b)_ Les mots : « définir le profil de l’intéressé » sont remplacés par le mot : « prévoir » ;

 _c)_ Après le mot : « aspects », la fin est ainsi rédigée : « personnels relatifs à la personne concernée, à l’exception : » ;

1° _bis_ _(nouveau)_ Après le même deuxième alinéa, sont insérés des 1° et 2° ainsi rédigés :

« 1° Des cas mentionnés aux _a_ et _c_ du 2 de l’article 22 du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité et sous les réserves mentionnées au 3 du même article 22 ;

« 2° __ Des décisions administratives individuelles prises dans le respect de l’article L. 311-3-1 et du chapitre Ier du titre Ier du livre IV du code des relations entre le public et l’administration, à condition que le traitement ne porte pas sur des données mentionnées au [I de l’article 8](/loi-78-17/chapitre-ii/section-2/article-8) de la présente loi. » ;

2° Le dernier alinéa est ainsi rédigé :

« Pour les décisions administratives individuelles mentionnées au deuxième alinéa du présent article, le responsable de traitement s’assure de la maîtrise du traitement algorithmique et de ses évolutions afin de pouvoir expliquer, en détails et sous une forme intelligible, à la personne concernée la manière dont le traitement a été mis en œuvre à son égard. »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 86 — Rejeté](/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-86)
  * [Amendement n° 91 — Rejeté](/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-91)
  * [Amendement n° 92 — Rejeté](/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-92)
  * [Amendement n° 183 — Non soutenu](/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-183)
  * [Amendement n° 108 — Non soutenu](/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-108)
  * [Amendement n° 184 — Adopté](/amendements-1-assemblee-hemicycle/avant-l-article-14-a/amendement-184)
  * [Amendement n° 35 — Rejeté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-35)
  * [Amendement n° 69 — Rejeté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-69)
  * [Amendement n° 21 — Rejeté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-21)
  * [Amendement n° 10 — Rejeté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-10)
  * [Amendement n° 103 — Adopté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-103)
  * [Amendement n° 182 — Adopté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-182)
  * [Amendement n° 9 — Tombé](/amendements-1-assemblee-hemicycle/article-14-a/amendement-9)
  * [Amendement n° 12 — Adopté](/amendements-1-assemblee-hemicycle/article-14-a/amendement-12)
  * [Amendement n° 19 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-14-a/amendement-19)
  * [Amendement n° 53 — Rejeté](/amendements-1-assemblee-hemicycle/article-14/amendement-53)
  * [Amendement n° 119 — Rejeté](/amendements-1-assemblee-hemicycle/article-14/amendement-119)
  * [Amendement n° 127 — Adopté](/amendements-1-assemblee-hemicycle/article-14/amendement-127)
  * [Amendement n° 61 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-14/amendement-61)
  * [Amendement n° 154 — Adopté](/amendements-1-assemblee-hemicycle/apres-l-article-14/amendement-154)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-v/article-14-a-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-v/article-15" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

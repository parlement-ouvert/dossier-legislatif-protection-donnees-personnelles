<!-- TITLE: Article 15 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-v/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-vi" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

L’article 40 de la loi n° 78-17 du 6 janvier 1978 précitée est complété par un III ainsi rédigé :

« III. – Un décret en Conseil d’État, pris après avis de la Commission nationale de l’informatique et des libertés, fixe la liste des traitements et des catégories de traitements autorisés à déroger au droit à la communication d’une violation de données régi par l’article 34 du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité lorsque la notification d’une divulgation ou d’un accès non autorisé à ces données est susceptible de représenter un risque pour la sécurité nationale, la défense nationale ou la sécurité publique. La dérogation prévue au présent III n’est applicable qu’aux seuls traitements de données à caractère personnel nécessaires au respect d’une obligation légale qui requiert le traitement de ces données ou à l’exercice d’une mission d’intérêt public dont est investi le responsable de traitement. »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 74 — Rejeté](/amendements-1-assemblee-hemicycle/article-15/amendement-74)
  * [Amendement n° 149 — Rejeté](/amendements-1-assemblee-hemicycle/article-15/amendement-149)
  * [Amendement n° 160 — Rejeté](/amendements-1-assemblee-hemicycle/article-15/amendement-160)
  * [Amendement n° 55 — Rejeté](/amendements-1-assemblee-hemicycle/article-15/amendement-55)
  * [Amendement n° 132 — Non soutenu](/amendements-1-assemblee-hemicycle/article-15/amendement-132)
  * [Amendement n° 56 — Rejeté](/amendements-1-assemblee-hemicycle/article-15/amendement-56)
  * [Amendement n° 71 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-15/amendement-71)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-v/article-14" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-vi" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

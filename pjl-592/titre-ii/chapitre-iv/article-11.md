<!-- TITLE: Article 11 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-iv/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

[L’article 9](/loi-78-17/chapitre-ii/section-2/article-9) de la loi n° 78-17 du 6 janvier 1978 précitée est ainsi modifié :

1° Au premier alinéa, les mots : « infractions, condamnations et mesures de sûreté ne peuvent être mis en œuvre que » sont remplacés par les mots : « condamnations pénales, aux infractions ou aux mesures de sûreté connexes ne peuvent être effectués que sous le contrôle de l’autorité publique ou » ;

2° Le 1° est complété par les mots : « ainsi que les personnes morales de droit privé collaborant au service public de la justice et appartenant à des catégories dont la liste est fixée par décret en Conseil d’État, pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés, dans la mesure strictement nécessaire à leur mission » ;

3° Le 3° est ainsi rédigé :

« 3° Les personnes physiques ou morales, aux fins de leur permettre de préparer et le cas échant, d’exercer et de suivre une action en justice en tant que victime, mise en cause, ou pour le compte de ceux-ci et de faire exécuter la décision rendue, pour une durée proportionnée à cette finalité ; la communication à un tiers n’est alors possible que sous les mêmes conditions et dans la mesure strictement nécessaire à la poursuite de ces mêmes finalités ; »

4° Il est ajouté un 5° ainsi rédigé :

« 5° Les réutilisateurs des informations publiques figurant dans les jugements mentionnés à l’article L. 10 du code de justice administrative et les décisions mentionnées à l’article [L. 111-13 du code de l’organisation judiciaire](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071164&idArticle=LEGIARTI000033206155), sous réserve que les traitements mis en œuvre n’aient ni pour objet ni pour effet de permettre la réidentification des personnes concernées. »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 67 — Rejeté](/amendements-1-assemblee-hemicycle/article-11/amendement-67)
  * [Amendement n° 126 — Non soutenu](/amendements-1-assemblee-hemicycle/article-11/amendement-126)
  * [Amendement n° 51 — Rejeté](/amendements-1-assemblee-hemicycle/article-11/amendement-51)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-iv/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 13 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-iv/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

Le chapitre IX de la même loi est ainsi rédigé :

 _« Chapitre IX_

 _ **« Traitements de données à caractère personnel
dans le domaine de la santé**_

 _« Section 1_

 _ **« Dispositions générales**_

« [_Art. 53_.](/loi-78-17/chapitre-ix/article-53) – Outre les dispositions du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité, les traitements contenant des données concernant la santé des personnes sont soumis aux dispositions du présent chapitre, à l’exception des catégories de traitements suivantes :

« 1° Les traitements relevant des 1° à [6° du II de l’article 8](/loi-78-17/chapitre-ii/section-2/article-8) ;

« 2° Les traitements permettant d’effectuer des études à partir des données recueillies en application du [6° du II de l’article 8](/loi-78-17/chapitre-ii/section-2/article-8) lorsque ces études sont réalisées par les personnels assurant ce suivi et destinées à leur usage exclusif ;

« 3° Les traitements effectués à des fins de remboursement ou de contrôle par les organismes chargés de la gestion d’un régime de base d’assurance maladie ;

« 4° Les traitements effectués au sein des établissements de santé par les médecins responsables de l’information médicale, dans les conditions prévues au deuxième alinéa de l’[article L. 6113-7 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006690710) ;

« 5° Les traitements effectués par les agences régionales de santé, par l’État et par la personne publique désignée par lui en application du premier alinéa de l’[article L. 6113-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021940593&cidTexte=LEGITEXT000006072665&dateTexte=20180112) du même code, dans le cadre défini au même [article L. 6113-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021940593&cidTexte=LEGITEXT000006072665&dateTexte=20180112).

« [_Art. 54. –_ I.](/loi-78-17/chapitre-ix/article-54) – Les traitements relevant du présent chapitre ne peuvent être mis en œuvre qu’en considération de la finalité d’intérêt public qu’ils présentent.

« II. – Des référentiels et règlements types, au sens des _a_ bis et _b_ du 2° de l’article 11, s’appliquant aux traitements relevant du présent chapitre sont établis par la Commission nationale de l’informatique et des libertés, en concertation avec l’Institut national des données de santé mentionné à [l’article L. 1462-1 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031923960) et des organismes publics et privés représentatifs des acteurs concernés.

« Les traitements conformes à ces référentiels peuvent être mis en œuvre à la condition que leurs responsables adressent préalablement à la Commission nationale de l’informatique et des libertés une déclaration attestant de cette conformité.

« Ces référentiels peuvent également porter sur la description et les garanties de procédure permettant la mise à disposition en vue de leur traitement de jeux de données de santé présentant un faible risque d’impact sur la vie privée.

« III. – Les traitements mentionnés au premier alinéa du I qui ne sont pas conformes à un référentiel mentionné au II ne peuvent être mis en œuvre qu’après autorisation par la Commission nationale de l’informatique et des libertés.

« L’Institut national des données de santé mentionné à [l’article L. 1462-1 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031923960) peut se saisir ou être saisi, dans des conditions définies par décret en Conseil d’État, par la Commission nationale de l’informatique et des libertés ou le ministre chargé de la santé sur le caractère d’intérêt public que présente le traitement.

« IV. – La Commission nationale de l’informatique et des libertés peut, par décision unique, délivrer à un même demandeur une autorisation pour des traitements répondant à une même finalité, portant sur des catégories de données identiques et ayant des catégories de destinataires identiques.

« V. – La Commission nationale de l’informatique et des libertés se prononce dans un délai de deux mois à compter de la réception de la demande. Toutefois, ce délai peut être prolongé une fois pour la même durée sur décision motivée de son président ou lorsque l’Institut national des données de santé est saisi en application du II du présent article.

« Lorsque la Commission nationale de l’informatique et des libertés ne s’est pas prononcée dans ces délais, la demande d’autorisation est réputée acceptée. Cette disposition n’est toutefois pas applicable si l’autorisation fait l’objet d’un avis préalable en application de la section 2 du présent chapitre et que l’avis ou les avis rendus ne sont pas expressément favorables.

« [_Art. 55_](/loi-78-17/chapitre-ix/article-55). – Par dérogation à [l’article 54](/loi-78-17/chapitre-ix/article-54), les traitements de données à caractère personnel dans le domaine de la santé mis en  œuvre par les organismes ou les services chargés d’une mission de service public figurant sur une liste fixée par arrêté des ministres chargés de la santé et de la sécurité sociale, pris après avis de la Commission nationale de l’informatique et des libertés, ayant pour seule finalité de répondre, en cas de situation d’urgence, à une alerte sanitaire et d’en gérer les suites, au sens de la [section 1 du chapitre III du titre Ier du livre IV du code de la santé publique](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000032404559&cidTexte=LEGITEXT000006072665&dateTexte=20180112), sont soumis aux seules dispositions de la [section 3 du chapitre IV du règlement (UE) 2016/79](/reglement-2016-679/chapitre-iv/section-3) du Parlement européen et du Conseil du 27 avril 2016 précité.

« Les traitements mentionnés au premier alinéa du présent article qui utilisent le numéro d’inscription des personnes au répertoire national d’identification des personnes physiques sont mis en œuvre dans les conditions prévues à [l’article 22](/loi-78-17/chapitre-iv/article-22) de la présente loi.

« Les dérogations régies par le premier alinéa du présent article prennent fin un an après la création du traitement si ce dernier continue à être mis en œuvre au delà de ce délai.

« [_Art. 56_.](/loi-78-17/chapitre-ix/article-56) – Nonobstant les règles relatives au secret professionnel, les membres des professions de santé peuvent transmettre au responsable de traitement de données autorisé en application de [l’article 54](/loi-78-17/chapitre-ix/article-54) les données à caractère personnel qu’ils détiennent.

« Lorsque ces données permettent l’identification des personnes, leur transmission doit être effectuée dans des conditions de nature à garantir leur confidentialité. La Commission nationale de l’informatique et des libertés peut adopter des recommandations ou des référentiels sur les procédés techniques à mettre en œuvre.

« Lorsque le résultat du traitement de données est rendu public, l’identification directe ou indirecte des personnes concernées doit être impossible.

« Les personnes appelées à mettre en œuvre le traitement de données ainsi que celles qui ont accès aux données sur lesquelles il porte sont astreintes au secret professionnel sous les peines prévues à [l’article 226-13 du code pénal](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417945).

« [_Art. 57.](/loi-78-17/chapitre-ix/article-57) –_ Toute personne a le droit de s’opposer à ce que des données à caractère personnel la concernant fassent l’objet de la levée du secret professionnel rendue nécessaire par un traitement de la nature de ceux mentionnés à l’[article 53](/loi-78-17/chapitre-ix/article-53).

« Les informations concernant les personnes décédées, y compris celles qui figurent sur les certificats des causes de décès, peuvent faire l’objet d’un traitement de données, sauf si l’intéressé a, de son vivant, exprimé son refus par écrit.

« [_Art. 58_.](/loi-78-17/chapitre-ix/article-58) – Les personnes auprès desquelles sont recueillies des données à caractère personnel ou à propos desquelles de telles données sont transmises sont individuellement informées conformément aux dispositions du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité.

« Toutefois, ces informations peuvent ne pas être délivrées si la personne concernée a entendu faire usage du droit qui lui est reconnu par [l’article L. 1111-2 du code de la santé](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685758) publique d’être laissée dans l’ignorance d’un diagnostic ou d’un pronostic.

[_« Art. 59_.](/loi-78-17/chapitre-ix/article-59) – Sont destinataires de l’information et exercent les droits de la personne concernée par le traitement les titulaires de l’exercice de l’autorité parentale, pour les mineurs, ou la personne chargée d’une mission de représentation dans le cadre d’une tutelle, d’une habilitation familiale ou d’un mandat de protection future, pour les majeurs protégés dont l’état ne leur permet pas de prendre seuls une décision personnelle éclairée.

« Par dérogation au premier alinéa du présent article, pour les traitements de données à caractère personnel réalisés dans le cadre de recherches mentionnées aux 2° et 3° de [l’article L. 1121-1 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032722870&cidTexte=LEGITEXT000006072665) ou d’études ou d’évaluations dans le domaine de la santé, ayant une finalité d’intérêt public et incluant des personnes mineures, l’information peut être effectuée auprès d’un seul des titulaires de l’exercice de l’autorité parentale, s’il est impossible d’informer l’autre titulaire ou s’il ne peut être consulté dans des délais compatibles avec les exigences méthodologiques propres à la réalisation de la recherche, de l’étude ou de l’évaluation au regard de ses finalités. Le présent alinéa ne fait pas obstacle à l’exercice ultérieur, par chaque titulaire de l’exercice de l’autorité parentale, des droits mentionnés au premier alinéa.

« Pour ces traitements, le mineur âgé de quinze ans ou plus peut s’opposer à ce que les titulaires de l’exercice de l’autorité parentale aient accès aux données le concernant recueillies au cours de la recherche, de l’étude ou de l’évaluation. Le mineur reçoit alors l’information et exerce seul ses droits.

« Pour ces mêmes traitements, le mineur âgé de quinze ans ou plus peut s’opposer à ce que les titulaires de l’exercice de l’autorité parentale soient informés du traitement de données si le fait d’y participer conduit à révéler une information sur une action de prévention, un dépistage, un diagnostic, un traitement ou une intervention pour laquelle le mineur s’est expressément opposé à la consultation des titulaires de l’autorité parentale, en application des articles [L. 1111-5](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685769) et [L. 1111-5-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031917409) du code de la santé publique, ou si les liens de famille sont rompus et que le mineur bénéficie à titre personnel du remboursement des prestations en nature de l’assurance maladie et maternité et de la couverture complémentaire mise en place par la [loi n° 99-641 du 27 juillet 1999](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000198392) portant création d’une couverture maladie universelle. Il exerce alors seul ses droits.

« [_Art. 60_.](/loi-78-17/chapitre-ix/article-60) – Une information relative aux dispositions du présent chapitre doit notamment être assurée dans tout établissement ou centre où s’exercent des activités de prévention, de diagnostic et de soins donnant lieu à la transmission de données à caractère personnel en vue d’un traitement mentionné au présent chapitre.

 _« Section 2_

 _ **« Dispositions particulières relatives aux traitements à des fins de recherche, d’étude ou d’évaluation dans le domaine de la santé.**_

« [_Art. 61_.](/loi-78-17/chapitre-ix/article-61) – Les traitements automatisés de données à caractère personnel dont la finalité est ou devient la recherche ou les études dans le domaine de la santé ainsi que l’évaluation ou l’analyse des pratiques ou des activités de soins ou de prévention sont soumis à la section 1 du présent chapitre, sous réserve de la présente section.

« Dans le cas où la recherche nécessite l’examen des caractéristiques génétiques, le consentement éclairé et exprès des personnes concernées doit être obtenu préalablement à la mise en œuvre du traitement de données. Les dispositions du présent alinéa ne sont pas applicables aux recherches réalisées en application de l’article L. 1131-1-1 du code de la santé publique.

« [_Art. 62_.](/loi-78-17-pjl-490/chapitre-ix/section-2/article-62) – Au titre des référentiels mentionnés au II de [l’article 54](/loi-78-17/chapitre-ix/article-54) de la présente loi, des méthodologies de référence sont homologuées et publiées par la Commission nationale de l’informatique et des libertés. Elles sont établies en concertation avec l’Institut national des données de santé mentionné à [l’article L. 1462-1 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031923960) et des organismes publics et privés représentatifs des acteurs concernés.

« Lorsque le traitement est conforme à une méthodologie de référence, il peut être mis en œuvre, sans autorisation mentionnée à [l’article 54](/loi-78-17/chapitre-ix/article-54) de la présente loi, à la condition que son responsable adresse préalablement à la Commission nationale de l’informatique et des libertés une déclaration attestant de cette conformité.

« _Art. 62-1 (nouveau)_. – Dans le cas où la recherche nécessite l’examen des caractéristiques génétiques, le consentement éclairé et exprès des personnes concernées doit être obtenu préalablement à la mise en  œuvre du traitement de données. Les dispositions du présent article ne sont pas applicables aux recherches réalisées en application de l’article L. 1131-1-1 du code de la santé publique.

« [_Art. 63_.](/loi-78-17-pjl-490/chapitre-ix/section-2/article-63) – L’autorisation du traitement est accordée par la Commission nationale de l’informatique et des libertés dans les conditions définies à [l’article 54](/loi-78-17/chapitre-ix/article-54), après avis :

« 1° Du comité compétent de protection des personnes mentionné à [l’article L. 1123-6 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685877), pour les demandes d’autorisation relatives aux recherches impliquant la personne humaine mentionnées à [l’article L. 1121-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032722870&cidTexte=LEGITEXT000006072665) du même code ;

« 2° Du comité d’expertise pour les recherches, les études et les évaluations dans le domaine de la santé, pour les demandes d’autorisation relatives à des études ou à des évaluations ainsi qu’à des recherches n’impliquant pas la personne humaine, au sens du 1° du présent article. Un décret en Conseil d’État, pris après avis de la Commission nationale de l’informatique et des libertés, fixe la composition de ce comité et définit ses règles de fonctionnement. Les membres du comité d’expertise sont soumis à [l’article L. 1451-1 du code de la santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000022019483).

« Les dossiers présentés dans le cadre de la présente section, à l’exclusion des recherches impliquant la personne humaine, sont déposés auprès d’un secrétariat unique assuré par l’Institut national des données de santé, qui assure leur orientation vers les instances compétentes. »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 68 — Rejeté](/amendements-1-assemblee-hemicycle/article-13/amendement-68)
  * [Amendement n° 128 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-128)
  * [Amendement n° 148 — Rejeté](/amendements-1-assemblee-hemicycle/article-13/amendement-148)
  * [Amendement n° 125 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-125)
  * [Amendement n° 176 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-176)
  * [Amendement n° 94 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-94)
  * [Amendement n° 175 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-175)
  * [Amendement n° 29 — Non soutenu](/amendements-1-assemblee-hemicycle/article-13/amendement-29)
  * [Amendement n° 36 — Non soutenu](/amendements-1-assemblee-hemicycle/article-13/amendement-36)
  * [Amendement n° 30 — Non soutenu](/amendements-1-assemblee-hemicycle/article-13/amendement-30)
  * [Amendement n° 141 — Rejeté](/amendements-1-assemblee-hemicycle/article-13/amendement-141)
  * [Amendement n° 95 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-95)
  * [Amendement n° 174 — Adopté](/amendements-1-assemblee-hemicycle/article-13/amendement-174)
  * [Amendement n° 134 — Retiré](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-134)
  * [Amendement n° 90 — Adopté](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-90)
  * [Amendement n° 144 — Adopté](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-144)
  * [Amendement n° 167 — Adopté](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-167)
  * [Amendement n° 62 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-62)
  * [Amendement n° 118 — Non soutenu](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-118)
  * [Amendement n° 75 — Retiré](/amendements-1-assemblee-hemicycle/apres-l-article-13/amendement-75)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-iv/article-12" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-v" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

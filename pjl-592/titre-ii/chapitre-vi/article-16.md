<!-- TITLE: Article 16 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-vi/article-16-a-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-vi/article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

La section 2 du chapitre V __ de la loi n° 78-17 du 6 janvier 1978 précitée est complétée par un article 43 _quater_ ainsi rédigé :

« _Art. 43_ quater. – Toute personne peut mandater une association ou une organisation mentionnée au IV de [l’article 43 _ter_](/loi-78-17/chapitre-v/section-2/article-43-ter) aux fins d’exercer en son nom les droits prévus aux articles [77](/reglement-2016-679/chapitre-viii/article-77) à [79](/reglement-2016-679/chapitre-viii/article-79) du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité. Elle peut également les mandater pour agir devant la Commission nationale de l’informatique et des libertés, contre celle-ci devant un juge ou contre le responsable de traitement ou son sous-traitant devant une juridiction lorsqu’est en cause un traitement relevant du [chapitre XIII](/loi-78-17/chapitre-xiii) de la présente loi. »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 82 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-82)
  * [Amendement n° 164 — Adopté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-164)
  * [Amendement n° 87 — Adopté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-87)
  * [Amendement n° 165 — Adopté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-165)
  * [Amendement n° 70 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-70)
  * [Amendement n° 73 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-73)
  * [Amendement n° 109 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-109)
  * [Amendement n° 150 — Rejeté](/amendements-1-assemblee-hemicycle/article-16-a/amendement-150)
  * [Amendement n° 133 — Non soutenu](/amendements-1-assemblee-hemicycle/article-16/amendement-133)
  * [Amendement n° 88 — Adopté](/amendements-1-assemblee-hemicycle/article-16/amendement-88)
  * [Amendement n° 110 — Rejeté](/amendements-1-assemblee-hemicycle/article-16/amendement-110)
  * [Amendement n° 151 — Rejeté](/amendements-1-assemblee-hemicycle/article-16/amendement-151)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-vi/article-16-a-nouveau" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-vi/article-17" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 16 A (nouveau) -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-vi" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-vi/article-16" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

L’article 43 _ter_ de la loi n° 78-17 du 6 janvier 1978 précitée est ainsi modifié :

1° Le III est ainsi rédigé :

« III. – Cette action peut être exercée en vue soit de la cessation du manquement mentionné au II, soit de l’engagement de la responsabilité de la personne ayant causé le dommage afin d’obtenir la réparation des préjudices matériels et moraux subis, soit de ces deux fins. » ;

2° Le IV est complété par un alinéa ainsi rédigé :

« Lorsque l’action tend à la réparation des préjudices subis, elle s’exerce dans le cadre de la procédure individuelle de réparation définie au chapitre Ier du titre V de la loi n° 2016-1547 du 18 novembre 2016 de modernisation de la justice du XXIe siècle. »

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-vi" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-vi/article-16" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

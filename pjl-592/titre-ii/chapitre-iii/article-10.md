<!-- TITLE: Article 10 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

[L’article 35](/loi-78-17/chapitre-v/section-1/article-35) de la loi n° 78-17 du 6 janvier 1978 précitée est ainsi modifié :

1° _(nouveau)_ Au début du premier alinéa, est ajoutée la mention : « I. – » ;

2° Sont ajoutés deux alinéas ainsi rédigés :

« Le présent I est applicable aux traitements ne relevant ni du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité, ni du chapitre XIII de la présente loi.

« II. – Dans le champ d’application du [règlement (UE) 2016/679](/reglement-2016-679) du Parlement européen et du Conseil du 27 avril 2016 précité, le sous-traitant respecte les conditions prévues par ce règlement. »

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 107 — Rejeté](/amendements-1-assemblee-hemicycle/avant-l-article-10/amendement-107)
  * [Amendement n° 177 — Retiré](/amendements-1-assemblee-hemicycle/article-10/amendement-177)
  * [Amendement n° 159 — Rejeté](/amendements-1-assemblee-hemicycle/article-10/amendement-159)
  * [Amendement n° 114 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-10/amendement-114)
  * [Amendement n° 115 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-10/amendement-115)
  * [Amendement n° 116 — Rejeté](/amendements-1-assemblee-hemicycle/apres-l-article-10/amendement-116)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-iv" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 9 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

I. – [L’article 22](/loi-78-17/chapitre-iv/article-22) de la loi n° 78-17 du 6 janvier 1978 précitée est ainsi rédigé :

« [Art. 22.](/loi-78-17/chapitre-iv/article-22) ***Un décret en Conseil d’État, pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés**, détermine les catégories de responsables de traitement et les finalités de ces traitements au vu desquelles ces derniers peuvent être mis en œuvre lorsqu’ils portent sur des données comportant le numéro d’inscription des personnes au répertoire national d’identification des personnes physiques. La mise en œuvre des traitements intervient sans préjudice des obligations qui incombent aux responsables de traitement ou à leurs sous-traitants en application de la [section 3 du chapitre IV du règlement (UE) 2016/679](/reglement-2016-679/chapitre-iv/section-3) du Parlement européen et du Conseil du 27 avril 2016 précité.*

« N’entrent pas dans le champ d’application du premier alinéa du présent article ceux des traitements portant sur des données à caractère personnel parmi lesquelles figure le numéro d’inscription des personnes au répertoire national d’identification des personnes physiques ou qui requièrent une consultation de ce répertoire :

« 1° Qui ont exclusivement des finalités de statistique publique, sont mis en œuvre par le service statistique public et ne comportent aucune des données mentionnées au [I de l’article 8](/loi-78-17/chapitre-ii/section-2/article-8) ou à [l’article 9](/loi-78-17/chapitre-ii/section-2/article-9) ;

« 2° Qui ont exclusivement des finalités de recherche scientifique ou historique ;

« 3° Qui ont pour objet de mettre à la disposition des usagers de l’administration un ou plusieurs téléservices de l’administration électronique définis à [l’article 1er de l’ordonnance n° 2005-1516 du 8 décembre 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives, mis en œuvre par l’État ou une personne morale de droit public ou une personne morale de droit privé gérant un service public.

« Pour les traitements dont les finalités sont mentionnées aux 1° et 2° du présent article, le numéro d’inscription au répertoire national d’identification des personnes physiques fait préalablement l’objet d’une opération cryptographique lui substituant un code statistique non signifiant. Cette opération est renouvelée à une fréquence définie par décret en Conseil d’État, pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés. Les traitements ayant comme finalité exclusive de réaliser cette opération cryptographique ne sont pas soumis aux dispositions du premier alinéa.

« Pour les traitements dont les finalités sont mentionnées au 1°, l’utilisation du code statistique non signifiant n’est autorisée qu’au sein du service statistique public.

« Pour les traitements dont les finalités sont mentionnées au 2°, l’opération cryptographique et, le cas échéant, l’interconnexion de deux fichiers par l’utilisation du code spécifique non signifiant qui en est issu ne peuvent être assurées par la même personne ni par le responsable de traitement.

« À l’exception des traitements mentionnés au [deuxième alinéa de l’article 55](/loi-78-17-pjl-490/chapitre-ix/section-1/article-55), le présent article n’est pas applicable aux traitements de données à caractère personnel dans le domaine de la santé qui sont régis par le chapitre IX. »

II. – L’[article 27](/loi-78-17/chapitre-iv/section-2/article-27) de la loi n° 78-17 du 6 janvier 1978 précitée est ainsi rédigé :

« _Art. 27_. – Sont autorisés par décret en Conseil d’État, pris après avis motivé et publié de la Commission nationale de l’informatique et des libertés, les traitements de données à caractère personnel mis en  œuvre pour le compte de l’État, agissant dans l’exercice de ses prérogatives de puissance publique, qui portent sur des données génétiques ou sur des données biométriques nécessaires à l’authentification ou au contrôle de l’identité des personnes. »

III. – Les articles [23](/loi-78-17/chapitre-iv/section-1/article-23) à [25](/loi-78-17/chapitre-iv/section-2/article-25) de la loi n° 78-17 du 6 janvier 1978 précitée sont abrogés.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Amendements en commission des lois de l'Assemblée (première lecture)
  * [Amendement n° 66 — Rejeté](/amendements-1-assemblee-hemicycle/article-9/amendement-66)
  * [Amendement n° 81 — Rejeté](/amendements-1-assemblee-hemicycle/article-9/amendement-81)
  * [Amendement n° 147 — Rejeté](/amendements-1-assemblee-hemicycle/article-9/amendement-147)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/pjl-592/titre-ii/chapitre-ii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Projet de loi, version issue de la 1ère lecture en Commission à l'Assemblée</span> <a href="/pjl-592/titre-ii/chapitre-iii" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

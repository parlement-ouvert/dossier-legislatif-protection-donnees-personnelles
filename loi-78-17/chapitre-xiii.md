<!-- TITLE: Chapitre XIII -->
<!-- SUBTITLE: Dispositions diverses. -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17/chapitre-xii/article-70" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés</span> <a href="/loi-78-17/chapitre-xiii/article-71" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 71](/loi-78-17/chapitre-xiii/article-71)
* [Article 72](/loi-78-17/chapitre-xiii/article-72)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [8.](/avis-conseil-etat-393836/choix-legistiques-et-propositions-pour-une-meilleure-lisibilite-du-droit/8)
  * [40.](/avis-conseil-etat-393836/en-ce-qui-concerne-le-titre-iii-relatif-a-la-transposition-de-la-directive-ue-2016-680/40)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLES 16 ET 17 — MODALITES D’EXERCICE DES VOIES DE RECOURS](/etude-impact-490/titre-ii/chapitre-v/articles-16-et-17)
  * [ARTICLES 18 ET 19 — PRESENTATION GENERALE ET DÉFINITIONS](/etude-impact-490/titre-iii/articles-18-et-19)
  * [ARTICLE 18 ET ARTICLE 19 SECTION 3 — DROITS DE LA PERSONNE CONCERNÉE](/etude-impact-490/titre-iii/article-18-et-article-19-section-3)
  * [ARTICLE 19 SECTION 4 — TRANSFERTS INTERNATIONAUX](/etude-impact-490/titre-iii/article-19-section-4)
* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 32](/loi-78-17-pjl-592/chapitre-v/section-1/article-32)
  * [Article 41](/loi-78-17-pjl-592/chapitre-v/section-2/article-41)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 6](/pjl-490/titre-i/chapitre-i/article-6)
  * [Article 16](/pjl-490/titre-ii/chapitre-vi/article-16)
  * [Article 18](/pjl-490/titre-iii/article-18)
  * [Article 19](/pjl-490/titre-iii/article-19)
* N° 592 annexe 0 - Rapport sur le projet de loi, après engagement de la procédure accélérée, relatif à la protection des données personnelles (n°490).
  * [Article 6](/pjl-592/titre-i/chapitre-i/article-6)
  * [Article 16](/pjl-592/titre-ii/chapitre-vi/article-16)
  * [Article 18](/pjl-592/titre-iii/article-18)
  * [Article 19](/pjl-592/titre-iii/article-19)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17/chapitre-xii/article-70" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés</span> <a href="/loi-78-17/chapitre-xiii/article-71" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

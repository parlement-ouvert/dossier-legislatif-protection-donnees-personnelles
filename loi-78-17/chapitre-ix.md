<!-- TITLE: Chapitre IX -->
<!-- SUBTITLE: Traitements de données à caractère personnel  à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé. -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17/chapitre-viii/article-52" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés</span> <a href="/loi-78-17/chapitre-ix/article-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 53](/loi-78-17/chapitre-ix/article-53)
* [Article 54](/loi-78-17/chapitre-ix/article-54)
* [Article 55](/loi-78-17/chapitre-ix/article-55)
* [Article 56](/loi-78-17/chapitre-ix/article-56)
* [Article 57](/loi-78-17/chapitre-ix/article-57)
* [Article 58](/loi-78-17/chapitre-ix/article-58)
* [Article 59](/loi-78-17/chapitre-ix/article-59)
* [Article 60](/loi-78-17/chapitre-ix/article-60)
* [Article 61](/loi-78-17/chapitre-ix/article-61)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [23.](/avis-conseil-etat-393836/formalites-prealables/23)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 13 — TRAITEMENTS DE DONNEES DE SANTE](/etude-impact-490/titre-ii/chapitre-iv/article-13)
  * [ARTICLE 19 SECTION 1 — DISPOSITIONS GENERALES](/etude-impact-490/titre-iii/article-19-section-1)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)
  * [Article 1er](/pjl-490/titre-i/chapitre-i/article-1)
  * [Article 7](/pjl-490/titre-i/chapitre-ii/article-7)
  * [Article 9](/pjl-490/titre-ii/chapitre-ii/article-9)
* N° 592 annexe 0 - Rapport sur le projet de loi, après engagement de la procédure accélérée, relatif à la protection des données personnelles (n°490).
  * [Article 7](/pjl-592/titre-i/chapitre-ii/article-7)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17/chapitre-viii/article-52" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés</span> <a href="/loi-78-17/chapitre-ix/article-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

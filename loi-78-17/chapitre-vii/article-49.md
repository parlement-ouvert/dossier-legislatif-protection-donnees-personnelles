<!-- TITLE: Article 49 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17/chapitre-vii/article-48" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés</span> <a href="/loi-78-17/chapitre-vii/article-49-bis" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2011-334
 du 29 mars 2011 - art. 8](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782616&dateTexte=20110330&categorieLien=id#LEGIARTI000023782616)

----

La commission peut, à la demande d'une autorité exerçant des compétences analogues aux siennes dans un autre Etat membre de l'Union européenne, procéder à des vérifications dans les mêmes conditions que celles prévues à l'article 44, sauf s'il s'agit d'un traitement mentionné aux I ou II de l'article 26.

Le président de la commission ou la formation restreinte peuvent, à la demande d'une autorité exerçant des compétences analogues aux leurs dans un autre Etat membre de l'Union européenne, prendre les décisions mentionnées aux articles 45 à 47 et dans les conditions prévues par ces mêmes articles, sauf s'il s'agit d'un traitement mentionné aux I ou II de l'article 26.

La commission est habilitée à communiquer les informations qu'elle recueille ou qu'elle détient, à leur demande, aux autorités exerçant des compétences analogues aux siennes dans d'autres Etats membres de la Communauté européenne.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 5 — PROCÉDURE DE COOPÉRATION DE LA CNIL AVEC LES AUTRES AUTORITÉS DE CONTRÔLE](/etude-impact-490/titre-ier/chapitre-ier/article-5)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 5](/pjl-490/titre-i/chapitre-i/article-5)
* N° 592 annexe 0 - Rapport sur le projet de loi, après engagement de la procédure accélérée, relatif à la protection des données personnelles (n°490).
  * [Article 5](/pjl-592/titre-i/chapitre-i/article-5)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17/chapitre-vii/article-48" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés</span> <a href="/loi-78-17/chapitre-vii/article-49-bis" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

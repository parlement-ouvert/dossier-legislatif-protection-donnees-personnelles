<!-- TITLE: Chapitre II -->
<!-- SUBTITLE: Conditions de licéité des traitements de données à caractère personnel -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17/chapitre-ier/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés</span> <a href="/loi-78-17/chapitre-ii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Section 1 — Dispositions générales](/loi-78-17/chapitre-ii/section-1)

  * [Article 6](/loi-78-17/chapitre-ii/section-1/article-6)
  * [Article 7](/loi-78-17/chapitre-ii/section-1/article-7)

* [Section 2 — Dispositions propres à certaines catégories de données](/loi-78-17/chapitre-ii/section-2)

  * [Article 8](/loi-78-17/chapitre-ii/section-2/article-8)
  * [Article 9](/loi-78-17/chapitre-ii/section-2/article-9)
  * [Article 10](/loi-78-17/chapitre-ii/section-2/article-10)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17/chapitre-ier/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés</span> <a href="/loi-78-17/chapitre-ii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 54 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17/chapitre-ix/article-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés</span> <a href="/loi-78-17/chapitre-ix/article-55" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

I.-Les traitements de données à caractère personnel ayant une finalité d'intérêt public de recherche, d'étude ou d'évaluation dans le domaine de la santé sont autorisés par la Commission nationale de l'informatique et des libertés, dans le respect des principes définis par la présente loi et en fonction de l'intérêt public que la recherche, l'étude ou l'évaluation présente.

II.-La Commission nationale de l'informatique et des libertés prend sa décision après avis :

1° Du comité compétent de protection des personnes mentionné à l' article L. 1123-6 du code de la santé publique , pour les demandes d'autorisation relatives aux recherches impliquant la personne humaine mentionnées à l'article L. 1121-1 du même code ;

2° Du comité d'expertise pour les recherches, les études et les évaluations dans le domaine de la santé, pour les demandes d'autorisation relatives à des études ou à des évaluations ainsi qu'à des recherches n'impliquant pas la personne humaine, au sens du 1° du présent II.

Le comité d'expertise est composé de personnes choisies en raison de leur compétence, dans une pluralité de disciplines. Un décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, précise la composition du comité et définit ses règles de fonctionnement. Il peut prévoir l'existence de plusieurs sections au sein du comité, compétentes en fonction de la nature ou de la finalité du traitement. Le comité d'expertise est soumis à l' article L. 1451-1 du code de la santé publique .

Le comité d'expertise émet, dans un délai d'un mois à compter de sa saisine, un avis sur la méthodologie retenue, sur la nécessité du recours à des données à caractère personnel, sur la pertinence de celles-ci par rapport à la finalité du traitement et, s'il y a lieu, sur la qualité scientifique du projet. Le cas échéant, le comité recommande aux demandeurs des modifications de leur projet afin de le mettre en conformité avec les obligations prévues par la présente loi. A défaut d'avis du comité dans le délai d'un mois, l'avis est réputé favorable. En cas d'urgence, ce délai peut être ramené à quinze jours.

Dans des conditions définies par décret en Conseil d'Etat, l'Institut national des données de santé, prévu à l' article L. 1462-1 du code de la santé publique , peut être saisi par la Commission nationale de l'informatique et des libertés ou le ministre chargé de la santé sur le caractère d'intérêt public que présente la recherche, l'étude ou l'évaluation justifiant la demande de traitement ; il peut également évoquer le cas de sa propre initiative. Dans tous les cas, il rend un avis dans un délai d'un mois à compter de sa saisine.

Les dossiers présentés dans le cadre du présent chapitre, à l'exclusion des recherches mentionnées aux 1° et 2° de l'article L. 1121-1 du code de la santé publique et de celles mentionnées au 3° du même article L. 1121-1 portant sur des produits mentionnés à l'article L. 5311-1 du même code, sont déposés auprès d'un secrétariat unique, qui assure leur orientation vers les instances compétentes .

III.-Pour chaque demande, la Commission nationale de l'informatique et des libertés vérifie les garanties présentées par le demandeur pour l'application des présentes dispositions et la conformité de sa demande à ses missions ou à son objet social. Si le demandeur n'apporte pas d'éléments suffisants pour attester la nécessité de disposer de certaines informations parmi l'ensemble des données à caractère personnel dont le traitement est envisagé, la commission peut interdire la communication de ces informations par l'organisme qui les détient et n'autoriser le traitement que pour ces données réduites.

La commission statue sur la durée de conservation des données nécessaires au traitement et apprécie les dispositions prises pour assurer leur sécurité et la garantie des secrets protégés par la loi.

IV.-Pour les catégories les plus usuelles de traitements automatisés de données de santé à caractère personnel à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé, la Commission nationale de l'informatique et des libertés peut homologuer et publier des méthodologies de référence destinées à simplifier la procédure d'examen. Celles-ci sont établies en concertation avec le comité d'expertise et des organismes publics et privés représentatifs des acteurs concernés.

V.-Des jeux de données agrégées ou des échantillons, issus des traitements des données de santé à caractère personnel pour des finalités et dans des conditions reconnues conformes à la présente loi par la Commission nationale de l'informatique et des libertés, peuvent faire l'objet d'une mise à disposition, dans des conditions préalablement homologuées par la commission, sans que l'autorisation prévue au I du présent article soit requise.

VI.-La commission peut, par décision unique, délivrer à un même demandeur une autorisation pour des traitements répondant à une même finalité, portant sur des catégories de données identiques et ayant des catégories de destinataires identiques.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [ARTICLE 13 — TRAITEMENTS DE DONNEES DE SANTE](/etude-impact-490/titre-ii/chapitre-iv/article-13)
* Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés Révision PJL-592
  * [Article 55](/loi-78-17-pjl-592/chapitre-ix/section-1/article-55)
  * [Article 56](/loi-78-17-pjl-592/chapitre-ix/section-1/article-56)
  * [Article 62](/loi-78-17-pjl-592/chapitre-ix/section-2/article-62)
  * [Article 63](/loi-78-17-pjl-592/chapitre-ix/section-2/article-63)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 6](/pjl-490/titre-i/chapitre-i/article-6)
  * [Article 13](/pjl-490/titre-ii/chapitre-iv/article-13)
* N° 592 annexe 0 - Rapport sur le projet de loi, après engagement de la procédure accélérée, relatif à la protection des données personnelles (n°490).
  * [Article 13](/pjl-592/titre-ii/chapitre-iv/article-13)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17/chapitre-ix/article-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés</span> <a href="/loi-78-17/chapitre-ix/article-55" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

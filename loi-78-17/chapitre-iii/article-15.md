<!-- TITLE: Article 15 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17/chapitre-iii/article-14-abroge" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés</span> <a href="/loi-78-17/chapitre-iii/article-15-bis" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* Modifié par [LOI n°2016-41 du 26 janvier 2016 - art. 193](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)

----

Sous réserve des compétences du bureau et de la formation restreinte, la commission se réunit en formation plénière.

En cas de partage égal des voix, la voix du président est prépondérante.

La commission peut charger le président ou le vice-président délégué d'exercer celles de ses attributions mentionnées :

\- au troisième alinéa du I de l'article 23 ;

\- aux e et f du 2° de l'article 11 ;

\- au c du 2° de l'article 11 ;

\- au d du 4° de l'article 11 ;

\- aux articles 41 et 42 ;

\- à l'article 54 ;

\- aux deux derniers alinéas de l'article 69, à l'exception des traitements mentionnés aux I ou II de l'article 26 ;

\- au premier alinéa de l'article 70.

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Article 21](/pjl-490/titre-v/article-21)
* N° 592 annexe 0 - Rapport sur le projet de loi, après engagement de la procédure accélérée, relatif à la protection des données personnelles (n°490).
  * [Article 2 bis (nouveau)](/pjl-592/titre-i/chapitre-i/article-2-bis-nouveau)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-78-17/chapitre-iii/article-14-abroge" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi informatique et libertés</span> <a href="/loi-78-17/chapitre-iii/article-15-bis" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Section 3 -->
<!-- SUBTITLE: Loyauté des plateformes et information des consommateurs -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-ii/chapitre-ier/section-2/article-48" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-ii/chapitre-ier/section-3/article-49" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 49](/loi-2016-1321/titre-ii/chapitre-ier/section-3/article-49)
* [Article 50](/loi-2016-1321/titre-ii/chapitre-ier/section-3/article-50)
* [Article 51](/loi-2016-1321/titre-ii/chapitre-ier/section-3/article-51)
* [Article 52](/loi-2016-1321/titre-ii/chapitre-ier/section-3/article-52)
* [Article 53](/loi-2016-1321/titre-ii/chapitre-ier/section-3/article-53)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-ii/chapitre-ier/section-2/article-48" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-ii/chapitre-ier/section-3/article-49" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

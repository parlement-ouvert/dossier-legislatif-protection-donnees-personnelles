<!-- TITLE: Chapitre II -->
<!-- SUBTITLE: Protection de la vie privée en ligne -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-ii/chapitre-ier/section-3/article-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-ii/chapitre-ii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Section 1 — Protection des données à caractère personnel](/loi-2016-1321/titre-ii/chapitre-ii/section-1)

  * [Article 54](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-54)
  * [Article 55](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-55)
  * [Article 56](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-56)
  * [Article 57](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-57)
  * [Article 58](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-58)
  * [Article 59](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-59)
  * [Article 60](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-60)
  * [Article 61](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-61)
  * [Article 62](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-62)
  * [Article 63](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-63)
  * [Article 64](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-64)
  * [Article 65](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-65)
  * [Article 66](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-66)
  * [Article 67](/loi-2016-1321/titre-ii/chapitre-ii/section-1/article-67)

* [Section 2 — Confidentialité des correspondances électroniques privées](/loi-2016-1321/titre-ii/chapitre-ii/section-2)

  * [Article 68](/loi-2016-1321/titre-ii/chapitre-ii/section-2/article-68)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-ii/chapitre-ier/section-3/article-53" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-ii/chapitre-ii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

<!-- TITLE: Article 1 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-ier/chapitre-ier/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

----

I. - Sous réserve des articles [L. 311-5](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367708&dateTexte=&categorieLien=cid) et [L. 311-6](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367716&dateTexte=&categorieLien=cid) du code des relations entre le public et l'administration et sans préjudice de l'article L. 114-8 du même code, les administrations mentionnées au premier alinéa de l'article L. 300-2 dudit code sont tenues de communiquer, dans le respect de la [loi n° 78-17 du 6 janvier 1978](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460&categorieLien=cid) relative à l'informatique, aux fichiers et aux libertés, les documents administratifs qu'elles détiennent aux autres administrations mentionnées au même premier alinéa de l'article L. 300-2 qui en font la demande pour l'accomplissement de leurs missions de service public.

Les informations figurant dans des documents administratifs communiqués ou publiés peuvent être utilisées par toute administration mentionnée audit premier alinéa de l'article L. 300-2 qui le souhaite à des fins d'accomplissement de missions de service public autres que celle pour les besoins de laquelle les documents ont été produits ou reçus.

A compter du 1er janvier 2017, l'échange d'informations publiques entre les administrations de l'Etat, entre les administrations de l'Etat et ses établissements publics administratifs et entre les établissements publics précités, aux fins de l'exercice de leurs missions de service public, ne peut donner lieu au versement d'une redevance.

II. - A modifié les dispositions suivantes :

> \- Code des relations entre le public et l'administration
>
>> [Art. L342-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218880&dateTexte=&categorieLien=id)

III. - Le titre Ier du livre III du code des relations entre le public et l'administration est applicable aux demandes de communication des documents administratifs exercées en application du I du présent article.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-ier/chapitre-ier/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

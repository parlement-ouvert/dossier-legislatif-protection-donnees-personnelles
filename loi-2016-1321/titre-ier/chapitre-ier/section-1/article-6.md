<!-- TITLE: Article 6 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

----

I.-A modifié les dispositions suivantes :

> -Code des relations entre le public et l'administration
>
>> Art. L311-6

II.-A créé les dispositions suivantes :

> -Code des relations entre le public et l'administration
>
>> [Art. L312-1-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205512&dateTexte=&categorieLien=id), [Art. L312-1-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205514&dateTexte=&categorieLien=id), [Art. L312-1-3 ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205516&dateTexte=&categorieLien=id)

III.-Un décret en Conseil d'Etat, pris après avis de la commission mentionnée à l'article L. 340-1 du code des relations entre le public et l'administration, définit les modalités d'application des articles L. 312-1 à L. 312-1-3 du même code

IV.-A abrogé les dispositions suivantes :

> -Code général des collectivités territoriales
>
>> [Art. L1112-23 ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000032273137&dateTexte=&categorieLien=id)

A modifié les dispositions suivantes :

> -Code général des collectivités territoriales
>
>> [Art. L1821-1 ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033218977&dateTexte=&categorieLien=id)

> Art. L1453-1

V.-A abrogé les dispositions suivantes :

> -Code des communes de la Nouvelle-Calédonie
>
>> [Section 3 : Transparence des données des communes ](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006070300&idSectionTA=LEGISCTA000031022040&dateTexte=&categorieLien=cid) [Art. L125-12](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070300&idArticle=LEGIARTI000032266982&dateTexte=&categorieLien=id)

VI.-A modifié les dispositions suivantes :

> -Code des relations entre le public et l'administration
>
>> [Art. L321-2 ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218992&dateTexte=&categorieLien=id)

VII.-A modifié les dispositions suivantes :

> -Code des relations entre le public et l'administration
>
>> Art. L322-2

VIII.-A modifié les dispositions suivantes :

> -Code de la santé publique
>
>> [Art. L1453-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000033219005&dateTexte=&categorieLien=id)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-5" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-7" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

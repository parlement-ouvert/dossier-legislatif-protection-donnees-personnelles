<!-- TITLE: Chapitre II -->
<!-- SUBTITLE: Economie du savoir -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-ier/chapitre-ier/section-3/article-29" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-ier/chapitre-ii/article-30" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 30](/loi-2016-1321/titre-ier/chapitre-ii/article-30)
* [Article 31](/loi-2016-1321/titre-ier/chapitre-ii/article-31)
* [Article 32](/loi-2016-1321/titre-ier/chapitre-ii/article-32)
* [Article 33](/loi-2016-1321/titre-ier/chapitre-ii/article-33)
* [Article 34](/loi-2016-1321/titre-ier/chapitre-ii/article-34)
* [Article 35](/loi-2016-1321/titre-ier/chapitre-ii/article-35)
* [Article 36](/loi-2016-1321/titre-ier/chapitre-ii/article-36)
* [Article 37](/loi-2016-1321/titre-ier/chapitre-ii/article-37)
* [Article 38](/loi-2016-1321/titre-ier/chapitre-ii/article-38)
* [Article 39](/loi-2016-1321/titre-ier/chapitre-ii/article-39)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-ier/chapitre-ier/section-3/article-29" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-ier/chapitre-ii/article-30" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

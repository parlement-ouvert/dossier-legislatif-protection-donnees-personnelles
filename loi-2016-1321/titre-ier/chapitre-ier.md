<!-- TITLE: Chapitre Ier -->
<!-- SUBTITLE: Economie de la donnée -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-ier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-ier/chapitre-ier/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Section 1 — Ouverture de l'accès aux données publiques](/loi-2016-1321/titre-ier/chapitre-ier/section-1)

  * [Article 1](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-1)
  * [Article 2](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-2)
  * [Article 3](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-3)
  * [Article 4](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-4)
  * [Article 5](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-5)
  * [Article 6](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-6)
  * [Article 7](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-7)
  * [Article 8](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-8)
  * [Article 9](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-9)
  * [Article 10](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-10)
  * [Article 11](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-11)
  * [Article 12](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-12)
  * [Article 13](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-13)
  * [Article 14](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-14)
  * [Article 15](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-15)
  * [Article 16](/loi-2016-1321/titre-ier/chapitre-ier/section-1/article-16)

* [Section 2 — Données d'intérêt général](/loi-2016-1321/titre-ier/chapitre-ier/section-2)

  * [Article 17](/loi-2016-1321/titre-ier/chapitre-ier/section-2/article-17)
  * [Article 18](/loi-2016-1321/titre-ier/chapitre-ier/section-2/article-18)
  * [Article 19](/loi-2016-1321/titre-ier/chapitre-ier/section-2/article-19)
  * [Article 20](/loi-2016-1321/titre-ier/chapitre-ier/section-2/article-20)
  * [Article 21](/loi-2016-1321/titre-ier/chapitre-ier/section-2/article-21)
  * [Article 22](/loi-2016-1321/titre-ier/chapitre-ier/section-2/article-22)
  * [Article 23](/loi-2016-1321/titre-ier/chapitre-ier/section-2/article-23)
  * [Article 24](/loi-2016-1321/titre-ier/chapitre-ier/section-2/article-24)

* [Section 3 — Gouvernance](/loi-2016-1321/titre-ier/chapitre-ier/section-3)

  * [Article 25](/loi-2016-1321/titre-ier/chapitre-ier/section-3/article-25)
  * [Article 26](/loi-2016-1321/titre-ier/chapitre-ier/section-3/article-26)
  * [Article 27](/loi-2016-1321/titre-ier/chapitre-ier/section-3/article-27)
  * [Article 28](/loi-2016-1321/titre-ier/chapitre-ier/section-3/article-28)
  * [Article 29](/loi-2016-1321/titre-ier/chapitre-ier/section-3/article-29)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-ier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-ier/chapitre-ier/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

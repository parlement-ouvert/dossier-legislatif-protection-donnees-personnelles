<!-- TITLE: Article 105 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-iii/chapitre-iii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-iii/chapitre-iii/section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

----

I à III. - A modifié les dispositions suivantes :

> \- Code des postes et des communications électroniques
>
>> [Art. L33-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219394&dateTexte=&categorieLien=id)

> \- Loi n° 2005-102 du 11 février 2005
>
>> [Art. 78](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000809647&idArticle=LEGIARTI000033220166&dateTexte=&categorieLien=id)

A créé les dispositions suivantes :

> \- Code de la consommation
>
>> [Art. L112-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033216993&dateTexte=&categorieLien=id)

IV. - La mise en œuvre du p du I de l'article L. 33-1 du code des postes et des communications électroniques, de l'article 78 de la loi n° 2005-102 du 11 février 2005 pour l'égalité des droits et des chances, la participation et la citoyenneté des personnes handicapées et de l'article L. 112-8 du code de la consommation s'appuie notamment sur la création d'un groupement interprofessionnel comportant notamment des opérateurs de communications électroniques, dont l'objet est d'assurer l'organisation, le fonctionnement et la gestion de services d'accessibilité téléphonique grâce à une mutualisation des coûts, selon des modalités définies par le décret mentionné au VII du présent article et sous le contrôle de l'Autorité de régulation des communications électroniques et des postes.

Les services de traduction mentionnés au p du I de l'article L. 33-1 du code des postes et des communications électroniques, à l'article 78 de la loi n° 2005-102 du 11 février 2005 précitée et à l'[article L. 112-8 du code de la consommation ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000006291881&dateTexte=&categorieLien=cid)assurent, en mode simultané et à la demande de l'utilisateur, l'interprétariat entre le français et la langue des signes française, la transcription écrite et le codage en langage parlé complété.

L'accessibilité des services d'accueil mentionnés à l'[article 78 de la loi n° 2005-102 du 11 février 2005 précitée ](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000809647&idArticle=LEGIARTI000006682314&dateTexte=&categorieLien=cid)et à l'article L. 112-8 du code de la consommation peut être réalisée directement par des téléconseillers professionnels maitrisant la langue des signes française, la transcription écrite ou le codage en langage parlé complété et dont les diplômes et qualifications sont précisés par le décret mentionné au VII du présent article.

V. - Au plus tard dix ans après la promulgation de la présente loi, et selon des modalités définies par le décret prévu au VII, le service de traduction mentionné au p du [I de l'article L. 33-1 du code des postes et des communications électroniques ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000006465743&dateTexte=&categorieLien=cid)fonctionne vingt-quatre heures sur vingt-quatre, tous les jours de l'année, le service de traduction mentionné à l'article 78 de la loi n° 2005-102 du 11 février 2005 précitée fonctionne aux horaires d'ouverture des services d'accueil téléphonique concerné et le service de traduction mentionné à l'article L. 112-8 du code de la consommation fonctionne aux horaires d'ouverture des services clients.

VI. - La mise en œuvre du p du I de l'article L. 33-1 du code des postes et des communications électroniques, de l'article 78 de la loi n° 2005-102 du 11 février 2005 précitée et de l'article L. 112-8 du code de la consommation peut s'appuyer sur des applications de communications électroniques permettant la vocalisation du texte, la transcription de la voix en texte, la traduction en et depuis la langue des signes française ou la transcription en et depuis le langage parlé complété. Cette mise en œuvre ne peut se substituer au service de traduction simultanée écrite et visuelle mentionné au p du I de l'article L. 33-1 du code des postes et des communications électroniques, à l'article 78 de la loi n° 2005-102 du 11 février 2005 précitée et à l'article L. 112-8 du code de la consommation qu'à la condition de garantir une accessibilité de qualité équivalente et d'offrir les mêmes conditions de traduction aux personnes sourdes, malentendantes, sourdaveugles et aphasiques.

VII. - Les I et II entrent en vigueur selon des modalités et à une date prévues par décret et, au plus tard, cinq ans après la promulgation de la présente loi. Le III entre en vigueur à une date fixée par décret et, au plus tard, deux ans après la promulgation de la présente loi. Ce décret précise également les modalités de suivi de l'application du présent article et les diplômes et qualifications requis pour les professionnels intervenant sur l'accessibilité simultanée des appels.

VIII. - Dans un délai de six mois à compter de la promulgation de la présente loi, le Gouvernement présente un plan des métiers visant à développer les formations conduisant aux professions spécialisées nécessaires à la mise en œuvre du présent article.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-iii/chapitre-iii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-iii/chapitre-iii/section-2" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

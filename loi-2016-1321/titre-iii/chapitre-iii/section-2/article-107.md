<!-- TITLE: Article 107 -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-iii/chapitre-iii/section-2/article-106" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-iii/chapitre-iii/section-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

----

I. à VIII.-A modifié les dispositions suivantes :

> -Code général des impôts, CGI.
>
>> [Art. 168](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220379&dateTexte=&categorieLien=id), [Art. 195](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220374&dateTexte=&categorieLien=id), [Art. 196 A bis](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220369&dateTexte=&categorieLien=id), [Art. 1011 bis](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220362&dateTexte=&categorieLien=id), [Art. 1011 ter](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220358&dateTexte=&categorieLien=id), [Art. 1411 ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220348&dateTexte=&categorieLien=id)

A modifié les dispositions suivantes :

> -Loi n° 87-588 du 30 juillet 1987
>
>> [Art. 88 ](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000687467&idArticle=LEGIARTI000033220273&dateTexte=&categorieLien=id)

A modifié les dispositions suivantes :

> -Code de la santé publique
>
>> [Art. L4321-3](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000033220313&dateTexte=&categorieLien=id)

> -Code des transports
>
>> [Art. L1112-8 ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000023086525&idArticle=LEGIARTI000033220309&dateTexte=&categorieLien=id)

A modifié les dispositions suivantes :

> -Code du travail applicable à Mayotte.
>
>> [Art. L328-18 ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072052&idArticle=LEGIARTI000033220327&dateTexte=&categorieLien=id)

A modifié les dispositions suivantes :

> -Code général des collectivités territoriales
>
>> [Art. L2213-2 ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033220276&dateTexte=&categorieLien=id)

A modifié les dispositions suivantes :

> -Code général des impôts, CGI.
>
>> [Art. 150 U](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220293&dateTexte=&categorieLien=id), [Art. 244 quater J ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220279&dateTexte=&categorieLien=id)

A modifié les dispositions suivantes :

> -Code du travail
>
>> [Art. L5212-13 ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000033220318&dateTexte=&categorieLien=id)

A modifié les dispositions suivantes :

> -Code de l'action sociale et des familles
>
>> [Art. L146-3](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220258&dateTexte=&categorieLien=id), [Art. L146-4](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220253&dateTexte=&categorieLien=id), [Art. L241-3](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220240&dateTexte=&categorieLien=id), [Art. L241-6](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000031928935&dateTexte=&categorieLien=id), [Art. L542-4 ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220202&dateTexte=&categorieLien=id)

A abrogé les dispositions suivantes :

> -Code de l'action sociale et des familles
>
>> [Art. L241-3-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000006797045&dateTexte=&categorieLien=id), [Art. L241-3-2 ](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000031727805&dateTexte=&categorieLien=id)

IX.-Les cartes d'invalidité, de priorité et de stationnement délivrées en application des articles L. 241-3 à L. 241-3-2 du code de l'action sociale et des familles, dans leur rédaction antérieure à la présente loi, demeurent valables jusqu'à leur date d'expiration et, au plus tard, jusqu'au 31 décembre 2026. Les titulaires de ces cartes peuvent demander une carte “ mobilité inclusion ” avant cette date. Cette carte se substitue aux cartes délivrées antérieurement.

X.-Le présent article entre en vigueur le 1er janvier 2017. A titre transitoire, les cartes d'invalidité, de priorité et de stationnement peuvent être délivrées, en tant que de besoin, jusqu'au 1er juillet 2017. Les articles L. 241-3 à L. 241-3-2 du code de l'action sociale et des familles, dans leur rédaction antérieure à la présente loi, restent applicables aux cas mentionnés à la deuxième phrase du présent alinéa jusqu'à cette même date.

Les demandes de carte en cours d'instruction à la date d'entrée en vigueur du présent article donnent lieu à la délivrance de la carte “ mobilité inclusion ” dès lors que les conditions en sont remplies.

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-iii/chapitre-iii/section-2/article-106" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-iii/chapitre-iii/section-3" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

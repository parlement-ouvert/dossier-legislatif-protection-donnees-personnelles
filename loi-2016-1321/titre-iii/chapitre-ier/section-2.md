<!-- TITLE: Section 2 -->
<!-- SUBTITLE: Couverture numérique -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-iii/chapitre-ier/section-1/article-71" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-72" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 72](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-72)
* [Article 73](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-73)
* [Article 74](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-74)
* [Article 75](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-75)
* [Article 76](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-76)
* [Article 77](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-77)
* [Article 78](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-78)
* [Article 79](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-79)
* [Article 80](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-80)
* [Article 81](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-81)
* [Article 82](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-82)
* [Article 83](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-83)
* [Article 84](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-84)
* [Article 85](/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-85)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-iii/chapitre-ier/section-1/article-71" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-72" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

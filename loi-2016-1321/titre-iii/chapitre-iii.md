<!-- TITLE: Chapitre III -->
<!-- SUBTITLE: Accès des publics fragiles au numérique -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-iii/chapitre-ii/section-5/article-104" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-iii/chapitre-iii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Section 1 — Accès des personnes handicapées aux services téléphoniques](/loi-2016-1321/titre-iii/chapitre-iii/section-1)

  * [Article 105](/loi-2016-1321/titre-iii/chapitre-iii/section-1/article-105)

* [Section 2 — Accès des personnes handicapées aux sites internet publics](/loi-2016-1321/titre-iii/chapitre-iii/section-2)

  * [Article 106](/loi-2016-1321/titre-iii/chapitre-iii/section-2/article-106)
  * [Article 107](/loi-2016-1321/titre-iii/chapitre-iii/section-2/article-107)

* [Section 3 — Maintien de la connexion à internet](/loi-2016-1321/titre-iii/chapitre-iii/section-3)

  * [Article 108](/loi-2016-1321/titre-iii/chapitre-iii/section-3/article-108)
  * [Article 109](/loi-2016-1321/titre-iii/chapitre-iii/section-3/article-109)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-iii/chapitre-ii/section-5/article-104" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-iii/chapitre-iii/section-1" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

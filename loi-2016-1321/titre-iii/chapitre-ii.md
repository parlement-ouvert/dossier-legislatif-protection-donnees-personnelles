<!-- TITLE: Chapitre II -->
<!-- SUBTITLE: Facilitation des usages -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-85" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-iii/chapitre-ii/article-86" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Article 86](/loi-2016-1321/titre-iii/chapitre-ii/article-86)
* [Article 87](/loi-2016-1321/titre-iii/chapitre-ii/article-87)
* [Article 88](/loi-2016-1321/titre-iii/chapitre-ii/article-88)
* [Article 89](/loi-2016-1321/titre-iii/chapitre-ii/article-89)
* [Article 90](/loi-2016-1321/titre-iii/chapitre-ii/article-90)
* [Article 91](/loi-2016-1321/titre-iii/chapitre-ii/article-91)
* [Article 92](/loi-2016-1321/titre-iii/chapitre-ii/article-92)

* [Section 1 — Recommandé électronique](/loi-2016-1321/titre-iii/chapitre-ii/section-1)

  * [Article 93](/loi-2016-1321/titre-iii/chapitre-ii/section-1/article-93)

* [Section 2 — Fourniture de services de paiement dans le cadre de l'exclusion de demande d'agrément applicable à certains instruments de paiement](/loi-2016-1321/titre-iii/chapitre-ii/section-2)

  * [Article 94](/loi-2016-1321/titre-iii/chapitre-ii/section-2/article-94)

* [Section 3 — Régulation des jeux en ligne](/loi-2016-1321/titre-iii/chapitre-ii/section-3)

  * [Article 95](/loi-2016-1321/titre-iii/chapitre-ii/section-3/article-95)
  * [Article 96](/loi-2016-1321/titre-iii/chapitre-ii/section-3/article-96)
  * [Article 97](/loi-2016-1321/titre-iii/chapitre-ii/section-3/article-97)
  * [Article 98](/loi-2016-1321/titre-iii/chapitre-ii/section-3/article-98)
  * [Article 99](/loi-2016-1321/titre-iii/chapitre-ii/section-3/article-99)
  * [Article 100](/loi-2016-1321/titre-iii/chapitre-ii/section-3/article-100)

* [Section 4 — Compétitions de jeux vidéo](/loi-2016-1321/titre-iii/chapitre-ii/section-4)

  * [Article 101](/loi-2016-1321/titre-iii/chapitre-ii/section-4/article-101)
  * [Article 102](/loi-2016-1321/titre-iii/chapitre-ii/section-4/article-102)

* [Section 5 — Simplification des ventes immobilières](/loi-2016-1321/titre-iii/chapitre-ii/section-5)

  * [Article 103](/loi-2016-1321/titre-iii/chapitre-ii/section-5/article-103)
  * [Article 104](/loi-2016-1321/titre-iii/chapitre-ii/section-5/article-104)

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-iii/chapitre-ier/section-2/article-85" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/titre-iii/chapitre-ii/article-86" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

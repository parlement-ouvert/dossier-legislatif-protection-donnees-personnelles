<!-- TITLE: Signataires -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-iv/article-113" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/nota" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

La présente loi sera exécutée comme loi de l'Etat.

Fait à Paris, le 7 octobre 2016.

François Hollande
Par le Président de la République :

Le Premier ministre,
Manuel Valls

La ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche,
Najat Vallaud-Belkacem

Le ministre de l'économie et des finances,
Michel Sapin

La ministre des affaires sociales et de la santé,
Marisol Touraine

Le garde des sceaux, ministre de la justice,
Jean-Jacques Urvoas

La ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social,
Myriam El Khomri

Le ministre de l'intérieur,
Bernard Cazeneuve

La ministre du logement et de l'habitat durable,
Emmanuelle Cosse

La ministre de la culture et de la communication,
Audrey Azoulay

La secrétaire d'Etat chargée du numérique et de l'innovation
Axelle Lemaire

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/titre-iv/article-113" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <a href="/loi-2016-1321/nota" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

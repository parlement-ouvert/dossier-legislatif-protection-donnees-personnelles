<!-- TITLE: Nota -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/signataires" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <span> </span> </div>

(1) Travaux préparatoires : loi n° 2016-1321.
Assemblée nationale :
Projet de loi n° 3318 ;
Rapport de M. Luc Belot, au nom de la commission des lois, n° 3399 ;
Avis de Mme Hélène Geoffroy, au nom de la commission des affaires sociales, n° 3387 ;
Avis de M. Emeric Bréhier, au nom de la commission des affaires culturelles, n° 3389 ;
Avis de Mme Corinne Erhel, au nom de la commission des affaires économiques, n° 3391 ;
Rapport d'information de Mme Catherine Coutelle, au nom de la délégation aux droits des femmes, n° 3348 ;
Discussion des 19, 20 et 21 janvier 2016 et adoption, après engagement de la procédure accélérée, le 26 janvier 2016 (TA n° 663).
Sénat :
Projet de loi, adopté par l'Assemblée nationale, n° 325 (2015-2016) ;
Rapport de M. Christophe-André Frassa, au nom de la commission des lois, n° 534 (2015-2016) ;
Texte de la commission n° 535 (2015-2016) ;
Avis de M. Philippe Dallier, au nom de la commission des finances, n° 524 (2015-2016) ;
Avis de Mme Colette Mélot, au nom de la commission de la culture, n° 525 (2015-2016) ;
Avis de M. Patrick Chaize, au nom de la commission de l'aménagement du territoire et du développement durable, n° 526 (2015-2016) ;
Avis de M. Bruno Sido, au nom de la commission des affaires économiques, n° 528 (2015-2016) ;
Discussion les 26, 27, 28, 29 avril, 2 et 3 mai 2016 et adoption le 3 mai 2016 (TA n° 131, 2015-2016).
Assemblée nationale :
Projet de loi, modifié par le Sénat, n° 3724 ;
Rapport de M. Luc Belot, au nom de la commission mixte paritaire, n° 3902 rect ;
Discussion et adoption le 20 juillet 2016 (TA n° 802).
Sénat :
Rapport de M. Christophe-André Frassa, au nom de la commission mixte paritaire, n° 743 (2015-2016) ;
Texte de la commission n° 744 rect. (2015-2016) ;
Discussion et adoption le 28 septembre 2016 (TA n° 185, 2015-2016).

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <a href="/loi-2016-1321/signataires" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a> <span style="text-align: center; text-transform: uppercase;">Loi république numérique</span> <span> </span> </div>

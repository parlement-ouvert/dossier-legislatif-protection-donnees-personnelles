<!-- TITLE: Liens externes -->
<!-- SUBTITLE: Index des liens externes référencés par ce dossier législatif -->

# Article

## CNIL

### Maîtriser mes données

* [https://www.cnil.fr/fr/maitriser-mes-donnees](https://www.cnil.fr/fr/maitriser-mes-donnees)

### Règlement européen sur la protection des données : que faut-il savoir ?

* [https://www.cnil.fr/fr/cnil-direct/question/1253](https://www.cnil.fr/fr/cnil-direct/question/1253)

## CSA

### Les Français et la protection de leurs données personnelles

* [https://www.csa.eu/fr/survey/les-fran%C3%A7ais-et-la-protection-de-leurs-donnees-personnelles](https://www.csa.eu/fr/survey/les-fran%C3%A7ais-et-la-protection-de-leurs-donnees-personnelles)

## Eu Observer

### Trump's anti-privacy order stirs EU angst

* [https://euobserver.com/justice/136699](https://euobserver.com/justice/136699)

## Institut National des Données de Santé

* [http://www.institut-des-donnees-de-sante.fr/](http://www.institut-des-donnees-de-sante.fr/)

## Le Monde (Campus)

### O21 : « La transparence des algorithmes relève des droits civiques »

* [http://www.lemonde.fr/campus/article/2017/05/02/o21-la-transparence-des-algorithmes-releve-des-droits-civiques_5121201_4401467.html#5ayGvqYycS1oLG0s.99](http://www.lemonde.fr/campus/article/2017/05/02/o21-la-transparence-des-algorithmes-releve-des-droits-civiques_5121201_4401467.html#5ayGvqYycS1oLG0s.99)

## Wikipédia

### Droit international privé

* [https://fr.wikipedia.org/wiki/Droit_international_priv%C3%A9](https://fr.wikipedia.org/wiki/Droit_international_priv%C3%A9)

### Journée européenne de la protection des données

* [https://fr.wikipedia.org/wiki/Journ%C3%A9e_europ%C3%A9enne_de_la_protection_des_donn%C3%A9es](https://fr.wikipedia.org/wiki/Journ%C3%A9e_europ%C3%A9enne_de_la_protection_des_donn%C3%A9es)

# Autres textes

## Allemagne

### Bundestag — Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE (règlement général sur la protection des données)

* [https://www.bgbl.de/xaver/bgbl/start.xav?start=%2F%2F*%5B@attr_id%3D'bgbl117s2097.pdf'%5D#__bgbl__%2F%2F*%5B@attr_id%3D'bgbl117s2097.pdf'%5D__1510752756678](https://www.bgbl.de/xaver/bgbl/start.xav?start=%2F%2F*%5B@attr_id%3D'bgbl117s2097.pdf'%5D#__bgbl__%2F%2F*%5B@attr_id%3D'bgbl117s2097.pdf'%5D__1510752756678)

## Belgique

### Chambre des représentants de Belgique — Projet de loi portant création de l’Autorité de protection des données du 9 novembre 2017

* [http://www.lachambre.be/FLWB/PDF/54/2648/54K2648007.pdf](http://www.lachambre.be/FLWB/PDF/54/2648/54K2648007.pdf) (PDF)

## Espagne

### Avant-projet de loi organique pour la protection des donées à caractère personnel

* [http://servicios.mpr.es/seacyp/search_def_asp.aspx?crypt=xh%8A%8Aw%98%85d%A2%B0%8DNs%90%8C%8An%87%A2%7F%8B%99tt%84sm%A3%91](http://servicios.mpr.es/seacyp/search_def_asp.aspx?crypt=xh%8A%8Aw%98%85d%A2%B0%8DNs%90%8C%8An%87%A2%7F%8B%99tt%84sm%A3%91) (PDF)

## Europe

### Charte des droits fondamentaux de l'Union européenne

* [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX:12012P/TXT](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX:12012P/TXT)
* [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A12012P%2FTXT](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A12012P%2FTXT)

### Commission Expert Group

* Commission expert group on the Regulation (EU) 2016/679 and Directive (EU)

  * [http://ec.europa.eu/transparency/regexpert/index.cfm?do=groupDetail.groupDetailPDF&groupID=3461](http://ec.europa.eu/transparency/regexpert/index.cfm?do=groupDetail.groupDetailPDF&groupID=3461) (PDF)

### Commission européenne

* Justice and Consumers — Data protection — Article 29 Working Party

  * [http://ec.europa.eu/newsroom/just/item-detail.cfm?item_id=50083](http://ec.europa.eu/newsroom/just/item-detail.cfm?item_id=50083)

### Conseil de l’Europe

* Convention de sauvegarde des Droits de l'Homme et des Libertés fondamentales telle qu'amendée par les Protocoles n° 11 et n° 14 *

  * [https://www.coe.int/fr/web/conventions/full-list/-/conventions/rms/0900001680063776](https://www.coe.int/fr/web/conventions/full-list/-/conventions/rms/0900001680063776)

* Convention européenne d'entraide judiciaire en matière pénale

  * [https://www.coe.int/fr/web/conventions/full-list/-/conventions/treaty/030](https://www.coe.int/fr/web/conventions/full-list/-/conventions/treaty/030)

* Convention pour la protection des personnes à l'égard du traitement automatisé des données à caractère personnel

  * [https://www.coe.int/fr/web/conventions/full-list/-/conventions/rms/0900001680078b39](https://www.coe.int/fr/web/conventions/full-list/-/conventions/rms/0900001680078b39)
  * [https://www.coe.int/fr/web/conventions/full-list/-/conventions/treaty/108](https://www.coe.int/fr/web/conventions/full-list/-/conventions/treaty/108)

* Convention pour la protection des personnes à l'égard du traitement automatisé des données à caractère personnel dite Convention 108

  * [https://rm.coe.int/1680078b39](https://rm.coe.int/1680078b39)

### Conseil de l’Union européenne

* Décision-cadre 2008/977/JAI du Conseil du 27 novembre 2008 relative à la protection des données à caractère personnel traitées dans le cadre de la coopération policière et judiciaire en matière pénale

  * [http://cdre.eu/82-documentation-en-ligne/justice/droit-penal-materiel/231-decision-cadre-2008-977-jai-du-Conseil-du-27-novembre-2008-relative-a-la-protection-des-donnees-a-caractere-personnel-traitees-dans-le-cadre-de-la-cooperation-policiere-et-judiciaire-en-matie](http://cdre.eu/82-documentation-en-ligne/justice/droit-penal-materiel/231-decision-cadre-2008-977-jai-du-Conseil-du-27-novembre-2008-relative-a-la-protection-des-donnees-a-caractere-personnel-traitees-dans-le-cadre-de-la-cooperation-policiere-et-judiciaire-en-matie)
  * [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX:32008F0977](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX:32008F0977)
  * [http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2008:350:0060:0071:fr:PDF](http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2008:350:0060:0071:fr:PDF) (PDF)
  * [https://edps.europa.eu/sites/edp/files/publication/dec_2008_977_fr_0.pdf](https://edps.europa.eu/sites/edp/files/publication/dec_2008_977_fr_0.pdf) (PDF)

### Conseil européen

* Conseil européen des 24 et 25 octobre 2013

  * EUCO 169/13 CO EUR 13 CONCL 7

    * [http://data.consilium.europa.eu/doc/document/ST-169-2013-INIT/fr/pdf](http://data.consilium.europa.eu/doc/document/ST-169-2013-INIT/fr/pdf) (PDF)

* Règlement (CE) n° 1/2003 du Conseil du 16 décembre 2002 relatif à la mise en œuvre des règles de concurrence prévues aux articles 81 et 82 du traité (Texte présentant de l'intérêt pour l'EEE)

  * [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX:32003R0001](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX:32003R0001)

### Convention européenne de sauvegarde des Droits de l’Homme et des Libertés fondamentales

* Article 6 – Droit à un procès équitable

  * [https://fr.wikisource.org/wiki/Convention_européenne_de_sauvegarde_des_Droits_de_l’Homme_et_des_Libertés_fondamentales#Article_6_–_Droit_à_un_procès_équitable](https://fr.wikisource.org/wiki/Convention_européenne_de_sauvegarde_des_Droits_de_l’Homme_et_des_Libertés_fondamentales#Article_6_–_Droit_à_un_procès_équitable)

### Cour de justice de l'Union européenne, 5e chambre

* Arrêt de la cour (grande chambre) du 28 avril 2015

  * [http://curia.europa.eu/juris/celex.jsf?celex=62013CJ0456&lang1=fr&type=TXT&ancre=](http://curia.europa.eu/juris/celex.jsf?celex=62013CJ0456&lang1=fr&type=TXT&ancre=)

* Arrêt de la cour (grande chambre) du 6 octobre 2015

  * [http://curia.europa.eu/juris/document/document.jsf?docid=169195](http://curia.europa.eu/juris/document/document.jsf?docid=169195)

### Cour de justice de l'Union européenne, communiqué de presse

* La Cour déclare invalide la décision de la Commission constatant que les États-Unis assurent un niveau de protection adéquat aux données à caractère personnel transférées

  * [https://curia.europa.eu/jcms/upload/docs/application/pdf/2015-10/cp150117fr.pdf](https://curia.europa.eu/jcms/upload/docs/application/pdf/2015-10/cp150117fr.pdf) (PDF)

### Cour de justice de l'Union européenne, grande chambre

* Arrêt de la Cour (cinquième chambre) du 20 mars 1997. - Commission des Communautés européennes contre République fédérale d'Allemagne. - Manquement d'Etat - Droit de séjour - Directives 90/364/CEE et 90/365/CEE du Conseil. - Affaire C-96/95.

  * [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A61995CJ0096](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A61995CJ0096)

### Directive 95/46/CE du Parlement européen et du Conseil du 24 octobre 1995 relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000697074](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000697074)

### Fonctionnement de l'Union européenne

* [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex:12012E/TXT](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex:12012E/TXT)

### Journal officiel de l’Union européenne

* Journal officiel de l’Union européenne, C 202, 7 juin 2016

  * [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=OJ:C:2016:202:TOC](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=OJ:C:2016:202:TOC)

### Parlement européen

* La Charte des Droits fondamentaux de l'Union européenne

  * [http://www.europarl.europa.eu/charter/default_fr.htm](http://www.europarl.europa.eu/charter/default_fr.htm)

### Parlement européen et Conseil de l’Union européenne

* Directive (UE) 2016/680 du Parlement européen et du Conseil du 27 avril 2016 relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel par les autorités compétentes à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales, et à la libre circulation de ces données, et abrogeant la Décision-cadre 2008/977/JAI du Conseil

  * [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX:32016L0680](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX:32016L0680)
  * [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32016L0680](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32016L0680)
  * [http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016L0680](http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016L0680)

* Directive 2002/58/CE du Parlement européen et du Conseil du 12 juillet 2002 concernant le traitement des données à caractère personnel et la protection de la vie privée dans le secteur des communications électroniques (directive vie privée et communications électroniques)

  * [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32002L0058](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32002L0058)

* Directive 2009/136/CE du Parlement européen et du Conseil du 25 novembre 2009 modifiant la directive 2002/22/CE concernant le service universel et les droits des utilisateurs au regard des réseaux et services de communications électroniques, la directive 2002/58/CE concernant le traitement des données à caractère personnel et la protection de la vie privée dans le secteur des communications électroniques et le règlement (CE) no 2006/2004 relatif à la coopération entre les autorités nationales chargées de veiller à l’application de la législation en matière de protection des consommateurs (Texte présentant de l’intérêt pour l’EEE)

  * [http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2009:337:0011:0036:fr:PDF](http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2009:337:0011:0036:fr:PDF) (PDF)

* Directive 95/46/CE du Parlement européen et du Conseil, du 24 octobre 1995 relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données

  * [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A31995L0046](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A31995L0046)
  * [http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:31995L0046](http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:31995L0046)

* Décision-cadre 2008/977/JAI du Conseil du 27 novembre 2008 relative à la protection des données à caractère personnel traitées dans le cadre de la coopération policière et judiciaire en matière pénale

  * [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex:32008F0977](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex:32008F0977)

* Proposition de règlement du Parlement européen et du Conseil relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (règlement général sur la protection des données)

  * [http://data.consilium.europa.eu/doc/document/ST-9565-2015-INIT/fr/pdf](http://data.consilium.europa.eu/doc/document/ST-9565-2015-INIT/fr/pdf) (PDF)

* Règlement (CE) No 45/2001 du Parlement européen et du Conseil du 18 décembre 2000 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel par les institutions et organes communautaires et à la libre circulation de ces données

  * [http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2001:008:0001:0022:FR:PDF](http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2001:008:0001:0022:FR:PDF) (PDF)

* Règlement (UE) 2016/679 du Parlement européen européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE (règlement général sur la protection des données)

  * [http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679](http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679)

* Règlement du Parlement européen et du Conseil relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (règlement général sur la protection des données)

  * [http://ec.europa.eu/justice/data-protection/document/review2012/com_2012_11_fr.pdf](http://ec.europa.eu/justice/data-protection/document/review2012/com_2012_11_fr.pdf) (PDF)

### Traité sur le fonctionnement de l'Union européenne

* [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX:12012E/TXT](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX:12012E/TXT)

### Versions consolidées du traité sur l'Union européenne et du traité sur le fonctionnement de l'Union européenne fournit des recommandations relatives à la planification, à la mise en œuvre, à l'entretien et à l'amélioration de la conservation des fonds et collections d'archives et de bibliothèques sous les formes suivantes:

* Article 55

  * [http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=uriserv:OJ.C_.2016.202.01.0001.01.FRA&toc=OJ:C:2016:202:TOC#C_2016202FR.01004701](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=uriserv:OJ.C_.2016.202.01.0001.01.FRA&toc=OJ:C:2016:202:TOC#C_2016202FR.01004701)

## France

### Assemblée nationale

* Etude d’impact relatif à la protection des données personnelles

  * [http://www.assemblee-nationale.fr/15/projets/pl0490-ei.asp](http://www.assemblee-nationale.fr/15/projets/pl0490-ei.asp)

* Incidences des nouvelles normes européennes en matière de protection des données personnelles sur la législation française

  * [http://www.assemblee-nationale.fr/14/rap-info/i4544.asp](http://www.assemblee-nationale.fr/14/rap-info/i4544.asp)

* Projet de loi n° 490 relatif à la protection des données personnelles

  * [http://www2.assemblee-nationale.fr/documents/notice/15/projets/pl0490/(index](http://www2.assemblee-nationale.fr/documents/notice/15/projets/pl0490/(index)

### CNIL

* Délibération 2017-299 du 30 novembre 2017

  * [https://www.legifrance.gouv.fr/affichcnil.do?oldAction=rechExpCNIL&id=CNILTEXT000036195647](https://www.legifrance.gouv.fr/affichcnil.do?oldAction=rechExpCNIL&id=CNILTEXT000036195647)

* Etude d'impacts sur la vie privée : suivez la méthode de la CNIL

  * [https://www.cnil.fr/fr/etude-dimpacts-sur-la-vie-privee-suivez-la-methode-de-la-CNIL](https://www.cnil.fr/fr/etude-dimpacts-sur-la-vie-privee-suivez-la-methode-de-la-CNIL)

### Code civil

* Article 1127-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070721&idArticle=LEGIARTI000033220042&dateTexte=20180111&categorieLien=id#LEGIARTI000033220042](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070721&idArticle=LEGIARTI000033220042&dateTexte=20180111&categorieLien=id#LEGIARTI000033220042)

* Article 1127-5

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070721&idArticle=LEGIARTI000032007512&dateTexte=20180111&categorieLien=id#LEGIARTI000032007512](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070721&idArticle=LEGIARTI000032007512&dateTexte=20180111&categorieLien=id#LEGIARTI000032007512)

* Article 1127-6

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070721&idArticle=LEGIARTI000032007514&dateTexte=20180111&categorieLien=id#LEGIARTI000032007514](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070721&idArticle=LEGIARTI000032007514&dateTexte=20180111&categorieLien=id#LEGIARTI000032007514)

* Article 3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006419282&cidTexte=LEGITEXT000006070721](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006419282&cidTexte=LEGITEXT000006070721)

### Code de justice administrative

* Article L10

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070933&idArticle=LEGIARTI000033219190&dateTexte=20180111&categorieLien=id#LEGIARTI000033219190](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070933&idArticle=LEGIARTI000033219190&dateTexte=20180111&categorieLien=id#LEGIARTI000033219190)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006449164&cidTexte=LEGITEXT000006070933](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006449164&cidTexte=LEGITEXT000006070933)

* Article R311-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006449855&cidTexte=LEGITEXT000006070933&dateTexte=20100223](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006449855&cidTexte=LEGITEXT000006070933&dateTexte=20100223)

* Article R312-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033340848&cidTexte=LEGITEXT000006070933&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033340848&cidTexte=LEGITEXT000006070933&dateTexte=20180114)

* Partie réglementaire - Décrets en Conseil d'Etat

  * Livre II : Les tribunaux administratifs et les cours administratives d'appel — Titre III : Dispositions statutaires — Chapitre II : Le Conseil supérieur des tribunaux administratifs et cours administratives d'appel — Section 2 : Composition du Conseil supérieur — Sous-section 2 : Election des représentants des magistrats

    * [https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000034333474&cidTexte=LEGITEXT000006070933&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000034333474&cidTexte=LEGITEXT000006070933&dateTexte=20180114)

### Code de l'action sociale et des familles

* Article L115-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220387&dateTexte=20180111&categorieLien=id#LEGIARTI000033220387](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220387&dateTexte=20180111&categorieLien=id#LEGIARTI000033220387)

* Article L146-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220258&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220258&dateTexte=&categorieLien=id)

* Article L146-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220253&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220253&dateTexte=&categorieLien=id)

* Article L241-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220240&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220240&dateTexte=&categorieLien=id)

* Article L241-3-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000006797045&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000006797045&dateTexte=&categorieLien=id)

* Article L241-3-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000031727805&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000031727805&dateTexte=&categorieLien=id)

* Article L241-6

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000031928935&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000031928935&dateTexte=&categorieLien=id)

* Article L247-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031728718&cidTexte=LEGITEXT000006074069&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031728718&cidTexte=LEGITEXT000006074069&dateTexte=20180114)

* Article L542-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220202&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000033220202&dateTexte=&categorieLien=id)

### Code de l'environnement

* Article L541-10

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074220&idArticle=LEGIARTI000033219025&dateTexte=20180111&categorieLien=id#LEGIARTI000033219025](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074220&idArticle=LEGIARTI000033219025&dateTexte=20180111&categorieLien=id#LEGIARTI000033219025)

### Code de l'organisation judiciaire

* Article L111-13

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071164&idArticle=LEGIARTI000033206155](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071164&idArticle=LEGIARTI000033206155)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071164&idArticle=LEGIARTI000033206155&dateTexte=20180111&categorieLien=id#LEGIARTI000033206155](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071164&idArticle=LEGIARTI000033206155&dateTexte=20180111&categorieLien=id#LEGIARTI000033206155)

### Code de l'éducation

* Article L611-8

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000033219258&dateTexte=20180111&categorieLien=id#LEGIARTI000033219258](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000033219258&dateTexte=20180111&categorieLien=id#LEGIARTI000033219258)

* Article L681-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000033220673&dateTexte=20180111&categorieLien=id#LEGIARTI000033220673](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000033220673&dateTexte=20180111&categorieLien=id#LEGIARTI000033220673)

* Article L683-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000033220633&dateTexte=20180111&categorieLien=id#LEGIARTI000033220633](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000033220633&dateTexte=20180111&categorieLien=id#LEGIARTI000033220633)

* Article L684-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000033220591&dateTexte=20180111&categorieLien=id#LEGIARTI000033220591](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000033220591&dateTexte=20180111&categorieLien=id#LEGIARTI000033220591)

* Article L822-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000033219268&dateTexte=20180111&categorieLien=id#LEGIARTI000033219268](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000033219268&dateTexte=20180111&categorieLien=id#LEGIARTI000033219268)

### Code de l'énergie

* Article L111-73-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000023983208&idArticle=LEGIARTI000033206361&dateTexte=20180111&categorieLien=id#LEGIARTI000033206361](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000023983208&idArticle=LEGIARTI000033206361&dateTexte=20180111&categorieLien=id#LEGIARTI000033206361)

* Article L111-77-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000023983208&idArticle=LEGIARTI000033206494&dateTexte=20180111&categorieLien=id#LEGIARTI000033206494](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000023983208&idArticle=LEGIARTI000033206494&dateTexte=20180111&categorieLien=id#LEGIARTI000033206494)

### Code de la consommation

* [https://www.gesetze-im-internet.de/bdsg_1990/](https://www.gesetze-im-internet.de/bdsg_1990/)

* Article L111-6

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000032227342&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000032227342&dateTexte=&categorieLien=id)

* Article L111-7

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033219601&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033219601&dateTexte=&categorieLien=id)

* Article L111-7-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033207023&dateTexte=20180111&categorieLien=id#LEGIARTI000033207023](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033207023&dateTexte=20180111&categorieLien=id#LEGIARTI000033207023)

* Article L111-7-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033207118&dateTexte=20180111&categorieLien=id#LEGIARTI000033207118](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033207118&dateTexte=20180111&categorieLien=id#LEGIARTI000033207118)

* Article L112-8

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000006291881&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000006291881&dateTexte=&categorieLien=cid)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033216993&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033216993&dateTexte=&categorieLien=id)

* Article L122-22

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033206982&dateTexte=20180111&categorieLien=id#LEGIARTI000033206982](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033206982&dateTexte=20180111&categorieLien=id#LEGIARTI000033206982)

* Article L131-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000032227138&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000032227138&dateTexte=&categorieLien=id)

* Article L131-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033219595&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033219595&dateTexte=&categorieLien=id)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033219595&dateTexte=20180111&categorieLien=id#LEGIARTI000033219595](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033219595&dateTexte=20180111&categorieLien=id#LEGIARTI000033219595)

* Article L224-30

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033219635&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033219635&dateTexte=&categorieLien=id)

* Article L224-42-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033206901&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033206901&dateTexte=&categorieLien=id)

* Article L224-42-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033206903&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033206903&dateTexte=&categorieLien=id)

* Article L224-42-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033206906&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033206906&dateTexte=&categorieLien=id)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033206912&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033206912&dateTexte=&categorieLien=id)

* Article L224-54

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000032221591&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000032221591&dateTexte=&categorieLien=cid)

* Article L242-20

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033219584&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000033219584&dateTexte=&categorieLien=id)

* Partie législative nouvelle

  * Livre II : FORMATION ET ÉXECUTION DES CONTRATS

    * Titre II : RÈGLES DE FORMATION ET D'EXÉCUTION DE CERTAINS CONTRATS — Chapitre IV : Règles spécifiques à des contrats ayant un objet particulier — Section 3 : Contrats de services de communications électroniques — Sous-section 4 : Récupération et portabilité des données

      * [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006069565&idSectionTA=LEGISCTA000033206899&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006069565&idSectionTA=LEGISCTA000033206899&dateTexte=&categorieLien=id)

  * Livre Ier : INFORMATION DES CONSOMMATEURS ET PRATIQUES COMMERCIALES

    * Titre II : PRATIQUES COMMERCIALES INTERDITES ET PRATIQUES COMMERCIALES RÉGLEMENTÉES — Chapitre II : Pratiques commerciales réglementées — Section 3 : Règles propres à certaines publicités et pratiques commerciales — Sous-section 5 : Appellation de coffre-fort numérique

      * [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006069565&idSectionTA=LEGISCTA000033206980&dateTexte=20180111&categorieLien=id#LEGISCTA000033206980](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006069565&idSectionTA=LEGISCTA000033206980&dateTexte=20180111&categorieLien=id#LEGISCTA000033206980)

### Code de la construction et de l'habitation

* Article L111-7-12

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074096&idArticle=LEGIARTI000033220190&dateTexte=20180111&categorieLien=id#LEGIARTI000033220190](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074096&idArticle=LEGIARTI000033220190&dateTexte=20180111&categorieLien=id#LEGIARTI000033220190)

### Code de la défense

* Article D4123-9

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000018717558&cidTexte=LEGITEXT000006071307&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000018717558&cidTexte=LEGITEXT000006071307&dateTexte=20180114)

* Article L2321-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000033206854&dateTexte=20180111&categorieLien=id#LEGIARTI000033206854](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000033206854&dateTexte=20180111&categorieLien=id#LEGIARTI000033206854)

* Article L2441-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000033220787&dateTexte=20180111&categorieLien=id#LEGIARTI000033220787](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000033220787&dateTexte=20180111&categorieLien=id#LEGIARTI000033220787)

* Article L2451-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000033220775&dateTexte=20180111&categorieLien=id#LEGIARTI000033220775](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000033220775&dateTexte=20180111&categorieLien=id#LEGIARTI000033220775)

* Article L2461-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000033220752&dateTexte=20180111&categorieLien=id#LEGIARTI000033220752](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000033220752&dateTexte=20180111&categorieLien=id#LEGIARTI000033220752)

* Article L2471-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000033220721&dateTexte=20180111&categorieLien=id#LEGIARTI000033220721](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000033220721&dateTexte=20180111&categorieLien=id#LEGIARTI000033220721)

* Article L4123-9-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000032646880&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071307&idArticle=LEGIARTI000032646880&dateTexte=&categorieLien=cid)

* Article R4123-51

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033824531&cidTexte=LEGITEXT000006071307&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033824531&cidTexte=LEGITEXT000006071307&dateTexte=20180114)

### Code de la mutualité

* [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006074067&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006074067&dateTexte=&categorieLien=cid)

### Code de la propriété intellectuelle

* [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006069414&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006069414&dateTexte=&categorieLien=cid)

* Article L122-5

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000033219336&dateTexte=20180111&categorieLien=id#LEGIARTI000033219336](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000033219336&dateTexte=20180111&categorieLien=id#LEGIARTI000033219336)

* Article L321-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000006279093&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000006279093&dateTexte=&categorieLien=cid)

* Article L331-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000006279126&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000006279126&dateTexte=&categorieLien=cid)

* Article L342-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000033219347&dateTexte=20180111&categorieLien=id#LEGIARTI000033219347](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000033219347&dateTexte=20180111&categorieLien=id#LEGIARTI000033219347)

### Code de la recherche

* Article L533-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071190&idArticle=LEGIARTI000033205794&dateTexte=20180111&categorieLien=id#LEGIARTI000033205794](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071190&idArticle=LEGIARTI000033205794&dateTexte=20180111&categorieLien=id#LEGIARTI000033205794)

* Article L545-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071190&idArticle=LEGIARTI000033220579&dateTexte=20180111&categorieLien=id#LEGIARTI000033220579](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071190&idArticle=LEGIARTI000033220579&dateTexte=20180111&categorieLien=id#LEGIARTI000033220579)

### Code de la santé publique

* Article L1111-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685758](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685758)

* Article L1111-23

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031929145&cidTexte=LEGITEXT000006072665&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031929145&cidTexte=LEGITEXT000006072665&dateTexte=20180114)

* Article L1111-5

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685769](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685769)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685769&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685769&dateTexte=&categorieLien=cid)

* Article L1111-5-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031917409](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031917409)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031917409&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031917409&dateTexte=&categorieLien=cid)

* Article L1111-7

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685776&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685776&dateTexte=&categorieLien=cid)

* Article L1121-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685825&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685825&dateTexte=&categorieLien=cid)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032722870&cidTexte=LEGITEXT000006072665](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032722870&cidTexte=LEGITEXT000006072665)

* Article L1123-6

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685877](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006685877)

* Article L1413-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006686950&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006686950&dateTexte=&categorieLien=cid)

* Article L1451-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000022019483](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000022019483)

* Article L1453-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000033219005&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000033219005&dateTexte=&categorieLien=id)

* Article L1462-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031923960](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000031923960)

* Article L4321-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000033220313&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000033220313&dateTexte=&categorieLien=id)

* Article L6113-7

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006690710](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006690710)

* Article L6113-8

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021940593&cidTexte=LEGITEXT000006072665&dateTexte=20180112](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021940593&cidTexte=LEGITEXT000006072665&dateTexte=20180112)

* Article R6113-7

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032480354&cidTexte=LEGITEXT000006072665&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032480354&cidTexte=LEGITEXT000006072665&dateTexte=20180114)

* Partie législative

  * Première partie : Protection générale de la santé

    * Livre IV : Administration générale de la santé

      * Titre III : Agences régionales de santé

        * Chapitre V : Modalités et moyens d'intervention des agences régionales de santé — Section 3 : Accès aux données de santé

          * [https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000020897700&cidTexte=LEGITEXT000006072665&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000020897700&cidTexte=LEGITEXT000006072665&dateTexte=20180114)

      * Titre Ier : Institutions

        * Chapitre III : Veille sanitaire, urgence sanitaire et promotion de la santé — Section 1 : Agence nationale de santé publique

          * [https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000032404559&cidTexte=LEGITEXT000006072665&dateTexte=20180112](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000032404559&cidTexte=LEGITEXT000006072665&dateTexte=20180112)

      * Titre VI : Mise à disposition des données de santé

        * Chapitre II : Institut national des données de santé

          * [https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031923955&cidTexte=LEGITEXT000006072665&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031923955&cidTexte=LEGITEXT000006072665&dateTexte=20180114)

        * Chapitre Ier : Système national des données de santé

          * [https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031923880&cidTexte=LEGITEXT000006072665&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031923880&cidTexte=LEGITEXT000006072665&dateTexte=20180114)

    * Livre Ier : Protection des personnes en matière de santé

      * Titre Ier : Droits des personnes malades et des usagers du système de santé

        * Chapitre Ier : Information des usagers du système de santé et expression de leur volonté

          * Section 3 : Dossier médical partagé et dossier pharmaceutique

            * [https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031929174&cidTexte=LEGITEXT000006072665&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031929174&cidTexte=LEGITEXT000006072665&dateTexte=20180114)

        * Chapitre Ier : Informations des usagers du système de santé et expression de leur volonté

          * Section 1 : Principes généraux — Sous-section 1 bis : Identifiant national de santé

            * [https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000034299702&cidTexte=LEGITEXT000006072665&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000034299702&cidTexte=LEGITEXT000006072665&dateTexte=20180114)

        * Chapitre préliminaire : Droits de la personne

          * Section 1 : Conditions d'échange et de partage d'informations entre professionnels de santé et autres professionnels des champs social et médico-social

            * [https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000032924980&cidTexte=LEGITEXT000006072665&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000032924980&cidTexte=LEGITEXT000006072665&dateTexte=20180114)

### Code de la sécurité intérieure

* Article L114-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000025504941](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000025504941)

* Article L321-10

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000033207380&dateTexte=20180111&categorieLien=id#LEGIARTI000033207380](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000033207380&dateTexte=20180111&categorieLien=id#LEGIARTI000033207380)

* Article L321-11

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000033207383&dateTexte=20180111&categorieLien=id#LEGIARTI000033207383](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000033207383&dateTexte=20180111&categorieLien=id#LEGIARTI000033207383)

* Article L321-8

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000033207374&dateTexte=20180111&categorieLien=id#LEGIARTI000033207374](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000033207374&dateTexte=20180111&categorieLien=id#LEGIARTI000033207374)

* Article L321-9

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000033207377&dateTexte=20180111&categorieLien=id#LEGIARTI000033207377](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000033207377&dateTexte=20180111&categorieLien=id#LEGIARTI000033207377)

* Article R234-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000028285216](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000025503132&idArticle=LEGIARTI000028285216)

* Article R234-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000035364473&cidTexte=LEGITEXT000025503132&dateTexte=20180113](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000035364473&cidTexte=LEGITEXT000025503132&dateTexte=20180113)

* Partie législative

  * LIVRE III : POLICES ADMINISTRATIVES SPÉCIALES — TITRE II : JEUX DE HASARD, CASINOS, LOTERIES — Chapitre Ier bis : Compétitions de jeux vidéo

    * [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000025503132&idSectionTA=LEGISCTA000033207371&dateTexte=20180111&categorieLien=id#LEGISCTA000033207371](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000025503132&idSectionTA=LEGISCTA000033207371&dateTexte=20180111&categorieLien=id#LEGISCTA000033207371)

### Code de la sécurité sociale

* Article L161-28-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000006741264&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000006741264&dateTexte=&categorieLien=cid)

* Article L161-29

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031931962&cidTexte=LEGITEXT000006073189&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031931962&cidTexte=LEGITEXT000006073189&dateTexte=20180114)

* Article L161-33

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006741272&cidTexte=LEGITEXT000006073189&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006741272&cidTexte=LEGITEXT000006073189&dateTexte=20180114)

### Code de la voirie routière

* Article L119-1-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070667&idArticle=LEGIARTI000033205871&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070667&idArticle=LEGIARTI000033205871&dateTexte=&categorieLien=id)

### Code de procédure pénale

* Article 230-8

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000032655577](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000032655577)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000035923618&cidTexte=LEGITEXT000006071154&categorieLien=id&dateTexte=20180501](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000035923618&cidTexte=LEGITEXT000006071154&categorieLien=id&dateTexte=20180501)

* Article 40

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000006574931&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000006574931&dateTexte=&categorieLien=cid)

* Article 804

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000006578405&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000006578405&dateTexte=&categorieLien=cid)

* Article R15-33-66-13

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000025832168&cidTexte=LEGITEXT000006071154&dateTexte=20180115&categorieLien=id&oldAction=&nbResultRech=](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000025832168&cidTexte=LEGITEXT000006071154&dateTexte=20180115&categorieLien=id&oldAction=&nbResultRech=)

* Article R40-27

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000025818438&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000025818438&dateTexte=&categorieLien=cid)

* Article R57-9-26

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000029018055&cidTexte=LEGITEXT000006071154&dateTexte=20180115&categorieLien=id&oldAction=&nbResultRech=](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000029018055&cidTexte=LEGITEXT000006071154&dateTexte=20180115&categorieLien=id&oldAction=&nbResultRech=)

### Code des assurances

* [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006073984&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006073984&dateTexte=&categorieLien=cid)

### Code des communes de la Nouvelle-Calédonie

* Article L125-12

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070300&idArticle=LEGIARTI000032266982&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070300&idArticle=LEGIARTI000032266982&dateTexte=&categorieLien=id)

* Article L212-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070300&idArticle=LEGIARTI000033219174&dateTexte=20180111&categorieLien=id#LEGIARTI000033219174](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070300&idArticle=LEGIARTI000033219174&dateTexte=20180111&categorieLien=id#LEGIARTI000033219174)

### Code des postes et des communications électroniques

* Article L100

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033207397&dateTexte=20180111&categorieLien=id#LEGIARTI000033207397](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033207397&dateTexte=20180111&categorieLien=id#LEGIARTI000033207397)

* Article L101

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033207399&dateTexte=20180111&categorieLien=id#LEGIARTI000033207399](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033207399&dateTexte=20180111&categorieLien=id#LEGIARTI000033207399)

* Article L125

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219448&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219448&dateTexte=&categorieLien=id)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219448&dateTexte=20180111&categorieLien=id#LEGIARTI000033219448](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219448&dateTexte=20180111&categorieLien=id#LEGIARTI000033219448)

* Article L130

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219565&dateTexte=20180111&categorieLien=id#LEGIARTI000033219565](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219565&dateTexte=20180111&categorieLien=id#LEGIARTI000033219565)

* Article L131

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219472&dateTexte=20180111&categorieLien=id#LEGIARTI000033219472](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219472&dateTexte=20180111&categorieLien=id#LEGIARTI000033219472)

* Article L135

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219461&dateTexte=20180111&categorieLien=id#LEGIARTI000033219461](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219461&dateTexte=20180111&categorieLien=id#LEGIARTI000033219461)

* Article L136

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033206940&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033206940&dateTexte=&categorieLien=id)

* Article L137

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033206958&dateTexte=20180111&categorieLien=id#LEGIARTI000033206958](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033206958&dateTexte=20180111&categorieLien=id#LEGIARTI000033206958)

* Article L2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219554&dateTexte=20180111&categorieLien=id#LEGIARTI000033219554](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219554&dateTexte=20180111&categorieLien=id#LEGIARTI000033219554)

* Article L2-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219549&dateTexte=20180111&categorieLien=id#LEGIARTI000033219549](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219549&dateTexte=20180111&categorieLien=id#LEGIARTI000033219549)

* Article L32

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000006465394&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000006465394&dateTexte=&categorieLien=cid)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219763&dateTexte=20180111&categorieLien=id#LEGIARTI000033219763](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219763&dateTexte=20180111&categorieLien=id#LEGIARTI000033219763)

* Article L32-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219424&dateTexte=20180111&categorieLien=id#LEGIARTI000033219424](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219424&dateTexte=20180111&categorieLien=id#LEGIARTI000033219424)

* Article L32-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219756&dateTexte=20180111&categorieLien=id#LEGIARTI000033219756](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219756&dateTexte=20180111&categorieLien=id#LEGIARTI000033219756)

* Article L32-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219415&dateTexte=20180111&categorieLien=id#LEGIARTI000033219415](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219415&dateTexte=20180111&categorieLien=id#LEGIARTI000033219415)

* Article L32-5

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219443&dateTexte=20180111&categorieLien=id#LEGIARTI000033219443](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219443&dateTexte=20180111&categorieLien=id#LEGIARTI000033219443)

* Article L33-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000006465743&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000006465743&dateTexte=&categorieLien=cid)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219394&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219394&dateTexte=&categorieLien=id)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219394&dateTexte=20180111&categorieLien=id#LEGIARTI000033219394](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219394&dateTexte=20180111&categorieLien=id#LEGIARTI000033219394)

* Article L33-11

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219838&dateTexte=20180111&categorieLien=id#LEGIARTI000033219838](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219838&dateTexte=20180111&categorieLien=id#LEGIARTI000033219838)

* Article L33-12

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219884&dateTexte=20180111&categorieLien=id#LEGIARTI000033219884](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219884&dateTexte=20180111&categorieLien=id#LEGIARTI000033219884)

* Article L33-13

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033207641&dateTexte=20180111&categorieLien=id#LEGIARTI000033207641](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033207641&dateTexte=20180111&categorieLien=id#LEGIARTI000033207641)

* Article L33-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219536&dateTexte=20180111&categorieLien=id#LEGIARTI000033219536](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219536&dateTexte=20180111&categorieLien=id#LEGIARTI000033219536)

* Article L34

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219522&dateTexte=20180111&categorieLien=id#LEGIARTI000033219522](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219522&dateTexte=20180111&categorieLien=id#LEGIARTI000033219522)

* Article L34-10

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033220415&dateTexte=20180111&categorieLien=id#LEGIARTI000033220415](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033220415&dateTexte=20180111&categorieLien=id#LEGIARTI000033220415)

* Article L34-8-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219870&dateTexte=20180111&categorieLien=id#LEGIARTI000033219870](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219870&dateTexte=20180111&categorieLien=id#LEGIARTI000033219870)

* Article L35

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219909&dateTexte=20180111&categorieLien=id#LEGIARTI000033219909](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219909&dateTexte=20180111&categorieLien=id#LEGIARTI000033219909)

* Article L35-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219511&dateTexte=20180111&categorieLien=id#LEGIARTI000033219511](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219511&dateTexte=20180111&categorieLien=id#LEGIARTI000033219511)

* Article L35-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219502&dateTexte=20180111&categorieLien=id#LEGIARTI000033219502](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219502&dateTexte=20180111&categorieLien=id#LEGIARTI000033219502)

* Article L35-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219490&dateTexte=20180111&categorieLien=id#LEGIARTI000033219490](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219490&dateTexte=20180111&categorieLien=id#LEGIARTI000033219490)

* Article L35-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219485&dateTexte=20180111&categorieLien=id#LEGIARTI000033219485](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219485&dateTexte=20180111&categorieLien=id#LEGIARTI000033219485)

* Article L35-7

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219921&dateTexte=20180111&categorieLien=id#LEGIARTI000033219921](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219921&dateTexte=20180111&categorieLien=id#LEGIARTI000033219921)

* Article L36-11

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219357&dateTexte=20180111&categorieLien=id#LEGIARTI000033219357](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219357&dateTexte=20180111&categorieLien=id#LEGIARTI000033219357)

* Article L36-7

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219381&dateTexte=20180111&categorieLien=id#LEGIARTI000033219381](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219381&dateTexte=20180111&categorieLien=id#LEGIARTI000033219381)

* Article L36-8

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219367&dateTexte=20180111&categorieLien=id#LEGIARTI000033219367](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219367&dateTexte=20180111&categorieLien=id#LEGIARTI000033219367)

* Article L40

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219431&dateTexte=20180111&categorieLien=id#LEGIARTI000033219431](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219431&dateTexte=20180111&categorieLien=id#LEGIARTI000033219431)

* Article L42-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033220002&dateTexte=20180111&categorieLien=id#LEGIARTI000033220002](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033220002&dateTexte=20180111&categorieLien=id#LEGIARTI000033220002)

* Article L42-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219898&dateTexte=20180111&categorieLien=id#LEGIARTI000033219898](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219898&dateTexte=20180111&categorieLien=id#LEGIARTI000033219898)

* Article L44

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219480&dateTexte=20180111&categorieLien=id#LEGIARTI000033219480](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219480&dateTexte=20180111&categorieLien=id#LEGIARTI000033219480)

* Article L47

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219925&dateTexte=20180111&categorieLien=id#LEGIARTI000033219925](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219925&dateTexte=20180111&categorieLien=id#LEGIARTI000033219925)

* Article L48

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219845&dateTexte=20180111&categorieLien=id#LEGIARTI000033219845](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219845&dateTexte=20180111&categorieLien=id#LEGIARTI000033219845)

* Article L51

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219933&dateTexte=20180111&categorieLien=id#LEGIARTI000033219933](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070987&idArticle=LEGIARTI000033219933&dateTexte=20180111&categorieLien=id#LEGIARTI000033219933)

* Partie législative

  * LIVRE III : Autres services, dispositions communes et finales

    * TITRE II : Dispositions communes

      * [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006070987&idSectionTA=LEGISCTA000033220027&dateTexte=20180111&categorieLien=id#LEGISCTA000033220027](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006070987&idSectionTA=LEGISCTA000033220027&dateTexte=20180111&categorieLien=id#LEGISCTA000033220027)

    * TITRE III : Dispositions finales

      * [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006070987&idSectionTA=LEGISCTA000033220025&dateTexte=20180111&categorieLien=id#LEGISCTA000033220025](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006070987&idSectionTA=LEGISCTA000033220025&dateTexte=20180111&categorieLien=id#LEGISCTA000033220025)

    * TITRE Ier : Autres services

      * [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006070987&idSectionTA=LEGISCTA000033207395&dateTexte=20180111&categorieLien=id#LEGISCTA000033207395](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006070987&idSectionTA=LEGISCTA000033207395&dateTexte=20180111&categorieLien=id#LEGISCTA000033207395)
      * [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006070987&idSectionTA=LEGISCTA000033220029&dateTexte=20180111&categorieLien=id#LEGISCTA000033220029](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006070987&idSectionTA=LEGISCTA000033220029&dateTexte=20180111&categorieLien=id#LEGISCTA000033220029)

### Code des relations entre le public et l'administration

* [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000031366350&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000031366350&dateTexte=20180114)

* Article L112-10

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219677&dateTexte=20180111&categorieLien=id#LEGIARTI000033219677](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219677&dateTexte=20180111&categorieLien=id#LEGIARTI000033219677)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033221175&dateTexte=20180111&categorieLien=id#LEGIARTI000033221175](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033221175&dateTexte=20180111&categorieLien=id#LEGIARTI000033221175)

* Article L112-11

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219980&dateTexte=20180111&categorieLien=id#LEGIARTI000033219980](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219980&dateTexte=20180111&categorieLien=id#LEGIARTI000033219980)

* Article L112-15

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220034&dateTexte=20180111&categorieLien=id#LEGIARTI000033220034](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220034&dateTexte=20180111&categorieLien=id#LEGIARTI000033220034)

* Article L112-7

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367344&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367344&dateTexte=&categorieLien=cid)

* Article L113-13

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219990&dateTexte=20180111&categorieLien=id#LEGIARTI000033219990](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219990&dateTexte=20180111&categorieLien=id#LEGIARTI000033219990)

* Article L114-8

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219997&dateTexte=20180111&categorieLien=id#LEGIARTI000033219997](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219997&dateTexte=20180111&categorieLien=id#LEGIARTI000033219997)

* Article L300-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367687&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367687&dateTexte=&categorieLien=cid)

* Article L300-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367689&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367689&dateTexte=&categorieLien=cid)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218936&dateTexte=20180111&categorieLien=id#LEGIARTI000033218936](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218936&dateTexte=20180111&categorieLien=id#LEGIARTI000033218936)

* Article L300-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033206528&dateTexte=20180111&categorieLien=id#LEGIARTI000033206528](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033206528&dateTexte=20180111&categorieLien=id#LEGIARTI000033206528)

* Article L300-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205519&dateTexte=20180111&categorieLien=id#LEGIARTI000033205519](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205519&dateTexte=20180111&categorieLien=id#LEGIARTI000033205519)

* Article L311-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218946&dateTexte=20180111&categorieLien=id#LEGIARTI000033218946](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218946&dateTexte=20180111&categorieLien=id#LEGIARTI000033218946)

* Article L311-3-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205535&dateTexte=20180111&categorieLien=id#LEGIARTI000033205535](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205535&dateTexte=20180111&categorieLien=id#LEGIARTI000033205535)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033205535&cidTexte=LEGITEXT000031366350&dateTexte=20180113](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033205535&cidTexte=LEGITEXT000031366350&dateTexte=20180113)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033205535&cidTexte=LEGITEXT000031366350&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033205535&cidTexte=LEGITEXT000031366350&dateTexte=20180114)

* Article L311-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219038&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219038&dateTexte=&categorieLien=id)

* Article L311-5

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367708&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367708&dateTexte=&categorieLien=cid)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218919&dateTexte=20180111&categorieLien=id#LEGIARTI000033218919](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218919&dateTexte=20180111&categorieLien=id#LEGIARTI000033218919)

* Article L311-6

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367716&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000031367716&dateTexte=&categorieLien=cid)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033218964&cidTexte=LEGITEXT000031366350&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033218964&cidTexte=LEGITEXT000031366350&dateTexte=20180114)

* Article L311-8

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219306&dateTexte=20180111&categorieLien=id#LEGIARTI000033219306](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219306&dateTexte=20180111&categorieLien=id#LEGIARTI000033219306)

* Article L311-9

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218942&dateTexte=20180111&categorieLien=id#LEGIARTI000033218942](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218942&dateTexte=20180111&categorieLien=id#LEGIARTI000033218942)

* Article L312-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218954&dateTexte=20180111&categorieLien=id#LEGIARTI000033218954](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218954&dateTexte=20180111&categorieLien=id#LEGIARTI000033218954)

* Article L312-1-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205512&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205512&dateTexte=&categorieLien=id)

* Article L312-1-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205514&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205514&dateTexte=&categorieLien=id)

* Article L312-1-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205516&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205516&dateTexte=&categorieLien=id)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033205516&cidTexte=LEGITEXT000031366350&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033205516&cidTexte=LEGITEXT000031366350&dateTexte=20180114)

* Article L321-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219044&dateTexte=20180111&categorieLien=id#LEGIARTI000033219044](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219044&dateTexte=20180111&categorieLien=id#LEGIARTI000033219044)

* Article L321-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218992&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218992&dateTexte=&categorieLien=id)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218992&dateTexte=20180111&categorieLien=id#LEGIARTI000033218992](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218992&dateTexte=20180111&categorieLien=id#LEGIARTI000033218992)

* Article L321-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205561&dateTexte=20180111&categorieLien=id#LEGIARTI000033205561](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205561&dateTexte=20180111&categorieLien=id#LEGIARTI000033205561)

* Article L321-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219118&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219118&dateTexte=&categorieLien=id)

* Article L322-6

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219056&dateTexte=20180111&categorieLien=id#LEGIARTI000033219056](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219056&dateTexte=20180111&categorieLien=id#LEGIARTI000033219056)

* Article L323-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219073&dateTexte=20180111&categorieLien=id#LEGIARTI000033219073](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219073&dateTexte=20180111&categorieLien=id#LEGIARTI000033219073)

* Article L324-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219051&dateTexte=20180111&categorieLien=id#LEGIARTI000033219051](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219051&dateTexte=20180111&categorieLien=id#LEGIARTI000033219051)

* Article L324-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219078&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219078&dateTexte=&categorieLien=id)

* Article L324-6

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219083&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219083&dateTexte=&categorieLien=id)

* Article L325-7

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219064&dateTexte=20180111&categorieLien=id#LEGIARTI000033219064](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219064&dateTexte=20180111&categorieLien=id#LEGIARTI000033219064)

* Article L326-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219110&dateTexte=20180111&categorieLien=id#LEGIARTI000033219110](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219110&dateTexte=20180111&categorieLien=id#LEGIARTI000033219110)

* Article L341-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219095&dateTexte=20180111&categorieLien=id#LEGIARTI000033219095](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219095&dateTexte=20180111&categorieLien=id#LEGIARTI000033219095)

* Article L341-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205724&dateTexte=20180111&categorieLien=id#LEGIARTI000033205724](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205724&dateTexte=20180111&categorieLien=id#LEGIARTI000033205724)

* Article L342-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219103&dateTexte=20180111&categorieLien=id#LEGIARTI000033219103](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219103&dateTexte=20180111&categorieLien=id#LEGIARTI000033219103)

* Article L342-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218880&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033218880&dateTexte=&categorieLien=id)

* Article L342-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219087&dateTexte=20180111&categorieLien=id#LEGIARTI000033219087](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033219087&dateTexte=20180111&categorieLien=id#LEGIARTI000033219087)

* Article L342-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205627&dateTexte=20180111&categorieLien=id#LEGIARTI000033205627](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033205627&dateTexte=20180111&categorieLien=id#LEGIARTI000033205627)

* Article L552-15

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220463&dateTexte=20180111&categorieLien=id#LEGIARTI000033220463](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220463&dateTexte=20180111&categorieLien=id#LEGIARTI000033220463)

* Article L552-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220550&dateTexte=20180111&categorieLien=id#LEGIARTI000033220550](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220550&dateTexte=20180111&categorieLien=id#LEGIARTI000033220550)

* Article L552-8

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220497&dateTexte=20180111&categorieLien=id#LEGIARTI000033220497](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220497&dateTexte=20180111&categorieLien=id#LEGIARTI000033220497)

* Article L553-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220452&dateTexte=20180111&categorieLien=id#LEGIARTI000033220452](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220452&dateTexte=20180111&categorieLien=id#LEGIARTI000033220452)

* Article L562-16

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220429&dateTexte=20180111&categorieLien=id#LEGIARTI000033220429](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220429&dateTexte=20180111&categorieLien=id#LEGIARTI000033220429)

* Article L562-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220529&dateTexte=20180111&categorieLien=id#LEGIARTI000033220529](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220529&dateTexte=20180111&categorieLien=id#LEGIARTI000033220529)

* Article L562-8

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220486&dateTexte=20180111&categorieLien=id#LEGIARTI000033220486](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220486&dateTexte=20180111&categorieLien=id#LEGIARTI000033220486)

* Article L563-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220441&dateTexte=20180111&categorieLien=id#LEGIARTI000033220441](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220441&dateTexte=20180111&categorieLien=id#LEGIARTI000033220441)

* Article L572-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220508&dateTexte=20180111&categorieLien=id#LEGIARTI000033220508](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220508&dateTexte=20180111&categorieLien=id#LEGIARTI000033220508)

* Article L574-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220475&dateTexte=20180111&categorieLien=id#LEGIARTI000033220475](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220475&dateTexte=20180111&categorieLien=id#LEGIARTI000033220475)

* Article L574-5

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220418&dateTexte=20180111&categorieLien=id#LEGIARTI000033220418](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000031366350&idArticle=LEGIARTI000033220418&dateTexte=20180111&categorieLien=id#LEGIARTI000033220418)

* Article R311-3-1-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000034195881&cidTexte=LEGITEXT000031366350&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000034195881&cidTexte=LEGITEXT000031366350&dateTexte=20180114)

* Livre III : L'ACCÈS AUX DOCUMENTS ADMINISTRATIFS ET LA RÉUTILISATION DES INFORMATIONS PUBLIQUES

  * Titre Ier : LE DROIT D'ACCÈS AUX DOCUMENTS ADMINISTRATIFS — Chapitre Ier : Communication des documents administratifs — Section 1 : Etendue du droit à communication

    * [https://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000033218946&idSectionTA=LEGISCTA000031367696&cidTexte=LEGITEXT000031366350&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000033218946&idSectionTA=LEGISCTA000031367696&cidTexte=LEGITEXT000031366350&dateTexte=20180114)

### Code des transports

* Article L1112-8

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000023086525&idArticle=LEGIARTI000033220309&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000023086525&idArticle=LEGIARTI000033220309&dateTexte=&categorieLien=id)

### Code du patrimoine

* Article L211-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074236&idArticle=LEGIARTI000006845560](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074236&idArticle=LEGIARTI000006845560)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006845560&cidTexte=LEGITEXT000006074236&dateTexte=20180112](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006845560&cidTexte=LEGITEXT000006074236&dateTexte=20180112)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006845560&cidTexte=LEGITEXT000006074236&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006845560&cidTexte=LEGITEXT000006074236&dateTexte=20180114)

* Article L212-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074236&idArticle=LEGIARTI000006845568&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074236&idArticle=LEGIARTI000006845568&dateTexte=&categorieLien=cid)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019202832&cidTexte=LEGITEXT000006074236&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019202832&cidTexte=LEGITEXT000006074236&dateTexte=20180114)

* Article L212-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019202826&cidTexte=LEGITEXT000006074236&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019202826&cidTexte=LEGITEXT000006074236&dateTexte=20180114)

* Article L213-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074236&idArticle=LEGIARTI000033219315&dateTexte=20180111&categorieLien=id#LEGIARTI000033219315](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074236&idArticle=LEGIARTI000033219315&dateTexte=20180111&categorieLien=id#LEGIARTI000033219315)

* Partie législative

  * LIVRE II : ARCHIVES — TITRE Ier : RÉGIME GÉNÉRAL DES ARCHIVES

    * Chapitre 1er : Dispositions générales

      * [https://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000032860025&idSectionTA=LEGISCTA000006159940&cidTexte=LEGITEXT000006074236&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000032860025&idSectionTA=LEGISCTA000006159940&cidTexte=LEGITEXT000006074236&dateTexte=20180114)

    * Chapitre 3 : Régime de communication

      * [https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006159942&cidTexte=LEGITEXT000006074236&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006159942&cidTexte=LEGITEXT000006074236&dateTexte=20180114)

### Code du tourisme

* Article L324-1-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074073&idArticle=LEGIARTI000033219619&dateTexte=20180111&categorieLien=id#LEGIARTI000033219619](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074073&idArticle=LEGIARTI000033219619&dateTexte=20180111&categorieLien=id#LEGIARTI000033219619)

* Article L324-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074073&idArticle=LEGIARTI000033219625&dateTexte=20180111&categorieLien=id#LEGIARTI000033219625](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074073&idArticle=LEGIARTI000033219625&dateTexte=20180111&categorieLien=id#LEGIARTI000033219625)

* Article L324-2-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074073&idArticle=LEGIARTI000033219612&dateTexte=20180111&categorieLien=id#LEGIARTI000033219612](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074073&idArticle=LEGIARTI000033219612&dateTexte=20180111&categorieLien=id#LEGIARTI000033219612)

### Code du travail

* Article L1221-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006900841&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006900841&dateTexte=&categorieLien=cid)

* Article L1242-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006901194&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006901194&dateTexte=&categorieLien=cid)

* Article L1243-8

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006901219&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006901219&dateTexte=&categorieLien=cid)

* Article L1244-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006901228&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006901228&dateTexte=&categorieLien=cid)

* Article L1246-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006901235&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006901235&dateTexte=&categorieLien=cid)

* Article L1248-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006901237&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006901237&dateTexte=&categorieLien=cid)

* Article L5212-13

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000033220318&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000033220318&dateTexte=&categorieLien=id)

* Article L6111-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000033220406&dateTexte=20180111&categorieLien=id#LEGIARTI000033220406](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000033220406&dateTexte=20180111&categorieLien=id#LEGIARTI000033220406)

* Article L6321-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000033220410&dateTexte=20180111&categorieLien=id#LEGIARTI000033220410](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000033220410&dateTexte=20180111&categorieLien=id#LEGIARTI000033220410)

* Article L7124-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000033220148&dateTexte=20180111&categorieLien=id#LEGIARTI000033220148](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000033220148&dateTexte=20180111&categorieLien=id#LEGIARTI000033220148)

### Code du travail applicable à Mayotte

* Article L328-18

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072052&idArticle=LEGIARTI000033220327&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072052&idArticle=LEGIARTI000033220327&dateTexte=&categorieLien=id)

### Code général de la propriété des personnes publiques

* Article L2125-10

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070299&idArticle=LEGIARTI000033216843&dateTexte=20180111&categorieLien=id#LEGIARTI000033216843](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070299&idArticle=LEGIARTI000033216843&dateTexte=20180111&categorieLien=id#LEGIARTI000033216843)

* Partie législative

  * Livre Ier : INFORMATION DES CONSOMMATEURS ET PRATIQUES COMMERCIALES — Titre II : PRATIQUES COMMERCIALES INTERDITES ET PRATIQUES COMMERCIALES RÉGLEMENTÉES — Chapitre II : Pratiques commerciales réglementées — Section 3 : Règles propres à certaines publicités et pratiques commerciales — Section 4 : Dispositions particulières aux services de communications électroniques utilisant le domaine public hertzien

    * [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006070299&idSectionTA=LEGISCTA000033216841&dateTexte=20180111&categorieLien=id#LEGISCTA000033216841](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006070299&idSectionTA=LEGISCTA000033216841&dateTexte=20180111&categorieLien=id#LEGISCTA000033216841)

### Code général des collectivités territoriales

* Article L1112-23

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000032273137&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000032273137&dateTexte=&categorieLien=id)

* Article L1212-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033745626&cidTexte=LEGITEXT000006070633&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033745626&cidTexte=LEGITEXT000006070633&dateTexte=20180114)

* Article L1425-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219830&dateTexte=20180111&categorieLien=id#LEGIARTI000033219830](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219830&dateTexte=20180111&categorieLien=id#LEGIARTI000033219830)

* Article L1425-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219769&dateTexte=20180111&categorieLien=id#LEGIARTI000033219769](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219769&dateTexte=20180111&categorieLien=id#LEGIARTI000033219769)

* Article L1615-7

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219842&dateTexte=20180111&categorieLien=id#LEGIARTI000033219842](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219842&dateTexte=20180111&categorieLien=id#LEGIARTI000033219842)

* Article L1821-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033218977&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033218977&dateTexte=&categorieLien=id)

* Article L2212-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019983190&cidTexte=LEGITEXT000006070633](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019983190&cidTexte=LEGITEXT000006070633)

* Article L2213-2

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033220276&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033220276&dateTexte=&categorieLien=id)

* Article L2223-42

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000021940466&dateTexte=20110214](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000021940466&dateTexte=20110214)

* Article L3661-16

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219168&dateTexte=20180111&categorieLien=id#LEGIARTI000033219168](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219168&dateTexte=20180111&categorieLien=id#LEGIARTI000033219168)

* Article L4313-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219161&dateTexte=20180111&categorieLien=id#LEGIARTI000033219161](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219161&dateTexte=20180111&categorieLien=id#LEGIARTI000033219161)

* Article L5217-10-15

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219154&dateTexte=20180111&categorieLien=id#LEGIARTI000033219154](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219154&dateTexte=20180111&categorieLien=id#LEGIARTI000033219154)

* Article L5219-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219781&dateTexte=20180111&categorieLien=id#LEGIARTI000033219781](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219781&dateTexte=20180111&categorieLien=id#LEGIARTI000033219781)

* Article L71-111-15

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219147&dateTexte=20180111&categorieLien=id#LEGIARTI000033219147](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219147&dateTexte=20180111&categorieLien=id#LEGIARTI000033219147)

* Article L72-101-15

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219140&dateTexte=20180111&categorieLien=id#LEGIARTI000033219140](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000033219140&dateTexte=20180111&categorieLien=id#LEGIARTI000033219140)

### Code général des impôts

* Article 1011 bis

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220362&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220362&dateTexte=&categorieLien=id)

* Article 1011 ter

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220358&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220358&dateTexte=&categorieLien=id)

* Article 1042 B

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033219263&dateTexte=20180111&categorieLien=id#LEGIARTI000033219263](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033219263&dateTexte=20180111&categorieLien=id#LEGIARTI000033219263)

* Article 1411

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220348&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220348&dateTexte=&categorieLien=id)

* Article 150 U

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220293&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220293&dateTexte=&categorieLien=id)

* Article 168

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220379&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220379&dateTexte=&categorieLien=id)

* Article 195

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220374&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220374&dateTexte=&categorieLien=id)

* Article 196 A bis

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220369&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220369&dateTexte=&categorieLien=id)

* Article 244 quater J

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220279&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033220279&dateTexte=&categorieLien=id)

* Article 39 decies

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033219859&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069577&idArticle=LEGIARTI000033219859&dateTexte=&categorieLien=id)

### Code monétaire et financier

* [https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006072026&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006072026&dateTexte=&categorieLien=cid)

* Article L311-4

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072026&idArticle=LEGIARTI000033220087&dateTexte=20180111&categorieLien=id#LEGIARTI000033220087](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072026&idArticle=LEGIARTI000033220087&dateTexte=20180111&categorieLien=id#LEGIARTI000033220087)

* Article L521-3

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072026&idArticle=LEGIARTI000033220074&dateTexte=20180111&categorieLien=id#LEGIARTI000033220074](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072026&idArticle=LEGIARTI000033220074&dateTexte=20180111&categorieLien=id#LEGIARTI000033220074)

* Article L521-3-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072026&idArticle=LEGIARTI000033207500&dateTexte=20180111&categorieLien=id#LEGIARTI000033207500](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072026&idArticle=LEGIARTI000033207500&dateTexte=20180111&categorieLien=id#LEGIARTI000033207500)

* Article L525-6

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072026&idArticle=LEGIARTI000033220060&dateTexte=20180111&categorieLien=id#LEGIARTI000033220060](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072026&idArticle=LEGIARTI000033220060&dateTexte=20180111&categorieLien=id#LEGIARTI000033220060)

* Article L525-6-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072026&idArticle=LEGIARTI000033207512&dateTexte=20180111&categorieLien=id#LEGIARTI000033207512](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072026&idArticle=LEGIARTI000033207512&dateTexte=20180111&categorieLien=id#LEGIARTI000033207512)

* Article L526-11

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072026&idArticle=LEGIARTI000033220050&dateTexte=20180111&categorieLien=id#LEGIARTI000033220050](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072026&idArticle=LEGIARTI000033220050&dateTexte=20180111&categorieLien=id#LEGIARTI000033220050)

### Code pénal

* Article 226-13

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417944&dateTexte&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417944&dateTexte&categorieLien=cid)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417944&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417944&dateTexte=&categorieLien=cid)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417945](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417945)

* Article 226-16

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417955&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417955&dateTexte=&categorieLien=cid)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000033219732&dateTexte=20180111&categorieLien=id#LEGIARTI000033219732](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000033219732&dateTexte=20180111&categorieLien=id#LEGIARTI000033219732)
  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033219732&cidTexte=LEGITEXT000006070719&dateTexte=20180114](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033219732&cidTexte=LEGITEXT000006070719&dateTexte=20180114)

* Article 226-2-1

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000033207318&dateTexte=20180111&categorieLien=id#LEGIARTI000033207318](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000033207318&dateTexte=20180111&categorieLien=id#LEGIARTI000033207318)

* Article 226-21

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417981](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417981)

* Article 226-24

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000020630874](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000020630874)

* Article 226-6

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000033219748&dateTexte=20180111&categorieLien=id#LEGIARTI000033219748](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000033219748&dateTexte=20180111&categorieLien=id#LEGIARTI000033219748)

* Article 413-10

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006418402&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006418402&dateTexte=&categorieLien=cid)

### Conseil constitutionnel

* Convention pour la protection des personnes à l'égard du traitement automatisé des données à caractère personnel

  * [https://www.coe.int/en/web/conventions/full-list/-/conventions/rms/0900001680078b39](https://www.coe.int/en/web/conventions/full-list/-/conventions/rms/0900001680078b39)

* Décision n° 2003-467 DC du 13 mars 2003

  * Loi pour la sécurité intérieure

    * [http://www.Conseil-constitutionnel.fr/Conseil-con..c/decision-n-2003-467-dc-du-13-mars-2003.855.html](http://www.Conseil-constitutionnel.fr/Conseil-con..c/decision-n-2003-467-dc-du-13-mars-2003.855.html)

* Décision n° 2004-499 DC du 29 juillet 2004

  * Loi relative à la protection des personnes physiques à l'égard des traitements de données à caractère personnel et modifiant la Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

    * [http://www.Conseil-constitutionnel.fr/Conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2004/2004-499-dc/decision-n-2004-499-dc-du-29-juillet-2004.904.html](http://www.Conseil-constitutionnel.fr/Conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2004/2004-499-dc/decision-n-2004-499-dc-du-29-juillet-2004.904.html)

* Décision n° 2008-566 DC du 9 juillet 2008

  * Loi organique relative aux archives du Conseil constitutionnel

    * [http://www.Conseil-constitutionnel.fr/Conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2008/2008-566-dc/decision-n-2008-566-dc-du-9-juillet-2008.17216.html](http://www.Conseil-constitutionnel.fr/Conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2008/2008-566-dc/decision-n-2008-566-dc-du-9-juillet-2008.17216.html)

* Décision n° 2012-280 QPC du 12 octobre 2012

  * Société Groupe Canal Plus et autre [Autorité de la concurrence : organisation et pouvoir de sanction]

    * [http://www.Conseil-constitutionnel.fr/Conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2012/2012-280-qpc/decision-n-2012-280-qpc-du-12-octobre-2012.115745.html](http://www.Conseil-constitutionnel.fr/Conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2012/2012-280-qpc/decision-n-2012-280-qpc-du-12-octobre-2012.115745.html)

* Décision n° 2012-652 DC du 22 mars 2012

  * Loi relative à la protection de l'identité

    * [http://www.Conseil-constitutionnel.fr/decision/2012/2012-652-dc/decision-n-2012-652-dc-du-22-mars-2012.105165.html](http://www.Conseil-constitutionnel.fr/decision/2012/2012-652-dc/decision-n-2012-652-dc-du-22-mars-2012.105165.html)

* Décision n° 2017-670 QPC du 27 octobre 2017

  * M. Mikhail P. [Effacement anticipé des données à caractère personnel inscrites dans un fichier de traitement d'antécédents judiciaires]

    * [http://www.Conseil-constitutionnel.fr/Conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2017/2017-670-qpc/decision-n-2017-670-qpc-du-27-octobre-2017.149951.html](http://www.Conseil-constitutionnel.fr/Conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2017/2017-670-qpc/decision-n-2017-670-qpc-du-27-octobre-2017.149951.html)

* Décision n° 2017-746 DC du 19 janvier 2017

  * Loi organique relative aux autorités administratives indépendantes et autorités publiques indépendantes

    * [http://www.Conseil-constitutionnel.fr/Conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2017/2017-746-dc/decision-n-2017-746-dc-du-19-janvier-2017.148502.html](http://www.Conseil-constitutionnel.fr/Conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2017/2017-746-dc/decision-n-2017-746-dc-du-19-janvier-2017.148502.html)

* Décision n° 80-116 DC du 17 juillet 1980

  * Loi autorisant la ratification de la convention franco-allemande additionnelle à la Convention européenne d'entraide judiciaire en matière pénale du 20 avril 1959

    * [http://www.Conseil-constitutionnel.fr/Conseil-con..decision-n-80-116-dc-du-17-juillet-1980.7771.html](http://www.Conseil-constitutionnel.fr/Conseil-con..decision-n-80-116-dc-du-17-juillet-1980.7771.html)

* Décision n° 91-294 DC du 25 juillet 1991

  * Loi autorisant l'approbation de la convention d'application de l'accord de Schengen du 14 juin 1985 entre les gouvernements des Etats de l'Union économique Benelux, de la République fédérale d'Allemagne et de la République française relatif à la suppression graduelle des contrôles aux frontières communes

    * [http://www.Conseil-constitutionnel.fr/Conseil-con..decision-n-91-294-dc-du-25-juillet-1991.8764.html](http://www.Conseil-constitutionnel.fr/Conseil-con..decision-n-91-294-dc-du-25-juillet-1991.8764.html)

### Conseil d’État

* 5 / 3 SSR, du 27 septembre 1989,

  * 74548 74549 74550, mentionné aux tables du recueil Lebon

    * [https://www.legifrance.gouv.fr/affichJuriAdmin.do?idTexte=CETATEXT000007768025](https://www.legifrance.gouv.fr/affichJuriAdmin.do?idTexte=CETATEXT000007768025)

* Avis sur un Projet de loi d’adaptation au droit de l’Union européenne de la Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

  * [http://www.Conseil-etat.fr/content/download/121205/1225385/version/2/file/393836_AVIS.pdf](http://www.Conseil-etat.fr/content/download/121205/1225385/version/2/file/393836_AVIS.pdf) (PDF)

* Etude annuelle 2013 : Le droit souple

  * [http://www.Conseil-etat.fr/Decisions-Avis-Publications/Etudes-Publications/Rapports-Etudes/Etude-annuelle-2013-Le-droit-souple](http://www.Conseil-etat.fr/Decisions-Avis-Publications/Etudes-Publications/Rapports-Etudes/Etude-annuelle-2013-Le-droit-souple)

* Etude annuelle 2017 - Puissance publique et plateformes numériques : accompagner l’«ubérisation»

  * [http://www.Conseil-etat.fr/Decisions-Avis-Publications/Etudes-Publications/Rapports-Etudes/Etude-annuelle-2017-Puissance-publique-et-plateformes-numeriques-accompagner-l-uberisation](http://www.Conseil-etat.fr/Decisions-Avis-Publications/Etudes-Publications/Rapports-Etudes/Etude-annuelle-2017-Puissance-publique-et-plateformes-numeriques-accompagner-l-uberisation)

* Rapport : Le numérique et les droits fondamentaux

  * [http://www.ladocumentationfrancaise.fr/var/storage/rapports-publics/144000541.pdf](http://www.ladocumentationfrancaise.fr/var/storage/rapports-publics/144000541.pdf) (PDF)

* Section du Contentieux, 06/11/2009

  * 304300, Publié au recueil Lebon

    * [https://www.legifrance.gouv.fr/affichJuriAdmin.do?oldAction=rechJuriAdmin&idTexte=CETATEXT000021242880](https://www.legifrance.gouv.fr/affichJuriAdmin.do?oldAction=rechJuriAdmin&idTexte=CETATEXT000021242880)

* Étude annuelle 2014 - Le numérique et les droits fondamentaux

  * [http://www.Conseil-etat.fr/Decisions-Avis-Publications/Etudes-Publications/Rapports-Etudes/Etude-annuelle-2014-Le-numerique-et-les-droits-fondamentaux](http://www.Conseil-etat.fr/Decisions-Avis-Publications/Etudes-Publications/Rapports-Etudes/Etude-annuelle-2014-Le-numerique-et-les-droits-fondamentaux)

### Constitution du 4 octobre 1958

* Article 38

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000571356&idArticle=LEGIARTI000006527512&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000571356&idArticle=LEGIARTI000006527512&dateTexte=&categorieLien=cid)

* Article 64

  * [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000571356&idArticle=&dateTexte=20180114#LEGIARTI000006527555](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000571356&idArticle=&dateTexte=20180114#LEGIARTI000006527555)

### Cour de cassation civile, Chambre commerciale, 20 janvier 2015

* 13-16.745 13-16.764 13-16.765 13-16.955, Publié au bulletin

  * [https://www.legifrance.gouv.fr/affichJuriJudi.do?oldAction=rechJuriJudi&idTexte=JURITEXT000030142815&fastPos=1](https://www.legifrance.gouv.fr/affichJuriJudi.do?oldAction=rechJuriJudi&idTexte=JURITEXT000030142815&fastPos=1)

### Création d’un Comité d’Expertise pour les Recherches, les Etudes et les Evaluations dans le domaine de la Santé (CEREES)

* [https://www.snds.gouv.fr/SNDS/Actualites/Actu-2](https://www.snds.gouv.fr/SNDS/Actualites/Actu-2)

### Déclaration des Droits de l'Homme et du Citoyen de 1789

* [https://www.legifrance.gouv.fr/Droit-francais/Constitution/Declaration-des-Droits-de-l-Homme-et-du-Citoyen-de-1789](https://www.legifrance.gouv.fr/Droit-francais/Constitution/Declaration-des-Droits-de-l-Homme-et-du-Citoyen-de-1789)

### Décret n° 2007-451 du 25 mars 2007 modifiant le décret n° 2005-1309 du 20 octobre 2005 pris pour l'application de la Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, modifiée par la Loi n° 2004-801 du 6 août 2004.

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000824352&dateTexte](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000824352&dateTexte)

### Décret n° 2016-1872 du 26 décembre 2016 modifiant le décret n° 2005-1309 du 20 octobre 2005 pris pour l'application de la Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

* [https://www.legifrance.gouv.fr/eli/decret/2016/12/26/AFSE1625288D/jo/texte](https://www.legifrance.gouv.fr/eli/decret/2016/12/26/AFSE1625288D/jo/texte)

### Décret n° 2017-719 du 2 mai 2017 relatif aux services publics d'archives, aux conditions de mutualisation des archives numériques et aux conventions de dépôt d'archives communales

* [https://www.legifrance.gouv.fr/eli/decret/2017/5/2/MCCB1632109D/jo/texte](https://www.legifrance.gouv.fr/eli/decret/2017/5/2/MCCB1632109D/jo/texte)

### Décret n°2005-1309 du 20 octobre 2005 pris pour l'application de la Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000241445](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000241445)
* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006052581&dateTexte=20090319](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006052581&dateTexte=20090319)

* Article 91

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000006551811&cidTexte=LEGITEXT000006052581&dateTexte=20180114](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000006551811&cidTexte=LEGITEXT000006052581&dateTexte=20180114)

* Article 94

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000006551817&cidTexte=LEGITEXT000006052581&dateTexte=20180114](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000006551817&cidTexte=LEGITEXT000006052581&dateTexte=20180114)

### Décret n°2005-1309 du 20 octobre 2006 pris pour l'application de la Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.

* Article 77

  * [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000241445#LEGIARTI000025082770](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000241445#LEGIARTI000025082770)

### Décret n°87-249 du 8 avril 1987 relatif au fichier automatisé des empreintes digitales géré par le ministère de l'intérieur

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006065909](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006065909)

### Délibération n° 2011-233 du 21 juillet 2011 portant avis sur un projet de décret en Conseil d'Etat renforçant l'efficacité et la sécurité du bureau d'ordre national automatisé des procédures judiciaires dénommé « Cassiopée » (demande d'avis n° 11009621)

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025825985&dateTexte=20120508&categorieLien=cid](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025825985&dateTexte=20120508&categorieLien=cid)

### Livre des procédures fiscales

* Article L107 B

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069583&idArticle=LEGIARTI000033219212&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069583&idArticle=LEGIARTI000033219212&dateTexte=&categorieLien=id)

* Article L135 B

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069583&idArticle=LEGIARTI000033219216&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069583&idArticle=LEGIARTI000033219216&dateTexte=&categorieLien=id)

* Article L135 J

  * [https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069583&idArticle=LEGIARTI000033219208&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069583&idArticle=LEGIARTI000033219208&dateTexte=&categorieLien=id)

### Loi n° 2000-321 du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations

* Article 10

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000215117&idArticle=LEGIARTI000033219132&dateTexte=20180111&categorieLien=id#LEGIARTI000033219132](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000215117&idArticle=LEGIARTI000033219132&dateTexte=20180111&categorieLien=id#LEGIARTI000033219132)

* Article 41

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000215117&idArticle=LEGIARTI000006529237&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000215117&idArticle=LEGIARTI000006529237&dateTexte=&categorieLien=cid)

* Article 5

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000215117&idArticle=LEGIARTI000006529189&dateTexte=20000413&categorieLien=id#LEGIARTI000006529189](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000215117&idArticle=LEGIARTI000006529189&dateTexte=20000413&categorieLien=id#LEGIARTI000006529189)

### Loi n° 2003-239 du 18 mars 2003 pour la sécurité intérieure

* Article 22

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000412199&idArticle=LEGIARTI000006528905&dateTexte=20030319&categorieLien=id#LEGIARTI000006528905](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000412199&idArticle=LEGIARTI000006528905&dateTexte=20030319&categorieLien=id#LEGIARTI000006528905)

### Loi n° 2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique

* Article 52-1

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000801164&idArticle=LEGIARTI000033219880&dateTexte=20180111&categorieLien=id#LEGIARTI000033219880](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000801164&idArticle=LEGIARTI000033219880&dateTexte=20180111&categorieLien=id#LEGIARTI000033219880)

### Loi n° 2004-801 du 6 août 2004 relative à la protection des personnes physiques à l'égard des traitements de données à caractère personnel et modifiant la Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000441676](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000441676)
* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000441676&categorieLien=id](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000441676&categorieLien=id)

* Article 1

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529394&dateTexte=20040807&categorieLien=id#LEGIARTI000006529394](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529394&dateTexte=20040807&categorieLien=id#LEGIARTI000006529394)

* Article 10

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529403&dateTexte=20040807&categorieLien=id#LEGIARTI000006529403](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529403&dateTexte=20040807&categorieLien=id#LEGIARTI000006529403)

* Article 12

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529405&dateTexte=20040807&categorieLien=id#LEGIARTI000006529405](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529405&dateTexte=20040807&categorieLien=id#LEGIARTI000006529405)

* Article 2

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529395&dateTexte=20040807&categorieLien=id#LEGIARTI000006529395](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529395&dateTexte=20040807&categorieLien=id#LEGIARTI000006529395)
  * [https://www.legifrance.gouv.fr/eli/Loi/2004/8/6/JUSX0100026L/jo#JORFARTI000002057492](https://www.legifrance.gouv.fr/eli/Loi/2004/8/6/JUSX0100026L/jo#JORFARTI000002057492)

* Article 3

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529396&dateTexte=20040807&categorieLien=id#LEGIARTI000006529396](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529396&dateTexte=20040807&categorieLien=id#LEGIARTI000006529396)

* Article 4

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529397&dateTexte=20040807&categorieLien=id#LEGIARTI000006529397](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529397&dateTexte=20040807&categorieLien=id#LEGIARTI000006529397)

* Article 5

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529398&dateTexte=20040807&categorieLien=id#LEGIARTI000006529398](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529398&dateTexte=20040807&categorieLien=id#LEGIARTI000006529398)

* Article 8

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529401&dateTexte=20040807&categorieLien=id#LEGIARTI000006529401](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529401&dateTexte=20040807&categorieLien=id#LEGIARTI000006529401)

* Article 9

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529402&dateTexte=20040807&categorieLien=id#LEGIARTI000006529402](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000441676&idArticle=LEGIARTI000006529402&dateTexte=20040807&categorieLien=id#LEGIARTI000006529402)

### Loi n° 2005-102 du 11 février 2005 pour l'égalité des droits et des chances, la participation et la citoyenneté des personnes handicapées

* Article 47

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000809647&idArticle=LEGIARTI000033220197&dateTexte=20180111&categorieLien=id#LEGIARTI000033220197](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000809647&idArticle=LEGIARTI000033220197&dateTexte=20180111&categorieLien=id#LEGIARTI000033220197)

* Article 78

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000809647&idArticle=LEGIARTI000006682314&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000809647&idArticle=LEGIARTI000006682314&dateTexte=&categorieLien=cid)
  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000809647&idArticle=LEGIARTI000033220166&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000809647&idArticle=LEGIARTI000033220166&dateTexte=&categorieLien=id)

### Loi n° 2006-586 du 23 mai 2006 relative à l'engagement éducatif

* Article 22

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000607509&idArticle=LEGIARTI000006435898&dateTexte=20180111&categorieLien=id#LEGIARTI000006435898](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000607509&idArticle=LEGIARTI000006435898&dateTexte=20180111&categorieLien=id#LEGIARTI000006435898)

### Loi n° 2006-64 du 23 janvier 2006 relative à la lutte contre le terrorisme et portant dispositions diverses relatives à la sécurité et aux contrôles frontaliers.

* Article 13

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000454124&idArticle=LEGIARTI000006529864&dateTexte=20060124&categorieLien=id#LEGIARTI000006529864](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000454124&idArticle=LEGIARTI000006529864&dateTexte=20060124&categorieLien=id#LEGIARTI000006529864)

### Loi n° 2010-476 du 12 mai 2010 relative à l'ouverture à la concurrence et à la régulation du secteur des jeux d'argent et de hasard en ligne

* Article 14

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033220102&dateTexte=20180111&categorieLien=id#LEGIARTI000033220102](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033220102&dateTexte=20180111&categorieLien=id#LEGIARTI000033220102)

* Article 26

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033220111&dateTexte=20180111&categorieLien=id#LEGIARTI000033220111](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033220111&dateTexte=20180111&categorieLien=id#LEGIARTI000033220111)

* Article 34

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033220099&dateTexte=20180111&categorieLien=id#LEGIARTI000033220099](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033220099&dateTexte=20180111&categorieLien=id#LEGIARTI000033220099)

* Article 35

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033220135&dateTexte=20180111&categorieLien=id#LEGIARTI000033220135](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033220135&dateTexte=20180111&categorieLien=id#LEGIARTI000033220135)

* Article 38

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033220126&dateTexte=20180111&categorieLien=id#LEGIARTI000033220126](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033220126&dateTexte=20180111&categorieLien=id#LEGIARTI000033220126)

* Article 45-1

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033217004&dateTexte=20180111&categorieLien=id#LEGIARTI000033217004](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033217004&dateTexte=20180111&categorieLien=id#LEGIARTI000033217004)

* Article 45-2

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033217018&dateTexte=20180111&categorieLien=id#LEGIARTI000033217018](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033217018&dateTexte=20180111&categorieLien=id#LEGIARTI000033217018)

* Article 61

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033220116&dateTexte=20180111&categorieLien=id#LEGIARTI000033220116](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000022204510&idArticle=LEGIARTI000033220116&dateTexte=20180111&categorieLien=id#LEGIARTI000033220116)

### Loi n° 2011-334 du 29 mars 2011 relative au Défenseur des droits

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000023781252](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000023781252)

* Article 3

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782608&dateTexte=20110330&categorieLien=id#LEGIARTI000023782608](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782608&dateTexte=20110330&categorieLien=id#LEGIARTI000023782608)

* Article 6

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782612&dateTexte=20110330&categorieLien=id#LEGIARTI000023782612](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782612&dateTexte=20110330&categorieLien=id#LEGIARTI000023782612)

* Article 7

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782614&dateTexte=20110330&categorieLien=id#LEGIARTI000023782614](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782614&dateTexte=20110330&categorieLien=id#LEGIARTI000023782614)

* Article 8

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782616&dateTexte=20110330&categorieLien=id#LEGIARTI000023782616](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000023781252&idArticle=LEGIARTI000023782616&dateTexte=20110330&categorieLien=id#LEGIARTI000023782616)

### Loi n° 2012-300 du 5 mars 2012 relative aux recherches impliquant la personne humaine

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025441587](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025441587)

### Loi n° 2014-344 du 17 mars 2014 relative à la consommation

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028738036&categorieLien=id](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028738036&categorieLien=id)

* Article 105

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000028738036&idArticle=LEGIARTI000028740052&dateTexte=20140318&categorieLien=id#LEGIARTI000028740052](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000028738036&idArticle=LEGIARTI000028740052&dateTexte=20140318&categorieLien=id#LEGIARTI000028740052)

* Article 145

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000028738036&idArticle=LEGIARTI000033219948&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000028738036&idArticle=LEGIARTI000033219948&dateTexte=&categorieLien=id)

### Loi n° 2016-1321 du 7 octobre 2016 pour une République numérique

* [https://www.legifrance.gouv.fr/eli/Loi/2016/10/7/ECFI1524250L/jo/texte](https://www.legifrance.gouv.fr/eli/Loi/2016/10/7/ECFI1524250L/jo/texte)
* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746)
* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746&categorieLien=cid](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746&categorieLien=cid)
* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746&dateTexte=20180114](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746&dateTexte=20180114)

* Article 16

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=JORFARTI000033203039&cidTexte=JORFTEXT000033202746&dateTexte=29990101&categorieLien=id](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=JORFARTI000033203039&cidTexte=JORFTEXT000033202746&dateTexte=29990101&categorieLien=id)

* Article 26

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205094&dateTexte=20161008&categorieLien=id#LEGIARTI000033205094](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205094&dateTexte=20161008&categorieLien=id#LEGIARTI000033205094)

* Article 34

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205110&dateTexte=20161008&categorieLien=id#LEGIARTI000033205110](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205110&dateTexte=20161008&categorieLien=id#LEGIARTI000033205110)
  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=JORFARTI000033202858&cidTexte=JORFTEXT000033202746&dateTexte=29990101&categorieLien=id](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=JORFARTI000033202858&cidTexte=JORFTEXT000033202746&dateTexte=29990101&categorieLien=id)

* Article 35

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205112&dateTexte=20161008&categorieLien=id#LEGIARTI000033205112](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205112&dateTexte=20161008&categorieLien=id#LEGIARTI000033205112)

* Article 37

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205052&dateTexte=20161008&categorieLien=id#LEGIARTI000033205052](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205052&dateTexte=20161008&categorieLien=id#LEGIARTI000033205052)

* Article 54

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205074&dateTexte=20161008&categorieLien=id#LEGIARTI000033205074](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205074&dateTexte=20161008&categorieLien=id#LEGIARTI000033205074)

* Article 55

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205076&dateTexte=20161008&categorieLien=id#LEGIARTI000033205076](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205076&dateTexte=20161008&categorieLien=id#LEGIARTI000033205076)

* Article 56

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205198&dateTexte=20161008&categorieLien=id#LEGIARTI000033205198](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205198&dateTexte=20161008&categorieLien=id#LEGIARTI000033205198)

* Article 57

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205078&dateTexte=20161008&categorieLien=id#LEGIARTI000033205078](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205078&dateTexte=20161008&categorieLien=id#LEGIARTI000033205078)

* Article 58

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205200&dateTexte=20161008&categorieLien=id#LEGIARTI000033205200](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205200&dateTexte=20161008&categorieLien=id#LEGIARTI000033205200)

* Article 62

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205080&dateTexte=20161008&categorieLien=id#LEGIARTI000033205080](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205080&dateTexte=20161008&categorieLien=id#LEGIARTI000033205080)

* Article 63

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205208&dateTexte=20161008&categorieLien=id#LEGIARTI000033205208](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205208&dateTexte=20161008&categorieLien=id#LEGIARTI000033205208)

* Article 64

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205210&dateTexte=20161008&categorieLien=id#LEGIARTI000033205210](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205210&dateTexte=20161008&categorieLien=id#LEGIARTI000033205210)

* Article 65

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205212&dateTexte=20161008&categorieLien=id#LEGIARTI000033205212](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205212&dateTexte=20161008&categorieLien=id#LEGIARTI000033205212)

* Article 66

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205214&dateTexte=20161008&categorieLien=id#LEGIARTI000033205214](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033202746&idArticle=LEGIARTI000033205214&dateTexte=20161008&categorieLien=id#LEGIARTI000033205214)

### Loi n° 2016-1547 du 18 novembre 2016 de modernisation de la justice du XXIe siècle

* [https://www.legifrance.gouv.fr/eli/Loi/2016/11/18/JUSX1515639L/jo](https://www.legifrance.gouv.fr/eli/Loi/2016/11/18/JUSX1515639L/jo)
* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033418805&dateTexte=20180114](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033418805&dateTexte=20180114)

* Article 112

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033418805&idArticle=LEGIARTI000033423740&dateTexte=20161119&categorieLien=id#LEGIARTI000033423740](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033418805&idArticle=LEGIARTI000033423740&dateTexte=20161119&categorieLien=id#LEGIARTI000033423740)

* Article 91

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033418805&idArticle=LEGIARTI000033423900&dateTexte=20161119&categorieLien=id#LEGIARTI000033423900](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033418805&idArticle=LEGIARTI000033423900&dateTexte=20161119&categorieLien=id#LEGIARTI000033423900)

### Loi n° 2016-41 du 26 janvier 2016 de modernisation de notre système de santé

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031912641&categorieLien=id](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031912641&categorieLien=id)
* [https://www.legifrance.gouv.fr/eli/Loi/2016/1/26/AFSX1418355L/jo](https://www.legifrance.gouv.fr/eli/Loi/2016/1/26/AFSX1418355L/jo)

* Article 193

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031912641&idArticle=LEGIARTI000031916294&dateTexte=20160127&categorieLien=id#LEGIARTI000031916294)
  * [https://www.legifrance.gouv.fr/eli/Loi/2016/1/26/AFSX1418355L/jo#JORFARTI000031914480](https://www.legifrance.gouv.fr/eli/Loi/2016/1/26/AFSX1418355L/jo#JORFARTI000031914480)

### Loi n° 2016-731 du 3 juin 2016 renforçant la lutte contre le crime organisé, le terrorisme et leur financement, et améliorant l'efficacité et les garanties de la procédure pénale

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032627231&dateTexte=20180114](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032627231&dateTexte=20180114)

* Article 117

  * [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032627231&categorieLien=id#JORFARTI000032628147](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032627231&categorieLien=id#JORFARTI000032628147)

### Loi n° 2017-55 du 20 janvier 2017 portant statut général des autorités administratives indépendantes et des autorités publiques indépendantes

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033897475&categorieLien=id](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033897475&categorieLien=id)

* Article 22

  * [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033897475&categorieLien=id#JORFARTI000033897498](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033897475&categorieLien=id#JORFARTI000033897498)

* Article 40

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033897475&idArticle=LEGIARTI000033898735&dateTexte=20170121&categorieLien=id#LEGIARTI000033898735](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000033897475&idArticle=LEGIARTI000033898735&dateTexte=20170121&categorieLien=id#LEGIARTI000033898735)

### Loi n° 2017-86 du 27 janvier 2017 relative à l'égalité et à la citoyenneté

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033934948&categorieLien=id](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033934948&categorieLien=id)

### Loi n° 51-711 du 7 juin 1951 sur l'obligation, la coordination et le secret en matière de statistiques.

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000888573&categorieLien=cid](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000888573&categorieLien=cid)

* Article 3

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000888573&idArticle=LEGIARTI000033219185&dateTexte=20180111&categorieLien=id#LEGIARTI000033219185](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000888573&idArticle=LEGIARTI000033219185&dateTexte=20180111&categorieLien=id#LEGIARTI000033219185)

* Article 3 bis

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000888573&idArticle=LEGIARTI000033205803&dateTexte=20180111&categorieLien=id#LEGIARTI000033205803](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000888573&idArticle=LEGIARTI000033205803&dateTexte=20180111&categorieLien=id#LEGIARTI000033205803)

### Loi n° 65-557 du 10 juillet 1965 fixant le statut de la copropriété des immeubles bâtis

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000880200&categorieLien=cid](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000880200&categorieLien=cid)

* Article 24-2

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000880200&idArticle=LEGIARTI000033219851&dateTexte=20180111&categorieLien=id#LEGIARTI000033219851](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000880200&idArticle=LEGIARTI000033219851&dateTexte=20180111&categorieLien=id#LEGIARTI000033219851)

### Loi n° 70-9 du 2 janvier 1970 réglementant les conditions d'exercice des activités relatives à certaines opérations portant sur les immeubles et les fonds de commerce

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000512228&categorieLien=cid](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000512228&categorieLien=cid)

### Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460)
* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460&categorieLien=cid](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460&categorieLien=cid)

* Article 1

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219641&dateTexte=20180111&categorieLien=id#LEGIARTI000033219641](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219641&dateTexte=20180111&categorieLien=id#LEGIARTI000033219641)

* Article 11

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219687&dateTexte=20180111&categorieLien=id#LEGIARTI000033219687](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219687&dateTexte=20180111&categorieLien=id#LEGIARTI000033219687)

* Article 13

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219252&dateTexte=20180111&categorieLien=id#LEGIARTI000033219252](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219252&dateTexte=20180111&categorieLien=id#LEGIARTI000033219252)

* Article 15 bis

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033205694&dateTexte=20180111&categorieLien=id#LEGIARTI000033205694](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033205694&dateTexte=20180111&categorieLien=id#LEGIARTI000033205694)

* Article 22

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219302&dateTexte=20180111&categorieLien=id#LEGIARTI000033219302](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219302&dateTexte=20180111&categorieLien=id#LEGIARTI000033219302)

* Article 25

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219293&dateTexte=20180111&categorieLien=id#LEGIARTI000033219293](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219293&dateTexte=20180111&categorieLien=id#LEGIARTI000033219293)

* Article 27

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219283&dateTexte=20180111&categorieLien=id#LEGIARTI000033219283](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219283&dateTexte=20180111&categorieLien=id#LEGIARTI000033219283)

* Article 31

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219644&dateTexte=20180111&categorieLien=id#LEGIARTI000033219644](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219644&dateTexte=20180111&categorieLien=id#LEGIARTI000033219644)

* Article 32

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219668&dateTexte=20180111&categorieLien=id#LEGIARTI000033219668](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219668&dateTexte=20180111&categorieLien=id#LEGIARTI000033219668)

* Article 36

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219695&dateTexte=20180111&categorieLien=id#LEGIARTI000033219695](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219695&dateTexte=20180111&categorieLien=id#LEGIARTI000033219695)

* Article 40

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219719&dateTexte=20180111&categorieLien=id#LEGIARTI000033219719](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219719&dateTexte=20180111&categorieLien=id#LEGIARTI000033219719)

* Article 40-1

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219717&dateTexte=20180111&categorieLien=id#LEGIARTI000033219717](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219717&dateTexte=20180111&categorieLien=id#LEGIARTI000033219717)

* Article 43 bis

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033206945&dateTexte=20180111&categorieLien=id#LEGIARTI000033206945](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033206945&dateTexte=20180111&categorieLien=id#LEGIARTI000033206945)

* Article 45

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219729&dateTexte=20180111&categorieLien=id#LEGIARTI000033219729](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219729&dateTexte=20180111&categorieLien=id#LEGIARTI000033219729)

* Article 46

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219726&dateTexte=20180111&categorieLien=id#LEGIARTI000033219726](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219726&dateTexte=20180111&categorieLien=id#LEGIARTI000033219726)

* Article 47

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219741&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219741&dateTexte=&categorieLien=id)

* Article 49 bis

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033207314&dateTexte=20180111&categorieLien=id#LEGIARTI000033207314](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033207314&dateTexte=20180111&categorieLien=id#LEGIARTI000033207314)

* Article 58

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219654&dateTexte=20180111&categorieLien=id#LEGIARTI000033219654](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219654&dateTexte=20180111&categorieLien=id#LEGIARTI000033219654)

* Article 67

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219700&dateTexte=20180111&categorieLien=id#LEGIARTI000033219700](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219700&dateTexte=20180111&categorieLien=id#LEGIARTI000033219700)

* Article 71

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219280&dateTexte=20180111&categorieLien=id#LEGIARTI000033219280](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219280&dateTexte=20180111&categorieLien=id#LEGIARTI000033219280)

* Article 72

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033220571&dateTexte=20180111&categorieLien=id#LEGIARTI000033220571](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033220571&dateTexte=20180111&categorieLien=id#LEGIARTI000033220571)

* Article 8

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219323&dateTexte=20180111&categorieLien=id#LEGIARTI000033219323](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000886460&idArticle=LEGIARTI000033219323&dateTexte=20180111&categorieLien=id#LEGIARTI000033219323)

### Loi n° 78-753 du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000339241](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000339241)

* Article 13

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000020566920&cidTexte=LEGITEXT000006068643&dateTexte=20180114](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000020566920&cidTexte=LEGITEXT000006068643&dateTexte=20180114)

* Article 59

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000339241&idArticle=LEGIARTI000020951176&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000339241&idArticle=LEGIARTI000020951176&dateTexte=&categorieLien=id)

### Loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires. Loi dite Loi Le Pors.

* Article 6 sexies

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000504704&idArticle=LEGIARTI000033220185&dateTexte=20180111&categorieLien=id#LEGIARTI000033220185](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000504704&idArticle=LEGIARTI000033220185&dateTexte=20180111&categorieLien=id#LEGIARTI000033220185)

### Loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication (Loi Léotard)

* Article 13

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000512205&idArticle=LEGIARTI000033219121&dateTexte=20180111&categorieLien=id#LEGIARTI000033219121](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000512205&idArticle=LEGIARTI000033219121&dateTexte=20180111&categorieLien=id#LEGIARTI000033219121)

### Loi n° 87-588 du 30 juillet 1987 portant diverses mesures d'ordre social

* Article 88

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000687467&idArticle=LEGIARTI000033220273&dateTexte=&categorieLien=id](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000687467&idArticle=LEGIARTI000033220273&dateTexte=&categorieLien=id)

### Loi n° 90-449 du 31 mai 1990 visant à la mise en oeuvre du droit au logement (1)

* Article 6

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000159413&idArticle=LEGIARTI000033220401&dateTexte=20180111&categorieLien=id#LEGIARTI000033220401](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000159413&idArticle=LEGIARTI000033220401&dateTexte=20180111&categorieLien=id#LEGIARTI000033220401)

* Article 6-1

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000159413&idArticle=LEGIARTI000033220398&dateTexte=20180111&categorieLien=id#LEGIARTI000033220398](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000159413&idArticle=LEGIARTI000033220398&dateTexte=20180111&categorieLien=id#LEGIARTI000033220398)

* Article 6-3

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000159413&idArticle=LEGIARTI000033220395&dateTexte=20180111&categorieLien=id#LEGIARTI000033220395](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000159413&idArticle=LEGIARTI000033220395&dateTexte=20180111&categorieLien=id#LEGIARTI000033220395)

### Loi n° 90-568 du 2 juillet 1990 relative à l'organisation du service public de la poste et à France Télécom.

* Article 38

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000716493&idArticle=LEGIARTI000033219558&dateTexte=20180111&categorieLien=id#LEGIARTI000033219558](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000716493&idArticle=LEGIARTI000033219558&dateTexte=20180111&categorieLien=id#LEGIARTI000033219558)

* Article 6

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000716493&idArticle=LEGIARTI000033219560&dateTexte=20180111&categorieLien=id#LEGIARTI000033219560](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000716493&idArticle=LEGIARTI000033219560&dateTexte=20180111&categorieLien=id#LEGIARTI000033219560)

### Loi n° 94-548 du 1 juillet 1994 relative au traitement de données nominatives ayant pour fin la recherche dans le domaine de la santé et modifiant la Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés

* Article 1

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000547135&idArticle=LEGIARTI000006527877&dateTexte=19940702&categorieLien=id#LEGIARTI000006527877](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000547135&idArticle=LEGIARTI000006527877&dateTexte=19940702&categorieLien=id#LEGIARTI000006527877)

### Loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000369046&dateTexte=19950124](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000369046&dateTexte=19950124)

### Loi n° 99-641 du 27 juillet 1999 portant création d'une couverture maladie universelle

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000198392](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000198392)
* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000198392&categorieLien=cid](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000198392&categorieLien=cid)

### Ordonnance n° 2005-1516 du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives.

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232)

* Article 1

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000636232&idArticle=LEGIARTI000006317133&dateTexte=&categorieLien=cid](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000636232&idArticle=LEGIARTI000006317133&dateTexte=&categorieLien=cid)

### Ordonnance n° 2011-1012 du 24 août 2011 relative aux communications électroniques

* [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000024502658&dateTexte=20180114](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000024502658&dateTexte=20180114)

* Article 38

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000024502658&idArticle=LEGIARTI000024503343&dateTexte=20110826&categorieLien=id#LEGIARTI000024503343](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000024502658&idArticle=LEGIARTI000024503343&dateTexte=20110826&categorieLien=id#LEGIARTI000024503343)

### Ordonnance n° 2015-1341 du 23 octobre 2015 relative aux dispositions législatives du code des relations entre le public et l'administration

* Article 3

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031360943&idArticle=LEGIARTI000031365580&dateTexte=20151025&categorieLien=id#LEGIARTI000031365580](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031360943&idArticle=LEGIARTI000031365580&dateTexte=20151025&categorieLien=id#LEGIARTI000031365580)

### Ordonnance n° 2016-65 du 29 janvier 2016 relative aux contrats de concession

* Article 53-1

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031939947&idArticle=LEGIARTI000033205690&dateTexte=20180111&categorieLien=id#LEGIARTI000033205690](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031939947&idArticle=LEGIARTI000033205690&dateTexte=20180111&categorieLien=id#LEGIARTI000033205690)

* Article 78

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031939947&idArticle=LEGIARTI000033219127&dateTexte=20180111&categorieLien=id#LEGIARTI000033219127](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000031939947&idArticle=LEGIARTI000033219127&dateTexte=20180111&categorieLien=id#LEGIARTI000033219127)

### Ordonnance n° 2017-1718 du 20 décembre 2017 visant à compléter et mettre en cohérence les dispositions prises en application de la Loi n° 2017-1340 du 15 septembre 2017 d'habilitation à prendre par ordonnances les mesures pour le renforcement du dialogue social

* Article 3

  * [https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000036240557&idArticle=LEGIARTI000036241434&dateTexte=20171221&categorieLien=id#LEGIARTI000036241434](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000036240557&idArticle=LEGIARTI000036241434&dateTexte=20171221&categorieLien=id#LEGIARTI000036241434)

### Ordonnance n° 58-1067 du 7 novembre 1958 portant Loi organique sur le Conseil constitutionnel

* Article 56

  * [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006069199#LEGIARTI000019200604](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006069199#LEGIARTI000019200604)

### Ordonnance n° 58-1100 du 17 novembre 1958 relative au fonctionnement des assemblées parlementaires.

* Article 4 bis

  * [https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000705067#LEGIARTI000028288243](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000705067#LEGIARTI000028288243)

## ISO

### TR 19814:2017 — Information et documentation -- Gestion des fonds et collections pour les archives et les bibliothèques

* [https://www.iso.org/fr/standard/66263.html](https://www.iso.org/fr/standard/66263.html)

## ONU

### UNESCO — Déclaration universelle sur les archives

* [https://www.ica.org/sites/default/files/UDA_June2012_web_FR.pdf](https://www.ica.org/sites/default/files/UDA_June2012_web_FR.pdf) (PDF)

## Royaume Uni

### Legislation

* Loi de 1998 sur la protection des données

  * [http://www.legislation.gov.uk/ukpga/1998/29/section/12](http://www.legislation.gov.uk/ukpga/1998/29/section/12)

### Parliament

* Data Protection Bill [HL] 2017-19

  * [https://services.parliament.uk/bills/2017-19/dataprotection.html](https://services.parliament.uk/bills/2017-19/dataprotection.html)
